<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Acoli_Default_Firefox_esr
</name>

<description>
   <am>Acoli localisation of default installed Firefox ESR</am>
   <ar>Acoli localisation of default installed Firefox ESR</ar>
   <be>Acoli localisation of default installed Firefox ESR</be>
   <bg>Acoli localisation of default installed Firefox ESR</bg>
   <bn>Acoli localisation of default installed Firefox ESR</bn>
   <ca>Localització del Firefox-ESR en Acoli</ca>
   <cs>Acoli localisation of default installed Firefox ESR</cs>
   <da>Acoli localisation of default installed Firefox ESR</da>
   <de>Lokalisation für die westnilotische Sprache “Acholi” des als Standard installierten “Firefox ESR”</de>
   <el>Acoli για το Firefox ESR</el>
   <en>Acoli localisation of default installed Firefox ESR</en>
   <es_ES>Localización de Acoli de Firefox ESR instalado por defecto</es_ES>
   <es>Localización Acoli de Firefox ESR instalado por defecto</es>
   <et>Acoli localisation of default installed Firefox ESR</et>
   <eu>Acoli localisation of default installed Firefox ESR</eu>
   <fa>Acoli localisation of default installed Firefox ESR</fa>
   <fil_PH>Acoli localisation of default installed Firefox ESR</fil_PH>
   <fi>Acoli localisation of default installed Firefox ESR</fi>
   <fr_BE>Localisation en Ac(h)oli de l'installation par défaut pour Firefox ESR</fr_BE>
   <fr>Localisation en Ac(h)oli de l'installation par défaut pour Firefox ESR</fr>
   <gl_ES>Acoli localisation of default installed Firefox ESR</gl_ES>
   <gu>Acoli localisation of default installed Firefox ESR</gu>
   <he_IL>Acoli localisation of default installed Firefox ESR</he_IL>
   <hi>डिफ़ॉल्ट रूप से इंस्टॉल हो रखें फायरफॉक्स ईएसआर का अकोली संस्करण</hi>
   <hr>Acoli localisation of default installed Firefox ESR</hr>
   <hu>Acoli localisation of default installed Firefox ESR</hu>
   <id>Acoli localisation of default installed Firefox ESR</id>
   <is>Acoli localisation of default installed Firefox ESR</is>
   <it>Localizzazione acoli di Firefox ESR installato di default</it>
   <ja>デフォルトでインストールされた Firefox ESR をアチョリ語にローカライズ</ja>
   <kk>Acoli localisation of default installed Firefox ESR</kk>
   <ko>Acoli localisation of default installed Firefox ESR</ko>
   <ku>Acoli localisation of default installed Firefox ESR</ku>
   <lt>Acoli localisation of default installed Firefox ESR</lt>
   <mk>Acoli localisation of default installed Firefox ESR</mk>
   <mr>Acoli localisation of default installed Firefox ESR</mr>
   <nb_NO>Acoli localisation of default installed Firefox ESR</nb_NO>
   <nb>Acoli lokaltilpassing av standardinstallasjonen av Firefox ESR</nb>
   <nl_BE>Acoli localisation of default installed Firefox ESR</nl_BE>
   <nl>Acoli-lokalisatie van standaard geïnstalleerde Firefox ESR</nl>
   <or>Acoli localisation of default installed Firefox ESR</or>
   <pl>Acoli localisation of default installed Firefox ESR</pl>
   <pt_BR>Acoli Localização padrão para o Firefox ESR instalado</pt_BR>
   <pt>Dicionário Acoli para hunspell</pt>
   <ro>Acoli localisation of default installed Firefox ESR</ro>
   <ru>Acoli localisation of default installed Firefox ESR</ru>
   <sk>Acoli localisation of default installed Firefox ESR</sk>
   <sl>Acoli krajevne nastavitve privzeto nameščenega Firefox ESR</sl>
   <so>Acoli localisation of default installed Firefox ESR</so>
   <sq>Përkthim në akoli i Firefox-it ESR, instaluar si parazgjedhje</sq>
   <sr>Acoli localisation of default installed Firefox ESR</sr>
   <sv>Acoli lokalisering av standard-installerad Firefox ESR</sv>
   <th>Acoli localisation of default installed Firefox ESR</th>
   <tr>Öntanımlı olarak yüklü Firefox ESR'nin Acoli yerelleştirmesi</tr>
   <uk>Acoli localisation of default installed Firefox ESR</uk>
   <vi>Acoli localisation of default installed Firefox ESR</vi>
   <zh_CN>Acoli localisation of default installed Firefox ESR</zh_CN>
   <zh_HK>Acoli localisation of default installed Firefox ESR</zh_HK>
   <zh_TW>Acoli localisation of default installed Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ach
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ach
</uninstall_package_names>

</app>
