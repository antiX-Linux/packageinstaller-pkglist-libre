<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Galician_Default_Firefox_esr
</name>

<description>
   <am>Galician localisation of Firefox ESR</am>
   <ar>Galician localisation of Firefox ESR</ar>
   <be>Galician localisation of Firefox ESR</be>
   <bg>Galician localisation of Firefox ESR</bg>
   <bn>Galician localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Gallec</ca>
   <cs>Galician localisation of Firefox ESR</cs>
   <da>Galician localisation of Firefox ESR</da>
   <de>Galegische Lokalisation von “Firefox ESR”</de>
   <el>Γαλικία για Firefox ESR</el>
   <en>Galician localisation of Firefox ESR</en>
   <es_ES>Localización Gallego de Firefox ESR</es_ES>
   <es>Localización Gallego de Firefox ESR</es>
   <et>Galician localisation of Firefox ESR</et>
   <eu>Galician localisation of Firefox ESR</eu>
   <fa>Galician localisation of Firefox ESR</fa>
   <fil_PH>Galician localisation of Firefox ESR</fil_PH>
   <fi>Galician localisation of Firefox ESR</fi>
   <fr_BE>Localisation en galicien pour Firefox ESR</fr_BE>
   <fr>Localisation en galicien pour Firefox ESR</fr>
   <gl_ES>Galician localisation of Firefox ESR</gl_ES>
   <gu>Galician localisation of Firefox ESR</gu>
   <he_IL>Galician localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का गलिसियाई संस्करण</hi>
   <hr>Galician localisation of Firefox ESR</hr>
   <hu>Galician localisation of Firefox ESR</hu>
   <id>Galician localisation of Firefox ESR</id>
   <is>Galician localisation of Firefox ESR</is>
   <it>Localizzazione in lingua galiziana di Firefox ESR</it>
   <ja>ガリシア語版 Firefox ESR</ja>
   <kk>Galician localisation of Firefox ESR</kk>
   <ko>Galician localisation of Firefox ESR</ko>
   <ku>Galician localisation of Firefox ESR</ku>
   <lt>Galician localisation of Firefox ESR</lt>
   <mk>Galician localisation of Firefox ESR</mk>
   <mr>Galician localisation of Firefox ESR</mr>
   <nb_NO>Galician localisation of Firefox ESR</nb_NO>
   <nb>Galisisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Galician localisation of Firefox ESR</nl_BE>
   <nl>Galicische lokalisatie van Firefox ESR</nl>
   <or>Galician localisation of Firefox ESR</or>
   <pl>Galician localisation of Firefox ESR</pl>
   <pt_BR>Galego Localização para o Firefox ESR</pt_BR>
   <pt>Galego Localização para Firefox ESR</pt>
   <ro>Galician localisation of Firefox ESR</ro>
   <ru>Galician localisation of Firefox ESR</ru>
   <sk>Galician localisation of Firefox ESR</sk>
   <sl>Galicijske  krajevne nastavitve za Firefox ESR</sl>
   <so>Galician localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në galicisht</sq>
   <sr>Galician localisation of Firefox ESR</sr>
   <sv>Galicisk lokalisering av Firefox ESR</sv>
   <th>Galician localisation of Firefox ESR</th>
   <tr>Firefox ESR Galiçyaca yerelleştirmesi</tr>
   <uk>Galician localisation of Firefox ESR</uk>
   <vi>Galician localisation of Firefox ESR</vi>
   <zh_CN>Galician localisation of Firefox ESR</zh_CN>
   <zh_HK>Galician localisation of Firefox ESR</zh_HK>
   <zh_TW>Galician localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-gl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-gl
</uninstall_package_names>

</app>
