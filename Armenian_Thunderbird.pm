<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Armenian_Thunderbird
</name>

<description>
   <am>Armenian localisation of Thunderbird</am>
   <ar>Armenian localisation of Thunderbird</ar>
   <be>Armenian localisation of Thunderbird</be>
   <bg>Armenian localisation of Thunderbird</bg>
   <bn>Armenian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Armeni</ca>
   <cs>Armenian localisation of Thunderbird</cs>
   <da>Armenian localisation of Thunderbird</da>
   <de>Armenische Lokalisation von Thunderbird</de>
   <el>Αρμενικά για Thunderbird</el>
   <en>Armenian localisation of Thunderbird</en>
   <es_ES>Localización Armenia de Thunderbird</es_ES>
   <es>Localización Armenio de Thunderbird</es>
   <et>Armenian localisation of Thunderbird</et>
   <eu>Armenian localisation of Thunderbird</eu>
   <fa>Armenian localisation of Thunderbird</fa>
   <fil_PH>Armenian localisation of Thunderbird</fil_PH>
   <fi>Armenian localisation of Thunderbird</fi>
   <fr_BE>Localisation en arménien pour Thunderbird</fr_BE>
   <fr>Localisation en arménien pour Thunderbird</fr>
   <gl_ES>Armenian localisation of Thunderbird</gl_ES>
   <gu>Armenian localisation of Thunderbird</gu>
   <he_IL>Armenian localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का अर्मेनियाई संस्करण</hi>
   <hr>Armenian localisation of Thunderbird</hr>
   <hu>Armenian localisation of Thunderbird</hu>
   <id>Armenian localisation of Thunderbird</id>
   <is>Armenian localisation of Thunderbird</is>
   <it>Localizzazione armena di Thunderbird</it>
   <ja>アルメニア語版 Thunderbird</ja>
   <kk>Armenian localisation of Thunderbird</kk>
   <ko>Armenian localisation of Thunderbird</ko>
   <ku>Armenian localisation of Thunderbird</ku>
   <lt>Armenian localisation of Thunderbird</lt>
   <mk>Armenian localisation of Thunderbird</mk>
   <mr>Armenian localisation of Thunderbird</mr>
   <nb_NO>Armenian localisation of Thunderbird</nb_NO>
   <nb>Armensk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Armenian localisation of Thunderbird</nl_BE>
   <nl>Armeense lokalisatie van Thunderbird</nl>
   <or>Armenian localisation of Thunderbird</or>
   <pl>Armenian localisation of Thunderbird</pl>
   <pt_BR>Armênia Localização para o Thunderbird</pt_BR>
   <pt>Arménio Localização para Thunderbird</pt>
   <ro>Armenian localisation of Thunderbird</ro>
   <ru>Armenian localisation of Thunderbird</ru>
   <sk>Armenian localisation of Thunderbird</sk>
   <sl>Armenske krajevne nastavitve za Thunderbird</sl>
   <so>Armenian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në armenisht</sq>
   <sr>Armenian localisation of Thunderbird</sr>
   <sv>Armenisk lokalisering av Thunderbird</sv>
   <th>Armenian localisation of Thunderbird</th>
   <tr>Thunderbird Ermenice yerelleştirmesi</tr>
   <uk>Armenian localisation of Thunderbird</uk>
   <vi>Armenian localisation of Thunderbird</vi>
   <zh_CN>Armenian localisation of Thunderbird</zh_CN>
   <zh_HK>Armenian localisation of Thunderbird</zh_HK>
   <zh_TW>Armenian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-hy-am
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-hy-am
</uninstall_package_names>

</app>
