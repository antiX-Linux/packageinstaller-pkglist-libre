<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Misc
</category>

<name>
Java
</name>

<description>
   <am>Java 17- installs openjdk-17</am>
   <ar>Java 17- installs openjdk-17</ar>
   <be>Java 17- installs openjdk-17</be>
   <bg>Java 17- installs openjdk-17</bg>
   <bn>Java 17- installs openjdk-17</bn>
   <ca>Java 17- instal·la openjdk-17</ca>
   <cs>Java 17- installs openjdk-17</cs>
   <da>Java 17- installs openjdk-17</da>
   <de>Java 17 (installiert “openjdk-17”)</de>
   <el>Java 17- εγκατάσταση του openjdk-17</el>
   <en>Java 17- installs openjdk-17</en>
   <es_ES>Java 17- instala openjdk-17</es_ES>
   <es>Java 17- instala openjdk-17</es>
   <et>Java 17- installs openjdk-17</et>
   <eu>Java 17- installs openjdk-17</eu>
   <fa>Java 17- installs openjdk-17</fa>
   <fil_PH>Java 17- installs openjdk-17</fil_PH>
   <fi>Java 17- asentaa openjdk-17</fi>
   <fr_BE>Java 17- installs openjdk-17</fr_BE>
   <fr>Java 17- installs openjdk-17</fr>
   <gl_ES>Java 17- installs openjdk-17</gl_ES>
   <gu>Java 17- installs openjdk-17</gu>
   <he_IL>Java 17- installs openjdk-17</he_IL>
   <hi>जावा 17- openjdk-17 इंस्टॉल होगा</hi>
   <hr>Java 17- installs openjdk-17</hr>
   <hu>Java 17- installs openjdk-17</hu>
   <id>Java 17- installs openjdk-17</id>
   <is>Java 17- installs openjdk-17</is>
   <it>Java 17- installa openjdk-17</it>
   <ja>Java 17- は openjdk-17 をインストールします</ja>
   <kk>Java 17- installs openjdk-17</kk>
   <ko>Java 17- installs openjdk-17</ko>
   <ku>Java 17- installs openjdk-17</ku>
   <lt>Java 17- installs openjdk-17</lt>
   <mk>Java 17- installs openjdk-17</mk>
   <mr>Java 17- installs openjdk-17</mr>
   <nb_NO>Java 17- installs openjdk-17</nb_NO>
   <nb>Java 17 – installerer openjdk-17</nb>
   <nl_BE>Java 17- installs openjdk-17</nl_BE>
   <nl>Java 17- installs openjdk-17</nl>
   <or>Java 17- installs openjdk-17</or>
   <pl>Java 17- installs openjdk-17</pl>
   <pt_BR>Java 17 - instalação do openjdk-17</pt_BR>
   <pt>Java 17- instala o openjdk-17</pt>
   <ro>Java 17- installs openjdk-17</ro>
   <ru>Java 17- installs openjdk-17</ru>
   <sk>Java 17- installs openjdk-17</sk>
   <sl>Java 17- namesti openjdk-17</sl>
   <so>Java 17- installs openjdk-17</so>
   <sq>Java 17- instalon openjdk-17</sq>
   <sr>Java 17- installs openjdk-17</sr>
   <sv>Java 17- installerar openjdk-17</sv>
   <th>Java 17- installs openjdk-17</th>
   <tr>Java 17- openjdk-17 yükler</tr>
   <uk>Java 17- installs openjdk-17</uk>
   <vi>Java 17- installs openjdk-17</vi>
   <zh_CN>Java 17- installs openjdk-17</zh_CN>
   <zh_HK>Java 17- installs openjdk-17</zh_HK>
   <zh_TW>Java 17- installs openjdk-17</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
openjdk-17-jre
openjdk-17-jre-headless
default-jre
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
openjdk-17-jre
openjdk-17-jre-headless
default-jre
</uninstall_package_names>
</app>
