<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Kernels
</category>

<name>
Kernel-Debian_32bit
</name>

<description>
   <am>Fallback Debian 6.1 32bit linux kernel</am>
   <ar>Fallback Debian 6.1 32bit linux kernel</ar>
   <be>Fallback Debian 6.1 32bit linux kernel</be>
   <bg>Fallback Debian 6.1 32bit linux kernel</bg>
   <bn>Fallback Debian 6.1 32bit linux kernel</bn>
   <ca>Kernel Linux alternatiu Debian 6.1 32bit</ca>
   <cs>Fallback Debian 6.1 32bit linux kernel</cs>
   <da>Fallback Debian 6.1 32bit linux kernel</da>
   <de>Ausweich-Kernel: Debian-Linux 6.1 32bit</de>
   <el>Debian 6.1 32bit linux kernel</el>
   <en>Fallback Debian 6.1 32bit linux kernel</en>
   <es_ES>Kernel linux 6.1 32 bits de Debian (respaldo)</es_ES>
   <es>Kernel/nucleo linux 6.1 32 bits de Debian (respaldo)</es>
   <et>Fallback Debian 6.1 32bit linux kernel</et>
   <eu>Fallback Debian 6.1 32bit linux kernel</eu>
   <fa>Fallback Debian 6.1 32bit linux kernel</fa>
   <fil_PH>Fallback Debian 6.1 32bit linux kernel</fil_PH>
   <fi>Fallback Debian 6.1 32bit linux kernel</fi>
   <fr_BE>Noyau linux Fallback Debian 6.1 32 bits</fr_BE>
   <fr>Noyau linux Fallback Debian 6.1 32 bits</fr>
   <gl_ES>Fallback Debian 6.1 32bit linux kernel</gl_ES>
   <gu>Fallback Debian 6.1 32bit linux kernel</gu>
   <he_IL>Fallback Debian 6.1 32bit linux kernel</he_IL>
   <hi>बैकअप डेबियन 6.1 32 बिट लिनक्स कर्नेल</hi>
   <hr>Fallback Debian 6.1 32bit linux kernel</hr>
   <hu>Fallback Debian 6.1 32bit linux kernel</hu>
   <id>Fallback Debian 6.1 32bit linux kernel</id>
   <is>Fallback Debian 6.1 32bit linux kernel</is>
   <it>Kernel Fallback Debian 6.1 32bit</it>
   <ja>Debian 6.1 32bit linux kernel フォールバック</ja>
   <kk>Fallback Debian 6.1 32bit linux kernel</kk>
   <ko>Fallback Debian 6.1 32bit linux kernel</ko>
   <ku>Fallback Debian 6.1 32bit linux kernel</ku>
   <lt>Fallback Debian 6.1 32bit linux kernel</lt>
   <mk>Fallback Debian 6.1 32bit linux kernel</mk>
   <mr>Fallback Debian 6.1 32bit linux kernel</mr>
   <nb_NO>Fallback Debian 6.1 32bit linux kernel</nb_NO>
   <nb>Siste utvei – Debian 6.1 32-biters Linux-kjerne</nb>
   <nl_BE>Fallback Debian 6.1 32bit linux kernel</nl_BE>
   <nl>Fallback Debian 6.1 32bit linux kernel</nl>
   <or>Fallback Debian 6.1 32bit linux kernel</or>
   <pl>Fallback Debian 6.1 32bit linux kernel</pl>
   <pt_BR>Fallback do Debian 6.1 de 32 bits linux kernel</pt_BR>
   <pt>Núcleo (Kernel) linux Debian 6.1 32bit de reserva</pt>
   <ro>Fallback Debian 6.1 32bit linux kernel</ro>
   <ru>Fallback Debian 6.1 32bit linux kernel</ru>
   <sk>Fallback Debian 6.1 32bit linux kernel</sk>
   <sl>Starejše Debian 6.1 32bit linux jedro</sl>
   <so>Fallback Debian 6.1 32bit linux kernel</so>
   <sq>Fallback Debian 6.1 32bit linux kernel</sq>
   <sr>Fallback Debian 6.1 32bit linux kernel</sr>
   <sv>Fallback Debian 6.1 32bit linux kernel</sv>
   <th>Fallback Debian 6.1 32bit linux kernel</th>
   <tr>İkincil Debian 6.1 32bit linux çekirdeği</tr>
   <uk>Fallback Debian 6.1 32bit linux kernel</uk>
   <vi>Fallback Debian 6.1 32bit linux kernel</vi>
   <zh_CN>Fallback Debian 6.1 32bit linux kernel</zh_CN>
   <zh_HK>Fallback Debian 6.1 32bit linux kernel</zh_HK>
   <zh_TW>Fallback Debian 6.1 32bit linux kernel</zh_TW>
</description>

<installable>
32
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
linux-image-6.1.0-12-686
linux-headers-6.1.0-12-686
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-6.1.0-12-686
linux-headers-6.1.0-12-686
</uninstall_package_names>

</app>
