<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Ukrainian_Firefox
</name>

<description>
   <am>Ukrainian localisation of Firefox</am>
   <ar>Ukrainian localisation of Firefox</ar>
   <be>Ukrainian localisation of Firefox</be>
   <bg>Ukrainian localisation of Firefox</bg>
   <bn>Ukrainian localisation of Firefox</bn>
   <ca>Localització de Firefox en Ucrainès</ca>
   <cs>Ukrainian localisation of Firefox</cs>
   <da>Ukrainsk oversættelse af Firefox</da>
   <de>Ukrainische Lokalisierung von Firefox</de>
   <el>Ουκρανικά για το Firefox</el>
   <en>Ukrainian localisation of Firefox</en>
   <es_ES>Localización Ucraniana de Firefox</es_ES>
   <es>Localización Ucraniano de Firefox</es>
   <et>Ukrainian localisation of Firefox</et>
   <eu>Ukrainian localisation of Firefox</eu>
   <fa>Ukrainian localisation of Firefox</fa>
   <fil_PH>Ukrainian localisation of Firefox</fil_PH>
   <fi>Ukrainalainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en ukrainien pour Firefox</fr_BE>
   <fr>Localisation en ukrainien pour Firefox</fr>
   <gl_ES>Localización de Firefox ao ucranian</gl_ES>
   <gu>Ukrainian localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לאוקראינית</he_IL>
   <hi>फायरफॉक्स का यूक्रेनी संस्करण</hi>
   <hr>Ukrainian localisation of Firefox</hr>
   <hu>Ukrainian localisation of Firefox</hu>
   <id>Ukrainian localisation of Firefox</id>
   <is>Ukrainian localisation of Firefox</is>
   <it>Localizzazione ucraina di Firefox</it>
   <ja>Firefox のウクライナ語版</ja>
   <kk>Ukrainian localisation of Firefox</kk>
   <ko>Ukrainian localisation of Firefox</ko>
   <ku>Ukrainian localisation of Firefox</ku>
   <lt>Ukrainian localisation of Firefox</lt>
   <mk>Ukrainian localisation of Firefox</mk>
   <mr>Ukrainian localisation of Firefox</mr>
   <nb_NO>Ukrainian localisation of Firefox</nb_NO>
   <nb>Ukrainsk lokaltilpassing av Firefox</nb>
   <nl_BE>Ukrainian localisation of Firefox</nl_BE>
   <nl>Oekraïense lokalisatie van Firefox</nl>
   <or>Ukrainian localisation of Firefox</or>
   <pl>Ukraińska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Ucraniano Localização para o Firefox</pt_BR>
   <pt>Ucraniano Localização para Firefox</pt>
   <ro>Ukrainian localisation of Firefox</ro>
   <ru>Украинская локализация Firefox</ru>
   <sk>Ukrainian localisation of Firefox</sk>
   <sl>Ukrajinske krajevne nastavitve za Firefox</sl>
   <so>Ukrainian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në ukrainisht</sq>
   <sr>Ukrainian localisation of Firefox</sr>
   <sv>Ukrainsk lokalisering av Firefox</sv>
   <th>Ukrainian localisation ของ Firefox</th>
   <tr>Firefox'un Ukraynaca yerelleştirmesi</tr>
   <uk>Ukrainian localisation of Firefox</uk>
   <vi>Ukrainian localisation of Firefox</vi>
   <zh_CN>Ukrainian localisation of Firefox</zh_CN>
   <zh_HK>Ukrainian localisation of Firefox</zh_HK>
   <zh_TW>Ukrainian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-uk
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-uk
</uninstall_package_names>
</app>
