<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
Gnome
</name>

<description>
   <am>Minimal install of gnome shell</am>
   <ar>Minimal install of gnome shell</ar>
   <be>Minimal install of gnome shell</be>
   <bg>Minimal install of gnome shell</bg>
   <bn>Minimal install of gnome shell</bn>
   <ca>Instal·lació mínima de Gnome Shell</ca>
   <cs>Minimal install of gnome shell</cs>
   <da>Minimal installation af gnome shell</da>
   <de>Minimale Installation der Gnome-Shell</de>
   <el>Ελάχιστη εγκατάσταση του gnome shell</el>
   <en>Minimal install of gnome shell</en>
   <es_ES>Instalación mínima de gnome shell</es_ES>
   <es>Instalación mínima de gnome shell</es>
   <et>Minimal install of gnome shell</et>
   <eu>Minimal install of gnome shell</eu>
   <fa>Minimal install of gnome shell</fa>
   <fil_PH>Minimal install of gnome shell</fil_PH>
   <fi>Gnome-työpöydän vähimmäisasennus</fi>
   <fr_BE>Installation minimale de Gnome Shell</fr_BE>
   <fr>Installation minimale de Gnome Shell</fr>
   <gl_ES>Instalación mínima do escritorio gnome</gl_ES>
   <gu>Minimal install of gnome shell</gu>
   <he_IL>Minimal install of gnome shell</he_IL>
   <hi>गनोम शैल का संक्षिप्त इंस्टॉल</hi>
   <hr>Minimal install of gnome shell</hr>
   <hu>Minimal install of gnome shell</hu>
   <id>Minimal install of gnome shell</id>
   <is>Minimal install of gnome shell</is>
   <it>Installazione minimale di Gnome Shell</it>
   <ja>GNOMEシェルの最小インストール</ja>
   <kk>Minimal install of gnome shell</kk>
   <ko>Minimal install of gnome shell</ko>
   <ku>Minimal install of gnome shell</ku>
   <lt>Minimal install of gnome shell</lt>
   <mk>Minimal install of gnome shell</mk>
   <mr>Minimal install of gnome shell</mr>
   <nb_NO>Minimal install of gnome shell</nb_NO>
   <nb>Minimal installasjon av gnome-skall</nb>
   <nl_BE>Minimal install of gnome shell</nl_BE>
   <nl>Minimale installatie van de gnome shell</nl>
   <or>Minimal install of gnome shell</or>
   <pl>minimalna instalalacja powłoki gnome</pl>
   <pt_BR>Instalação mínima do ambiente de trabalho gnome</pt_BR>
   <pt>Instalação mínima do ambiente de trabalho gnome</pt>
   <ro>Minimal install of gnome shell</ro>
   <ru>Минимальная установка Gnome Shell</ru>
   <sk>Minimal install of gnome shell</sk>
   <sl>Minimalna namestitev gnome ogrodja</sl>
   <so>Minimal install of gnome shell</so>
   <sq>Instalim minimal i shellit gnome</sq>
   <sr>Minimal install of gnome shell</sr>
   <sv>Minimal installation av gnome shell</sv>
   <th>Minimal install of gnome shell</th>
   <tr>Gnome kabuğunun en küçük kurulumu</tr>
   <uk>мінімальний набір середовища gnome</uk>
   <vi>Minimal install of gnome shell</vi>
   <zh_CN>最小化安装 gnome shell</zh_CN>
   <zh_HK>Minimal install of gnome shell</zh_HK>
   <zh_TW>Minimal install of gnome shell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
gnome
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
gnome
</uninstall_package_names>

</app>
