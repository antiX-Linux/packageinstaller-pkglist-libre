<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Catalan_LO_Latest_main
</name>

<description>
   <am>Catalan LibreOffice Language Meta-Package</am>
   <ar>Catalan LibreOffice Language Meta-Package</ar>
   <be>Catalan LibreOffice Language Meta-Package</be>
   <bg>Catalan LibreOffice Language Meta-Package</bg>
   <bn>Catalan LibreOffice Language Meta-Package</bn>
   <ca>Meta-paquet de llengua Catalana per LibreOffice</ca>
   <cs>Catalan LibreOffice Language Meta-Package</cs>
   <da>Catalansk LibreOffice sprog-metapakke</da>
   <de>Katalanisches LibreOffice Meta-Paket</de>
   <el>LibreOffice στα Καταλανικά</el>
   <en>Catalan LibreOffice Language Meta-Package</en>
   <es_ES>Meta paquete catalán de Libre Office</es_ES>
   <es>Metapaquete Catalán de Libre Office</es>
   <et>Catalan LibreOffice Language Meta-Package</et>
   <eu>Catalan LibreOffice Language Meta-Package</eu>
   <fa>Catalan LibreOffice Language Meta-Package</fa>
   <fil_PH>Catalan LibreOffice Language Meta-Package</fil_PH>
   <fi>Katalonialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Localisation en catalan pour Meta-Package</fr_BE>
   <fr>Localisation en catalan pour Meta-Package</fr>
   <gl_ES>Catalán Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Catalan LibreOffice Language Meta-Package</gu>
   <he_IL>Catalan LibreOffice Language Meta-Package</he_IL>
   <hi>लिब्रे-ऑफिस हेतु कैटलन भाषा मेटा-पैकेज</hi>
   <hr>Catalan LibreOffice Language Meta-Package</hr>
   <hu>Catalan LibreOffice Language Meta-Package</hu>
   <id>Catalan LibreOffice Language Meta-Package</id>
   <is>Catalan LibreOffice Language Meta-Package</is>
   <it>Meta-pacchetto della lingua catalana per LibreOffice</it>
   <ja>カタロニア語用 LibreOffice 言語メタパッケージ</ja>
   <kk>Catalan LibreOffice Language Meta-Package</kk>
   <ko>Catalan LibreOffice Language Meta-Package</ko>
   <ku>Catalan LibreOffice Language Meta-Package</ku>
   <lt>Catalan LibreOffice Language Meta-Package</lt>
   <mk>Catalan LibreOffice Language Meta-Package</mk>
   <mr>Catalan LibreOffice Language Meta-Package</mr>
   <nb_NO>Catalan LibreOffice Language Meta-Package</nb_NO>
   <nb>Katalansk språkpakke for LibreOffice</nb>
   <nl_BE>Catalan LibreOffice Language Meta-Package</nl_BE>
   <nl>Catalaanse LibreOffice Taal Meta-Pakket</nl>
   <or>Catalan LibreOffice Language Meta-Package</or>
   <pl>Kataloński metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Catalão Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Catalão Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Catalan LibreOffice Language Meta-Package</ro>
   <ru>Catalan LibreOffice Language Meta-Package</ru>
   <sk>Catalan LibreOffice Language Meta-Package</sk>
   <sl>Katalonski jezikovni metapaket za LibreOffice</sl>
   <so>Catalan LibreOffice Language Meta-Package</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në katalanisht</sq>
   <sr>Catalan LibreOffice Language Meta-Package</sr>
   <sv>Katalansk LibreOffice Språk-Meta-Paket</sv>
   <th>Meta-Package ภาษา Catalan สำหรับ LibreOffice</th>
   <tr>LibreOffice için Katalanca Dili Üst-Paketi</tr>
   <uk>мета-пакунок з мовою Catalan для LibreOffice</uk>
   <vi>Catalan LibreOffice Language Meta-Package</vi>
   <zh_CN>LibreOffice 加泰罗尼亚语语言包</zh_CN>
   <zh_HK>Catalan LibreOffice Language Meta-Package</zh_HK>
   <zh_TW>Catalan LibreOffice Language Meta-Package</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ca
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ca
libreoffice-gtk3
</uninstall_package_names>

</app>
