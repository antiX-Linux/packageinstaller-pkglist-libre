<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Swedish_Default_Firefox_esr
</name>

<description>
   <am>Swedish localisation of Firefox ESR</am>
   <ar>Swedish localisation of Firefox ESR</ar>
   <be>Swedish localisation of Firefox ESR</be>
   <bg>Swedish localisation of Firefox ESR</bg>
   <bn>Swedish localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Suec</ca>
   <cs>Swedish localisation of Firefox ESR</cs>
   <da>Swedish localisation of Firefox ESR</da>
   <de>Schwedische Lokalisation von “Firefox ESR”</de>
   <el>Σουηδικά για Firefox ESR</el>
   <en>Swedish localisation of Firefox ESR</en>
   <es_ES>Localización Sueco de Firefox ESR</es_ES>
   <es>Localización Sueco de Firefox ESR</es>
   <et>Swedish localisation of Firefox ESR</et>
   <eu>Swedish localisation of Firefox ESR</eu>
   <fa>Swedish localisation of Firefox ESR</fa>
   <fil_PH>Swedish localisation of Firefox ESR</fil_PH>
   <fi>Swedish localisation of Firefox ESR</fi>
   <fr_BE>Localisation en suédois pour Firefox ESR</fr_BE>
   <fr>Localisation en suédois pour Firefox ESR</fr>
   <gl_ES>Swedish localisation of Firefox ESR</gl_ES>
   <gu>Swedish localisation of Firefox ESR</gu>
   <he_IL>Swedish localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का स्वीडिश संस्करण</hi>
   <hr>Swedish localisation of Firefox ESR</hr>
   <hu>Swedish localisation of Firefox ESR</hu>
   <id>Swedish localisation of Firefox ESR</id>
   <is>Swedish localisation of Firefox ESR</is>
   <it>Localizzazione svedese di Firefox ESR</it>
   <ja>スウェーデン語版 Firefox ESR</ja>
   <kk>Swedish localisation of Firefox ESR</kk>
   <ko>Swedish localisation of Firefox ESR</ko>
   <ku>Swedish localisation of Firefox ESR</ku>
   <lt>Swedish localisation of Firefox ESR</lt>
   <mk>Swedish localisation of Firefox ESR</mk>
   <mr>Swedish localisation of Firefox ESR</mr>
   <nb_NO>Swedish localisation of Firefox ESR</nb_NO>
   <nb>Svensk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Swedish localisation of Firefox ESR</nl_BE>
   <nl>Swedish localisation of Firefox ESR</nl>
   <or>Swedish localisation of Firefox ESR</or>
   <pl>Swedish localisation of Firefox ESR</pl>
   <pt_BR>Sueco Localização para o Firefox ESR</pt_BR>
   <pt>Sueco Localização para Firefox ESR</pt>
   <ro>Swedish localisation of Firefox ESR</ro>
   <ru>Swedish localisation of Firefox ESR</ru>
   <sk>Swedish localisation of Firefox ESR</sk>
   <sl>Švedske krajevne nastavitve za Firefox ESR</sl>
   <so>Swedish localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në suedisht</sq>
   <sr>Swedish localisation of Firefox ESR</sr>
   <sv>Svensk lokalisering av Firefox ESR</sv>
   <th>Swedish localisation of Firefox ESR</th>
   <tr>Firefox ESR İsveççe yerelleştirmesi</tr>
   <uk>Swedish localisation of Firefox ESR</uk>
   <vi>Swedish localisation of Firefox ESR</vi>
   <zh_CN>Swedish localisation of Firefox ESR</zh_CN>
   <zh_HK>Swedish localisation of Firefox ESR</zh_HK>
   <zh_TW>Swedish localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-sv-se
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-sv-se
</uninstall_package_names>

</app>
