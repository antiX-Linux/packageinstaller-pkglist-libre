<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Finnish_Thunderbird
</name>

<description>
   <am>Finnish localisation of Thunderbird</am>
   <ar>Finnish localisation of Thunderbird</ar>
   <be>Finnish localisation of Thunderbird</be>
   <bg>Finnish localisation of Thunderbird</bg>
   <bn>Finnish localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Finès</ca>
   <cs>Finnish localisation of Thunderbird</cs>
   <da>Finsk oversættelse af Thunderbird</da>
   <de>Finnische Lokalisierung von Thunderbird</de>
   <el>Φινλανδικά για το Thunderbird</el>
   <en>Finnish localisation of Thunderbird</en>
   <es_ES>Localización Finlandesa de Thunderbird</es_ES>
   <es>Localización Finés de Thunderbird</es>
   <et>Finnish localisation of Thunderbird</et>
   <eu>Finnish localisation of Thunderbird</eu>
   <fa>Finnish localisation of Thunderbird</fa>
   <fil_PH>Finnish localisation of Thunderbird</fil_PH>
   <fi>Thunderbirdin suomenkielinen käyttöliittymä</fi>
   <fr_BE>Localisation en finnois pour Thunderbird</fr_BE>
   <fr>Localisation en finnois pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao finés</gl_ES>
   <gu>Finnish localisation of Thunderbird</gu>
   <he_IL>Finnish localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का फिनिश संस्करण</hi>
   <hr>Finnish localisation of Thunderbird</hr>
   <hu>Finnish localisation of Thunderbird</hu>
   <id>Finnish localisation of Thunderbird</id>
   <is>Finnish localisation of Thunderbird</is>
   <it>Localizzazione finlandese di Thunderbird</it>
   <ja>Thunderbird のフィンランド語版</ja>
   <kk>Finnish localisation of Thunderbird</kk>
   <ko>Finnish localisation of Thunderbird</ko>
   <ku>Finnish localisation of Thunderbird</ku>
   <lt>Finnish localisation of Thunderbird</lt>
   <mk>Finnish localisation of Thunderbird</mk>
   <mr>Finnish localisation of Thunderbird</mr>
   <nb_NO>Finnish localisation of Thunderbird</nb_NO>
   <nb>Finsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Finnish localisation of Thunderbird</nl_BE>
   <nl>Finse lokalisatie van Thunderbird</nl>
   <or>Finnish localisation of Thunderbird</or>
   <pl>Fińska lokalizacja Thunderbirda</pl>
   <pt_BR>Finlandês Localização para o Thunderbird</pt_BR>
   <pt>Finlandês Localização para Thunderbird</pt>
   <ro>Finnish localisation of Thunderbird</ro>
   <ru>Finnish localisation of Thunderbird</ru>
   <sk>Finnish localisation of Thunderbird</sk>
   <sl>Finske krajevne nastavitve za Thunderbird</sl>
   <so>Finnish localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në finlandisht</sq>
   <sr>Finnish localisation of Thunderbird</sr>
   <sv>Finsk lokalisering av Thunderbird</sv>
   <th>Finnish localisation ของ Thunderbird</th>
   <tr>Thunderbird Fince yerelleştirmesi</tr>
   <uk>Finnish локалізація Thunderbird</uk>
   <vi>Finnish localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 芬兰语语言包</zh_CN>
   <zh_HK>Finnish localisation of Thunderbird</zh_HK>
   <zh_TW>Finnish localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-fi
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-fi
</uninstall_package_names>

</app>
