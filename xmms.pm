<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Audio
</category>

<name>
XMMS
</name>

<description>
   <am>multimedia player modelled on winamp</am>
   <ar>multimedia player modelled on winamp</ar>
   <be>multimedia player modelled on winamp</be>
   <bg>multimedia player modelled on winamp</bg>
   <bn>multimedia player modelled on winamp</bn>
   <ca>Reproductor multimèdia modelat en winamp</ca>
   <cs>multimedia player modelled on winamp</cs>
   <da>multimedieafspiller som er udformet efter winamp</da>
   <de>Multimedia-Player nach dem Vorbild von Winamp</de>
   <el>Αναπαραγωγή πολυμέσων με βάση το winamp</el>
   <en>multimedia player modelled on winamp</en>
   <es_ES>Reproductor multimedia estilo Winamp</es_ES>
   <es>Reproductor multimedia estilo Winamp</es>
   <et>multimedia player modelled on winamp</et>
   <eu>multimedia player modelled on winamp</eu>
   <fa>multimedia player modelled on winamp</fa>
   <fil_PH>multimedia player modelled on winamp</fil_PH>
   <fi>multimedian toistamiseen, mallinnettu Winamp:in mukaan</fi>
   <fr_BE>Lecteur multimédia inspiré de winamp</fr_BE>
   <fr>Lecteur multimédia inspiré de winamp</fr>
   <gl_ES>reprodutor de multimedia semellante ao winamp</gl_ES>
   <gu>multimedia player modelled on winamp</gu>
   <he_IL>multimedia player modelled on winamp</he_IL>
   <hi>winamp आधारित मल्टीमीडिया प्लेयर</hi>
   <hr>multimedia player modelled on winamp</hr>
   <hu>multimedia player modelled on winamp</hu>
   <id>multimedia player modelled on winamp</id>
   <is>multimedia player modelled on winamp</is>
   <it>riproduttore multimediale simile a winamp</it>
   <ja>winamp を真似たマルチメディアプレーヤー</ja>
   <kk>multimedia player modelled on winamp</kk>
   <ko>multimedia player modelled on winamp</ko>
   <ku>multimedia player modelled on winamp</ku>
   <lt>multimedia player modelled on winamp</lt>
   <mk>multimedia player modelled on winamp</mk>
   <mr>multimedia player modelled on winamp</mr>
   <nb_NO>multimedia player modelled on winamp</nb_NO>
   <nb>multimedia-avspiller som ligner winamp</nb>
   <nl_BE>multimedia player modelled on winamp</nl_BE>
   <nl>multimediaspeler gemodelleerd naar winamp</nl>
   <or>multimedia player modelled on winamp</or>
   <pl>odtwarzacz multimedialny wzorowany na Winampie</pl>
   <pt_BR>Reprodutor de multimídia semelhante ao winamp</pt_BR>
   <pt>Reprodutor de multimédia semelhante ao winamp</pt>
   <ro>multimedia player modelled on winamp</ro>
   <ru>Универсальный аудиоплеер наподобие WinAmp</ru>
   <sk>multimedia player modelled on winamp</sk>
   <sl>Večpredstavnostni predvajalnik, ki temelji na Winampu</sl>
   <so>multimedia player modelled on winamp</so>
   <sq>Lojtës multimedia i modeluar sipas winamp-it</sq>
   <sr>multimedia player modelled on winamp</sr>
   <sv>multimediaspelare formad efter winamp</sv>
   <th>multimedia player modelled on winamp</th>
   <tr>winamp'dan örnek alınmış çoklu ortam oynatıcısı</tr>
   <uk>мультимедія програвач схожий на winamp</uk>
   <vi>multimedia player modelled on winamp</vi>
   <zh_CN>multimedia player modelled on winamp</zh_CN>
   <zh_HK>multimedia player modelled on winamp</zh_HK>
   <zh_TW>multimedia player modelled on winamp</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
xmms
xmms-skins-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
xmms
xmms-skins-antix
</uninstall_package_names>
</app>
