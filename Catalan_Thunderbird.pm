<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Catalan_Thunderbird
</name>

<description>
   <am>Catalan localisation of Thunderbird</am>
   <ar>Catalan localisation of Thunderbird</ar>
   <be>Catalan localisation of Thunderbird</be>
   <bg>Catalan localisation of Thunderbird</bg>
   <bn>Catalan localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Català</ca>
   <cs>Catalan localisation of Thunderbird</cs>
   <da>Catalansk oversættelse af Thunderbird</da>
   <de>Katalanische Lokalisierung von Thunderbird</de>
   <el>Καταλανική για το Thunderbird</el>
   <en>Catalan localisation of Thunderbird</en>
   <es_ES>Localización catalana de Thunderbird</es_ES>
   <es>Localización Catalán de Thunderbird</es>
   <et>Catalan localisation of Thunderbird</et>
   <eu>Catalan localisation of Thunderbird</eu>
   <fa>Catalan localisation of Thunderbird</fa>
   <fil_PH>Catalan localisation of Thunderbird</fil_PH>
   <fi>Katalonialainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en catalan pour Thunderbird</fr_BE>
   <fr>Localisation en catalan pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird para o catalán</gl_ES>
   <gu>Catalan localisation of Thunderbird</gu>
   <he_IL>Catalan localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का कैटलन संस्करण</hi>
   <hr>Catalan localisation of Thunderbird</hr>
   <hu>Catalan localisation of Thunderbird</hu>
   <id>Catalan localisation of Thunderbird</id>
   <is>Catalan localisation of Thunderbird</is>
   <it>Localizzazione catalana di Thunderbird</it>
   <ja>Thunderbird のカタロニア語版</ja>
   <kk>Catalan localisation of Thunderbird</kk>
   <ko>Catalan localisation of Thunderbird</ko>
   <ku>Catalan localisation of Thunderbird</ku>
   <lt>Catalan localisation of Thunderbird</lt>
   <mk>Catalan localisation of Thunderbird</mk>
   <mr>Catalan localisation of Thunderbird</mr>
   <nb_NO>Catalan localisation of Thunderbird</nb_NO>
   <nb>Katalansk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Catalan localisation of Thunderbird</nl_BE>
   <nl>Catalaanse lokalisatie van Thunderbird</nl>
   <or>Catalan localisation of Thunderbird</or>
   <pl>Katalońska lokalizacja Thunderbirda</pl>
   <pt_BR>Catalão Localização para o Thunderbird</pt_BR>
   <pt>Catalão Localização para Thunderbird</pt>
   <ro>Catalan localisation of Thunderbird</ro>
   <ru>Catalan localisation of Thunderbird</ru>
   <sk>Catalan localisation of Thunderbird</sk>
   <sl>Katalonske krajevne nastavitve za Thunderbird</sl>
   <so>Catalan localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në katalanisht</sq>
   <sr>Catalan localisation of Thunderbird</sr>
   <sv>Katalansk lokalisering av Thunderbird</sv>
   <th>Catalan localisation ของ Thunderbird</th>
   <tr>Thunderbird Katalanca yerelleştirmesi</tr>
   <uk>Catalan локалізація Thunderbird</uk>
   <vi>Catalan localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 加泰罗尼亚语语言包</zh_CN>
   <zh_HK>Catalan localisation of Thunderbird</zh_HK>
   <zh_TW>Catalan localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-ca
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-ca
</uninstall_package_names>

</app>
