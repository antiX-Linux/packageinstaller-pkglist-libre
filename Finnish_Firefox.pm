<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Finnish_Firefox
</name>

<description>
   <am>Finnish localisation of Firefox</am>
   <ar>Finnish localisation of Firefox</ar>
   <be>Finnish localisation of Firefox</be>
   <bg>Finnish localisation of Firefox</bg>
   <bn>Finnish localisation of Firefox</bn>
   <ca>Localització de Firefox en Finès</ca>
   <cs>Finnish localisation of Firefox</cs>
   <da>Finsk oversættelse af Firefox</da>
   <de>Finnische Lokalisierung von Firefox</de>
   <el>Φινλανδικά για το Firefox</el>
   <en>Finnish localisation of Firefox</en>
   <es_ES>Localización Finlandesa de Firefox</es_ES>
   <es>Localización Finés de Firefox</es>
   <et>Finnish localisation of Firefox</et>
   <eu>Finnish localisation of Firefox</eu>
   <fa>Finnish localisation of Firefox</fa>
   <fil_PH>Finnish localisation of Firefox</fil_PH>
   <fi>Suomalainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en finnois pour Firefox</fr_BE>
   <fr>Localisation en finnois pour Firefox</fr>
   <gl_ES>Localización do Firefox ao finés</gl_ES>
   <gu>Finnish localisation of Firefox</gu>
   <he_IL>Finnish localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का फिनिश संस्करण</hi>
   <hr>Finnish localisation of Firefox</hr>
   <hu>Finnish localisation of Firefox</hu>
   <id>Finnish localisation of Firefox</id>
   <is>Finnish localisation of Firefox</is>
   <it>Localizzazione finlandese di Firefox</it>
   <ja>Firefox のフィンランド語版</ja>
   <kk>Finnish localisation of Firefox</kk>
   <ko>Finnish localisation of Firefox</ko>
   <ku>Finnish localisation of Firefox</ku>
   <lt>Finnish localisation of Firefox</lt>
   <mk>Finnish localisation of Firefox</mk>
   <mr>Finnish localisation of Firefox</mr>
   <nb_NO>Finnish localisation of Firefox</nb_NO>
   <nb>Finsk lokaltilpassing av Firefox</nb>
   <nl_BE>Finnish localisation of Firefox</nl_BE>
   <nl>Finse lokalisatie van Firefox</nl>
   <or>Finnish localisation of Firefox</or>
   <pl>Fińska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Finlandês Localização para o Firefox</pt_BR>
   <pt>Finlandês Localização para Firefox</pt>
   <ro>Finnish localisation of Firefox</ro>
   <ru>Финская локализация Firefox</ru>
   <sk>Finnish localisation of Firefox</sk>
   <sl>Finske krajevne nastavitve za Firefox</sl>
   <so>Finnish localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në finlandisht</sq>
   <sr>Finnish localisation of Firefox</sr>
   <sv>Finsk lokalisering av Firefox</sv>
   <th>Finnish localisation ของ Firefox</th>
   <tr>Firefox'un Fince yerelleştirmesi</tr>
   <uk>Finnish локалізація Firefox</uk>
   <vi>Finnish localisation of Firefox</vi>
   <zh_CN>Firefox 芬兰语语言包</zh_CN>
   <zh_HK>Finnish localisation of Firefox</zh_HK>
   <zh_TW>Finnish localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-fi
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-fi
</uninstall_package_names>
</app>
