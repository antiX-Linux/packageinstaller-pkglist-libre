<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Estonian_Default_Firefox_esr
</name>

<description>
   <am>Estonian localisation of Firefox ESR</am>
   <ar>Estonian localisation of Firefox ESR</ar>
   <be>Estonian localisation of Firefox ESR</be>
   <bg>Estonian localisation of Firefox ESR</bg>
   <bn>Estonian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Estonià</ca>
   <cs>Estonian localisation of Firefox ESR</cs>
   <da>Estonian localisation of Firefox ESR</da>
   <de>Estnische Lokalisation von “Firefox ESR”</de>
   <el>Εσθονικά για τον Firefox ESR</el>
   <en>Estonian localisation of Firefox ESR</en>
   <es_ES>Localización Estonia de Firefox ESR</es_ES>
   <es>Localización Estonio de Firefox ESR</es>
   <et>Estonian localisation of Firefox ESR</et>
   <eu>Estonian localisation of Firefox ESR</eu>
   <fa>Estonian localisation of Firefox ESR</fa>
   <fil_PH>Estonian localisation of Firefox ESR</fil_PH>
   <fi>Estonian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en Estonien pour Firefox ESR</fr_BE>
   <fr>Localisation en Estonien pour Firefox ESR</fr>
   <gl_ES>Estonian localisation of Firefox ESR</gl_ES>
   <gu>Estonian localisation of Firefox ESR</gu>
   <he_IL>Estonian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का एस्टोनियाई संस्करण</hi>
   <hr>Estonian localisation of Firefox ESR</hr>
   <hu>Estonian localisation of Firefox ESR</hu>
   <id>Estonian localisation of Firefox ESR</id>
   <is>Estonian localisation of Firefox ESR</is>
   <it>Localizzazione estone di Firefox ESR</it>
   <ja>エストニア語版 Firefox ESR</ja>
   <kk>Estonian localisation of Firefox ESR</kk>
   <ko>Estonian localisation of Firefox ESR</ko>
   <ku>Estonian localisation of Firefox ESR</ku>
   <lt>Estonian localisation of Firefox ESR</lt>
   <mk>Estonian localisation of Firefox ESR</mk>
   <mr>Estonian localisation of Firefox ESR</mr>
   <nb_NO>Estonian localisation of Firefox ESR</nb_NO>
   <nb>Estisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Estonian localisation of Firefox ESR</nl_BE>
   <nl>Estische lokalisatie van Firefox ESR</nl>
   <or>Estonian localisation of Firefox ESR</or>
   <pl>Estonian localisation of Firefox ESR</pl>
   <pt_BR>Estónio Localização para o Firefox ESR</pt_BR>
   <pt>Estónio Localização para Firefox ESR</pt>
   <ro>Estonian localisation of Firefox ESR</ro>
   <ru>Estonian localisation of Firefox ESR</ru>
   <sk>Estonian localisation of Firefox ESR</sk>
   <sl>Estonske krajevne nastavitve za Firefox ESR</sl>
   <so>Estonian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në estonisht</sq>
   <sr>Estonian localisation of Firefox ESR</sr>
   <sv>Estnisk lokalisering av Firefox ESR</sv>
   <th>Estonian localisation of Firefox ESR</th>
   <tr>Firefox ESR Estonya Dili yerelleştirmesi</tr>
   <uk>Estonian localisation of Firefox ESR</uk>
   <vi>Estonian localisation of Firefox ESR</vi>
   <zh_CN>Estonian localisation of Firefox ESR</zh_CN>
   <zh_HK>Estonian localisation of Firefox ESR</zh_HK>
   <zh_TW>Estonian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-et
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-et
</uninstall_package_names>

</app>
