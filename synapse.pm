<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Utility
</category>

<name>
Synapse
</name>

<description>
   <am>alternate semantic file launcher</am>
   <ar>alternate semantic file launcher</ar>
   <be>alternate semantic file launcher</be>
   <bg>alternate semantic file launcher</bg>
   <bn>alternate semantic file launcher</bn>
   <ca>Llançadora de fitxers amb semàntica alternativa</ca>
   <cs>alternate semantic file launcher</cs>
   <da>alternativ skematisk filstarter</da>
   <de>Alternativer semantischer Datei-Starter</de>
   <el>Εναλλακτική σημασιολογική εκκίνηση αρχείων</el>
   <en>alternate semantic file launcher</en>
   <es_ES>Lanzador de archivos semántico alternativo</es_ES>
   <es>Lanzador de archivos semántico</es>
   <et>alternate semantic file launcher</et>
   <eu>alternate semantic file launcher</eu>
   <fa>alternate semantic file launcher</fa>
   <fil_PH>alternate semantic file launcher</fil_PH>
   <fi>vaihtoehtoinen merkitysopillinen tiedostokäynnistin</fi>
   <fr_BE>Lanceur sémantique d'application</fr_BE>
   <fr>Lanceur sémantique d'application</fr>
   <gl_ES>Lanzador de ficheiros semántico</gl_ES>
   <gu>alternate semantic file launcher</gu>
   <he_IL>alternate semantic file launcher</he_IL>
   <hi>वैकल्पिक अर्थ-संबंधी फाइल आरंभ साधन</hi>
   <hr>alternate semantic file launcher</hr>
   <hu>alternate semantic file launcher</hu>
   <id>alternate semantic file launcher</id>
   <is>alternate semantic file launcher</is>
   <it>avviatore semantico per avviare applicazioni e accedere a file rilevanti</it>
   <ja>代替セマンティック ファイルランチャー</ja>
   <kk>alternate semantic file launcher</kk>
   <ko>alternate semantic file launcher</ko>
   <ku>alternate semantic file launcher</ku>
   <lt>alternate semantic file launcher</lt>
   <mk>alternate semantic file launcher</mk>
   <mr>alternate semantic file launcher</mr>
   <nb_NO>alternate semantic file launcher</nb_NO>
   <nb>semantisk filstarter</nb>
   <nl_BE>alternate semantic file launcher</nl_BE>
   <nl>alternatieve semantische bestandsopstarter</nl>
   <or>alternate semantic file launcher</or>
   <pl>alternatywny program uruchamiający pliki semantyczne</pl>
   <pt_BR>Pesquisador rápido de aplicativos, arquivos e informações</pt_BR>
   <pt>Lançador de ficheiros semântico</pt>
   <ro>alternate semantic file launcher</ro>
   <ru>Приложение для продвинутого поиска и запуска файлов</ru>
   <sk>alternate semantic file launcher</sk>
   <sl>alternativni semantični zaganjalec datotek</sl>
   <so>alternate semantic file launcher</so>
   <sq>alternate semantic file launcher</sq>
   <sr>alternate semantic file launcher</sr>
   <sv>alternativ semantisk filstartare</sv>
   <th>alternate semantic file launcher</th>
   <tr>başka bir anlamsal dosya başlatıcı</tr>
   <uk>alternate semantic file launcher</uk>
   <vi>alternate semantic file launcher</vi>
   <zh_CN>alternate semantic file launcher</zh_CN>
   <zh_HK>alternate semantic file launcher</zh_HK>
   <zh_TW>alternate semantic file launcher</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>https://screenshots.debian.net/screenshots/000/009/035/large.png</screenshot>

<preinstall>

</preinstall>

<install_package_names>
synapse
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
synapse
</uninstall_package_names>
</app>
