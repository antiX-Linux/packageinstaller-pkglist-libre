<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Austrian (German)
</name>

<description>
   <am>Austrian (German) dictionary for hunspell</am>
   <ar>Austrian (German) dictionary for hunspell</ar>
   <be>Austrian (German) dictionary for hunspell</be>
   <bg>Austrian (German) dictionary for hunspell</bg>
   <bn>Austrian (German) dictionary for hunspell</bn>
   <ca>Diccionari Alemany (Àustria) per hunspell</ca>
   <cs>Austrian (German) dictionary for hunspell</cs>
   <da>Austrian (German) dictionary for hunspell</da>
   <de>Österreichisches (deutschsprachiges) Wörterbuch für “Hunspell”</de>
   <el>Αυστριακά (Γερμανικά) λεξικό για hunspell</el>
   <en>Austrian (German) dictionary for hunspell</en>
   <es_ES>Diccionario Austriaco (Aleman) para hunspell</es_ES>
   <es>Diccionario Austríaco (Alemán) para hunspell</es>
   <et>Austrian (German) dictionary for hunspell</et>
   <eu>Austrian (German) dictionary for hunspell</eu>
   <fa>Austrian (German) dictionary for hunspell</fa>
   <fil_PH>Austrian (German) dictionary for hunspell</fil_PH>
   <fi>Austrian (German) dictionary for hunspell</fi>
   <fr_BE>Autrichien (Allemand) dictionnaire pour hunspell</fr_BE>
   <fr>Autrichien (Allemand) dictionnaire pour hunspell</fr>
   <gl_ES>Austrian (German) dictionary for hunspell</gl_ES>
   <gu>Austrian (German) dictionary for hunspell</gu>
   <he_IL>Austrian (German) dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु ऑस्ट्रियाई (जर्मन) शब्दकोष</hi>
   <hr>Austrian (German) dictionary for hunspell</hr>
   <hu>Austrian (German) dictionary for hunspell</hu>
   <id>Austrian (German) dictionary for hunspell</id>
   <is>Austrian (German) dictionary for hunspell</is>
   <it>Dizionario austriaco (tedesco) per hunspell</it>
   <ja>Hunspell 用オーストリア（ドイツ）語辞書</ja>
   <kk>Austrian (German) dictionary for hunspell</kk>
   <ko>Austrian (German) dictionary for hunspell</ko>
   <ku>Austrian (German) dictionary for hunspell</ku>
   <lt>Austrian (German) dictionary for hunspell</lt>
   <mk>Austrian (German) dictionary for hunspell</mk>
   <mr>Austrian (German) dictionary for hunspell</mr>
   <nb_NO>Austrian (German) dictionary for hunspell</nb_NO>
   <nb>Østerriksk (tysk) ordliste for hunspell</nb>
   <nl_BE>Austrian (German) dictionary for hunspell</nl_BE>
   <nl>Oostenrijks (Duits) woordenboek voor hunspell</nl>
   <or>Austrian (German) dictionary for hunspell</or>
   <pl>Austrian (German) dictionary for hunspell</pl>
   <pt_BR>Dicionário Austríaco (Alemão) para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Austríaco (Alemão) para hunspell</pt>
   <ro>Austrian (German) dictionary for hunspell</ro>
   <ru>Austrian (German) dictionary for hunspell</ru>
   <sk>Austrian (German) dictionary for hunspell</sk>
   <sl>Avstrijski (nemščina) slovar za hunspell</sl>
   <so>Austrian (German) dictionary for hunspell</so>
   <sq>Fjalor gjermanisht (Austri) për hunspell</sq>
   <sr>Austrian (German) dictionary for hunspell</sr>
   <sv>Österrikisk (Tysk) ordbok för hunspell</sv>
   <th>Austrian (German) dictionary for hunspell</th>
   <tr>Hunspell için Avusturyaca (Alman) sözlüğü</tr>
   <uk>Austrian (German) dictionary for hunspell</uk>
   <vi>Austrian (German) dictionary for hunspell</vi>
   <zh_CN>Austrian (German) dictionary for hunspell</zh_CN>
   <zh_HK>Austrian (German) dictionary for hunspell</zh_HK>
   <zh_TW>Austrian (German) dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-de-at
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-de-at
</uninstall_package_names>

</app>
