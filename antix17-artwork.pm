<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Wallpapers
</category>

<name>
antiX 17 Wallpapers
</name>

<description>
   <am>backgrounds originally supplied with antiX 17</am>
   <ar>backgrounds originally supplied with antiX 17</ar>
   <bg>backgrounds originally supplied with antiX 17</bg>
   <bn>backgrounds originally supplied with antiX 17</bn>
   <ca>Fons de pantalla subministrats amb antiX 17</ca>
   <cs>backgrounds originally supplied with antiX 17</cs>
   <da>baggrunde fra antiX 17</da>
   <de>Bildschirmhintergründe, die ursprünglich mit antiX 17 geliefert wurden</de>
   <el>επιφάνεια εργασίας που παρέχεται αρχικά με antiX 17</el>
   <en>backgrounds originally supplied with antiX 17</en>
   <es>Fondos de escritorio suministrados originalmente con antiX 17</es>
   <et>backgrounds originally supplied with antiX 17</et>
   <eu>backgrounds originally supplied with antiX 17</eu>
   <fa>backgrounds originally supplied with antiX 17</fa>
   <fil_PH>backgrounds originally supplied with antiX 17</fil_PH>
   <fi>backgrounds originally supplied with antiX 17</fi>
   <fr>Fonds d'écran initialement fournis avec antiX 17</fr>
   <he_IL>backgrounds originally supplied with antiX 17</he_IL>
   <hi>backgrounds originally supplied with antiX 17</hi>
   <hr>backgrounds originally supplied with antiX 17</hr>
   <hu>backgrounds originally supplied with antiX 17</hu>
   <id>backgrounds originally supplied with antiX 17</id>
   <is>backgrounds originally supplied with antiX 17</is>
   <it>sfondi originariamente forniti con antiX 17</it>
   <ja_JP>backgrounds originally supplied with antiX 17</ja_JP>
   <ja>backgrounds originally supplied with antiX 17</ja>
   <kk>backgrounds originally supplied with antiX 17</kk>
   <ko>backgrounds originally supplied with antiX 17</ko>
   <lt>backgrounds originally supplied with antiX 17</lt>
   <mk>backgrounds originally supplied with antiX 17</mk>
   <mr>backgrounds originally supplied with antiX 17</mr>
   <nb>backgrounds originally supplied with antiX 17</nb>
   <nl>achtergronden oorspronkelijk geleverd met antiX 17</nl>
   <pl>tła oryginalnie dostarczone z antiX 17</pl>
   <pt_BR>Imagens de fundo originalmente incluídas no antiX 17</pt_BR>
   <pt>Imagens de fundo originalmente incluídas no antiX 17</pt>
   <ro>backgrounds originally supplied with antiX 17</ro>
   <ru>Обои первоначально входившие в antiX 17</ru>
   <sk>backgrounds originally supplied with antiX 17</sk>
   <sl>Izvirna ozadja za antiX 17</sl>
   <sq>backgrounds originally supplied with antiX 17</sq>
   <sr>backgrounds originally supplied with antiX 17</sr>
   <sv>wallpapers ursprungligen medföljande antiX 17</sv>
   <tr>backgrounds originally supplied with antiX 17</tr>
   <uk>backgrounds originally supplied with antiX 17</uk>
   <vi>backgrounds originally supplied with antiX 17</vi>
   <zh_CN>backgrounds originally supplied with antiX 17</zh_CN>
   <zh_TW>backgrounds originally supplied with antiX 17</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
artwork17-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
artwork17-antix
</uninstall_package_names>
</app>
