<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Development
</category>

<name>
GIT Tools
</name>

<description>
   <am>GIT Repo tools</am>
   <ar>GIT Repo tools</ar>
   <be>GIT Repo tools</be>
   <bg>GIT Repo tools</bg>
   <bn>GIT Repo tools</bn>
   <ca>eines per al dipòsit GIT</ca>
   <cs>GIT Repo tools</cs>
   <da>GIT-softwarekildeværktøjer</da>
   <de>GIT Repo Werkzeuge</de>
   <el>Εργαλεία GIT</el>
   <en>GIT Repo tools</en>
   <es_ES>Herramientas de repositorio GIT</es_ES>
   <es>Herramientas de repositorio GIT</es>
   <et>GIT Repo tools</et>
   <eu>GIT Repo tools</eu>
   <fa>GIT Repo tools</fa>
   <fil_PH>GIT Repo tools</fil_PH>
   <fi>GIT-ohjelmavarastotyökalut</fi>
   <fr_BE>Outils pour dépôt GIT</fr_BE>
   <fr>Outils pour dépôt GIT</fr>
   <gl_ES>Ferramentas para Repositorios GIT</gl_ES>
   <gu>GIT Repo tools</gu>
   <he_IL>GIT Repo tools</he_IL>
   <hi>GIT पैकेज-संग्रह साधन</hi>
   <hr>GIT Repo tools</hr>
   <hu>GIT Repo tools</hu>
   <id>GIT Repo tools</id>
   <is>GIT Repo tools</is>
   <it>Strumenti GIT Repo</it>
   <ja>GIT Repo ツール</ja>
   <kk>GIT Repo tools</kk>
   <ko>GIT Repo tools</ko>
   <ku>GIT Repo tools</ku>
   <lt>GIT saugyklos įrankiai</lt>
   <mk>GIT Repo tools</mk>
   <mr>GIT Repo tools</mr>
   <nb_NO>GIT Repo tools</nb_NO>
   <nb>Verktøy for GIT-kodelager</nb>
   <nl_BE>GIT Repo tools</nl_BE>
   <nl>GIT Pakketbron Gereedschappen</nl>
   <or>GIT Repo tools</or>
   <pl>repozytorium narzędzi GIT</pl>
   <pt_BR>GIT Repo - Ferramenta de Gerenciamento de Repositórios</pt_BR>
   <pt>Ferramentas para  Repositórios GIT</pt>
   <ro>GIT Repo tools</ro>
   <ru>Инструменты репозитория GIT</ru>
   <sk>GIT Repo tools</sk>
   <sl>orodja za GIT repozitorije</sl>
   <so>GIT Repo tools</so>
   <sq>Përpunues i thelluar fotosh - instalon GIMP-in, ndihmën dhe shtojca</sq>
   <sr>GIT Repo tools</sr>
   <sv>GIT Förråds verktyg</sv>
   <th>เครื่องมือ GIT Repo</th>
   <tr>GIT depo araçları</tr>
   <uk>інструменти сховища GIT</uk>
   <vi>GIT Repo tools</vi>
   <zh_CN>GIT 仓库工具</zh_CN>
   <zh_HK>GIT Repo tools</zh_HK>
   <zh_TW>GIT Repo tools</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
git
gitk
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
git
gitk
</uninstall_package_names>
</app>
