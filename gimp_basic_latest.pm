<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Graphics
</category>

<name>
Latest GIMP Basic
</name>

<description>
   <am>advanced picture editor - installs GIMP only</am>
   <ar>advanced picture editor - installs GIMP only</ar>
   <be>advanced picture editor - installs GIMP only</be>
   <bg>advanced picture editor - installs GIMP only</bg>
   <bn>advanced picture editor - installs GIMP only</bn>
   <ca>editor gràfic avançat; instal·la només GIMP</ca>
   <cs>advanced picture editor - installs GIMP only</cs>
   <da>avanceret billedredigering - installerer kun GIMP</da>
   <de>Erweiterter Bildeditor - installiert nur GIMP</de>
   <el>Επεξεργαστής εικόνων - εγκαθιστά μόνο το GIMP</el>
   <en>advanced picture editor - installs GIMP only</en>
   <es_ES>Editor de imágenes avanzado. Instala GIMP solamente.</es_ES>
   <es>Editor de imágenes avanzado. Instala GIMP solamente.</es>
   <et>advanced picture editor - installs GIMP only</et>
   <eu>advanced picture editor - installs GIMP only</eu>
   <fa>advanced picture editor - installs GIMP only</fa>
   <fil_PH>advanced picture editor - installs GIMP only</fil_PH>
   <fi>edistynyt kuvankäsittely - asentaa vain GIMP:in</fi>
   <fr_BE>Éditeur d'image avancé - installe Gimp uniquement</fr_BE>
   <fr>Éditeur d'image avancé - installe Gimp uniquement</fr>
   <gl_ES>editor de imaxe avanzado - instala só o GIMP</gl_ES>
   <gu>advanced picture editor - installs GIMP only</gu>
   <he_IL>advanced picture editor - installs GIMP only</he_IL>
   <hi>विस्तृत चित्र संपादक - केवल GIMP इंस्टॉल होगा</hi>
   <hr>advanced picture editor - installs GIMP only</hr>
   <hu>advanced picture editor - installs GIMP only</hu>
   <id>advanced picture editor - installs GIMP only</id>
   <is>advanced picture editor - installs GIMP only</is>
   <it>Editor di immagini avanzato - installa solo GIMP</it>
   <ja>高度な画像エディター - GIMPのみインストール</ja>
   <kk>advanced picture editor - installs GIMP only</kk>
   <ko>advanced picture editor - installs GIMP only</ko>
   <ku>advanced picture editor - installs GIMP only</ku>
   <lt>išplėstinis paveikslų redaktorius - įdiegia tik GIMP</lt>
   <mk>advanced picture editor - installs GIMP only</mk>
   <mr>advanced picture editor - installs GIMP only</mr>
   <nb_NO>advanced picture editor - installs GIMP only</nb_NO>
   <nb>avansert bilderedigering – installerer kun GIMP</nb>
   <nl_BE>advanced picture editor - installs GIMP only</nl_BE>
   <nl>geavanceerde foto-editor - installeert alleen GIMP</nl>
   <or>advanced picture editor - installs GIMP only</or>
   <pl>zaawansowany edytor grafiki i zdjęć - instaluje tylko program GIMP</pl>
   <pt_BR>Editor de imagem avançado - instala apenas o GIMP</pt_BR>
   <pt>Editor de imagem avançado - instala apenas o GIMP</pt>
   <ro>advanced picture editor - installs GIMP only</ro>
   <ru>Продвинутый графический редактор - установка только GIMP</ru>
   <sk>advanced picture editor - installs GIMP only</sk>
   <sl>napredni urejevalnik slik - namesti le GIMP</sl>
   <so>advanced picture editor - installs GIMP only</so>
   <sq>Një klient FTP me shumë rrjedha</sq>
   <sr>advanced picture editor - installs GIMP only</sr>
   <sv>avancerad bildredigerare - installerar enbart GIMP</sv>
   <th>โปรแกรมตัดต่อถาพขั้นสูง - ติดตั้ง GIMP เท่านั้น</th>
   <tr>gelişmiş resim düzenleyici - yalnızca GIMP yükler</tr>
   <uk>розширений редактор зображень - встановлює лише GIMP</uk>
   <vi>advanced picture editor - installs GIMP only</vi>
   <zh_CN>高级图像编辑器 - 仅安装 GIMP</zh_CN>
   <zh_HK>advanced picture editor - installs GIMP only</zh_HK>
   <zh_TW>advanced picture editor - installs GIMP only</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gimp
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gimp
</uninstall_package_names>
</app>
