<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
Endless Sky
</name>

<description>
   <en>Space exploration and combat game</en>
</description>

<installable>
all
</installable>

<screenshot>
</screenshot>

<preinstall>

</preinstall>

<install_package_names>
endless-sky
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
endless-sky
</uninstall_package_names>
</app>
