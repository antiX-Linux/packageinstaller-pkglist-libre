<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
File Managers
</category>

<name>
PCManFM
</name>

<description>
   <am>a fast and lightweight file manager</am>
   <ar>a fast and lightweight file manager</ar>
   <be>a fast and lightweight file manager</be>
   <bg>a fast and lightweight file manager</bg>
   <bn>a fast and lightweight file manager</bn>
   <ca>Un gestor de fitxers ràpid i lleuger</ca>
   <cs>a fast and lightweight file manager</cs>
   <da>en hurtig og letvægts filhåndtering</da>
   <de>Ein schneller und leichter Dateimanager</de>
   <el>Γρήγορος και ελαφρύς διαχειριστής αρχείων</el>
   <en>a fast and lightweight file manager</en>
   <es_ES>Gestor de archivos rápido y liviano</es_ES>
   <es>gestor de archivos rápido y liviano</es>
   <et>a fast and lightweight file manager</et>
   <eu>a fast and lightweight file manager</eu>
   <fa>a fast and lightweight file manager</fa>
   <fil_PH>a fast and lightweight file manager</fil_PH>
   <fi>Nopea ja kevyt tiedostonhallinta</fi>
   <fr_BE>Un gestionnaire de fichiers rapide et léger</fr_BE>
   <fr>Un gestionnaire de fichiers rapide et léger</fr>
   <gl_ES>Xestor de ficheiros lixeiro e rápido</gl_ES>
   <gu>a fast and lightweight file manager</gu>
   <he_IL>a fast and lightweight file manager</he_IL>
   <hi>तीव्र व सरल फाइल प्रबंधक</hi>
   <hr>a fast and lightweight file manager</hr>
   <hu>a fast and lightweight file manager</hu>
   <id>a fast and lightweight file manager</id>
   <is>a fast and lightweight file manager</is>
   <it>file manager veloce e leggero</it>
   <ja>高速で軽量なファイルマネージャ</ja>
   <kk>a fast and lightweight file manager</kk>
   <ko>a fast and lightweight file manager</ko>
   <ku>a fast and lightweight file manager</ku>
   <lt>greita ir supaprastinta failų tvarkytuvė</lt>
   <mk>a fast and lightweight file manager</mk>
   <mr>a fast and lightweight file manager</mr>
   <nb_NO>a fast and lightweight file manager</nb_NO>
   <nb>rask og lettvektig filbehandler</nb>
   <nl_BE>a fast and lightweight file manager</nl_BE>
   <nl>een snelle en lichtgewicht bestandsmanager</nl>
   <or>a fast and lightweight file manager</or>
   <pl>szybki i lekki menedżer plików</pl>
   <pt_BR>Gerenciador de arquivos rápido e leve</pt_BR>
   <pt>Gestor de ficheiros ligeiro e rápido</pt>
   <ro>a fast and lightweight file manager</ro>
   <ru>Быстрый и легковесный файловый менеджер</ru>
   <sk>a fast and lightweight file manager</sk>
   <sl>Lahek in hiter upravljalnik datotek</sl>
   <so>a fast and lightweight file manager</so>
   <sq>Një përgjegjës kartelash i shpejtë dhe i peshës së lehtë</sq>
   <sr>a fast and lightweight file manager</sr>
   <sv>en snabb lättvikts filhanterare</sv>
   <th>โปรแกรมจัดการไฟล์ที่รวดเร็วและน้ำหนักเบา</th>
   <tr>hızlı ve hafif bir dosya yöneticisi</tr>
   <uk>швидкий та легкий файловий менеджер</uk>
   <vi>a fast and lightweight file manager</vi>
   <zh_CN>a fast and lightweight file manager</zh_CN>
   <zh_HK>a fast and lightweight file manager</zh_HK>
   <zh_TW>a fast and lightweight file manager</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
pcmanfm
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
pcmanfm
</uninstall_package_names>
</app>
