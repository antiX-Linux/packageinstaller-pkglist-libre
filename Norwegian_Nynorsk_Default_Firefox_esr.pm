<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Norwegian_Nynorsk_Default_Firefox_esr
</name>

<description>
   <am>Norwegian_nynorsk Language localisation of Firefox ESR</am>
   <ar>Norwegian_nynorsk Language localisation of Firefox ESR</ar>
   <be>Norwegian_nynorsk Language localisation of Firefox ESR</be>
   <bg>Norwegian_nynorsk Language localisation of Firefox ESR</bg>
   <bn>Norwegian_nynorsk Language localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Noruec (Nynorsk)</ca>
   <cs>Norwegian_nynorsk Language localisation of Firefox ESR</cs>
   <da>Norwegian_nynorsk Language localisation of Firefox ESR</da>
   <de>Norwegische Lokalisation (Neunorwegisch) von “Firefox ESR”</de>
   <el>Νορβηγικό Nynorsk για Firefox ESR</el>
   <en>Norwegian_nynorsk Language localisation of Firefox ESR</en>
   <es_ES>Localización Noruego_Nynorsk de Firefox ESR</es_ES>
   <es>Localización Noruego Nynorsk de Firefox ESR</es>
   <et>Norwegian_nynorsk Language localisation of Firefox ESR</et>
   <eu>Norwegian_nynorsk Language localisation of Firefox ESR</eu>
   <fa>Norwegian_nynorsk Language localisation of Firefox ESR</fa>
   <fil_PH>Norwegian_nynorsk Language localisation of Firefox ESR</fil_PH>
   <fi>Norwegian_nynorsk Language localisation of Firefox ESR</fi>
   <fr_BE>Localisation en norvégien (Nynorsk) pour Firefox ESR</fr_BE>
   <fr>Localisation en norvégien (Nynorsk) pour Firefox ESR</fr>
   <gl_ES>Norwegian_nynorsk Language localisation of Firefox ESR</gl_ES>
   <gu>Norwegian_nynorsk Language localisation of Firefox ESR</gu>
   <he_IL>Norwegian_nynorsk Language localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का नॉर्वेजियाई_नाइनोर्स्क संस्करण</hi>
   <hr>Norwegian_nynorsk Language localisation of Firefox ESR</hr>
   <hu>Norwegian_nynorsk Language localisation of Firefox ESR</hu>
   <id>Norwegian_nynorsk Language localisation of Firefox ESR</id>
   <is>Norwegian_nynorsk Language localisation of Firefox ESR</is>
   <it>Localizzazione norvegese_nynorsk di Firefox ESR</it>
   <ja>ニーノシュク版 Firefox ESR</ja>
   <kk>Norwegian_nynorsk Language localisation of Firefox ESR</kk>
   <ko>Norwegian_nynorsk Language localisation of Firefox ESR</ko>
   <ku>Norwegian_nynorsk Language localisation of Firefox ESR</ku>
   <lt>Norwegian_nynorsk Language localisation of Firefox ESR</lt>
   <mk>Norwegian_nynorsk Language localisation of Firefox ESR</mk>
   <mr>Norwegian_nynorsk Language localisation of Firefox ESR</mr>
   <nb_NO>Norwegian_nynorsk Language localisation of Firefox ESR</nb_NO>
   <nb>Nynorsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Norwegian_nynorsk Language localisation of Firefox ESR</nl_BE>
   <nl>Norwegian_nynorsk Language localisation of Firefox ESR</nl>
   <or>Norwegian_nynorsk Language localisation of Firefox ESR</or>
   <pl>Norwegian_nynorsk Language localisation of Firefox ESR</pl>
   <pt_BR>Novo Norueguês Localização para o Firefox ESR</pt_BR>
   <pt>Norueguês_nynorsk Localização para Firefox ESR</pt>
   <ro>Norwegian_nynorsk Language localisation of Firefox ESR</ro>
   <ru>Norwegian_nynorsk Language localisation of Firefox ESR</ru>
   <sk>Norwegian_nynorsk Language localisation of Firefox ESR</sk>
   <sl>Norveške nynorsk krajevne nastavitve za Firefox ESR</sl>
   <so>Norwegian_nynorsk Language localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në norvegjishte ninorsk</sq>
   <sr>Norwegian_nynorsk Language localisation of Firefox ESR</sr>
   <sv>Norsk_nynorsk Språk lokalisering av Firefox ESR</sv>
   <th>Norwegian_nynorsk Language localisation of Firefox ESR</th>
   <tr>Firefox ESR Yeni Norveççe yerelleştirmesi</tr>
   <uk>Norwegian_nynorsk Language localisation of Firefox ESR</uk>
   <vi>Norwegian_nynorsk Language localisation of Firefox ESR</vi>
   <zh_CN>Norwegian_nynorsk Language localisation of Firefox ESR</zh_CN>
   <zh_HK>Norwegian_nynorsk Language localisation of Firefox ESR</zh_HK>
   <zh_TW>Norwegian_nynorsk Language localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-nn-no
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-nn-no
</uninstall_package_names>

</app>
