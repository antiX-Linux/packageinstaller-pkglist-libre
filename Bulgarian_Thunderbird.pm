<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bulgarian_Thunderbird
</name>

<description>
   <am>Bulgarian localisation of Thunderbird</am>
   <ar>Bulgarian localisation of Thunderbird</ar>
   <be>Bulgarian localisation of Thunderbird</be>
   <bg>Bulgarian localisation of Thunderbird</bg>
   <bn>Bulgarian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Búlgar</ca>
   <cs>Bulgarian localisation of Thunderbird</cs>
   <da>Bulgarsk oversættelse af Thunderbird</da>
   <de>Bulgarische Lokalisierung von Thunderbird</de>
   <el>Βουλγαρικά για το Thunderbird</el>
   <en>Bulgarian localisation of Thunderbird</en>
   <es_ES>Localización Búlgaro de Thunderbird</es_ES>
   <es>Localización Búlgaro de Thunderbird</es>
   <et>Bulgarian localisation of Thunderbird</et>
   <eu>Bulgarian localisation of Thunderbird</eu>
   <fa>Bulgarian localisation of Thunderbird</fa>
   <fil_PH>Bulgarian localisation of Thunderbird</fil_PH>
   <fi>Bulgarialainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en bulgare pour Thunderbird</fr_BE>
   <fr>Localisation en bulgare pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird para búlgaro</gl_ES>
   <gu>Bulgarian localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לבולגרית</he_IL>
   <hi>थंडरबर्ड का बल्गेरियाई संस्करण</hi>
   <hr>Bulgarian localisation of Thunderbird</hr>
   <hu>Bulgarian localisation of Thunderbird</hu>
   <id>Bulgarian localisation of Thunderbird</id>
   <is>Bulgarian localisation of Thunderbird</is>
   <it>Localizzazione bulgara di Thunderbird</it>
   <ja>Thunderbird のブルガリア語版</ja>
   <kk>Bulgarian localisation of Thunderbird</kk>
   <ko>Bulgarian localisation of Thunderbird</ko>
   <ku>Bulgarian localisation of Thunderbird</ku>
   <lt>Bulgarian localisation of Thunderbird</lt>
   <mk>Bulgarian localisation of Thunderbird</mk>
   <mr>Bulgarian localisation of Thunderbird</mr>
   <nb_NO>Bulgarian localisation of Thunderbird</nb_NO>
   <nb>Bulgarsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Bulgarian localisation of Thunderbird</nl_BE>
   <nl>Bulgaarse lokalisatie van Thunderbird</nl>
   <or>Bulgarian localisation of Thunderbird</or>
   <pl>Bułgarska lokalizacja Thunderbirda</pl>
   <pt_BR>Búlgaro Localização para o Thunderbird</pt_BR>
   <pt>Búlgaro Localização para Thunderbird</pt>
   <ro>Bulgarian localisation of Thunderbird</ro>
   <ru>Bulgarian localisation of Thunderbird</ru>
   <sk>Bulgarian localisation of Thunderbird</sk>
   <sl>Bulgarian localisation of Thunderbird</sl>
   <so>Bulgarian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në bullgarisht</sq>
   <sr>Bulgarian localisation of Thunderbird</sr>
   <sv>Bulgarisk lokalisering av  Thunderbird</sv>
   <th>Bulgarian localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Bulgarca yerelleştirmesi</tr>
   <uk>Bulgarian локалізація Thunderbird</uk>
   <vi>Bulgarian localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 保加利亚语语言包</zh_CN>
   <zh_HK>Bulgarian localisation of Thunderbird</zh_HK>
   <zh_TW>Bulgarian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-bg
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-bg
</uninstall_package_names>

</app>
