<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Croatian
</name>

<description>
   <am>Croatian dictionary for hunspell</am>
   <ar>Croatian dictionary for hunspell</ar>
   <be>Croatian dictionary for hunspell</be>
   <bg>Croatian dictionary for hunspell</bg>
   <bn>Croatian dictionary for hunspell</bn>
   <ca>Diccionari Croata per hunspell</ca>
   <cs>Croatian dictionary for hunspell</cs>
   <da>Croatian dictionary for hunspell</da>
   <de>Kroatisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Κροατικά για hunspell</el>
   <en>Croatian dictionary for hunspell</en>
   <es_ES>Diccionario Croata para hunspell</es_ES>
   <es>Diccionario Croata para hunspell</es>
   <et>Croatian dictionary for hunspell</et>
   <eu>Croatian dictionary for hunspell</eu>
   <fa>Croatian dictionary for hunspell</fa>
   <fil_PH>Croatian dictionary for hunspell</fil_PH>
   <fi>Croatian dictionary for hunspell</fi>
   <fr_BE>Croate dictionnaire pour hunspell</fr_BE>
   <fr>Croate dictionnaire pour hunspell</fr>
   <gl_ES>Croatian dictionary for hunspell</gl_ES>
   <gu>Croatian dictionary for hunspell</gu>
   <he_IL>Croatian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु क्रोएशियाई शब्दकोष</hi>
   <hr>Croatian dictionary for hunspell</hr>
   <hu>Croatian dictionary for hunspell</hu>
   <id>Croatian dictionary for hunspell</id>
   <is>Croatian dictionary for hunspell</is>
   <it>Dizionario croato per hunspell</it>
   <ja>Hunspell 用クロアチア語辞書</ja>
   <kk>Croatian dictionary for hunspell</kk>
   <ko>Croatian dictionary for hunspell</ko>
   <ku>Croatian dictionary for hunspell</ku>
   <lt>Croatian dictionary for hunspell</lt>
   <mk>Croatian dictionary for hunspell</mk>
   <mr>Croatian dictionary for hunspell</mr>
   <nb_NO>Croatian dictionary for hunspell</nb_NO>
   <nb>Kroatisk ordliste for hunspell</nb>
   <nl_BE>Croatian dictionary for hunspell</nl_BE>
   <nl>Kroatisch woordenboek voor hunspell</nl>
   <or>Croatian dictionary for hunspell</or>
   <pl>Croatian dictionary for hunspell</pl>
   <pt_BR>Dicionário Croata para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Croata para hunspell</pt>
   <ro>Croatian dictionary for hunspell</ro>
   <ru>Croatian dictionary for hunspell</ru>
   <sk>Croatian dictionary for hunspell</sk>
   <sl>Hrvaški slovar za hunspell</sl>
   <so>Croatian dictionary for hunspell</so>
   <sq>Fjalor kroatisht për hunspell</sq>
   <sr>Croatian dictionary for hunspell</sr>
   <sv>Kroatisk ordbok för hunspell</sv>
   <th>Croatian dictionary for hunspell</th>
   <tr>Hunspell için Hırvatça sözlük</tr>
   <uk>Croatian dictionary for hunspell</uk>
   <vi>Croatian dictionary for hunspell</vi>
   <zh_CN>Croatian dictionary for hunspell</zh_CN>
   <zh_HK>Croatian dictionary for hunspell</zh_HK>
   <zh_TW>Croatian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-hr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-hr
</uninstall_package_names>

</app>
