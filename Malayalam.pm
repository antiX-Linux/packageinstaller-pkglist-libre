<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Malayalam
</name>

<description>
   <am>Malayalam dictionary for hunspell</am>
   <ar>Malayalam dictionary for hunspell</ar>
   <be>Malayalam dictionary for hunspell</be>
   <bg>Malayalam dictionary for hunspell</bg>
   <bn>Malayalam dictionary for hunspell</bn>
   <ca>Diccionari Malai per hunspell</ca>
   <cs>Malayalam dictionary for hunspell</cs>
   <da>Malayalam dictionary for hunspell</da>
   <de>Indisches (malayalamisches) Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Μαλαγιαλάμ για hunspell</el>
   <en>Malayalam dictionary for hunspell</en>
   <es_ES>Diccionario Malayalam para hunspell</es_ES>
   <es>Diccionario Malayalam para hunspell</es>
   <et>Malayalam dictionary for hunspell</et>
   <eu>Malayalam dictionary for hunspell</eu>
   <fa>Malayalam dictionary for hunspell</fa>
   <fil_PH>Malayalam dictionary for hunspell</fil_PH>
   <fi>Malayalam dictionary for hunspell</fi>
   <fr_BE>Malayalam dictionnaire pour hunspell</fr_BE>
   <fr>Malayalam dictionnaire pour hunspell</fr>
   <gl_ES>Malayalam dictionary for hunspell</gl_ES>
   <gu>Malayalam dictionary for hunspell</gu>
   <he_IL>Malayalam dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु मलयालम शब्दकोष</hi>
   <hr>Malayalam dictionary for hunspell</hr>
   <hu>Malayalam dictionary for hunspell</hu>
   <id>Malayalam dictionary for hunspell</id>
   <is>Malayalam dictionary for hunspell</is>
   <it>Dizionario malayalam per hunspell</it>
   <ja>Hunspell 用マラヤーラム語辞書</ja>
   <kk>Malayalam dictionary for hunspell</kk>
   <ko>Malayalam dictionary for hunspell</ko>
   <ku>Malayalam dictionary for hunspell</ku>
   <lt>Malayalam dictionary for hunspell</lt>
   <mk>Malayalam dictionary for hunspell</mk>
   <mr>Malayalam dictionary for hunspell</mr>
   <nb_NO>Malayalam dictionary for hunspell</nb_NO>
   <nb>Malayalam ordliste for hunspell</nb>
   <nl_BE>Malayalam dictionary for hunspell</nl_BE>
   <nl>Malayalam dictionary for hunspell</nl>
   <or>Malayalam dictionary for hunspell</or>
   <pl>Malayalam dictionary for hunspell</pl>
   <pt_BR>Dicionário Malaiala para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Malaiala para hunspell</pt>
   <ro>Malayalam dictionary for hunspell</ro>
   <ru>Malayalam dictionary for hunspell</ru>
   <sk>Malayalam dictionary for hunspell</sk>
   <sl>Malajalamski slovar za hunspell</sl>
   <so>Malayalam dictionary for hunspell</so>
   <sq>Fjalor majalamisht për hunspell</sq>
   <sr>Malayalam dictionary for hunspell</sr>
   <sv>Malayalam ordbok för hunspell</sv>
   <th>Malayalam dictionary for hunspell</th>
   <tr>Hunspell için Malayalamca sözlük</tr>
   <uk>Malayalam dictionary for hunspell</uk>
   <vi>Malayalam dictionary for hunspell</vi>
   <zh_CN>Malayalam dictionary for hunspell</zh_CN>
   <zh_HK>Malayalam dictionary for hunspell</zh_HK>
   <zh_TW>Malayalam dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-ml
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ml
</uninstall_package_names>

</app>
