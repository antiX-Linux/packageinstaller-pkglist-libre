<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Icelandic_Firefox
</name>

<description>
   <am>Icelandic localisation of Firefox</am>
   <ar>Icelandic localisation of Firefox</ar>
   <be>Icelandic localisation of Firefox</be>
   <bg>Icelandic localisation of Firefox</bg>
   <bn>Icelandic localisation of Firefox</bn>
   <ca>Localització de Firefox en Islandès</ca>
   <cs>Icelandic localisation of Firefox</cs>
   <da>Islandsk oversættelse af Firefox</da>
   <de>Isländische Lokalisierung von Firefox</de>
   <el>Ισλανδικά για το Firefox</el>
   <en>Icelandic localisation of Firefox</en>
   <es_ES>Localización Islandesa de Firefox</es_ES>
   <es>Localización Islandés de Firefox</es>
   <et>Icelandic localisation of Firefox</et>
   <eu>Icelandic localisation of Firefox</eu>
   <fa>Icelandic localisation of Firefox</fa>
   <fil_PH>Icelandic localisation of Firefox</fil_PH>
   <fi>Islantilainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en islandais pour Firefox</fr_BE>
   <fr>Localisation en islandais pour Firefox</fr>
   <gl_ES>Localización de Firefox ao islandés</gl_ES>
   <gu>Icelandic localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לאיסלנדית</he_IL>
   <hi>फायरफॉक्स का आइसलैंडिक संस्करण</hi>
   <hr>Icelandic localisation of Firefox</hr>
   <hu>Icelandic localisation of Firefox</hu>
   <id>Icelandic localisation of Firefox</id>
   <is>Icelandic localisation of Firefox</is>
   <it>Localizzazione islandese di Firefox</it>
   <ja>Firefox のアイスランド語版</ja>
   <kk>Icelandic localisation of Firefox</kk>
   <ko>Icelandic localisation of Firefox</ko>
   <ku>Icelandic localisation of Firefox</ku>
   <lt>Icelandic localisation of Firefox</lt>
   <mk>Icelandic localisation of Firefox</mk>
   <mr>Icelandic localisation of Firefox</mr>
   <nb_NO>Icelandic localisation of Firefox</nb_NO>
   <nb>Islandsk lokaltilpassing av Firefox</nb>
   <nl_BE>Icelandic localisation of Firefox</nl_BE>
   <nl>IJslandse lokalisatie van Firefox</nl>
   <or>Icelandic localisation of Firefox</or>
   <pl>Islandzka lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Islandês Localização para o Firefox</pt_BR>
   <pt>Islandês Localização para Firefox</pt>
   <ro>Icelandic localisation of Firefox</ro>
   <ru>Исландская локализация Firefox</ru>
   <sk>Icelandic localisation of Firefox</sk>
   <sl>Icelandic localisation of Firefox</sl>
   <so>Icelandic localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në islandisht</sq>
   <sr>Icelandic localisation of Firefox</sr>
   <sv>Isländsk lokalisering av Firefox </sv>
   <th>Icelandic localisation ของ Firefox</th>
   <tr>Firefox'un İzlandaca yerelleştirmesi</tr>
   <uk>Icelandic локалізація Firefox</uk>
   <vi>Icelandic localisation of Firefox</vi>
   <zh_CN>Icelandic localisation of Firefox</zh_CN>
   <zh_HK>Icelandic localisation of Firefox</zh_HK>
   <zh_TW>Icelandic localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-is
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-is
</uninstall_package_names>
</app>
