<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Newsreader
</category>

<name>
Pan
</name>

<description>
   <am>a gnome Usenet newsreader</am>
   <ar>a gnome Usenet newsreader</ar>
   <be>a gnome Usenet newsreader</be>
   <bg>a gnome Usenet newsreader</bg>
   <bn>a gnome Usenet newsreader</bn>
   <ca>lector de notícies Usenet</ca>
   <cs>a gnome Usenet newsreader</cs>
   <da>en gnome Usenet-nyhedslæser</da>
   <de>Ein Gnome Usenet Newsreader</de>
   <el>Καθαρός αναγνώστης ροών</el>
   <en>a gnome Usenet newsreader</en>
   <es_ES>Lector de noticias de Usenet de gnome</es_ES>
   <es>Lector de noticias Usenet de gnome</es>
   <et>a gnome Usenet newsreader</et>
   <eu>a gnome Usenet newsreader</eu>
   <fa>a gnome Usenet newsreader</fa>
   <fil_PH>a gnome Usenet newsreader</fil_PH>
   <fi>GNOME-kehittäjäryhmän tekemä Usenet-uutistenlukija</fi>
   <fr_BE>Un lecteur de nouvelles Usenet pour Gnome</fr_BE>
   <fr>Un lecteur de nouvelles Usenet pour Gnome</fr>
   <gl_ES>Engadidor de noticias Usenet para gnome</gl_ES>
   <gu>a gnome Usenet newsreader</gu>
   <he_IL>a gnome Usenet newsreader</he_IL>
   <hi>गनोम यूज़नेट समाचार साधन</hi>
   <hr>a gnome Usenet newsreader</hr>
   <hu>a gnome Usenet newsreader</hu>
   <id>a gnome Usenet newsreader</id>
   <is>a gnome Usenet newsreader</is>
   <it>lettore di news dalla rete Usenet basato su GTK2</it>
   <ja>GNOME Usenet ニュースリーダ</ja>
   <kk>a gnome Usenet newsreader</kk>
   <ko>a gnome Usenet newsreader</ko>
   <ku>a gnome Usenet newsreader</ku>
   <lt>a gnome Usenet newsreader</lt>
   <mk>a gnome Usenet newsreader</mk>
   <mr>a gnome Usenet newsreader</mr>
   <nb_NO>a gnome Usenet newsreader</nb_NO>
   <nb>Usenet nyhetsleser fra GNOME</nb>
   <nl_BE>a gnome Usenet newsreader</nl_BE>
   <nl>a gnome Usenet nieuwslezer</nl>
   <or>a gnome Usenet newsreader</or>
   <pl>gnomowy czytnik grup dyskusyjnych Usenet</pl>
   <pt_BR>Agregador e leitor de notícias Usenet para gnome</pt_BR>
   <pt>Agregador de notícias Usenet para gnome</pt>
   <ro>a gnome Usenet newsreader</ro>
   <ru>Новостной клиент Usenet для окружения Gnome</ru>
   <sk>a gnome Usenet newsreader</sk>
   <sl>Bralnik novic za Usenet za Gnome okolje</sl>
   <so>a gnome Usenet newsreader</so>
   <sq>Një lexues lajmesh Usenet për Gnome</sq>
   <sr>a gnome Usenet newsreader</sr>
   <sv>en gnome Usenet nyhetsläsare</sv>
   <th>a gnome Usenet newsreader</th>
   <tr>gnome Usenet haber okuyucu</tr>
   <uk>a gnome Usenet newsreader</uk>
   <vi>a gnome Usenet newsreader</vi>
   <zh_CN>a gnome Usenet newsreader</zh_CN>
   <zh_HK>a gnome Usenet newsreader</zh_HK>
   <zh_TW>a gnome Usenet newsreader</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
pan
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
pan
</uninstall_package_names>
</app>
