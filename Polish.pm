<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Polish
</name>

<description>
   <am>Polish dictionary for hunspell</am>
   <ar>Polish dictionary for hunspell</ar>
   <be>Polish dictionary for hunspell</be>
   <bg>Polish dictionary for hunspell</bg>
   <bn>Polish dictionary for hunspell</bn>
   <ca>Diccionari Polonès per hunspell</ca>
   <cs>Polish dictionary for hunspell</cs>
   <da>Polish dictionary for hunspell</da>
   <de>Polnisches Wörterbuch für “Hunspell”</de>
   <el>Πολωνικό λεξικό για hunspell</el>
   <en>Polish dictionary for hunspell</en>
   <es_ES>Diccionario Polaco para hunspell</es_ES>
   <es>Diccionario Polaco para hunspell</es>
   <et>Polish dictionary for hunspell</et>
   <eu>Polish dictionary for hunspell</eu>
   <fa>Polish dictionary for hunspell</fa>
   <fil_PH>Polish dictionary for hunspell</fil_PH>
   <fi>Polish dictionary for hunspell</fi>
   <fr_BE>Polonais dictionnaire pour hunspell</fr_BE>
   <fr>Polonais dictionnaire pour hunspell</fr>
   <gl_ES>Polish dictionary for hunspell</gl_ES>
   <gu>Polish dictionary for hunspell</gu>
   <he_IL>Polish dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु पोलिश शब्दकोष</hi>
   <hr>Polish dictionary for hunspell</hr>
   <hu>Polish dictionary for hunspell</hu>
   <id>Polish dictionary for hunspell</id>
   <is>Polish dictionary for hunspell</is>
   <it>Dizionario polacco per hunspell</it>
   <ja>Hunspell 用ポーランド語辞書</ja>
   <kk>Polish dictionary for hunspell</kk>
   <ko>Polish dictionary for hunspell</ko>
   <ku>Polish dictionary for hunspell</ku>
   <lt>Polish dictionary for hunspell</lt>
   <mk>Polish dictionary for hunspell</mk>
   <mr>Polish dictionary for hunspell</mr>
   <nb_NO>Polish dictionary for hunspell</nb_NO>
   <nb>Polsk ordliste for hunspell</nb>
   <nl_BE>Polish dictionary for hunspell</nl_BE>
   <nl>Polish dictionary for hunspell</nl>
   <or>Polish dictionary for hunspell</or>
   <pl>Polish dictionary for hunspell</pl>
   <pt_BR>Dicionário Polonês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Polaco para hunspell</pt>
   <ro>Polish dictionary for hunspell</ro>
   <ru>Polish dictionary for hunspell</ru>
   <sk>Polish dictionary for hunspell</sk>
   <sl>Poljski slovar za hunspell</sl>
   <so>Polish dictionary for hunspell</so>
   <sq>Fjalor polonisht për hunspell</sq>
   <sr>Polish dictionary for hunspell</sr>
   <sv>Polsk ordbok för hunspell</sv>
   <th>Polish dictionary for hunspell</th>
   <tr>Hunspell için Lehçe sözlük</tr>
   <uk>Polish dictionary for hunspell</uk>
   <vi>Polish dictionary for hunspell</vi>
   <zh_CN>Polish dictionary for hunspell</zh_CN>
   <zh_HK>Polish dictionary for hunspell</zh_HK>
   <zh_TW>Polish dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-pl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-pl
</uninstall_package_names>

</app>
