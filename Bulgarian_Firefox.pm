<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bulgarian_Firefox
</name>

<description>
   <am>Bulgarian localisation of Firefox</am>
   <ar>Bulgarian localisation of Firefox</ar>
   <be>Bulgarian localisation of Firefox</be>
   <bg>Bulgarian localisation of Firefox</bg>
   <bn>Bulgarian localisation of Firefox</bn>
   <ca>Localització de Firefox en Búlgar</ca>
   <cs>Bulgarian localisation of Firefox</cs>
   <da>Bulgarsk oversættelse af Firefox</da>
   <de>Bulgarische Lokalisierung von Firefox</de>
   <el>Βουλγαρικά για το Firefox</el>
   <en>Bulgarian localisation of Firefox</en>
   <es_ES>Localización Búlgara de Firefox</es_ES>
   <es>Localización Búlgaro de Firefox</es>
   <et>Bulgarian localisation of Firefox</et>
   <eu>Bulgarian localisation of Firefox</eu>
   <fa>Bulgarian localisation of Firefox</fa>
   <fil_PH>Bulgarian localisation of Firefox</fil_PH>
   <fi>Bulgarialainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en bulgare pour Firefox</fr_BE>
   <fr>Localisation en bulgare pour Firefox</fr>
   <gl_ES>Localización de Firefox en búlgaro</gl_ES>
   <gu>Bulgarian localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לבולגרית</he_IL>
   <hi>फायरफॉक्स का बल्गेरियाई संस्करण</hi>
   <hr>Bulgarian localisation of Firefox</hr>
   <hu>Bulgarian localisation of Firefox</hu>
   <id>Bulgarian localisation of Firefox</id>
   <is>Bulgarian localisation of Firefox</is>
   <it>Localizzazione bulgara di Firefox</it>
   <ja>ブルガリア語版 Firefox</ja>
   <kk>Bulgarian localisation of Firefox</kk>
   <ko>Bulgarian localisation of Firefox</ko>
   <ku>Bulgarian localisation of Firefox</ku>
   <lt>Bulgarian localisation of Firefox</lt>
   <mk>Bulgarian localisation of Firefox</mk>
   <mr>Bulgarian localisation of Firefox</mr>
   <nb_NO>Bulgarian localisation of Firefox</nb_NO>
   <nb>Bulgarsk lokaltilpassing av Firefox</nb>
   <nl_BE>Bulgarian localisation of Firefox</nl_BE>
   <nl>Bulgaarse lokalisatie van Firefox</nl>
   <or>Bulgarian localisation of Firefox</or>
   <pl>Bułgarska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Búlgaro Localização para o Firefox</pt_BR>
   <pt>Búlgaro Localização para Firefox</pt>
   <ro>Bulgarian localisation of Firefox</ro>
   <ru>Болгарская локализация Firefox</ru>
   <sk>Bulgarian localisation of Firefox</sk>
   <sl>Bulgarian localisation of Firefox</sl>
   <so>Bulgarian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në bullgarisht</sq>
   <sr>Bulgarian localisation of Firefox</sr>
   <sv>Bulgarisk lokalisering av  Firefox</sv>
   <th>Bulgarian localisation ของ Firefox</th>
   <tr>Firefox Bulgarca yerelleştirmesi</tr>
   <uk>Bulgarian локалізація Firefox</uk>
   <vi>Bulgarian localisation of Firefox</vi>
   <zh_CN>Firefox 保加利亚语语言包</zh_CN>
   <zh_HK>Bulgarian localisation of Firefox</zh_HK>
   <zh_TW>Bulgarian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-bg
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-bg
</uninstall_package_names>
</app>
