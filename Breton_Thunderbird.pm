<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Breton_Thunderbird
</name>

<description>
   <am>Breton localisation of Thunderbird</am>
   <ar>Breton localisation of Thunderbird</ar>
   <be>Breton localisation of Thunderbird</be>
   <bg>Breton localisation of Thunderbird</bg>
   <bn>Breton localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Bretó</ca>
   <cs>Breton localisation of Thunderbird</cs>
   <da>Breton localisation of Thunderbird</da>
   <de>Bretonische Lokalisation von “Thunderbird”</de>
   <el>Breton για Firefox Thunderbird</el>
   <en>Breton localisation of Thunderbird</en>
   <es_ES>Localización Breton de Thunderbird</es_ES>
   <es>Localización Bretón de Thunderbird</es>
   <et>Breton localisation of Thunderbird</et>
   <eu>Breton localisation of Thunderbird</eu>
   <fa>Breton localisation of Thunderbird</fa>
   <fil_PH>Breton localisation of Thunderbird</fil_PH>
   <fi>Breton localisation of Thunderbird</fi>
   <fr_BE>Localisation en breton pour Thunderbird</fr_BE>
   <fr>Localisation en breton pour Thunderbird</fr>
   <gl_ES>Breton localisation of Thunderbird</gl_ES>
   <gu>Breton localisation of Thunderbird</gu>
   <he_IL>Breton localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का ब्रेटन संस्करण</hi>
   <hr>Breton localisation of Thunderbird</hr>
   <hu>Breton localisation of Thunderbird</hu>
   <id>Breton localisation of Thunderbird</id>
   <is>Breton localisation of Thunderbird</is>
   <it>Localizzazione breton di Thunderbird</it>
   <ja>ブルトン語版 Thunderbird</ja>
   <kk>Breton localisation of Thunderbird</kk>
   <ko>Breton localisation of Thunderbird</ko>
   <ku>Breton localisation of Thunderbird</ku>
   <lt>Breton localisation of Thunderbird</lt>
   <mk>Breton localisation of Thunderbird</mk>
   <mr>Breton localisation of Thunderbird</mr>
   <nb_NO>Breton localisation of Thunderbird</nb_NO>
   <nb>Bretonsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Breton localisation of Thunderbird</nl_BE>
   <nl>Bretonse lokalisatie van Thunderbird</nl>
   <or>Breton localisation of Thunderbird</or>
   <pl>Breton localisation of Thunderbird</pl>
   <pt_BR>Bretão Localização para o Thunderbird</pt_BR>
   <pt>Bretão Localização para Thunderbird</pt>
   <ro>Breton localisation of Thunderbird</ro>
   <ru>Breton localisation of Thunderbird</ru>
   <sk>Breton localisation of Thunderbird</sk>
   <sl>Bretonske krajevne nastavitve za Thunderbird</sl>
   <so>Breton localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në bretonisht</sq>
   <sr>Breton localisation of Thunderbird</sr>
   <sv>Bretonsk lokalisering av Thunderbird</sv>
   <th>Breton localisation of Thunderbird</th>
   <tr>Thunderbird Bretonca yerelleştirmesi</tr>
   <uk>Breton localisation of Thunderbird</uk>
   <vi>Breton localisation of Thunderbird</vi>
   <zh_CN>Breton localisation of Thunderbird</zh_CN>
   <zh_HK>Breton localisation of Thunderbird</zh_HK>
   <zh_TW>Breton localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-br
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-br
</uninstall_package_names>

</app>
