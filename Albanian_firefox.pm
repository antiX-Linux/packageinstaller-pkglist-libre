<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Albanian_Firefox
</name>

<description>
   <am>Albanian localisation of Firefox</am>
   <ar>Albanian localisation of Firefox</ar>
   <be>Albanian localisation of Firefox</be>
   <bg>Albanian localisation of Firefox</bg>
   <bn>Albanian localisation of Firefox</bn>
   <ca>Localització de Firefox en Albanès</ca>
   <cs>Albanian localisation of Firefox</cs>
   <da>Albansk oversættelse af Firefox</da>
   <de>Albanische Lokalisierung von Firefox</de>
   <el>Αλβανικά για το Firefox</el>
   <en>Albanian localisation of Firefox</en>
   <es_ES>Localización albanés de Firefox</es_ES>
   <es>Localización Albanés de Firefox</es>
   <et>Albanian localisation of Firefox</et>
   <eu>Albanian localisation of Firefox</eu>
   <fa>Albanian localisation of Firefox</fa>
   <fil_PH>Albanian localisation of Firefox</fil_PH>
   <fi>Albanian Firefox-kielipaketti</fi>
   <fr_BE>Localisation en albanais pour Firefox</fr_BE>
   <fr>Localisation en albanais pour Firefox</fr>
   <gl_ES>Firefox localizado en albanés</gl_ES>
   <gu>Albanian localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לאלבנית</he_IL>
   <hi>फायरफॉक्स का अल्बानियाई अनुवाद</hi>
   <hr>Albanska lokalizacija Firefoxa</hr>
   <hu>Albanian localisation of Firefox</hu>
   <id>Albanian localisation of Firefox</id>
   <is>Albanian localisation of Firefox</is>
   <it>Localizzazione albanese di Firefox</it>
   <ja>アルバニア語版 Firefox</ja>
   <kk>Albanian localisation of Firefox</kk>
   <ko>Albanian localisation of Firefox</ko>
   <ku>Albanian localisation of Firefox</ku>
   <lt>Albanian localisation of Firefox</lt>
   <mk>Albanian localisation of Firefox</mk>
   <mr>Albanian localisation of Firefox</mr>
   <nb_NO>Albanian localisation of Firefox</nb_NO>
   <nb>Albansk lokaltilpassing av Firefox</nb>
   <nl_BE>Albanian localisation of Firefox</nl_BE>
   <nl>Albanese lokalisatie van Firefox</nl>
   <or>Albanian localisation of Firefox</or>
   <pl>Albańska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Albanês Localização para o Firefox</pt_BR>
   <pt>Albanês Localização para Firefox</pt>
   <ro>Albanian localisation of Firefox</ro>
   <ru>Албанская локализация Firefox</ru>
   <sk>Albanian lokalizácia pre Firefox</sk>
   <sl>Albanske krajevne nastavitve za Firefox</sl>
   <so>Albanian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në shqip</sq>
   <sr>Albanian localisation of Firefox</sr>
   <sv>Albansk lokalisering av Firefox</sv>
   <th>Albanian localisation ของ Firefox</th>
   <tr>Firefox Arnavutça yerelleştirmesi</tr>
   <uk>Albanian локалізація Firefox</uk>
   <vi>Albanian localisation of Firefox</vi>
   <zh_CN>Firefox 阿尔巴尼亚语语言包</zh_CN>
   <zh_HK>Albanian localisation of Firefox</zh_HK>
   <zh_TW>Albanian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-sq
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-sq
</uninstall_package_names>
</app>
