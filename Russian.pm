<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Russian
</name>

<description>
   <am>Russian dictionary for hunspell</am>
   <ar>Russian dictionary for hunspell</ar>
   <be>Russian dictionary for hunspell</be>
   <bg>Russian dictionary for hunspell</bg>
   <bn>Russian dictionary for hunspell</bn>
   <ca>Diccionari Rus per hunspell</ca>
   <cs>Russian dictionary for hunspell</cs>
   <da>Russian dictionary for hunspell</da>
   <de>Russisches Wörterbuch (kyrillisch) für “Hunspell”</de>
   <el>Ρωσικό λεξικό για hunspell</el>
   <en>Russian dictionary for hunspell</en>
   <es_ES>Diccionario Ruso para hunspell</es_ES>
   <es>Diccionario Ruso para hunspell</es>
   <et>Russian dictionary for hunspell</et>
   <eu>Russian dictionary for hunspell</eu>
   <fa>Russian dictionary for hunspell</fa>
   <fil_PH>Russian dictionary for hunspell</fil_PH>
   <fi>Russian dictionary for hunspell</fi>
   <fr_BE>Russe dictionnaire pour hunspell</fr_BE>
   <fr>Russe dictionnaire pour hunspell</fr>
   <gl_ES>Russian dictionary for hunspell</gl_ES>
   <gu>Russian dictionary for hunspell</gu>
   <he_IL>Russian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु रूसी शब्दकोष</hi>
   <hr>Russian dictionary for hunspell</hr>
   <hu>Russian dictionary for hunspell</hu>
   <id>Russian dictionary for hunspell</id>
   <is>Russian dictionary for hunspell</is>
   <it>Dizionario russo per hunspell</it>
   <ja>Hunspell 用ロシア語辞書</ja>
   <kk>Russian dictionary for hunspell</kk>
   <ko>Russian dictionary for hunspell</ko>
   <ku>Russian dictionary for hunspell</ku>
   <lt>Russian dictionary for hunspell</lt>
   <mk>Russian dictionary for hunspell</mk>
   <mr>Russian dictionary for hunspell</mr>
   <nb_NO>Russian dictionary for hunspell</nb_NO>
   <nb>Russisk ordliste for hunspell</nb>
   <nl_BE>Russian dictionary for hunspell</nl_BE>
   <nl>Russian dictionary for hunspell</nl>
   <or>Russian dictionary for hunspell</or>
   <pl>Russian dictionary for hunspell</pl>
   <pt_BR>Dicionário Russo para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Russo para hunspell</pt>
   <ro>Russian dictionary for hunspell</ro>
   <ru>Russian dictionary for hunspell</ru>
   <sk>Russian dictionary for hunspell</sk>
   <sl>Ruski slovar za hunspell</sl>
   <so>Russian dictionary for hunspell</so>
   <sq>Fjalor rusisht për hunspell</sq>
   <sr>Russian dictionary for hunspell</sr>
   <sv>Rysk ordbok för hunspell</sv>
   <th>Russian dictionary for hunspell</th>
   <tr>Hunspell için Rusça sözlük</tr>
   <uk>Russian dictionary for hunspell</uk>
   <vi>Russian dictionary for hunspell</vi>
   <zh_CN>Russian dictionary for hunspell</zh_CN>
   <zh_HK>Russian dictionary for hunspell</zh_HK>
   <zh_TW>Russian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-ru
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ru
</uninstall_package_names>

</app>
