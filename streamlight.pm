<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Video
</category>

<name>
Streamlight
</name>

<description>
   <am>Stream or download Youtube videos by selecting links in any app or browser</am>
   <ar>Stream or download Youtube videos by selecting links in any app or browser</ar>
   <be>Stream or download Youtube videos by selecting links in any app or browser</be>
   <bg>Stream or download Youtube videos by selecting links in any app or browser</bg>
   <bn>Stream or download Youtube videos by selecting links in any app or browser</bn>
   <ca>Transmet un flux o descarrega vídeos de Youtube seleccionant els enllaços en qualsevol aplicació o navegador</ca>
   <cs>Stream or download Youtube videos by selecting links in any app or browser</cs>
   <da>Stream or download Youtube videos by selecting links in any app or browser</da>
   <de>Übertragen (Ugs.: “Stream”) oder herunterladen von Videos von “Youtube” durch einfache Auswahl von Links in Internetbrowsern oder beliebeigen Programmen.</de>
   <el>Ροή ή λήψη βίντεο Youtube επιλέγοντας συνδέσμους σε οποιαδήποτε εφαρμογή ή πρόγραμμα περιήγησης</el>
   <en>Stream or download Youtube videos by selecting links in any app or browser</en>
   <es_ES>Transmita o descargue videos de Youtube seleccionando enlaces en cualquier aplicación o navegador</es_ES>
   <es>Transmita o descargue videos de Youtube seleccionando enlaces en cualquier aplicación o navegador</es>
   <et>Stream or download Youtube videos by selecting links in any app or browser</et>
   <eu>Stream or download Youtube videos by selecting links in any app or browser</eu>
   <fa>Stream or download Youtube videos by selecting links in any app or browser</fa>
   <fil_PH>Stream or download Youtube videos by selecting links in any app or browser</fil_PH>
   <fi>Toista tai lataa Youtube-videoita valitsemalla linkit missä tahansa sovelluksessa tai selaimessa</fi>
   <fr_BE>Visionnez ou téléchargez des vidéos Youtube en sélectionnant les liens dans n'importe quelle application ou navigateur.</fr_BE>
   <fr>Visionnez ou téléchargez des vidéos Youtube en sélectionnant les liens dans n'importe quelle application ou navigateur.</fr>
   <gl_ES>Stream or download Youtube videos by selecting links in any app or browser</gl_ES>
   <gu>Stream or download Youtube videos by selecting links in any app or browser</gu>
   <he_IL>Stream or download Youtube videos by selecting links in any app or browser</he_IL>
   <hi>अनुप्रयोग या ब्राउज़र में यूट्यूब वीडियो लिंक चयनित कर उन्हें स्ट्रीम या डाउनलोड करें</hi>
   <hr>Stream or download Youtube videos by selecting links in any app or browser</hr>
   <hu>Stream or download Youtube videos by selecting links in any app or browser</hu>
   <id>Stream or download Youtube videos by selecting links in any app or browser</id>
   <is>Stream or download Youtube videos by selecting links in any app or browser</is>
   <it>Riproduci o scarica i video di Youtube selezionando i link in qualsiasi app o browser</it>
   <ja>任意のアプリやブラウザでリンクを選択して、Youtubeビデオのストリーミングまたはダウンロードができます</ja>
   <kk>Stream or download Youtube videos by selecting links in any app or browser</kk>
   <ko>Stream or download Youtube videos by selecting links in any app or browser</ko>
   <ku>Stream or download Youtube videos by selecting links in any app or browser</ku>
   <lt>Stream or download Youtube videos by selecting links in any app or browser</lt>
   <mk>Stream or download Youtube videos by selecting links in any app or browser</mk>
   <mr>Stream or download Youtube videos by selecting links in any app or browser</mr>
   <nb_NO>Stream or download Youtube videos by selecting links in any app or browser</nb_NO>
   <nb>Strøm eller last ned Youtube-videoer ved å velge lenker i et program eller nettleser</nb>
   <nl_BE>Stream or download Youtube videos by selecting links in any app or browser</nl_BE>
   <nl>Stream or download Youtube videos by selecting links in any app or browser</nl>
   <or>Stream or download Youtube videos by selecting links in any app or browser</or>
   <pl>Stream or download Youtube videos by selecting links in any app or browser</pl>
   <pt_BR>Transmita ou baixe vídeos do Youtube selecionando links em qualquer aplicativo ou navegador de internet</pt_BR>
   <pt>Ver ou transferir videos do Youtube, ao selecionar ligações em qualquer aplicação ou navegador de Internet</pt>
   <ro>Stream or download Youtube videos by selecting links in any app or browser</ro>
   <ru>Stream or download Youtube videos by selecting links in any app or browser</ru>
   <sk>Stream or download Youtube videos by selecting links in any app or browser</sk>
   <sl>Pretakanje ali prenos Youtube videov preko izbire povezav v poljubni aplikaciji ali brskalniku</sl>
   <so>Stream or download Youtube videos by selecting links in any app or browser</so>
   <sq>SHihni ose shkarkoni video Youtube duke përzgjedhur lidhje në çfarëdo aplikacioni ose shfletuesi</sq>
   <sr>Stream or download Youtube videos by selecting links in any app or browser</sr>
   <sv>Strömma eller ladda ner Youtube videos genom att välja länkar i alla appar eller webbläsare</sv>
   <th>Stream or download Youtube videos by selecting links in any app or browser</th>
   <tr>Herhangi bir uygulamada veya tarayıcıda bağlantıları seçerek Youtube videolarını izleyin veya indirin</tr>
   <uk>Stream or download Youtube videos by selecting links in any app or browser</uk>
   <vi>Stream or download Youtube videos by selecting links in any app or browser</vi>
   <zh_CN>Stream or download Youtube videos by selecting links in any app or browser</zh_CN>
   <zh_HK>Stream or download Youtube videos by selecting links in any app or browser</zh_HK>
   <zh_TW>Stream or download Youtube videos by selecting links in any app or browser</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
streamlight-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
streamlight-antix
</uninstall_package_names>
</app>
