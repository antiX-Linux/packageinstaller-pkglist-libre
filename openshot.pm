<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Video
</category>

<name>
Openshot
</name>

<description>
   <am>non-linear video editor</am>
   <ar>non-linear video editor</ar>
   <be>non-linear video editor</be>
   <bg>non-linear video editor</bg>
   <bn>non-linear video editor</bn>
   <ca>Editor de vídeo no lineal</ca>
   <cs>non-linear video editor</cs>
   <da>ikke-lineær videoredigering</da>
   <de>Nichtlinearer Videoeditor</de>
   <el>Επεξεργαστής βίντεο - μη γραμμικός</el>
   <en>non-linear video editor</en>
   <es_ES>Editor de video no lineal</es_ES>
   <es>Editor de video no lineal</es>
   <et>non-linear video editor</et>
   <eu>non-linear video editor</eu>
   <fa>non-linear video editor</fa>
   <fil_PH>non-linear video editor</fil_PH>
   <fi>ei-lineaarinen videonmuokkausohjelma</fi>
   <fr_BE>Éditeur vidéo non-linéaire</fr_BE>
   <fr>Éditeur vidéo non-linéaire</fr>
   <gl_ES>editor de vídeo non-lineal</gl_ES>
   <gu>non-linear video editor</gu>
   <he_IL>non-linear video editor</he_IL>
   <hi>अरेखीय वीडियो संपादक</hi>
   <hr>non-linear video editor</hr>
   <hu>non-linear video editor</hu>
   <id>non-linear video editor</id>
   <is>non-linear video editor</is>
   <it>editor video non lineare</it>
   <ja>ノンリニア・ビデオエディター</ja>
   <kk>non-linear video editor</kk>
   <ko>non-linear video editor</ko>
   <ku>non-linear video editor</ku>
   <lt>non-linear video editor</lt>
   <mk>non-linear video editor</mk>
   <mr>non-linear video editor</mr>
   <nb_NO>non-linear video editor</nb_NO>
   <nb>ikkelineær videoredigering</nb>
   <nl_BE>non-linear video editor</nl_BE>
   <nl>niet-lineaire video-editor</nl>
   <or>non-linear video editor</or>
   <pl>nieliniowy edytor wideo</pl>
   <pt_BR>Editor de vídeo não linear</pt_BR>
   <pt>Editor de vídeo não-linear</pt>
   <ro>non-linear video editor</ro>
   <ru>Нелинейный видеоредактор</ru>
   <sk>non-linear video editor</sk>
   <sl>Nelinearni video urejevalnik</sl>
   <so>non-linear video editor</so>
   <sq>Përpunues jolinear videosh</sq>
   <sr>non-linear video editor</sr>
   <sv>icke-linjär videoredigerare</sv>
   <th>โปรแกรมตัดต่อวิดีโอแบบ non-linear</th>
   <tr>Doğrusal olmayan video düzenleyici</tr>
   <uk>нелінійний відео редактор</uk>
   <vi>non-linear video editor</vi>
   <zh_CN>non-linear video editor</zh_CN>
   <zh_HK>non-linear video editor</zh_HK>
   <zh_TW>non-linear video editor</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
openshot-qt
openshot-qt-doc
frei0r-plugins
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
openshot-qt
openshot-qt-doc
frei0r-plugins
</uninstall_package_names>
</app>
