<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English_SA_LO_Latest_main
</name>

<description>
   <am>English_southafrican Language Meta-Package for LibreOffice</am>
   <ar>English_southafrican Language Meta-Package for LibreOffice</ar>
   <be>English_southafrican Language Meta-Package for LibreOffice</be>
   <bg>English_southafrican Language Meta-Package for LibreOffice</bg>
   <bn>English_southafrican Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'Angés Sudafricà per LibreOffice</ca>
   <cs>English_southafrican Language Meta-Package for LibreOffice</cs>
   <da>English_southafrican Language Meta-Package for LibreOffice</da>
   <de>Südafrikanisches Englisch Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Αγγλικά-Νότιας Αφρικής</el>
   <en>English_southafrican Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma inglés Sudafricano para LibreOffice</es_ES>
   <es>Metapaquete de idioma Inglés_Sudafricano para LibreOffice</es>
   <et>English_southafrican Language Meta-Package for LibreOffice</et>
   <eu>English_southafrican Language Meta-Package for LibreOffice</eu>
   <fa>English_southafrican Language Meta-Package for LibreOffice</fa>
   <fil_PH>English_southafrican Language Meta-Package for LibreOffice</fil_PH>
   <fi>Englantilais_Etelä-Afrikkalainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de langue anglaise (Afrique du Sud) pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de langue anglaise (Afrique du Sud) pour LibreOffice</fr>
   <gl_ES>Meta-paquete de Idioma para LibreOffice para o inglés surafricano</gl_ES>
   <gu>English_southafrican Language Meta-Package for LibreOffice</gu>
   <he_IL>English_southafrican Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु अंग्रेजी_दक्षिण अफ्रीका भाषा मेटा-पैकेज</hi>
   <hr>English_southafrican Language Meta-Package for LibreOffice</hr>
   <hu>English_southafrican Language Meta-Package for LibreOffice</hu>
   <id>English_southafrican Language Meta-Package for LibreOffice</id>
   <is>English_southafrican Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua anglo-sudafricana per LibreOffice</it>
   <ja>LibreOffice用の南アフリカ英語版メタパッケージ</ja>
   <kk>English_southafrican Language Meta-Package for LibreOffice</kk>
   <ko>English_southafrican Language Meta-Package for LibreOffice</ko>
   <ku>English_southafrican Language Meta-Package for LibreOffice</ku>
   <lt>English_southafrican Language Meta-Package for LibreOffice</lt>
   <mk>English_southafrican Language Meta-Package for LibreOffice</mk>
   <mr>English_southafrican Language Meta-Package for LibreOffice</mr>
   <nb_NO>English_southafrican Language Meta-Package for LibreOffice</nb_NO>
   <nb>Sørafrikansk engelsk språkpakke for LibreOffice</nb>
   <nl_BE>English_southafrican Language Meta-Package for LibreOffice</nl_BE>
   <nl>Engels-Zuid Afrikaanse Taal Meta-Pakket voor LibreOffice</nl>
   <or>English_southafrican Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka angielskiego-południowoafrykańskiego dla LibreOffice</pl>
   <pt_BR>Inglês da África do Sul Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Inglês SA Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>English_southafrican Language Meta-Package for LibreOffice</ro>
   <ru>English_southafrican Language Meta-Package for LibreOffice</ru>
   <sk>English_southafrican Language Meta-Package for LibreOffice</sk>
   <sl>Angleški južnoafriški jezikovni meta-paket za LibreOffice</sl>
   <so>English_southafrican Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në anglishte afrikanojugore</sq>
   <sr>English_southafrican Language Meta-Package for LibreOffice</sr>
   <sv>Engelska_sydafrikansk Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา English_southafrican สำหรับ LibreOffice</th>
   <tr>LibreOffice için Güney Afrika İngilizcesi Dili Üst-Paketi</tr>
   <uk>English_southafrican Language Meta-Package for LibreOffice</uk>
   <vi>English_southafrican Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 南非英语语言包</zh_CN>
   <zh_HK>English_southafrican Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>English_southafrican Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-l10n-en-za
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-l10n-en-za
</uninstall_package_names>

</app>
