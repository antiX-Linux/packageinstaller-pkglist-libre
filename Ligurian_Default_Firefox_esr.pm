<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Ligurian_Default_Firefox_esr
</name>

<description>
   <am>Ligurian localisation of Firefox ESR</am>
   <ar>Ligurian localisation of Firefox ESR</ar>
   <be>Ligurian localisation of Firefox ESR</be>
   <bg>Ligurian localisation of Firefox ESR</bg>
   <bn>Ligurian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Ligurià</ca>
   <cs>Ligurian localisation of Firefox ESR</cs>
   <da>Ligurian localisation of Firefox ESR</da>
   <de>Italienische Lokalisation für die ligurische Sprache von “Firefox ESR”</de>
   <el>Ligurian για Firefox ESR</el>
   <en>Ligurian localisation of Firefox ESR</en>
   <es_ES>Localización Ligur de Firefox ESR</es_ES>
   <es>Localización Ligur de Firefox ESR</es>
   <et>Ligurian localisation of Firefox ESR</et>
   <eu>Ligurian localisation of Firefox ESR</eu>
   <fa>Ligurian localisation of Firefox ESR</fa>
   <fil_PH>Ligurian localisation of Firefox ESR</fil_PH>
   <fi>Ligurian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en ligurien pour Firefox ESR</fr_BE>
   <fr>Localisation en ligurien pour Firefox ESR</fr>
   <gl_ES>Ligurian localisation of Firefox ESR</gl_ES>
   <gu>Ligurian localisation of Firefox ESR</gu>
   <he_IL>Ligurian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का लीगुरियाई संस्करण</hi>
   <hr>Ligurian localisation of Firefox ESR</hr>
   <hu>Ligurian localisation of Firefox ESR</hu>
   <id>Ligurian localisation of Firefox ESR</id>
   <is>Ligurian localisation of Firefox ESR</is>
   <it>Localizzazione ligure di Firefox ESR</it>
   <ja>リグリア語版 Firefox ESR</ja>
   <kk>Ligurian localisation of Firefox ESR</kk>
   <ko>Ligurian localisation of Firefox ESR</ko>
   <ku>Ligurian localisation of Firefox ESR</ku>
   <lt>Ligurian localisation of Firefox ESR</lt>
   <mk>Ligurian localisation of Firefox ESR</mk>
   <mr>Ligurian localisation of Firefox ESR</mr>
   <nb_NO>Ligurian localisation of Firefox ESR</nb_NO>
   <nb>Ligursk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Ligurian localisation of Firefox ESR</nl_BE>
   <nl>Ligurian localisation of Firefox ESR</nl>
   <or>Ligurian localisation of Firefox ESR</or>
   <pl>Ligurian localisation of Firefox ESR</pl>
   <pt_BR>Ligúria Localização para o Firefox ESR</pt_BR>
   <pt>Ligúrio Localização para Firefox ESR</pt>
   <ro>Ligurian localisation of Firefox ESR</ro>
   <ru>Ligurian localisation of Firefox ESR</ru>
   <sk>Ligurian localisation of Firefox ESR</sk>
   <sl>Ligurijske krajevne nastavitve za Firefox ESR</sl>
   <so>Ligurian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në ligure</sq>
   <sr>Ligurian localisation of Firefox ESR</sr>
   <sv>Ligurisk lokalisering av Firefox ESR</sv>
   <th>Ligurian localisation of Firefox ESR</th>
   <tr>Firefox ESR Cenevizce yerelleştirmesi</tr>
   <uk>Ligurian localisation of Firefox ESR</uk>
   <vi>Ligurian localisation of Firefox ESR</vi>
   <zh_CN>Ligurian localisation of Firefox ESR</zh_CN>
   <zh_HK>Ligurian localisation of Firefox ESR</zh_HK>
   <zh_TW>Ligurian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-esr-l10n-lij
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-lij
</uninstall_package_names>

</app>
