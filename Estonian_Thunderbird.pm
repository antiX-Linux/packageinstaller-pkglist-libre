<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Estonian_Thunderbird
</name>

<description>
   <am>Estonian localisation of Thunderbird</am>
   <ar>Estonian localisation of Thunderbird</ar>
   <be>Estonian localisation of Thunderbird</be>
   <bg>Estonian localisation of Thunderbird</bg>
   <bn>Estonian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Estonià</ca>
   <cs>Estonian localisation of Thunderbird</cs>
   <da>Estisk oversættelse af Thunderbird</da>
   <de>Estnische Lokalisierung von Thunderbird</de>
   <el>Εσθονικά για το Thunderbird</el>
   <en>Estonian localisation of Thunderbird</en>
   <es_ES>Localización Estonia de Thunderbird</es_ES>
   <es>Localización Estonio de Thunderbird</es>
   <et>Estonian localisation of Thunderbird</et>
   <eu>Estonian localisation of Thunderbird</eu>
   <fa>Estonian localisation of Thunderbird</fa>
   <fil_PH>Estonian localisation of Thunderbird</fil_PH>
   <fi>Eestiläinen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en estonien pour Thunderbird</fr_BE>
   <fr>Localisation en estonien pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao estonio</gl_ES>
   <gu>Estonian localisation of Thunderbird</gu>
   <he_IL>Estonian localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का एस्टोनियाई संस्करण</hi>
   <hr>Estonian localisation of Thunderbird</hr>
   <hu>Estonian localisation of Thunderbird</hu>
   <id>Estonian localisation of Thunderbird</id>
   <is>Estonian localisation of Thunderbird</is>
   <it>Localizzazione estone di Thunderbird</it>
   <ja>Thunderbird のエストニア語版</ja>
   <kk>Estonian localisation of Thunderbird</kk>
   <ko>Estonian localisation of Thunderbird</ko>
   <ku>Estonian localisation of Thunderbird</ku>
   <lt>Estonian localisation of Thunderbird</lt>
   <mk>Estonian localisation of Thunderbird</mk>
   <mr>Estonian localisation of Thunderbird</mr>
   <nb_NO>Estonian localisation of Thunderbird</nb_NO>
   <nb>Estisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Estonian localisation of Thunderbird</nl_BE>
   <nl>Estische lokalisatie van Thunderbird</nl>
   <or>Estonian localisation of Thunderbird</or>
   <pl>Estońska lokalizacja Thunderbirda</pl>
   <pt_BR>Estónio Localização para o Thunderbird</pt_BR>
   <pt>Estónio Localização para Thunderbird</pt>
   <ro>Estonian localisation of Thunderbird</ro>
   <ru>Estonian localisation of Thunderbird</ru>
   <sk>Estonian localisation of Thunderbird</sk>
   <sl>Estonska lokalizacjja za Thunderbird</sl>
   <so>Estonian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në estonisht</sq>
   <sr>Estonian localisation of Thunderbird</sr>
   <sv>Estnisk lokalisering av Thunderbird</sv>
   <th>Estonian localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Estonya dili yerelleştirmesi</tr>
   <uk>Estonian локалізація Thunderbird</uk>
   <vi>Estonian localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 爱沙尼亚语语言包</zh_CN>
   <zh_HK>Estonian localisation of Thunderbird</zh_HK>
   <zh_TW>Estonian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-et
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-et
</uninstall_package_names>

</app>
