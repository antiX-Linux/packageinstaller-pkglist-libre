<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Danish_Thunderbird
</name>

<description>
   <am>Danish localisation of Thunderbird</am>
   <ar>Danish localisation of Thunderbird</ar>
   <be>Danish localisation of Thunderbird</be>
   <bg>Danish localisation of Thunderbird</bg>
   <bn>Danish localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Danès</ca>
   <cs>Danish localisation of Thunderbird</cs>
   <da>Dansk oversættelse af Thunderbird</da>
   <de>Dänische Lokalisierung von Thunderbird</de>
   <el>Δανέζικα για το Thunderbird</el>
   <en>Danish localisation of Thunderbird</en>
   <es_ES>Localización Danesa de Thunderbird</es_ES>
   <es>Localización Danés de Thunderbird</es>
   <et>Danish localisation of Thunderbird</et>
   <eu>Danish localisation of Thunderbird</eu>
   <fa>Danish localisation of Thunderbird</fa>
   <fil_PH>Danish localisation of Thunderbird</fil_PH>
   <fi>Tanskalainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en danois pour Thunderbird</fr_BE>
   <fr>Localisation en danois pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird para danés</gl_ES>
   <gu>Danish localisation of Thunderbird</gu>
   <he_IL>Danish localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का डेनिश संस्करण</hi>
   <hr>Danish localisation of Thunderbird</hr>
   <hu>Danish localisation of Thunderbird</hu>
   <id>Danish localisation of Thunderbird</id>
   <is>Danish localisation of Thunderbird</is>
   <it>Localizzazione danese di Thunderbird</it>
   <ja>Thunderbird のデンマーク語版</ja>
   <kk>Danish localisation of Thunderbird</kk>
   <ko>Danish localisation of Thunderbird</ko>
   <ku>Danish localisation of Thunderbird</ku>
   <lt>Danish localisation of Thunderbird</lt>
   <mk>Danish localisation of Thunderbird</mk>
   <mr>Danish localisation of Thunderbird</mr>
   <nb_NO>Danish localisation of Thunderbird</nb_NO>
   <nb>Dansk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Danish localisation of Thunderbird</nl_BE>
   <nl>Deense lokalisatie van Thunderbird</nl>
   <or>Danish localisation of Thunderbird</or>
   <pl>Duńska lokalizacja Thunderbirda</pl>
   <pt_BR>Dinamarquês Localização para o Thunderbird</pt_BR>
   <pt>Dinamarquês Localização para Thunderbird</pt>
   <ro>Danish localisation of Thunderbird</ro>
   <ru>Danish localisation of Thunderbird</ru>
   <sk>Danish localisation of Thunderbird</sk>
   <sl>Danske krajevne nastavitve za Thunderbird</sl>
   <so>Danish localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në danisht</sq>
   <sr>Danish localisation of Thunderbird</sr>
   <sv>Dansk lokalisering av Thunderbird</sv>
   <th>Danish localisation ของ Thunderbird</th>
   <tr>Thunderbird Danca yerelleştirmesi</tr>
   <uk>Danish локалізація Thunderbird</uk>
   <vi>Danish localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 丹麦语语言包</zh_CN>
   <zh_HK>Danish localisation of Thunderbird</zh_HK>
   <zh_TW>Danish localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-da
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-da
</uninstall_package_names>

</app>
