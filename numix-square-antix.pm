<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Icons
</category>

<name>
Numix Square antiX
</name>

<description>
   <am>Numix Square icons for antiX Linux</am>
   <ar>Numix Square icons for antiX Linux</ar>
   <be>Numix Square icons for antiX Linux</be>
   <bg>Numix Square icons for antiX Linux</bg>
   <bn>Numix Square icons for antiX Linux</bn>
   <ca>Icones Numix Square per antiX Linux</ca>
   <cs>Numix Square icons for antiX Linux</cs>
   <da>Numix Square icons for antiX Linux</da>
   <de>Piktogramme (Ugs.: “Icons”) für antiX im Stil “Numix Square”</de>
   <el>Numix Square εικονίδια για antiX Linux</el>
   <en>Numix Square icons for antiX Linux</en>
   <es_ES>Iconos Numix Square para antiX Linux</es_ES>
   <es>Iconos Numix Square para antiX Linux</es>
   <et>Numix Square icons for antiX Linux</et>
   <eu>Numix Square icons for antiX Linux</eu>
   <fa>Numix Square icons for antiX Linux</fa>
   <fil_PH>Numix Square icons for antiX Linux</fil_PH>
   <fi>Numix Square kuvakkeet antiX Linux:ille</fi>
   <fr_BE>Icônes pour antiX dans le style "Numix Square"</fr_BE>
   <fr>Icônes pour antiX dans le style "Numix Square"</fr>
   <gl_ES>Numix Square icons for antiX Linux</gl_ES>
   <gu>Numix Square icons for antiX Linux</gu>
   <he_IL>Numix Square icons for antiX Linux</he_IL>
   <hi>एंटी-एक्स लिनक्स हेतु Numix Square आइकन</hi>
   <hr>Numix Square icons for antiX Linux</hr>
   <hu>Numix Square icons for antiX Linux</hu>
   <id>Numix Square icons for antiX Linux</id>
   <is>Numix Square icons for antiX Linux</is>
   <it>Icone Numix Square per antiX Linux</it>
   <ja>antiX Linux用 Numix Square アイコン</ja>
   <kk>Numix Square icons for antiX Linux</kk>
   <ko>Numix Square icons for antiX Linux</ko>
   <ku>Numix Square icons for antiX Linux</ku>
   <lt>Numix Square icons for antiX Linux</lt>
   <mk>Numix Square icons for antiX Linux</mk>
   <mr>Numix Square icons for antiX Linux</mr>
   <nb_NO>Numix Square icons for antiX Linux</nb_NO>
   <nb>Numix Square-ikoner for antiX Linux</nb>
   <nl_BE>Numix Square icons for antiX Linux</nl_BE>
   <nl>Numix Square icons for antiX Linux</nl>
   <or>Numix Square icons for antiX Linux</or>
   <pl>Numix Square icons for antiX Linux</pl>
   <pt_BR>Ícones Numix Square para o antiX Linux</pt_BR>
   <pt>Ícones  Numix Squasre para o antiX Linux</pt>
   <ro>Numix Square icons for antiX Linux</ro>
   <ru>Numix Square icons for antiX Linux</ru>
   <sk>Numix Square icons for antiX Linux</sk>
   <sl>Numix kvadratne ikone za antiX Linux</sl>
   <so>Numix Square icons for antiX Linux</so>
   <sq>Ikona Numix Square për antiX Linux</sq>
   <sr>Numix Square icons for antiX Linux</sr>
   <sv>Numix Square ikoner för antiX Linux</sv>
   <th>Numix Square icons for antiX Linux</th>
   <tr>antiX Linux için Numix Square simgeleri</tr>
   <uk>Numix Square icons for antiX Linux</uk>
   <vi>Numix Square icons for antiX Linux</vi>
   <zh_CN>Numix Square icons for antiX Linux</zh_CN>
   <zh_HK>Numix Square icons for antiX Linux</zh_HK>
   <zh_TW>Numix Square icons for antiX Linux</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
numix-square-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
numix-square-antix
</uninstall_package_names>
</app>
