<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Fulah_Default_Firefox_esr
</name>

<description>
   <am>Fulah localisation of Firefox ESR</am>
   <ar>Fulah localisation of Firefox ESR</ar>
   <be>Fulah localisation of Firefox ESR</be>
   <bg>Fulah localisation of Firefox ESR</bg>
   <bn>Fulah localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Fulah</ca>
   <cs>Fulah localisation of Firefox ESR</cs>
   <da>Fulah localisation of Firefox ESR</da>
   <de>Afrikanische Lokalisation für die westatlantische Niger-Kongo-Sprache “Fulfulde” von “Firefox ESR”</de>
   <el>Fulah για Firefox ESR</el>
   <en>Fulah localisation of Firefox ESR</en>
   <es_ES>Localización Fulah de Firefox ESR</es_ES>
   <es>Localización Fulah de Firefox ESR</es>
   <et>Fulah localisation of Firefox ESR</et>
   <eu>Fulah localisation of Firefox ESR</eu>
   <fa>Fulah localisation of Firefox ESR</fa>
   <fil_PH>Fulah localisation of Firefox ESR</fil_PH>
   <fi>Fulah localisation of Firefox ESR</fi>
   <fr_BE>Localisation en Fulah pour Firefox ESR</fr_BE>
   <fr>Localisation en Fulah pour Firefox ESR</fr>
   <gl_ES>Fulah localisation of Firefox ESR</gl_ES>
   <gu>Fulah localisation of Firefox ESR</gu>
   <he_IL>Fulah localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का फुलाह संस्करण</hi>
   <hr>Fulah localisation of Firefox ESR</hr>
   <hu>Fulah localisation of Firefox ESR</hu>
   <id>Fulah localisation of Firefox ESR</id>
   <is>Fulah localisation of Firefox ESR</is>
   <it>Localizzazione in lingua fula di Firefox ESR</it>
   <ja>フラニ語版 Firefox ESR</ja>
   <kk>Fulah localisation of Firefox ESR</kk>
   <ko>Fulah localisation of Firefox ESR</ko>
   <ku>Fulah localisation of Firefox ESR</ku>
   <lt>Fulah localisation of Firefox ESR</lt>
   <mk>Fulah localisation of Firefox ESR</mk>
   <mr>Fulah localisation of Firefox ESR</mr>
   <nb_NO>Fulah localisation of Firefox ESR</nb_NO>
   <nb>Fulah lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Fulah localisation of Firefox ESR</nl_BE>
   <nl>Fulah lokalisatie van Firefox ESR</nl>
   <or>Fulah localisation of Firefox ESR</or>
   <pl>Fulah localisation of Firefox ESR</pl>
   <pt_BR>Fula Localização de Firefox ESR</pt_BR>
   <pt>Fula Localização para Firefox ESR</pt>
   <ro>Fulah localisation of Firefox ESR</ro>
   <ru>Fulah localisation of Firefox ESR</ru>
   <sk>Fulah localisation of Firefox ESR</sk>
   <sl>Fulske krajevne nastavitve za Firefox ESR</sl>
   <so>Fulah localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në fula</sq>
   <sr>Fulah localisation of Firefox ESR</sr>
   <sv>Fulah lokalisering av Firefox ESR</sv>
   <th>Fulah localisation of Firefox ESR</th>
   <tr>Firefox ESR Fulah yerelleştirmesi</tr>
   <uk>Fulah localisation of Firefox ESR</uk>
   <vi>Fulah localisation of Firefox ESR</vi>
   <zh_CN>Fulah localisation of Firefox ESR</zh_CN>
   <zh_HK>Fulah localisation of Firefox ESR</zh_HK>
   <zh_TW>Fulah localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ff
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ff
</uninstall_package_names>

</app>
