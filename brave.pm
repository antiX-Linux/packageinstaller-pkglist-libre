<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Brave
</name>

<description>
   <am>Latest Brave browser</am>
   <ar>Latest Brave browser</ar>
   <be>Latest Brave browser</be>
   <bg>Latest Brave browser</bg>
   <bn>Latest Brave browser</bn>
   <ca>Darrer navegador Brave</ca>
   <cs>Latest Brave browser</cs>
   <da>Latest Brave browser</da>
   <de>Brave Browser (neuste Version)</de>
   <el>Το τελευταίο πρόγραμμα περιήγησης Brave</el>
   <en>Latest Brave browser</en>
   <es_ES>Último navegador Brave</es_ES>
   <es>Último navegador Brave</es>
   <et>Latest Brave browser</et>
   <eu>Latest Brave browser</eu>
   <fa>Latest Brave browser</fa>
   <fil_PH>Latest Brave browser</fil_PH>
   <fi>Viimeisin Brave-selain</fi>
   <fr_BE>Dernière version du navigateur Brave</fr_BE>
   <fr>Dernière version du navigateur Brave</fr>
   <gl_ES>Navegador web Brave, última versión</gl_ES>
   <gu>Latest Brave browser</gu>
   <he_IL>דפדפן Brave העדכני</he_IL>
   <hi>नवीनतम ब्रेव ब्राउज़र</hi>
   <hr>Latest Brave browser</hr>
   <hu>Latest Brave browser</hu>
   <id>Latest Brave browser</id>
   <is>Latest Brave browser</is>
   <it>Browser Brave più recente</it>
   <ja>最新の Brave ブラウザー</ja>
   <kk>Latest Brave browser</kk>
   <ko>Latest Brave browser</ko>
   <ku>Latest Brave browser</ku>
   <lt>Latest Brave browser</lt>
   <mk>Latest Brave browser</mk>
   <mr>Latest Brave browser</mr>
   <nb_NO>Latest Brave browser</nb_NO>
   <nb>Nettleseren Brave, seneste utgivelse</nb>
   <nl_BE>Latest Brave browser</nl_BE>
   <nl>Meest recente Brave browser</nl>
   <or>Latest Brave browser</or>
   <pl>Najnowsza przeglądarka Brave</pl>
   <pt_BR>Brave - Versão mais recente do navegador de internet</pt_BR>
   <pt>Navegador web Brave, última versão</pt>
   <ro>Latest Brave browser</ro>
   <ru>Latest Brave browser</ru>
   <sk>Latest Brave browser</sk>
   <sl>Zadnja različica Brave brskalnika</sl>
   <so>Latest Brave browser</so>
   <sq>Shfletuesi më i ri Brave</sq>
   <sr>Latest Brave browser</sr>
   <sv>Senaste Brave Webbläsare</sv>
   <th>เบราว์เซอร์ Brave ล่าสุด</th>
   <tr>En son Brave tarayıcı</tr>
   <uk>Latest Brave browser</uk>
   <vi>Latest Brave browser</vi>
   <zh_CN>最新版 Brave 浏览器</zh_CN>
   <zh_HK>Latest Brave browser</zh_HK>
   <zh_TW>Latest Brave browser</zh_TW>
</description>

<installable>
64
</installable>

<screenshot> </screenshot>

<preinstall>
URL="https://brave-browser-apt-release.s3.brave.com"
ASC="${URL}/brave-core.asc"
TKR="/etc/apt/trusted.gpg.d/brave-browser-release.gpg"
SRC="/etc/apt/sources.list.d/brave-browser-release.list"
VER="$(sed -n  's/^VERSION_CODENAME=//p' /etc/os-release)"
case $(cat /etc/debian_version) in
8*) VER=jessie   ;;
9*) VER=stretch  ;;
10*) VER=bullseye   ;;
11*) VER=bullseye ;;
*) VER=bullseye ;;
esac

DEB="deb [arch=amd64] ${URL}/ $VER main"

curl -s $ASC | ( apt-key --keyring $TKR add - 2>/dev/null )
echo "$DEB" | tee $SRC
apt-get update

</preinstall>

<install_package_names>
brave-browser
</install_package_names>

<postinstall>
# fix sandbox

SYSCTL=/etc/sysctl.d/10-securized-yama-scope.conf
echo "kernel.yama.ptrace_scope = 1" > $SYSCTL
if [ -f $SYSCTL ]; then  sysctl --load $SYSCTL; fi

</postinstall>

<uninstall_package_names>
brave-browser
brave-keyring
</uninstall_package_names>

<postuninstall>

apt-get -y remove --purge brave-browser brave-browser
SRC=/etc/apt/sources.list.d/brave-browser-release.list
TKR="/etc/apt/trusted.gpg.d/brave-browser-release.gpg"
rm -f $SRC 2>/dev/null
rm -f $TKR 2>/dev/null
apt-get update
</postuninstall>

</app>
