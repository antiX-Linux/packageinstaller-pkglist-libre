<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
Droopy
</name>

<description>
   <am>Simple way to share files via web server</am>
   <ar>Simple way to share files via web server</ar>
   <be>Simple way to share files via web server</be>
   <bg>Simple way to share files via web server</bg>
   <bn>Simple way to share files via web server</bn>
   <ca>Forma simple de compartir fitxers mitjançant un servidor web</ca>
   <cs>Simple way to share files via web server</cs>
   <da>Simple way to share files via web server</da>
   <de>Einfache Methode zum Dateiaustausch über einen Netzwerkserver</de>
   <el>Απλός τρόπος για κοινή χρήση αρχείων μέσω διακομιστή ιστού</el>
   <en>Simple way to share files via web server</en>
   <es_ES>Manera sencilla de compartir archivos a través del servidor web</es_ES>
   <es>Manera sencilla de compartir archivos a través del servidor web</es>
   <et>Simple way to share files via web server</et>
   <eu>Simple way to share files via web server</eu>
   <fa>Simple way to share files via web server</fa>
   <fil_PH>Simple way to share files via web server</fil_PH>
   <fi>Helppo tapa jakaa tiedostoja webpalvelimen kautta</fi>
   <fr_BE>Un moyen simple de partager des fichiers via un serveur web</fr_BE>
   <fr>Un moyen simple de partager des fichiers via un serveur web</fr>
   <gl_ES>Simple way to share files via web server</gl_ES>
   <gu>Simple way to share files via web server</gu>
   <he_IL>Simple way to share files via web server</he_IL>
   <hi>वेब सर्वर द्वारा फाइल सहभाजन का सरल मार्ग</hi>
   <hr>Simple way to share files via web server</hr>
   <hu>Simple way to share files via web server</hu>
   <id>Simple way to share files via web server</id>
   <is>Simple way to share files via web server</is>
   <it>Un modo semplice per condividere file tramite web server</it>
   <ja>ウェブサーバを介したシンプルなファイル共有方法</ja>
   <kk>Simple way to share files via web server</kk>
   <ko>Simple way to share files via web server</ko>
   <ku>Simple way to share files via web server</ku>
   <lt>Simple way to share files via web server</lt>
   <mk>Simple way to share files via web server</mk>
   <mr>Simple way to share files via web server</mr>
   <nb_NO>Simple way to share files via web server</nb_NO>
   <nb>Enkel måte å dele filer via web-tjener</nb>
   <nl_BE>Simple way to share files via web server</nl_BE>
   <nl>Eenvoudige manier om bestanden te delen via de webserver</nl>
   <or>Simple way to share files via web server</or>
   <pl>Simple way to share files via web server</pl>
   <pt_BR>Forma simples de compartilhar arquivos via servidor de internet</pt_BR>
   <pt>Uma forma simples de partilhar ficheiros via servidor web</pt>
   <ro>Simple way to share files via web server</ro>
   <ru>Simple way to share files via web server</ru>
   <sk>Simple way to share files via web server</sk>
   <sl>Preprost način za souporabo datotek preko spletnega strežnika</sl>
   <so>Simple way to share files via web server</so>
   <sq>Rrugë e thjeshtë për të shkëmbyer kartela përmes shërbyesi</sq>
   <sr>Simple way to share files via web server</sr>
   <sv>Enkelt sätt att dela sidor via webserver</sv>
   <th>Simple way to share files via web server</th>
   <tr>Dosyaları web sunucusu üzerinden paylaşmanın basit yolu</tr>
   <uk>Simple way to share files via web server</uk>
   <vi>Simple way to share files via web server</vi>
   <zh_CN>Simple way to share files via web server</zh_CN>
   <zh_HK>Simple way to share files via web server</zh_HK>
   <zh_TW>Simple way to share files via web server</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
droopy
</install_package_names>

<uninstall_package_names>
droopy
</uninstall_package_names>
</app>
