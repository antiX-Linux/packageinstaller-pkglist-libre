<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Development
</category>

<name>
Geany
</name>

<description>
   <am>fast and lightweight IDE and text editor</am>
   <ar>fast and lightweight IDE and text editor</ar>
   <be>fast and lightweight IDE and text editor</be>
   <bg>fast and lightweight IDE and text editor</bg>
   <bn>fast and lightweight IDE and text editor</bn>
   <ca>IDE i editor de text ràpid i lleuger</ca>
   <cs>fast and lightweight IDE and text editor</cs>
   <da>hurtig og letvægts IDE og tekstredigering</da>
   <de>Schneller und schlanker Texteditor und IDE</de>
   <el>Γρήγορο και ελαφρύ πρόγραμμα IDE επεξεργασίας κειμένου</el>
   <en>fast and lightweight IDE and text editor</en>
   <es_ES>Editor de textos rápido y liviano (IDE)</es_ES>
   <es>Editor de textos rápido y liviano (IDE)</es>
   <et>fast and lightweight IDE and text editor</et>
   <eu>fast and lightweight IDE and text editor</eu>
   <fa>fast and lightweight IDE and text editor</fa>
   <fil_PH>fast and lightweight IDE and text editor</fil_PH>
   <fi>nopea ja keveä ohjelmointiympäristö ja tekstinmuokkain</fi>
   <fr_BE>Éditeur de texte IDE rapide et léger</fr_BE>
   <fr>Éditeur de texte IDE rapide et léger</fr>
   <gl_ES>IDE e editor de texto, lixeiro e rápido</gl_ES>
   <gu>fast and lightweight IDE and text editor</gu>
   <he_IL>fast and lightweight IDE and text editor</he_IL>
   <hi>तीव्र व सरल आईडीई व टेक्स्ट संपादक</hi>
   <hr>fast and lightweight IDE and text editor</hr>
   <hu>fast and lightweight IDE and text editor</hu>
   <id>fast and lightweight IDE and text editor</id>
   <is>fast and lightweight IDE and text editor</is>
   <it>Editor di testo, ed Ambiente di Sviluppo Integrato, veloce e leggero</it>
   <ja>高速で軽量な IDE（統合開発環境）とテキストエディタ</ja>
   <kk>fast and lightweight IDE and text editor</kk>
   <ko>fast and lightweight IDE and text editor</ko>
   <ku>fast and lightweight IDE and text editor</ku>
   <lt>fast and lightweight IDE and text editor</lt>
   <mk>fast and lightweight IDE and text editor</mk>
   <mr>fast and lightweight IDE and text editor</mr>
   <nb_NO>fast and lightweight IDE and text editor</nb_NO>
   <nb>rask og lettvektig IDE og tekstredigeringsprogram</nb>
   <nl_BE>fast and lightweight IDE and text editor</nl_BE>
   <nl>snelle en lichtgewicht IDE en tekstverwerker</nl>
   <or>fast and lightweight IDE and text editor</or>
   <pl>szybkie i lekkie środowisko programistyczne oraz edytor tekstu</pl>
   <pt_BR>IDE (Integrated Development Environment) e editor de texto, leve e rápido</pt_BR>
   <pt>IDE (Integrated Development Environment) e editor de texto, ligeiro e rápido</pt>
   <ro>fast and lightweight IDE and text editor</ro>
   <ru>Быстрая и легковесная IDE и текстовый редактор</ru>
   <sk>fast and lightweight IDE and text editor</sk>
   <sl>lahek in hiter IDE urejevalnik teksta</sl>
   <so>fast and lightweight IDE and text editor</so>
   <sq>Përkthimi i Thunderbird-it në galicisht</sq>
   <sr>fast and lightweight IDE and text editor</sr>
   <sv>snabb lättvikts IDE och textredigerare</sv>
   <th>fast and lightweight IDE and text editor</th>
   <tr>hızlı, hafif IDE ve metin düzenleyici</tr>
   <uk>швидкий і легкий текстовий редактор та IDE</uk>
   <vi>fast and lightweight IDE and text editor</vi>
   <zh_CN>快速轻量的 IDE 和文本编辑器</zh_CN>
   <zh_HK>fast and lightweight IDE and text editor</zh_HK>
   <zh_TW>fast and lightweight IDE and text editor</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>http://screenshots.debian.net/screenshots/000/010/462/large.png</screenshot>

<preinstall>

</preinstall>

<install_package_names>
geany
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
geany
</uninstall_package_names>
</app>
