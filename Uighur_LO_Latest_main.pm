<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Uighur_LO_Latest_main
</name>

<description>
   <am>Uighur Language Meta-Package for LibreOffice</am>
   <ar>Uighur Language Meta-Package for LibreOffice</ar>
   <be>Uighur Language Meta-Package for LibreOffice</be>
   <bg>Uighur Language Meta-Package for LibreOffice</bg>
   <bn>Uighur Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Uigur per LibreOffice</ca>
   <cs>Uighur Language Meta-Package for LibreOffice</cs>
   <da>Uighur Language Meta-Package for LibreOffice</da>
   <de>Uighurisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Uighur</el>
   <en>Uighur Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete idioma Uighur para LibreOffice</es_ES>
   <es>Metapaquete idioma Uighur para LibreOffice</es>
   <et>Uighur Language Meta-Package for LibreOffice</et>
   <eu>Uighur Language Meta-Package for LibreOffice</eu>
   <fa>Uighur Language Meta-Package for LibreOffice</fa>
   <fil_PH>Uighur Language Meta-Package for LibreOffice</fil_PH>
   <fi>Uiguurinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue ouïgoure pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue ouïgoure pour LibreOffice</fr>
   <gl_ES>Uigur Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Uighur Language Meta-Package for LibreOffice</gu>
   <he_IL>Uighur Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु उइगुर भाषा मेटा-पैकेज</hi>
   <hr>Uighur Language Meta-Package for LibreOffice</hr>
   <hu>Uighur Language Meta-Package for LibreOffice</hu>
   <id>Uighur Language Meta-Package for LibreOffice</id>
   <is>Uighur Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua uigura per LibreOffice</it>
   <ja>LibreOffice用 ウイグル語メタパッケージ</ja>
   <kk>Uighur Language Meta-Package for LibreOffice</kk>
   <ko>Uighur Language Meta-Package for LibreOffice</ko>
   <ku>Uighur Language Meta-Package for LibreOffice</ku>
   <lt>Uighur Language Meta-Package for LibreOffice</lt>
   <mk>Uighur Language Meta-Package for LibreOffice</mk>
   <mr>Uighur Language Meta-Package for LibreOffice</mr>
   <nb_NO>Uighur Language Meta-Package for LibreOffice</nb_NO>
   <nb>Uighur språkpakke for LibreOffice</nb>
   <nl_BE>Uighur Language Meta-Package for LibreOffice</nl_BE>
   <nl>Uighur Taal Meta-Pakket voor LibreOffice</nl>
   <or>Uighur Language Meta-Package for LibreOffice</or>
   <pl>Ujgurski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Uigur Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Uigur Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Uighur Language Meta-Package for LibreOffice</ro>
   <ru>Uighur Language Meta-Package for LibreOffice</ru>
   <sk>Uighur Language Meta-Package for LibreOffice</sk>
   <sl>Ujgurski jezikovni meta-paket za LibreOffice</sl>
   <so>Uighur Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në ujgure</sq>
   <sr>Uighur Language Meta-Package for LibreOffice</sr>
   <sv>Uighur Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Uighur สำหรับ LibreOffice</th>
   <tr>LibreOffice için Uygurca Dili Üst-Paketi</tr>
   <uk>Uighur Language Meta-Package for LibreOffice</uk>
   <vi>Uighur Language Meta-Package for LibreOffice</vi>
   <zh_CN>Uighur Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Uighur Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Uighur Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ug
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ug
libreoffice-gtk3
</uninstall_package_names>

</app>
