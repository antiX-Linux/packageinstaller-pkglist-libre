<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Belarusian_Default_Firefox_esr
</name>

<description>
   <am>Belarusian localisation of Firefox ESR</am>
   <ar>Belarusian localisation of Firefox ESR</ar>
   <be>Belarusian localisation of Firefox ESR</be>
   <bg>Belarusian localisation of Firefox ESR</bg>
   <bn>Belarusian localisation of Firefox ESR</bn>
   <ca>Localització en Bielorús de Firefox ESR</ca>
   <cs>Belarusian localisation of Firefox ESR</cs>
   <da>Belarusian localisation of Firefox ESR</da>
   <de>Belarusian localisation of Firefox ESR</de>
   <el>Λευκορώσικα για το Firefox ESR</el>
   <en>Belarusian localisation of Firefox ESR</en>
   <es_ES>Localización bielorrusa de Firefox ESR</es_ES>
   <es>Localización bielorrusa de Firefox ESR</es>
   <et>Belarusian localisation of Firefox ESR</et>
   <eu>Belarusian localisation of Firefox ESR</eu>
   <fa>Belarusian localisation of Firefox ESR</fa>
   <fil_PH>Belarusian localisation of Firefox ESR</fil_PH>
   <fi>Belarusian localisation of Firefox ESR</fi>
   <fr_BE>Localisation biélorusse pour Firefox ESR</fr_BE>
   <fr>Localisation biélorusse pour Firefox ESR</fr>
   <gl_ES>Belarusian localisation of Firefox ESR</gl_ES>
   <gu>Belarusian localisation of Firefox ESR</gu>
   <he_IL>Belarusian localisation of Firefox ESR</he_IL>
   <hi>Belarusian localisation of Firefox ESR</hi>
   <hr>Belarusian localisation of Firefox ESR</hr>
   <hu>Belarusian localisation of Firefox ESR</hu>
   <id>Belarusian localisation of Firefox ESR</id>
   <is>Belarusian localisation of Firefox ESR</is>
   <it>Localizzazione Belarusian di Firefox ESR</it>
   <ja>Belarusian localisation of Firefox ESR</ja>
   <kk>Belarusian localisation of Firefox ESR</kk>
   <ko>Belarusian localisation of Firefox ESR</ko>
   <ku>Belarusian localisation of Firefox ESR</ku>
   <lt>Belarusian localisation of Firefox ESR</lt>
   <mk>Belarusian localisation of Firefox ESR</mk>
   <mr>Belarusian localisation of Firefox ESR</mr>
   <nb_NO>Belarusian localisation of Firefox ESR</nb_NO>
   <nb>Hviterussisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Belarusian localisation of Firefox ESR</nl_BE>
   <nl>Wit-Russische lokalisatie van Firefox ESR</nl>
   <or>Belarusian localisation of Firefox ESR</or>
   <pl>Belarusian localisation of Firefox ESR</pl>
   <pt_BR>Bielorrusso Localização para o Firefox ESR</pt_BR>
   <pt>Bielorrusso Localização para Firefox ESR</pt>
   <ro>Belarusian localisation of Firefox ESR</ro>
   <ru>Belarusian localisation of Firefox ESR</ru>
   <sk>Belarusian localisation of Firefox ESR</sk>
   <sl>Beloruske krajevne nastavitve za Firefox ESR</sl>
   <so>Belarusian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në bjellorusisht</sq>
   <sr>Belarusian localisation of Firefox ESR</sr>
   <sv>Belarus lokalisering av Firefox ESR</sv>
   <th>Belarusian localisation of Firefox ESR</th>
   <tr>Belarusian localisation of Firefox ESR</tr>
   <uk>Belarusian localisation of Firefox ESR</uk>
   <vi>Belarusian localisation of Firefox ESR</vi>
   <zh_CN>Belarusian localisation of Firefox ESR</zh_CN>
   <zh_HK>Belarusian localisation of Firefox ESR</zh_HK>
   <zh_TW>Belarusian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-be
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-be
</uninstall_package_names>

</app>
