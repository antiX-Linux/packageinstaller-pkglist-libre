<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Serbian_Thunderbird
</name>

<description>
   <am>Serbian localisation of Thunderbird</am>
   <ar>Serbian localisation of Thunderbird</ar>
   <be>Serbian localisation of Thunderbird</be>
   <bg>Serbian localisation of Thunderbird</bg>
   <bn>Serbian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Serbi</ca>
   <cs>Serbian localisation of Thunderbird</cs>
   <da>Serbisk oversættelse af Thunderbird</da>
   <de>Serbische Lokalisierung von Thunderbird</de>
   <el>Σερβικά για το Thunderbird</el>
   <en>Serbian localisation of Thunderbird</en>
   <es_ES>Localización Serbia de Thunderbird</es_ES>
   <es>Localización Serbio de Thunderbird</es>
   <et>Serbian localisation of Thunderbird</et>
   <eu>Serbian localisation of Thunderbird</eu>
   <fa>Serbian localisation of Thunderbird</fa>
   <fil_PH>Serbian localisation of Thunderbird</fil_PH>
   <fi>Serbialainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en serbe pour Thunderbird</fr_BE>
   <fr>Localisation en serbe pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao serbio</gl_ES>
   <gu>Serbian localisation of Thunderbird</gu>
   <he_IL>Serbian localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का सर्बियाई संस्करण</hi>
   <hr>Serbian localisation of Thunderbird</hr>
   <hu>Serbian localisation of Thunderbird</hu>
   <id>Serbian localisation of Thunderbird</id>
   <is>Serbian localisation of Thunderbird</is>
   <it>Localizzazione serba di Thunderbird</it>
   <ja>Thunderbird のセルビア語版</ja>
   <kk>Serbian localisation of Thunderbird</kk>
   <ko>Serbian localisation of Thunderbird</ko>
   <ku>Serbian localisation of Thunderbird</ku>
   <lt>Serbian localisation of Thunderbird</lt>
   <mk>Serbian localisation of Thunderbird</mk>
   <mr>Serbian localisation of Thunderbird</mr>
   <nb_NO>Serbian localisation of Thunderbird</nb_NO>
   <nb>Serbisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Serbian localisation of Thunderbird</nl_BE>
   <nl>Servische lokalisatie van Thunderbird</nl>
   <or>Serbian localisation of Thunderbird</or>
   <pl>Serbska lokalizacja Thunderbirda</pl>
   <pt_BR>Sérvio Localização para o Thunderbird</pt_BR>
   <pt>Sérvio Localização para Thunderbird</pt>
   <ro>Serbian localisation of Thunderbird</ro>
   <ru>Serbian localisation of Thunderbird</ru>
   <sk>Serbian localisation of Thunderbird</sk>
   <sl>Srbske krajevne nastavitve za Thunderbird</sl>
   <so>Serbian localisation of Thunderbird</so>
   <sq>Përkthim në serbisht i Thunderbird-it</sq>
   <sr>Serbian localisation of Thunderbird</sr>
   <sv>Serbisk lokalisering av Thunderbird </sv>
   <th>Serbian localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Sırpça yerelleştirmesi</tr>
   <uk>Serbian localisation of Thunderbird</uk>
   <vi>Serbian localisation of Thunderbird</vi>
   <zh_CN>Serbian localisation of Thunderbird</zh_CN>
   <zh_HK>Serbian localisation of Thunderbird</zh_HK>
   <zh_TW>Serbian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-sr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-sr
</uninstall_package_names>

</app>
