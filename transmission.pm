<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Torrent
</category>

<name>
Transmission
</name>

<description>
   <am>Lightweight BitTorrent client</am>
   <ar>Lightweight BitTorrent client</ar>
   <be>Lightweight BitTorrent client</be>
   <bg>Lightweight BitTorrent client</bg>
   <bn>Lightweight BitTorrent client</bn>
   <ca>Client lleuger de BitTorrent</ca>
   <cs>Lightweight BitTorrent client</cs>
   <da>Lightweight BitTorrent client</da>
   <de>Einfacher Client für das Dateiaustauschprogramm “BitTorrent”</de>
   <el>Ελαφρύς πελάτης BitTorrent</el>
   <en>Lightweight BitTorrent client</en>
   <es_ES>Cliente ligero BitTorrent</es_ES>
   <es>Cliente ligero BitTorrent</es>
   <et>Lightweight BitTorrent client</et>
   <eu>Lightweight BitTorrent client</eu>
   <fa>Lightweight BitTorrent client</fa>
   <fil_PH>Lightweight BitTorrent client</fil_PH>
   <fi>Kevyt BitTorrent asiakasohjelma</fi>
   <fr_BE>Client BitTorrent léger</fr_BE>
   <fr>Client BitTorrent léger</fr>
   <gl_ES>Lightweight BitTorrent client</gl_ES>
   <gu>Lightweight BitTorrent client</gu>
   <he_IL>Lightweight BitTorrent client</he_IL>
   <hi>सरल बिट-टोरेंट साधन</hi>
   <hr>Lightweight BitTorrent client</hr>
   <hu>Lightweight BitTorrent client</hu>
   <id>Lightweight BitTorrent client</id>
   <is>Lightweight BitTorrent client</is>
   <it>Cliente BitTorrent leggero</it>
   <ja>軽量な BitTorrentクライアント</ja>
   <kk>Lightweight BitTorrent client</kk>
   <ko>Lightweight BitTorrent client</ko>
   <ku>Lightweight BitTorrent client</ku>
   <lt>Lightweight BitTorrent client</lt>
   <mk>Lightweight BitTorrent client</mk>
   <mr>Lightweight BitTorrent client</mr>
   <nb_NO>Lightweight BitTorrent client</nb_NO>
   <nb>Lettvektig BitTorrent-klient</nb>
   <nl_BE>Lightweight BitTorrent client</nl_BE>
   <nl>Lightweight BitTorrent client</nl>
   <or>Lightweight BitTorrent client</or>
   <pl>Lightweight BitTorrent client</pl>
   <pt_BR>Cliente BitTorrent leve</pt_BR>
   <pt>Cliente de BitTorrent ligeiro</pt>
   <ro>Lightweight BitTorrent client</ro>
   <ru>Lightweight BitTorrent client</ru>
   <sk>Lightweight BitTorrent client</sk>
   <sl>Lahek odjemalec za BitTorrent</sl>
   <so>Lightweight BitTorrent client</so>
   <sq>Klient BitTorrent i peshës së lehtë</sq>
   <sr>Lightweight BitTorrent client</sr>
   <sv>Lättvikts BitTorrent klient</sv>
   <th>Lightweight BitTorrent client</th>
   <tr>Hafif BitTorrent istemcisi</tr>
   <uk>Lightweight BitTorrent client</uk>
   <vi>Lightweight BitTorrent client</vi>
   <zh_CN>Lightweight BitTorrent client</zh_CN>
   <zh_HK>Lightweight BitTorrent client</zh_HK>
   <zh_TW>Lightweight BitTorrent client</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
transmission
transmission-cli
transmission-gtk
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
transmission
transmission-cli
transmission-gtk
</uninstall_package_names>
</app>
