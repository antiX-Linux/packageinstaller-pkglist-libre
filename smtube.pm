<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Video
</category>

<name>
SMtube
</name>

<description>
   <am>search and download Youtube videos</am>
   <ar>search and download Youtube videos</ar>
   <be>search and download Youtube videos</be>
   <bg>search and download Youtube videos</bg>
   <bn>search and download Youtube videos</bn>
   <ca>Cerca i descarrega vídeos de Youtube</ca>
   <cs>search and download Youtube videos</cs>
   <da>søg efter og download Youtube-videoer</da>
   <de>YouTube-Videos finden und herunterladen</de>
   <el>Αναζήτηση και λήψη βίντεο από το Youtube</el>
   <en>search and download Youtube videos</en>
   <es_ES>Busque y descargue videos de Youtube</es_ES>
   <es>Busque y descargue videos de Youtube</es>
   <et>search and download Youtube videos</et>
   <eu>search and download Youtube videos</eu>
   <fa>search and download Youtube videos</fa>
   <fil_PH>search and download Youtube videos</fil_PH>
   <fi>etsi ja lataa Youtube-videoita</fi>
   <fr_BE>Chercher et télécharger des vidéos Youtube</fr_BE>
   <fr>Chercher et télécharger des vidéos Youtube</fr>
   <gl_ES>Pesquisar e descargar vídeos de Youtube</gl_ES>
   <gu>search and download Youtube videos</gu>
   <he_IL>search and download Youtube videos</he_IL>
   <hi>यूट्यूब वीडियो खोज व डाउनलोड</hi>
   <hr>search and download Youtube videos</hr>
   <hu>search and download Youtube videos</hu>
   <id>search and download Youtube videos</id>
   <is>search and download Youtube videos</is>
   <it>cerca e scarica video da Youtube</it>
   <ja>YouTube動画の検索とダウンロード</ja>
   <kk>search and download Youtube videos</kk>
   <ko>search and download Youtube videos</ko>
   <ku>search and download Youtube videos</ku>
   <lt>search and download Youtube videos</lt>
   <mk>search and download Youtube videos</mk>
   <mr>search and download Youtube videos</mr>
   <nb_NO>search and download Youtube videos</nb_NO>
   <nb>søk og last ned Youtube-videoer</nb>
   <nl_BE>search and download Youtube videos</nl_BE>
   <nl>zoek en download Youtube video's</nl>
   <or>search and download Youtube videos</or>
   <pl>wyszukuj i pobieraj filmy z YouTube</pl>
   <pt_BR>Pesquisar e baixar vídeos do Youtube</pt_BR>
   <pt>Pesquisar e descarregar vídeos do Youtube</pt>
   <ro>search and download Youtube videos</ro>
   <ru>Поиск и скачивание видео в Youtube</ru>
   <sk>search and download Youtube videos</sk>
   <sl>Išči in prenesivideo posnetke iz Youtube</sl>
   <so>search and download Youtube videos</so>
   <sq>Kërkoni dhe shkarkoni video nga Youtube</sq>
   <sr>search and download Youtube videos</sr>
   <sv>sök och ladda ner Youtube videos</sv>
   <th>ค้นหาและดาวน์โหลดวิดีโอจาก YouTube</th>
   <tr>Youtube videolarını arama ve indirme</tr>
   <uk>пошук та завантаження відео з Youtube</uk>
   <vi>search and download Youtube videos</vi>
   <zh_CN>search and download Youtube videos</zh_CN>
   <zh_HK>search and download Youtube videos</zh_HK>
   <zh_TW>search and download Youtube videos</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
smtube
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
smtube
</uninstall_package_names>
</app>
