<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Vietnamese
</name>

<description>
   <am>Vietnamese dictionary for hunspell</am>
   <ar>Vietnamese dictionary for hunspell</ar>
   <be>Vietnamese dictionary for hunspell</be>
   <bg>Vietnamese dictionary for hunspell</bg>
   <bn>Vietnamese dictionary for hunspell</bn>
   <ca>Diccionari Vietnamita per hunspell</ca>
   <cs>Vietnamese dictionary for hunspell</cs>
   <da>Vietnamese dictionary for hunspell</da>
   <de>Vietnamesisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Βιετνάμ για hunspell</el>
   <en>Vietnamese dictionary for hunspell</en>
   <es_ES>Diccionario Vietnamita para hunspell</es_ES>
   <es>Diccionario Vietnamita para hunspell</es>
   <et>Vietnamese dictionary for hunspell</et>
   <eu>Vietnamese dictionary for hunspell</eu>
   <fa>Vietnamese dictionary for hunspell</fa>
   <fil_PH>Vietnamese dictionary for hunspell</fil_PH>
   <fi>Vietnamese dictionary for hunspell</fi>
   <fr_BE>Vietnamien dictionnaire pour hunspell</fr_BE>
   <fr>Vietnamien dictionnaire pour hunspell</fr>
   <gl_ES>Vietnamese dictionary for hunspell</gl_ES>
   <gu>Vietnamese dictionary for hunspell</gu>
   <he_IL>Vietnamese dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु वियतनामी शब्दकोष</hi>
   <hr>Vietnamese dictionary for hunspell</hr>
   <hu>Vietnamese dictionary for hunspell</hu>
   <id>Vietnamese dictionary for hunspell</id>
   <is>Vietnamese dictionary for hunspell</is>
   <it>Dizionario vietnamese per hunspell</it>
   <ja>Hunspell 用ベトナム語辞書</ja>
   <kk>Vietnamese dictionary for hunspell</kk>
   <ko>Vietnamese dictionary for hunspell</ko>
   <ku>Vietnamese dictionary for hunspell</ku>
   <lt>Vietnamese dictionary for hunspell</lt>
   <mk>Vietnamese dictionary for hunspell</mk>
   <mr>Vietnamese dictionary for hunspell</mr>
   <nb_NO>Vietnamese dictionary for hunspell</nb_NO>
   <nb>Vietnamesisk ordliste for hunspell</nb>
   <nl_BE>Vietnamese dictionary for hunspell</nl_BE>
   <nl>Vietnamese dictionary for hunspell</nl>
   <or>Vietnamese dictionary for hunspell</or>
   <pl>Vietnamese dictionary for hunspell</pl>
   <pt_BR>Dicionário Vietnamita para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Vietnamita para hunspell</pt>
   <ro>Vietnamese dictionary for hunspell</ro>
   <ru>Vietnamese dictionary for hunspell</ru>
   <sk>Vietnamese dictionary for hunspell</sk>
   <sl>Vietnamski slovar za hunspell</sl>
   <so>Vietnamese dictionary for hunspell</so>
   <sq>Përkthimi i Thunderbird-it në vietnamisht</sq>
   <sr>Vietnamese dictionary for hunspell</sr>
   <sv>Vietnamesisk ordbok för hunspell</sv>
   <th>Vietnamese dictionary for hunspell</th>
   <tr>Hunspell için Vietnamca sözlük</tr>
   <uk>Vietnamese dictionary for hunspell</uk>
   <vi>Vietnamese dictionary for hunspell</vi>
   <zh_CN>Vietnamese dictionary for hunspell</zh_CN>
   <zh_HK>Vietnamese dictionary for hunspell</zh_HK>
   <zh_TW>Vietnamese dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-vi
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-vi
</uninstall_package_names>

</app>
