<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Swedish_Firefox
</name>

<description>
   <am>Swedish localisation of Firefox</am>
   <ar>Swedish localisation of Firefox</ar>
   <be>Swedish localisation of Firefox</be>
   <bg>Swedish localisation of Firefox</bg>
   <bn>Swedish localisation of Firefox</bn>
   <ca>Localització de Firefox en Suec</ca>
   <cs>Swedish localisation of Firefox</cs>
   <da>Svensk oversættelse af Firefox</da>
   <de>Schwedische Lokalisierung von Firefox</de>
   <el>Σουηδικά για το Firefox</el>
   <en>Swedish localisation of Firefox</en>
   <es_ES>Localización Sueca de Firefox</es_ES>
   <es>Localización Sueco de Firefox</es>
   <et>Swedish localisation of Firefox</et>
   <eu>Swedish localisation of Firefox</eu>
   <fa>Swedish localisation of Firefox</fa>
   <fil_PH>Swedish localisation of Firefox</fil_PH>
   <fi>Ruotsalainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en suédois pour Firefox</fr_BE>
   <fr>Localisation en suédois pour Firefox</fr>
   <gl_ES>Localización de Firefox ao sueco</gl_ES>
   <gu>Swedish localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לשוודית</he_IL>
   <hi>फायरफॉक्स का स्वीडिश संस्करण</hi>
   <hr>Swedish localisation of Firefox</hr>
   <hu>Swedish localisation of Firefox</hu>
   <id>Swedish localisation of Firefox</id>
   <is>Swedish localisation of Firefox</is>
   <it>Localizzazione svedese di Firefox</it>
   <ja>Firefox のスウェーデン語版</ja>
   <kk>Swedish localisation of Firefox</kk>
   <ko>Swedish localisation of Firefox</ko>
   <ku>Swedish localisation of Firefox</ku>
   <lt>Swedish localisation of Firefox</lt>
   <mk>Swedish localisation of Firefox</mk>
   <mr>Swedish localisation of Firefox</mr>
   <nb_NO>Swedish localisation of Firefox</nb_NO>
   <nb>Svensk lokaltilpassing av Firefox</nb>
   <nl_BE>Swedish localisation of Firefox</nl_BE>
   <nl>Zweedse lokalisatie van Firefox</nl>
   <or>Swedish localisation of Firefox</or>
   <pl>Szwedzka lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Sueco Localização para o Firefox</pt_BR>
   <pt>Sueco Localização para Firefox</pt>
   <ro>Swedish localisation of Firefox</ro>
   <ru>Шведская локализация Firefox</ru>
   <sk>Swedish localisation of Firefox</sk>
   <sl>Švedske krajevne nastavitve za Firefox</sl>
   <so>Swedish localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në suedisht</sq>
   <sr>Swedish localisation of Firefox</sr>
   <sv>Svensk lokalisering av Firefox</sv>
   <th>Swedish localisation ของ Firefox</th>
   <tr>Firefox'un İsveççe yerelleştirmesi</tr>
   <uk>Swedish localisation of Firefox</uk>
   <vi>Swedish localisation of Firefox</vi>
   <zh_CN>Swedish localisation of Firefox</zh_CN>
   <zh_HK>Swedish localisation of Firefox</zh_HK>
   <zh_TW>Swedish localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-sv-se
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-sv-se
</uninstall_package_names>
</app>
