<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Themes
</category>

<name>
Obsidian-2 Gtk Theme
</name>

<description>
   <am>a dark Gtk Theme</am>
   <ar>a dark Gtk Theme</ar>
   <be>a dark Gtk Theme</be>
   <bg>a dark Gtk Theme</bg>
   <bn>a dark Gtk Theme</bn>
   <ca>un tema GTK fosc</ca>
   <cs>a dark Gtk Theme</cs>
   <da>et mørkt Gtk-tema</da>
   <de>Ein dunkles Gtk-Thema</de>
   <el>Σκοτεινό θέμα Gtk</el>
   <en>a dark Gtk Theme</en>
   <es_ES>Tema GTK oscuro</es_ES>
   <es>Tema GTK oscuro</es>
   <et>a dark Gtk Theme</et>
   <eu>a dark Gtk Theme</eu>
   <fa>a dark Gtk Theme</fa>
   <fil_PH>a dark Gtk Theme</fil_PH>
   <fi>tumma Gtk ulkoasu</fi>
   <fr_BE>Thème sombre GTK</fr_BE>
   <fr>Thème sombre GTK</fr>
   <gl_ES>Tema escuro para Gtk</gl_ES>
   <gu>a dark Gtk Theme</gu>
   <he_IL>a dark Gtk Theme</he_IL>
   <hi>गहरे रंग की जीटीके थीम</hi>
   <hr>a dark Gtk Theme</hr>
   <hu>a dark Gtk Theme</hu>
   <id>a dark Gtk Theme</id>
   <is>a dark Gtk Theme</is>
   <it>un tema Gtk scuro</it>
   <ja>Gtk のダーク テーマ</ja>
   <kk>a dark Gtk Theme</kk>
   <ko>a dark Gtk Theme</ko>
   <ku>a dark Gtk Theme</ku>
   <lt>tamsi Gtk tema</lt>
   <mk>a dark Gtk Theme</mk>
   <mr>a dark Gtk Theme</mr>
   <nb_NO>a dark Gtk Theme</nb_NO>
   <nb>et mørkt Gtk-tema</nb>
   <nl_BE>a dark Gtk Theme</nl_BE>
   <nl>een donker Gtk thema</nl>
   <or>a dark Gtk Theme</or>
   <pl>ciemny motyw GTK</pl>
   <pt_BR>Tema escuro para Gtk</pt_BR>
   <pt>Tema escuro para Gtk</pt>
   <ro>a dark Gtk Theme</ro>
   <ru>Тёмная тема GTK</ru>
   <sk>a dark Gtk Theme</sk>
   <sl>Temna Gtk predloga</sl>
   <so>a dark Gtk Theme</so>
   <sq>Një temë Gtk e errët</sq>
   <sr>a dark Gtk Theme</sr>
   <sv>ett mörkt Gtk Tema</sv>
   <th>a dark Gtk Theme</th>
   <tr>Koyu bir Gtk teması</tr>
   <uk>темна Gtk-тема</uk>
   <vi>a dark Gtk Theme</vi>
   <zh_CN>a dark Gtk Theme</zh_CN>
   <zh_HK>a dark Gtk Theme</zh_HK>
   <zh_TW>a dark Gtk Theme</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
obsidian-2-gtk-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
obsidian-2-gtk-theme
</uninstall_package_names>
</app>
