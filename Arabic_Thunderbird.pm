<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Arabic_Thunderbird
</name>

<description>
   <am>Arabic localisation of Thunderbird</am>
   <ar>Arabic localisation of Thunderbird</ar>
   <be>Arabic localisation of Thunderbird</be>
   <bg>Arabic localisation of Thunderbird</bg>
   <bn>Arabic localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Àrab</ca>
   <cs>Arabic localisation of Thunderbird</cs>
   <da>Arabisk oversættelse af Thunderbird</da>
   <de>Arabische Lokalisierung von Thunderbird</de>
   <el>Αραβικά για το Thunderbird</el>
   <en>Arabic localisation of Thunderbird</en>
   <es_ES>Localización Arabe de Thunderbird</es_ES>
   <es>Localización Árabe de Thunderbird</es>
   <et>Arabic localisation of Thunderbird</et>
   <eu>Arabic localisation of Thunderbird</eu>
   <fa>Arabic localisation of Thunderbird</fa>
   <fil_PH>Arabic localisation of Thunderbird</fil_PH>
   <fi>Arabialainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation arabe pour Thunderbird</fr_BE>
   <fr>Localisation arabe pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird en árabe</gl_ES>
   <gu>Arabic localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לערבית</he_IL>
   <hi>थंडरबर्ड का अरबी संस्करण</hi>
   <hr>Arapska lokalizacija Thunderbirda</hr>
   <hu>Arabic localisation of Thunderbird</hu>
   <id>Arabic localisation of Thunderbird</id>
   <is>Arabic localisation of Thunderbird</is>
   <it>Localizzazione araba di Thunderbird</it>
   <ja>アラビア語版 Thunderbird</ja>
   <kk>Arabic localisation of Thunderbird</kk>
   <ko>Arabic localisation of Thunderbird</ko>
   <ku>Arabic localisation of Thunderbird</ku>
   <lt>Arabic localisation of Thunderbird</lt>
   <mk>Arabic localisation of Thunderbird</mk>
   <mr>Arabic localisation of Thunderbird</mr>
   <nb_NO>Arabic localisation of Thunderbird</nb_NO>
   <nb>Arabisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Arabic localisation of Thunderbird</nl_BE>
   <nl>Arabische lokalisatie van Thunderbird</nl>
   <or>Arabic localisation of Thunderbird</or>
   <pl>Arabska lokalizacja Thunderbirda</pl>
   <pt_BR>Árabe Localização para o Thunderbird</pt_BR>
   <pt>Árabe Localização para Thunderbird</pt>
   <ro>Arabic localisation of Thunderbird</ro>
   <ru>Arabic localisation of Thunderbird</ru>
   <sk>Arabic lokalizácia pre Thunderbird</sk>
   <sl>Arabic krajevne nastavitve za Thunderbird</sl>
   <so>Arabic localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në arabisht</sq>
   <sr>Arabic localisation of Thunderbird</sr>
   <sv>Arabisk lokalisering av Thunderbird</sv>
   <th>Arabic localisation ของ Thunderbird</th>
   <tr>Thunderbird Arapça yerelleştirmesi</tr>
   <uk>Arabic localisation of Thunderbird</uk>
   <vi>Arabic localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 阿拉伯语语言包</zh_CN>
   <zh_HK>Arabic localisation of Thunderbird</zh_HK>
   <zh_TW>Arabic localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-ar
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-ar
</uninstall_package_names>

</app>
