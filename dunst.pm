<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Misc
</category>

<name>
Dunst
</name>

<description>
   <am>Highly configurable and lightweight notification-daemon</am>
   <ar>Highly configurable and lightweight notification-daemon</ar>
   <be>Highly configurable and lightweight notification-daemon</be>
   <bg>Highly configurable and lightweight notification-daemon</bg>
   <bn>Highly configurable and lightweight notification-daemon</bn>
   <ca>Dimoni de notificacions lleuger i molt configurable</ca>
   <cs>Highly configurable and lightweight notification-daemon</cs>
   <da>Highly configurable and lightweight notification-daemon</da>
   <de>Besonders einfach und umfassend konfigurierbares Benachrichtigungs-Dienstprogramm</de>
   <el>Υψηλής διαμόρφωσης και ελαφρύς δαίμονας ειδοποιήσεων</el>
   <en>Highly configurable and lightweight notification-daemon</en>
   <es_ES>Demonio de notificación ligero y altamente configurable</es_ES>
   <es>Demonio de notificación ligero y altamente configurable</es>
   <et>Highly configurable and lightweight notification-daemon</et>
   <eu>Highly configurable and lightweight notification-daemon</eu>
   <fa>Highly configurable and lightweight notification-daemon</fa>
   <fil_PH>Highly configurable and lightweight notification-daemon</fil_PH>
   <fi>Highly configurable and lightweight notification-daemon</fi>
   <fr_BE>Hautement configurable et léger notification-daemon</fr_BE>
   <fr>Hautement configurable et léger notification-daemon</fr>
   <gl_ES>Highly configurable and lightweight notification-daemon</gl_ES>
   <gu>Highly configurable and lightweight notification-daemon</gu>
   <he_IL>Highly configurable and lightweight notification-daemon</he_IL>
   <hi>विस्तृत विन्यास समर्थित व सरल अधिसूचना-प्रोग्राम</hi>
   <hr>Highly configurable and lightweight notification-daemon</hr>
   <hu>Highly configurable and lightweight notification-daemon</hu>
   <id>Highly configurable and lightweight notification-daemon</id>
   <is>Highly configurable and lightweight notification-daemon</is>
   <it>Demone di notifica altamente configurabile e leggero</it>
   <ja>高度な設定が可能な軽量の通知デーモン</ja>
   <kk>Highly configurable and lightweight notification-daemon</kk>
   <ko>Highly configurable and lightweight notification-daemon</ko>
   <ku>Highly configurable and lightweight notification-daemon</ku>
   <lt>Highly configurable and lightweight notification-daemon</lt>
   <mk>Highly configurable and lightweight notification-daemon</mk>
   <mr>Highly configurable and lightweight notification-daemon</mr>
   <nb_NO>Highly configurable and lightweight notification-daemon</nb_NO>
   <nb>Lettvekts varslingsnisse med mange alternativer</nb>
   <nl_BE>Highly configurable and lightweight notification-daemon</nl_BE>
   <nl>Zeer configureerbare en lichtgewicht notificatie-daemon</nl>
   <or>Highly configurable and lightweight notification-daemon</or>
   <pl>Highly configurable and lightweight notification-daemon</pl>
   <pt_BR>Daemon de notificação altamente configurável e leve</pt_BR>
   <pt>Serviço (daemon) de notificações leve e altamente configurável</pt>
   <ro>Highly configurable and lightweight notification-daemon</ro>
   <ru>Highly configurable and lightweight notification-daemon</ru>
   <sk>Highly configurable and lightweight notification-daemon</sk>
   <sl>Zelo nastavljiv in lahek pritajeni program za obveščanje</sl>
   <so>Highly configurable and lightweight notification-daemon</so>
   <sq>Demon njoftimesh, tejet i formësueshëm dhe i peshës së lehtë</sq>
   <sr>Highly configurable and lightweight notification-daemon</sr>
   <sv>Mycket konfigurerbar lättvikts notifikation-daemon</sv>
   <th>Highly configurable and lightweight notification-daemon</th>
   <tr>Son derece yapılandırılabilir ve hafif bildirim hizmeti</tr>
   <uk>Highly configurable and lightweight notification-daemon</uk>
   <vi>Highly configurable and lightweight notification-daemon</vi>
   <zh_CN>Highly configurable and lightweight notification-daemon</zh_CN>
   <zh_HK>Highly configurable and lightweight notification-daemon</zh_HK>
   <zh_TW>Highly configurable and lightweight notification-daemon</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
dunst
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
dunst
</uninstall_package_names>
</app>
