<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Icons
</category>

<name>
Evolvere Icon Themes
</name>

<description>
   <am>evolvere icons (vibrant)</am>
   <ar>evolvere icons (vibrant)</ar>
   <be>evolvere icons (vibrant)</be>
   <bg>evolvere icons (vibrant)</bg>
   <bn>evolvere icons (vibrant)</bn>
   <ca>icones evolvere (vibrants)</ca>
   <cs>evolvere icons (vibrant)</cs>
   <da>evolvere-ikoner (farverige)</da>
   <de>Symbolthema Evolvere</de>
   <el>Εικονίδια evolvere</el>
   <en>evolvere icons (vibrant)</en>
   <es_ES>iconos evolvere (vibrante)</es_ES>
   <es>iconos evolvere (vibrante)</es>
   <et>evolvere icons (vibrant)</et>
   <eu>evolvere icons (vibrant)</eu>
   <fa>evolvere icons (vibrant)</fa>
   <fil_PH>evolvere icons (vibrant)</fil_PH>
   <fi>evolvere-kuvakkeet (eloisat)</fi>
   <fr_BE>icônes evolvere (dynamiques)</fr_BE>
   <fr>icônes evolvere (dynamiques)</fr>
   <gl_ES>iconas evolvere (vibrant)</gl_ES>
   <gu>evolvere icons (vibrant)</gu>
   <he_IL>evolvere icons (vibrant)</he_IL>
   <hi>evolvere आइकन (आकर्षक)</hi>
   <hr>evolvere icons (vibrant)</hr>
   <hu>evolvere icons (vibrant)</hu>
   <id>evolvere icons (vibrant)</id>
   <is>evolvere icons (vibrant)</is>
   <it>icone evolvere (vibrant)</it>
   <ja>Evolvere アイコン (鮮明な)</ja>
   <kk>evolvere icons (vibrant)</kk>
   <ko>evolvere icons (vibrant)</ko>
   <ku>evolvere icons (vibrant)</ku>
   <lt>evolvere icons (vibrant)</lt>
   <mk>evolvere icons (vibrant)</mk>
   <mr>evolvere icons (vibrant)</mr>
   <nb_NO>evolvere icons (vibrant)</nb_NO>
   <nb>evolvere-ikoner (fargerike)</nb>
   <nl_BE>evolvere icons (vibrant)</nl_BE>
   <nl>evolvere iconen (levendig)</nl>
   <or>evolvere icons (vibrant)</or>
   <pl>ikony evolvere (vibrant)</pl>
   <pt_BR>Ícones do evolvere (vibrante)</pt_BR>
   <pt>Ícones evolvere (vibrant)</pt>
   <ro>evolvere icons (vibrant)</ro>
   <ru>Значки evolvere (яркие)</ru>
   <sk>evolvere icons (vibrant)</sk>
   <sl>evlovere ikone (žive)</sl>
   <so>evolvere icons (vibrant)</so>
   <sq>Ikona evolvere (vibrante)</sq>
   <sr>evolvere icons (vibrant)</sr>
   <sv>evolvere ikoner (lysande)</sv>
   <th>evolvere icons (vibrant)</th>
   <tr>değişken simgeler (canlı)</tr>
   <uk>evolvere icons (vibrant)</uk>
   <vi>evolvere icons (vibrant)</vi>
   <zh_CN>evolvere 图标（亮色）</zh_CN>
   <zh_HK>evolvere icons (vibrant)</zh_HK>
   <zh_TW>evolvere icons (vibrant)</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
evolvere-icon-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
evolvere-icon-theme
</uninstall_package_names>
</app>
