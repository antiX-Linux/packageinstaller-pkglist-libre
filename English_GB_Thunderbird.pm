<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English_GB_Thunderbird
</name>

<description>
   <am>GB English localisation of Thunderbird</am>
   <ar>GB English localisation of Thunderbird</ar>
   <be>GB English localisation of Thunderbird</be>
   <bg>GB English localisation of Thunderbird</bg>
   <bn>GB English localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en anglès (UK)</ca>
   <cs>GB English localisation of Thunderbird</cs>
   <da>Engelsk (storbritannien) oversættelse af Thunderbird</da>
   <de>Englische (GB) Lokalisierung von Thunderbird</de>
   <el>Αγγλικά(GΒ) για το Thunderbird</el>
   <en>GB English localisation of Thunderbird</en>
   <es_ES>Localización Inglesa (GB) de Thunderbird</es_ES>
   <es>Localización Inglés (GB) de Thunderbird</es>
   <et>GB English localisation of Thunderbird</et>
   <eu>GB English localisation of Thunderbird</eu>
   <fa>GB English localisation of Thunderbird</fa>
   <fil_PH>GB English localisation of Thunderbird</fil_PH>
   <fi>GB-englantilainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation anglaise_GB pour Thunderbird</fr_BE>
   <fr>Localisation anglaise_GB pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird para British English</gl_ES>
   <gu>GB English localisation of Thunderbird</gu>
   <he_IL>GB English localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का ब्रिटिश अंग्रेज़ी संस्करण</hi>
   <hr>GB English localisation of Thunderbird</hr>
   <hu>GB English localisation of Thunderbird</hu>
   <id>GB English localisation of Thunderbird</id>
   <is>GB English localisation of Thunderbird</is>
   <it>Localizzazione in inglese GB di Thunderbird</it>
   <ja>Thunderbird の イギリス英語版</ja>
   <kk>GB English localisation of Thunderbird</kk>
   <ko>GB English localisation of Thunderbird</ko>
   <ku>GB English localisation of Thunderbird</ku>
   <lt>GB English localisation of Thunderbird</lt>
   <mk>GB English localisation of Thunderbird</mk>
   <mr>GB English localisation of Thunderbird</mr>
   <nb_NO>GB English localisation of Thunderbird</nb_NO>
   <nb>Britisk engelsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>GB English localisation of Thunderbird</nl_BE>
   <nl>GB Engelse lokalisatie van Thunderbird</nl>
   <or>GB English localisation of Thunderbird</or>
   <pl>Angielski Brytyjski lokalizacja Thunderbirda</pl>
   <pt_BR>Inglês da Grã-Bretanha Localização para o Thunderbird</pt_BR>
   <pt>Inglês GB Localização para Thunderbird</pt>
   <ro>GB English localisation of Thunderbird</ro>
   <ru>GB English localisation of Thunderbird</ru>
   <sk>GB English localisation of Thunderbird</sk>
   <sl>VB angleške krajevne nastavitve za Thunderbird</sl>
   <so>GB English localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në anglishte britanike</sq>
   <sr>GB English localisation of Thunderbird</sr>
   <sv>GB Engelsk lokalisering av Thunderbird</sv>
   <th>GB English localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Britanya İngilizcesi yerelleştirmesi</tr>
   <uk>GB English локалізація Thunderbird</uk>
   <vi>GB English localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 英式英语语言包</zh_CN>
   <zh_HK>GB English localisation of Thunderbird</zh_HK>
   <zh_TW>GB English localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-en-gb
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-en-gb
</uninstall_package_names>

</app>
