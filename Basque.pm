<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Basque (Euskera)
</name>

<description>
   <am>Basque (Euskera) dictionary for hunspell</am>
   <ar>Basque (Euskera) dictionary for hunspell</ar>
   <be>Basque (Euskera) dictionary for hunspell</be>
   <bg>Basque (Euskera) dictionary for hunspell</bg>
   <bn>Basque (Euskera) dictionary for hunspell</bn>
   <ca>Diccionari Euskera per hunspell</ca>
   <cs>Basque (Euskera) dictionary for hunspell</cs>
   <da>Basque (Euskera) dictionary for hunspell</da>
   <de>Wörterbuch für die baskische Sprache “Euskara” für “Hunspell”</de>
   <el>Λεξικό των Βάσκων (Euskera) για hunspell</el>
   <en>Basque (Euskera) dictionary for hunspell</en>
   <es_ES>Diccionario Euskera para hunspell</es_ES>
   <es>Diccionario Vasco (Euskera) para hunspell</es>
   <et>Basque (Euskera) dictionary for hunspell</et>
   <eu>Basque (Euskera) dictionary for hunspell</eu>
   <fa>Basque (Euskera) dictionary for hunspell</fa>
   <fil_PH>Basque (Euskera) dictionary for hunspell</fil_PH>
   <fi>Basque (Euskera) dictionary for hunspell</fi>
   <fr_BE>Basque (Euskera) dictionnaire pour hunspell</fr_BE>
   <fr>Basque (Euskera) dictionnaire pour hunspell</fr>
   <gl_ES>Basque (Euskera) dictionary for hunspell</gl_ES>
   <gu>Basque (Euskera) dictionary for hunspell</gu>
   <he_IL>Basque (Euskera) dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु बास्क (युस्कारा) शब्दकोष</hi>
   <hr>Basque (Euskera) dictionary for hunspell</hr>
   <hu>Basque (Euskera) dictionary for hunspell</hu>
   <id>Basque (Euskera) dictionary for hunspell</id>
   <is>Basque (Euskera) dictionary for hunspell</is>
   <it>Dizionario basco (Euskera) per Hunspell</it>
   <ja>Hunspell 用バスク語（Euskera）辞書</ja>
   <kk>Basque (Euskera) dictionary for hunspell</kk>
   <ko>Basque (Euskera) dictionary for hunspell</ko>
   <ku>Basque (Euskera) dictionary for hunspell</ku>
   <lt>Basque (Euskera) dictionary for hunspell</lt>
   <mk>Basque (Euskera) dictionary for hunspell</mk>
   <mr>Basque (Euskera) dictionary for hunspell</mr>
   <nb_NO>Basque (Euskera) dictionary for hunspell</nb_NO>
   <nb>Baskisk (euskera) ordliste for hunspell</nb>
   <nl_BE>Basque (Euskera) dictionary for hunspell</nl_BE>
   <nl>Baskisch (Euskera) woordenboek voor hunspell</nl>
   <or>Basque (Euskera) dictionary for hunspell</or>
   <pl>Basque (Euskera) dictionary for hunspell</pl>
   <pt_BR>Dicionário Basco (Euskera) para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Basco (Euskera) para hunspell</pt>
   <ro>Basque (Euskera) dictionary for hunspell</ro>
   <ru>Basque (Euskera) dictionary for hunspell</ru>
   <sk>Basque (Euskera) dictionary for hunspell</sk>
   <sl>Baskovski (Euskera) slovar za hunspell</sl>
   <so>Basque (Euskera) dictionary for hunspell</so>
   <sq>Fjalor baskisht (Euskera) për hunspell</sq>
   <sr>Basque (Euskera) dictionary for hunspell</sr>
   <sv>Baskisk (Euskera) ordbok för hunspell</sv>
   <th>Basque (Euskera) dictionary for hunspell</th>
   <tr>Hunspell için Bask Dili (Euskera) sözlüğü</tr>
   <uk>Basque (Euskera) dictionary for hunspell</uk>
   <vi>Basque (Euskera) dictionary for hunspell</vi>
   <zh_CN>Basque (Euskera) dictionary for hunspell</zh_CN>
   <zh_HK>Basque (Euskera) dictionary for hunspell</zh_HK>
   <zh_TW>Basque (Euskera) dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-eu
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-eu
</uninstall_package_names>

</app>
