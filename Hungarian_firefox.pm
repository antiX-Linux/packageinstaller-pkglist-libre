<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hungarian_Firefox
</name>

<description>
   <am>Hungarian localisation of Firefox</am>
   <ar>Hungarian localisation of Firefox</ar>
   <be>Hungarian localisation of Firefox</be>
   <bg>Hungarian localisation of Firefox</bg>
   <bn>Hungarian localisation of Firefox</bn>
   <ca>Localització de Firefox en Hongarès</ca>
   <cs>Hungarian localisation of Firefox</cs>
   <da>Ungarsk oversættelse af Firefox</da>
   <de>Ungarische Lokalisierung von Firefox</de>
   <el>Ουγγρικά για το Firefox</el>
   <en>Hungarian localisation of Firefox</en>
   <es_ES>Localización Húngara de Firefox</es_ES>
   <es>Localización Húngaro de Firefox</es>
   <et>Hungarian localisation of Firefox</et>
   <eu>Hungarian localisation of Firefox</eu>
   <fa>Hungarian localisation of Firefox</fa>
   <fil_PH>Hungarian localisation of Firefox</fil_PH>
   <fi>Unkarilainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en hongrois pour Firefox</fr_BE>
   <fr>Localisation en hongrois pour Firefox</fr>
   <gl_ES>Localización de Firefo ao húngaro</gl_ES>
   <gu>Hungarian localisation of Firefox</gu>
   <he_IL>תרגומי Firefox להונגרית</he_IL>
   <hi>फायरफॉक्स का हंगेरियन संस्करण</hi>
   <hr>Hungarian localisation of Firefox</hr>
   <hu>Hungarian localisation of Firefox</hu>
   <id>Hungarian localisation of Firefox</id>
   <is>Hungarian localisation of Firefox</is>
   <it>Localizzazione ungherese di Firefox</it>
   <ja>Firefox のハンガリー語版</ja>
   <kk>Hungarian localisation of Firefox</kk>
   <ko>Hungarian localisation of Firefox</ko>
   <ku>Hungarian localisation of Firefox</ku>
   <lt>Hungarian localisation of Firefox</lt>
   <mk>Hungarian localisation of Firefox</mk>
   <mr>Hungarian localisation of Firefox</mr>
   <nb_NO>Hungarian localisation of Firefox</nb_NO>
   <nb>Ungarsk lokaltilpassing av Firefox</nb>
   <nl_BE>Hungarian localisation of Firefox</nl_BE>
   <nl>Hongaarse lokalisatie van Firefox</nl>
   <or>Hungarian localisation of Firefox</or>
   <pl>Węgierska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Húngaro Localização para o Firefox</pt_BR>
   <pt>Húngaro Localização para Firefox</pt>
   <ro>Hungarian localisation of Firefox</ro>
   <ru>Венгерская локализация Firefox</ru>
   <sk>Hungarian localisation of Firefox</sk>
   <sl>Madžarska lokalizacija za Firefox</sl>
   <so>Hungarian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it ESR në hungarisht</sq>
   <sr>Hungarian localisation of Firefox</sr>
   <sv>Ungersk lokalisering av Firefox</sv>
   <th>Hungarian localisation ของ Firefox</th>
   <tr>Firefox'un Macarca yerelleştirmesi</tr>
   <uk>Hungarian локалізація Firefox</uk>
   <vi>Hungarian localisation of Firefox</vi>
   <zh_CN>Firefox 匈牙利语语言包</zh_CN>
   <zh_HK>Hungarian localisation of Firefox</zh_HK>
   <zh_TW>Hungarian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-hu
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-hu
</uninstall_package_names>
</app>
