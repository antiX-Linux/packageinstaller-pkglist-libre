<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Norwegian_Nynorsk_LO_Latest_main
</name>

<description>
   <am>Norwegian_nynorsk Language Meta-Package for LibreOffice</am>
   <ar>Norwegian_nynorsk Language Meta-Package for LibreOffice</ar>
   <be>Norwegian_nynorsk Language Meta-Package for LibreOffice</be>
   <bg>Norwegian_nynorsk Language Meta-Package for LibreOffice</bg>
   <bn>Norwegian_nynorsk Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Noruec-nynorsk per LibreOffice</ca>
   <cs>Norwegian_nynorsk Language Meta-Package for LibreOffice</cs>
   <da>Norwegian_nynorsk Language Meta-Package for LibreOffice</da>
   <de>Norwegisches (Nynorsk) Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Νορβηγική Nynorsk</el>
   <en>Norwegian_nynorsk Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Noruego para LibreOffice</es_ES>
   <es>Metapaquete de idioma Noruego Nynorsk para LibreOffice</es>
   <et>Norwegian_nynorsk Language Meta-Package for LibreOffice</et>
   <eu>Norwegian_nynorsk Language Meta-Package for LibreOffice</eu>
   <fa>Norwegian_nynorsk Language Meta-Package for LibreOffice</fa>
   <fil_PH>Norwegian_nynorsk Language Meta-Package for LibreOffice</fil_PH>
   <fi>Uusinorjalainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue norvégienne (Nynorsk) pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue norvégienne (Nynorsk) pour LibreOffice</fr>
   <gl_ES>Noruegués mynorsk Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Norwegian_nynorsk Language Meta-Package for LibreOffice</gu>
   <he_IL>Norwegian_nynorsk Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु नॉर्वेजियाई_नाइनोर्स्क भाषा मेटा-पैकेज</hi>
   <hr>Norwegian_nynorsk Language Meta-Package for LibreOffice</hr>
   <hu>Norwegian_nynorsk Language Meta-Package for LibreOffice</hu>
   <id>Norwegian_nynorsk Language Meta-Package for LibreOffice</id>
   <is>Norwegian_nynorsk Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua norvegese_nynorsk per LibreOffice</it>
   <ja>LibreOffice用のニーノシュク メタパッケージ</ja>
   <kk>Norwegian_nynorsk Language Meta-Package for LibreOffice</kk>
   <ko>Norwegian_nynorsk Language Meta-Package for LibreOffice</ko>
   <ku>Norwegian_nynorsk Language Meta-Package for LibreOffice</ku>
   <lt>Norwegian_nynorsk Language Meta-Package for LibreOffice</lt>
   <mk>Norwegian_nynorsk Language Meta-Package for LibreOffice</mk>
   <mr>Norwegian_nynorsk Language Meta-Package for LibreOffice</mr>
   <nb_NO>Norwegian_nynorsk Language Meta-Package for LibreOffice</nb_NO>
   <nb>Norsk nynorsk språkpakke for LibreOffice</nb>
   <nl_BE>Norwegian_nynorsk Language Meta-Package for LibreOffice</nl_BE>
   <nl>Noors Taal Meta-Pakket voor LibreOffice</nl>
   <or>Norwegian_nynorsk Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka norweskiego nynorsk (nowonorweskiego) dla LibreOffice</pl>
   <pt_BR>Novo Norueguês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Norueguês nynorsk Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Norwegian_nynorsk Language Meta-Package for LibreOffice</ro>
   <ru>Norwegian_nynorsk Language Meta-Package for LibreOffice</ru>
   <sk>Norwegian_nynorsk Language Meta-Package for LibreOffice</sk>
   <sl>Norveški nynorsk jezikovni meta-paket za LibreOffice</sl>
   <so>Norwegian_nynorsk Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në norvegjishte ninorks</sq>
   <sr>Norwegian_nynorsk Language Meta-Package for LibreOffice</sr>
   <sv>Norsk_nynorsk Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Norwegian_nynorsk สำหรับ LibreOffice</th>
   <tr>LibreOffice için Yeni Norveççe Dili Üst-Paketi</tr>
   <uk>Norwegian_nynorsk Language Meta-Package for LibreOffice</uk>
   <vi>Norwegian_nynorsk Language Meta-Package for LibreOffice</vi>
   <zh_CN>Norwegian_nynorsk Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Norwegian_nynorsk Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Norwegian_nynorsk Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-nn
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-nn
libreoffice-gtk3
</uninstall_package_names>

</app>
