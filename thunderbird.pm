<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Email
</category>

<name>
thunderbird
</name>

<description>
   <am>Latest Thunderbird email client from MX Community</am>
   <ar>Latest Thunderbird email client from MX Community</ar>
   <be>Latest Thunderbird email client from MX Community</be>
   <bg>Latest Thunderbird email client from MX Community</bg>
   <bn>Latest Thunderbird email client from MX Community</bn>
   <ca>Darrer client de correu Thunderbird de la Comunitat MX</ca>
   <cs>Latest Thunderbird email client from MX Community</cs>
   <da>Seneste Thunderbird e-mailklient fra MX-fællesskabet</da>
   <de>Neuester Thunderbird E-Mail-Client von MX Community</de>
   <el>Το τελευταίο πρόγραμμα ηλεκτρονικού ταχυδρομείου Thunderbird από την κοινότητα MX</el>
   <en>Latest Thunderbird email client from MX Community</en>
   <es_ES>Thunderbird el último cliente de correo de la comunidad MX</es_ES>
   <es>El último Thunderbird (cliente de correo) de la comunidad MX</es>
   <et>Latest Thunderbird email client from MX Community</et>
   <eu>Latest Thunderbird email client from MX Community</eu>
   <fa>Latest Thunderbird email client from MX Community</fa>
   <fil_PH>Latest Thunderbird email client from MX Community</fil_PH>
   <fi>Viimeisin MX-yhteisön Thunderbird sähköpostiohjelma</fi>
   <fr_BE>La dernière version du client mail Thunderbird par la Communauté MX</fr_BE>
   <fr>La dernière version du client mail Thunderbird par la Communauté MX</fr>
   <gl_ES>Thunderbird máis recente da comunidade MX</gl_ES>
   <gu>Latest Thunderbird email client from MX Community</gu>
   <he_IL>Latest Thunderbird email client from MX Community</he_IL>
   <hi>एमएक्स समुदाय द्वारा नवीनतम थंडरबर्ड ईमेल साधन</hi>
   <hr>Latest Thunderbird email client from MX Community</hr>
   <hu>Latest Thunderbird email client from MX Community</hu>
   <id>Latest Thunderbird email client from MX Community</id>
   <is>Latest Thunderbird email client from MX Community</is>
   <it>l'ultima versione di client email Thunderbird dalla MX Community</it>
   <ja>最新の Thunderbird メールソフト（MXコミュニティによる）</ja>
   <kk>Latest Thunderbird email client from MX Community</kk>
   <ko>Latest Thunderbird email client from MX Community</ko>
   <ku>Latest Thunderbird email client from MX Community</ku>
   <lt>Latest Thunderbird email client from MX Community</lt>
   <mk>Latest Thunderbird email client from MX Community</mk>
   <mr>Latest Thunderbird email client from MX Community</mr>
   <nb_NO>Latest Thunderbird email client from MX Community</nb_NO>
   <nb>Seneste Thunderbird e-postklient fra MX-fellesskapet</nb>
   <nl_BE>Latest Thunderbird email client from MX Community</nl_BE>
   <nl>Meest recente Thunderbird email programma vanuit MX Community</nl>
   <or>Latest Thunderbird email client from MX Community</or>
   <pl>najnowszy klient poczty e-mail Thunderbird od społeczności MX</pl>
   <pt_BR>Thunderbird - Versão mais recente do cliente de e-mail da Comunidade MX</pt_BR>
   <pt>Thunderbird (cliente de correio-electrónico) mais recente da comunidade MX</pt>
   <ro>Latest Thunderbird email client from MX Community</ro>
   <ru>Почтовый клиент Thunderbird последней версии из MX Community</ru>
   <sk>Latest Thunderbird email client from MX Community</sk>
   <sl>Zadnja različica poštnega odjemalca Thunderbird od MX skupnosti</sl>
   <so>Latest Thunderbird email client from MX Community</so>
   <sq>Klienti më i ri email Thunderbird, nga bashkësia MX</sq>
   <sr>Latest Thunderbird email client from MX Community</sr>
   <sv>Senaste Thunderbird mejlklient från MX Community</sv>
   <th>Thunderbird email client ล่าสุดจากคอมมูนิตี้ MX</th>
   <tr>MX Topluluğundan En son Thunderbird e-posta istemcisi</tr>
   <uk>Latest Thunderbird email client from MX Community</uk>
   <vi>Latest Thunderbird email client from MX Community</vi>
   <zh_CN>Latest Thunderbird email client from MX Community</zh_CN>
   <zh_HK>Latest Thunderbird email client from MX Community</zh_HK>
   <zh_TW>Latest Thunderbird email client from MX Community</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
thunderbird
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
thunderbird
</uninstall_package_names>
</app>
