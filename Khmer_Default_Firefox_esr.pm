<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Khmer_Default_Firefox_esr
</name>

<description>
   <am>Central Khmer localisation of Firefox ESR</am>
   <ar>Central Khmer localisation of Firefox ESR</ar>
   <be>Central Khmer localisation of Firefox ESR</be>
   <bg>Central Khmer localisation of Firefox ESR</bg>
   <bn>Central Khmer localisation of Firefox ESR</bn>
   <ca>Localització en Khmer central de Firefox ESR</ca>
   <cs>Central Khmer localisation of Firefox ESR</cs>
   <da>Central Khmer localisation of Firefox ESR</da>
   <de>Central Khmer localisation of Firefox ESR</de>
   <el>Khmer για Firefox ESR</el>
   <en>Central Khmer localisation of Firefox ESR</en>
   <es_ES>Localización en jémer central de Firefox ESR</es_ES>
   <es>Localización en jémer central de Firefox ESR</es>
   <et>Central Khmer localisation of Firefox ESR</et>
   <eu>Central Khmer localisation of Firefox ESR</eu>
   <fa>Central Khmer localisation of Firefox ESR</fa>
   <fil_PH>Central Khmer localisation of Firefox ESR</fil_PH>
   <fi>Central Khmer localisation of Firefox ESR</fi>
   <fr_BE>Localisation en khmer central pour Firefox ESR</fr_BE>
   <fr>Localisation en khmer central pour Firefox ESR</fr>
   <gl_ES>Central Khmer localisation of Firefox ESR</gl_ES>
   <gu>Central Khmer localisation of Firefox ESR</gu>
   <he_IL>Central Khmer localisation of Firefox ESR</he_IL>
   <hi>Central Khmer localisation of Firefox ESR</hi>
   <hr>Central Khmer localisation of Firefox ESR</hr>
   <hu>Central Khmer localisation of Firefox ESR</hu>
   <id>Central Khmer localisation of Firefox ESR</id>
   <is>Central Khmer localisation of Firefox ESR</is>
   <it>Localizzazione Central Khmer di Firefox ESR</it>
   <ja>Central Khmer localisation of Firefox ESR</ja>
   <kk>Central Khmer localisation of Firefox ESR</kk>
   <ko>Central Khmer localisation of Firefox ESR</ko>
   <ku>Central Khmer localisation of Firefox ESR</ku>
   <lt>Central Khmer localisation of Firefox ESR</lt>
   <mk>Central Khmer localisation of Firefox ESR</mk>
   <mr>Central Khmer localisation of Firefox ESR</mr>
   <nb_NO>Central Khmer localisation of Firefox ESR</nb_NO>
   <nb>Sentral khmer lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Central Khmer localisation of Firefox ESR</nl_BE>
   <nl>Central Khmer localisation of Firefox ESR</nl>
   <or>Central Khmer localisation of Firefox ESR</or>
   <pl>Central Khmer localisation of Firefox ESR</pl>
   <pt_BR>Quemer Central Localização para o Firefox ESR</pt_BR>
   <pt>Khmer Central Localização para Firefox ESR</pt>
   <ro>Central Khmer localisation of Firefox ESR</ro>
   <ru>Central Khmer localisation of Firefox ESR</ru>
   <sk>Central Khmer localisation of Firefox ESR</sk>
   <sl>Centralno kmerske krajevne nastavitve za Firefox ESR</sl>
   <so>Central Khmer localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në kamboxhishte qendrore</sq>
   <sr>Central Khmer localisation of Firefox ESR</sr>
   <sv>Central Khmer lokalisering av Firefox ESR</sv>
   <th>Central Khmer localisation of Firefox ESR</th>
   <tr>Central Khmer localisation of Firefox ESR</tr>
   <uk>Central Khmer localisation of Firefox ESR</uk>
   <vi>Central Khmer localisation of Firefox ESR</vi>
   <zh_CN>Central Khmer localisation of Firefox ESR</zh_CN>
   <zh_HK>Central Khmer localisation of Firefox ESR</zh_HK>
   <zh_TW>Central Khmer localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-km
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-km
</uninstall_package_names>

</app>
