<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bulgarian
</name>

<description>
   <am>Bulgarian dictionary for hunspell</am>
   <ar>Bulgarian dictionary for hunspell</ar>
   <be>Bulgarian dictionary for hunspell</be>
   <bg>Bulgarian dictionary for hunspell</bg>
   <bn>Bulgarian dictionary for hunspell</bn>
   <ca>Diccionari Búlgar per hunspell</ca>
   <cs>Bulgarian dictionary for hunspell</cs>
   <da>Bulgarian dictionary for hunspell</da>
   <de>Bulgarisches Wörterbuch für “Hunspell”</de>
   <el>Βουλγαρικό λεξικό για hunspell</el>
   <en>Bulgarian dictionary for hunspell</en>
   <es_ES>Diccionario Bulgaro para hunspell</es_ES>
   <es>Diccionario Búlgaro para hunspell</es>
   <et>Bulgarian dictionary for hunspell</et>
   <eu>Bulgarian dictionary for hunspell</eu>
   <fa>Bulgarian dictionary for hunspell</fa>
   <fil_PH>Bulgarian dictionary for hunspell</fil_PH>
   <fi>Bulgarian dictionary for hunspell</fi>
   <fr_BE>Bulgare dictionnaire pour hunspell</fr_BE>
   <fr>Bulgare dictionnaire pour hunspell</fr>
   <gl_ES>Bulgarian dictionary for hunspell</gl_ES>
   <gu>Bulgarian dictionary for hunspell</gu>
   <he_IL>Bulgarian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु बल्गेरियाई शब्दकोष</hi>
   <hr>Bulgarian dictionary for hunspell</hr>
   <hu>Bulgarian dictionary for hunspell</hu>
   <id>Bulgarian dictionary for hunspell</id>
   <is>Bulgarian dictionary for hunspell</is>
   <it>Dizionario bulgaro per hunspell</it>
   <ja>Hunspell 用ブルガリア語辞書</ja>
   <kk>Bulgarian dictionary for hunspell</kk>
   <ko>Bulgarian dictionary for hunspell</ko>
   <ku>Bulgarian dictionary for hunspell</ku>
   <lt>Bulgarian dictionary for hunspell</lt>
   <mk>Bulgarian dictionary for hunspell</mk>
   <mr>Bulgarian dictionary for hunspell</mr>
   <nb_NO>Bulgarian dictionary for hunspell</nb_NO>
   <nb>Bulgarsk ordliste for hunspell</nb>
   <nl_BE>Bulgarian dictionary for hunspell</nl_BE>
   <nl>Bulgaars woordenboek voor hunspell</nl>
   <or>Bulgarian dictionary for hunspell</or>
   <pl>Bulgarian dictionary for hunspell</pl>
   <pt_BR>Dicionário Búlgaro para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Búlgaro para hunspell</pt>
   <ro>Bulgarian dictionary for hunspell</ro>
   <ru>Bulgarian dictionary for hunspell</ru>
   <sk>Bulgarian dictionary for hunspell</sk>
   <sl>Bolgarski slovar za hunspell</sl>
   <so>Bulgarian dictionary for hunspell</so>
   <sq>Fjalor bullgarisht për hunspell</sq>
   <sr>Bulgarian dictionary for hunspell</sr>
   <sv>Bulgarisk ordbok för hunspell</sv>
   <th>Bulgarian dictionary for hunspell</th>
   <tr>Hunspell için Bulgarca sözlük</tr>
   <uk>Bulgarian dictionary for hunspell</uk>
   <vi>Bulgarian dictionary for hunspell</vi>
   <zh_CN>Bulgarian dictionary for hunspell</zh_CN>
   <zh_HK>Bulgarian dictionary for hunspell</zh_HK>
   <zh_TW>Bulgarian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-bg
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-bg
</uninstall_package_names>

</app>
