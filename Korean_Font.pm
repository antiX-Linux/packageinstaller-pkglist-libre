<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Korean_Font
</name>

<description>
   <am>Korean fonts packages</am>
   <ar>Korean fonts packages</ar>
   <be>Korean fonts packages</be>
   <bg>Korean fonts packages</bg>
   <bn>Korean fonts packages</bn>
   <ca>Paquets de tipus de lletra Coreans</ca>
   <cs>Korean fonts packages</cs>
   <da>Korean fonts packages</da>
   <de>Koreanische Schriftartensammlung (Glyphensätze)</de>
   <el>Πακέτα κορεατικών γραμματοσειρών</el>
   <en>Korean fonts packages</en>
   <es_ES>Paquetes de fuentes coreanas</es_ES>
   <es>Paquetes de fuentes coreanas</es>
   <et>Korean fonts packages</et>
   <eu>Korean fonts packages</eu>
   <fa>Korean fonts packages</fa>
   <fil_PH>Korean fonts packages</fil_PH>
   <fi>Korean fonts packages</fi>
   <fr_BE>Coréen Paquets de polices</fr_BE>
   <fr>Coréen Paquets de polices</fr>
   <gl_ES>Korean fonts packages</gl_ES>
   <gu>Korean fonts packages</gu>
   <he_IL>Korean fonts packages</he_IL>
   <hi>कोरियाई मुद्रलिपि पैकेज</hi>
   <hr>Korean fonts packages</hr>
   <hu>Korean fonts packages</hu>
   <id>Korean fonts packages</id>
   <is>Korean fonts packages</is>
   <it>Pacchetti di fonts per il coreano</it>
   <ja>韓国（朝鮮）語フォント</ja>
   <kk>Korean fonts packages</kk>
   <ko>Korean fonts packages</ko>
   <ku>Korean fonts packages</ku>
   <lt>Korean fonts packages</lt>
   <mk>Korean fonts packages</mk>
   <mr>Korean fonts packages</mr>
   <nb_NO>Korean fonts packages</nb_NO>
   <nb>Pakke med koreanske skrifttyper</nb>
   <nl_BE>Korean fonts packages</nl_BE>
   <nl>Korean fonts packages</nl>
   <or>Korean fonts packages</or>
   <pl>Korean fonts packages</pl>
   <pt_BR>Pacotes de Fontes Coreano</pt_BR>
   <pt>Pacotes de fontes para coreano</pt>
   <ro>Korean fonts packages</ro>
   <ru>Korean fonts packages</ru>
   <sk>Korean fonts packages</sk>
   <sl>Korejski paketi pisav</sl>
   <so>Korean fonts packages</so>
   <sq>Paketa shkronjash koreançe</sq>
   <sr>Korean fonts packages</sr>
   <sv>Koreanska font paket</sv>
   <th>Korean fonts packages</th>
   <tr>Korece yazı tipi paketleri</tr>
   <uk>Korean fonts packages</uk>
   <vi>Korean fonts packages</vi>
   <zh_CN>Korean fonts packages</zh_CN>
   <zh_HK>Korean fonts packages</zh_HK>
   <zh_TW>Korean fonts packages</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
fonts-nanum
fonts-nanum-extra
fonts-unfonts-core
fonts-unfonts-extra
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
fonts-nanum
fonts-nanum-extra
fonts-unfonts-core
fonts-unfonts-extra
</uninstall_package_names>

</app>
