<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Gujarati
</name>

<description>
   <am>Gujarati dictionary for hunspell</am>
   <ar>Gujarati dictionary for hunspell</ar>
   <be>Gujarati dictionary for hunspell</be>
   <bg>Gujarati dictionary for hunspell</bg>
   <bn>Gujarati dictionary for hunspell</bn>
   <ca>Diccionari Gujarati per hunspell</ca>
   <cs>Gujarati dictionary for hunspell</cs>
   <da>Gujarati dictionary for hunspell</da>
   <de>Indisches (gujaratisches) Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Γκουτζαράτι για hunspell</el>
   <en>Gujarati dictionary for hunspell</en>
   <es_ES>Diccionario Gujarati para hunspell</es_ES>
   <es>Diccionario Gujarati para hunspell</es>
   <et>Gujarati dictionary for hunspell</et>
   <eu>Gujarati dictionary for hunspell</eu>
   <fa>Gujarati dictionary for hunspell</fa>
   <fil_PH>Gujarati dictionary for hunspell</fil_PH>
   <fi>Gujarati dictionary for hunspell</fi>
   <fr_BE>Gujarati dictionnaire pour hunspell</fr_BE>
   <fr>Gujarati dictionnaire pour hunspell</fr>
   <gl_ES>Gujarati dictionary for hunspell</gl_ES>
   <gu>Gujarati dictionary for hunspell</gu>
   <he_IL>Gujarati dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु गुजराती शब्दकोष</hi>
   <hr>Gujarati dictionary for hunspell</hr>
   <hu>Gujarati dictionary for hunspell</hu>
   <id>Gujarati dictionary for hunspell</id>
   <is>Gujarati dictionary for hunspell</is>
   <it>Dizionario della lingua gujarati per hunspell</it>
   <ja>Hunspell 用グジャラート語辞書</ja>
   <kk>Gujarati dictionary for hunspell</kk>
   <ko>Gujarati dictionary for hunspell</ko>
   <ku>Gujarati dictionary for hunspell</ku>
   <lt>Gujarati dictionary for hunspell</lt>
   <mk>Gujarati dictionary for hunspell</mk>
   <mr>Gujarati dictionary for hunspell</mr>
   <nb_NO>Gujarati dictionary for hunspell</nb_NO>
   <nb>Gujarati ordliste for hunspell</nb>
   <nl_BE>Gujarati dictionary for hunspell</nl_BE>
   <nl>Gujarati dictionary for hunspell</nl>
   <or>Gujarati dictionary for hunspell</or>
   <pl>Gujarati dictionary for hunspell</pl>
   <pt_BR>Dicionário Guzerate para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Gujarati para hunspell</pt>
   <ro>Gujarati dictionary for hunspell</ro>
   <ru>Gujarati dictionary for hunspell</ru>
   <sk>Gujarati dictionary for hunspell</sk>
   <sl>Gudžaraški slvoar za hunspell</sl>
   <so>Gujarati dictionary for hunspell</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në gujaratase</sq>
   <sr>Gujarati dictionary for hunspell</sr>
   <sv>Gujarati ordbok för hunspell</sv>
   <th>Gujarati dictionary for hunspell</th>
   <tr>Hunspell için Gujaratça sözlük</tr>
   <uk>Gujarati dictionary for hunspell</uk>
   <vi>Gujarati dictionary for hunspell</vi>
   <zh_CN>Gujarati dictionary for hunspell</zh_CN>
   <zh_HK>Gujarati dictionary for hunspell</zh_HK>
   <zh_TW>Gujarati dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-gu
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-gu
</uninstall_package_names>

</app>
