<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Afrikaan_Firefox
</name>

<description>
   <am>Afrikaan localisation of Firefox</am>
   <ar>Afrikaan localisation of Firefox</ar>
   <be>Afrikaan localisation of Firefox</be>
   <bg>Afrikaan localisation of Firefox</bg>
   <bn>Afrikaan localisation of Firefox</bn>
   <ca>Localització de Firefox en Afrikaan</ca>
   <cs>Afrikaan localisation of Firefox</cs>
   <da>Afrikaan oversættelse af Firefox</da>
   <de>Afrikaans Lokalisierung von Firefox</de>
   <el>Αφρικανικά για το Firefox</el>
   <en>Afrikaan localisation of Firefox</en>
   <es_ES>Localización africana de Firefox</es_ES>
   <es>Localización Africano de Firefox</es>
   <et>Afrikaan localisation of Firefox</et>
   <eu>Afrikaan localisation of Firefox</eu>
   <fa>Afrikaan localisation of Firefox</fa>
   <fil_PH>Afrikaan localisation of Firefox</fil_PH>
   <fi>Afrikaansin kielinen Firefox kielipaketti</fi>
   <fr_BE>Localisation en afrikaans pour Firefox</fr_BE>
   <fr>Localisation en afrikaans pour Firefox</fr>
   <gl_ES>Localización de Firefox a africánder</gl_ES>
   <gu>Afrikaan localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לאפריקאנס</he_IL>
   <hi>फायरफॉक्स का अफ़्रीकी डच अनुवाद</hi>
   <hr>Afrikaanska lokalizacija Firefoxa</hr>
   <hu>Afrikaan localisation of Firefox</hu>
   <id>Afrikaan localisation of Firefox</id>
   <is>Afrikaan localisation of Firefox</is>
   <it>Localizzazione in afrikaans di Firefox</it>
   <ja>Firefox のアフリカーンス語版</ja>
   <kk>Afrikaan localisation of Firefox</kk>
   <ko>Afrikaan localisation of Firefox</ko>
   <ku>Afrikaan localisation of Firefox</ku>
   <lt>Afrikaan localisation of Firefox</lt>
   <mk>Afrikaan localisation of Firefox</mk>
   <mr>Afrikaan localisation of Firefox</mr>
   <nb_NO>Afrikaan localisation of Firefox</nb_NO>
   <nb>Afrikaans lokaltilpassing av Firefox</nb>
   <nl_BE>Afrikaan localisation of Firefox</nl_BE>
   <nl>Afrikaanse lokalisatie van Firefox</nl>
   <or>Afrikaan localisation of Firefox</or>
   <pl>Afrykanerska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Africâner Localização para o Firefox</pt_BR>
   <pt>Africander Localização para Firefox</pt>
   <ro>Afrikaan localisation of Firefox</ro>
   <ru>Африканская локализация Firefox</ru>
   <sk>Afrikaan lokalizácia pre Firefox</sk>
   <sl>Afrikaan localisation of Firefox</sl>
   <so>Afrikaan localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në afrikaans</sq>
   <sr>Afrikaan localisation of Firefox</sr>
   <sv>Afrikaan lokalisering av Firefox</sv>
   <th>Afrikaan localisation ของ Firefox</th>
   <tr>Firefox Afrika yerelleştirmesi</tr>
   <uk>Afrikaan локалізація Firefox</uk>
   <vi>Afrikaan localisation of Firefox</vi>
   <zh_CN>Firefox 南非语语言包</zh_CN>
   <zh_HK>Afrikaan localisation of Firefox</zh_HK>
   <zh_TW>Afrikaan localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-af
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-af
</uninstall_package_names>
</app>
