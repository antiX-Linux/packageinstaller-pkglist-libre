<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Mongolian_LO_Latest_full
</name>

<description>
   <am>Mongolian Language Meta-Package for LibreOffice</am>
   <ar>Mongolian Language Meta-Package for LibreOffice</ar>
   <be>Mongolian Language Meta-Package for LibreOffice</be>
   <bg>Mongolian Language Meta-Package for LibreOffice</bg>
   <bn>Mongolian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Mongol per LibreOffice</ca>
   <cs>Mongolian Language Meta-Package for LibreOffice</cs>
   <da>Mongolian Language Meta-Package for LibreOffice</da>
   <de>Mongolisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Mongolian</el>
   <en>Mongolian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Mongol para LibreOffice</es_ES>
   <es>Metapaquete de idioma Mongol para LibreOffice</es>
   <et>Mongolian Language Meta-Package for LibreOffice</et>
   <eu>Mongolian Language Meta-Package for LibreOffice</eu>
   <fa>Mongolian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Mongolian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Mongolialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue mongole pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue mongole pour LibreOffice</fr>
   <gl_ES>Mongol Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Mongolian Language Meta-Package for LibreOffice</gu>
   <he_IL>Mongolian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु मंगोल भाषा मेटा-पैकेज</hi>
   <hr>Mongolian Language Meta-Package for LibreOffice</hr>
   <hu>Mongolian Language Meta-Package for LibreOffice</hu>
   <id>Mongolian Language Meta-Package for LibreOffice</id>
   <is>Mongolian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua mongola per LibreOffice</it>
   <ja>LibreOffice用モンゴル語メタパッケージ</ja>
   <kk>Mongolian Language Meta-Package for LibreOffice</kk>
   <ko>Mongolian Language Meta-Package for LibreOffice</ko>
   <ku>Mongolian Language Meta-Package for LibreOffice</ku>
   <lt>Mongolian Language Meta-Package for LibreOffice</lt>
   <mk>Mongolian Language Meta-Package for LibreOffice</mk>
   <mr>Mongolian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Mongolian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Mongolsk språkpakke for LibreOffice</nb>
   <nl_BE>Mongolian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Mongools Taal Meta-Pakket voor LibreOffice</nl>
   <or>Mongolian Language Meta-Package for LibreOffice</or>
   <pl>Mongolski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Mongol Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Mongol Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Mongolian Language Meta-Package for LibreOffice</ro>
   <ru>Mongolian Language Meta-Package for LibreOffice</ru>
   <sk>Mongolian Language Meta-Package for LibreOffice</sk>
   <sl>Mongolski jezikovni meta-paket za LibreOffice</sl>
   <so>Mongolian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në mongolisht</sq>
   <sr>Mongolian Language Meta-Package for LibreOffice</sr>
   <sv>Mongoliskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Mongolian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Moğolca Dili Üst-Paketi</tr>
   <uk>Mongolian Language Meta-Package for LibreOffice</uk>
   <vi>Mongolian Language Meta-Package for LibreOffice</vi>
   <zh_CN>Mongolian Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Mongolian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Mongolian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-mm
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-mm
</uninstall_package_names>

</app>
