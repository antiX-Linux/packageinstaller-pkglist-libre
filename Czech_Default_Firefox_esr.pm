<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Czech_Default_Firefox_esr
</name>

<description>
   <am>Czech localisation of Firefox ESR</am>
   <ar>Czech localisation of Firefox ESR</ar>
   <be>Czech localisation of Firefox ESR</be>
   <bg>Czech localisation of Firefox ESR</bg>
   <bn>Czech localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Txec</ca>
   <cs>Czech localisation of Firefox ESR</cs>
   <da>Czech localisation of Firefox ESR</da>
   <de>Tschechische Lokalisation von “Firefox ESR”</de>
   <el>Τσεχική για Firefox ESR</el>
   <en>Czech localisation of Firefox ESR</en>
   <es_ES>Localización Checa de Firefox ESR</es_ES>
   <es>Localización Checo de Firefox ESR</es>
   <et>Czech localisation of Firefox ESR</et>
   <eu>Czech localisation of Firefox ESR</eu>
   <fa>Czech localisation of Firefox ESR</fa>
   <fil_PH>Czech localisation of Firefox ESR</fil_PH>
   <fi>Czech localisation of Firefox ESR</fi>
   <fr_BE>Localisation en tchèque pour Firefox ESR</fr_BE>
   <fr>Localisation en tchèque pour Firefox ESR</fr>
   <gl_ES>Czech localisation of Firefox ESR</gl_ES>
   <gu>Czech localisation of Firefox ESR</gu>
   <he_IL>Czech localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का चेक संस्करण</hi>
   <hr>Czech localisation of Firefox ESR</hr>
   <hu>Czech localisation of Firefox ESR</hu>
   <id>Czech localisation of Firefox ESR</id>
   <is>Czech localisation of Firefox ESR</is>
   <it>Localizzazione in ceco di Firefox ESR</it>
   <ja>チェコ語版 Firefox ESR</ja>
   <kk>Czech localisation of Firefox ESR</kk>
   <ko>Czech localisation of Firefox ESR</ko>
   <ku>Czech localisation of Firefox ESR</ku>
   <lt>Czech localisation of Firefox ESR</lt>
   <mk>Czech localisation of Firefox ESR</mk>
   <mr>Czech localisation of Firefox ESR</mr>
   <nb_NO>Czech localisation of Firefox ESR</nb_NO>
   <nb>Tsjekkisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Czech localisation of Firefox ESR</nl_BE>
   <nl>Tsjechische lokalisatie van Firefox ESR</nl>
   <or>Czech localisation of Firefox ESR</or>
   <pl>Czech localisation of Firefox ESR</pl>
   <pt_BR>Checo Localização para o Firefox ESR</pt_BR>
   <pt>Checo Localização para Firefox ESR</pt>
   <ro>Czech localisation of Firefox ESR</ro>
   <ru>Czech localisation of Firefox ESR</ru>
   <sk>Czech localisation of Firefox ESR</sk>
   <sl>Češke krajevne nastavitve za Firefox ESR</sl>
   <so>Czech localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në çekisht</sq>
   <sr>Czech localisation of Firefox ESR</sr>
   <sv>Tjeckisk lokalisering av Firefox ESR</sv>
   <th>Czech localisation of Firefox ESR</th>
   <tr>Firefox ESR Çekce yerelleştirmesi</tr>
   <uk>Czech localisation of Firefox ESR</uk>
   <vi>Czech localisation of Firefox ESR</vi>
   <zh_CN>Czech localisation of Firefox ESR</zh_CN>
   <zh_HK>Czech localisation of Firefox ESR</zh_HK>
   <zh_TW>Czech localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-cs
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-cs
</uninstall_package_names>

</app>
