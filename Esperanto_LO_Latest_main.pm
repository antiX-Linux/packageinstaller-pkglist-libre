<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Esperanto_LO_Latest_main
</name>

<description>
   <am>Esperanto Language Meta-Package for LibreOffice</am>
   <ar>Esperanto Language Meta-Package for LibreOffice</ar>
   <be>Esperanto Language Meta-Package for LibreOffice</be>
   <bg>Esperanto Language Meta-Package for LibreOffice</bg>
   <bn>Esperanto Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Esperanto per Libreoffice</ca>
   <cs>Esperanto Language Meta-Package for LibreOffice</cs>
   <da>Esperanto Language Meta-Package for LibreOffice</da>
   <de>Esperanto Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Εσπεράντο</el>
   <en>Esperanto Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Esperanto para LibreOffice</es_ES>
   <es>Metapaquete de idioma Esperanto para LibreOffice</es>
   <et>Esperanto Language Meta-Package for LibreOffice</et>
   <eu>Esperanto Language Meta-Package for LibreOffice</eu>
   <fa>Esperanto Language Meta-Package for LibreOffice</fa>
   <fil_PH>Esperanto Language Meta-Package for LibreOffice</fil_PH>
   <fi>Esperanto-kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet de langue espérantiste pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue espérantiste pour LibreOffice</fr>
   <gl_ES>Esperanto Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Esperanto Language Meta-Package for LibreOffice</gu>
   <he_IL>Esperanto Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु एस्पेरांतो भाषा मेटा-पैकेज</hi>
   <hr>Esperanto Language Meta-Package for LibreOffice</hr>
   <hu>Esperanto Language Meta-Package for LibreOffice</hu>
   <id>Esperanto Language Meta-Package for LibreOffice</id>
   <is>Esperanto Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua esperanto per LibreOffice</it>
   <ja>LibreOffice用のエスペラント言語メタパッケージ</ja>
   <kk>Esperanto Language Meta-Package for LibreOffice</kk>
   <ko>Esperanto Language Meta-Package for LibreOffice</ko>
   <ku>Esperanto Language Meta-Package for LibreOffice</ku>
   <lt>Esperanto Language Meta-Package for LibreOffice</lt>
   <mk>Esperanto Language Meta-Package for LibreOffice</mk>
   <mr>Esperanto Language Meta-Package for LibreOffice</mr>
   <nb_NO>Esperanto Language Meta-Package for LibreOffice</nb_NO>
   <nb>Esperanto språkpakke for LibreOffice</nb>
   <nl_BE>Esperanto Language Meta-Package for LibreOffice</nl_BE>
   <nl>Esperano Taal Meta-Pakket voor LibreOffice</nl>
   <or>Esperanto Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka esperanto dla LibreOffice</pl>
   <pt_BR>Esperanto Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Esperanto Meta-Pacote de idioma para LibreOffice</pt>
   <ro>Esperanto Language Meta-Package for LibreOffice</ro>
   <ru>Esperanto Language Meta-Package for LibreOffice</ru>
   <sk>Esperanto Language Meta-Package for LibreOffice</sk>
   <sl>Esperanto jezikovni meta-paket za LibreOffice</sl>
   <so>Esperanto Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në esperanto</sq>
   <sr>Esperanto Language Meta-Package for LibreOffice</sr>
   <sv>Esperanto Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Esparento สำหรับ LibreOffice</th>
   <tr>LibreOffice için Esperanto Dili Üst-Paketi</tr>
   <uk>Esperanto Language Meta-Package for LibreOffice</uk>
   <vi>Esperanto Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 世界语语言包</zh_CN>
   <zh_HK>Esperanto Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Esperanto Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-eo
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-eo
libreoffice-gtk3
</uninstall_package_names>

</app>
