<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Sinhala_LO_Latest_main
</name>

<description>
   <am>Sinhala Language Meta-Package for LibreOffice</am>
   <ar>Sinhala Language Meta-Package for LibreOffice</ar>
   <be>Sinhala Language Meta-Package for LibreOffice</be>
   <bg>Sinhala Language Meta-Package for LibreOffice</bg>
   <bn>Sinhala Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Sinhali per LibreOffice</ca>
   <cs>Sinhala Language Meta-Package for LibreOffice</cs>
   <da>Sinhala Language Meta-Package for LibreOffice</da>
   <de>Sinhala Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Sinhala</el>
   <en>Sinhala Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete en idioma Singalés para LibreOffice</es_ES>
   <es>Metapaquete de Lenguaje Cingalés para LibreOffice</es>
   <et>Sinhala Language Meta-Package for LibreOffice</et>
   <eu>Sinhala Language Meta-Package for LibreOffice</eu>
   <fa>Sinhala Language Meta-Package for LibreOffice</fa>
   <fil_PH>Sinhala Language Meta-Package for LibreOffice</fil_PH>
   <fi>Sinhalinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue cingalaise pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue cingalaise pour LibreOffice</fr>
   <gl_ES>Cingalés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Sinhala Language Meta-Package for LibreOffice</gu>
   <he_IL>Sinhala Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु सिंहली भाषा मेटा-पैकेज</hi>
   <hr>Sinhala Language Meta-Package for LibreOffice</hr>
   <hu>Sinhala Language Meta-Package for LibreOffice</hu>
   <id>Sinhala Language Meta-Package for LibreOffice</id>
   <is>Sinhala Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua singalese per LibreOffice</it>
   <ja>LibreOffice用のシンハラ語メタパッケージ</ja>
   <kk>Sinhala Language Meta-Package for LibreOffice</kk>
   <ko>Sinhala Language Meta-Package for LibreOffice</ko>
   <ku>Sinhala Language Meta-Package for LibreOffice</ku>
   <lt>Sinhala Language Meta-Package for LibreOffice</lt>
   <mk>Sinhala Language Meta-Package for LibreOffice</mk>
   <mr>Sinhala Language Meta-Package for LibreOffice</mr>
   <nb_NO>Sinhala Language Meta-Package for LibreOffice</nb_NO>
   <nb>Sinhala språkpakke for LibreOffice</nb>
   <nl_BE>Sinhala Language Meta-Package for LibreOffice</nl_BE>
   <nl>Sinhala Taal Meta-Pakket voor LibreOffice</nl>
   <or>Sinhala Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka syngaleskiego dla LibreOffice</pl>
   <pt_BR>Cingalês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Cingalês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Sinhala Language Meta-Package for LibreOffice</ro>
   <ru>Sinhala Language Meta-Package for LibreOffice</ru>
   <sk>Sinhala Language Meta-Package for LibreOffice</sk>
   <sl>Sinhalski jezikovni meta-paket za LibreOffice</sl>
   <so>Sinhala Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në sinhala</sq>
   <sr>Sinhala Language Meta-Package for LibreOffice</sr>
   <sv>Sinhala Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Sinhala สำหรับ LibreOffice</th>
   <tr>LibreOffice için Sinhala Dili Üst-Paketi</tr>
   <uk>Sinhala Language Meta-Package for LibreOffice</uk>
   <vi>Sinhala Language Meta-Package for LibreOffice</vi>
   <zh_CN>Sinhala Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Sinhala Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Sinhala Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-si
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-si
libreoffice-gtk3
</uninstall_package_names>

</app>
