<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Lithuanian_Thunderbird
</name>

<description>
   <am>Lithuanian localisation of Thunderbird</am>
   <ar>Lithuanian localisation of Thunderbird</ar>
   <be>Lithuanian localisation of Thunderbird</be>
   <bg>Lithuanian localisation of Thunderbird</bg>
   <bn>Lithuanian localisation of Thunderbird</bn>
   <ca>Localització en lituà de Thunderbird</ca>
   <cs>Lithuanian localisation of Thunderbird</cs>
   <da>Litauisk oversættelse af Thunderbird</da>
   <de>Litauische Lokalisierung von Thunderbird</de>
   <el>Λιθουανικά για το Thunderbird</el>
   <en>Lithuanian localisation of Thunderbird</en>
   <es_ES>Localización Lituana de Thunderbird</es_ES>
   <es>Localización Lituano de Thunderbird</es>
   <et>Lithuanian localisation of Thunderbird</et>
   <eu>Lithuanian localisation of Thunderbird</eu>
   <fa>Lithuanian localisation of Thunderbird</fa>
   <fil_PH>Lithuanian localisation of Thunderbird</fil_PH>
   <fi>Liettualainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en lituanien pour Thunderbird</fr_BE>
   <fr>Localisation en lituanien pour Thunderbird</fr>
   <gl_ES>Localización do Thunderbird ao lituano</gl_ES>
   <gu>Lithuanian localisation of Thunderbird</gu>
   <he_IL>Lithuanian localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का लिथुआनियाई संस्करण</hi>
   <hr>Lithuanian localisation of Thunderbird</hr>
   <hu>Lithuanian localisation of Thunderbird</hu>
   <id>Lithuanian localisation of Thunderbird</id>
   <is>Lithuanian localisation of Thunderbird</is>
   <it>Localizzazione lituana di Thunderbird</it>
   <ja>Thunderbird のリトアニア語版</ja>
   <kk>Lithuanian localisation of Thunderbird</kk>
   <ko>Lithuanian localisation of Thunderbird</ko>
   <ku>Lithuanian localisation of Thunderbird</ku>
   <lt>Lithuanian localisation of Thunderbird</lt>
   <mk>Lithuanian localisation of Thunderbird</mk>
   <mr>Lithuanian localisation of Thunderbird</mr>
   <nb_NO>Lithuanian localisation of Thunderbird</nb_NO>
   <nb>Litauisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Lithuanian localisation of Thunderbird</nl_BE>
   <nl>Litouwse lokalisatie van Thunderbird</nl>
   <or>Lithuanian localisation of Thunderbird</or>
   <pl>Litewska lokalizacja Thunderbirda</pl>
   <pt_BR>Lituano Localização para o Thunderbird</pt_BR>
   <pt>Lituano Localização para Thunderbird</pt>
   <ro>Lithuanian localisation of Thunderbird</ro>
   <ru>Lithuanian localisation of Thunderbird</ru>
   <sk>Lithuanian localisation of Thunderbird</sk>
   <sl>Lithuanian localisation of Thunderbird</sl>
   <so>Lithuanian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në lituanisht</sq>
   <sr>Lithuanian localisation of Thunderbird</sr>
   <sv>Litauisk lokalisering av Thunderbird </sv>
   <th>Lithuanian localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Litvanyaca yerelleştirmesi</tr>
   <uk>Lithuanian локалізація Thunderbird</uk>
   <vi>Lithuanian localisation of Thunderbird</vi>
   <zh_CN>Lithuanian localisation of Thunderbird</zh_CN>
   <zh_HK>Lithuanian localisation of Thunderbird</zh_HK>
   <zh_TW>Lithuanian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-lt
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-lt
</uninstall_package_names>

</app>
