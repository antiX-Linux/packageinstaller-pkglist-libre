<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Indonesian_Thunderbird
</name>

<description>
   <am>Indonesian localisation of Thunderbird</am>
   <ar>Indonesian localisation of Thunderbird</ar>
   <be>Indonesian localisation of Thunderbird</be>
   <bg>Indonesian localisation of Thunderbird</bg>
   <bn>Indonesian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Indonesi</ca>
   <cs>Indonesian localisation of Thunderbird</cs>
   <da>Indonesian localisation of Thunderbird</da>
   <de>Indonesische Lokalisation von Thunderbird</de>
   <el>Ινδονησιακά για το Thunderbird</el>
   <en>Indonesian localisation of Thunderbird</en>
   <es_ES>Localización Indonesia de Thunderbird</es_ES>
   <es>Localización Indonesio de Thunderbird</es>
   <et>Indonesian localisation of Thunderbird</et>
   <eu>Indonesian localisation of Thunderbird</eu>
   <fa>Indonesian localisation of Thunderbird</fa>
   <fil_PH>Indonesian localisation of Thunderbird</fil_PH>
   <fi>Indonesian localisation of Thunderbird</fi>
   <fr_BE>Localisation en indonésien pour Thunderbird</fr_BE>
   <fr>Localisation en indonésien pour Thunderbird</fr>
   <gl_ES>Indonesian localisation of Thunderbird</gl_ES>
   <gu>Indonesian localisation of Thunderbird</gu>
   <he_IL>Indonesian localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का इन्डोनेशियाई संस्करण</hi>
   <hr>Indonesian localisation of Thunderbird</hr>
   <hu>Indonesian localisation of Thunderbird</hu>
   <id>Indonesian localisation of Thunderbird</id>
   <is>Indonesian localisation of Thunderbird</is>
   <it>Localizzazione indonesiana di Thunderbird</it>
   <ja>インドネシア語版 Thunderbird</ja>
   <kk>Indonesian localisation of Thunderbird</kk>
   <ko>Indonesian localisation of Thunderbird</ko>
   <ku>Indonesian localisation of Thunderbird</ku>
   <lt>Indonesian localisation of Thunderbird</lt>
   <mk>Indonesian localisation of Thunderbird</mk>
   <mr>Indonesian localisation of Thunderbird</mr>
   <nb_NO>Indonesian localisation of Thunderbird</nb_NO>
   <nb>Indonesisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Indonesian localisation of Thunderbird</nl_BE>
   <nl>Indonesian localisation of Thunderbird</nl>
   <or>Indonesian localisation of Thunderbird</or>
   <pl>Indonesian localisation of Thunderbird</pl>
   <pt_BR>Indonésio Localização para o Thunderbird</pt_BR>
   <pt>Indonésio Localização para Thunderbird</pt>
   <ro>Indonesian localisation of Thunderbird</ro>
   <ru>Indonesian localisation of Thunderbird</ru>
   <sk>Indonesian localisation of Thunderbird</sk>
   <sl>Indonezijske krajevne nastavitve za Thunderbird</sl>
   <so>Indonesian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në indonezisht</sq>
   <sr>Indonesian localisation of Thunderbird</sr>
   <sv>Indonesisk lokalisering av Thunderbird</sv>
   <th>Indonesian localisation of Thunderbird</th>
   <tr>Thunderbird'ün Endonezce yerelleştirmesi</tr>
   <uk>Indonesian localisation of Thunderbird</uk>
   <vi>Indonesian localisation of Thunderbird</vi>
   <zh_CN>Indonesian localisation of Thunderbird</zh_CN>
   <zh_HK>Indonesian localisation of Thunderbird</zh_HK>
   <zh_TW>Indonesian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-id
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-id
</uninstall_package_names>

</app>
