<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Irish_Thunderbird
</name>

<description>
   <am>Irish localisation of Thunderbird</am>
   <ar>Irish localisation of Thunderbird</ar>
   <be>Irish localisation of Thunderbird</be>
   <bg>Irish localisation of Thunderbird</bg>
   <bn>Irish localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Irlandès</ca>
   <cs>Irish localisation of Thunderbird</cs>
   <da>Irish localisation of Thunderbird</da>
   <de>Irische Lokalisation von Thunderbird</de>
   <el>Ιρλανδικά για Firefox ESR</el>
   <en>Irish localisation of Thunderbird</en>
   <es_ES>Localización Irlandés de Thunderbird</es_ES>
   <es>Localización Irlandés de Thunderbird</es>
   <et>Irish localisation of Thunderbird</et>
   <eu>Irish localisation of Thunderbird</eu>
   <fa>Irish localisation of Thunderbird</fa>
   <fil_PH>Irish localisation of Thunderbird</fil_PH>
   <fi>Irish localisation of Thunderbird</fi>
   <fr_BE>Localisation en irlandais pour Thunderbird</fr_BE>
   <fr>Localisation en irlandais pour Thunderbird</fr>
   <gl_ES>Irish localisation of Thunderbird</gl_ES>
   <gu>Irish localisation of Thunderbird</gu>
   <he_IL>Irish localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का आयरिश संस्करण</hi>
   <hr>Irish localisation of Thunderbird</hr>
   <hu>Irish localisation of Thunderbird</hu>
   <id>Irish localisation of Thunderbird</id>
   <is>Irish localisation of Thunderbird</is>
   <it>Localizzazione irlandese di Thunderbird</it>
   <ja>アイルランド語版 Thunderbird</ja>
   <kk>Irish localisation of Thunderbird</kk>
   <ko>Irish localisation of Thunderbird</ko>
   <ku>Irish localisation of Thunderbird</ku>
   <lt>Irish localisation of Thunderbird</lt>
   <mk>Irish localisation of Thunderbird</mk>
   <mr>Irish localisation of Thunderbird</mr>
   <nb_NO>Irish localisation of Thunderbird</nb_NO>
   <nb>Irsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Irish localisation of Thunderbird</nl_BE>
   <nl>Irish localisation of Thunderbird</nl>
   <or>Irish localisation of Thunderbird</or>
   <pl>Irish localisation of Thunderbird</pl>
   <pt_BR>Irlandês Localização para o Thunderbird</pt_BR>
   <pt>Irlandês Localização para Thunderbird</pt>
   <ro>Irish localisation of Thunderbird</ro>
   <ru>Irish localisation of Thunderbird</ru>
   <sk>Irish localisation of Thunderbird</sk>
   <sl>Irske krajevne nastavitve za Thunderbird</sl>
   <so>Irish localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në irlandisht</sq>
   <sr>Irish localisation of Thunderbird</sr>
   <sv>Irländsk lokalisering av Thunderbird</sv>
   <th>Irish localisation of Thunderbird</th>
   <tr>Thunderbird'ün İrlandaca yerelleştirmesi</tr>
   <uk>Irish localisation of Thunderbird</uk>
   <vi>Irish localisation of Thunderbird</vi>
   <zh_CN>Irish localisation of Thunderbird</zh_CN>
   <zh_HK>Irish localisation of Thunderbird</zh_HK>
   <zh_TW>Irish localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-ga-ie
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-ga-ie
</uninstall_package_names>

</app>
