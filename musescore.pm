<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Audio
</category>

<name>
MuseScore
</name>

<description>
   <am>Music notation software</am>
   <ar>Music notation software</ar>
   <be>Music notation software</be>
   <bg>Music notation software</bg>
   <bn>Music notation software</bn>
   <ca>Programari de notació musical</ca>
   <cs>Music notation software</cs>
   <da>Music notation software</da>
   <de>Music notation software</de>
   <el>Λογισμικό μουσικής σημειογραφίας</el>
   <en>Music notation software</en>
   <es_ES>Programa de notación musical</es_ES>
   <es>Programa de notación musical</es>
   <et>Music notation software</et>
   <eu>Music notation software</eu>
   <fa>Music notation software</fa>
   <fil_PH>Music notation software</fil_PH>
   <fi>Music notation software</fi>
   <fr_BE>Logiciel de notation musicale</fr_BE>
   <fr>Logiciel de notation musicale</fr>
   <gl_ES>Music notation software</gl_ES>
   <gu>Music notation software</gu>
   <he_IL>Music notation software</he_IL>
   <hi>Music notation software</hi>
   <hr>Music notation software</hr>
   <hu>Music notation software</hu>
   <id>Music notation software</id>
   <is>Music notation software</is>
   <it>Software di notazione musicale</it>
   <ja>Music notation software</ja>
   <kk>Music notation software</kk>
   <ko>Music notation software</ko>
   <ku>Music notation software</ku>
   <lt>Music notation software</lt>
   <mk>Music notation software</mk>
   <mr>Music notation software</mr>
   <nb_NO>Music notation software</nb_NO>
   <nb>Noteprogram (musikalsk)</nb>
   <nl_BE>Music notation software</nl_BE>
   <nl>Music notation software</nl>
   <or>Music notation software</or>
   <pl>Music notation software</pl>
   <pt_BR>Programa aplicativo de notação musical</pt_BR>
   <pt>Software de notação musical</pt>
   <ro>Music notation software</ro>
   <ru>Music notation software</ru>
   <sk>Music notation software</sk>
   <sl>Programska oprema za pisanje not</sl>
   <so>Music notation software</so>
   <sq>Software shkrimi partiturash</sq>
   <sr>Music notation software</sr>
   <sv>Musiknotations-mjukvara</sv>
   <th>Music notation software</th>
   <tr>Music notation software</tr>
   <uk>Music notation software</uk>
   <vi>Music notation software</vi>
   <zh_CN>Music notation software</zh_CN>
   <zh_HK>Music notation software</zh_HK>
   <zh_TW>Music notation software</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
musescore
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
musescore
</uninstall_package_names>
</app>
