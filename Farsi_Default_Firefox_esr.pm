<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Farsi_Default_Firefox_esr
</name>

<description>
   <am>Farsi localisation of Firefox ESR</am>
   <ar>Farsi localisation of Firefox ESR</ar>
   <be>Farsi localisation of Firefox ESR</be>
   <bg>Farsi localisation of Firefox ESR</bg>
   <bn>Farsi localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Farsi</ca>
   <cs>Farsi localisation of Firefox ESR</cs>
   <da>Farsi localisation of Firefox ESR</da>
   <de>Persische Lokalisation (Farsi) von “Firefox ESR”</de>
   <el>Farsi για Firefox ESR</el>
   <en>Farsi localisation of Firefox ESR</en>
   <es_ES>Localización Farsi de Firefox ESR</es_ES>
   <es>Localización Farsi de Firefox ESR</es>
   <et>Farsi localisation of Firefox ESR</et>
   <eu>Farsi localisation of Firefox ESR</eu>
   <fa>Farsi localisation of Firefox ESR</fa>
   <fil_PH>Farsi localisation of Firefox ESR</fil_PH>
   <fi>Farsi localisation of Firefox ESR</fi>
   <fr_BE>Localisation en farsi pour Firefox ESR</fr_BE>
   <fr>Localisation en farsi pour Firefox ESR</fr>
   <gl_ES>Farsi localisation of Firefox ESR</gl_ES>
   <gu>Farsi localisation of Firefox ESR</gu>
   <he_IL>Farsi localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का फ़ारसी संस्करण</hi>
   <hr>Farsi localisation of Firefox ESR</hr>
   <hu>Farsi localisation of Firefox ESR</hu>
   <id>Farsi localisation of Firefox ESR</id>
   <is>Farsi localisation of Firefox ESR</is>
   <it>Localizzazione persiana di Firefox ESR</it>
   <ja>ペルシャ語版 Firefox ESR</ja>
   <kk>Farsi localisation of Firefox ESR</kk>
   <ko>Farsi localisation of Firefox ESR</ko>
   <ku>Farsi localisation of Firefox ESR</ku>
   <lt>Farsi localisation of Firefox ESR</lt>
   <mk>Farsi localisation of Firefox ESR</mk>
   <mr>Farsi localisation of Firefox ESR</mr>
   <nb_NO>Farsi localisation of Firefox ESR</nb_NO>
   <nb>Farsi lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Farsi localisation of Firefox ESR</nl_BE>
   <nl>Perzische lokalisatie van Firefox ESR</nl>
   <or>Farsi localisation of Firefox ESR</or>
   <pl>Farsi localisation of Firefox ESR</pl>
   <pt_BR>Persa Localização para o Firefox ESR</pt_BR>
   <pt>Persa Localização para Firefox ESR</pt>
   <ro>Farsi localisation of Firefox ESR</ro>
   <ru>Farsi localisation of Firefox ESR</ru>
   <sk>Farsi localisation of Firefox ESR</sk>
   <sl>Farsi krajevne nastavitve za Firefox ESR</sl>
   <so>Farsi localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në persisht</sq>
   <sr>Farsi localisation of Firefox ESR</sr>
   <sv>Farsi lokalisering av Firefox ESR</sv>
   <th>Farsi localisation of Firefox ESR</th>
   <tr>Firefox ESR Farsça yerelleştirmesi</tr>
   <uk>Farsi localisation of Firefox ESR</uk>
   <vi>Farsi localisation of Firefox ESR</vi>
   <zh_CN>Farsi localisation of Firefox ESR</zh_CN>
   <zh_HK>Farsi localisation of Firefox ESR</zh_HK>
   <zh_TW>Farsi localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-fa
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-fa
</uninstall_package_names>

</app>
