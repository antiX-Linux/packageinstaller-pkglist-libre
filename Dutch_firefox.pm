<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Dutch_Firefox
</name>

<description>
   <am>Dutch localisation of Firefox</am>
   <ar>Dutch localisation of Firefox</ar>
   <be>Dutch localisation of Firefox</be>
   <bg>Dutch localisation of Firefox</bg>
   <bn>Dutch localisation of Firefox</bn>
   <ca>Localització de Firefox en Holandès</ca>
   <cs>Dutch localisation of Firefox</cs>
   <da>Hollandsk oversættelse af Firefox</da>
   <de>Niederländische Lokalisierung von Firefox</de>
   <el>Ολλανδικά για το Firefox</el>
   <en>Dutch localisation of Firefox</en>
   <es_ES>Localización Holandesa de Firefox</es_ES>
   <es>Localización Holandés de Firefox</es>
   <et>Dutch localisation of Firefox</et>
   <eu>Dutch localisation of Firefox</eu>
   <fa>Dutch localisation of Firefox</fa>
   <fil_PH>Dutch localisation of Firefox</fil_PH>
   <fi>Hollantilainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en néerlandais pour Firefox</fr_BE>
   <fr>Localisation en néerlandais pour Firefox</fr>
   <gl_ES>Localización de Firefox en neerlandés</gl_ES>
   <gu>Dutch localisation of Firefox</gu>
   <he_IL>Dutch localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का डच संस्करण</hi>
   <hr>Dutch localisation of Firefox</hr>
   <hu>Dutch localisation of Firefox</hu>
   <id>Dutch localisation of Firefox</id>
   <is>Dutch localisation of Firefox</is>
   <it>Localizzazione olandese di Firefox</it>
   <ja>Firefox のオランダ語版</ja>
   <kk>Dutch localisation of Firefox</kk>
   <ko>Dutch localisation of Firefox</ko>
   <ku>Dutch localisation of Firefox</ku>
   <lt>Dutch localisation of Firefox</lt>
   <mk>Dutch localisation of Firefox</mk>
   <mr>Dutch localisation of Firefox</mr>
   <nb_NO>Dutch localisation of Firefox</nb_NO>
   <nb>Nederlandsk lokaltilpassing av Firefox</nb>
   <nl_BE>Dutch localisation of Firefox</nl_BE>
   <nl>Nederlandse lokalisatie van Firefox</nl>
   <or>Dutch localisation of Firefox</or>
   <pl>Holenderska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Holandês Localização para o Firefox</pt_BR>
   <pt>Holandês Localização para Firefox</pt>
   <ro>Dutch localisation of Firefox</ro>
   <ru>Голландская локализация Firefox</ru>
   <sk>Dutch localisation of Firefox</sk>
   <sl>Dutch localisation of Firefox</sl>
   <so>Dutch localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në holandisht</sq>
   <sr>Dutch localisation of Firefox</sr>
   <sv>Holländsk lokalisering av Firefox</sv>
   <th>Dutch localisation ของ Firefox</th>
   <tr>Firefox Flemenkçe yerelleştirmesi</tr>
   <uk>Dutch локалізація Firefox</uk>
   <vi>Dutch localisation of Firefox</vi>
   <zh_CN>Firefox 荷兰语语言包</zh_CN>
   <zh_HK>Dutch localisation of Firefox</zh_HK>
   <zh_TW>Dutch localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-nl
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-nl
</uninstall_package_names>
</app>
