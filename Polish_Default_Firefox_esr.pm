<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Polish_Default_Firefox_esr
</name>

<description>
   <am>Polish localisation of Firefox ESR</am>
   <ar>Polish localisation of Firefox ESR</ar>
   <be>Polish localisation of Firefox ESR</be>
   <bg>Polish localisation of Firefox ESR</bg>
   <bn>Polish localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Polonès</ca>
   <cs>Polish localisation of Firefox ESR</cs>
   <da>Polish localisation of Firefox ESR</da>
   <de>Polnische Lokalisation von “Firefox ESR”</de>
   <el>Πολωνικά για Firefox ESR</el>
   <en>Polish localisation of Firefox ESR</en>
   <es_ES>Localización Polaco de Firefox ESR</es_ES>
   <es>Localización Polaco de Firefox ESR</es>
   <et>Polish localisation of Firefox ESR</et>
   <eu>Polish localisation of Firefox ESR</eu>
   <fa>Polish localisation of Firefox ESR</fa>
   <fil_PH>Polish localisation of Firefox ESR</fil_PH>
   <fi>Polish localisation of Firefox ESR</fi>
   <fr_BE>Localisation en polonais pour Firefox ESR</fr_BE>
   <fr>Localisation en polonais pour Firefox ESR</fr>
   <gl_ES>Polish localisation of Firefox ESR</gl_ES>
   <gu>Polish localisation of Firefox ESR</gu>
   <he_IL>Polish localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का पोलिश संस्करण</hi>
   <hr>Polish localisation of Firefox ESR</hr>
   <hu>Polish localisation of Firefox ESR</hu>
   <id>Polish localisation of Firefox ESR</id>
   <is>Polish localisation of Firefox ESR</is>
   <it>Localizzazione polacca di Firefox ESR</it>
   <ja>ポーランド語版 Firefox ESR</ja>
   <kk>Polish localisation of Firefox ESR</kk>
   <ko>Polish localisation of Firefox ESR</ko>
   <ku>Polish localisation of Firefox ESR</ku>
   <lt>Polish localisation of Firefox ESR</lt>
   <mk>Polish localisation of Firefox ESR</mk>
   <mr>Polish localisation of Firefox ESR</mr>
   <nb_NO>Polish localisation of Firefox ESR</nb_NO>
   <nb>Polsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Polish localisation of Firefox ESR</nl_BE>
   <nl>Polish localisation of Firefox ESR</nl>
   <or>Polish localisation of Firefox ESR</or>
   <pl>Polish localisation of Firefox ESR</pl>
   <pt_BR>Polonês Localização para o Firefox ESR</pt_BR>
   <pt>Polaco Localização para Firefox ESR</pt>
   <ro>Polish localisation of Firefox ESR</ro>
   <ru>Polish localisation of Firefox ESR</ru>
   <sk>Polish localisation of Firefox ESR</sk>
   <sl>Poljske krajevne nastavitve za Firefox ESR</sl>
   <so>Polish localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në polonisht</sq>
   <sr>Polish localisation of Firefox ESR</sr>
   <sv>Polsk lokalisering av Firefox ESR</sv>
   <th>Polish localisation of Firefox ESR</th>
   <tr>Firefox ESR Lehçe yerelleştirmesi</tr>
   <uk>Polish localisation of Firefox ESR</uk>
   <vi>Polish localisation of Firefox ESR</vi>
   <zh_CN>Polish localisation of Firefox ESR</zh_CN>
   <zh_HK>Polish localisation of Firefox ESR</zh_HK>
   <zh_TW>Polish localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-pl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-pl
</uninstall_package_names>

</app>
