<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
FreeCol
</name>

<description>
   <en>Open source remake of the old Colonization</en>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
freecol
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
freecol
</uninstall_package_names>
</app>
