<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
French_Default_Firefox_esr
</name>

<description>
   <am>French localisation of Firefox ESR</am>
   <ar>French localisation of Firefox ESR</ar>
   <be>French localisation of Firefox ESR</be>
   <bg>French localisation of Firefox ESR</bg>
   <bn>French localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Francès</ca>
   <cs>French localisation of Firefox ESR</cs>
   <da>French localisation of Firefox ESR</da>
   <de>Französische Lokalisation von “Firefox ESR”</de>
   <el>Γαλλικά για Firefox ESR</el>
   <en>French localisation of Firefox ESR</en>
   <es_ES>Localización Francés de Firefox ESR</es_ES>
   <es>Localización Francés de Firefox ESR</es>
   <et>French localisation of Firefox ESR</et>
   <eu>French localisation of Firefox ESR</eu>
   <fa>French localisation of Firefox ESR</fa>
   <fil_PH>French localisation of Firefox ESR</fil_PH>
   <fi>French localisation of Firefox ESR</fi>
   <fr_BE>Localisation en français pour Firefox ESR</fr_BE>
   <fr>Localisation en français pour Firefox ESR</fr>
   <gl_ES>French localisation of Firefox ESR</gl_ES>
   <gu>French localisation of Firefox ESR</gu>
   <he_IL>French localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का फ्रेंच संस्करण</hi>
   <hr>French localisation of Firefox ESR</hr>
   <hu>French localisation of Firefox ESR</hu>
   <id>French localisation of Firefox ESR</id>
   <is>French localisation of Firefox ESR</is>
   <it>Localizzazione francese di Firefox ESR</it>
   <ja>フランス語版 Firefox ESR</ja>
   <kk>French localisation of Firefox ESR</kk>
   <ko>French localisation of Firefox ESR</ko>
   <ku>French localisation of Firefox ESR</ku>
   <lt>French localisation of Firefox ESR</lt>
   <mk>French localisation of Firefox ESR</mk>
   <mr>French localisation of Firefox ESR</mr>
   <nb_NO>French localisation of Firefox ESR</nb_NO>
   <nb>Fransk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>French localisation of Firefox ESR</nl_BE>
   <nl>Franse lokalisatie van Firefox ESR</nl>
   <or>French localisation of Firefox ESR</or>
   <pl>French localisation of Firefox ESR</pl>
   <pt_BR>Francês Localização para o Firefox ESR</pt_BR>
   <pt>Francês Localização para Firefox ESR</pt>
   <ro>French localisation of Firefox ESR</ro>
   <ru>French localisation of Firefox ESR</ru>
   <sk>French localisation of Firefox ESR</sk>
   <sl>Francoske krajevne nastavitve za Firefox ESR</sl>
   <so>French localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në frëngjisht</sq>
   <sr>French localisation of Firefox ESR</sr>
   <sv>Fransk lokalisering av Firefox ESR</sv>
   <th>French localisation of Firefox ESR</th>
   <tr>Firefox ESR Fransızca yerelleştirmesi</tr>
   <uk>French localisation of Firefox ESR</uk>
   <vi>French localisation of Firefox ESR</vi>
   <zh_CN>French localisation of Firefox ESR</zh_CN>
   <zh_HK>French localisation of Firefox ESR</zh_HK>
   <zh_TW>French localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-fr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-fr
</uninstall_package_names>

</app>
