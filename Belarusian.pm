<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Belarusian
</name>

<description>
   <am>Belarusian dictionary for hunspell</am>
   <ar>Belarusian dictionary for hunspell</ar>
   <be>Belarusian dictionary for hunspell</be>
   <bg>Belarusian dictionary for hunspell</bg>
   <bn>Belarusian dictionary for hunspell</bn>
   <ca>Diccionari Bielorús per hunspell</ca>
   <cs>Belarusian dictionary for hunspell</cs>
   <da>Belarusian dictionary for hunspell</da>
   <de>Weißrussisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Λευκορωσικά για hunspell</el>
   <en>Belarusian dictionary for hunspell</en>
   <es_ES>Diccionario Bielorruso para hunspell</es_ES>
   <es>Diccionario Bielorruso para hunspell</es>
   <et>Belarusian dictionary for hunspell</et>
   <eu>Belarusian dictionary for hunspell</eu>
   <fa>Belarusian dictionary for hunspell</fa>
   <fil_PH>Belarusian dictionary for hunspell</fil_PH>
   <fi>Belarusian dictionary for hunspell</fi>
   <fr_BE>Biélorusse dictionnaire pour hunspell</fr_BE>
   <fr>Biélorusse dictionnaire pour hunspell</fr>
   <gl_ES>Belarusian dictionary for hunspell</gl_ES>
   <gu>Belarusian dictionary for hunspell</gu>
   <he_IL>Belarusian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु बेलारूसी शब्दकोष</hi>
   <hr>Belarusian dictionary for hunspell</hr>
   <hu>Belarusian dictionary for hunspell</hu>
   <id>Belarusian dictionary for hunspell</id>
   <is>Belarusian dictionary for hunspell</is>
   <it>Dizionario bielorusso per hunspell</it>
   <ja>Hunspell 用ベラルーシ語辞書</ja>
   <kk>Belarusian dictionary for hunspell</kk>
   <ko>Belarusian dictionary for hunspell</ko>
   <ku>Belarusian dictionary for hunspell</ku>
   <lt>Belarusian dictionary for hunspell</lt>
   <mk>Belarusian dictionary for hunspell</mk>
   <mr>Belarusian dictionary for hunspell</mr>
   <nb_NO>Belarusian dictionary for hunspell</nb_NO>
   <nb>Hviterussisk ordliste for hunspell</nb>
   <nl_BE>Belarusian dictionary for hunspell</nl_BE>
   <nl>Wit-Russisch woordenboek voor hunspell</nl>
   <or>Belarusian dictionary for hunspell</or>
   <pl>Belarusian dictionary for hunspell</pl>
   <pt_BR>Dicionário Bielorrusso para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Bielorusso para hunspell</pt>
   <ro>Belarusian dictionary for hunspell</ro>
   <ru>Belarusian dictionary for hunspell</ru>
   <sk>Belarusian dictionary for hunspell</sk>
   <sl>Beloruski slovar za hunspell</sl>
   <so>Belarusian dictionary for hunspell</so>
   <sq>Fjalor bjellorusisht për hunspell</sq>
   <sr>Belarusian dictionary for hunspell</sr>
   <sv>Belarusisk ordbok för hunspell</sv>
   <th>Belarusian dictionary for hunspell</th>
   <tr>Hunspell için Beyaz Rusça sözlük</tr>
   <uk>Belarusian dictionary for hunspell</uk>
   <vi>Belarusian dictionary for hunspell</vi>
   <zh_CN>Belarusian dictionary for hunspell</zh_CN>
   <zh_HK>Belarusian dictionary for hunspell</zh_HK>
   <zh_TW>Belarusian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-be
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-be
</uninstall_package_names>

</app>
