<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Office
</category>

<name>
Dictionary
</name>

<description>
   <am>OpenDict Dictionary</am>
   <ar>OpenDict Dictionary</ar>
   <be>OpenDict Dictionary</be>
   <bg>OpenDict Dictionary</bg>
   <bn>OpenDict Dictionary</bn>
   <ca>Diccionari OpenDict</ca>
   <cs>OpenDict Dictionary</cs>
   <da>OpenDict-ordbog</da>
   <de>OpenDict Wörterbuch</de>
   <el>Λεξικό OpenDict</el>
   <en>OpenDict Dictionary</en>
   <es_ES>Diccionario OpenDict</es_ES>
   <es>Diccionario OpenDict</es>
   <et>OpenDict Dictionary</et>
   <eu>OpenDict Dictionary</eu>
   <fa>OpenDict Dictionary</fa>
   <fil_PH>OpenDict Dictionary</fil_PH>
   <fi>OpenDict-sanakirja</fi>
   <fr_BE>Dictionnaire OpenDict</fr_BE>
   <fr>Dictionnaire OpenDict</fr>
   <gl_ES>Dicionario OpenDict</gl_ES>
   <gu>OpenDict Dictionary</gu>
   <he_IL>OpenDict Dictionary</he_IL>
   <hi>OpenDict शब्दकोष</hi>
   <hr>OpenDict Dictionary</hr>
   <hu>OpenDict Dictionary</hu>
   <id>OpenDict Dictionary</id>
   <is>OpenDict Dictionary</is>
   <it>Dizionario OpenDict</it>
   <ja>OpenDict 辞書</ja>
   <kk>OpenDict Dictionary</kk>
   <ko>OpenDict Dictionary</ko>
   <ku>OpenDict Dictionary</ku>
   <lt>OpenDict žodynas</lt>
   <mk>OpenDict Dictionary</mk>
   <mr>OpenDict Dictionary</mr>
   <nb_NO>OpenDict Dictionary</nb_NO>
   <nb>OpenDict ordbok</nb>
   <nl_BE>OpenDict Dictionary</nl_BE>
   <nl>OpenDict Woordenboek</nl>
   <or>OpenDict Dictionary</or>
   <pl>słownik OpenDict</pl>
   <pt_BR>Dicionário OpenDict</pt_BR>
   <pt>Dicionário OpenDict</pt>
   <ro>OpenDict Dictionary</ro>
   <ru>Словарь OpenDict</ru>
   <sk>OpenDict Dictionary</sk>
   <sl>OpenDict slovar</sl>
   <so>OpenDict Dictionary</so>
   <sq>Fjalor OpenDict</sq>
   <sr>OpenDict Dictionary</sr>
   <sv>OpenDict Ordbok</sv>
   <th>พจนานุกรท OpenDict</th>
   <tr>OpenDict Sözlük</tr>
   <uk>Словник OpenDict</uk>
   <vi>OpenDict Dictionary</vi>
   <zh_CN>OpenDict Dictionary</zh_CN>
   <zh_HK>OpenDict Dictionary</zh_HK>
   <zh_TW>OpenDict Dictionary</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
opendict
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
opendict
</uninstall_package_names>
</app>
