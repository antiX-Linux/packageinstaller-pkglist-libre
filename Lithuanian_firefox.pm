<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Lithuanian_Firefox
</name>

<description>
   <am>Lithuanian localisation of Firefox</am>
   <ar>Lithuanian localisation of Firefox</ar>
   <be>Lithuanian localisation of Firefox</be>
   <bg>Lithuanian localisation of Firefox</bg>
   <bn>Lithuanian localisation of Firefox</bn>
   <ca>Localització de Firefox en Lituà</ca>
   <cs>Lithuanian localisation of Firefox</cs>
   <da>Litauisk oversættelse af Firefox</da>
   <de>Litauische Lokalisierung von Firefox</de>
   <el>Λιθουανικά για το Firefox</el>
   <en>Lithuanian localisation of Firefox</en>
   <es_ES>Localización Lituana de Firefox</es_ES>
   <es>Localización Lituano de Firefox</es>
   <et>Lithuanian localisation of Firefox</et>
   <eu>Lithuanian localisation of Firefox</eu>
   <fa>Lithuanian localisation of Firefox</fa>
   <fil_PH>Lithuanian localisation of Firefox</fil_PH>
   <fi>Liettualainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en lituanien pour Firefox</fr_BE>
   <fr>Localisation en lituanien pour Firefox</fr>
   <gl_ES>Localización do Firefox ao lituan</gl_ES>
   <gu>Lithuanian localisation of Firefox</gu>
   <he_IL>Lithuanian localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का लिथुआनियाई संस्करण</hi>
   <hr>Lithuanian localisation of Firefox</hr>
   <hu>Lithuanian localisation of Firefox</hu>
   <id>Lithuanian localisation of Firefox</id>
   <is>Lithuanian localisation of Firefox</is>
   <it>Localizzazione lituana di Firefox</it>
   <ja>Firefox のリトアニア語版</ja>
   <kk>Lithuanian localisation of Firefox</kk>
   <ko>Lithuanian localisation of Firefox</ko>
   <ku>Lithuanian localisation of Firefox</ku>
   <lt>Lithuanian localisation of Firefox</lt>
   <mk>Lithuanian localisation of Firefox</mk>
   <mr>Lithuanian localisation of Firefox</mr>
   <nb_NO>Lithuanian localisation of Firefox</nb_NO>
   <nb>Litauisk lokaltilpassing av Firefox</nb>
   <nl_BE>Lithuanian localisation of Firefox</nl_BE>
   <nl>Litouwse lokalisatie van Firefox</nl>
   <or>Lithuanian localisation of Firefox</or>
   <pl>Litewska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Lituano Localização para o Firefox</pt_BR>
   <pt>Lituano Localização para Firefox</pt>
   <ro>Lithuanian localisation of Firefox</ro>
   <ru>Литовская локализация Firefox</ru>
   <sk>Lithuanian localisation of Firefox</sk>
   <sl>Litvanske krajevne nastavitve za Firefox</sl>
   <so>Lithuanian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në lituanisht</sq>
   <sr>Lithuanian localisation of Firefox</sr>
   <sv>Litauisk lokalisering av Firefox</sv>
   <th>Lithuanian localisation ของFirefox</th>
   <tr>Firefox'un Litvanyaca yerelleştirmesi</tr>
   <uk>Lithuanian локалізація Firefox</uk>
   <vi>Lithuanian localisation of Firefox</vi>
   <zh_CN>Lithuanian localisation of Firefox</zh_CN>
   <zh_HK>Lithuanian localisation of Firefox</zh_HK>
   <zh_TW>Lithuanian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-lt
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-lt
</uninstall_package_names>
</app>
