<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Development
</category>

<name>
atom
</name>

<description>
   <am>atom text editor IDE</am>
   <ar>atom text editor IDE</ar>
   <be>atom text editor IDE</be>
   <bg>atom text editor IDE</bg>
   <bn>atom text editor IDE</bn>
   <ca>Editor de text IDE  atom</ca>
   <cs>atom text editor IDE</cs>
   <da>atom-tekstredigerings-IDE</da>
   <de>IDE des Atom Textprogramms</de>
   <el>Πρόγραμμα επεξεργασίας κειμένου IDE</el>
   <en>atom text editor IDE</en>
   <es_ES>Editor de texto atom IDE</es_ES>
   <es>Editor de texto atom IDE</es>
   <et>atom text editor IDE</et>
   <eu>atom text editor IDE</eu>
   <fa>atom text editor IDE</fa>
   <fil_PH>atom text editor IDE</fil_PH>
   <fi>atom-tekstimuokkain sekä ohjelmointiympäristö</fi>
   <fr_BE>IDE pour l'éditeur de texte Atom</fr_BE>
   <fr>IDE pour l'éditeur de texte Atom</fr>
   <gl_ES>Editor de texto</gl_ES>
   <gu>atom text editor IDE</gu>
   <he_IL>atom text editor IDE</he_IL>
   <hi>एटम टेक्स्ट संपादक आईडीई</hi>
   <hr>atom text editor IDE</hr>
   <hu>atom text editor IDE</hu>
   <id>atom text editor IDE</id>
   <is>atom text editor IDE</is>
   <it>atom editor di testo IDE</it>
   <ja>Atom IDE テキストエディター</ja>
   <kk>atom text editor IDE</kk>
   <ko>atom text editor IDE</ko>
   <ku>atom text editor IDE</ku>
   <lt>atom text editor IDE</lt>
   <mk>atom text editor IDE</mk>
   <mr>atom text editor IDE</mr>
   <nb_NO>atom text editor IDE</nb_NO>
   <nb>atom tekstprogram (IDE)</nb>
   <nl_BE>atom text editor IDE</nl_BE>
   <nl>atom text editor IDE</nl>
   <or>atom text editor IDE</or>
   <pl>edytor tekstu atom IDE</pl>
   <pt_BR>Editor de texto IDE (Integrated Development Environment)</pt_BR>
   <pt>Editor de texto / IDE (Integrated Development Environment)</pt>
   <ro>atom text editor IDE</ro>
   <ru>Текстовый редактор и среда разработки Atom</ru>
   <sk>atom textový editor a IDE</sk>
   <sl>atom IDE urejevalnik teksta</sl>
   <so>atom text editor IDE</so>
   <sq>Përpunuesi i teksteve dhe IDE, Atom</sq>
   <sr>atom text editor IDE</sr>
   <sv>atom textredigerare IDE</sv>
   <th>Atom โปรแกรมแก้ไขข้อความแบบ IDE</th>
   <tr>IDE atom metin düzenleyicisi</tr>
   <uk>atom text editor IDE</uk>
   <vi>atom text editor IDE</vi>
   <zh_CN>atom 文本编辑 IDE</zh_CN>
   <zh_HK>atom text editor IDE</zh_HK>
   <zh_TW>atom text editor IDE</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | apt-key add -
echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main">/etc/apt/sources.list.d/atom.list
apt-get update
</preinstall>

<install_package_names>
atom
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
atom
</uninstall_package_names>
</app>
