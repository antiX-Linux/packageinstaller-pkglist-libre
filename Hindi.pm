<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hindi
</name>

<description>
   <am>Hindi dictionary for hunspell</am>
   <ar>Hindi dictionary for hunspell</ar>
   <be>Hindi dictionary for hunspell</be>
   <bg>Hindi dictionary for hunspell</bg>
   <bn>Hindi dictionary for hunspell</bn>
   <ca>Diccionari Hindi per hunspell</ca>
   <cs>Hindi dictionary for hunspell</cs>
   <da>Hindi dictionary for hunspell</da>
   <de>Indisches (hindi) Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Χίντι για hunspell</el>
   <en>Hindi dictionary for hunspell</en>
   <es_ES>Diccionario Hindi para hunspell</es_ES>
   <es>Diccionario Hindi para hunspell</es>
   <et>Hindi dictionary for hunspell</et>
   <eu>Hindi dictionary for hunspell</eu>
   <fa>Hindi dictionary for hunspell</fa>
   <fil_PH>Hindi dictionary for hunspell</fil_PH>
   <fi>Hindi dictionary for hunspell</fi>
   <fr_BE>Hindi dictionnaire pour hunspell</fr_BE>
   <fr>Hindi dictionnaire pour hunspell</fr>
   <gl_ES>Hindi dictionary for hunspell</gl_ES>
   <gu>Hindi dictionary for hunspell</gu>
   <he_IL>Hindi dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु हिंदी शब्दकोष</hi>
   <hr>Hindi dictionary for hunspell</hr>
   <hu>Hindi dictionary for hunspell</hu>
   <id>Hindi dictionary for hunspell</id>
   <is>Hindi dictionary for hunspell</is>
   <it>Dizionario di lingua hindi per hunspell</it>
   <ja>Hunspell 用ヒンディー語辞書</ja>
   <kk>Hindi dictionary for hunspell</kk>
   <ko>Hindi dictionary for hunspell</ko>
   <ku>Hindi dictionary for hunspell</ku>
   <lt>Hindi dictionary for hunspell</lt>
   <mk>Hindi dictionary for hunspell</mk>
   <mr>Hindi dictionary for hunspell</mr>
   <nb_NO>Hindi dictionary for hunspell</nb_NO>
   <nb>Hindi ordliste for hunspell</nb>
   <nl_BE>Hindi dictionary for hunspell</nl_BE>
   <nl>Hindi dictionary for hunspell</nl>
   <or>Hindi dictionary for hunspell</or>
   <pl>Hindi dictionary for hunspell</pl>
   <pt_BR>Dicionário Hindi para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Hindi para hunspell</pt>
   <ro>Hindi dictionary for hunspell</ro>
   <ru>Hindi dictionary for hunspell</ru>
   <sk>Hindi dictionary for hunspell</sk>
   <sl>Hindujski slovar za hunspell</sl>
   <so>Hindi dictionary for hunspell</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në hinduisht</sq>
   <sr>Hindi dictionary for hunspell</sr>
   <sv>Hindi ordbok för hunspell</sv>
   <th>Hindi dictionary for hunspell</th>
   <tr>Hunspell için Hintçe sözlük</tr>
   <uk>Hindi dictionary for hunspell</uk>
   <vi>Hindi dictionary for hunspell</vi>
   <zh_CN>Hindi dictionary for hunspell</zh_CN>
   <zh_HK>Hindi dictionary for hunspell</zh_HK>
   <zh_TW>Hindi dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-hi
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-hi
</uninstall_package_names>

</app>
