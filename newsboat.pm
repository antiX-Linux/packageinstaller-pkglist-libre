<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Newsreader
</category>

<name>
Newsboat
</name>

<description>
   <am>RSS/Atom feed reader for text terminals</am>
   <ar>RSS/Atom feed reader for text terminals</ar>
   <be>RSS/Atom feed reader for text terminals</be>
   <bg>RSS/Atom feed reader for text terminals</bg>
   <bn>RSS/Atom feed reader for text terminals</bn>
   <ca>Lector de canals RSS/Atom per terminals de text</ca>
   <cs>RSS/Atom feed reader for text terminals</cs>
   <da>RSS/Atom feed reader for text terminals</da>
   <de>RSS/Atom Aggregator für die Textkonsole</de>
   <el>Αναγνώστης τροφοδοσίας RSS/Atom για τερματικά κειμένου</el>
   <en>RSS/Atom feed reader for text terminals</en>
   <es_ES>Lector de noticias RSS/Atom para terminales de texto</es_ES>
   <es>Lector de noticias RSS / Atom para terminales de texto</es>
   <et>RSS/Atom feed reader for text terminals</et>
   <eu>RSS/Atom feed reader for text terminals</eu>
   <fa>RSS/Atom feed reader for text terminals</fa>
   <fil_PH>RSS/Atom feed reader for text terminals</fil_PH>
   <fi>RSS/Atom-syötteen lukija tekstipäätteelle</fi>
   <fr_BE>Lecteur de flux RSS/Atom pour les terminaux de texte</fr_BE>
   <fr>Lecteur de flux RSS/Atom pour les terminaux de texte</fr>
   <gl_ES>RSS/Atom feed reader for text terminals</gl_ES>
   <gu>RSS/Atom feed reader for text terminals</gu>
   <he_IL>RSS/Atom feed reader for text terminals</he_IL>
   <hi>टेक्स्ट टर्मिनल हेतु आरएसएस/एटम समाचार स्रोत रीडर</hi>
   <hr>RSS/Atom feed reader for text terminals</hr>
   <hu>RSS/Atom feed reader for text terminals</hu>
   <id>RSS/Atom feed reader for text terminals</id>
   <is>RSS/Atom feed reader for text terminals</is>
   <it>Lettore di feed RSS/Atom per terminali di testo</it>
   <ja>テキスト端末用 RSS/Atomフィードリーダ</ja>
   <kk>RSS/Atom feed reader for text terminals</kk>
   <ko>RSS/Atom feed reader for text terminals</ko>
   <ku>RSS/Atom feed reader for text terminals</ku>
   <lt>RSS/Atom feed reader for text terminals</lt>
   <mk>RSS/Atom feed reader for text terminals</mk>
   <mr>RSS/Atom feed reader for text terminals</mr>
   <nb_NO>RSS/Atom feed reader for text terminals</nb_NO>
   <nb>RSS/Atom-nyhetsleser for tekstterminaler</nb>
   <nl_BE>RSS/Atom feed reader for text terminals</nl_BE>
   <nl>RSS/Atom feed reader for text terminals</nl>
   <or>RSS/Atom feed reader for text terminals</or>
   <pl>RSS/Atom feed reader for text terminals</pl>
   <pt_BR>Leitor RSS/Atom feed para terminais de texto</pt_BR>
   <pt>Leitor de feeds RSS/ATOM para terminais de texto</pt>
   <ro>RSS/Atom feed reader for text terminals</ro>
   <ru>RSS/Atom feed reader for text terminals</ru>
   <sk>RSS/Atom feed reader for text terminals</sk>
   <sl>Terminalski bralnik za RSS/Atom vire</sl>
   <so>RSS/Atom feed reader for text terminals</so>
   <sq>Lexues prurjesh RSS/Atom, për terminale teksti</sq>
   <sr>RSS/Atom feed reader for text terminals</sr>
   <sv>RSS/Atom feed läsare för textterminaler</sv>
   <th>RSS/Atom feed reader for text terminals</th>
   <tr>Metin uçbirimleri için RSS/Atom kaynak okuyucu</tr>
   <uk>RSS/Atom feed reader for text terminals</uk>
   <vi>RSS/Atom feed reader for text terminals</vi>
   <zh_CN>RSS/Atom feed reader for text terminals</zh_CN>
   <zh_HK>RSS/Atom feed reader for text terminals</zh_HK>
   <zh_TW>RSS/Atom feed reader for text terminals</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
newsboat
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
newsboat
</uninstall_package_names>
</app>
