<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Frisian_Thunderbird
</name>

<description>
   <am>Frisian localisation of Thunderbird</am>
   <ar>Frisian localisation of Thunderbird</ar>
   <be>Frisian localisation of Thunderbird</be>
   <bg>Frisian localisation of Thunderbird</bg>
   <bn>Frisian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Frisó</ca>
   <cs>Frisian localisation of Thunderbird</cs>
   <da>Frisian localisation of Thunderbird</da>
   <de>Niederländische Lokalisation (Westfriesisch) von “Thunderbird”</de>
   <el>Frisian για Thunderbird</el>
   <en>Frisian localisation of Thunderbird</en>
   <es_ES>Localización Frisio de Thunderbird</es_ES>
   <es>Localización Frisio de Thunderbird</es>
   <et>Frisian localisation of Thunderbird</et>
   <eu>Frisian localisation of Thunderbird</eu>
   <fa>Frisian localisation of Thunderbird</fa>
   <fil_PH>Frisian localisation of Thunderbird</fil_PH>
   <fi>Frisian localisation of Thunderbird</fi>
   <fr_BE>Localisation en frison pour Thunderbird</fr_BE>
   <fr>Localisation en frison pour Thunderbird</fr>
   <gl_ES>Frisian localisation of Thunderbird</gl_ES>
   <gu>Frisian localisation of Thunderbird</gu>
   <he_IL>Frisian localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का फ्रीसियाई संस्करण</hi>
   <hr>Frisian localisation of Thunderbird</hr>
   <hu>Frisian localisation of Thunderbird</hu>
   <id>Frisian localisation of Thunderbird</id>
   <is>Frisian localisation of Thunderbird</is>
   <it>Localizzazione in lingua frisone di Thunderbird</it>
   <ja>西フリジア語版 Thunderbird</ja>
   <kk>Frisian localisation of Thunderbird</kk>
   <ko>Frisian localisation of Thunderbird</ko>
   <ku>Frisian localisation of Thunderbird</ku>
   <lt>Frisian localisation of Thunderbird</lt>
   <mk>Frisian localisation of Thunderbird</mk>
   <mr>Frisian localisation of Thunderbird</mr>
   <nb_NO>Frisian localisation of Thunderbird</nb_NO>
   <nb>Frisisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Frisian localisation of Thunderbird</nl_BE>
   <nl>Friese lokalisatie van Thunderbird</nl>
   <or>Frisian localisation of Thunderbird</or>
   <pl>Frisian localisation of Thunderbird</pl>
   <pt_BR>Frísia Localização para o Thunderbird</pt_BR>
   <pt>Frísio Localização para Thunderbird</pt>
   <ro>Frisian localisation of Thunderbird</ro>
   <ru>Frisian localisation of Thunderbird</ru>
   <sk>Frisian localisation of Thunderbird</sk>
   <sl>Frizijske krajevne nastavitve za Thunderbird</sl>
   <so>Frisian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në frizisht</sq>
   <sr>Frisian localisation of Thunderbird</sr>
   <sv>Frisisk lokalisering av Thunderbird</sv>
   <th>Frisian localisation of Thunderbird</th>
   <tr>Thunderbird Frizye Dili yerelleştirmesi</tr>
   <uk>Frisian localisation of Thunderbird</uk>
   <vi>Frisian localisation of Thunderbird</vi>
   <zh_CN>Frisian localisation of Thunderbird</zh_CN>
   <zh_HK>Frisian localisation of Thunderbird</zh_HK>
   <zh_TW>Frisian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-fy-nl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-fy-nl
</uninstall_package_names>

</app>
