<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bosnian
</name>

<description>
   <am>Bosnian dictionary for hunspell</am>
   <ar>Bosnian dictionary for hunspell</ar>
   <be>Bosnian dictionary for hunspell</be>
   <bg>Bosnian dictionary for hunspell</bg>
   <bn>Bosnian dictionary for hunspell</bn>
   <ca>Diccionari Bosnià per hunspell</ca>
   <cs>Bosnian dictionary for hunspell</cs>
   <da>Bosnian dictionary for hunspell</da>
   <de>Bosnisches Wörterbuch für “Hunspell”</de>
   <el>Βοσνιακό λεξικό για hunspell</el>
   <en>Bosnian dictionary for hunspell</en>
   <es_ES>Diccionario Bosnio para hunspell</es_ES>
   <es>Diccionario Bosnio para hunspell</es>
   <et>Bosnian dictionary for hunspell</et>
   <eu>Bosnian dictionary for hunspell</eu>
   <fa>Bosnian dictionary for hunspell</fa>
   <fil_PH>Bosnian dictionary for hunspell</fil_PH>
   <fi>Bosnian dictionary for hunspell</fi>
   <fr_BE>Bosniaque dictionnaire pour hunspell</fr_BE>
   <fr>Bosniaque dictionnaire pour hunspell</fr>
   <gl_ES>Bosnian dictionary for hunspell</gl_ES>
   <gu>Bosnian dictionary for hunspell</gu>
   <he_IL>Bosnian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु बोस्नियाई शब्दकोष</hi>
   <hr>Bosnian dictionary for hunspell</hr>
   <hu>Bosnian dictionary for hunspell</hu>
   <id>Bosnian dictionary for hunspell</id>
   <is>Bosnian dictionary for hunspell</is>
   <it>Dizionario bosniaco per hunspell</it>
   <ja>Hunspell 用ボスニア語辞書</ja>
   <kk>Bosnian dictionary for hunspell</kk>
   <ko>Bosnian dictionary for hunspell</ko>
   <ku>Bosnian dictionary for hunspell</ku>
   <lt>Bosnian dictionary for hunspell</lt>
   <mk>Bosnian dictionary for hunspell</mk>
   <mr>Bosnian dictionary for hunspell</mr>
   <nb_NO>Bosnian dictionary for hunspell</nb_NO>
   <nb>Bosnisk ordliste for hunspell</nb>
   <nl_BE>Bosnian dictionary for hunspell</nl_BE>
   <nl>Bosnisch woordenboek voor hunspell</nl>
   <or>Bosnian dictionary for hunspell</or>
   <pl>Bosnian dictionary for hunspell</pl>
   <pt_BR>Dicionário Bósnio para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Bósnio para hunspell</pt>
   <ro>Bosnian dictionary for hunspell</ro>
   <ru>Bosnian dictionary for hunspell</ru>
   <sk>Bosnian dictionary for hunspell</sk>
   <sl>Bosanski slovar za hunspell</sl>
   <so>Bosnian dictionary for hunspell</so>
   <sq>Fjalor bosnjisht për hunspell</sq>
   <sr>Bosnian dictionary for hunspell</sr>
   <sv>Bosnisk ordbok för hunspell</sv>
   <th>Bosnian dictionary for hunspell</th>
   <tr>Hunspell için Boşnakça sözlük</tr>
   <uk>Bosnian dictionary for hunspell</uk>
   <vi>Bosnian dictionary for hunspell</vi>
   <zh_CN>Bosnian dictionary for hunspell</zh_CN>
   <zh_HK>Bosnian dictionary for hunspell</zh_HK>
   <zh_TW>Bosnian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-bs
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-bs
</uninstall_package_names>

</app>
