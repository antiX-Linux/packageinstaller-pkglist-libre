<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Welsh_Default_Firefox_esr
</name>

<description>
   <am>Welsh localisation of Firefox ESR</am>
   <ar>Welsh localisation of Firefox ESR</ar>
   <be>Welsh localisation of Firefox ESR</be>
   <bg>Welsh localisation of Firefox ESR</bg>
   <bn>Welsh localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Gal·lès</ca>
   <cs>Welsh localisation of Firefox ESR</cs>
   <da>Welsh localisation of Firefox ESR</da>
   <de>Kymrische Lokalisation für Großbritannien von “Firefox ESR”</de>
   <el>Ουαλικά για το Firefox ESR</el>
   <en>Welsh localisation of Firefox ESR</en>
   <es_ES>Localización Galés de Firefox ESR</es_ES>
   <es>Localización Galés de Firefox ESR</es>
   <et>Welsh localisation of Firefox ESR</et>
   <eu>Welsh localisation of Firefox ESR</eu>
   <fa>Welsh localisation of Firefox ESR</fa>
   <fil_PH>Welsh localisation of Firefox ESR</fil_PH>
   <fi>Walesilainen FIrefox-ESR kotoistus</fi>
   <fr_BE>Localisation en gallois pour Firefox ESR</fr_BE>
   <fr>Localisation en gallois pour Firefox ESR</fr>
   <gl_ES>Welsh localisation of Firefox ESR</gl_ES>
   <gu>Welsh localisation of Firefox ESR</gu>
   <he_IL>Welsh localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का वेल्श संस्करण</hi>
   <hr>Welsh localisation of Firefox ESR</hr>
   <hu>Welsh localisation of Firefox ESR</hu>
   <id>Welsh localisation of Firefox ESR</id>
   <is>Welsh localisation of Firefox ESR</is>
   <it>Localizzazione welsh di Firefox ESR</it>
   <ja>ウェールズ語版 Firefox ESR</ja>
   <kk>Welsh localisation of Firefox ESR</kk>
   <ko>Welsh localisation of Firefox ESR</ko>
   <ku>Welsh localisation of Firefox ESR</ku>
   <lt>Welsh localisation of Firefox ESR</lt>
   <mk>Welsh localisation of Firefox ESR</mk>
   <mr>Welsh localisation of Firefox ESR</mr>
   <nb_NO>Welsh localisation of Firefox ESR</nb_NO>
   <nb>Walisisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Welsh localisation of Firefox ESR</nl_BE>
   <nl>Welsh localisation of Firefox ESR</nl>
   <or>Welsh localisation of Firefox ESR</or>
   <pl>Welsh localisation of Firefox ESR</pl>
   <pt_BR>Galês Localização para o Firefox ESR</pt_BR>
   <pt>Galês Localização para Firefox ESR</pt>
   <ro>Welsh localisation of Firefox ESR</ro>
   <ru>Welsh localisation of Firefox ESR</ru>
   <sk>Welsh localisation of Firefox ESR</sk>
   <sl>Velške krajevne nastavitve za Firefox ESR</sl>
   <so>Welsh localisation of Firefox ESR</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në uellsisht</sq>
   <sr>Welsh localisation of Firefox ESR</sr>
   <sv>Walesisk lokalisering av Firefox ESR</sv>
   <th>Welsh localisation of Firefox ESR</th>
   <tr>Firefox ESR Galce yerelleştirmesi</tr>
   <uk>Welsh localisation of Firefox ESR</uk>
   <vi>Welsh localisation of Firefox ESR</vi>
   <zh_CN>Welsh localisation of Firefox ESR</zh_CN>
   <zh_HK>Welsh localisation of Firefox ESR</zh_HK>
   <zh_TW>Welsh localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-esr-l10n-cy
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-cy
</uninstall_package_names>

</app>
