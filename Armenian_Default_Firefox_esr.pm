<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Armenian_Default_Firefox_esr
</name>

<description>
   <am>Armenian localisation of Firefox-ESR</am>
   <ar>Armenian localisation of Firefox-ESR</ar>
   <be>Armenian localisation of Firefox-ESR</be>
   <bg>Armenian localisation of Firefox-ESR</bg>
   <bn>Armenian localisation of Firefox-ESR</bn>
   <ca>Localització de Firefox-ESR en Armeni</ca>
   <cs>Armenian localisation of Firefox-ESR</cs>
   <da>Armenian localisation of Firefox-ESR</da>
   <de>Armenische Lokalisation von “Firefox ESR”</de>
   <el>Αρμενικά για Firefox-ESR</el>
   <en>Armenian localisation of Firefox-ESR</en>
   <es_ES>Localización Armenia de Firefox-ESR</es_ES>
   <es>Localización Armenio de Firefox ESR</es>
   <et>Armenian localisation of Firefox-ESR</et>
   <eu>Armenian localisation of Firefox-ESR</eu>
   <fa>Armenian localisation of Firefox-ESR</fa>
   <fil_PH>Armenian localisation of Firefox-ESR</fil_PH>
   <fi>Armenian localisation of Firefox-ESR</fi>
   <fr_BE>Localisation en arménien pour Firefox ESR</fr_BE>
   <fr>Localisation en arménien pour Firefox ESR</fr>
   <gl_ES>Armenian localisation of Firefox-ESR</gl_ES>
   <gu>Armenian localisation of Firefox-ESR</gu>
   <he_IL>Armenian localisation of Firefox-ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का अर्मेनियाई संस्करण</hi>
   <hr>Armenian localisation of Firefox-ESR</hr>
   <hu>Armenian localisation of Firefox-ESR</hu>
   <id>Armenian localisation of Firefox-ESR</id>
   <is>Armenian localisation of Firefox-ESR</is>
   <it>Localizzazione armena di Firefox ESR</it>
   <ja>アルメニア語版 Firefox-ESR</ja>
   <kk>Armenian localisation of Firefox-ESR</kk>
   <ko>Armenian localisation of Firefox-ESR</ko>
   <ku>Armenian localisation of Firefox-ESR</ku>
   <lt>Armenian localisation of Firefox-ESR</lt>
   <mk>Armenian localisation of Firefox-ESR</mk>
   <mr>Armenian localisation of Firefox-ESR</mr>
   <nb_NO>Armenian localisation of Firefox-ESR</nb_NO>
   <nb>Armensk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Armenian localisation of Firefox-ESR</nl_BE>
   <nl>Armeense lokalisatie van Firefox-ESR</nl>
   <or>Armenian localisation of Firefox-ESR</or>
   <pl>Armenian localisation of Firefox-ESR</pl>
   <pt_BR>Armênia Localização para o Firefox ESR</pt_BR>
   <pt>Arménio Localização para Firefox ESR</pt>
   <ro>Armenian localisation of Firefox-ESR</ro>
   <ru>Armenian localisation of Firefox-ESR</ru>
   <sk>Armenian localisation of Firefox-ESR</sk>
   <sl>Armenske krajevne nastavitve za Firefox-ESR</sl>
   <so>Armenian localisation of Firefox-ESR</so>
   <sq>Përkthimi i Firefox-it ESR në armenisht</sq>
   <sr>Armenian localisation of Firefox-ESR</sr>
   <sv>Armenisk lokalisering av Firefox-ESR</sv>
   <th>Armenian localisation of Firefox-ESR</th>
   <tr>Firefox-ESR Ermenice yerelleştirmesi</tr>
   <uk>Armenian localisation of Firefox-ESR</uk>
   <vi>Armenian localisation of Firefox-ESR</vi>
   <zh_CN>Armenian localisation of Firefox-ESR</zh_CN>
   <zh_HK>Armenian localisation of Firefox-ESR</zh_HK>
   <zh_TW>Armenian localisation of Firefox-ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-hy-am
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-hy-am
</uninstall_package_names>

</app>
