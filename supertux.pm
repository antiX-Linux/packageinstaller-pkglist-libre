<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
SuperTux
</name>

<description>
   <am>Mario style platform game w/ Tux</am>
   <ar>Mario style platform game w/ Tux</ar>
   <be>Mario style platform game w/ Tux</be>
   <bg>Mario style platform game w/ Tux</bg>
   <bn>Mario style platform game w/ Tux</bn>
   <ca>Plataforma de jocs de l'estil Mario amb Tux</ca>
   <cs>Mario style platform game w/ Tux</cs>
   <da>Platformspil med Tux i stil med Mario</da>
   <de>Mario Style Plattformspiel mit Tux</de>
   <el>Mario style platform game w/ Tux</el>
   <en>Mario style platform game w/ Tux</en>
   <es_ES>Juego de plataformas estilo Mario con Tux</es_ES>
   <es>Juego de plataformas con Tux estilo Mario</es>
   <et>Mario style platform game w/ Tux</et>
   <eu>Mario style platform game w/ Tux</eu>
   <fa>Mario style platform game w/ Tux</fa>
   <fil_PH>Mario style platform game w/ Tux</fil_PH>
   <fi>Super Mario- tyylinen tasohyppely jossa pääosassa on Tux</fi>
   <fr_BE>Jeu de plate-formes de style Mario, mais avec Tux comme héros</fr_BE>
   <fr>Jeu de plate-formes de style Mario, mais avec Tux comme héros</fr>
   <gl_ES>Plataforma de xogo do tipo Mario, con Tux</gl_ES>
   <gu>Mario style platform game w/ Tux</gu>
   <he_IL>Mario style platform game w/ Tux</he_IL>
   <hi>मारिओ स्टाइल प्लेटफार्म खेल, टक्स युक्त</hi>
   <hr>Mario style platform game w/ Tux</hr>
   <hu>Mario style platform game w/ Tux</hu>
   <id>Mario style platform game w/ Tux</id>
   <is>Mario style platform game w/ Tux</is>
   <it>Piattaforma di gioco in stile SuperMario con la presenza del pinguino Tux</it>
   <ja>Tuxくんが登場するマリオ風プラットフォーム ゲーム</ja>
   <kk>Mario style platform game w/ Tux</kk>
   <ko>Mario style platform game w/ Tux</ko>
   <ku>Mario style platform game w/ Tux</ku>
   <lt>Mario style platform game w/ Tux</lt>
   <mk>Mario style platform game w/ Tux</mk>
   <mr>Mario style platform game w/ Tux</mr>
   <nb_NO>Mario style platform game w/ Tux</nb_NO>
   <nb>Super Mario Bros.-lignende plattformspill med Tux</nb>
   <nl_BE>Mario style platform game w/ Tux</nl_BE>
   <nl>Mario stijl platform spel met Tux</nl>
   <or>Mario style platform game w/ Tux</or>
   <pl>Gra platformowa w/ Tux w stylu Mario</pl>
   <pt_BR>Jogo de plataforma estilo Mario, com Tux (jogo do pinguim)</pt_BR>
   <pt>Plataforma de jogo do tipo Mario, com Tux</pt>
   <ro>Mario style platform game w/ Tux</ro>
   <ru>Игра-платформер в духе Mario</ru>
   <sk>Mario style platform game w/ Tux</sk>
   <sl>Platformska igra tipa Mario z Tux</sl>
   <so>Mario style platform game w/ Tux</so>
   <sq>Lojë platforme në stil Mario me Tux-in</sq>
   <sr>Mario style platform game w/ Tux</sr>
   <sv>Plattformsspel i Mariostil med Tux</sv>
   <th>Mario style platform game w/ Tux</th>
   <tr>w/ Tux Mario tarzı platform oyunu</tr>
   <uk>Mario style platform game w/ Tux</uk>
   <vi>Mario style platform game w/ Tux</vi>
   <zh_CN>Mario style platform game w/ Tux</zh_CN>
   <zh_HK>Mario style platform game w/ Tux</zh_HK>
   <zh_TW>Mario style platform game w/ Tux</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
supertux
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
supertux
</uninstall_package_names>
</app>
