<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Gujarati_Default_Firefox_esr
</name>

<description>
   <am>Gujarati (India) localisation of Firefox ESR</am>
   <ar>Gujarati (India) localisation of Firefox ESR</ar>
   <be>Gujarati (India) localisation of Firefox ESR</be>
   <bg>Gujarati (India) localisation of Firefox ESR</bg>
   <bn>Gujarati (India) localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Gujarati (Índia)</ca>
   <cs>Gujarati (India) localisation of Firefox ESR</cs>
   <da>Gujarati (India) localisation of Firefox ESR</da>
   <de>Indische Lokalisation für die Sprache “Gujarati” von “Firefox ESR”</de>
   <el>Gujarati (Ινδία) για Firefox ESR</el>
   <en>Gujarati (India) localisation of Firefox ESR</en>
   <es_ES>Localización Gujarati (India) de Firefox ESR</es_ES>
   <es>Localización Gujarati (India) de Firefox ESR</es>
   <et>Gujarati (India) localisation of Firefox ESR</et>
   <eu>Gujarati (India) localisation of Firefox ESR</eu>
   <fa>Gujarati (India) localisation of Firefox ESR</fa>
   <fil_PH>Gujarati (India) localisation of Firefox ESR</fil_PH>
   <fi>Gujarati (India) localisation of Firefox ESR</fi>
   <fr_BE>Localisation en gujarati (Inde) pour Firefox ESR</fr_BE>
   <fr>Localisation en gujarati (Inde) pour Firefox ESR</fr>
   <gl_ES>Gujarati (India) localisation of Firefox ESR</gl_ES>
   <gu>Gujarati (India) localisation of Firefox ESR</gu>
   <he_IL>Gujarati (India) localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का गुजराती (भारत) संस्करण</hi>
   <hr>Gujarati (India) localisation of Firefox ESR</hr>
   <hu>Gujarati (India) localisation of Firefox ESR</hu>
   <id>Gujarati (India) localisation of Firefox ESR</id>
   <is>Gujarati (India) localisation of Firefox ESR</is>
   <it>Localizzazione in lingua gujarati (India) di Firefox ESR</it>
   <ja>グジャラート語 (インド) 版 Firefox ESR</ja>
   <kk>Gujarati (India) localisation of Firefox ESR</kk>
   <ko>Gujarati (India) localisation of Firefox ESR</ko>
   <ku>Gujarati (India) localisation of Firefox ESR</ku>
   <lt>Gujarati (India) localisation of Firefox ESR</lt>
   <mk>Gujarati (India) localisation of Firefox ESR</mk>
   <mr>Gujarati (India) localisation of Firefox ESR</mr>
   <nb_NO>Gujarati (India) localisation of Firefox ESR</nb_NO>
   <nb>Gujarati (India) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Gujarati (India) localisation of Firefox ESR</nl_BE>
   <nl>Gujarati (India) localisation of Firefox ESR</nl>
   <or>Gujarati (India) localisation of Firefox ESR</or>
   <pl>Gujarati (India) localisation of Firefox ESR</pl>
   <pt_BR>Guzerate (Índia) Localização para o Firefox ESR</pt_BR>
   <pt>Gujarati (Índia) Localização para Firefox ESR</pt>
   <ro>Gujarati (India) localisation of Firefox ESR</ro>
   <ru>Gujarati (India) localisation of Firefox ESR</ru>
   <sk>Gujarati (India) localisation of Firefox ESR</sk>
   <sl>Gudžaraške (Indija) krajevne nastavitve za Firefox ESR</sl>
   <so>Gujarati (India) localisation of Firefox ESR</so>
   <sq>Meta-Paketë gjuhësore guaranisht për LibreOffice-in në guaranisht</sq>
   <sr>Gujarati (India) localisation of Firefox ESR</sr>
   <sv>Gujarati (Indien) lokalisering av Firefox ESR</sv>
   <th>Gujarati (India) localisation of Firefox ESR</th>
   <tr>Firefox ESR Gujaratça (Hindistan) yerelleştirmesi</tr>
   <uk>Gujarati (India) localisation of Firefox ESR</uk>
   <vi>Gujarati (India) localisation of Firefox ESR</vi>
   <zh_CN>Gujarati (India) localisation of Firefox ESR</zh_CN>
   <zh_HK>Gujarati (India) localisation of Firefox ESR</zh_HK>
   <zh_TW>Gujarati (India) localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-gu-in
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-gu-in
</uninstall_package_names>

</app>
