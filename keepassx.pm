<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Misc
</category>

<name>
KeepassX
</name>

<description>
   <am>KeePassX is a free/open-source password manager/safe</am>
   <ar>KeePassX is a free/open-source password manager/safe</ar>
   <be>KeePassX is a free/open-source password manager/safe</be>
   <bg>KeePassX is a free/open-source password manager/safe</bg>
   <bn>KeePassX is a free/open-source password manager/safe</bn>
   <ca>KeePassX és un gestor de contrasenyes lliure/codi obert</ca>
   <cs>KeePassX is a free/open-source password manager/safe</cs>
   <da>KeePassX er en fri/open source adgangskodehåndtering/-skab</da>
   <de>KeePassX ist ein kostenloser/open-source Passwortmanager/-safe</de>
   <el>Το KeePassX είναι ένας διαχειριστής κωδικών πρόσβασης ελεύθερου/ανοιχτού κώδικα</el>
   <en>KeePassX is a free/open-source password manager/safe</en>
   <es_ES>Administrador de contraseñas de código abierto</es_ES>
   <es>Gestor de contraseñas de código abierto</es>
   <et>KeePassX is a free/open-source password manager/safe</et>
   <eu>KeePassX is a free/open-source password manager/safe</eu>
   <fa>KeePassX is a free/open-source password manager/safe</fa>
   <fil_PH>KeePassX is a free/open-source password manager/safe</fil_PH>
   <fi>KeePassX on vapaa/koodiltaan avoin säilö/hallintaohjelma salasanoille</fi>
   <fr_BE>KeePassX est un gestionnaire sécurisé open-source de mots de passe</fr_BE>
   <fr>KeePassX est un gestionnaire sécurisé open-source de mots de passe</fr>
   <gl_ES>Gardado e xestión de contrasinais en software libre e de código aberto</gl_ES>
   <gu>KeePassX is a free/open-source password manager/safe</gu>
   <he_IL>KeePassX is a free/open-source password manager/safe</he_IL>
   <hi>KeePassX एक निःशुल्क/मुक्त स्रोत कूटशब्द प्रबंधक है</hi>
   <hr>KeePassX is a free/open-source password manager/safe</hr>
   <hu>KeePassX is a free/open-source password manager/safe</hu>
   <id>KeePassX is a free/open-source password manager/safe</id>
   <is>KeePassX is a free/open-source password manager/safe</is>
   <it>KeePassX è un gestore di password free/open-source</it>
   <ja>KeePassX はフリーでオープンソースのパスワードマネージャー兼金庫です</ja>
   <kk>KeePassX is a free/open-source password manager/safe</kk>
   <ko>KeePassX is a free/open-source password manager/safe</ko>
   <ku>KeePassX is a free/open-source password manager/safe</ku>
   <lt>KeePassX is a free/open-source password manager/safe</lt>
   <mk>KeePassX is a free/open-source password manager/safe</mk>
   <mr>KeePassX is a free/open-source password manager/safe</mr>
   <nb_NO>KeePassX is a free/open-source password manager/safe</nb_NO>
   <nb>KeePassX lagrer passord på en sikker måte</nb>
   <nl_BE>KeePassX is a free/open-source password manager/safe</nl_BE>
   <nl>KeePassX is een gratis/open-source wachtwoord manager/kluis</nl>
   <or>KeePassX is a free/open-source password manager/safe</or>
   <pl>KeePassX to bezpłatny menedżer haseł/sejf o otwartym kodzie źródłowym</pl>
   <pt_BR>KeePassX - Gerenciador de senhas seguro livre/de código aberto</pt_BR>
   <pt>Guarda e gestão de senhas (palavras-passe) em software livre e de fonte aberta</pt>
   <ro>KeePassX is a free/open-source password manager/safe</ro>
   <ru>KeePassX - менеджер паролей с открытым кодом (форк KeePass)</ru>
   <sk>KeePassX is a free/open-source password manager/safe</sk>
   <sl>KeePassXje brezplačen odprtokodni urejevalnik/sef za gesla</sl>
   <so>KeePassX is a free/open-source password manager/safe</so>
   <sq>KeePassX është një përgjegjës/kasafortë i lirë/me burim të hapur fjalëkalimesh</sq>
   <sr>KeePassX is a free/open-source password manager/safe</sr>
   <sv>KeePassX är en fri/öppen källkod lösenordshanterare/förvarare</sv>
   <th>KeePassX is a free/open-source password manager/safe</th>
   <tr>KeePassX özgür/açık kaynaklı bir parola yöneticisidir/güvenlidir</tr>
   <uk>KeePassX вільний та відкритий менеджер і сховище паролів</uk>
   <vi>KeePassX is a free/open-source password manager/safe</vi>
   <zh_CN>KeePassX is a free/open-source password manager/safe</zh_CN>
   <zh_HK>KeePassX is a free/open-source password manager/safe</zh_HK>
   <zh_TW>KeePassX is a free/open-source password manager/safe</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
keepassx
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
keepassx
</uninstall_package_names>
</app>
