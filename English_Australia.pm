<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English (Australia)
</name>

<description>
   <am>English (Australian) dictionary for hunspell</am>
   <ar>English (Australian) dictionary for hunspell</ar>
   <be>English (Australian) dictionary for hunspell</be>
   <bg>English (Australian) dictionary for hunspell</bg>
   <bn>English (Australian) dictionary for hunspell</bn>
   <ca>Diccionari Anglès (Australià) per hunspell</ca>
   <cs>English (Australian) dictionary for hunspell</cs>
   <da>English (Australian) dictionary for hunspell</da>
   <de>Englisches (australisches) Wörterbuch für “Hunspell”</de>
   <el>Αγγλικό (Αυστραλιανό) λεξικό για hunspell</el>
   <en>English (Australian) dictionary for hunspell</en>
   <es_ES>Diccionario Ingles (Australiano) para hunspell</es_ES>
   <es>Diccionario Inglés (Australiano) para hunspell</es>
   <et>English (Australian) dictionary for hunspell</et>
   <eu>English (Australian) dictionary for hunspell</eu>
   <fa>English (Australian) dictionary for hunspell</fa>
   <fil_PH>English (Australian) dictionary for hunspell</fil_PH>
   <fi>English (Australian) dictionary for hunspell</fi>
   <fr_BE>Anglais (australien) dictionnaire pour hunspell</fr_BE>
   <fr>Anglais (australien) dictionnaire pour hunspell</fr>
   <gl_ES>English (Australian) dictionary for hunspell</gl_ES>
   <gu>English (Australian) dictionary for hunspell</gu>
   <he_IL>English (Australian) dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु अंग्रेजी (ऑस्ट्रेलियाई) शब्दकोष</hi>
   <hr>English (Australian) dictionary for hunspell</hr>
   <hu>English (Australian) dictionary for hunspell</hu>
   <id>English (Australian) dictionary for hunspell</id>
   <is>English (Australian) dictionary for hunspell</is>
   <it>Dizionario inglese (australiano) per hunspell</it>
   <ja>Hunspell 用英語（オーストラリア）辞書</ja>
   <kk>English (Australian) dictionary for hunspell</kk>
   <ko>English (Australian) dictionary for hunspell</ko>
   <ku>English (Australian) dictionary for hunspell</ku>
   <lt>English (Australian) dictionary for hunspell</lt>
   <mk>English (Australian) dictionary for hunspell</mk>
   <mr>English (Australian) dictionary for hunspell</mr>
   <nb_NO>English (Australian) dictionary for hunspell</nb_NO>
   <nb>Engelsk (australsk) ordliste for hunspell</nb>
   <nl_BE>English (Australian) dictionary for hunspell</nl_BE>
   <nl>Engels (Australisch) woordenboek voor hunspell</nl>
   <or>English (Australian) dictionary for hunspell</or>
   <pl>English (Australian) dictionary for hunspell</pl>
   <pt_BR>Dicionário Inglês (Australiano) para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Inglês (Australiano) para hunspell</pt>
   <ro>English (Australian) dictionary for hunspell</ro>
   <ru>English (Australian) dictionary for hunspell</ru>
   <sk>English (Australian) dictionary for hunspell</sk>
   <sl>Angleški (avstralska) slovar za hunspell</sl>
   <so>English (Australian) dictionary for hunspell</so>
   <sq>Fjalor anglisht (Australie) për hunspell</sq>
   <sr>English (Australian) dictionary for hunspell</sr>
   <sv>Engelsk (Australisk) ordbok för hunspell</sv>
   <th>English (Australian) dictionary for hunspell</th>
   <tr>Hunspell için İngilizce (Avustralya) sözlüğü</tr>
   <uk>English (Australian) dictionary for hunspell</uk>
   <vi>English (Australian) dictionary for hunspell</vi>
   <zh_CN>English (Australian) dictionary for hunspell</zh_CN>
   <zh_HK>English (Australian) dictionary for hunspell</zh_HK>
   <zh_TW>English (Australian) dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-en-au
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-en-au
</uninstall_package_names>

</app>
