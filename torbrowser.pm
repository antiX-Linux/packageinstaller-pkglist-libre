<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Tor Browser Launcher
</name>

<description>
   <am>Helps download and run the Tor Browser Bundle</am>
   <ar>Helps download and run the Tor Browser Bundle</ar>
   <be>Helps download and run the Tor Browser Bundle</be>
   <bg>Helps download and run the Tor Browser Bundle</bg>
   <bn>Helps download and run the Tor Browser Bundle</bn>
   <ca>Ajuda a descarregar i executar el paquet de Navegador Tor</ca>
   <cs>Helps download and run the Tor Browser Bundle</cs>
   <da>Helps download and run the Tor Browser Bundle</da>
   <de>Vereinfacht das Herunterladen und Starten des Tor-Browser Paketes zur anonymen Internetnutzung</de>
   <el>Βοηθά στη λήψη και εκτέλεση του Tor Browser Bundle</el>
   <en>Helps download and run the Tor Browser Bundle</en>
   <es_ES>Ayuda a descargar y ejecutar el paquete del navegador Tor</es_ES>
   <es>Ayuda a descargar y ejecutar el paquete del navegador Tor</es>
   <et>Helps download and run the Tor Browser Bundle</et>
   <eu>Helps download and run the Tor Browser Bundle</eu>
   <fa>Helps download and run the Tor Browser Bundle</fa>
   <fil_PH>Helps download and run the Tor Browser Bundle</fil_PH>
   <fi>Helps download and run the Tor Browser Bundle</fi>
   <fr_BE>Aide pour télécharger et lancer le paquetage du navigateur “Tor” pour une utilisation anonyme d'Internet.</fr_BE>
   <fr>Aide pour télécharger et lancer le paquetage du navigateur “Tor” pour une utilisation anonyme d'Internet.</fr>
   <gl_ES>Helps download and run the Tor Browser Bundle</gl_ES>
   <gu>Helps download and run the Tor Browser Bundle</gu>
   <he_IL>Helps download and run the Tor Browser Bundle</he_IL>
   <hi>Tor ब्राउज़र संग्रह के डाउनलोड व निष्पादन हेतु सहायक</hi>
   <hr>Helps download and run the Tor Browser Bundle</hr>
   <hu>Helps download and run the Tor Browser Bundle</hu>
   <id>Helps download and run the Tor Browser Bundle</id>
   <is>Helps download and run the Tor Browser Bundle</is>
   <it>Aiuta a scaricare ed avviare Tor Browser Bundle</it>
   <ja>Tor Browser Bundle のダウンロードと実行を支援します</ja>
   <kk>Helps download and run the Tor Browser Bundle</kk>
   <ko>Helps download and run the Tor Browser Bundle</ko>
   <ku>Helps download and run the Tor Browser Bundle</ku>
   <lt>Helps download and run the Tor Browser Bundle</lt>
   <mk>Helps download and run the Tor Browser Bundle</mk>
   <mr>Helps download and run the Tor Browser Bundle</mr>
   <nb_NO>Helps download and run the Tor Browser Bundle</nb_NO>
   <nb>Veiledning for nedlasting og kjøring av nettleserpakken Tor</nb>
   <nl_BE>Helps download and run the Tor Browser Bundle</nl_BE>
   <nl>Helps download and run the Tor Browser Bundle</nl>
   <or>Helps download and run the Tor Browser Bundle</or>
   <pl>Helps download and run the Tor Browser Bundle</pl>
   <pt_BR>Ajuda a baixar e executar o pacote do navegador de internet Tor</pt_BR>
   <pt>Auxilia a transferência e a execução do Conjunto Navegador de Internet Tor</pt>
   <ro>Helps download and run the Tor Browser Bundle</ro>
   <ru>Helps download and run the Tor Browser Bundle</ru>
   <sk>Helps download and run the Tor Browser Bundle</sk>
   <sl>Pomoč pri prenosu in zagonu paketa Tor brskalnika</sl>
   <so>Helps download and run the Tor Browser Bundle</so>
   <sq>Ndihmon të shkarkohet dhe xhirohet Tor Browser Bundle</sq>
   <sr>Helps download and run the Tor Browser Bundle</sr>
   <sv>Hjälper till att ladda ner och köra Tor Browser Bundle</sv>
   <th>Helps download and run the Tor Browser Bundle</th>
   <tr>Tor Tarayıcı Paketini indirmeye ve çalıştırmaya yardımcı olur</tr>
   <uk>Helps download and run the Tor Browser Bundle</uk>
   <vi>Helps download and run the Tor Browser Bundle</vi>
   <zh_CN>Helps download and run the Tor Browser Bundle</zh_CN>
   <zh_HK>Helps download and run the Tor Browser Bundle</zh_HK>
   <zh_TW>Helps download and run the Tor Browser Bundle</zh_TW>
</description>

<installable>
all
</installable>

<preinstall>

</preinstall>

<install_package_names>
torbrowser-launcher
tor
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
torbrowser-launcher
tor
</uninstall_package_names>
</app>
