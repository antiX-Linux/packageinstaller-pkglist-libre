<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bengali_Default_Firefox_esr
</name>

<description>
   <am>Bengali (India) localisation of Firefox ESR</am>
   <ar>Bengali (India) localisation of Firefox ESR</ar>
   <be>Bengali (India) localisation of Firefox ESR</be>
   <bg>Bengali (India) localisation of Firefox ESR</bg>
   <bn>Bengali (India) localisation of Firefox ESR</bn>
   <ca>Localització en Bengalí (India) de Firefox ESR</ca>
   <cs>Bengali (India) localisation of Firefox ESR</cs>
   <da>Bengali (India) localisation of Firefox ESR</da>
   <de>Bengali (India) localisation of Firefox ESR</de>
   <el>Μπενγκάλι (Ινδία) για Firefox ESR</el>
   <en>Bengali (India) localisation of Firefox ESR</en>
   <es_ES>Localización en bengalí (India) de Firefox ESR</es_ES>
   <es>Localización en bengalí (India) de Firefox ESR</es>
   <et>Bengali (India) localisation of Firefox ESR</et>
   <eu>Bengali (India) localisation of Firefox ESR</eu>
   <fa>Bengali (India) localisation of Firefox ESR</fa>
   <fil_PH>Bengali (India) localisation of Firefox ESR</fil_PH>
   <fi>Bengali (India) localisation of Firefox ESR</fi>
   <fr_BE>Localisation en bengali (Inde) pour Firefox ESR</fr_BE>
   <fr>Localisation en bengali (Inde) pour Firefox ESR</fr>
   <gl_ES>Bengali (India) localisation of Firefox ESR</gl_ES>
   <gu>Bengali (India) localisation of Firefox ESR</gu>
   <he_IL>Bengali (India) localisation of Firefox ESR</he_IL>
   <hi>Bengali (India) localisation of Firefox ESR</hi>
   <hr>Bengali (India) localisation of Firefox ESR</hr>
   <hu>Bengali (India) localisation of Firefox ESR</hu>
   <id>Bengali (India) localisation of Firefox ESR</id>
   <is>Bengali (India) localisation of Firefox ESR</is>
   <it>Localizzazione Bengali (India) di Firefox ESR</it>
   <ja>Bengali (India) localisation of Firefox ESR</ja>
   <kk>Bengali (India) localisation of Firefox ESR</kk>
   <ko>Bengali (India) localisation of Firefox ESR</ko>
   <ku>Bengali (India) localisation of Firefox ESR</ku>
   <lt>Bengali (India) localisation of Firefox ESR</lt>
   <mk>Bengali (India) localisation of Firefox ESR</mk>
   <mr>Bengali (India) localisation of Firefox ESR</mr>
   <nb_NO>Bengali (India) localisation of Firefox ESR</nb_NO>
   <nb>Bengali (India) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Bengali (India) localisation of Firefox ESR</nl_BE>
   <nl>Bengaalse (India) lokalisatie van Firefox ESR</nl>
   <or>Bengali (India) localisation of Firefox ESR</or>
   <pl>Bengali (India) localisation of Firefox ESR</pl>
   <pt_BR>Bengali (Índia) Localização para Firefox ESR</pt_BR>
   <pt>Bengali (Índia) Localização para Firefox ESR</pt>
   <ro>Bengali (India) localisation of Firefox ESR</ro>
   <ru>Bengali (India) localisation of Firefox ESR</ru>
   <sk>Bengali (India) localisation of Firefox ESR</sk>
   <sl>Bengalske (Indija) krajevne nastavitve za Firefox-ESR</sl>
   <so>Bengali (India) localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në bengalisht (Indi)</sq>
   <sr>Bengali (India) localisation of Firefox ESR</sr>
   <sv>Bengali (Indien) lokalisering av  Firefox ESR</sv>
   <th>Bengali (India) localisation of Firefox ESR</th>
   <tr>Bengali (India) localisation of Firefox ESR</tr>
   <uk>Bengali (India) localisation of Firefox ESR</uk>
   <vi>Bengali (India) localisation of Firefox ESR</vi>
   <zh_CN>Bengali (India) localisation of Firefox ESR</zh_CN>
   <zh_HK>Bengali (India) localisation of Firefox ESR</zh_HK>
   <zh_TW>Bengali (India) localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-bn-in
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-l10n-bn-in
</uninstall_package_names>

</app>
