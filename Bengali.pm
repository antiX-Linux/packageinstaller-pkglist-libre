<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bengali
</name>

<description>
   <am>Bengali dictionary for hunspell</am>
   <ar>Bengali dictionary for hunspell</ar>
   <be>Bengali dictionary for hunspell</be>
   <bg>Bengali dictionary for hunspell</bg>
   <bn>Bengali dictionary for hunspell</bn>
   <ca>Diccionari Bengalí per hunspell</ca>
   <cs>Bengali dictionary for hunspell</cs>
   <da>Bengali dictionary for hunspell</da>
   <de>Bengalisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Μπενγκάλι για hunspell</el>
   <en>Bengali dictionary for hunspell</en>
   <es_ES>Diccionario Bengali para hunspell</es_ES>
   <es>Diccionario Bengalí para hunspell</es>
   <et>Bengali dictionary for hunspell</et>
   <eu>Bengali dictionary for hunspell</eu>
   <fa>Bengali dictionary for hunspell</fa>
   <fil_PH>Bengali dictionary for hunspell</fil_PH>
   <fi>Bengali dictionary for hunspell</fi>
   <fr_BE>Bengali dictionnaire pour hunspell</fr_BE>
   <fr>Bengali dictionnaire pour hunspell</fr>
   <gl_ES>Bengali dictionary for hunspell</gl_ES>
   <gu>Bengali dictionary for hunspell</gu>
   <he_IL>Bengali dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु बंगाली शब्दकोष</hi>
   <hr>Bengali dictionary for hunspell</hr>
   <hu>Bengali dictionary for hunspell</hu>
   <id>Bengali dictionary for hunspell</id>
   <is>Bengali dictionary for hunspell</is>
   <it>Dizionario bengalese per hunspell</it>
   <ja>Hunspell 用ベンガル語辞書</ja>
   <kk>Bengali dictionary for hunspell</kk>
   <ko>Bengali dictionary for hunspell</ko>
   <ku>Bengali dictionary for hunspell</ku>
   <lt>Bengali dictionary for hunspell</lt>
   <mk>Bengali dictionary for hunspell</mk>
   <mr>Bengali dictionary for hunspell</mr>
   <nb_NO>Bengali dictionary for hunspell</nb_NO>
   <nb>Bengali ordliste for hunspell</nb>
   <nl_BE>Bengali dictionary for hunspell</nl_BE>
   <nl>Bengaals woordenboek voor hunspell</nl>
   <or>Bengali dictionary for hunspell</or>
   <pl>Bengali dictionary for hunspell</pl>
   <pt_BR>Dicionário Bengali para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Bengali para hunspell</pt>
   <ro>Bengali dictionary for hunspell</ro>
   <ru>Bengali dictionary for hunspell</ru>
   <sk>Bengali dictionary for hunspell</sk>
   <sl>Bengalski slovar za hunspell</sl>
   <so>Bengali dictionary for hunspell</so>
   <sq>Fjalor bengalisht për hunspell</sq>
   <sr>Bengali dictionary for hunspell</sr>
   <sv>Bengali ordbok för hunspell</sv>
   <th>Bengali dictionary for hunspell</th>
   <tr>Hunspell için Bengalce sözlük</tr>
   <uk>Bengali dictionary for hunspell</uk>
   <vi>Bengali dictionary for hunspell</vi>
   <zh_CN>Bengali dictionary for hunspell</zh_CN>
   <zh_HK>Bengali dictionary for hunspell</zh_HK>
   <zh_TW>Bengali dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-bn
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-bn
</uninstall_package_names>

</app>
