<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
German_Thunderbird
</name>

<description>
   <am>German localisation of Thunderbird</am>
   <ar>German localisation of Thunderbird</ar>
   <be>German localisation of Thunderbird</be>
   <bg>German localisation of Thunderbird</bg>
   <bn>German localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Alemany</ca>
   <cs>German localisation of Thunderbird</cs>
   <da>Tysk oversættelse af Thunderbird</da>
   <de>Deutsche Lokalisierung von Thunderbird</de>
   <el>Γερμανικά για το Thunderbird</el>
   <en>German localisation of Thunderbird</en>
   <es_ES>Localización Alemana de Thunderbird</es_ES>
   <es>Localización Alemán de Thunderbird</es>
   <et>German localisation of Thunderbird</et>
   <eu>German localisation of Thunderbird</eu>
   <fa>German localisation of Thunderbird</fa>
   <fil_PH>German localisation of Thunderbird</fil_PH>
   <fi>Saksalainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en allemand pour Thunderbird</fr_BE>
   <fr>Localisation en allemand pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao alemán</gl_ES>
   <gu>German localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לגרמנית</he_IL>
   <hi>थंडरबर्ड का जर्मन संस्करण</hi>
   <hr>German localisation of Thunderbird</hr>
   <hu>German localisation of Thunderbird</hu>
   <id>German localisation of Thunderbird</id>
   <is>German localisation of Thunderbird</is>
   <it>Localizzazione tedesca di Thunderbird</it>
   <ja>Thunderbird のドイツ語版</ja>
   <kk>German localisation of Thunderbird</kk>
   <ko>German localisation of Thunderbird</ko>
   <ku>German localisation of Thunderbird</ku>
   <lt>German localisation of Thunderbird</lt>
   <mk>German localisation of Thunderbird</mk>
   <mr>German localisation of Thunderbird</mr>
   <nb_NO>German localisation of Thunderbird</nb_NO>
   <nb>Tysk lokaltilpassing av Thunderbird</nb>
   <nl_BE>German localisation of Thunderbird</nl_BE>
   <nl>Duitse lokalisatie van Thunderbird</nl>
   <or>German localisation of Thunderbird</or>
   <pl>Niemiecka lokalizacja Thunderbirda</pl>
   <pt_BR>Alemão Localização para o Thunderbird</pt_BR>
   <pt>Alemão Localização para Thunderbird</pt>
   <ro>German localisation of Thunderbird</ro>
   <ru>German localisation of Thunderbird</ru>
   <sk>German localisation of Thunderbird</sk>
   <sl>Nemške krajevne nastavitve za Thunderbird</sl>
   <so>German localisation of Thunderbird</so>
   <sq>Fjalor gjermanisht për hunspell</sq>
   <sr>German localisation of Thunderbird</sr>
   <sv>Tysk lokalisering av Thunderbird</sv>
   <th>German localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Almanca yerelleştirmesi</tr>
   <uk>German локалізація Thunderbird</uk>
   <vi>German localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 德语语言包</zh_CN>
   <zh_HK>German localisation of Thunderbird</zh_HK>
   <zh_TW>German localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-de
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-de
</uninstall_package_names>

</app>
