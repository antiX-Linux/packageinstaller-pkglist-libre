<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hungarian_Thunderbird
</name>

<description>
   <am>Hungarian localisation of Thunderbird</am>
   <ar>Hungarian localisation of Thunderbird</ar>
   <be>Hungarian localisation of Thunderbird</be>
   <bg>Hungarian localisation of Thunderbird</bg>
   <bn>Hungarian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Hongarès</ca>
   <cs>Hungarian localisation of Thunderbird</cs>
   <da>Ungarsk oversættelse af Thunderbird</da>
   <de>Ungarische Lokalisierung von Thunderbird</de>
   <el>Ουγγρικά για το Thunderbird</el>
   <en>Hungarian localisation of Thunderbird</en>
   <es_ES>Localización Húngara de Thunderbird</es_ES>
   <es>Localización Húngaro de Thunderbird</es>
   <et>Hungarian localisation of Thunderbird</et>
   <eu>Hungarian localisation of Thunderbird</eu>
   <fa>Hungarian localisation of Thunderbird</fa>
   <fil_PH>Hungarian localisation of Thunderbird</fil_PH>
   <fi>Unkarilainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en hongrois pour Thunderbird</fr_BE>
   <fr>Localisation en hongrois pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao húngaro</gl_ES>
   <gu>Hungarian localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird להונגרית</he_IL>
   <hi>थंडरबर्ड का हंगेरियन संस्करण</hi>
   <hr>Hungarian localisation of Thunderbird</hr>
   <hu>Hungarian localisation of Thunderbird</hu>
   <id>Hungarian localisation of Thunderbird</id>
   <is>Hungarian localisation of Thunderbird</is>
   <it>Localizzazione ungherese di Thunderbird</it>
   <ja>Thunderbird のハンガリー語版</ja>
   <kk>Hungarian localisation of Thunderbird</kk>
   <ko>Hungarian localisation of Thunderbird</ko>
   <ku>Hungarian localisation of Thunderbird</ku>
   <lt>Hungarian localisation of Thunderbird</lt>
   <mk>Hungarian localisation of Thunderbird</mk>
   <mr>Hungarian localisation of Thunderbird</mr>
   <nb_NO>Hungarian localisation of Thunderbird</nb_NO>
   <nb>Ungarsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Hungarian localisation of Thunderbird</nl_BE>
   <nl>Hongaarse lokalisatie van Thunderbird</nl>
   <or>Hungarian localisation of Thunderbird</or>
   <pl>Węgierska lokalizacja Thunderbirda</pl>
   <pt_BR>Húngaro Localização para o Thunderbird</pt_BR>
   <pt>Húngaro Localização para Thunderbird</pt>
   <ro>Hungarian localisation of Thunderbird</ro>
   <ru>Hungarian localisation of Thunderbird</ru>
   <sk>Hungarian localisation of Thunderbird</sk>
   <sl>Hungarian localisation of Thunderbird</sl>
   <so>Hungarian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në hungarisht</sq>
   <sr>Hungarian localisation of Thunderbird</sr>
   <sv>Ungersk lokalisering av Thunderbird</sv>
   <th>Hungarian localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Macarca yerelleştirmesi</tr>
   <uk>Hungarian локалізація Thunderbird</uk>
   <vi>Hungarian localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 匈牙利语语言包</zh_CN>
   <zh_HK>Hungarian localisation of Thunderbird</zh_HK>
   <zh_TW>Hungarian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-hu
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-hu
</uninstall_package_names>

</app>
