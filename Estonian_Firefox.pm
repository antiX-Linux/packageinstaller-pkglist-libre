<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Estonian_Firefox
</name>

<description>
   <am>Estonian localisation of Firefox</am>
   <ar>Estonian localisation of Firefox</ar>
   <be>Estonian localisation of Firefox</be>
   <bg>Estonian localisation of Firefox</bg>
   <bn>Estonian localisation of Firefox</bn>
   <ca>Localització de Firefox en Estonià</ca>
   <cs>Estonian localisation of Firefox</cs>
   <da>Estisk oversættelse af Firefox</da>
   <de>Estnische Lokalisierung von Firefox</de>
   <el>Εσθονικά για το Firefox</el>
   <en>Estonian localisation of Firefox</en>
   <es_ES>Localización Estonia de Firefox</es_ES>
   <es>Localización Estonio de Firefox</es>
   <et>Estonian localisation of Firefox</et>
   <eu>Estonian localisation of Firefox</eu>
   <fa>Estonian localisation of Firefox</fa>
   <fil_PH>Estonian localisation of Firefox</fil_PH>
   <fi>Eestiläinen Firefox-kotoistus</fi>
   <fr_BE>Localisation en estonien pour Firefox</fr_BE>
   <fr>Localisation en estonien pour Firefox</fr>
   <gl_ES>Localización de Firefox en estonio</gl_ES>
   <gu>Estonian localisation of Firefox</gu>
   <he_IL>Estonian localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का एस्टोनियाई संस्करण</hi>
   <hr>Estonian localisation of Firefox</hr>
   <hu>Estonian localisation of Firefox</hu>
   <id>Estonian localisation of Firefox</id>
   <is>Estonian localisation of Firefox</is>
   <it>Localizzazione estone di Firefox</it>
   <ja>Firefox のエストニア語版</ja>
   <kk>Estonian localisation of Firefox</kk>
   <ko>Estonian localisation of Firefox</ko>
   <ku>Estonian localisation of Firefox</ku>
   <lt>Estonian localisation of Firefox</lt>
   <mk>Estonian localisation of Firefox</mk>
   <mr>Estonian localisation of Firefox</mr>
   <nb_NO>Estonian localisation of Firefox</nb_NO>
   <nb>Estisk lokaltilpassing av Firefox</nb>
   <nl_BE>Estonian localisation of Firefox</nl_BE>
   <nl>Estische lokalisatie van Firefox</nl>
   <or>Estonian localisation of Firefox</or>
   <pl>Estońska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Estónio Localização para o Firefox</pt_BR>
   <pt>Estónio Localização para Firefox</pt>
   <ro>Estonian localisation of Firefox</ro>
   <ru>Estonian localisation of Firefox</ru>
   <sk>Estonian localisation of Firefox</sk>
   <sl>Estonska lokalizacija za Firefox</sl>
   <so>Estonian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në estonisht</sq>
   <sr>Estonian localisation of Firefox</sr>
   <sv>Estnisk lokalisering av Firefox</sv>
   <th>Estonian localisation ของ Firefox</th>
   <tr>Firefox'un Estonya dili yerelleştirmesi</tr>
   <uk>Estonian локалізація Firefox</uk>
   <vi>Estonian localisation of Firefox</vi>
   <zh_CN>Firefox 爱沙尼亚语语言包</zh_CN>
   <zh_HK>Estonian localisation of Firefox</zh_HK>
   <zh_TW>Estonian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-et
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-et
</uninstall_package_names>
</app>
