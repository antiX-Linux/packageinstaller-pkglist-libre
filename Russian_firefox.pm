<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Russian_Firefox
</name>

<description>
   <am>Russian localisation of Firefox</am>
   <ar>Russian localisation of Firefox</ar>
   <be>Russian localisation of Firefox</be>
   <bg>Russian localisation of Firefox</bg>
   <bn>Russian localisation of Firefox</bn>
   <ca>Localització de Firefox en Rus</ca>
   <cs>Russian localisation of Firefox</cs>
   <da>Russisk oversættelse af Firefox</da>
   <de>Russische Lokalisierung von Firefox</de>
   <el>Ρωσικά για το Firefox</el>
   <en>Russian localisation of Firefox</en>
   <es_ES>Localización Rusa de Firefox</es_ES>
   <es>Localización Ruso de Firefox</es>
   <et>Russian localisation of Firefox</et>
   <eu>Russian localisation of Firefox</eu>
   <fa>Russian localisation of Firefox</fa>
   <fil_PH>Russian localisation of Firefox</fil_PH>
   <fi>Venäläinen Firefox-kotoistus</fi>
   <fr_BE>Localisation en russe pour Firefox</fr_BE>
   <fr>Localisation en russe pour Firefox</fr>
   <gl_ES>Localización de Firefox ao ruso</gl_ES>
   <gu>Russian localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לרוסית</he_IL>
   <hi>फायरफॉक्स का रूसी संस्करण</hi>
   <hr>Russian localisation of Firefox</hr>
   <hu>Russian localisation of Firefox</hu>
   <id>Russian localisation of Firefox</id>
   <is>Russian localisation of Firefox</is>
   <it>Localizzazione russa di Firefox</it>
   <ja>Firefox のロシア語版</ja>
   <kk>Russian localisation of Firefox</kk>
   <ko>Russian localisation of Firefox</ko>
   <ku>Russian localisation of Firefox</ku>
   <lt>Russian localisation of Firefox</lt>
   <mk>Russian localisation of Firefox</mk>
   <mr>Russian localisation of Firefox</mr>
   <nb_NO>Russian localisation of Firefox</nb_NO>
   <nb>Russisk lokaltilpassing av Firefox</nb>
   <nl_BE>Russian localisation of Firefox</nl_BE>
   <nl>Russische lokalisatie van Firefox</nl>
   <or>Russian localisation of Firefox</or>
   <pl>Rosyjska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Russo Localização para o Firefox</pt_BR>
   <pt>Russo Localização para Firefox</pt>
   <ro>Russian localisation of Firefox</ro>
   <ru>Русская локализация Firefox</ru>
   <sk>Russian localisation of Firefox</sk>
   <sl>Ruske krajevne nastavitve za Firefox</sl>
   <so>Russian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në rusisht</sq>
   <sr>Russian localisation of Firefox</sr>
   <sv>Rysk lokalisering av Firefox </sv>
   <th>Russian localisation ของ Firefox</th>
   <tr>Firefox'un Rusça yerelleştirmesi</tr>
   <uk>Russian локалізація Firefox</uk>
   <vi>Russian localisation of Firefox</vi>
   <zh_CN>Russian localisation of Firefox</zh_CN>
   <zh_HK>Russian localisation of Firefox</zh_HK>
   <zh_TW>Russian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-ru
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-ru
</uninstall_package_names>
</app>
