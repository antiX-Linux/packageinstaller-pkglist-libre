<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Utility
</category>

<name>
App Select
</name>

<description>
   <am>Quick applications search</am>
   <ar>Quick applications search</ar>
   <be>Quick applications search</be>
   <bg>Quick applications search</bg>
   <bn>Quick applications search</bn>
   <ca>Cerca ràpida d'aplicacions</ca>
   <cs>Quick applications search</cs>
   <da>Quick applications search</da>
   <de>Schnelle Programmsuche</de>
   <el>Γρήγορη αναζήτηση εφαρμογών</el>
   <en>Quick applications search</en>
   <es_ES>Búsqueda rápida de aplicaciones</es_ES>
   <es>Búsqueda rápida de aplicaciones</es>
   <et>Quick applications search</et>
   <eu>Quick applications search</eu>
   <fa>Quick applications search</fa>
   <fil_PH>Quick applications search</fil_PH>
   <fi>Nopea sovellusten haku</fi>
   <fr_BE>Recherche rapide d'applications</fr_BE>
   <fr>Recherche rapide d'applications</fr>
   <gl_ES>Quick applications search</gl_ES>
   <gu>Quick applications search</gu>
   <he_IL>Quick applications search</he_IL>
   <hi>त्वरित अनुप्रयोग खोज</hi>
   <hr>Quick applications search</hr>
   <hu>Quick applications search</hu>
   <id>Quick applications search</id>
   <is>Quick applications search</is>
   <it>Ricerca veloce applicazioni</it>
   <ja>アプリケーションの迅速な検索</ja>
   <kk>Quick applications search</kk>
   <ko>Quick applications search</ko>
   <ku>Quick applications search</ku>
   <lt>Quick applications search</lt>
   <mk>Quick applications search</mk>
   <mr>Quick applications search</mr>
   <nb_NO>Quick applications search</nb_NO>
   <nb>Rask programsøk</nb>
   <nl_BE>Quick applications search</nl_BE>
   <nl>Snel zoeken naar toepassingen</nl>
   <or>Quick applications search</or>
   <pl>Quick applications search</pl>
   <pt_BR>Pesquisa rápida de aplicativos</pt_BR>
   <pt>Pesquisador rápido de aplicações</pt>
   <ro>Quick applications search</ro>
   <ru>Quick applications search</ru>
   <sk>Quick applications search</sk>
   <sl>Hitro iskanje aplikacij</sl>
   <so>Quick applications search</so>
   <sq>Kërkim i shpejtë aplikacionesh</sq>
   <sr>Quick applications search</sr>
   <sv>Snabb sökning av  applikationer</sv>
   <th>Quick applications search</th>
   <tr>Hızlı uygulama arama</tr>
   <uk>Quick applications search</uk>
   <vi>Quick applications search</vi>
   <zh_CN>Quick applications search</zh_CN>
   <zh_HK>Quick applications search</zh_HK>
   <zh_TW>Quick applications search</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
app-select-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
app-select-antix
</uninstall_package_names>
</app>
