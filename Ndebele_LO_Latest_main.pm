<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Ndebele_LO_Latest_main
</name>

<description>
   <am>Ndebele Language Meta-Package for LibreOffice</am>
   <ar>Ndebele Language Meta-Package for LibreOffice</ar>
   <be>Ndebele Language Meta-Package for LibreOffice</be>
   <bg>Ndebele Language Meta-Package for LibreOffice</bg>
   <bn>Ndebele Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Ndebele per LibreOffice</ca>
   <cs>Ndebele Language Meta-Package for LibreOffice</cs>
   <da>Ndebele Language Meta-Package for LibreOffice</da>
   <de>Ndebele Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Ndebele</el>
   <en>Ndebele Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Idioma Ndebele para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Ndebele para LibreOffice</es>
   <et>Ndebele Language Meta-Package for LibreOffice</et>
   <eu>Ndebele Language Meta-Package for LibreOffice</eu>
   <fa>Ndebele Language Meta-Package for LibreOffice</fa>
   <fil_PH>Ndebele Language Meta-Package for LibreOffice</fil_PH>
   <fi>Ndebelekielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue Ndebele pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue Ndebele pour LibreOffice</fr>
   <gl_ES>Ndebele Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Ndebele Language Meta-Package for LibreOffice</gu>
   <he_IL>Ndebele Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु नेबेले भाषा मेटा-पैकेज</hi>
   <hr>Ndebele Language Meta-Package for LibreOffice</hr>
   <hu>Ndebele Language Meta-Package for LibreOffice</hu>
   <id>Ndebele Language Meta-Package for LibreOffice</id>
   <is>Ndebele Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua ndebele per LibreOffice</it>
   <ja>LibreOffice用ンデベレ語メタパッケージ</ja>
   <kk>Ndebele Language Meta-Package for LibreOffice</kk>
   <ko>Ndebele Language Meta-Package for LibreOffice</ko>
   <ku>Ndebele Language Meta-Package for LibreOffice</ku>
   <lt>Ndebele Language Meta-Package for LibreOffice</lt>
   <mk>Ndebele Language Meta-Package for LibreOffice</mk>
   <mr>Ndebele Language Meta-Package for LibreOffice</mr>
   <nb_NO>Ndebele Language Meta-Package for LibreOffice</nb_NO>
   <nb>Ndebele språkpakke for LibreOffice</nb>
   <nl_BE>Ndebele Language Meta-Package for LibreOffice</nl_BE>
   <nl>Ndebele Taal Meta-Pakket voor LibreOffice</nl>
   <or>Ndebele Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Ndebele dla LibreOffice</pl>
   <pt_BR>Sindebele Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Ndebele Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Ndebele Language Meta-Package for LibreOffice</ro>
   <ru>Ndebele Language Meta-Package for LibreOffice</ru>
   <sk>Ndebele Language Meta-Package for LibreOffice</sk>
   <sl>Ndebelski jezikovni meta-paket za LibreOffice</sl>
   <so>Ndebele Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në ndebel</sq>
   <sr>Ndebele Language Meta-Package for LibreOffice</sr>
   <sv>Ndebele Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Ndebele สำหรับ LibreOffice</th>
   <tr>LibreOffice için Bantuca Dili Üst-Paketi</tr>
   <uk>Ndebele Language Meta-Package for LibreOffice</uk>
   <vi>Ndebele Language Meta-Package for LibreOffice</vi>
   <zh_CN>Ndebele Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Ndebele Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Ndebele Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-nr
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-nr
libreoffice-gtk3
</uninstall_package_names>

</app>
