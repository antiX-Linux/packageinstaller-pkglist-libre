<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Kazakh_LO_Latest_main
</name>

<description>
   <am>Kazakh Language Meta-Package for LibreOffice</am>
   <ar>Kazakh Language Meta-Package for LibreOffice</ar>
   <be>Kazakh Language Meta-Package for LibreOffice</be>
   <bg>Kazakh Language Meta-Package for LibreOffice</bg>
   <bn>Kazakh Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Kazakh per LibreOffice</ca>
   <cs>Kazakh Language Meta-Package for LibreOffice</cs>
   <da>Kazakh Language Meta-Package for LibreOffice</da>
   <de>Kasachisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Καζακχ</el>
   <en>Kazakh Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma kazajo para LibreOffice</es_ES>
   <es>Metapaquete de idioma Kazajo para LibreOffice</es>
   <et>Kazakh Language Meta-Package for LibreOffice</et>
   <eu>Kazakh Language Meta-Package for LibreOffice</eu>
   <fa>Kazakh Language Meta-Package for LibreOffice</fa>
   <fil_PH>Kazakh Language Meta-Package for LibreOffice</fil_PH>
   <fi>Kazakinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue kazakhe pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue kazakhe pour LibreOffice</fr>
   <gl_ES>Casaco Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Kazakh Language Meta-Package for LibreOffice</gu>
   <he_IL>Kazakh Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु कज़ाख भाषा मेटा-पैकेज</hi>
   <hr>Kazakh Language Meta-Package for LibreOffice</hr>
   <hu>Kazakh Language Meta-Package for LibreOffice</hu>
   <id>Kazakh Language Meta-Package for LibreOffice</id>
   <is>Kazakh Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua kazaka per LibreOffice</it>
   <ja>LibreOffice用カザフ語メタパッケージ</ja>
   <kk>Kazakh Language Meta-Package for LibreOffice</kk>
   <ko>Kazakh Language Meta-Package for LibreOffice</ko>
   <ku>Kazakh Language Meta-Package for LibreOffice</ku>
   <lt>Kazakh Language Meta-Package for LibreOffice</lt>
   <mk>Kazakh Language Meta-Package for LibreOffice</mk>
   <mr>Kazakh Language Meta-Package for LibreOffice</mr>
   <nb_NO>Kazakh Language Meta-Package for LibreOffice</nb_NO>
   <nb>Kasakhisk språkpakke for LibreOffice</nb>
   <nl_BE>Kazakh Language Meta-Package for LibreOffice</nl_BE>
   <nl>Kazaks Taal Meta-Pakket voor LibreOffice</nl>
   <or>Kazakh Language Meta-Package for LibreOffice</or>
   <pl>Kazachski  meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Cazaque Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Cazaque Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Kazakh Language Meta-Package for LibreOffice</ro>
   <ru>Kazakh Language Meta-Package for LibreOffice</ru>
   <sk>Kazakh Language Meta-Package for LibreOffice</sk>
   <sl>Kazahski jezikovni meta-paket za LibreOffice</sl>
   <so>Kazakh Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në kazakistanisht</sq>
   <sr>Kazakh Language Meta-Package for LibreOffice</sr>
   <sv>Kazakiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Kazakh สำหรับ LibreOffice</th>
   <tr>LibreOffice için Kazakça Dili Üst-Paketi</tr>
   <uk>Kazakh Language Meta-Package for LibreOffice</uk>
   <vi>Kazakh Language Meta-Package for LibreOffice</vi>
   <zh_CN>Kazakh Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Kazakh Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Kazakh Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-kk
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-kk
libreoffice-gtk3
</uninstall_package_names>

</app>
