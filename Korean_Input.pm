<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Korean_Input
</name>

<description>
   <am>Korean Fonts and ibus</am>
   <ar>Korean Fonts and ibus</ar>
   <be>Korean Fonts and ibus</be>
   <bg>Korean Fonts and ibus</bg>
   <bn>Korean Fonts and ibus</bn>
   <ca>Tipus de lletra i ibus per Coreà</ca>
   <cs>Korean Fonts and ibus</cs>
   <da>Koreansk skrifttyper og ibus</da>
   <de>Koreanische Schriftzeichen und IBus</de>
   <el>Κορεατικά γραμματοσειρές και ibus</el>
   <en>Korean Fonts and ibus</en>
   <es_ES>Fuentes Coreanas e ibus</es_ES>
   <es>Fuentes Coreanas e ibus</es>
   <et>Korean Fonts and ibus</et>
   <eu>Korean Fonts and ibus</eu>
   <fa>Korean Fonts and ibus</fa>
   <fil_PH>Korean Fonts and ibus</fil_PH>
   <fi>Korean Fonts and ibus</fi>
   <fr_BE>Polices coréennes et ibus</fr_BE>
   <fr>Polices coréennes et ibus</fr>
   <gl_ES>Korean Fonts and ibus</gl_ES>
   <gu>Korean Fonts and ibus</gu>
   <he_IL>Korean Fonts and ibus</he_IL>
   <hi>कोरियाई मुद्रलिपि व Ibus</hi>
   <hr>Korean Fonts and ibus</hr>
   <hu>Korean Fonts and ibus</hu>
   <id>Korean Fonts and ibus</id>
   <is>Korean Fonts and ibus</is>
   <it>Font coreani e ibus</it>
   <ja>韓国（朝鮮）語フォントと ibus 入力</ja>
   <kk>Korean Fonts and ibus</kk>
   <ko>Korean Fonts and ibus</ko>
   <ku>Korean Fonts and ibus</ku>
   <lt>Korėjietiški šriftai ir ibus</lt>
   <mk>Korean Fonts and ibus</mk>
   <mr>Korean Fonts and ibus</mr>
   <nb_NO>Korean Fonts and ibus</nb_NO>
   <nb>Koreanske skrifter og ibus</nb>
   <nl_BE>Korean Fonts and ibus</nl_BE>
   <nl>Koreaanse Fonts en ibus</nl>
   <or>Korean Fonts and ibus</or>
   <pl>Koreańskie fonty i ibus</pl>
   <pt_BR>Pacotes de Fontes Coreano e ibus</pt_BR>
   <pt>Coreano Fontes e ibus</pt>
   <ro>Korean Fonts and ibus</ro>
   <ru>Korean Fonts and ibus</ru>
   <sk>Korean Fonts and ibus</sk>
   <sl>Korejske pisave in ibus</sl>
   <so>Korean Fonts and ibus</so>
   <sq>Shkronja dhe ibus për koreançen</sq>
   <sr>Korean Fonts and ibus</sr>
   <sv>Koreanska Typsnitt och ibus</sv>
   <th>Korean Fonts and ibus</th>
   <tr>Korece Yazı Tipleri ve ibus</tr>
   <uk>Korean Fonts and ibus</uk>
   <vi>Korean Fonts and ibus</vi>
   <zh_CN>Korean Fonts and ibus</zh_CN>
   <zh_HK>Korean Fonts and ibus</zh_HK>
   <zh_TW>Korean Fonts and ibus</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
im-config
ibus-hangul
ibus-gtk
fonts-baekmuk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
im-config
ibus-hangul
ibus-gtk
fonts-baekmuk
</uninstall_package_names>

</app>
