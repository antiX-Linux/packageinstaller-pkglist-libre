<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Norwegian_Bokmal_Default_Firefox_esr
</name>

<description>
   <am>Norwegian localisation of Firefox ESR</am>
   <ar>Norwegian localisation of Firefox ESR</ar>
   <be>Norwegian localisation of Firefox ESR</be>
   <bg>Norwegian localisation of Firefox ESR</bg>
   <bn>Norwegian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Noruec</ca>
   <cs>Norwegian localisation of Firefox ESR</cs>
   <da>Norwegian localisation of Firefox ESR</da>
   <de>Norwegische Lokalisation von “Firefox ESR”</de>
   <el>Νορβηγικά για το Firefox ESR</el>
   <en>Norwegian localisation of Firefox ESR</en>
   <es_ES>Localización Noruego de Firefox ESR</es_ES>
   <es>Localización Noruego de Firefox ESR</es>
   <et>Norwegian localisation of Firefox ESR</et>
   <eu>Norwegian localisation of Firefox ESR</eu>
   <fa>Norwegian localisation of Firefox ESR</fa>
   <fil_PH>Norwegian localisation of Firefox ESR</fil_PH>
   <fi>Norwegian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en norvégien pour Firefox ESR</fr_BE>
   <fr>Localisation en norvégien pour Firefox ESR</fr>
   <gl_ES>Norwegian localisation of Firefox ESR</gl_ES>
   <gu>Norwegian localisation of Firefox ESR</gu>
   <he_IL>Norwegian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का नॉर्वेजियाई संस्करण</hi>
   <hr>Norwegian localisation of Firefox ESR</hr>
   <hu>Norwegian localisation of Firefox ESR</hu>
   <id>Norwegian localisation of Firefox ESR</id>
   <is>Norwegian localisation of Firefox ESR</is>
   <it>Localizzazione norvegese di Firefox ESR</it>
   <ja>ノルウェー語版 Firefox ESR</ja>
   <kk>Norwegian localisation of Firefox ESR</kk>
   <ko>Norwegian localisation of Firefox ESR</ko>
   <ku>Norwegian localisation of Firefox ESR</ku>
   <lt>Norwegian localisation of Firefox ESR</lt>
   <mk>Norwegian localisation of Firefox ESR</mk>
   <mr>Norwegian localisation of Firefox ESR</mr>
   <nb_NO>Norwegian localisation of Firefox ESR</nb_NO>
   <nb>Bokmålslokaltilpassing av Firefox ESR</nb>
   <nl_BE>Norwegian localisation of Firefox ESR</nl_BE>
   <nl>Norwegian localisation of Firefox ESR</nl>
   <or>Norwegian localisation of Firefox ESR</or>
   <pl>Norwegian localisation of Firefox ESR</pl>
   <pt_BR>Norueguês Localização para o Firefox ESR</pt_BR>
   <pt>Norueguês Localização para Firefox ESR</pt>
   <ro>Norwegian localisation of Firefox ESR</ro>
   <ru>Norwegian localisation of Firefox ESR</ru>
   <sk>Norwegian localisation of Firefox ESR</sk>
   <sl>Norveške krajevne nastavitve za Firefox ESR</sl>
   <so>Norwegian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në norvegjisht</sq>
   <sr>Norwegian localisation of Firefox ESR</sr>
   <sv>Norsk lokalisering av Firefox ESR</sv>
   <th>Norwegian localisation of Firefox ESR</th>
   <tr>Firefox ESR Norveççe yerelleştirmesi</tr>
   <uk>Norwegian localisation of Firefox ESR</uk>
   <vi>Norwegian localisation of Firefox ESR</vi>
   <zh_CN>Norwegian localisation of Firefox ESR</zh_CN>
   <zh_HK>Norwegian localisation of Firefox ESR</zh_HK>
   <zh_TW>Norwegian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-nb-no
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-nb-no
</uninstall_package_names>

</app>
