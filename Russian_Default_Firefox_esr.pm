<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Russian_Default_Firefox_esr
</name>

<description>
   <am>Russian localisation of Firefox ESR</am>
   <ar>Russian localisation of Firefox ESR</ar>
   <be>Russian localisation of Firefox ESR</be>
   <bg>Russian localisation of Firefox ESR</bg>
   <bn>Russian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Rus</ca>
   <cs>Russian localisation of Firefox ESR</cs>
   <da>Russian localisation of Firefox ESR</da>
   <de>Russische Lokalisation von “Firefox ESR”</de>
   <el>Ρωσικά για τον Firefox ESR</el>
   <en>Russian localisation of Firefox ESR</en>
   <es_ES>Localización Rusa de Firefox ESR</es_ES>
   <es>Localización Ruso de Firefox ESR</es>
   <et>Russian localisation of Firefox ESR</et>
   <eu>Russian localisation of Firefox ESR</eu>
   <fa>Russian localisation of Firefox ESR</fa>
   <fil_PH>Russian localisation of Firefox ESR</fil_PH>
   <fi>Russian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en langue russe pour Firefox ESR</fr_BE>
   <fr>Localisation en langue russe pour Firefox ESR</fr>
   <gl_ES>Russian localisation of Firefox ESR</gl_ES>
   <gu>Russian localisation of Firefox ESR</gu>
   <he_IL>Russian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का रुसी संस्करण</hi>
   <hr>Russian localisation of Firefox ESR</hr>
   <hu>Russian localisation of Firefox ESR</hu>
   <id>Russian localisation of Firefox ESR</id>
   <is>Russian localisation of Firefox ESR</is>
   <it>Localizzazione russa di Firefox ESR</it>
   <ja>ロシア語版 Firefox ESR</ja>
   <kk>Russian localisation of Firefox ESR</kk>
   <ko>Russian localisation of Firefox ESR</ko>
   <ku>Russian localisation of Firefox ESR</ku>
   <lt>Russian localisation of Firefox ESR</lt>
   <mk>Russian localisation of Firefox ESR</mk>
   <mr>Russian localisation of Firefox ESR</mr>
   <nb_NO>Russian localisation of Firefox ESR</nb_NO>
   <nb>Russisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Russian localisation of Firefox ESR</nl_BE>
   <nl>Russian localisation of Firefox ESR</nl>
   <or>Russian localisation of Firefox ESR</or>
   <pl>Russian localisation of Firefox ESR</pl>
   <pt_BR>Russo Localização para o Firefox ESR</pt_BR>
   <pt>Russo Localização para Firefox ESR</pt>
   <ro>Russian localisation of Firefox ESR</ro>
   <ru>Russian localisation of Firefox ESR</ru>
   <sk>Russian localisation of Firefox ESR</sk>
   <sl>Ruske krajevne nastavitve za Firefox ESR</sl>
   <so>Russian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në rusisht</sq>
   <sr>Russian localisation of Firefox ESR</sr>
   <sv>Rysk lokalisering av Firefox ESR</sv>
   <th>Russian localisation of Firefox ESR</th>
   <tr>Firefox ESR Rusça yerelleştirmesi</tr>
   <uk>Russian localisation of Firefox ESR</uk>
   <vi>Russian localisation of Firefox ESR</vi>
   <zh_CN>Russian localisation of Firefox ESR</zh_CN>
   <zh_HK>Russian localisation of Firefox ESR</zh_HK>
   <zh_TW>Russian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ru
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ru
</uninstall_package_names>

</app>
