<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Welsh_LO_Latest_full
</name>

<description>
   <am>Welsh Language Meta-Package for LibreOffice</am>
   <ar>Welsh Language Meta-Package for LibreOffice</ar>
   <be>Welsh Language Meta-Package for LibreOffice</be>
   <bg>Welsh Language Meta-Package for LibreOffice</bg>
   <bn>Welsh Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Gal·lès per LibreOffice</ca>
   <cs>Welsh Language Meta-Package for LibreOffice</cs>
   <da>Welsh Language Meta-Package for LibreOffice</da>
   <de>Walisisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Ουαλικά</el>
   <en>Welsh Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Galés para LibreOffice</es_ES>
   <es>Metapaquete de idioma Galés para LibreOffice</es>
   <et>Welsh Language Meta-Package for LibreOffice</et>
   <eu>Welsh Language Meta-Package for LibreOffice</eu>
   <fa>Welsh Language Meta-Package for LibreOffice</fa>
   <fil_PH>Welsh Language Meta-Package for LibreOffice</fil_PH>
   <fi>Kymrinkielinen (Wales) kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue galloise pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue galloise pour LibreOffice</fr>
   <gl_ES>Galés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Welsh Language Meta-Package for LibreOffice</gu>
   <he_IL>Welsh Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु वेल्श भाषा मेटा-पैकेज</hi>
   <hr>Welsh Language Meta-Package for LibreOffice</hr>
   <hu>Welsh Language Meta-Package for LibreOffice</hu>
   <id>Welsh Language Meta-Package for LibreOffice</id>
   <is>Welsh Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua gallese per LibreOffice</it>
   <ja>LibreOffice用 ウェールズ語メタパッケージ</ja>
   <kk>Welsh Language Meta-Package for LibreOffice</kk>
   <ko>Welsh Language Meta-Package for LibreOffice</ko>
   <ku>Welsh Language Meta-Package for LibreOffice</ku>
   <lt>Welsh Language Meta-Package for LibreOffice</lt>
   <mk>Welsh Language Meta-Package for LibreOffice</mk>
   <mr>Welsh Language Meta-Package for LibreOffice</mr>
   <nb_NO>Welsh Language Meta-Package for LibreOffice</nb_NO>
   <nb>Walisisk språkpakke for LibreOffice</nb>
   <nl_BE>Welsh Language Meta-Package for LibreOffice</nl_BE>
   <nl>Welsh Taal Meta-Pakket voor LibreOffice</nl>
   <or>Welsh Language Meta-Package for LibreOffice</or>
   <pl>Walijski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Galês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Galês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Welsh Language Meta-Package for LibreOffice</ro>
   <ru>Welsh Language Meta-Package for LibreOffice</ru>
   <sk>Welsh Language Meta-Package for LibreOffice</sk>
   <sl>Velški jezikovni meta-paket za LibreOffice</sl>
   <so>Welsh Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në uellsisht</sq>
   <sr>Welsh Language Meta-Package for LibreOffice</sr>
   <sv>Walesiska Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Welsh สำหรับ LibreOffice</th>
   <tr>LibreOffice için Galce Dili Üst-Paketi</tr>
   <uk>Welsh Language Meta-Package for LibreOffice</uk>
   <vi>Welsh Language Meta-Package for LibreOffice</vi>
   <zh_CN>Welsh Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Welsh Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Welsh Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-cy
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-cy
</uninstall_package_names>

</app>
