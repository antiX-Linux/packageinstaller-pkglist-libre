<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
Retroarch
</name>

<description>
   <en>Simple frontend for the libretro library</en>
</description>

<installable>
all
</installable>

<screenshot>
</screenshot>

<preinstall>

</preinstall>

<install_package_names>
retroarch
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
retroarch
</uninstall_package_names>
</app>
