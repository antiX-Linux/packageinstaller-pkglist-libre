<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
LXQT
</name>

<description>
   <am>Basic install of LXQT</am>
   <ar>Basic install of LXQT</ar>
   <be>Basic install of LXQT</be>
   <bg>Basic install of LXQT</bg>
   <bn>Basic install of LXQT</bn>
   <ca>Instal·lació bàsica de LXQT</ca>
   <cs>Basic install of LXQT</cs>
   <da>Basic install of LXQT</da>
   <de>Basisinstallation der Desktop-Umgebung “LXQt”</de>
   <el>Βασική εγκατάσταση του LXQT</el>
   <en>Basic install of LXQT</en>
   <es_ES>Instalación básica de LXQT</es_ES>
   <es>Instalación básica de LXQT</es>
   <et>Basic install of LXQT</et>
   <eu>Basic install of LXQT</eu>
   <fa>Basic install of LXQT</fa>
   <fil_PH>Basic install of LXQT</fil_PH>
   <fi>LXQT-työpöytäympäristön perusasennus</fi>
   <fr_BE>Installation basique de LXQT</fr_BE>
   <fr>Installation basique de LXQT</fr>
   <gl_ES>Basic install of LXQT</gl_ES>
   <gu>Basic install of LXQT</gu>
   <he_IL>Basic install of LXQT</he_IL>
   <hi>एलएक्सक्यूटी का सामान्य इंस्टॉल</hi>
   <hr>Basic install of LXQT</hr>
   <hu>Basic install of LXQT</hu>
   <id>Basic install of LXQT</id>
   <is>Basic install of LXQT</is>
   <it>Installazione base di LXQT</it>
   <ja>LXQT の基本インストール</ja>
   <kk>Basic install of LXQT</kk>
   <ko>Basic install of LXQT</ko>
   <ku>Basic install of LXQT</ku>
   <lt>Basic install of LXQT</lt>
   <mk>Basic install of LXQT</mk>
   <mr>Basic install of LXQT</mr>
   <nb_NO>Basic install of LXQT</nb_NO>
   <nb>Grunnleggende installasjon av LXQT</nb>
   <nl_BE>Basic install of LXQT</nl_BE>
   <nl>Basic install of LXQT</nl>
   <or>Basic install of LXQT</or>
   <pl>Basic install of LXQT</pl>
   <pt_BR>Instalação Básica do Ambiente de Trabalho LXQT</pt_BR>
   <pt>Instalação básica do Ambiente de Trabalho KXQT</pt>
   <ro>Basic install of LXQT</ro>
   <ru>Basic install of LXQT</ru>
   <sk>Basic install of LXQT</sk>
   <sl>Osnovna namestitev LXQT</sl>
   <so>Basic install of LXQT</so>
   <sq>Instalim elementar i LXQT-së</sq>
   <sr>Basic install of LXQT</sr>
   <sv>Bas-installation av LXQT</sv>
   <th>Basic install of LXQT</th>
   <tr>LXQT temel kurulumu</tr>
   <uk>Basic install of LXQT</uk>
   <vi>Basic install of LXQT</vi>
   <zh_CN>Basic install of LXQT</zh_CN>
   <zh_HK>Basic install of LXQT</zh_HK>
   <zh_TW>Basic install of LXQT</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
lxqt
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
lxqt
</uninstall_package_names>
</app>
