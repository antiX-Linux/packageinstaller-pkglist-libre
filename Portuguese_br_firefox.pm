<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Portugese(BR)_Firefox
</name>

<description>
   <am>Portuguese(BR) localisation of Firefox</am>
   <ar>Portuguese(BR) localisation of Firefox</ar>
   <be>Portuguese(BR) localisation of Firefox</be>
   <bg>Portuguese(BR) localisation of Firefox</bg>
   <bn>Portuguese(BR) localisation of Firefox</bn>
   <ca>Localització de Firefox en Portuguès</ca>
   <cs>Portuguese(BR) localisation of Firefox</cs>
   <da>Portugisisk (brasilien) oversættelse af Firefox</da>
   <de>Portugiesische (BR) Lokalisierung von Firefox</de>
   <el>Πορτογαλικά(Βραζιλίας) για το Firefox</el>
   <en>Portuguese(BR) localisation of Firefox</en>
   <es_ES>Localización Portuguesa (BR) de Firefox</es_ES>
   <es>Localización Portuguesa (BR) de Firefox</es>
   <et>Portuguese(BR) localisation of Firefox</et>
   <eu>Portuguese(BR) localisation of Firefox</eu>
   <fa>Portuguese(BR) localisation of Firefox</fa>
   <fil_PH>Portuguese(BR) localisation of Firefox</fil_PH>
   <fi>Portugalilainen (BR) Firefox-kotoistus</fi>
   <fr_BE>Localisation portugaise (BR) pour Firefox</fr_BE>
   <fr>Localisation portugaise (BR) pour Firefox</fr>
   <gl_ES>Localización de Firefox ao portugués brasileiro</gl_ES>
   <gu>Portuguese(BR) localisation of Firefox</gu>
   <he_IL>Portuguese(BR) localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का पुर्तगाली(ब्राज़ील) संस्करण</hi>
   <hr>Portuguese(BR) localisation of Firefox</hr>
   <hu>Portuguese(BR) localisation of Firefox</hu>
   <id>Portuguese(BR) localisation of Firefox</id>
   <is>Portuguese(BR) localisation of Firefox</is>
   <it>Localizzazione portoghese(BR) di Firefox</it>
   <ja>Firefox のポルトガル(BR)語版</ja>
   <kk>Portuguese(BR) localisation of Firefox</kk>
   <ko>Portuguese(BR) localisation of Firefox</ko>
   <ku>Portuguese(BR) localisation of Firefox</ku>
   <lt>Portuguese(BR) localisation of Firefox</lt>
   <mk>Portuguese(BR) localisation of Firefox</mk>
   <mr>Portuguese(BR) localisation of Firefox</mr>
   <nb_NO>Portuguese(BR) localisation of Firefox</nb_NO>
   <nb>Brasiliansk portugisisk lokaltilpassing av Firefox</nb>
   <nl_BE>Portuguese(BR) localisation of Firefox</nl_BE>
   <nl>Portugese(BR) lokalisatie van Firefox</nl>
   <or>Portuguese(BR) localisation of Firefox</or>
   <pl>Portugalska (BR) lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Português (BR) Localização para o Firefox</pt_BR>
   <pt>Português(BR) Localização para Firefox)</pt>
   <ro>Portuguese(BR) localisation of Firefox</ro>
   <ru>Portuguese(BR) localisation of Firefox</ru>
   <sk>Portuguese(BR) localisation of Firefox</sk>
   <sl>Portugalske (BR) krajevne nastavitve za Firefox</sl>
   <so>Portuguese(BR) localisation of Firefox</so>
   <sq>Portuguese(BR) localisation of Firefox</sq>
   <sr>Portuguese(BR) localisation of Firefox</sr>
   <sv>Portugisisk (BR) lokalisering av Firefox </sv>
   <th>Portuguese(BR) localisation ของ Firefox</th>
   <tr>Firefox'un Brezilya Portekizcesi yerelleştirmesi</tr>
   <uk>Portuguese(BR) локалізація Firefox</uk>
   <vi>Portuguese(BR) localisation of Firefox</vi>
   <zh_CN>Portuguese(BR) localisation of Firefox</zh_CN>
   <zh_HK>Portuguese(BR) localisation of Firefox</zh_HK>
   <zh_TW>Portuguese(BR) localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-pt-br
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-pt-br
</uninstall_package_names>
</app>
