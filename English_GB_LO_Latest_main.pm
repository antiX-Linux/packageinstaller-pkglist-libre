<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English_GB_LO_Latest_main
</name>

<description>
   <am>GB English Help for LibreOffice</am>
   <ar>GB English Help for LibreOffice</ar>
   <be>GB English Help for LibreOffice</be>
   <bg>GB English Help for LibreOffice</bg>
   <bn>GB English Help for LibreOffice</bn>
   <ca>Ajuda en anglès (UK) per LibreOffice</ca>
   <cs>GB English Help for LibreOffice</cs>
   <da>Engelsk (storbritannien) hjælp til LibreOffice</da>
   <de>English (GB) Help for LibreOffice</de>
   <el>Βοήθεια για το LibreOffice στα Αγγλικά(GΒ)</el>
   <en>GB English Help for LibreOffice</en>
   <es_ES>Ayuda Inglesa (GB) para LibreOffice</es_ES>
   <es>Ayuda Inglesa (GB) para LibreOffice</es>
   <et>GB English Help for LibreOffice</et>
   <eu>GB English Help for LibreOffice</eu>
   <fa>GB English Help for LibreOffice</fa>
   <fil_PH>GB English Help for LibreOffice</fil_PH>
   <fi>GB-englantilainen apuopas LibreOffice:lle</fi>
   <fr_BE>Aide anglais_GB pour LibreOffice</fr_BE>
   <fr>Aide anglais_GB pour LibreOffice</fr>
   <gl_ES>Axuda para LibreOffice en British English</gl_ES>
   <gu>GB English Help for LibreOffice</gu>
   <he_IL>GB English Help for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु ब्रिटिश अंग्रेजी में सहायता</hi>
   <hr>GB English Help for LibreOffice</hr>
   <hu>GB English Help for LibreOffice</hu>
   <id>GB English Help for LibreOffice</id>
   <is>GB English Help for LibreOffice</is>
   <it>Guida in inglese GB per LibreOffice</it>
   <ja>LibreOffice用のイギリス英語ヘルプ</ja>
   <kk>GB English Help for LibreOffice</kk>
   <ko>GB English Help for LibreOffice</ko>
   <ku>GB English Help for LibreOffice</ku>
   <lt>GB English Help for LibreOffice</lt>
   <mk>GB English Help for LibreOffice</mk>
   <mr>GB English Help for LibreOffice</mr>
   <nb_NO>GB English Help for LibreOffice</nb_NO>
   <nb>Britisk engelsk hjelpetekst for LibreOffice</nb>
   <nl_BE>GB English Help for LibreOffice</nl_BE>
   <nl>GB Engelse Hulp voor LibreOffice</nl>
   <or>GB English Help for LibreOffice</or>
   <pl>Angielski Brytyjski pomoc dla LibreOffice</pl>
   <pt_BR>Inglês da Grã-Bretanha Ajuda para o LibreOffice</pt_BR>
   <pt>Inglês GB Help para LibreOffice</pt>
   <ro>GB English Help for LibreOffice</ro>
   <ru>GB English Help for LibreOffice</ru>
   <sk>GB English Help for LibreOffice</sk>
   <sl>VB angleščina - pomoč za LibreOffice</sl>
   <so>GB English Help for LibreOffice</so>
   <sq>Ndihmë për LibreOffice-in në anglishte britanike</sq>
   <sr>GB English Help for LibreOffice</sr>
   <sv>GB Engelsk Hjälp för LibreOffice</sv>
   <th>ความช่วยเหลือภาษา GB English สำหรับ LibreOffice</th>
   <tr>LibreOffice için Britanya ingilizcesi Yardımı</tr>
   <uk>GB English довідка LibreOffice</uk>
   <vi>GB English Help for LibreOffice</vi>
   <zh_CN>LibreOffice 英式英语帮助</zh_CN>
   <zh_HK>GB English Help for LibreOffice</zh_HK>
   <zh_TW>GB English Help for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-l10n-en-gb
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-l10n-en-gb
</uninstall_package_names>

</app>
