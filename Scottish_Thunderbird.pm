<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Scottish_Thunderbird
</name>

<description>
   <am>Gaelic (Scottish) localisation of Thunderbird</am>
   <ar>Gaelic (Scottish) localisation of Thunderbird</ar>
   <be>Gaelic (Scottish) localisation of Thunderbird</be>
   <bg>Gaelic (Scottish) localisation of Thunderbird</bg>
   <bn>Gaelic (Scottish) localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Gaèlic (Escocès)</ca>
   <cs>Gaelic (Scottish) localisation of Thunderbird</cs>
   <da>Gaelic (Scottish) localisation of Thunderbird</da>
   <de>Schottisch-Gälische (Gàidhlig) Lokalisation von “Thunderbird”</de>
   <el>Scottish Gaelic για Thunderbird</el>
   <en>Gaelic (Scottish) localisation of Thunderbird</en>
   <es_ES>Localización Gaélico Escocés de Thunderbird</es_ES>
   <es>Localización Gaélico Escocés de Thunderbird</es>
   <et>Gaelic (Scottish) localisation of Thunderbird</et>
   <eu>Gaelic (Scottish) localisation of Thunderbird</eu>
   <fa>Gaelic (Scottish) localisation of Thunderbird</fa>
   <fil_PH>Gaelic (Scottish) localisation of Thunderbird</fil_PH>
   <fi>Gaelic (Scottish) localisation of Thunderbird</fi>
   <fr_BE>Localisation en gaélique (Ecosse) pour Thunderbird</fr_BE>
   <fr>Localisation en gaélique (Ecosse) pour Thunderbird</fr>
   <gl_ES>Gaelic (Scottish) localisation of Thunderbird</gl_ES>
   <gu>Gaelic (Scottish) localisation of Thunderbird</gu>
   <he_IL>Gaelic (Scottish) localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का गेलिक (स्कॉटिश) संस्करण</hi>
   <hr>Gaelic (Scottish) localisation of Thunderbird</hr>
   <hu>Gaelic (Scottish) localisation of Thunderbird</hu>
   <id>Gaelic (Scottish) localisation of Thunderbird</id>
   <is>Gaelic (Scottish) localisation of Thunderbird</is>
   <it>Localizzazione gaelic (scottish) di Thunderbird</it>
   <ja>高地スコットランド・ゲール語版 Thunderbird</ja>
   <kk>Gaelic (Scottish) localisation of Thunderbird</kk>
   <ko>Gaelic (Scottish) localisation of Thunderbird</ko>
   <ku>Gaelic (Scottish) localisation of Thunderbird</ku>
   <lt>Gaelic (Scottish) localisation of Thunderbird</lt>
   <mk>Gaelic (Scottish) localisation of Thunderbird</mk>
   <mr>Gaelic (Scottish) localisation of Thunderbird</mr>
   <nb_NO>Gaelic (Scottish) localisation of Thunderbird</nb_NO>
   <nb>Gælisk (skotsk) lokaltilpassing av Thunderbird</nb>
   <nl_BE>Gaelic (Scottish) localisation of Thunderbird</nl_BE>
   <nl>Gaelic (Scottish) localisation of Thunderbird</nl>
   <or>Gaelic (Scottish) localisation of Thunderbird</or>
   <pl>Gaelic (Scottish) localisation of Thunderbird</pl>
   <pt_BR>Gaélico Escocês Localização para o Thunderbird</pt_BR>
   <pt>Gaélico_escocês Localização para Thunderbird</pt>
   <ro>Gaelic (Scottish) localisation of Thunderbird</ro>
   <ru>Gaelic (Scottish) localisation of Thunderbird</ru>
   <sk>Gaelic (Scottish) localisation of Thunderbird</sk>
   <sl>Škotsko gelške krajevne nastavitve za Thunderbird</sl>
   <so>Gaelic (Scottish) localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në gaelike (skotisht)</sq>
   <sr>Gaelic (Scottish) localisation of Thunderbird</sr>
   <sv>Gaelisk (Skottsk) lokalisering av Thunderbird</sv>
   <th>Gaelic (Scottish) localisation of Thunderbird</th>
   <tr>Thunderbird'ün İskoçça yerelleştirmesi</tr>
   <uk>Gaelic (Scottish) localisation of Thunderbird</uk>
   <vi>Gaelic (Scottish) localisation of Thunderbird</vi>
   <zh_CN>Gaelic (Scottish) localisation of Thunderbird</zh_CN>
   <zh_HK>Gaelic (Scottish) localisation of Thunderbird</zh_HK>
   <zh_TW>Gaelic (Scottish) localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-gd
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-gd
</uninstall_package_names>

</app>
