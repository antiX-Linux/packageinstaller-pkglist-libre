<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
OpenTTD
</name>

<description>
   <en>Reimplementation of Transport Tycoon Deluxe with enhancements</en>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
openttd
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
openttd
</uninstall_package_names>
</app>
