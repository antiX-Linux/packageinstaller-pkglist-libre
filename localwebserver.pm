<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Server
</category>

<name>
Local Web Server
</name>

<description>
   <am>apache2, php7, mariaDB</am>
   <ar>apache2, php7, mariaDB</ar>
   <be>apache2, php7, mariaDB</be>
   <bg>apache2, php7, mariaDB</bg>
   <bn>apache2, php7, mariaDB</bn>
   <ca>apache2, php7, mariaDB</ca>
   <cs>apache2, php7, mariaDB</cs>
   <da>apache2, php7, mariaDB</da>
   <de>Apache2, PHP7, MariaDB</de>
   <el>apache2, php7, mariaDB</el>
   <en>apache2, php7, mariaDB</en>
   <es_ES>apache2, php7, mariaDB</es_ES>
   <es>apache2, php7, mariaDB</es>
   <et>apache2, php7, mariaDB</et>
   <eu>apache2, php7, mariaDB</eu>
   <fa>apache2, php7, mariaDB</fa>
   <fil_PH>apache2, php7, mariaDB</fil_PH>
   <fi>apache2, php7, mariaDB</fi>
   <fr_BE>apache2, php7, mariaDB</fr_BE>
   <fr>apache2, php7, mariaDB</fr>
   <gl_ES>apache2, php7, mariaDB</gl_ES>
   <gu>apache2, php7, mariaDB</gu>
   <he_IL>apache2, php7, mariaDB</he_IL>
   <hi>अपाचे2, पीएचपी7, मारिया डीबी</hi>
   <hr>apache2, php7, mariaDB</hr>
   <hu>apache2, php7, mariaDB</hu>
   <id>apache2, php7, mariaDB</id>
   <is>apache2, php7, mariaDB</is>
   <it>apache2, php7, mariaDB</it>
   <ja>apache2, php7, mariaDB</ja>
   <kk>apache2, php7, mariaDB</kk>
   <ko>apache2, php7, mariaDB</ko>
   <ku>apache2, php7, mariaDB</ku>
   <lt>apache2, php7, mariaDB</lt>
   <mk>apache2, php7, mariaDB</mk>
   <mr>apache2, php7, mariaDB</mr>
   <nb_NO>apache2, php7, mariaDB</nb_NO>
   <nb>apache2, php7, mariaDB</nb>
   <nl_BE>apache2, php7, mariaDB</nl_BE>
   <nl>apache2, php7, mariaDB</nl>
   <or>apache2, php7, mariaDB</or>
   <pl>Apache2, PHP7, MariaDB</pl>
   <pt_BR>apache2, php7, mariaDB</pt_BR>
   <pt>apache2, php7, mariaDB</pt>
   <ro>apache2, php7, mariaDB</ro>
   <ru>ПО веб-сервера: apache2, php7, mariaDB</ru>
   <sk>apache2, php7, mariaDB</sk>
   <sl>apache2, php7, mariaDB</sl>
   <so>apache2, php7, mariaDB</so>
   <sq>apache2, php7, mariaDB</sq>
   <sr>apache2, php7, mariaDB</sr>
   <sv>apache2, php7, mariaDB</sv>
   <th>apache2, php7, mariaDB</th>
   <tr>apache2, php7, mariaDB</tr>
   <uk>apache2, php7, mariaDB</uk>
   <vi>apache2, php7, mariaDB</vi>
   <zh_CN>apache2, php7, mariaDB</zh_CN>
   <zh_HK>apache2, php7, mariaDB</zh_HK>
   <zh_TW>apache2, php7, mariaDB</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
apache2
apache2-utils
php7.4
php7.4-mysql
php7.4-common
mariadb-client
mariadb-server
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
apache2
apache2-utils
php7.4
php7.4-mysql
php7.4-common
mariadb-client
mariadb-server
</uninstall_package_names>
</app>
