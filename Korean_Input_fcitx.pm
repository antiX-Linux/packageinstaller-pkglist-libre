<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Korean_Input_fcitx
</name>

<description>
   <am>Korean Fonts and fcitx</am>
   <ar>Korean Fonts and fcitx</ar>
   <be>Korean Fonts and fcitx</be>
   <bg>Korean Fonts and fcitx</bg>
   <bn>Korean Fonts and fcitx</bn>
   <ca>Tipus de lletra i fcitx per Coreà</ca>
   <cs>Korean Fonts and fcitx</cs>
   <da>Koreansk skrifttyper og fcitx</da>
   <de>Koreanische Schriftzeichen und Fcitx</de>
   <el>Κορεατικές γραμματοσειρές και fcitx</el>
   <en>Korean Fonts and fcitx</en>
   <es_ES>Fuentes Coreanas y fcitx</es_ES>
   <es>Fuentes Coreanas y fcitx</es>
   <et>Korean Fonts and fcitx</et>
   <eu>Korean Fonts and fcitx</eu>
   <fa>Korean Fonts and fcitx</fa>
   <fil_PH>Korean Fonts and fcitx</fil_PH>
   <fi>Korealaiset kirjasimet ja fcitx</fi>
   <fr_BE>Polices coréennes et fcitx</fr_BE>
   <fr>Polices coréennes et fcitx</fr>
   <gl_ES>Fontes e fcitx do corean</gl_ES>
   <gu>Korean Fonts and fcitx</gu>
   <he_IL>Korean Fonts and fcitx</he_IL>
   <hi>कोरियाई मुद्रलिपि व Fcitx</hi>
   <hr>Korean Fonts and fcitx</hr>
   <hu>Korean Fonts and fcitx</hu>
   <id>Korean Fonts and fcitx</id>
   <is>Korean Fonts and fcitx</is>
   <it>Font coreani e fcitx</it>
   <ja>韓国（朝鮮）語のフォントと fcitx</ja>
   <kk>Korean Fonts and fcitx</kk>
   <ko>Korean Fonts and fcitx</ko>
   <ku>Korean Fonts and fcitx</ku>
   <lt>Korėjietiški šriftai ir fcitx</lt>
   <mk>Korean Fonts and fcitx</mk>
   <mr>Korean Fonts and fcitx</mr>
   <nb_NO>Korean Fonts and fcitx</nb_NO>
   <nb>Koreanske skrifter og fcitx</nb>
   <nl_BE>Korean Fonts and fcitx</nl_BE>
   <nl>Koreaanse Fonts en fcitx</nl>
   <or>Korean Fonts and fcitx</or>
   <pl>Koreańskie fonty i fcitx</pl>
   <pt_BR>Pacotes de Fontes Coreano e fcitx</pt_BR>
   <pt>Coreano Fontes e fcitx</pt>
   <ro>Korean Fonts and fcitx</ro>
   <ru>Korean Fonts and fcitx</ru>
   <sk>Korean Fonts and fcitx</sk>
   <sl>Korejske pisave in fcitx</sl>
   <so>Korean Fonts and fcitx</so>
   <sq>Shkronja dhe fcitx për koreançen</sq>
   <sr>Korean Fonts and fcitx</sr>
   <sv>Koreanska Typsnitt och fcitx</sv>
   <th>ฟอนต์ภาษาเกาหลีและ fcitx</th>
   <tr>Korece Yazı Tipleri ve fcitx</tr>
   <uk>Korean Fonts and fcitx</uk>
   <vi>Korean Fonts and fcitx</vi>
   <zh_CN>Korean Fonts and fcitx</zh_CN>
   <zh_HK>Korean Fonts and fcitx</zh_HK>
   <zh_TW>Korean Fonts and fcitx</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
fcitx
fcitx-frontend-all
fcitx-frontend-fbterm
fcitx-tools
fcitx-ui-classic
fcitx-ui-light
fcitx-hangul
fonts-baekmuk
im-config
fcitx-config-gtk
fcitx-frontend-gtk2
fcitx-frontend-gtk3
fcitx-frontend-qt5
fonts-alee
mozc-utils-gui
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
fcitx
fcitx-config-gtk
fcitx-frontend-all
fcitx-frontend-fbterm
fcitx-tools
fcitx-ui-classic
fcitx-ui-light
fcitx-hangul
fonts-baekmuk
im-config
fcitx-frontend-gtk2
fcitx-frontend-gtk3
fcitx-frontend-qt5
fonts-alee
mozc-utils-gui
</uninstall_package_names>
</app>
