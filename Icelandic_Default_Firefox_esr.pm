<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Icelandic_Default_Firefox_esr
</name>

<description>
   <am>Icelandic localisation of Firefox ESR</am>
   <ar>Icelandic localisation of Firefox ESR</ar>
   <be>Icelandic localisation of Firefox ESR</be>
   <bg>Icelandic localisation of Firefox ESR</bg>
   <bn>Icelandic localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Islandès</ca>
   <cs>Icelandic localisation of Firefox ESR</cs>
   <da>Icelandic localisation of Firefox ESR</da>
   <de>Isländische Lokalisation von “Firefox ESR”</de>
   <el>Ισλανδικά για Firefox ESR</el>
   <en>Icelandic localisation of Firefox ESR</en>
   <es_ES>Localización Islandés de Firefox ESR</es_ES>
   <es>Localización Islandés de Firefox ESR</es>
   <et>Icelandic localisation of Firefox ESR</et>
   <eu>Icelandic localisation of Firefox ESR</eu>
   <fa>Icelandic localisation of Firefox ESR</fa>
   <fil_PH>Icelandic localisation of Firefox ESR</fil_PH>
   <fi>Icelandic localisation of Firefox ESR</fi>
   <fr_BE>Localisation en islandais pour Firefox ESR</fr_BE>
   <fr>Localisation en islandais pour Firefox ESR</fr>
   <gl_ES>Icelandic localisation of Firefox ESR</gl_ES>
   <gu>Icelandic localisation of Firefox ESR</gu>
   <he_IL>Icelandic localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का आइसलैंडिक संस्करण</hi>
   <hr>Icelandic localisation of Firefox ESR</hr>
   <hu>Icelandic localisation of Firefox ESR</hu>
   <id>Icelandic localisation of Firefox ESR</id>
   <is>Icelandic localisation of Firefox ESR</is>
   <it>Localizzazione islandese di Firefox ESR</it>
   <ja>アイスランド語版 Firefox ESR</ja>
   <kk>Icelandic localisation of Firefox ESR</kk>
   <ko>Icelandic localisation of Firefox ESR</ko>
   <ku>Icelandic localisation of Firefox ESR</ku>
   <lt>Icelandic localisation of Firefox ESR</lt>
   <mk>Icelandic localisation of Firefox ESR</mk>
   <mr>Icelandic localisation of Firefox ESR</mr>
   <nb_NO>Icelandic localisation of Firefox ESR</nb_NO>
   <nb>Islandsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Icelandic localisation of Firefox ESR</nl_BE>
   <nl>Icelandic localisation of Firefox ESR</nl>
   <or>Icelandic localisation of Firefox ESR</or>
   <pl>Icelandic localisation of Firefox ESR</pl>
   <pt_BR>Islandês Localização para o Firefox ESR</pt_BR>
   <pt>Islandês Localização para Firefox ESR</pt>
   <ro>Icelandic localisation of Firefox ESR</ro>
   <ru>Icelandic localisation of Firefox ESR</ru>
   <sk>Icelandic localisation of Firefox ESR</sk>
   <sl>Islandske krajevne nastavitve za Firefox ESR</sl>
   <so>Icelandic localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në islandisht</sq>
   <sr>Icelandic localisation of Firefox ESR</sr>
   <sv>Isländsk lokalisering av Firefox ESR</sv>
   <th>Icelandic localisation of Firefox ESR</th>
   <tr>Firefox ESR İzlandaca yerelleştirmesi</tr>
   <uk>Icelandic localisation of Firefox ESR</uk>
   <vi>Icelandic localisation of Firefox ESR</vi>
   <zh_CN>Icelandic localisation of Firefox ESR</zh_CN>
   <zh_HK>Icelandic localisation of Firefox ESR</zh_HK>
   <zh_TW>Icelandic localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-is
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-is
</uninstall_package_names>

</app>
