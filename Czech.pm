<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Czech
</name>

<description>
   <am>Czech dictionary for hunspell</am>
   <ar>Czech dictionary for hunspell</ar>
   <be>Czech dictionary for hunspell</be>
   <bg>Czech dictionary for hunspell</bg>
   <bn>Czech dictionary for hunspell</bn>
   <ca>Diccionari Txec per hunspell</ca>
   <cs>Czech dictionary for hunspell</cs>
   <da>Czech dictionary for hunspell</da>
   <de>Tschechisches Wörterbuch  für “Hunspell”</de>
   <el>Τσεχικό λεξικό για hunspell</el>
   <en>Czech dictionary for hunspell</en>
   <es_ES>Diccionario Checo para hunspell</es_ES>
   <es>Diccionario Checo para hunspell</es>
   <et>Czech dictionary for hunspell</et>
   <eu>Czech dictionary for hunspell</eu>
   <fa>Czech dictionary for hunspell</fa>
   <fil_PH>Czech dictionary for hunspell</fil_PH>
   <fi>Czech dictionary for hunspell</fi>
   <fr_BE>Tchèque dictionnaire pour hunspell</fr_BE>
   <fr>Tchèque dictionnaire pour hunspell</fr>
   <gl_ES>Czech dictionary for hunspell</gl_ES>
   <gu>Czech dictionary for hunspell</gu>
   <he_IL>Czech dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु चेक शब्दकोष</hi>
   <hr>Czech dictionary for hunspell</hr>
   <hu>Czech dictionary for hunspell</hu>
   <id>Czech dictionary for hunspell</id>
   <is>Czech dictionary for hunspell</is>
   <it>Dizionario di ceco per hunspell</it>
   <ja>Hunspell 用チェコ語辞書</ja>
   <kk>Czech dictionary for hunspell</kk>
   <ko>Czech dictionary for hunspell</ko>
   <ku>Czech dictionary for hunspell</ku>
   <lt>Czech dictionary for hunspell</lt>
   <mk>Czech dictionary for hunspell</mk>
   <mr>Czech dictionary for hunspell</mr>
   <nb_NO>Czech dictionary for hunspell</nb_NO>
   <nb>Tsjekkisk ordliste for hunspell</nb>
   <nl_BE>Czech dictionary for hunspell</nl_BE>
   <nl>Tsjechisch woordenboek voor hunspell</nl>
   <or>Czech dictionary for hunspell</or>
   <pl>Czech dictionary for hunspell</pl>
   <pt_BR>Dicionário Tcheco para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Checo para hunspell</pt>
   <ro>Czech dictionary for hunspell</ro>
   <ru>Czech dictionary for hunspell</ru>
   <sk>Czech dictionary for hunspell</sk>
   <sl>Češki slovar za hunspell</sl>
   <so>Czech dictionary for hunspell</so>
   <sq>Fjalor çekisht për hunspell</sq>
   <sr>Czech dictionary for hunspell</sr>
   <sv>Tjeckisk ordbok för hunspell</sv>
   <th>Czech dictionary for hunspell</th>
   <tr>Hunspell için Çekce sözlük</tr>
   <uk>Czech dictionary for hunspell</uk>
   <vi>Czech dictionary for hunspell</vi>
   <zh_CN>Czech dictionary for hunspell</zh_CN>
   <zh_HK>Czech dictionary for hunspell</zh_HK>
   <zh_TW>Czech dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-cs
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-cs
</uninstall_package_names>

</app>
