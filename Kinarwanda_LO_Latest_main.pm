<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Kinarwanda_LO_Latest_main
</name>

<description>
   <am>Kinarwanda Language Meta-Package for LibreOffice</am>
   <ar>Kinarwanda Language Meta-Package for LibreOffice</ar>
   <be>Kinarwanda Language Meta-Package for LibreOffice</be>
   <bg>Kinarwanda Language Meta-Package for LibreOffice</bg>
   <bn>Kinarwanda Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Kinarwanda per LibreOffice</ca>
   <cs>Kinarwanda Language Meta-Package for LibreOffice</cs>
   <da>Kinarwanda Language Meta-Package for LibreOffice</da>
   <de>Kinarwanda Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Kinarwanda</el>
   <en>Kinarwanda Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma kinarwanda para LibreOffice</es_ES>
   <es>Metapaquete de idioma Kinarwanda para LibreOffice</es>
   <et>Kinarwanda Language Meta-Package for LibreOffice</et>
   <eu>Kinarwanda Language Meta-Package for LibreOffice</eu>
   <fa>Kinarwanda Language Meta-Package for LibreOffice</fa>
   <fil_PH>Kinarwanda Language Meta-Package for LibreOffice</fil_PH>
   <fi>Ruandankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue kinarwanda pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue kinarwanda pour LibreOffice</fr>
   <gl_ES>Kinarwanda Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Kinarwanda Language Meta-Package for LibreOffice</gu>
   <he_IL>Kinarwanda Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु किन्यारवाण्डा भाषा मेटा-पैकेज</hi>
   <hr>Kinarwanda Language Meta-Package for LibreOffice</hr>
   <hu>Kinarwanda Language Meta-Package for LibreOffice</hu>
   <id>Kinarwanda Language Meta-Package for LibreOffice</id>
   <is>Kinarwanda Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua kinyarwanda per LibreOffice</it>
   <ja>LibreOffice 用ルワンダ言語メタパッケージ</ja>
   <kk>Kinarwanda Language Meta-Package for LibreOffice</kk>
   <ko>Kinarwanda Language Meta-Package for LibreOffice</ko>
   <ku>Kinarwanda Language Meta-Package for LibreOffice</ku>
   <lt>Kinarwanda Language Meta-Package for LibreOffice</lt>
   <mk>Kinarwanda Language Meta-Package for LibreOffice</mk>
   <mr>Kinarwanda Language Meta-Package for LibreOffice</mr>
   <nb_NO>Kinarwanda Language Meta-Package for LibreOffice</nb_NO>
   <nb>Kinarwanda språkpakke for LibreOffice</nb>
   <nl_BE>Kinarwanda Language Meta-Package for LibreOffice</nl_BE>
   <nl>Kinarwanda Taal Meta-Pakket voor LibreOffice</nl>
   <or>Kinarwanda Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Kinarwanda dla LibreOffice</pl>
   <pt_BR>Quiniaruanda Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Kinarwanda Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Kinarwanda Language Meta-Package for LibreOffice</ro>
   <ru>Kinarwanda Language Meta-Package for LibreOffice</ru>
   <sk>Kinarwanda Language Meta-Package for LibreOffice</sk>
   <sl>Kinarvandski jezikovni meta-paket za LibreOffice</sl>
   <so>Kinarwanda Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në kinaruandisht</sq>
   <sr>Kinarwanda Language Meta-Package for LibreOffice</sr>
   <sv>Kinarwanda Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Kinarwanda สำหรับ LibreOffice</th>
   <tr>LibreOffice için Kinarwanda Dili Üst-Paketi</tr>
   <uk>Kinarwanda Language Meta-Package for LibreOffice</uk>
   <vi>Kinarwanda Language Meta-Package for LibreOffice</vi>
   <zh_CN>Kinarwanda Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Kinarwanda Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Kinarwanda Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-rw
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-rw
libreoffice-gtk3
</uninstall_package_names>

</app>
