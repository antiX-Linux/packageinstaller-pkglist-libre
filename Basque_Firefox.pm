<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Basque_Firefox
</name>

<description>
   <am>Basque localisation of Firefox</am>
   <ar>Basque localisation of Firefox</ar>
   <be>Basque localisation of Firefox</be>
   <bg>Basque localisation of Firefox</bg>
   <bn>Basque localisation of Firefox</bn>
   <ca>Localització de Firefox en Euskara</ca>
   <cs>Basque localisation of Firefox</cs>
   <da>Baskisk oversættelse af Firefox</da>
   <de>Baskische Lokalisierung von Firefox</de>
   <el>Βασκικά για το Firefox</el>
   <en>Basque localisation of Firefox</en>
   <es_ES>Localización Eusquera de Firefox</es_ES>
   <es>Localización Vasco de Firefox</es>
   <et>Basque localisation of Firefox</et>
   <eu>Basque localisation of Firefox</eu>
   <fa>Basque localisation of Firefox</fa>
   <fil_PH>Basque localisation of Firefox</fil_PH>
   <fi>Baskilainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en basque pour Firefox</fr_BE>
   <fr>Localisation en basque pour Firefox</fr>
   <gl_ES>Localización de Firefox en Basco</gl_ES>
   <gu>Basque localisation of Firefox</gu>
   <he_IL>Basque localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का बास्क संस्करण</hi>
   <hr>Baskijska lokalizacija Firefoxa</hr>
   <hu>Basque localisation of Firefox</hu>
   <id>Basque localisation of Firefox</id>
   <is>Basque localisation of Firefox</is>
   <it>Localizzazione basca di Firefox</it>
   <ja>バスク語版 Firefox</ja>
   <kk>Basque localisation of Firefox</kk>
   <ko>Basque localisation of Firefox</ko>
   <ku>Basque localisation of Firefox</ku>
   <lt>Basque localisation of Firefox</lt>
   <mk>Basque localisation of Firefox</mk>
   <mr>Basque localisation of Firefox</mr>
   <nb_NO>Basque localisation of Firefox</nb_NO>
   <nb>Baskisk lokaltilpassing av Firefox</nb>
   <nl_BE>Basque localisation of Firefox</nl_BE>
   <nl>Baskische lokalisatie van Firefox</nl>
   <or>Basque localisation of Firefox</or>
   <pl>Baskijska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Basco Localização para o Firefox</pt_BR>
   <pt>Basco Localização para Firefox</pt>
   <ro>Basque localisation of Firefox</ro>
   <ru>Баскская локализация Firefox</ru>
   <sk>Basque lokalizácia pre Firefox</sk>
   <sl>Basque localisation of Firefox</sl>
   <so>Basque localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në baskisht</sq>
   <sr>Basque localisation of Firefox</sr>
   <sv>Baskisk lokalisering av Firefox</sv>
   <th>Basque localisation ของ Firefox</th>
   <tr>Firefox'un Bask dili yerelleştirmesi</tr>
   <uk>Basque локалізація Firefox</uk>
   <vi>Basque localisation of Firefox</vi>
   <zh_CN>Firefox 巴斯克语语言包</zh_CN>
   <zh_HK>Basque localisation of Firefox</zh_HK>
   <zh_TW>Basque localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-eu
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-eu
</uninstall_package_names>
</app>
