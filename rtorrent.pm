<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Torrent
</category>

<name>
rTorrent
</name>

<description>
   <am>an ncurses BitTorrent client</am>
   <ar>an ncurses BitTorrent client</ar>
   <be>an ncurses BitTorrent client</be>
   <bg>an ncurses BitTorrent client</bg>
   <bn>an ncurses BitTorrent client</bn>
   <ca>Un client de BitTorrent en ncurses</ca>
   <cs>an ncurses BitTorrent client</cs>
   <da>en ncurses BitTorrent-klient</da>
   <de>Ein mit ncurses realisierter BitTorrent-Client</de>
   <el>Προγραμματισμός σε ncurses του BitTorrent</el>
   <en>an ncurses BitTorrent client</en>
   <es_ES>Un cliente ncurses BitTorrent</es_ES>
   <es>Un cliente ncurses BitTorrent</es>
   <et>an ncurses BitTorrent client</et>
   <eu>an ncurses BitTorrent client</eu>
   <fa>an ncurses BitTorrent client</fa>
   <fil_PH>an ncurses BitTorrent client</fil_PH>
   <fi>ncurses-tyyppinen BitTorrent-asiakasohjelma</fi>
   <fr_BE>Client BitTorrent ncurses</fr_BE>
   <fr>Client BitTorrent ncurses</fr>
   <gl_ES>Cliente de BitTorrent baseado en ncurses</gl_ES>
   <gu>an ncurses BitTorrent client</gu>
   <he_IL>an ncurses BitTorrent client</he_IL>
   <hi>ncurses बिट-टोरेंट साधन</hi>
   <hr>an ncurses BitTorrent client</hr>
   <hu>an ncurses BitTorrent client</hu>
   <id>an ncurses BitTorrent client</id>
   <is>an ncurses BitTorrent client</is>
   <it>client BitTorrent che usa ncurses</it>
   <ja>ncurse BitTorrent クライアント</ja>
   <kk>an ncurses BitTorrent client</kk>
   <ko>an ncurses BitTorrent client</ko>
   <ku>an ncurses BitTorrent client</ku>
   <lt>an ncurses BitTorrent client</lt>
   <mk>an ncurses BitTorrent client</mk>
   <mr>an ncurses BitTorrent client</mr>
   <nb_NO>an ncurses BitTorrent client</nb_NO>
   <nb>BitTorrent-klient, ncurses</nb>
   <nl_BE>an ncurses BitTorrent client</nl_BE>
   <nl>een ncurses BitTorrent programma</nl>
   <or>an ncurses BitTorrent client</or>
   <pl>klient ncurses BitTorrent</pl>
   <pt_BR>Cliente de BitTorrent baseado em ncurses</pt_BR>
   <pt>Cliente de BitTorrent baseado em ncurses</pt>
   <ro>an ncurses BitTorrent client</ro>
   <ru>Клиент BitTorrent с интерфейсом на ncurses</ru>
   <sk>an ncurses BitTorrent client</sk>
   <sl>Ncurses BitTorrent odjemalec</sl>
   <so>an ncurses BitTorrent client</so>
   <sq>Një klient BitTorrent shkruar në ncurses</sq>
   <sr>an ncurses BitTorrent client</sr>
   <sv>en ncurses BitTorrent klient</sv>
   <th>an ncurses BitTorrent client</th>
   <tr>bir ncurses BitTorrent istemcisi</tr>
   <uk>консольний BitTorrent клієнт</uk>
   <vi>an ncurses BitTorrent client</vi>
   <zh_CN>an ncurses BitTorrent client</zh_CN>
   <zh_HK>an ncurses BitTorrent client</zh_HK>
   <zh_TW>an ncurses BitTorrent client</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
rtorrent
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
rtorrent
</uninstall_package_names>
</app>
