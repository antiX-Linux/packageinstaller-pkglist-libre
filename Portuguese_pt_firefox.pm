<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Portugese(PT) Firefox
</name>

<description>
   <am>Portuguese(PT) localisation of Firefox</am>
   <ar>Portuguese(PT) localisation of Firefox</ar>
   <be>Portuguese(PT) localisation of Firefox</be>
   <bg>Portuguese(PT) localisation of Firefox</bg>
   <bn>Portuguese(PT) localisation of Firefox</bn>
   <ca>Localització de Firefox en Portuguès (PT)</ca>
   <cs>Portuguese(PT) localisation of Firefox</cs>
   <da>Portuguese(PT) localisation of Firefox</da>
   <de>Portugiesische (PT) Lokalisation von “Firefox”</de>
   <el>Πορτογαλικά (PT) εντοπισμός του Firefox</el>
   <en>Portuguese(PT) localisation of Firefox</en>
   <es_ES>Localización Portugués (PT) de Firefox</es_ES>
   <es>Localización Portugués (PT) de Firefox</es>
   <et>Portuguese(PT) localisation of Firefox</et>
   <eu>Portuguese(PT) localisation of Firefox</eu>
   <fa>Portuguese(PT) localisation of Firefox</fa>
   <fil_PH>Portuguese(PT) localisation of Firefox</fil_PH>
   <fi>Portuguese(PT) localisation of Firefox</fi>
   <fr_BE>Localisation en portugais pour Firefox</fr_BE>
   <fr>Localisation en portugais pour Firefox</fr>
   <gl_ES>Portuguese(PT) localisation of Firefox</gl_ES>
   <gu>Portuguese(PT) localisation of Firefox</gu>
   <he_IL>Portuguese(PT) localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का पुर्तगाली (पुर्तगाल) संस्करण</hi>
   <hr>Portuguese(PT) localisation of Firefox</hr>
   <hu>Portuguese(PT) localisation of Firefox</hu>
   <id>Portuguese(PT) localisation of Firefox</id>
   <is>Portuguese(PT) localisation of Firefox</is>
   <it>Localizzazione portoghese(PT) di Firefox</it>
   <ja>ポルトガル語（ポルトガル）版 Firefox</ja>
   <kk>Portuguese(PT) localisation of Firefox</kk>
   <ko>Portuguese(PT) localisation of Firefox</ko>
   <ku>Portuguese(PT) localisation of Firefox</ku>
   <lt>Portuguese(PT) localisation of Firefox</lt>
   <mk>Portuguese(PT) localisation of Firefox</mk>
   <mr>Portuguese(PT) localisation of Firefox</mr>
   <nb_NO>Portuguese(PT) localisation of Firefox</nb_NO>
   <nb>Portugisisk lokaltilpassing av Firefox</nb>
   <nl_BE>Portuguese(PT) localisation of Firefox</nl_BE>
   <nl>Portuguese(PT) localisation of Firefox</nl>
   <or>Portuguese(PT) localisation of Firefox</or>
   <pl>Portuguese(PT) localisation of Firefox</pl>
   <pt_BR>Português (PT) Localização para o Firefox</pt_BR>
   <pt>Português (PT) Localização para Firefox</pt>
   <ro>Portuguese(PT) localisation of Firefox</ro>
   <ru>Portuguese(PT) localisation of Firefox</ru>
   <sk>Portuguese(PT) localisation of Firefox</sk>
   <sl>Portugalske (PT) krajevne nastavitve za Firefox</sl>
   <so>Portuguese(PT) localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në portugalisht (PT)</sq>
   <sr>Portuguese(PT) localisation of Firefox</sr>
   <sv>Portugisisk(PT) lokalisering för Firefox</sv>
   <th>Portuguese(PT) localisation of Firefox</th>
   <tr>Firefox'un Portekiz Portekizcesi yerelleştirmesi</tr>
   <uk>Portuguese(PT) localisation of Firefox</uk>
   <vi>Portuguese(PT) localisation of Firefox</vi>
   <zh_CN>Portuguese(PT) localisation of Firefox</zh_CN>
   <zh_HK>Portuguese(PT) localisation of Firefox</zh_HK>
   <zh_TW>Portuguese(PT) localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-pt-pt
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-pt-pt
</uninstall_package_names>
</app>
