<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
Rclone
</name>

<description>
   <en>rsync for commercial cloud storage</en>
</description>

<installable>
all
</installable>

<screenshot></screenshot>

<preinstall>

</preinstall>

<install_package_names>
rclone
rclone-browser
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
rclone
rclone-browser
</uninstall_package_names>

</app>
