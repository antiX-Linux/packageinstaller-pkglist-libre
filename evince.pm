<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Office
</category>

<name>
Evince
</name>

<description>
   <am>a simple document (PostScript, PDF) viewer</am>
   <ar>a simple document (PostScript, PDF) viewer</ar>
   <be>a simple document (PostScript, PDF) viewer</be>
   <bg>a simple document (PostScript, PDF) viewer</bg>
   <bn>a simple document (PostScript, PDF) viewer</bn>
   <ca>senzill visor de documents (PDF, Postscript)</ca>
   <cs>a simple document (PostScript, PDF) viewer</cs>
   <da>en simpel dokumentfremviser (PostScript, PDF)</da>
   <de>Ein einfacher Dokumentenbetrachter (PostScript, PDF)</de>
   <el>Απλό πρόγραμμα προβολής (PostScript, PDF)</el>
   <en>a simple document (PostScript, PDF) viewer</en>
   <es_ES>Visor de documentos (PostScript, PDF) sencillo</es_ES>
   <es>Visor de documentos (PostScript, PDF) sencillo</es>
   <et>a simple document (PostScript, PDF) viewer</et>
   <eu>a simple document (PostScript, PDF) viewer</eu>
   <fa>a simple document (PostScript, PDF) viewer</fa>
   <fil_PH>a simple document (PostScript, PDF) viewer</fil_PH>
   <fi>yksinkertainen asiakirjakatselin (PostScript, PDF)</fi>
   <fr_BE>Visionneuse simple de documents (PostScript, PDF)</fr_BE>
   <fr>Visionneuse simple de documents (PostScript, PDF)</fr>
   <gl_ES>visualizador de documentos (PostScript, PDF) simple</gl_ES>
   <gu>a simple document (PostScript, PDF) viewer</gu>
   <he_IL>a simple document (PostScript, PDF) viewer</he_IL>
   <hi>सरल प्रलेख (पोस्ट स्क्रिप्ट, पीडीएफ)प्रदर्शक</hi>
   <hr>a simple document (PostScript, PDF) viewer</hr>
   <hu>a simple document (PostScript, PDF) viewer</hu>
   <id>a simple document (PostScript, PDF) viewer</id>
   <is>a simple document (PostScript, PDF) viewer</is>
   <it>Un semplice visualizzatore di documenti (PostScript, PDF)</it>
   <ja>シンプルな文書(PostScript, PDF)ビューア</ja>
   <kk>a simple document (PostScript, PDF) viewer</kk>
   <ko>a simple document (PostScript, PDF) viewer</ko>
   <ku>a simple document (PostScript, PDF) viewer</ku>
   <lt>paprasta dokumentų (PostScript, PDF) žiūryklė</lt>
   <mk>a simple document (PostScript, PDF) viewer</mk>
   <mr>a simple document (PostScript, PDF) viewer</mr>
   <nb_NO>a simple document (PostScript, PDF) viewer</nb_NO>
   <nb>en enkel dokumentviser (PostScript, PDF)</nb>
   <nl_BE>a simple document (PostScript, PDF) viewer</nl_BE>
   <nl>een eenvoudige document (PostScript, PDF) lezer</nl>
   <or>a simple document (PostScript, PDF) viewer</or>
   <pl>prosta przeglądarka dokumentów (PostScript, PDF)</pl>
   <pt_BR>Visualizador de documentos (PostScript, PDF) simples e de fácil utilização</pt_BR>
   <pt>Visualizador de documentos (PostScript, PDF) simples e de fácil utilização</pt>
   <ro>a simple document (PostScript, PDF) viewer</ro>
   <ru>Просмотрщик документов PostScript, PDF</ru>
   <sk>Zobrazovač jednoduchých dokumentov (PostScript, PDF)</sk>
   <sl>preprost pregledovalnik dokumentov (PostScript, PDF)</sl>
   <so>a simple document (PostScript, PDF) viewer</so>
   <sq>Një parës i thjeshtë dokumentash (PostScript, PDF)</sq>
   <sr>a simple document (PostScript, PDF) viewer</sr>
   <sv>en enkel dokument (PostScript, PDF) läsare</sv>
   <th>โปรแกรมเปิดเอกสาร (PostScript, PDF) อย่างง่าย</th>
   <tr>basit bir belge (PostScript, PDF) görüntüleyici</tr>
   <uk>простий переглядач документів (PostScript, PDF)</uk>
   <vi>a simple document (PostScript, PDF) viewer</vi>
   <zh_CN>简单的文档（PostScript，PDF）查看器</zh_CN>
   <zh_HK>a simple document (PostScript, PDF) viewer</zh_HK>
   <zh_TW>a simple document (PostScript, PDF) viewer</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
evince
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
evince
</uninstall_package_names>
</app>
