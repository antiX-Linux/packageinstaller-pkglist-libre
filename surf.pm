<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Surf
</name>

<description>
   <am>Simple web browser by suckless community</am>
   <ar>Simple web browser by suckless community</ar>
   <be>Simple web browser by suckless community</be>
   <bg>Simple web browser by suckless community</bg>
   <bn>Simple web browser by suckless community</bn>
   <ca>Navegador web senzill fet per una comunitat trempada</ca>
   <cs>Simple web browser by suckless community</cs>
   <da>Simple web browser by suckless community</da>
   <de>Minimalistischer Internetbrowser, entwickelt von der Programmierergemeinschaft “suckless.org”</de>
   <el>Απλό πρόγραμμα περιήγησης ιστού από την κοινότητα suckless</el>
   <en>Simple web browser by suckless community</en>
   <es_ES>Navegador web simple de la comunidad menos malo (suckless)</es_ES>
   <es>Navegador web simple de la comunidad menos malo (suckless)</es>
   <et>Simple web browser by suckless community</et>
   <eu>Simple web browser by suckless community</eu>
   <fa>Simple web browser by suckless community</fa>
   <fil_PH>Simple web browser by suckless community</fil_PH>
   <fi>Yksinkertainen verkkoselain suckless-yhteisöltä</fi>
   <fr_BE>Navigateur Internet simple de la communauté de programmeurs “suckless.org”</fr_BE>
   <fr>Navigateur Internet simple de la communauté de programmeurs “suckless.org”</fr>
   <gl_ES>Simple web browser by suckless community</gl_ES>
   <gu>Simple web browser by suckless community</gu>
   <he_IL>Simple web browser by suckless community</he_IL>
   <hi>समुदाय द्वारा निर्मित सरल वेब ब्राउज़र</hi>
   <hr>Simple web browser by suckless community</hr>
   <hu>Simple web browser by suckless community</hu>
   <id>Simple web browser by suckless community</id>
   <is>Simple web browser by suckless community</is>
   <it>Web browser semplice dalla suckless community</it>
   <ja>suckless.org が開発したシンプルなブラウザ</ja>
   <kk>Simple web browser by suckless community</kk>
   <ko>Simple web browser by suckless community</ko>
   <ku>Simple web browser by suckless community</ku>
   <lt>Simple web browser by suckless community</lt>
   <mk>Simple web browser by suckless community</mk>
   <mr>Simple web browser by suckless community</mr>
   <nb_NO>Simple web browser by suckless community</nb_NO>
   <nb>Enkel nettleser skrevet av suckless-miljøet</nb>
   <nl_BE>Simple web browser by suckless community</nl_BE>
   <nl>Simple web browser by suckless community</nl>
   <or>Simple web browser by suckless community</or>
   <pl>Simple web browser by suckless community</pl>
   <pt_BR>Navegador de internet simples da comunidade 'suckless'</pt_BR>
   <pt>Navegador Web simples, da comunidade suckless</pt>
   <ro>Simple web browser by suckless community</ro>
   <ru>Simple web browser by suckless community</ru>
   <sk>Simple web browser by suckless community</sk>
   <sl>Preprost brskalnik suckless skupnosti</sl>
   <so>Simple web browser by suckless community</so>
   <sq>Shfletues i thjeshtë nga bashkësia suckless</sq>
   <sr>Simple web browser by suckless community</sr>
   <sv>Enkel webläsare av suckless community</sv>
   <th>Simple web browser by suckless community</th>
   <tr>Suckless topluluğundan basit web tarayıcısı</tr>
   <uk>Simple web browser by suckless community</uk>
   <vi>Simple web browser by suckless community</vi>
   <zh_CN>Simple web browser by suckless community</zh_CN>
   <zh_HK>Simple web browser by suckless community</zh_HK>
   <zh_TW>Simple web browser by suckless community</zh_TW>
</description>

<installable>
all
</installable>

<preinstall>

</preinstall>

<install_package_names>
surf
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
surf
</uninstall_package_names>
</app>
