<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Serbian_Default_Firefox_esr
</name>

<description>
   <am>Serbian localisation of Firefox ESR</am>
   <ar>Serbian localisation of Firefox ESR</ar>
   <be>Serbian localisation of Firefox ESR</be>
   <bg>Serbian localisation of Firefox ESR</bg>
   <bn>Serbian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Serbi</ca>
   <cs>Serbian localisation of Firefox ESR</cs>
   <da>Serbian localisation of Firefox ESR</da>
   <de>Serbische Lokalisation von “Firefox ESR”</de>
   <el>Σερβικά για Firefox ESR</el>
   <en>Serbian localisation of Firefox ESR</en>
   <es_ES>Localización Serbio de Firefox ESR</es_ES>
   <es>Localización Serbio de Firefox ESR</es>
   <et>Serbian localisation of Firefox ESR</et>
   <eu>Serbian localisation of Firefox ESR</eu>
   <fa>Serbian localisation of Firefox ESR</fa>
   <fil_PH>Serbian localisation of Firefox ESR</fil_PH>
   <fi>Serbian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en serbe pour Firefox ESR</fr_BE>
   <fr>Localisation en serbe pour Firefox ESR</fr>
   <gl_ES>Serbian localisation of Firefox ESR</gl_ES>
   <gu>Serbian localisation of Firefox ESR</gu>
   <he_IL>Serbian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का सर्बियाई संस्करण</hi>
   <hr>Serbian localisation of Firefox ESR</hr>
   <hu>Serbian localisation of Firefox ESR</hu>
   <id>Serbian localisation of Firefox ESR</id>
   <is>Serbian localisation of Firefox ESR</is>
   <it>Localizzazione serbian di Firefox ESR</it>
   <ja>セルビア語版 Firefox ESR</ja>
   <kk>Serbian localisation of Firefox ESR</kk>
   <ko>Serbian localisation of Firefox ESR</ko>
   <ku>Serbian localisation of Firefox ESR</ku>
   <lt>Serbian localisation of Firefox ESR</lt>
   <mk>Serbian localisation of Firefox ESR</mk>
   <mr>Serbian localisation of Firefox ESR</mr>
   <nb_NO>Serbian localisation of Firefox ESR</nb_NO>
   <nb>Serbisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Serbian localisation of Firefox ESR</nl_BE>
   <nl>Serbian localisation of Firefox ESR</nl>
   <or>Serbian localisation of Firefox ESR</or>
   <pl>Serbian localisation of Firefox ESR</pl>
   <pt_BR>Sérvio Localização para o Firefox ESR</pt_BR>
   <pt>Sérvio Localização para Firefox ESR</pt>
   <ro>Serbian localisation of Firefox ESR</ro>
   <ru>Serbian localisation of Firefox ESR</ru>
   <sk>Serbian localisation of Firefox ESR</sk>
   <sl>Srbske krajevne nastavitve za Firefox ESR</sl>
   <so>Serbian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në serbisht</sq>
   <sr>Serbian localisation of Firefox ESR</sr>
   <sv>Serbisk lokalisering av Firefox ESR</sv>
   <th>Serbian localisation of Firefox ESR</th>
   <tr>Firefox ESR Sırpça yerelleştirmesi</tr>
   <uk>Serbian localisation of Firefox ESR</uk>
   <vi>Serbian localisation of Firefox ESR</vi>
   <zh_CN>Serbian localisation of Firefox ESR</zh_CN>
   <zh_HK>Serbian localisation of Firefox ESR</zh_HK>
   <zh_TW>Serbian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-sr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-sr
</uninstall_package_names>

</app>
