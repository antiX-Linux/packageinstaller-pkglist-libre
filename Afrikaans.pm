<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Afrikaans
</name>

<description>
   <am>Afrikaans dictionary for hunspell</am>
   <ar>Afrikaans dictionary for hunspell</ar>
   <be>Afrikaans dictionary for hunspell</be>
   <bg>Afrikaans dictionary for hunspell</bg>
   <bn>Afrikaans dictionary for hunspell</bn>
   <ca>Diccionari Afrikaans per hunspell</ca>
   <cs>Afrikaans dictionary for hunspell</cs>
   <da>Afrikaans dictionary for hunspell</da>
   <de>Wörterbuch für die südafrikanische Sprache “Kapholländisch” für “Hunspell”</de>
   <el>Λεξικό Αφρικανικά για hunspell</el>
   <en>Afrikaans dictionary for hunspell</en>
   <es_ES>Diccionario africano para hunspell</es_ES>
   <es>Diccionario Africano para hunspell</es>
   <et>Afrikaans dictionary for hunspell</et>
   <eu>Afrikaans dictionary for hunspell</eu>
   <fa>Afrikaans dictionary for hunspell</fa>
   <fil_PH>Afrikaans dictionary for hunspell</fil_PH>
   <fi>Afrikaansin sanakirja hunspell:iä varten</fi>
   <fr_BE>Afrikaans dictionnaire pour hunspell</fr_BE>
   <fr>Afrikaans dictionnaire pour hunspell</fr>
   <gl_ES>Afrikaans dictionary for hunspell</gl_ES>
   <gu>Afrikaans dictionary for hunspell</gu>
   <he_IL>Afrikaans dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु अफ्रीकी डच शब्दकोष</hi>
   <hr>Afrikaans dictionary for hunspell</hr>
   <hu>Afrikaans dictionary for hunspell</hu>
   <id>Afrikaans dictionary for hunspell</id>
   <is>Afrikaans dictionary for hunspell</is>
   <it>Dizionario afrikaans per hunspell</it>
   <ja>Hunspell 用のアフリカーンス語辞書</ja>
   <kk>Afrikaans dictionary for hunspell</kk>
   <ko>Afrikaans dictionary for hunspell</ko>
   <ku>Afrikaans dictionary for hunspell</ku>
   <lt>Afrikaans dictionary for hunspell</lt>
   <mk>Afrikaans dictionary for hunspell</mk>
   <mr>Afrikaans dictionary for hunspell</mr>
   <nb_NO>Afrikaans dictionary for hunspell</nb_NO>
   <nb>Afrikaans ordliste for hunspell</nb>
   <nl_BE>Afrikaans dictionary for hunspell</nl_BE>
   <nl>Afrikaans woordenboek voor hunspell</nl>
   <or>Afrikaans dictionary for hunspell</or>
   <pl>Afrikaans dictionary for hunspell</pl>
   <pt_BR>Dicionário Africâner para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Afrikaans para hunspell</pt>
   <ro>Afrikaans dictionary for hunspell</ro>
   <ru>Afrikaans dictionary for hunspell</ru>
   <sk>Afrikaans dictionary for hunspell</sk>
   <sl>Afrikaan slovar za hunspell</sl>
   <so>Afrikaans dictionary for hunspell</so>
   <sq>Fjalor në afrikaans për hunspell</sq>
   <sr>Afrikaans dictionary for hunspell</sr>
   <sv>Afrikaans ordbok för hunspell</sv>
   <th>Afrikaans dictionary for hunspell</th>
   <tr>Hunspell için Afrikanca sözlüğü</tr>
   <uk>Afrikaans dictionary for hunspell</uk>
   <vi>Afrikaans dictionary for hunspell</vi>
   <zh_CN>Afrikaans dictionary for hunspell</zh_CN>
   <zh_HK>Afrikaans dictionary for hunspell</zh_HK>
   <zh_TW>Afrikaans dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-af
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-af
</uninstall_package_names>

</app>
