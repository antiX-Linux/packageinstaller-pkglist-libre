<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Japanese_Input
</name>

<description>
   <am>Japanese ibus</am>
   <ar>Japanese ibus</ar>
   <be>Japanese ibus</be>
   <bg>Japanese ibus</bg>
   <bn>Japanese ibus</bn>
   <ca>Ibus en japonès</ca>
   <cs>Japanese ibus</cs>
   <da>Japansk ibus</da>
   <de>Japanischer IBus</de>
   <el>Ιαπωνικά ibus</el>
   <en>Japanese ibus</en>
   <es_ES>Ibus Japonés</es_ES>
   <es>Ibus Japonés</es>
   <et>Japanese ibus</et>
   <eu>Japanese ibus</eu>
   <fa>Japanese ibus</fa>
   <fil_PH>Japanese ibus</fil_PH>
   <fi>Japanilainen ibus</fi>
   <fr_BE>Japonais ibus</fr_BE>
   <fr>Japonais ibus</fr>
   <gl_ES>Xaponés ibus</gl_ES>
   <gu>Japanese ibus</gu>
   <he_IL>Japanese ibus</he_IL>
   <hi>जापानी Ibus</hi>
   <hr>Japanese ibus</hr>
   <hu>Japanese ibus</hu>
   <id>Japanese ibus</id>
   <is>Japanese ibus</is>
   <it>Ibus per la lingua giapponese</it>
   <ja>日本語 ibus 入力</ja>
   <kk>Japanese ibus</kk>
   <ko>Japanese ibus</ko>
   <ku>Japanese ibus</ku>
   <lt>Japonų ibus</lt>
   <mk>Japanese ibus</mk>
   <mr>Japanese ibus</mr>
   <nb_NO>Japanese ibus</nb_NO>
   <nb>Japansk ibus</nb>
   <nl_BE>Japanese ibus</nl_BE>
   <nl>Japanse ibus</nl>
   <or>Japanese ibus</or>
   <pl>Japoński ibus</pl>
   <pt_BR>ibus para o idioma Japonês</pt_BR>
   <pt>Japonês ibus</pt>
   <ro>Japanese ibus</ro>
   <ru>Japanese ibus</ru>
   <sk>Japanese ibus</sk>
   <sl>Japonski ibus</sl>
   <so>Japanese ibus</so>
   <sq>ibus për japonishten</sq>
   <sr>Japanese ibus</sr>
   <sv>Japansk ibus</sv>
   <th>ibus ภาษาญี่ปุ่น</th>
   <tr>Japonca ibus</tr>
   <uk>Japanese ibus</uk>
   <vi>Japanese ibus</vi>
   <zh_CN>Japanese ibus</zh_CN>
   <zh_HK>Japanese ibus</zh_HK>
   <zh_TW>Japanese ibus</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
im-config
ibus-anthy
ibus-mozc
ibus-gtk3
ibus-gtk
fonts-vlgothic
manpages-ja
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
im-config
ibus-anthy
ibus-mozc
ibus-gtk3
ibus-gtk
fonts-vlgothic
manpages-ja
</uninstall_package_names>

</app>
