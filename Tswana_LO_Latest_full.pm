<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Tswana_LO_Latest_full
</name>

<description>
   <am>Tswana Language Meta-Package for LibreOffice</am>
   <ar>Tswana Language Meta-Package for LibreOffice</ar>
   <be>Tswana Language Meta-Package for LibreOffice</be>
   <bg>Tswana Language Meta-Package for LibreOffice</bg>
   <bn>Tswana Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Tswana per LibreOffice</ca>
   <cs>Tswana Language Meta-Package for LibreOffice</cs>
   <da>Tswana Language Meta-Package for LibreOffice</da>
   <de>Tswana Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Tswana</el>
   <en>Tswana Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Idioma Tswana para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Tswana para LibreOffice</es>
   <et>Tswana Language Meta-Package for LibreOffice</et>
   <eu>Tswana Language Meta-Package for LibreOffice</eu>
   <fa>Tswana Language Meta-Package for LibreOffice</fa>
   <fil_PH>Tswana Language Meta-Package for LibreOffice</fil_PH>
   <fi>Tswanankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue tswana pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue tswana pour LibreOffice</fr>
   <gl_ES>Tswana Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Tswana Language Meta-Package for LibreOffice</gu>
   <he_IL>Tswana Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु सेत्स्वाना भाषा मेटा-पैकेज</hi>
   <hr>Tswana Language Meta-Package for LibreOffice</hr>
   <hu>Tswana Language Meta-Package for LibreOffice</hu>
   <id>Tswana Language Meta-Package for LibreOffice</id>
   <is>Tswana Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua tswana per LibreOffice</it>
   <ja>LibreOffice用のツワナ語メタパッケージ</ja>
   <kk>Tswana Language Meta-Package for LibreOffice</kk>
   <ko>Tswana Language Meta-Package for LibreOffice</ko>
   <ku>Tswana Language Meta-Package for LibreOffice</ku>
   <lt>Tswana Language Meta-Package for LibreOffice</lt>
   <mk>Tswana Language Meta-Package for LibreOffice</mk>
   <mr>Tswana Language Meta-Package for LibreOffice</mr>
   <nb_NO>Tswana Language Meta-Package for LibreOffice</nb_NO>
   <nb>Tswana språkpakke for LibreOffice</nb>
   <nl_BE>Tswana Language Meta-Package for LibreOffice</nl_BE>
   <nl>Tswana Taal Meta-Pakket voor LibreOffice</nl>
   <or>Tswana Language Meta-Package for LibreOffice</or>
   <pl>Tswana meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Tswana Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Tswana Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Tswana Language Meta-Package for LibreOffice</ro>
   <ru>Tswana Language Meta-Package for LibreOffice</ru>
   <sk>Tswana Language Meta-Package for LibreOffice</sk>
   <sl>Tswanski jezikovni meta-paket za LibreOffice</sl>
   <so>Tswana Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në cuana</sq>
   <sr>Tswana Language Meta-Package for LibreOffice</sr>
   <sv>Tswana Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Tswana สำหรับ LibreOffice</th>
   <tr>LibreOffice için Tswana Dili Üst-Paketi</tr>
   <uk>Tswana Language Meta-Package for LibreOffice</uk>
   <vi>Tswana Language Meta-Package for LibreOffice</vi>
   <zh_CN>Tswana Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Tswana Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Tswana Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-tn
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-tn
</uninstall_package_names>

</app>
