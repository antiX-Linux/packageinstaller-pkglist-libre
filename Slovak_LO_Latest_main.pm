<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Slovak_LO_Latest_main
</name>

<description>
   <am>Slovak Language Meta-Package for LibreOffice</am>
   <ar>Slovak Language Meta-Package for LibreOffice</ar>
   <be>Slovak Language Meta-Package for LibreOffice</be>
   <bg>Slovak Language Meta-Package for LibreOffice</bg>
   <bn>Slovak Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Eslovac per LibreOffice</ca>
   <cs>Slovak Language Meta-Package for LibreOffice</cs>
   <da>Slovak Language Meta-Package for LibreOffice</da>
   <de>Slowakisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Σλοβακικά</el>
   <en>Slovak Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Eslovaco para LibreOffice</es_ES>
   <es>Metapaquete de idioma Eslovaco para LibreOffice</es>
   <et>Slovak Language Meta-Package for LibreOffice</et>
   <eu>Slovak Language Meta-Package for LibreOffice</eu>
   <fa>Slovak Language Meta-Package for LibreOffice</fa>
   <fil_PH>Slovak Language Meta-Package for LibreOffice</fil_PH>
   <fi>Slovakialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue slovaque pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue slovaque pour LibreOffice</fr>
   <gl_ES>Eslovaco Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Slovak Language Meta-Package for LibreOffice</gu>
   <he_IL>Slovak Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु स्लोवाक भाषा मेटा-पैकेज</hi>
   <hr>Slovak Language Meta-Package for LibreOffice</hr>
   <hu>Slovak Language Meta-Package for LibreOffice</hu>
   <id>Slovak Language Meta-Package for LibreOffice</id>
   <is>Slovak Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua slovacca per LibreOffice</it>
   <ja>LibreOffice用のスロバキア語メタパッケージ</ja>
   <kk>Slovak Language Meta-Package for LibreOffice</kk>
   <ko>Slovak Language Meta-Package for LibreOffice</ko>
   <ku>Slovak Language Meta-Package for LibreOffice</ku>
   <lt>Slovak Language Meta-Package for LibreOffice</lt>
   <mk>Slovak Language Meta-Package for LibreOffice</mk>
   <mr>Slovak Language Meta-Package for LibreOffice</mr>
   <nb_NO>Slovak Language Meta-Package for LibreOffice</nb_NO>
   <nb>Slovakisk språkpakke for LibreOffice</nb>
   <nl_BE>Slovak Language Meta-Package for LibreOffice</nl_BE>
   <nl>Slovaaks Taal Meta-Pakket voor LibreOffice</nl>
   <or>Slovak Language Meta-Package for LibreOffice</or>
   <pl>Słowacki meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Eslovaco Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Eslovaco Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Slovak Language Meta-Package for LibreOffice</ro>
   <ru>Slovak Language Meta-Package for LibreOffice</ru>
   <sk>Slovak Language Meta-Package for LibreOffice</sk>
   <sl>Slovaški jezikovni meta-paket za LibreOffice</sl>
   <so>Slovak Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në sllovakisht</sq>
   <sr>Slovak Language Meta-Package for LibreOffice</sr>
   <sv>Slovakiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Slovak สำหรับ LibreOffice</th>
   <tr>LibreOffice için Slovakça Dili Üst-Paketi</tr>
   <uk>Slovak Language Meta-Package for LibreOffice</uk>
   <vi>Slovak Language Meta-Package for LibreOffice</vi>
   <zh_CN>Slovak Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Slovak Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Slovak Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-sk
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-sk
libreoffice-gtk3
</uninstall_package_names>

</app>
