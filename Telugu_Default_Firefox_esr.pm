<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Telugu_Default_Firefox_esr
</name>

<description>
   <am>Telugu localisation of Firefox ESR</am>
   <ar>Telugu localisation of Firefox ESR</ar>
   <be>Telugu localisation of Firefox ESR</be>
   <bg>Telugu localisation of Firefox ESR</bg>
   <bn>Telugu localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Telugu</ca>
   <cs>Telugu localisation of Firefox ESR</cs>
   <da>Telugu localisation of Firefox ESR</da>
   <de>Indische Lokalisation für die Sprache “Telugu” von “Firefox ESR”</de>
   <el>Telugu για Firefox ESR</el>
   <en>Telugu localisation of Firefox ESR</en>
   <es_ES>Localización Telugu de Firefox ESR</es_ES>
   <es>Localización Telugu de Firefox ESR</es>
   <et>Telugu localisation of Firefox ESR</et>
   <eu>Telugu localisation of Firefox ESR</eu>
   <fa>Telugu localisation of Firefox ESR</fa>
   <fil_PH>Telugu localisation of Firefox ESR</fil_PH>
   <fi>Telugu localisation of Firefox ESR</fi>
   <fr_BE>Localisation en télougou (Inde) pour Firefox ESR</fr_BE>
   <fr>Localisation en télougou (Inde) pour Firefox ESR</fr>
   <gl_ES>Telugu localisation of Firefox ESR</gl_ES>
   <gu>Telugu localisation of Firefox ESR</gu>
   <he_IL>Telugu localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का तेलुगू संस्करण</hi>
   <hr>Telugu localisation of Firefox ESR</hr>
   <hu>Telugu localisation of Firefox ESR</hu>
   <id>Telugu localisation of Firefox ESR</id>
   <is>Telugu localisation of Firefox ESR</is>
   <it>Localizzazione telugu di Firefox ESR</it>
   <ja>テルグ語版 Firefox ESR</ja>
   <kk>Telugu localisation of Firefox ESR</kk>
   <ko>Telugu localisation of Firefox ESR</ko>
   <ku>Telugu localisation of Firefox ESR</ku>
   <lt>Telugu localisation of Firefox ESR</lt>
   <mk>Telugu localisation of Firefox ESR</mk>
   <mr>Telugu localisation of Firefox ESR</mr>
   <nb_NO>Telugu localisation of Firefox ESR</nb_NO>
   <nb>Telugu lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Telugu localisation of Firefox ESR</nl_BE>
   <nl>Telugu localisation of Firefox ESR</nl>
   <or>Telugu localisation of Firefox ESR</or>
   <pl>Telugu localisation of Firefox ESR</pl>
   <pt_BR>Telugo Localização para o Firefox ESR</pt_BR>
   <pt>Telego Localização para Firefox ESR</pt>
   <ro>Telugu localisation of Firefox ESR</ro>
   <ru>Telugu localisation of Firefox ESR</ru>
   <sk>Telugu localisation of Firefox ESR</sk>
   <sl>Teluške krajevne nastavitve za Firefox ESR</sl>
   <so>Telugu localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në telugu</sq>
   <sr>Telugu localisation of Firefox ESR</sr>
   <sv>Telugu lokalisering av Firefox ESR</sv>
   <th>Telugu localisation of Firefox ESR</th>
   <tr>Firefox ESR Teluguca yerelleştirmesi</tr>
   <uk>Telugu localisation of Firefox ESR</uk>
   <vi>Telugu localisation of Firefox ESR</vi>
   <zh_CN>Telugu localisation of Firefox ESR</zh_CN>
   <zh_HK>Telugu localisation of Firefox ESR</zh_HK>
   <zh_TW>Telugu localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-te
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-te
</uninstall_package_names>

</app>
