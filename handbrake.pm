<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Media Converter
</category>

<name>
Handbrake
</name>

<description>
   <am>versatile DVD ripper and video transcoder (cli and GUI)</am>
   <ar>versatile DVD ripper and video transcoder (cli and GUI)</ar>
   <be>versatile DVD ripper and video transcoder (cli and GUI)</be>
   <bg>versatile DVD ripper and video transcoder (cli and GUI)</bg>
   <bn>versatile DVD ripper and video transcoder (cli and GUI)</bn>
   <ca>Gravador de DVDs i transcodificador de vídeo versàtil (IGU i terminal)</ca>
   <cs>versatile DVD ripper and video transcoder (cli and GUI)</cs>
   <da>alsidig DVD-ripper og -videotranskoder (kommandolinje og grafisk brugerflade)</da>
   <de>Vielseitiger DVD-Ripper und Video-Transcoder (Cli und GUI)</de>
   <el>Ευέλικτο πρόγραμμα για DVD ripper και video transcoder (cli και GUI)</el>
   <en>versatile DVD ripper and video transcoder (cli and GUI)</en>
   <es_ES>Versátil DVD ripper y transcodificador de vídeo (CLI y GUI)</es_ES>
   <es>Versátil DVD ripper y transcodificador de vídeo (CLI y GUI)</es>
   <et>versatile DVD ripper and video transcoder (cli and GUI)</et>
   <eu>versatile DVD ripper and video transcoder (cli and GUI)</eu>
   <fa>versatile DVD ripper and video transcoder (cli and GUI)</fa>
   <fil_PH>versatile DVD ripper and video transcoder (cli and GUI)</fil_PH>
   <fi>monipuolinen DVD:n kopiointi- ja rippaus-muunnoskooderi (komentokehote sekä graafinen käyttöliittymä)</fi>
   <fr_BE>Extracteur de DVD et transcodeur vidéo (cli and GUI)</fr_BE>
   <fr>Extracteur de DVD et transcodeur vidéo (cli and GUI)</fr>
   <gl_ES>Versátil extractor de datos de DVDs e transcodificador de v´dieo (por consola e interface gráfica)</gl_ES>
   <gu>versatile DVD ripper and video transcoder (cli and GUI)</gu>
   <he_IL>versatile DVD ripper and video transcoder (cli and GUI)</he_IL>
   <hi>बहु उपयोगी डीवीडी रिपर व वीडियो ट्रान्सकडेर (कमांड लाइन व ग्राफ़िकल अंतरफलक)</hi>
   <hr>versatile DVD ripper and video transcoder (cli and GUI)</hr>
   <hu>versatile DVD ripper and video transcoder (cli and GUI)</hu>
   <id>versatile DVD ripper and video transcoder (cli and GUI)</id>
   <is>versatile DVD ripper and video transcoder (cli and GUI)</is>
   <it>versatile etrattore audio da DVD e transcoder video (CLI e GUI)</it>
   <ja>万能なDVDリッパーとビデオ トランスコーダー (CLIとGUI)</ja>
   <kk>versatile DVD ripper and video transcoder (cli and GUI)</kk>
   <ko>versatile DVD ripper and video transcoder (cli and GUI)</ko>
   <ku>versatile DVD ripper and video transcoder (cli and GUI)</ku>
   <lt>versatile DVD ripper and video transcoder (cli and GUI)</lt>
   <mk>versatile DVD ripper and video transcoder (cli and GUI)</mk>
   <mr>versatile DVD ripper and video transcoder (cli and GUI)</mr>
   <nb_NO>versatile DVD ripper and video transcoder (cli and GUI)</nb_NO>
   <nb>allsidig DVD-ripper og videotranskoder (terminal og grafisk brukergrensesnitt)</nb>
   <nl_BE>versatile DVD ripper and video transcoder (cli and GUI)</nl_BE>
   <nl>veelzijdige DVD ripper en video transcoder (cli en GUI)</nl>
   <or>versatile DVD ripper and video transcoder (cli and GUI)</or>
   <pl>wszechstronny ripper DVD i transkoder wideo (CLI i GUI)</pl>
   <pt_BR>Versátil extrator de dados de DVDs (DVD ripper) e transcodificador de vídeo (por console/terminal - CLI e por interface gráfica - GUI)</pt_BR>
   <pt>Versátil extractor de dados de DVDs (DVD ripper) e transcodificador de vídeo (por consola e interface gráfica)</pt>
   <ro>versatile DVD ripper and video transcoder (cli and GUI)</ro>
   <ru>Многофункциональный DVD риппер и перекодировщик видео</ru>
   <sk>versatile DVD ripper and video transcoder (cli and GUI)</sk>
   <sl>Vsestranski zajemalnik DVDjev in video transkoder (ukazna vrstica in grafični vmesnik)</sl>
   <so>versatile DVD ripper and video transcoder (cli and GUI)</so>
   <sq>Fjalor gujaratase për hunspell</sq>
   <sr>versatile DVD ripper and video transcoder (cli and GUI)</sr>
   <sv>mångsidig DVD ripper och video transcoder (cli och GUI)</sv>
   <th>versatile DVD ripper and video transcoder (cli and GUI)</th>
   <tr>çok yönlü DVD kesici ve görüntü dönüştürücü (cli ve Grafik Arayüzü))</tr>
   <uk>універсальний DVD витягувач та відео перетворювач (графічний та консольний)</uk>
   <vi>versatile DVD ripper and video transcoder (cli and GUI)</vi>
   <zh_CN>多功能 DVD 抓取与视频转码工具（cli 和 GUI 程序）</zh_CN>
   <zh_HK>versatile DVD ripper and video transcoder (cli and GUI)</zh_HK>
   <zh_TW>versatile DVD ripper and video transcoder (cli and GUI)</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
handbrake
handbrake-cli
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
handbrake
handbrake-cli
</uninstall_package_names>
</app>
