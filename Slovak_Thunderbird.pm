<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Slovak_Thunderbird
</name>

<description>
   <am>Slovak localisation of Thunderbird</am>
   <ar>Slovak localisation of Thunderbird</ar>
   <be>Slovak localisation of Thunderbird</be>
   <bg>Slovak localisation of Thunderbird</bg>
   <bn>Slovak localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Eslovac</ca>
   <cs>Slovak localisation of Thunderbird</cs>
   <da>Slovakisk oversættelse af Thunderbird</da>
   <de>Slowakische Lokalisierung von Thunderbird</de>
   <el>Σλοβακική εντοπισμός του Thunderbird</el>
   <en>Slovak localisation of Thunderbird</en>
   <es_ES>Localización Eslovaca de Thunderbird</es_ES>
   <es>Localización Eslovaco de Thunderbird</es>
   <et>Slovak localisation of Thunderbird</et>
   <eu>Slovak localisation of Thunderbird</eu>
   <fa>Slovak localisation of Thunderbird</fa>
   <fil_PH>Slovak localisation of Thunderbird</fil_PH>
   <fi>Slovakialainen kielipaketti Thunderbirdille</fi>
   <fr_BE>Localisation en slovaque pour Thunderbird</fr_BE>
   <fr>Localisation en slovaque pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao eslovaco</gl_ES>
   <gu>Slovak localisation of Thunderbird</gu>
   <he_IL>Slovak localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का स्लोवाक संस्करण</hi>
   <hr>Slovak localisation of Thunderbird</hr>
   <hu>Slovak localisation of Thunderbird</hu>
   <id>Slovak localisation of Thunderbird</id>
   <is>Slovak localisation of Thunderbird</is>
   <it>Localizzazione slovacca di Thunderbird</it>
   <ja>Thunderbird のスロバキア語版</ja>
   <kk>Slovak localisation of Thunderbird</kk>
   <ko>Slovak localisation of Thunderbird</ko>
   <ku>Slovak localisation of Thunderbird</ku>
   <lt>Slovak localisation of Thunderbird</lt>
   <mk>Slovak localisation of Thunderbird</mk>
   <mr>Slovak localisation of Thunderbird</mr>
   <nb_NO>Slovak localisation of Thunderbird</nb_NO>
   <nb>Slovakisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Slovak localisation of Thunderbird</nl_BE>
   <nl>Slovaakse lokalisatie van Thunderbird</nl>
   <or>Slovak localisation of Thunderbird</or>
   <pl>Słowacka lokalizacja Thunderbirda</pl>
   <pt_BR>Eslovaco Localização para o Thunderbird</pt_BR>
   <pt>Eslovaco Localização para Thunderbird</pt>
   <ro>Slovak localisation of Thunderbird</ro>
   <ru>Slovak localisation of Thunderbird</ru>
   <sk>Slovak localisation of Thunderbird</sk>
   <sl>Slovaške krajevne nastavitve za Thunderbird</sl>
   <so>Slovak localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në sllovakisht</sq>
   <sr>Slovak localisation of Thunderbird</sr>
   <sv>Slovakisk lokalisering av Thunderbird</sv>
   <th>Slovak localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Slovakça yerelleştirmesi</tr>
   <uk>Slovak localisation of Thunderbird</uk>
   <vi>Slovak localisation of Thunderbird</vi>
   <zh_CN>Slovak localisation of Thunderbird</zh_CN>
   <zh_HK>Slovak localisation of Thunderbird</zh_HK>
   <zh_TW>Slovak localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-sk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-sk
</uninstall_package_names>

</app>
