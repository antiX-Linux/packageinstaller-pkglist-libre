<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bulgarian_Default_Firefox_esr
</name>

<description>
   <am>Bulgarian localisation of Firefox ESR</am>
   <ar>Bulgarian localisation of Firefox ESR</ar>
   <be>Bulgarian localisation of Firefox ESR</be>
   <bg>Bulgarian localisation of Firefox ESR</bg>
   <bn>Bulgarian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Búlgar</ca>
   <cs>Bulgarian localisation of Firefox ESR</cs>
   <da>Bulgarian localisation of Firefox ESR</da>
   <de>Bulgarische Lokalisation von “Firefox ESR”</de>
   <el>Βουλγαρικά για τον Firefox ESR</el>
   <en>Bulgarian localisation of Firefox ESR</en>
   <es_ES>Localización Búlgara de Firefox ESR</es_ES>
   <es>Localización Búlgaro de Firefox ESR</es>
   <et>Bulgarian localisation of Firefox ESR</et>
   <eu>Bulgarian localisation of Firefox ESR</eu>
   <fa>Bulgarian localisation of Firefox ESR</fa>
   <fil_PH>Bulgarian localisation of Firefox ESR</fil_PH>
   <fi>Bulgarian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en bulgare pour Firefox ESR</fr_BE>
   <fr>Localisation en bulgare pour Firefox ESR</fr>
   <gl_ES>Bulgarian localisation of Firefox ESR</gl_ES>
   <gu>Bulgarian localisation of Firefox ESR</gu>
   <he_IL>Bulgarian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का बल्गेरियाई संस्करण</hi>
   <hr>Bulgarian localisation of Firefox ESR</hr>
   <hu>Bulgarian localisation of Firefox ESR</hu>
   <id>Bulgarian localisation of Firefox ESR</id>
   <is>Bulgarian localisation of Firefox ESR</is>
   <it>Localizzazione bulgara di Firefox ESR</it>
   <ja>ブルガリア語版 Firefox ESR</ja>
   <kk>Bulgarian localisation of Firefox ESR</kk>
   <ko>Bulgarian localisation of Firefox ESR</ko>
   <ku>Bulgarian localisation of Firefox ESR</ku>
   <lt>Bulgarian localisation of Firefox ESR</lt>
   <mk>Bulgarian localisation of Firefox ESR</mk>
   <mr>Bulgarian localisation of Firefox ESR</mr>
   <nb_NO>Bulgarian localisation of Firefox ESR</nb_NO>
   <nb>Bulgarsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Bulgarian localisation of Firefox ESR</nl_BE>
   <nl>Bulgaarse lokalisatie van Firefox ESR</nl>
   <or>Bulgarian localisation of Firefox ESR</or>
   <pl>Bulgarian localisation of Firefox ESR</pl>
   <pt_BR>Búlgaro Localização para o Firefox ESR</pt_BR>
   <pt>Búlgaro Localização para Firefox ESR</pt>
   <ro>Bulgarian localisation of Firefox ESR</ro>
   <ru>Bulgarian localisation of Firefox ESR</ru>
   <sk>Bulgarian localisation of Firefox ESR</sk>
   <sl>Bolgarske krajevne nastavitve za Firefox ESR</sl>
   <so>Bulgarian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në bullgarisht</sq>
   <sr>Bulgarian localisation of Firefox ESR</sr>
   <sv>Bulgarisk lokalisering av Firefox ESR</sv>
   <th>Bulgarian localisation of Firefox ESR</th>
   <tr>Firefox ESR Bulgarca yerelleştirmesi</tr>
   <uk>Bulgarian localisation of Firefox ESR</uk>
   <vi>Bulgarian localisation of Firefox ESR</vi>
   <zh_CN>Bulgarian localisation of Firefox ESR</zh_CN>
   <zh_HK>Bulgarian localisation of Firefox ESR</zh_HK>
   <zh_TW>Bulgarian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-bg
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-bg
</uninstall_package_names>

</app>
