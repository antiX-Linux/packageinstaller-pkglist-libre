<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Chinese_CN_Firefox
</name>

<description>
   <am>Chinese_simplified localisation of Firefox</am>
   <ar>Chinese_simplified localisation of Firefox</ar>
   <be>Chinese_simplified localisation of Firefox</be>
   <bg>Chinese_simplified localisation of Firefox</bg>
   <bn>Chinese_simplified localisation of Firefox</bn>
   <ca>Localització de Firefox en Xinès simplificat</ca>
   <cs>Chinese_simplified localisation of Firefox</cs>
   <da>Kinesisk (forenklet) oversættelse af Firefox</da>
   <de>Chinesische (vereinfacht) Lokalisierung von Firefox</de>
   <el>Κινεζικά για το Firefox</el>
   <en>Chinese_simplified localisation of Firefox</en>
   <es_ES>Localización Chino simplificado de Firefox</es_ES>
   <es>Localización Chino simplificado de Firefox</es>
   <et>Chinese_simplified localisation of Firefox</et>
   <eu>Chinese_simplified localisation of Firefox</eu>
   <fa>Chinese_simplified localisation of Firefox</fa>
   <fil_PH>Chinese_simplified localisation of Firefox</fil_PH>
   <fi>Yksinkertaistetun kiinankielen Firefox-kotoistus</fi>
   <fr_BE>Localisation en chinois-simplifié pour Firefox</fr_BE>
   <fr>Localisation en chinois-simplifié pour Firefox</fr>
   <gl_ES>Localización de Firefox en chinés simplificado</gl_ES>
   <gu>Chinese_simplified localisation of Firefox</gu>
   <he_IL>Chinese_simplified localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का सरल चीनी संस्करण</hi>
   <hr>Chinese_simplified localisation of Firefox</hr>
   <hu>Chinese_simplified localisation of Firefox</hu>
   <id>Chinese_simplified localisation of Firefox</id>
   <is>Chinese_simplified localisation of Firefox</is>
   <it>Localizzazione in cinese_semplificato di Firefox</it>
   <ja>Firefox の簡体中国語版</ja>
   <kk>Chinese_simplified localisation of Firefox</kk>
   <ko>Chinese_simplified localisation of Firefox</ko>
   <ku>Chinese_simplified localisation of Firefox</ku>
   <lt>Chinese_simplified localisation of Firefox</lt>
   <mk>Chinese_simplified localisation of Firefox</mk>
   <mr>Chinese_simplified localisation of Firefox</mr>
   <nb_NO>Chinese_simplified localisation of Firefox</nb_NO>
   <nb>Kinesisk (forenklet) lokaltilpassing av Firefox</nb>
   <nl_BE>Chinese_simplified localisation of Firefox</nl_BE>
   <nl>Vereenvoudigd Chinese lokalisatie van Firefox</nl>
   <or>Chinese_simplified localisation of Firefox</or>
   <pl>Chiński uproszczony lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Chinês Simplificado Localização para o Firefox</pt_BR>
   <pt>Chinês_simplificado Localização para Firefox</pt>
   <ro>Chinese_simplified localisation of Firefox</ro>
   <ru>Китайская упрощенная локализация Firefox</ru>
   <sk>Chinese_simplified localisation of Firefox</sk>
   <sl>Kitajske (poenostavljeno) krajevne nastavitve za Firefox</sl>
   <so>Chinese_simplified localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në kinezçe të thjeshtuar</sq>
   <sr>Chinese_simplified localisation of Firefox</sr>
   <sv>Kinesisk_förenklad lokalisering av Firefox</sv>
   <th>Chinese_CN localisation ของ Firefox</th>
   <tr>Firefox Basitleştirilmiş_Çince yerelleştirmesi</tr>
   <uk>Chinese_simplified локалізація Firefox</uk>
   <vi>Chinese_simplified localisation of Firefox</vi>
   <zh_CN>Firefox 简体中文语言包</zh_CN>
   <zh_HK>Chinese_simplified localisation of Firefox</zh_HK>
   <zh_TW>Chinese_simplified localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-zh-tw
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-zh-tw
</uninstall_package_names>
</app>
