<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Latvian_Firefox
</name>

<description>
   <am>Latvian localisation of Firefox</am>
   <ar>Latvian localisation of Firefox</ar>
   <be>Latvian localisation of Firefox</be>
   <bg>Latvian localisation of Firefox</bg>
   <bn>Latvian localisation of Firefox</bn>
   <ca>Localització de Firefox en Latvi</ca>
   <cs>Latvian localisation of Firefox</cs>
   <da>Lettisk oversættelse af Firefox</da>
   <de>Lettische Lokalisierung von Firefox</de>
   <el>Λετονικά για το Firefox</el>
   <en>Latvian localisation of Firefox</en>
   <es_ES>Localización Letona de Firefox</es_ES>
   <es>Localización Letón de Firefox</es>
   <et>Latvian localisation of Firefox</et>
   <eu>Latvian localisation of Firefox</eu>
   <fa>Latvian localisation of Firefox</fa>
   <fil_PH>Latvian localisation of Firefox</fil_PH>
   <fi>Latvialainen Firefox-kotoistus</fi>
   <fr_BE>Localisation lettone pour Firefox</fr_BE>
   <fr>Localisation lettone pour Firefox</fr>
   <gl_ES>Localización do Firefox ao letón</gl_ES>
   <gu>Latvian localisation of Firefox</gu>
   <he_IL>Latvian localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का लात्वियाई संस्करण</hi>
   <hr>Latvian localisation of Firefox</hr>
   <hu>Latvian localisation of Firefox</hu>
   <id>Latvian localisation of Firefox</id>
   <is>Latvian localisation of Firefox</is>
   <it>Localizzazione lettone di Firefox</it>
   <ja>Firefox のラトビア語版</ja>
   <kk>Latvian localisation of Firefox</kk>
   <ko>Latvian localisation of Firefox</ko>
   <ku>Latvian localisation of Firefox</ku>
   <lt>Latvian localisation of Firefox</lt>
   <mk>Latvian localisation of Firefox</mk>
   <mr>Latvian localisation of Firefox</mr>
   <nb_NO>Latvian localisation of Firefox</nb_NO>
   <nb>Latvisk lokaltilpassing av Firefox</nb>
   <nl_BE>Latvian localisation of Firefox</nl_BE>
   <nl>Letse lokalisatie van Firefox</nl>
   <or>Latvian localisation of Firefox</or>
   <pl>Łotewska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Letão Localização para o Firefox</pt_BR>
   <pt>Letão Localização para Firefox</pt>
   <ro>Latvian localisation of Firefox</ro>
   <ru>Латвийская локализация Firefox</ru>
   <sk>Latvian localisation of Firefox</sk>
   <sl>Latvijske krajevne nastavitve za Firefox</sl>
   <so>Latvian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në letonisht</sq>
   <sr>Latvian localisation of Firefox</sr>
   <sv>Lettländsk lokalisering av Firefox</sv>
   <th>Latvian localisation ของ Firefox</th>
   <tr>Firefox'un Letonca yerelleştirmesi</tr>
   <uk>Latvian локалізація of Firefox</uk>
   <vi>Latvian localisation of Firefox</vi>
   <zh_CN>Latvian localisation of Firefox</zh_CN>
   <zh_HK>Latvian localisation of Firefox</zh_HK>
   <zh_TW>Latvian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-lv
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-lv
</uninstall_package_names>
</app>
