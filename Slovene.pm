<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Slovene
</name>

<description>
   <am>Slovene dictionary for hunspell</am>
   <ar>Slovene dictionary for hunspell</ar>
   <be>Slovene dictionary for hunspell</be>
   <bg>Slovene dictionary for hunspell</bg>
   <bn>Slovene dictionary for hunspell</bn>
   <ca>Diccionari Eslovè per hunspell</ca>
   <cs>Slovene dictionary for hunspell</cs>
   <da>Slovene dictionary for hunspell</da>
   <de>Slowenisches Wörterbuch für “Hunspell”</de>
   <el>Σλοβενικό λεξικό για hunspell</el>
   <en>Slovene dictionary for hunspell</en>
   <es_ES>Diccionario Esloveno para hunspell</es_ES>
   <es>Diccionario Esloveno para hunspell</es>
   <et>Slovene dictionary for hunspell</et>
   <eu>Slovene dictionary for hunspell</eu>
   <fa>Slovene dictionary for hunspell</fa>
   <fil_PH>Slovene dictionary for hunspell</fil_PH>
   <fi>Slovene dictionary for hunspell</fi>
   <fr_BE>Slovène dictionnaire pour hunspell</fr_BE>
   <fr>Slovène dictionnaire pour hunspell</fr>
   <gl_ES>Slovene dictionary for hunspell</gl_ES>
   <gu>Slovene dictionary for hunspell</gu>
   <he_IL>Slovene dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु स्लोवेनियाई शब्दकोष</hi>
   <hr>Slovene dictionary for hunspell</hr>
   <hu>Slovene dictionary for hunspell</hu>
   <id>Slovene dictionary for hunspell</id>
   <is>Slovene dictionary for hunspell</is>
   <it>Dizionario slovene per hunspell</it>
   <ja>Hunspell 用スロベニア語辞書</ja>
   <kk>Slovene dictionary for hunspell</kk>
   <ko>Slovene dictionary for hunspell</ko>
   <ku>Slovene dictionary for hunspell</ku>
   <lt>Slovene dictionary for hunspell</lt>
   <mk>Slovene dictionary for hunspell</mk>
   <mr>Slovene dictionary for hunspell</mr>
   <nb_NO>Slovene dictionary for hunspell</nb_NO>
   <nb>Slovensk ordliste for hunspell</nb>
   <nl_BE>Slovene dictionary for hunspell</nl_BE>
   <nl>Slovene dictionary for hunspell</nl>
   <or>Slovene dictionary for hunspell</or>
   <pl>Slovene dictionary for hunspell</pl>
   <pt_BR>Dicionário Esloveno para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Esloveno para hunspell</pt>
   <ro>Slovene dictionary for hunspell</ro>
   <ru>Slovene dictionary for hunspell</ru>
   <sk>Slovene dictionary for hunspell</sk>
   <sl>Slovenski slovar za hunspell</sl>
   <so>Slovene dictionary for hunspell</so>
   <sq>Fjalor sllovenisht për hunspell</sq>
   <sr>Slovene dictionary for hunspell</sr>
   <sv>Slovensk ordbok för hunspell</sv>
   <th>Slovene dictionary for hunspell</th>
   <tr>Hunspell için Slovence sözlük</tr>
   <uk>Slovene dictionary for hunspell</uk>
   <vi>Slovene dictionary for hunspell</vi>
   <zh_CN>Slovene dictionary for hunspell</zh_CN>
   <zh_HK>Slovene dictionary for hunspell</zh_HK>
   <zh_TW>Slovene dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-sl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-sl
</uninstall_package_names>

</app>
