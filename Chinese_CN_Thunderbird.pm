<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Chinese_CN_Thunderbird
</name>

<description>
   <am>Chinese_simplified localisation of Thunderbird</am>
   <ar>Chinese_simplified localisation of Thunderbird</ar>
   <be>Chinese_simplified localisation of Thunderbird</be>
   <bg>Chinese_simplified localisation of Thunderbird</bg>
   <bn>Chinese_simplified localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Xinès simplificat</ca>
   <cs>Chinese_simplified localisation of Thunderbird</cs>
   <da>Kinesisk (forenklet) oversættelse af Thunderbird</da>
   <de>Chinesische (vereinfacht) Lokalisierung von Thunderbird</de>
   <el>Κινεζικά για το Thunderbird</el>
   <en>Chinese_simplified localisation of Thunderbird</en>
   <es_ES>Localización Chino simplificado de Thunderbird</es_ES>
   <es>Localización Chino simplificado de Thunderbird</es>
   <et>Chinese_simplified localisation of Thunderbird</et>
   <eu>Chinese_simplified localisation of Thunderbird</eu>
   <fa>Chinese_simplified localisation of Thunderbird</fa>
   <fil_PH>Chinese_simplified localisation of Thunderbird</fil_PH>
   <fi>Yksinkertaistetun kiinankielen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en chinois-simplifié pour Thunderbird</fr_BE>
   <fr>Localisation en chinois-simplifié pour Thunderbird</fr>
   <gl_ES>Localización do Thunderbird en chinés simplicado</gl_ES>
   <gu>Chinese_simplified localisation of Thunderbird</gu>
   <he_IL>Chinese_simplified localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का सरल चीनी संस्करण</hi>
   <hr>Chinese_simplified localisation of Thunderbird</hr>
   <hu>Chinese_simplified localisation of Thunderbird</hu>
   <id>Chinese_simplified localisation of Thunderbird</id>
   <is>Chinese_simplified localisation of Thunderbird</is>
   <it>Localizzazione in cinese_semplificato di Thunderbird</it>
   <ja>Thunderbird の簡体中国語版</ja>
   <kk>Chinese_simplified localisation of Thunderbird</kk>
   <ko>Chinese_simplified localisation of Thunderbird</ko>
   <ku>Chinese_simplified localisation of Thunderbird</ku>
   <lt>Chinese_simplified localisation of Thunderbird</lt>
   <mk>Chinese_simplified localisation of Thunderbird</mk>
   <mr>Chinese_simplified localisation of Thunderbird</mr>
   <nb_NO>Chinese_simplified localisation of Thunderbird</nb_NO>
   <nb>Kinesisk (forenklet) lokaltilpassing av Thunderbird</nb>
   <nl_BE>Chinese_simplified localisation of Thunderbird</nl_BE>
   <nl>Vereenvoudigd Chinese lokalisatie van Thunderbird</nl>
   <or>Chinese_simplified localisation of Thunderbird</or>
   <pl>Chiński uproszczony lokalizacja Thunderbirda</pl>
   <pt_BR>Chinês Simplificado Localização para o Thunderbird</pt_BR>
   <pt>Chinês_simplificado Localização para Thunderbird</pt>
   <ro>Chinese_simplified localisation of Thunderbird</ro>
   <ru>Chinese_simplified localisation of Thunderbird</ru>
   <sk>Chinese_simplified localisation of Thunderbird</sk>
   <sl>Kitajske (poenostavljeno) krajevne nastavitve za Thunderbird</sl>
   <so>Chinese_simplified localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në kinezçe të thjeshtuar</sq>
   <sr>Chinese_simplified localisation of Thunderbird</sr>
   <sv>Kinesisk_förenklad lokalisering av Thunderbird</sv>
   <th>Chinese_CN localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Basitleştirilmiş_Çince yerelleştirmesi</tr>
   <uk>Chinese_simplified локалізація Thunderbird</uk>
   <vi>Chinese_simplified localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 简体中文语言包</zh_CN>
   <zh_HK>Chinese_simplified localisation of Thunderbird</zh_HK>
   <zh_TW>Chinese_simplified localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-zh-tw
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-zh-tw
</uninstall_package_names>

</app>
