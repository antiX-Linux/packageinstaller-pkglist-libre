<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Vietnamese_Input_fcitx
</name>

<description>
   <am>Vietnamese Fonts and fcitx</am>
   <ar>Vietnamese Fonts and fcitx</ar>
   <be>Vietnamese Fonts and fcitx</be>
   <bg>Vietnamese Fonts and fcitx</bg>
   <bn>Vietnamese Fonts and fcitx</bn>
   <ca>Lletra tipogràfica i FCITX Vietnamites</ca>
   <cs>Vietnamese Fonts and fcitx</cs>
   <da>Vietnamesisk skrifttyper og fcitx</da>
   <de>Vietnamesische Schriftzeichen und Fcitx</de>
   <el>Βιετναμέζικες γραμματοσειρές και fcitx</el>
   <en>Vietnamese Fonts and fcitx</en>
   <es_ES>Fuentes tipográficas vietnamitas y fcitx</es_ES>
   <es>Fuentes tipográficas vietnamitas y fcitx</es>
   <et>Vietnamese Fonts and fcitx</et>
   <eu>Vietnamese Fonts and fcitx</eu>
   <fa>Vietnamese Fonts and fcitx</fa>
   <fil_PH>Vietnamese Fonts and fcitx</fil_PH>
   <fi>Vietnamilaiset kirjasimet ja fcitx</fi>
   <fr_BE>Polices vietnamiennes et fcitx</fr_BE>
   <fr>Polices vietnamiennes et fcitx</fr>
   <gl_ES>Fontes e fcitx vietnamita</gl_ES>
   <gu>Vietnamese Fonts and fcitx</gu>
   <he_IL>Vietnamese Fonts and fcitx</he_IL>
   <hi>वियतनामी मुद्रलिपि व Fcitx</hi>
   <hr>Vietnamese Fonts and fcitx</hr>
   <hu>Vietnamese Fonts and fcitx</hu>
   <id>Vietnamese Fonts and fcitx</id>
   <is>Vietnamese Fonts and fcitx</is>
   <it>Font e fcitx per la lingua vietnamita</it>
   <ja>ベトナム語のフォントと fcitx</ja>
   <kk>Vietnamese Fonts and fcitx</kk>
   <ko>Vietnamese Fonts and fcitx</ko>
   <ku>Vietnamese Fonts and fcitx</ku>
   <lt>Vietnamese Fonts and fcitx</lt>
   <mk>Vietnamese Fonts and fcitx</mk>
   <mr>Vietnamese Fonts and fcitx</mr>
   <nb_NO>Vietnamese Fonts and fcitx</nb_NO>
   <nb>Vietnamesiske skrifter og fcitx</nb>
   <nl_BE>Vietnamese Fonts and fcitx</nl_BE>
   <nl>Vietnamese Fonts en fcitx</nl>
   <or>Vietnamese Fonts and fcitx</or>
   <pl>Wietnamskie fonty i fcitx</pl>
   <pt_BR>Fontes e fcitx Vietnamita</pt_BR>
   <pt>Vietnamita Fontes e fcitx</pt>
   <ro>Vietnamese Fonts and fcitx</ro>
   <ru>Vietnamese Fonts and fcitx</ru>
   <sk>Vietnamese Fonts and fcitx</sk>
   <sl>Vietnamske pisave in fcitx</sl>
   <so>Vietnamese Fonts and fcitx</so>
   <sq>Shkronja dhe citx për vietnamishten</sq>
   <sr>Vietnamese Fonts and fcitx</sr>
   <sv>Vietnamesiska Typsnitt och fcitx</sv>
   <th>ฟอนต์ภาษาเวียดนามและ fcitx</th>
   <tr>Vietnamca Yazı Tipi ve fcitx</tr>
   <uk>Vietnamese Fonts and fcitx</uk>
   <vi>Vietnamese Fonts and fcitx</vi>
   <zh_CN>Vietnamese Fonts and fcitx</zh_CN>
   <zh_HK>Vietnamese Fonts and fcitx</zh_HK>
   <zh_TW>Vietnamese Fonts and fcitx</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
fcitx
fcitx-config-gtk
fcitx-frontend-all
fcitx-frontend-fbterm
fcitx-tools
fcitx-ui-classic
fcitx-ui-light
fcitx-unikey
im-config
mozc-utils-gui
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
fcitx
fcitx-config-gtk
fcitx-frontend-all
fcitx-frontend-fbterm
fcitx-tools
fcitx-ui-classic
fcitx-ui-light
fcitx-unikey
im-config
mozc-utils-gui
</uninstall_package_names>
</app>
