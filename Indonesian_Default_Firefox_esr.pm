<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Indonesian_Default_Firefox_esr
</name>

<description>
   <am>Indonesian localisation of Firefox ESR</am>
   <ar>Indonesian localisation of Firefox ESR</ar>
   <be>Indonesian localisation of Firefox ESR</be>
   <bg>Indonesian localisation of Firefox ESR</bg>
   <bn>Indonesian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Indonesi</ca>
   <cs>Indonesian localisation of Firefox ESR</cs>
   <da>Indonesian localisation of Firefox ESR</da>
   <de>Indonesische Lokalisation von “Firefox ESR”</de>
   <el>Ινδονησιακά για Firefox ESR</el>
   <en>Indonesian localisation of Firefox ESR</en>
   <es_ES>Localización Indonesia de Firefox ESR</es_ES>
   <es>Localización Indonesio de Firefox ESR</es>
   <et>Indonesian localisation of Firefox ESR</et>
   <eu>Indonesian localisation of Firefox ESR</eu>
   <fa>Indonesian localisation of Firefox ESR</fa>
   <fil_PH>Indonesian localisation of Firefox ESR</fil_PH>
   <fi>Indonesian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en indonésien pour Firefox ESR</fr_BE>
   <fr>Localisation en indonésien pour Firefox ESR</fr>
   <gl_ES>Indonesian localisation of Firefox ESR</gl_ES>
   <gu>Indonesian localisation of Firefox ESR</gu>
   <he_IL>Indonesian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का इन्डोनेशियाई संस्करण</hi>
   <hr>Indonesian localisation of Firefox ESR</hr>
   <hu>Indonesian localisation of Firefox ESR</hu>
   <id>Indonesian localisation of Firefox ESR</id>
   <is>Indonesian localisation of Firefox ESR</is>
   <it>Localizzazione indonesiana di Firefox ESR</it>
   <ja>インドネシア語版 Firefox ESR</ja>
   <kk>Indonesian localisation of Firefox ESR</kk>
   <ko>Indonesian localisation of Firefox ESR</ko>
   <ku>Indonesian localisation of Firefox ESR</ku>
   <lt>Indonesian localisation of Firefox ESR</lt>
   <mk>Indonesian localisation of Firefox ESR</mk>
   <mr>Indonesian localisation of Firefox ESR</mr>
   <nb_NO>Indonesian localisation of Firefox ESR</nb_NO>
   <nb>Indonesisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Indonesian localisation of Firefox ESR</nl_BE>
   <nl>Indonesian localisation of Firefox ESR</nl>
   <or>Indonesian localisation of Firefox ESR</or>
   <pl>Indonesian localisation of Firefox ESR</pl>
   <pt_BR>Indonésio Localização para o Firefox ESR</pt_BR>
   <pt>Indonésio Localização para Firefox ESR</pt>
   <ro>Indonesian localisation of Firefox ESR</ro>
   <ru>Indonesian localisation of Firefox ESR</ru>
   <sk>Indonesian localisation of Firefox ESR</sk>
   <sl>Indonezijske krajevne nastavitve za Firefox ESR</sl>
   <so>Indonesian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në indonezisht</sq>
   <sr>Indonesian localisation of Firefox ESR</sr>
   <sv>Indonesisk lokalisering av Firefox ESR</sv>
   <th>Indonesian localisation of Firefox ESR</th>
   <tr>Firefox ESR Endonezce yerelleştirmesi</tr>
   <uk>Indonesian localisation of Firefox ESR</uk>
   <vi>Indonesian localisation of Firefox ESR</vi>
   <zh_CN>Indonesian localisation of Firefox ESR</zh_CN>
   <zh_HK>Indonesian localisation of Firefox ESR</zh_HK>
   <zh_TW>Indonesian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ga-id
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ga-id
</uninstall_package_names>

</app>
