<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Uzbek
</name>

<description>
   <am>Uzbek dictionary for hunspell</am>
   <ar>Uzbek dictionary for hunspell</ar>
   <be>Uzbek dictionary for hunspell</be>
   <bg>Uzbek dictionary for hunspell</bg>
   <bn>Uzbek dictionary for hunspell</bn>
   <ca>Diccionari Uzbek per hunspell</ca>
   <cs>Uzbek dictionary for hunspell</cs>
   <da>Uzbek dictionary for hunspell</da>
   <de>Usbekisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Ουζμπεκιστάν για hunspell</el>
   <en>Uzbek dictionary for hunspell</en>
   <es_ES>Diccionario Uzbeko para hunspell</es_ES>
   <es>Diccionario Uzbeko para hunspell</es>
   <et>Uzbek dictionary for hunspell</et>
   <eu>Uzbek dictionary for hunspell</eu>
   <fa>Uzbek dictionary for hunspell</fa>
   <fil_PH>Uzbek dictionary for hunspell</fil_PH>
   <fi>Uzbek dictionary for hunspell</fi>
   <fr_BE>Ouzbek dictionnaire pour hunspell</fr_BE>
   <fr>Ouzbek dictionnaire pour hunspell</fr>
   <gl_ES>Uzbek dictionary for hunspell</gl_ES>
   <gu>Uzbek dictionary for hunspell</gu>
   <he_IL>Uzbek dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु उज़बेक शब्दकोष</hi>
   <hr>Uzbek dictionary for hunspell</hr>
   <hu>Uzbek dictionary for hunspell</hu>
   <id>Uzbek dictionary for hunspell</id>
   <is>Uzbek dictionary for hunspell</is>
   <it>Dizionario uzbek per hunspell</it>
   <ja>Hunspell 用ウズベク語辞書</ja>
   <kk>Uzbek dictionary for hunspell</kk>
   <ko>Uzbek dictionary for hunspell</ko>
   <ku>Uzbek dictionary for hunspell</ku>
   <lt>Uzbek dictionary for hunspell</lt>
   <mk>Uzbek dictionary for hunspell</mk>
   <mr>Uzbek dictionary for hunspell</mr>
   <nb_NO>Uzbek dictionary for hunspell</nb_NO>
   <nb>Usbekisk ordliste for hunspell</nb>
   <nl_BE>Uzbek dictionary for hunspell</nl_BE>
   <nl>Uzbek dictionary for hunspell</nl>
   <or>Uzbek dictionary for hunspell</or>
   <pl>Uzbek dictionary for hunspell</pl>
   <pt_BR>Dicionário Usbeque para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Uzbeque para hunspell</pt>
   <ro>Uzbek dictionary for hunspell</ro>
   <ru>Uzbek dictionary for hunspell</ru>
   <sk>Uzbek dictionary for hunspell</sk>
   <sl>Uzbeški slovar za hunspell</sl>
   <so>Uzbek dictionary for hunspell</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në venda</sq>
   <sr>Uzbek dictionary for hunspell</sr>
   <sv>Uzbekisk ordbok för hunspell</sv>
   <th>Uzbek dictionary for hunspell</th>
   <tr>Hunspell için Özbekçe sözlük</tr>
   <uk>Uzbek dictionary for hunspell</uk>
   <vi>Uzbek dictionary for hunspell</vi>
   <zh_CN>Uzbek dictionary for hunspell</zh_CN>
   <zh_HK>Uzbek dictionary for hunspell</zh_HK>
   <zh_TW>Uzbek dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-uz
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-uz
</uninstall_package_names>

</app>
