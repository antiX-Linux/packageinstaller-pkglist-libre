<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
ScummVM
</name>

<description>
   <en>Engine for several graphical adventure games</en>
</description>

<installable>
all
</installable>

<screenshot>
</screenshot>

<preinstall>

</preinstall>

<install_package_names>
scummvm
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
scummvm
</uninstall_package_names>
</app>
