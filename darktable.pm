<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Graphics
</category>

<name>
Latest darktable
</name>

<description>
   <am>Virtual light table and darkroom for photographers</am>
   <ar>Virtual light table and darkroom for photographers</ar>
   <be>Virtual light table and darkroom for photographers</be>
   <bg>Virtual light table and darkroom for photographers</bg>
   <bn>Virtual light table and darkroom for photographers</bn>
   <ca>Taula de retocs virtual per a fotògrafs</ca>
   <cs>Virtual light table and darkroom for photographers</cs>
   <da>Virtual light table and darkroom for photographers</da>
   <de>Virtuelles Leuchtpult und Dunkelkammer für Photographen</de>
   <el>Εικονικό ελαφρύ τραπέζι και σκοτεινό δωμάτιο για φωτογράφους</el>
   <en>Virtual light table and darkroom for photographers</en>
   <es_ES>Mesa de luz virtual y cuarto oscuro para fotógrafos</es_ES>
   <es>Mesa de luz virtual y cuarto oscuro para fotógrafos</es>
   <et>Virtual light table and darkroom for photographers</et>
   <eu>Virtual light table and darkroom for photographers</eu>
   <fa>Virtual light table and darkroom for photographers</fa>
   <fil_PH>Virtual light table and darkroom for photographers</fil_PH>
   <fi>Virtuaalinen valopöytä sekä pimiö valokuvaajille</fi>
   <fr_BE>Table lumineuse et chambre noire virtuelles pour les photographes</fr_BE>
   <fr>Table lumineuse et chambre noire virtuelles pour les photographes</fr>
   <gl_ES>Virtual light table and darkroom for photographers</gl_ES>
   <gu>Virtual light table and darkroom for photographers</gu>
   <he_IL>Virtual light table and darkroom for photographers</he_IL>
   <hi>फोटोग्राफर हेतु वर्चुअल लाइट टेबल व डार्क रूम</hi>
   <hr>Virtual light table and darkroom for photographers</hr>
   <hu>Virtual light table and darkroom for photographers</hu>
   <id>Virtual light table and darkroom for photographers</id>
   <is>Virtual light table and darkroom for photographers</is>
   <it>Tavolo luminoso virtuale e camera oscura per fotografi</it>
   <ja>写真家向けのバーチャル・ライトテーブルと暗室</ja>
   <kk>Virtual light table and darkroom for photographers</kk>
   <ko>Virtual light table and darkroom for photographers</ko>
   <ku>Virtual light table and darkroom for photographers</ku>
   <lt>Virtual light table and darkroom for photographers</lt>
   <mk>Virtual light table and darkroom for photographers</mk>
   <mr>Virtual light table and darkroom for photographers</mr>
   <nb_NO>Virtual light table and darkroom for photographers</nb_NO>
   <nb>Virtuelt lysbord og mørkerom for fotografer</nb>
   <nl_BE>Virtual light table and darkroom for photographers</nl_BE>
   <nl>Virtuele lichttafel en doka voor fotografen</nl>
   <or>Virtual light table and darkroom for photographers</or>
   <pl>Virtual light table and darkroom for photographers</pl>
   <pt_BR>Mesa de luz virtual e câmara escura para fotógrafos</pt_BR>
   <pt>"Quarto escuro" e "mesa de luz" virtuais para fotógrafos</pt>
   <ro>Virtual light table and darkroom for photographers</ro>
   <ru>Virtual light table and darkroom for photographers</ru>
   <sk>Virtual light table and darkroom for photographers</sk>
   <sl>Virtualna temnica in fotografska miza</sl>
   <so>Virtual light table and darkroom for photographers</so>
   <sq>Tryezë drite dhe dhomë e errët virtuale për fotografë</sq>
   <sr>Virtual light table and darkroom for photographers</sr>
   <sv>Virtuellt ljusbord och mörkrum för fotografer</sv>
   <th>Virtual light table and darkroom for photographers</th>
   <tr>Fotoğrafçılar için sanal ışık masası ve karanlık oda</tr>
   <uk>Virtual light table and darkroom for photographers</uk>
   <vi>Virtual light table and darkroom for photographers</vi>
   <zh_CN>Virtual light table and darkroom for photographers</zh_CN>
   <zh_HK>Virtual light table and darkroom for photographers</zh_HK>
   <zh_TW>Virtual light table and darkroom for photographers</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
darktable
liblensfun-bin
</install_package_names>


<postinstall>
lensfun-update-data
</postinstall>


<uninstall_package_names>
darktable
liblensfun-bin
</uninstall_package_names>
</app>
