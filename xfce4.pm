<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
Xfce4 with elogind
</name>

<description>
   <am>Install xfce4 and lightdm</am>
   <ar>Install xfce4 and lightdm</ar>
   <be>Install xfce4 and lightdm</be>
   <bg>Install xfce4 and lightdm</bg>
   <bn>Install xfce4 and lightdm</bn>
   <ca>Instal·la Xfce4 i lightdm</ca>
   <cs>Install xfce4 and lightdm</cs>
   <da>Install xfce4 and lightdm</da>
   <de>Installation der Desktop-Umgebung “Xfce 4” sowie des Displaymanagers “LightDM” zur grafischen Anmeldung.</de>
   <el>Εγκαταστήστε xfce4 και lightdm</el>
   <en>Install xfce4 and lightdm</en>
   <es_ES>Instalar xfce4 y lightdm</es_ES>
   <es>Instalar xfce4 y lightdm</es>
   <et>Install xfce4 and lightdm</et>
   <eu>Install xfce4 and lightdm</eu>
   <fa>Install xfce4 and lightdm</fa>
   <fil_PH>Install xfce4 and lightdm</fil_PH>
   <fi>Asenna xfce4 sekä lightdm</fi>
   <fr_BE>Installer xfce4 et lightdm</fr_BE>
   <fr>Installer xfce4 et lightdm</fr>
   <gl_ES>Install xfce4 and lightdm</gl_ES>
   <gu>Install xfce4 and lightdm</gu>
   <he_IL>Install xfce4 and lightdm</he_IL>
   <hi>एक्सएफसीई4 व लाइट-डीएम इंस्टॉल</hi>
   <hr>Install xfce4 and lightdm</hr>
   <hu>Install xfce4 and lightdm</hu>
   <id>Install xfce4 and lightdm</id>
   <is>Install xfce4 and lightdm</is>
   <it>Installa xfce4 e lightdm</it>
   <ja>xfce 4 と lightdm をインストール</ja>
   <kk>Install xfce4 and lightdm</kk>
   <ko>Install xfce4 and lightdm</ko>
   <ku>Install xfce4 and lightdm</ku>
   <lt>Install xfce4 and lightdm</lt>
   <mk>Install xfce4 and lightdm</mk>
   <mr>Install xfce4 and lightdm</mr>
   <nb_NO>Install xfce4 and lightdm</nb_NO>
   <nb>Installer Xfce4 og lightdm</nb>
   <nl_BE>Install xfce4 and lightdm</nl_BE>
   <nl>Install xfce4 and lightdm</nl>
   <or>Install xfce4 and lightdm</or>
   <pl>Install xfce4 and lightdm</pl>
   <pt_BR>Instalação do xfce4 e do lightdm</pt_BR>
   <pt>Instalar o xfce4 e o gestor de acesso lightdm</pt>
   <ro>Install xfce4 and lightdm</ro>
   <ru>Install xfce4 and lightdm</ru>
   <sk>Install xfce4 and lightdm</sk>
   <sl>Namestitev xfce4 in lightdm</sl>
   <so>Install xfce4 and lightdm</so>
   <sq>Instaloni xfce4-n dhe lightdm-në</sq>
   <sr>Install xfce4 and lightdm</sr>
   <sv>Installera xfce4 och lightdm</sv>
   <th>Install xfce4 and lightdm</th>
   <tr>xfce4 ve lightdm kurun</tr>
   <uk>Install xfce4 and lightdm</uk>
   <vi>Install xfce4 and lightdm</vi>
   <zh_CN>Install xfce4 and lightdm</zh_CN>
   <zh_HK>Install xfce4 and lightdm</zh_HK>
   <zh_TW>Install xfce4 and lightdm</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
xfce4
xfce4-appfinder
xfce4-battery-plugin
xfce4-clipman-plugin
xfce4-cpufreq-plugin
xfce4-cpugraph-plugin
xfce4-diskperf-plugin
xfce4-fsguard-plugin
xfce4-genmon-plugin
xfce4-mailwatch-plugin
xfce4-mount-plugin
xfce4-panel
xfce4-screenshooter
xfce4-sensors-plugin
xfce4-smartbookmark-plugin
xfce4-systemload-plugin
xfce4-terminal
xfce4-verve-plugin
xfce4-wavelan-plugin
xfce4-weather-plugin
thunar
thunar-archive-plugin
thunar-media-tags-plugin
thunar-volman
gvfs
gvfs-backends
lightdm
lightdm-gtk-greeter
policykit-1
elogind
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
xfce4
xfce4-appfinder
xfce4-battery-plugin
xfce4-clipman-plugin
xfce4-cpufreq-plugin
xfce4-cpugraph-plugin
xfce4-diskperf-plugin
xfce4-fsguard-plugin
xfce4-genmon-plugin
xfce4-mailwatch-plugin
xfce4-mount-plugin
xfce4-panel
xfce4-quicklauncher-plugin
xfce4-screenshooter-plugin
xfce4-sensors-plugin
xfce4-smartbookmark-plugin
xfce4-systemload-plugin
xfce4-terminal
xfce4-verve-plugin
xfce4-wavelan-plugin
xfce4-weather-plugin
thunar
thunar-archive-plugin
thunar-media-tags-plugin
thunar-volman
gvfs
lightdm
lightdm-gtk-greeter
policykit-1
</uninstall_package_names>

</app>
