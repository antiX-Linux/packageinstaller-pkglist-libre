<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Greek_Firefox
</name>

<description>
   <am>Greek localisation of Firefox</am>
   <ar>Greek localisation of Firefox</ar>
   <be>Greek localisation of Firefox</be>
   <bg>Greek localisation of Firefox</bg>
   <bn>Greek localisation of Firefox</bn>
   <ca>Localització de Firefox en Grec</ca>
   <cs>Greek localisation of Firefox</cs>
   <da>Græsk oversættelse af Firefox</da>
   <de>Griechische Lokalisierung von Firefox</de>
   <el>Ελληνικά για το Firefox</el>
   <en>Greek localisation of Firefox</en>
   <es_ES>Localización Griega de Firefox</es_ES>
   <es>Localización Griego de Firefox</es>
   <et>Greek localisation of Firefox</et>
   <eu>Greek localisation of Firefox</eu>
   <fa>Greek localisation of Firefox</fa>
   <fil_PH>Greek localisation of Firefox</fil_PH>
   <fi>Kreikkalainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en grec pour Firefox</fr_BE>
   <fr>Localisation en grec pour Firefox</fr>
   <gl_ES>Localización do Firefox ao grego</gl_ES>
   <gu>Greek localisation of Firefox</gu>
   <he_IL>תרגומי Firefox ליוונית</he_IL>
   <hi>फायरफॉक्स का यूनानी संस्करण</hi>
   <hr>Greek localisation of Firefox</hr>
   <hu>Greek localisation of Firefox</hu>
   <id>Greek localisation of Firefox</id>
   <is>Greek localisation of Firefox</is>
   <it>Localizzazione greca di Firefox</it>
   <ja>Firefox のギリシャ語版</ja>
   <kk>Greek localisation of Firefox</kk>
   <ko>Greek localisation of Firefox</ko>
   <ku>Greek localisation of Firefox</ku>
   <lt>Greek localisation of Firefox</lt>
   <mk>Greek localisation of Firefox</mk>
   <mr>Greek localisation of Firefox</mr>
   <nb_NO>Greek localisation of Firefox</nb_NO>
   <nb>Gresk lokaltilpassing av Firefox</nb>
   <nl_BE>Greek localisation of Firefox</nl_BE>
   <nl>Griekse lokalisatie van Firefox</nl>
   <or>Greek localisation of Firefox</or>
   <pl>Grecka lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Grego Localização para o Firefox</pt_BR>
   <pt>Grego Localização para Firefox</pt>
   <ro>Greek localisation of Firefox</ro>
   <ru>Греческая локализация Firefox</ru>
   <sk>Greek localisation of Firefox</sk>
   <sl>Grška lokalizacija za Firefox</sl>
   <so>Greek localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it ESR në greqisht</sq>
   <sr>Greek localisation of Firefox</sr>
   <sv>Grekisk lokalisering för Firefox</sv>
   <th>Greek localisation ของ Firefox</th>
   <tr>Firefox'un Yunanca yerelleştirmesi</tr>
   <uk>Greek локалізація Firefox</uk>
   <vi>Greek localisation of Firefox</vi>
   <zh_CN>Firefox 希腊语语言包</zh_CN>
   <zh_HK>Greek localisation of Firefox</zh_HK>
   <zh_TW>Greek localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-el
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-el
</uninstall_package_names>
</app>
