<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
NSotho_LO_Latest_full
</name>

<description>
   <am>Northern_sotho Language Meta-Package for LibreOffice</am>
   <ar>Northern_sotho Language Meta-Package for LibreOffice</ar>
   <be>Northern_sotho Language Meta-Package for LibreOffice</be>
   <bg>Northern_sotho Language Meta-Package for LibreOffice</bg>
   <bn>Northern_sotho Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet de Northern_sotho per LibreOffice</ca>
   <cs>Northern_sotho Language Meta-Package for LibreOffice</cs>
   <da>Northern_sotho Language Meta-Package for LibreOffice</da>
   <de>Nord-Sotho Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Northern_sotho</el>
   <en>Northern_sotho Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Lenguaje Northern_sotho para LibreOffice</es_ES>
   <es>Metapaquete de Lenguaje Sotho_norte para LibreOffice</es>
   <et>Northern_sotho Language Meta-Package for LibreOffice</et>
   <eu>Northern_sotho Language Meta-Package for LibreOffice</eu>
   <fa>Northern_sotho Language Meta-Package for LibreOffice</fa>
   <fil_PH>Northern_sotho Language Meta-Package for LibreOffice</fil_PH>
   <fi>Pohjoissothonkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de la langue sotho du Nord pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de la langue sotho du Nord pour LibreOffice</fr>
   <gl_ES>Sotho septentrional Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Northern_sotho Language Meta-Package for LibreOffice</gu>
   <he_IL>Northern_sotho Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु उत्तरी_सोथो भाषा मेटा-पैकेज</hi>
   <hr>Northern_sotho Language Meta-Package for LibreOffice</hr>
   <hu>Northern_sotho Language Meta-Package for LibreOffice</hu>
   <id>Northern_sotho Language Meta-Package for LibreOffice</id>
   <is>Northern_sotho Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua sotho del nord per LibreOffice</it>
   <ja>LibreOffice用北ソト語メタパッケージ</ja>
   <kk>Northern_sotho Language Meta-Package for LibreOffice</kk>
   <ko>Northern_sotho Language Meta-Package for LibreOffice</ko>
   <ku>Northern_sotho Language Meta-Package for LibreOffice</ku>
   <lt>Northern_sotho Language Meta-Package for LibreOffice</lt>
   <mk>Northern_sotho Language Meta-Package for LibreOffice</mk>
   <mr>Northern_sotho Language Meta-Package for LibreOffice</mr>
   <nb_NO>Northern_sotho Language Meta-Package for LibreOffice</nb_NO>
   <nb>Nordsotho språkpakke for LibreOffice</nb>
   <nl_BE>Northern_sotho Language Meta-Package for LibreOffice</nl_BE>
   <nl>Noord_sotho Taal Meta-Pakket voor LibreOffice</nl>
   <or>Northern_sotho Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Northern_sotho dla LibreOffice</pl>
   <pt_BR>Sepedi Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Sotho setentrional Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Northern_sotho Language Meta-Package for LibreOffice</ro>
   <ru>Northern_sotho Language Meta-Package for LibreOffice</ru>
   <sk>Northern_sotho Language Meta-Package for LibreOffice</sk>
   <sl>Severni sotovski jezikovni meta-paket za LibreOffice</sl>
   <so>Northern_sotho Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në soto veriore</sq>
   <sr>Northern_sotho Language Meta-Package for LibreOffice</sr>
   <sv>Nordlig_sotho Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Northern_sotho สำหรับ LibreOffice</th>
   <tr>LibreOffice için Kuzey sotho Dili Üst-Paketi</tr>
   <uk>Northern_sotho Language Meta-Package for LibreOffice</uk>
   <vi>Northern_sotho Language Meta-Package for LibreOffice</vi>
   <zh_CN>Northern_sotho Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Northern_sotho Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Northern_sotho Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-nso
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-nso
</uninstall_package_names>

</app>
