<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Disks
</category>

<name>
Disk-Recovery
</name>

<description>
   <am>Partition scanner and disk recovery</am>
   <ar>Partition scanner and disk recovery</ar>
   <be>Partition scanner and disk recovery</be>
   <bg>Partition scanner and disk recovery</bg>
   <bn>Partition scanner and disk recovery</bn>
   <ca>Escanejador de particions i recuperació de discos</ca>
   <cs>Partition scanner and disk recovery</cs>
   <da>Partition scanner and disk recovery</da>
   <de>Programm zur Partitionsanalyse und Dateisystemwiederherstellung von Datenträgern</de>
   <el>Σαρωτής διαμερισμάτων και ανάκτηση δίσκου</el>
   <en>Partition scanner and disk recovery</en>
   <es_ES>Escáner de partición y Recuperación de disco</es_ES>
   <es>Escáner de partición y recuperación de disco</es>
   <et>Partition scanner and disk recovery</et>
   <eu>Partition scanner and disk recovery</eu>
   <fa>Partition scanner and disk recovery</fa>
   <fil_PH>Partition scanner and disk recovery</fil_PH>
   <fi>Kintolevyosioiden haku sekä levyn palautus</fi>
   <fr_BE>Scanner de partitions et récupération de disques</fr_BE>
   <fr>Scanner de partitions et récupération de disques</fr>
   <gl_ES>Partition scanner and disk recovery</gl_ES>
   <gu>Partition scanner and disk recovery</gu>
   <he_IL>Partition scanner and disk recovery</he_IL>
   <hi>विभाजन खोज व डिस्क निवारण</hi>
   <hr>Partition scanner and disk recovery</hr>
   <hu>Partition scanner and disk recovery</hu>
   <id>Partition scanner and disk recovery</id>
   <is>Partition scanner and disk recovery</is>
   <it>Scanner di partizioni e recupero di dischi</it>
   <ja>パーティションスキャナーとディスクリカバリー</ja>
   <kk>Partition scanner and disk recovery</kk>
   <ko>Partition scanner and disk recovery</ko>
   <ku>Partition scanner and disk recovery</ku>
   <lt>Partition scanner and disk recovery</lt>
   <mk>Partition scanner and disk recovery</mk>
   <mr>Partition scanner and disk recovery</mr>
   <nb_NO>Partition scanner and disk recovery</nb_NO>
   <nb>Partisjonsskanner og diskoppretting</nb>
   <nl_BE>Partition scanner and disk recovery</nl_BE>
   <nl>Partition scanner and disk recovery</nl>
   <or>Partition scanner and disk recovery</or>
   <pl>Partition scanner and disk recovery</pl>
   <pt_BR>Verificador de partição e recuperação de disco</pt_BR>
   <pt>Pesquisador de partições e recuperação de discos</pt>
   <ro>Partition scanner and disk recovery</ro>
   <ru>Partition scanner and disk recovery</ru>
   <sk>Partition scanner and disk recovery</sk>
   <sl>Pregledovalnik razdelkov in obnova diskov</sl>
   <so>Partition scanner and disk recovery</so>
   <sq>Skanues pjesësh dhe ringjallje disqesh</sq>
   <sr>Partition scanner and disk recovery</sr>
   <sv>Partitions-skanner och disk-återställning</sv>
   <th>Partition scanner and disk recovery</th>
   <tr>Bölüm tarayıcı ve disk kurtarma</tr>
   <uk>Partition scanner and disk recovery</uk>
   <vi>Partition scanner and disk recovery</vi>
   <zh_CN>Partition scanner and disk recovery</zh_CN>
   <zh_HK>Partition scanner and disk recovery</zh_HK>
   <zh_TW>Partition scanner and disk recovery</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
testdisk
magicrescue
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
testdisk
magicrescue
</uninstall_package_names>

</app>
