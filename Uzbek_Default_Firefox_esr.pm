<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Uzbek_Default_Firefox_esr
</name>

<description>
   <am>Uzbek localisation of Firefox ESR</am>
   <ar>Uzbek localisation of Firefox ESR</ar>
   <be>Uzbek localisation of Firefox ESR</be>
   <bg>Uzbek localisation of Firefox ESR</bg>
   <bn>Uzbek localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Uzbek</ca>
   <cs>Uzbek localisation of Firefox ESR</cs>
   <da>Uzbek localisation of Firefox ESR</da>
   <de>Usbekische Lokalisation von “Firefox ESR”</de>
   <el>Uzbek for Firefox ESR</el>
   <en>Uzbek localisation of Firefox ESR</en>
   <es_ES>Localización Uzbeko de Firefox ESR</es_ES>
   <es>Localización Uzbeko de Firefox ESR</es>
   <et>Uzbek localisation of Firefox ESR</et>
   <eu>Uzbek localisation of Firefox ESR</eu>
   <fa>Uzbek localisation of Firefox ESR</fa>
   <fil_PH>Uzbek localisation of Firefox ESR</fil_PH>
   <fi>Uzbek localisation of Firefox ESR</fi>
   <fr_BE>Localisation en langue ouzbèke pour Firefox ESR</fr_BE>
   <fr>Localisation en langue ouzbèke pour Firefox ESR</fr>
   <gl_ES>Uzbek localisation of Firefox ESR</gl_ES>
   <gu>Uzbek localisation of Firefox ESR</gu>
   <he_IL>Uzbek localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का उज़बेक संस्करण</hi>
   <hr>Uzbek localisation of Firefox ESR</hr>
   <hu>Uzbek localisation of Firefox ESR</hu>
   <id>Uzbek localisation of Firefox ESR</id>
   <is>Uzbek localisation of Firefox ESR</is>
   <it>Localizzazione uzbek di Firefox ESR</it>
   <ja>ウズベク語版 Firefox ESR</ja>
   <kk>Uzbek localisation of Firefox ESR</kk>
   <ko>Uzbek localisation of Firefox ESR</ko>
   <ku>Uzbek localisation of Firefox ESR</ku>
   <lt>Uzbek localisation of Firefox ESR</lt>
   <mk>Uzbek localisation of Firefox ESR</mk>
   <mr>Uzbek localisation of Firefox ESR</mr>
   <nb_NO>Uzbek localisation of Firefox ESR</nb_NO>
   <nb>Usbekisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Uzbek localisation of Firefox ESR</nl_BE>
   <nl>Uzbek localisation of Firefox ESR</nl>
   <or>Uzbek localisation of Firefox ESR</or>
   <pl>Uzbek localisation of Firefox ESR</pl>
   <pt_BR>Usbeque Localização para o Firefox ESR</pt_BR>
   <pt>Uzebeque Localização para Firefox ESR</pt>
   <ro>Uzbek localisation of Firefox ESR</ro>
   <ru>Uzbek localisation of Firefox ESR</ru>
   <sk>Uzbek localisation of Firefox ESR</sk>
   <sl>Uzbeške krajevne nastavitve za Firefox ESR</sl>
   <so>Uzbek localisation of Firefox ESR</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në uzbeke</sq>
   <sr>Uzbek localisation of Firefox ESR</sr>
   <sv>Uzbekisk lokalisering av Firefox ESR</sv>
   <th>Uzbek localisation of Firefox ESR</th>
   <tr>Firefox ESR Özbekçe yerelleştirmesi</tr>
   <uk>Uzbek localisation of Firefox ESR</uk>
   <vi>Uzbek localisation of Firefox ESR</vi>
   <zh_CN>Uzbek localisation of Firefox ESR</zh_CN>
   <zh_HK>Uzbek localisation of Firefox ESR</zh_HK>
   <zh_TW>Uzbek localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-uz
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-uz
</uninstall_package_names>

</app>
