<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Danish
</name>

<description>
   <am>Danish dictionary for hunspell</am>
   <ar>Danish dictionary for hunspell</ar>
   <be>Danish dictionary for hunspell</be>
   <bg>Danish dictionary for hunspell</bg>
   <bn>Danish dictionary for hunspell</bn>
   <ca>Diccionari Danès per hunspell</ca>
   <cs>Danish dictionary for hunspell</cs>
   <da>Danish dictionary for hunspell</da>
   <de>Dänisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό στα Δανικά για hunspell</el>
   <en>Danish dictionary for hunspell</en>
   <es_ES>Diccionario Danes para hunspell</es_ES>
   <es>Diccionario Danés para hunspell</es>
   <et>Danish dictionary for hunspell</et>
   <eu>Danish dictionary for hunspell</eu>
   <fa>Danish dictionary for hunspell</fa>
   <fil_PH>Danish dictionary for hunspell</fil_PH>
   <fi>Danish dictionary for hunspell</fi>
   <fr_BE>Danois dictionnaire pour hunspell</fr_BE>
   <fr>Danois dictionnaire pour hunspell</fr>
   <gl_ES>Danish dictionary for hunspell</gl_ES>
   <gu>Danish dictionary for hunspell</gu>
   <he_IL>Danish dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु डेनिश शब्दकोष</hi>
   <hr>Danish dictionary for hunspell</hr>
   <hu>Danish dictionary for hunspell</hu>
   <id>Danish dictionary for hunspell</id>
   <is>Danish dictionary for hunspell</is>
   <it>Dizionario danese per hunspell</it>
   <ja>Hunspell 用デンマーク語辞書</ja>
   <kk>Danish dictionary for hunspell</kk>
   <ko>Danish dictionary for hunspell</ko>
   <ku>Danish dictionary for hunspell</ku>
   <lt>Danish dictionary for hunspell</lt>
   <mk>Danish dictionary for hunspell</mk>
   <mr>Danish dictionary for hunspell</mr>
   <nb_NO>Danish dictionary for hunspell</nb_NO>
   <nb>Dansk ordliste for hunspell</nb>
   <nl_BE>Danish dictionary for hunspell</nl_BE>
   <nl>Deens woordenboek voor hunspell</nl>
   <or>Danish dictionary for hunspell</or>
   <pl>Danish dictionary for hunspell</pl>
   <pt_BR>Dicionário Dinamarquês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Dinamarquês para hunspell</pt>
   <ro>Danish dictionary for hunspell</ro>
   <ru>Danish dictionary for hunspell</ru>
   <sk>Danish dictionary for hunspell</sk>
   <sl>Danski slovar za hunspell</sl>
   <so>Danish dictionary for hunspell</so>
   <sq>Fjalor danisht për hunspell</sq>
   <sr>Danish dictionary for hunspell</sr>
   <sv>Dansk ordbok för hunspell</sv>
   <th>Danish dictionary for hunspell</th>
   <tr>Hunspell için Danca sözlük</tr>
   <uk>Danish dictionary for hunspell</uk>
   <vi>Danish dictionary for hunspell</vi>
   <zh_CN>Danish dictionary for hunspell</zh_CN>
   <zh_HK>Danish dictionary for hunspell</zh_HK>
   <zh_TW>Danish dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-da
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-da
</uninstall_package_names>

</app>
