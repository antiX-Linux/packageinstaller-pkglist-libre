<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Graphics
</category>

<name>
Zim
</name>

<description>
   <am>Graphical text editor used to maintain a collection of wiki pages</am>
   <ar>Graphical text editor used to maintain a collection of wiki pages</ar>
   <be>Graphical text editor used to maintain a collection of wiki pages</be>
   <bg>Graphical text editor used to maintain a collection of wiki pages</bg>
   <bn>Graphical text editor used to maintain a collection of wiki pages</bn>
   <ca>Editor de text gràfic usat per mantenir una col·lecció de pàgines web</ca>
   <cs>Graphical text editor used to maintain a collection of wiki pages</cs>
   <da>Graphical text editor used to maintain a collection of wiki pages</da>
   <de>Grafische Textbearbeitung zur Pflege einer Sammlung von Wiki-Seiten</de>
   <el>Γραφικός επεξεργαστής κειμένου χρησιμοποιείται για τη συντήρηση μιας συλλογής σελίδων wiki</el>
   <en>Graphical text editor used to maintain a collection of wiki pages</en>
   <es_ES>Editor de texto gráfico utilizado para mantener una colección de páginas wiki</es_ES>
   <es>Editor de texto gráfico utilizado para mantener una colección de páginas wiki</es>
   <et>Graphical text editor used to maintain a collection of wiki pages</et>
   <eu>Graphical text editor used to maintain a collection of wiki pages</eu>
   <fa>Graphical text editor used to maintain a collection of wiki pages</fa>
   <fil_PH>Graphical text editor used to maintain a collection of wiki pages</fil_PH>
   <fi>Graphical text editor used to maintain a collection of wiki pages</fi>
   <fr_BE>Éditeur de texte graphique utilisé pour gérer une collection de pages wiki</fr_BE>
   <fr>Éditeur de texte graphique utilisé pour gérer une collection de pages wiki</fr>
   <gl_ES>Graphical text editor used to maintain a collection of wiki pages</gl_ES>
   <gu>Graphical text editor used to maintain a collection of wiki pages</gu>
   <he_IL>Graphical text editor used to maintain a collection of wiki pages</he_IL>
   <hi>विकी पृष्ठों के संग्रह के प्रबंधन हेतु ग्राफ़िकल लेख संपादक</hi>
   <hr>Graphical text editor used to maintain a collection of wiki pages</hr>
   <hu>Graphical text editor used to maintain a collection of wiki pages</hu>
   <id>Graphical text editor used to maintain a collection of wiki pages</id>
   <is>Graphical text editor used to maintain a collection of wiki pages</is>
   <it>Editor grafico di testo usato per mantenere un elenco di pagine wiki</it>
   <ja>Wikiページ管理用のグラフィカルなテキストエディタ</ja>
   <kk>Graphical text editor used to maintain a collection of wiki pages</kk>
   <ko>Graphical text editor used to maintain a collection of wiki pages</ko>
   <ku>Graphical text editor used to maintain a collection of wiki pages</ku>
   <lt>Graphical text editor used to maintain a collection of wiki pages</lt>
   <mk>Graphical text editor used to maintain a collection of wiki pages</mk>
   <mr>Graphical text editor used to maintain a collection of wiki pages</mr>
   <nb_NO>Graphical text editor used to maintain a collection of wiki pages</nb_NO>
   <nb>Grafisk tekstredigering med behandling av wiki-sider</nb>
   <nl_BE>Graphical text editor used to maintain a collection of wiki pages</nl_BE>
   <nl>Grafische teksteditor die wordt gebruikt om een verzameling wikipagina's bij te houden</nl>
   <or>Graphical text editor used to maintain a collection of wiki pages</or>
   <pl>Graphical text editor used to maintain a collection of wiki pages</pl>
   <pt_BR>Editor de texto gráfico usado para manter uma coleção de páginas wiki</pt_BR>
   <pt>Editor em modo gráfico usado para manter uma coleção de páginas wiki</pt>
   <ro>Graphical text editor used to maintain a collection of wiki pages</ro>
   <ru>Graphical text editor used to maintain a collection of wiki pages</ru>
   <sk>Graphical text editor used to maintain a collection of wiki pages</sk>
   <sl>Grafični urejevalnik besedila uporabljen za vzdrževanje zbirke wiki strani</sl>
   <so>Graphical text editor used to maintain a collection of wiki pages</so>
   <sq>Përpunues grafik tekstesh, i përdorur për të mirëmbajtur një koleksion faqesh wiki</sq>
   <sr>Graphical text editor used to maintain a collection of wiki pages</sr>
   <sv>Grafisk textredigerare använd för att sköta en samling wiki-sidor</sv>
   <th>Graphical text editor used to maintain a collection of wiki pages</th>
   <tr>Wiki sayfalarının toplama ve bakımı için kullanılan grafiksel metin düzenleyici</tr>
   <uk>Graphical text editor used to maintain a collection of wiki pages</uk>
   <vi>Graphical text editor used to maintain a collection of wiki pages</vi>
   <zh_CN>Graphical text editor used to maintain a collection of wiki pages</zh_CN>
   <zh_HK>Graphical text editor used to maintain a collection of wiki pages</zh_HK>
   <zh_TW>Graphical text editor used to maintain a collection of wiki pages</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
zim
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
zim
</uninstall_package_names>
</app>
