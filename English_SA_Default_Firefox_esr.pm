<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English_SA_Default_Firefox_esr
</name>

<description>
   <am>SA English localisation of Firefox ESR</am>
   <ar>SA English localisation of Firefox ESR</ar>
   <be>SA English localisation of Firefox ESR</be>
   <bg>SA English localisation of Firefox ESR</bg>
   <bn>SA English localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Anglès (SA)</ca>
   <cs>SA English localisation of Firefox ESR</cs>
   <da>SA English localisation of Firefox ESR</da>
   <de>Englische Lokalisation für Südafrika von “Firefox ESR”</de>
   <el>Αγγλικά (Νότια Αφρική) για Firefox ESR</el>
   <en>SA English localisation of Firefox ESR</en>
   <es_ES>Localización Inglés (SA) de Firefox ESR</es_ES>
   <es>Localización Inglés (SA) de Firefox</es>
   <et>SA English localisation of Firefox ESR</et>
   <eu>SA English localisation of Firefox ESR</eu>
   <fa>SA English localisation of Firefox ESR</fa>
   <fil_PH>SA English localisation of Firefox ESR</fil_PH>
   <fi>SA English localisation of Firefox ESR</fi>
   <fr_BE>Localisation en anglais (Afrique du Sud) pour Firefox ESR</fr_BE>
   <fr>Localisation en anglais (Afrique du Sud) pour Firefox ESR</fr>
   <gl_ES>SA English localisation of Firefox ESR</gl_ES>
   <gu>SA English localisation of Firefox ESR</gu>
   <he_IL>SA English localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का दक्षिण अफ्रीकी अंग्रेज़ी संस्करण</hi>
   <hr>SA English localisation of Firefox ESR</hr>
   <hu>SA English localisation of Firefox ESR</hu>
   <id>SA English localisation of Firefox ESR</id>
   <is>SA English localisation of Firefox ESR</is>
   <it>Localizzazione in inglese SA di Firefox ESR</it>
   <ja>南アフリカ英語版 Firefox ESR</ja>
   <kk>SA English localisation of Firefox ESR</kk>
   <ko>SA English localisation of Firefox ESR</ko>
   <ku>SA English localisation of Firefox ESR</ku>
   <lt>SA English localisation of Firefox ESR</lt>
   <mk>SA English localisation of Firefox ESR</mk>
   <mr>SA English localisation of Firefox ESR</mr>
   <nb_NO>SA English localisation of Firefox ESR</nb_NO>
   <nb>Sør-afrikansk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>SA English localisation of Firefox ESR</nl_BE>
   <nl>SA Engelse lokalisatie van Firefox ESR</nl>
   <or>SA English localisation of Firefox ESR</or>
   <pl>SA English localisation of Firefox ESR</pl>
   <pt_BR>Inglês SA Localização para o Firefox ESR</pt_BR>
   <pt>Inglês SA Localização para Firefox ESR</pt>
   <ro>SA English localisation of Firefox ESR</ro>
   <ru>SA English localisation of Firefox ESR</ru>
   <sk>SA English localisation of Firefox ESR</sk>
   <sl>Južnoafriške angleške krajevne nastavitve za Firefox ESR</sl>
   <so>SA English localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në anglishte afrikanojugore</sq>
   <sr>SA English localisation of Firefox ESR</sr>
   <sv>SA Engelsk lokalisering av Firefox ESR</sv>
   <th>SA English localisation of Firefox ESR</th>
   <tr>Firefox ESR İngilizce (Güney Afrika) yerelleştirmesi</tr>
   <uk>SA English localisation of Firefox ESR</uk>
   <vi>SA English localisation of Firefox ESR</vi>
   <zh_CN>SA English localisation of Firefox ESR</zh_CN>
   <zh_HK>SA English localisation of Firefox ESR</zh_HK>
   <zh_TW>SA English localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-en-za
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-en-za
</uninstall_package_names>

</app>
