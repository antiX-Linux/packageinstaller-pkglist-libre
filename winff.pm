<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Media Converter
</category>

<name>
WinFF
</name>

<description>
   <am>graphical video and audio batch converter</am>
   <ar>graphical video and audio batch converter</ar>
   <be>graphical video and audio batch converter</be>
   <bg>graphical video and audio batch converter</bg>
   <bn>graphical video and audio batch converter</bn>
   <ca>Convertidor gràfic de vídeo i àudio per lots</ca>
   <cs>graphical video and audio batch converter</cs>
   <da>grafisk batchkonvertering af video og lyd</da>
   <de>Grafischer Video- und Audio-Batch-Konverter</de>
   <el>Γραφικός μετατροπέας βίντεο και ήχου</el>
   <en>graphical video and audio batch converter</en>
   <es_ES>Conversor gráfico de formatos de audio y video</es_ES>
   <es>Conversor gráfico de formatos de audio y video</es>
   <et>graphical video and audio batch converter</et>
   <eu>graphical video and audio batch converter</eu>
   <fa>graphical video and audio batch converter</fa>
   <fil_PH>graphical video and audio batch converter</fil_PH>
   <fi>graafinen video- ja audiomuunnin</fi>
   <fr_BE>Convertisseur vidéo et audio</fr_BE>
   <fr>Convertisseur vidéo et audio</fr>
   <gl_ES>conversor gráfico de vídeo e audio</gl_ES>
   <gu>graphical video and audio batch converter</gu>
   <he_IL>graphical video and audio batch converter</he_IL>
   <hi>एकाधिक वीडियो व ऑडियो सामग्री हेतु ग्राफ़िकल रूपांतरण साधन</hi>
   <hr>graphical video and audio batch converter</hr>
   <hu>graphical video and audio batch converter</hu>
   <id>graphical video and audio batch converter</id>
   <is>graphical video and audio batch converter</is>
   <it>convertitore grafico batch per video e audio che usa FFmpeg o avconv</it>
   <ja>グラフィカルなビデオとオーディオのバッチコンバータ</ja>
   <kk>graphical video and audio batch converter</kk>
   <ko>graphical video and audio batch converter</ko>
   <ku>graphical video and audio batch converter</ku>
   <lt>graphical video and audio batch converter</lt>
   <mk>graphical video and audio batch converter</mk>
   <mr>graphical video and audio batch converter</mr>
   <nb_NO>graphical video and audio batch converter</nb_NO>
   <nb>grafisk verktøy for konvertering av video- og lydfiler</nb>
   <nl_BE>graphical video and audio batch converter</nl_BE>
   <nl>grafische video- en audiobatchomzetter</nl>
   <or>graphical video and audio batch converter</or>
   <pl>graficzny konwerter wsadowy wideo i audio</pl>
   <pt_BR>Conversor gráfico de áudio e vídeo (conversão em lotes)</pt_BR>
   <pt>Conversor gráfico de vídeo e áudio (conversão em lotes)</pt>
   <ro>graphical video and audio batch converter</ro>
   <ru>Пакетный конвертер видео и аудио</ru>
   <sk>graphical video and audio batch converter</sk>
   <sl>Grafični video in zvočni pretvornik za več datotek</sl>
   <so>graphical video and audio batch converter</so>
   <sq>Shndërrues grafik videosh dhe audiosh në masë</sq>
   <sr>graphical video and audio batch converter</sr>
   <sv>grafisk video och ljud mass-konverterare</sv>
   <th>graphical video and audio batch converter</th>
   <tr>grafiksel video ve ses toplu dönüştürücü</tr>
   <uk>graphical video and audio batch converter</uk>
   <vi>graphical video and audio batch converter</vi>
   <zh_CN>graphical video and audio batch converter</zh_CN>
   <zh_HK>graphical video and audio batch converter</zh_HK>
   <zh_TW>graphical video and audio batch converter</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
winff
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
winff
</uninstall_package_names>
</app>
