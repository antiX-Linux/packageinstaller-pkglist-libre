<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
Network-Samba
</name>

<description>
   <am>Samba and pyneighborhood</am>
   <ar>Samba and pyneighborhood</ar>
   <be>Samba and pyneighborhood</be>
   <bg>Samba and pyneighborhood</bg>
   <bn>Samba and pyneighborhood</bn>
   <ca>Samba i pyneighborhood</ca>
   <cs>Samba and pyneighborhood</cs>
   <da>Samba and pyneighborhood</da>
   <de>Samba und Sambaclient “pyneighborhood” für “MS-Windows” Datei- und Druckdienste</de>
   <el>Samba και pyneighborhood</el>
   <en>Samba and pyneighborhood</en>
   <es_ES>Samba y pyneighborhood</es_ES>
   <es>Samba y pyneighborhood</es>
   <et>Samba and pyneighborhood</et>
   <eu>Samba and pyneighborhood</eu>
   <fa>Samba and pyneighborhood</fa>
   <fil_PH>Samba and pyneighborhood</fil_PH>
   <fi>Samba sekä pyneighborhood</fi>
   <fr_BE>Samba et interface graphique PyNeighborhood</fr_BE>
   <fr>Samba et interface graphique PyNeighborhood</fr>
   <gl_ES>Samba and pyneighborhood</gl_ES>
   <gu>Samba and pyneighborhood</gu>
   <he_IL>Samba and pyneighborhood</he_IL>
   <hi>Samba व pyneighborhood</hi>
   <hr>Samba and pyneighborhood</hr>
   <hu>Samba and pyneighborhood</hu>
   <id>Samba and pyneighborhood</id>
   <is>Samba and pyneighborhood</is>
   <it>Samba e pyneighborhood</it>
   <ja>サンバと pyNeighborhood</ja>
   <kk>Samba and pyneighborhood</kk>
   <ko>Samba and pyneighborhood</ko>
   <ku>Samba and pyneighborhood</ku>
   <lt>Samba and pyneighborhood</lt>
   <mk>Samba and pyneighborhood</mk>
   <mr>Samba and pyneighborhood</mr>
   <nb_NO>Samba and pyneighborhood</nb_NO>
   <nb>Samba og pyneighborhood</nb>
   <nl_BE>Samba and pyneighborhood</nl_BE>
   <nl>Samba and pyneighborhood</nl>
   <or>Samba and pyneighborhood</or>
   <pl>Samba and pyneighborhood</pl>
   <pt_BR>Samba e pyneighborhood</pt_BR>
   <pt>Samba e pyneighborhood</pt>
   <ro>Samba and pyneighborhood</ro>
   <ru>Samba and pyneighborhood</ru>
   <sk>Samba and pyneighborhood</sk>
   <sl>Samba in pyneighborhood</sl>
   <so>Samba and pyneighborhood</so>
   <sq>Samba dhe pyneighborhood</sq>
   <sr>Samba and pyneighborhood</sr>
   <sv>Samba och pyneighborhood</sv>
   <th>Samba and pyneighborhood</th>
   <tr>Samba ve pyneighborhood</tr>
   <uk>Samba and pyneighborhood</uk>
   <vi>Samba and pyneighborhood</vi>
   <zh_CN>Samba and pyneighborhood</zh_CN>
   <zh_HK>Samba and pyneighborhood</zh_HK>
   <zh_TW>Samba and pyneighborhood</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
samba
pyneighborhood
smbclient
cifs-utils
</install_package_names>

<postinstall>
echo "---- in postprocessing ----"
echo "Removing deb-multimedia repository from /etc/apt/sources.list.d/debian.list"
sed -i -r '/http:.*multimedia\.org.* testing/ s/^([^#])/#\1/' /etc/apt/sources.list.d/debian.list
echo "---- postprocessing done----"
</postinstall>

<uninstall_package_names>
samba
pyneighborhood
smbclient
cifs-utils
</uninstall_package_names>

</app>
