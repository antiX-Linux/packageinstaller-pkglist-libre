<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Thai
</name>

<description>
   <am>Thai dictionary for hunspell</am>
   <ar>Thai dictionary for hunspell</ar>
   <be>Thai dictionary for hunspell</be>
   <bg>Thai dictionary for hunspell</bg>
   <bn>Thai dictionary for hunspell</bn>
   <ca>Diccionari Thai per hunspell</ca>
   <cs>Thai dictionary for hunspell</cs>
   <da>Thai dictionary for hunspell</da>
   <de>Thailändisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Ταϊλάνδης για hunspell</el>
   <en>Thai dictionary for hunspell</en>
   <es_ES>Diccionario Tailandés para hunspell</es_ES>
   <es>Diccionario Tailandés para hunspell</es>
   <et>Thai dictionary for hunspell</et>
   <eu>Thai dictionary for hunspell</eu>
   <fa>Thai dictionary for hunspell</fa>
   <fil_PH>Thai dictionary for hunspell</fil_PH>
   <fi>Thai dictionary for hunspell</fi>
   <fr_BE>Thaïlandais dictionnaire pour hunspell</fr_BE>
   <fr>Thaïlandais dictionnaire pour hunspell</fr>
   <gl_ES>Thai dictionary for hunspell</gl_ES>
   <gu>Thai dictionary for hunspell</gu>
   <he_IL>Thai dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु थाई शब्दकोष</hi>
   <hr>Thai dictionary for hunspell</hr>
   <hu>Thai dictionary for hunspell</hu>
   <id>Thai dictionary for hunspell</id>
   <is>Thai dictionary for hunspell</is>
   <it>Dizionario thai per hunspell</it>
   <ja>Hunspell 用タイ語辞書</ja>
   <kk>Thai dictionary for hunspell</kk>
   <ko>Thai dictionary for hunspell</ko>
   <ku>Thai dictionary for hunspell</ku>
   <lt>Thai dictionary for hunspell</lt>
   <mk>Thai dictionary for hunspell</mk>
   <mr>Thai dictionary for hunspell</mr>
   <nb_NO>Thai dictionary for hunspell</nb_NO>
   <nb>Thai ordliste for hunspell</nb>
   <nl_BE>Thai dictionary for hunspell</nl_BE>
   <nl>Thai dictionary for hunspell</nl>
   <or>Thai dictionary for hunspell</or>
   <pl>Thai dictionary for hunspell</pl>
   <pt_BR>Dicionário Tailandês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Tailandês para hunspell</pt>
   <ro>Thai dictionary for hunspell</ro>
   <ru>Thai dictionary for hunspell</ru>
   <sk>Thai dictionary for hunspell</sk>
   <sl>Tajski slovar za hunspell</sl>
   <so>Thai dictionary for hunspell</so>
   <sq>Fjalor tajlandisht për hunspell</sq>
   <sr>Thai dictionary for hunspell</sr>
   <sv>Thai ordbok för hunspell</sv>
   <th>Thai dictionary for hunspell</th>
   <tr>Hunspell için Tayca sözlük</tr>
   <uk>Thai dictionary for hunspell</uk>
   <vi>Thai dictionary for hunspell</vi>
   <zh_CN>Thai dictionary for hunspell</zh_CN>
   <zh_HK>Thai dictionary for hunspell</zh_HK>
   <zh_TW>Thai dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-th
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-th
</uninstall_package_names>

</app>
