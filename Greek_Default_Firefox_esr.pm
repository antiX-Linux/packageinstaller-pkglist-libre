<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Greek_Default_Firefox_esr
</name>

<description>
   <am>Greek localisation of Firefox ESR</am>
   <ar>Greek localisation of Firefox ESR</ar>
   <be>Greek localisation of Firefox ESR</be>
   <bg>Greek localisation of Firefox ESR</bg>
   <bn>Greek localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Grec</ca>
   <cs>Greek localisation of Firefox ESR</cs>
   <da>Greek localisation of Firefox ESR</da>
   <de>Griechische Lokalisation von “Firefox ESR”</de>
   <el>Ελληνικά για Firefox ESR</el>
   <en>Greek localisation of Firefox ESR</en>
   <es_ES>Localización Griego de Firefox ESR</es_ES>
   <es>Localización Griego de Firefox ESR</es>
   <et>Greek localisation of Firefox ESR</et>
   <eu>Greek localisation of Firefox ESR</eu>
   <fa>Greek localisation of Firefox ESR</fa>
   <fil_PH>Greek localisation of Firefox ESR</fil_PH>
   <fi>Greek localisation of Firefox ESR</fi>
   <fr_BE>Localisation en grec pour Firefox ESR</fr_BE>
   <fr>Localisation en grec pour Firefox ESR</fr>
   <gl_ES>Greek localisation of Firefox ESR</gl_ES>
   <gu>Greek localisation of Firefox ESR</gu>
   <he_IL>Greek localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का यूनानी संस्करण</hi>
   <hr>Greek localisation of Firefox ESR</hr>
   <hu>Greek localisation of Firefox ESR</hu>
   <id>Greek localisation of Firefox ESR</id>
   <is>Greek localisation of Firefox ESR</is>
   <it>Localizzazione greca di Firefox ESR</it>
   <ja>ギリシャ語版 Firefox ESR</ja>
   <kk>Greek localisation of Firefox ESR</kk>
   <ko>Greek localisation of Firefox ESR</ko>
   <ku>Greek localisation of Firefox ESR</ku>
   <lt>Greek localisation of Firefox ESR</lt>
   <mk>Greek localisation of Firefox ESR</mk>
   <mr>Greek localisation of Firefox ESR</mr>
   <nb_NO>Greek localisation of Firefox ESR</nb_NO>
   <nb>Gresk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Greek localisation of Firefox ESR</nl_BE>
   <nl>Griekse lokalisatie van Firefox ESR</nl>
   <or>Greek localisation of Firefox ESR</or>
   <pl>Greek localisation of Firefox ESR</pl>
   <pt_BR>Grego Localização para o Firefox ESR</pt_BR>
   <pt>Grego Localização para Firefox ESR</pt>
   <ro>Greek localisation of Firefox ESR</ro>
   <ru>Greek localisation of Firefox ESR</ru>
   <sk>Greek localisation of Firefox ESR</sk>
   <sl>Grške krajevne nastavitve za Firefox ESR</sl>
   <so>Greek localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në greqisht</sq>
   <sr>Greek localisation of Firefox ESR</sr>
   <sv>Grekisk lokalisering av Firefox ESR</sv>
   <th>Greek localisation of Firefox ESR</th>
   <tr>Firefox ESR Yunanca yerelleştirmesi</tr>
   <uk>Greek localisation of Firefox ESR</uk>
   <vi>Greek localisation of Firefox ESR</vi>
   <zh_CN>Greek localisation of Firefox ESR</zh_CN>
   <zh_HK>Greek localisation of Firefox ESR</zh_HK>
   <zh_TW>Greek localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-el
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-el
</uninstall_package_names>

</app>
