<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Slovenian_Default_Firefox_esr
</name>

<description>
   <am>Slovenian localisation of Firefox ESR</am>
   <ar>Slovenian localisation of Firefox ESR</ar>
   <be>Slovenian localisation of Firefox ESR</be>
   <bg>Slovenian localisation of Firefox ESR</bg>
   <bn>Slovenian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Eslovè</ca>
   <cs>Slovenian localisation of Firefox ESR</cs>
   <da>Slovenian localisation of Firefox ESR</da>
   <de>Slowenische Lokalisation von “Firefox ESR”</de>
   <el>Σλοβενικά για Firefox ESR</el>
   <en>Slovenian localisation of Firefox ESR</en>
   <es_ES>Localización Esloveno de Firefox ESR</es_ES>
   <es>Localización Esloveno de Firefox ESR</es>
   <et>Slovenian localisation of Firefox ESR</et>
   <eu>Slovenian localisation of Firefox ESR</eu>
   <fa>Slovenian localisation of Firefox ESR</fa>
   <fil_PH>Slovenian localisation of Firefox ESR</fil_PH>
   <fi>Slovenian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en slovène pour Firefox ESR</fr_BE>
   <fr>Localisation en slovène pour Firefox ESR</fr>
   <gl_ES>Slovenian localisation of Firefox ESR</gl_ES>
   <gu>Slovenian localisation of Firefox ESR</gu>
   <he_IL>Slovenian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का स्लोवेनियाई संस्करण</hi>
   <hr>Slovenian localisation of Firefox ESR</hr>
   <hu>Slovenian localisation of Firefox ESR</hu>
   <id>Slovenian localisation of Firefox ESR</id>
   <is>Slovenian localisation of Firefox ESR</is>
   <it>Localizzazione slovenian di Firefox ESR</it>
   <ja>スロベニア語版 Firefox ESR</ja>
   <kk>Slovenian localisation of Firefox ESR</kk>
   <ko>Slovenian localisation of Firefox ESR</ko>
   <ku>Slovenian localisation of Firefox ESR</ku>
   <lt>Slovenian localisation of Firefox ESR</lt>
   <mk>Slovenian localisation of Firefox ESR</mk>
   <mr>Slovenian localisation of Firefox ESR</mr>
   <nb_NO>Slovenian localisation of Firefox ESR</nb_NO>
   <nb>Slovensk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Slovenian localisation of Firefox ESR</nl_BE>
   <nl>Slovenian localisation of Firefox ESR</nl>
   <or>Slovenian localisation of Firefox ESR</or>
   <pl>Slovenian localisation of Firefox ESR</pl>
   <pt_BR>Esloveno Localização para o Firefox ESR</pt_BR>
   <pt>Esloveno Localização para Firefox ESR</pt>
   <ro>Slovenian localisation of Firefox ESR</ro>
   <ru>Slovenian localisation of Firefox ESR</ru>
   <sk>Slovenian localisation of Firefox ESR</sk>
   <sl>Slovenske krajevne nastavitve za Firefox ESR</sl>
   <so>Slovenian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në sllovenisht</sq>
   <sr>Slovenian localisation of Firefox ESR</sr>
   <sv>Slovensk lokalisering av Firefox ESR</sv>
   <th>Slovenian localisation of Firefox ESR</th>
   <tr>Firefox ESR Slovence yerelleştirmesi</tr>
   <uk>Slovenian localisation of Firefox ESR</uk>
   <vi>Slovenian localisation of Firefox ESR</vi>
   <zh_CN>Slovenian localisation of Firefox ESR</zh_CN>
   <zh_HK>Slovenian localisation of Firefox ESR</zh_HK>
   <zh_TW>Slovenian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-sl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-sl
</uninstall_package_names>

</app>
