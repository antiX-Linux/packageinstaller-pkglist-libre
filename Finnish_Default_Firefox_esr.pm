<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Finnish_Default_Firefox_esr
</name>

<description>
   <am>Finnish localisation of Firefox ESR</am>
   <ar>Finnish localisation of Firefox ESR</ar>
   <be>Finnish localisation of Firefox ESR</be>
   <bg>Finnish localisation of Firefox ESR</bg>
   <bn>Finnish localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Finès</ca>
   <cs>Finnish localisation of Firefox ESR</cs>
   <da>Finnish localisation of Firefox ESR</da>
   <de>Finnische Lokalisation von “Firefox ESR”</de>
   <el>Φινλανδικά για το Firefox ESR</el>
   <en>Finnish localisation of Firefox ESR</en>
   <es_ES>Localización Finlandesa de Firefox ESR</es_ES>
   <es>Localización Finés de Firefox ESR</es>
   <et>Finnish localisation of Firefox ESR</et>
   <eu>Finnish localisation of Firefox ESR</eu>
   <fa>Finnish localisation of Firefox ESR</fa>
   <fil_PH>Finnish localisation of Firefox ESR</fil_PH>
   <fi>Finnish localisation of Firefox ESR</fi>
   <fr_BE>Localisation en finnois pour Firefox ESR</fr_BE>
   <fr>Localisation en finnois pour Firefox ESR</fr>
   <gl_ES>Finnish localisation of Firefox ESR</gl_ES>
   <gu>Finnish localisation of Firefox ESR</gu>
   <he_IL>Finnish localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का फिन्निश संस्करण</hi>
   <hr>Finnish localisation of Firefox ESR</hr>
   <hu>Finnish localisation of Firefox ESR</hu>
   <id>Finnish localisation of Firefox ESR</id>
   <is>Finnish localisation of Firefox ESR</is>
   <it>Localizzazione finlandese di Firefox ESR</it>
   <ja>フィンランド語版 Firefox ESR</ja>
   <kk>Finnish localisation of Firefox ESR</kk>
   <ko>Finnish localisation of Firefox ESR</ko>
   <ku>Finnish localisation of Firefox ESR</ku>
   <lt>Finnish localisation of Firefox ESR</lt>
   <mk>Finnish localisation of Firefox ESR</mk>
   <mr>Finnish localisation of Firefox ESR</mr>
   <nb_NO>Finnish localisation of Firefox ESR</nb_NO>
   <nb>Finsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Finnish localisation of Firefox ESR</nl_BE>
   <nl>Finse lokalisatie van Firefox ESR</nl>
   <or>Finnish localisation of Firefox ESR</or>
   <pl>Finnish localisation of Firefox ESR</pl>
   <pt_BR>Finlandês Localização para o Firefox ESR</pt_BR>
   <pt>Finlandês Localização para Firefox ESR</pt>
   <ro>Finnish localisation of Firefox ESR</ro>
   <ru>Finnish localisation of Firefox ESR</ru>
   <sk>Finnish localisation of Firefox ESR</sk>
   <sl>Finske krajevne nastavitve za Firefox ESR</sl>
   <so>Finnish localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në finlandisht</sq>
   <sr>Finnish localisation of Firefox ESR</sr>
   <sv>Finsk lokalisering av Firefox ESR</sv>
   <th>Finnish localisation of Firefox ESR</th>
   <tr>Firefox ESR Fince yerelleştirmesi</tr>
   <uk>Finnish localisation of Firefox ESR</uk>
   <vi>Finnish localisation of Firefox ESR</vi>
   <zh_CN>Finnish localisation of Firefox ESR</zh_CN>
   <zh_HK>Finnish localisation of Firefox ESR</zh_HK>
   <zh_TW>Finnish localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-fi
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-fi
</uninstall_package_names>

</app>
