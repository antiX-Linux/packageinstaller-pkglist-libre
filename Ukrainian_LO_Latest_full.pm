<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Ukrainian_LO_Latest_full
</name>

<description>
   <am>Ukrainian localisation of LibreOffice</am>
   <ar>Ukrainian localisation of LibreOffice</ar>
   <be>Ukrainian localisation of LibreOffice</be>
   <bg>Ukrainian localisation of LibreOffice</bg>
   <bn>Ukrainian localisation of LibreOffice</bn>
   <ca>Localització de LibreOffice en Ucrainès</ca>
   <cs>Ukrainian localisation of LibreOffice</cs>
   <da>Ukrainsk oversættelse af LibreOffice</da>
   <de>Ukrainische Lokalisierung von LibreOffice</de>
   <el>LibreOffice στα Ουκρανικά</el>
   <en>Ukrainian localisation of LibreOffice</en>
   <es_ES>Localización Ucraniana de LibreOffice</es_ES>
   <es>Localización Ucraniano de LibreOffice</es>
   <et>Ukrainian localisation of LibreOffice</et>
   <eu>Ukrainian localisation of LibreOffice</eu>
   <fa>Ukrainian localisation of LibreOffice</fa>
   <fil_PH>Ukrainian localisation of LibreOffice</fil_PH>
   <fi>Ukrainalainen kotoistus LibreOffice:lle</fi>
   <fr_BE>Localisation en ukrainien pour LibreOffice</fr_BE>
   <fr>Localisation en ukrainien pour LibreOffice</fr>
   <gl_ES>Localización do LibreOffice ao ucraniano</gl_ES>
   <gu>Ukrainian localisation of LibreOffice</gu>
   <he_IL>תרגומי LibreOffice לאוקראינית</he_IL>
   <hi>लिब्रे-ऑफिस का यूक्रेनी संस्करण</hi>
   <hr>Ukrainian localisation of LibreOffice</hr>
   <hu>Ukrainian localisation of LibreOffice</hu>
   <id>Ukrainian localisation of LibreOffice</id>
   <is>Ukrainian localisation of LibreOffice</is>
   <it>Localizzazione ucraina di LibreOffice</it>
   <ja>LibreOffice のウクライナ語版</ja>
   <kk>Ukrainian localisation of LibreOffice</kk>
   <ko>Ukrainian localisation of LibreOffice</ko>
   <ku>Ukrainian localisation of LibreOffice</ku>
   <lt>Ukrainian localisation of LibreOffice</lt>
   <mk>Ukrainian localisation of LibreOffice</mk>
   <mr>Ukrainian localisation of LibreOffice</mr>
   <nb_NO>Ukrainian localisation of LibreOffice</nb_NO>
   <nb>Ukrainsk lokaltilpassing av LibreOffice</nb>
   <nl_BE>Ukrainian localisation of LibreOffice</nl_BE>
   <nl>Oekraïense lokalisatie van LibreOffice</nl>
   <or>Ukrainian localisation of LibreOffice</or>
   <pl>Ukraińska lokalizacja LibreOffice</pl>
   <pt_BR>Ucraniano Localização para o LibreOffice</pt_BR>
   <pt>Ucraniano Localização para LibreOffice</pt>
   <ro>Ukrainian localisation of LibreOffice</ro>
   <ru>Ukrainian localisation of LibreOffice</ru>
   <sk>Ukrainian localisation of LibreOffice</sk>
   <sl>Ukrajinske krajevne nastavitve za LibreOffice</sl>
   <so>Ukrainian localisation of LibreOffice</so>
   <sq>Përkthim i LibreOffice-it në ukrainisht</sq>
   <sr>Ukrainian localisation of LibreOffice</sr>
   <sv>Ukrainsk lokalisering av LibreOffice</sv>
   <th>Ukrainian localisation ของ LibreOffice</th>
   <tr>LibreOffice Ukraynaca yerelleştirmesi</tr>
   <uk>Ukrainian localisation of LibreOffice</uk>
   <vi>Ukrainian localisation of LibreOffice</vi>
   <zh_CN>Ukrainian localisation of LibreOffice</zh_CN>
   <zh_HK>Ukrainian localisation of LibreOffice</zh_HK>
   <zh_TW>Ukrainian localisation of LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-uk
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-uk
</uninstall_package_names>

</app>
