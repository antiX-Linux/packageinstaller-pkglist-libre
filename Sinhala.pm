<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Sinhala
</name>

<description>
   <am>Sinhala dictionary for hunspell</am>
   <ar>Sinhala dictionary for hunspell</ar>
   <be>Sinhala dictionary for hunspell</be>
   <bg>Sinhala dictionary for hunspell</bg>
   <bn>Sinhala dictionary for hunspell</bn>
   <ca>Diccionari Sinhala per hunspell</ca>
   <cs>Sinhala dictionary for hunspell</cs>
   <da>Sinhala dictionary for hunspell</da>
   <de>Singhalesisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Sinhala για hunspell</el>
   <en>Sinhala dictionary for hunspell</en>
   <es_ES>Diccionario Cingalés para hunspell</es_ES>
   <es>Diccionario Cingalés para hunspell</es>
   <et>Sinhala dictionary for hunspell</et>
   <eu>Sinhala dictionary for hunspell</eu>
   <fa>Sinhala dictionary for hunspell</fa>
   <fil_PH>Sinhala dictionary for hunspell</fil_PH>
   <fi>Sinhala dictionary for hunspell</fi>
   <fr_BE>Cinghalais (Sri Lanka) dictionnaire pour hunspell</fr_BE>
   <fr>Cinghalais (Sri Lanka) dictionnaire pour hunspell</fr>
   <gl_ES>Sinhala dictionary for hunspell</gl_ES>
   <gu>Sinhala dictionary for hunspell</gu>
   <he_IL>Sinhala dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु सिंहला शब्दकोष</hi>
   <hr>Sinhala dictionary for hunspell</hr>
   <hu>Sinhala dictionary for hunspell</hu>
   <id>Sinhala dictionary for hunspell</id>
   <is>Sinhala dictionary for hunspell</is>
   <it>Dizionario sinhala per hunspell</it>
   <ja>Hunspell 用シンハラ語辞書</ja>
   <kk>Sinhala dictionary for hunspell</kk>
   <ko>Sinhala dictionary for hunspell</ko>
   <ku>Sinhala dictionary for hunspell</ku>
   <lt>Sinhala dictionary for hunspell</lt>
   <mk>Sinhala dictionary for hunspell</mk>
   <mr>Sinhala dictionary for hunspell</mr>
   <nb_NO>Sinhala dictionary for hunspell</nb_NO>
   <nb>Sinhala ordliste for hunspell</nb>
   <nl_BE>Sinhala dictionary for hunspell</nl_BE>
   <nl>Sinhala dictionary for hunspell</nl>
   <or>Sinhala dictionary for hunspell</or>
   <pl>Sinhala dictionary for hunspell</pl>
   <pt_BR>Dicionário Cingalês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Sinhala para hunspell</pt>
   <ro>Sinhala dictionary for hunspell</ro>
   <ru>Sinhala dictionary for hunspell</ru>
   <sk>Sinhala dictionary for hunspell</sk>
   <sl>Sinhalski slovar za hunspell</sl>
   <so>Sinhala dictionary for hunspell</so>
   <sq>Fjalor në sinhala për hunspell</sq>
   <sr>Sinhala dictionary for hunspell</sr>
   <sv>Sinhala ordbok för hunspell</sv>
   <th>Sinhala dictionary for hunspell</th>
   <tr>Hunspell için Seylanca sözlük</tr>
   <uk>Sinhala dictionary for hunspell</uk>
   <vi>Sinhala dictionary for hunspell</vi>
   <zh_CN>Sinhala dictionary for hunspell</zh_CN>
   <zh_HK>Sinhala dictionary for hunspell</zh_HK>
   <zh_TW>Sinhala dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-si
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-si
</uninstall_package_names>

</app>
