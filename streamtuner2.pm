<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Audio
</category>

<name>
Streamtuner2
</name>

<description>
   <am>Browser for radio station directories</am>
   <ar>Browser for radio station directories</ar>
   <be>Browser for radio station directories</be>
   <bg>Browser for radio station directories</bg>
   <bn>Browser for radio station directories</bn>
   <ca>Navega per directoris d'estacions de ràdio</ca>
   <cs>Browser for radio station directories</cs>
   <da>Browser for radio station directories</da>
   <de>Browser für Verzeichnisse von Radiosendern</de>
   <el>Πρόγραμμα περιήγησης για καταλόγους ραδιοφωνικών σταθμών</el>
   <en>Browser for radio station directories</en>
   <es_ES>Navegador de directorios de emisoras de radio</es_ES>
   <es>Navegador de directorios de emisoras de radio</es>
   <et>Browser for radio station directories</et>
   <eu>Browser for radio station directories</eu>
   <fa>Browser for radio station directories</fa>
   <fil_PH>Browser for radio station directories</fil_PH>
   <fi>Browser for radio station directories</fi>
   <fr_BE>Navigateur pour les répertoires de stations de radio</fr_BE>
   <fr>Navigateur pour les répertoires de stations de radio</fr>
   <gl_ES>Browser for radio station directories</gl_ES>
   <gu>Browser for radio station directories</gu>
   <he_IL>Browser for radio station directories</he_IL>
   <hi>रेडियो स्टेशन डायरेक्टरी हेतु ब्राउज़र</hi>
   <hr>Browser for radio station directories</hr>
   <hu>Browser for radio station directories</hu>
   <id>Browser for radio station directories</id>
   <is>Browser for radio station directories</is>
   <it>Browser per elenchi di stazioni radio</it>
   <ja>ラジオ局のディレクトリ用ブラウザー</ja>
   <kk>Browser for radio station directories</kk>
   <ko>Browser for radio station directories</ko>
   <ku>Browser for radio station directories</ku>
   <lt>Browser for radio station directories</lt>
   <mk>Browser for radio station directories</mk>
   <mr>Browser for radio station directories</mr>
   <nb_NO>Browser for radio station directories</nb_NO>
   <nb>Utforsk katalogtjenester for radiostasjoner</nb>
   <nl_BE>Browser for radio station directories</nl_BE>
   <nl>Browser for radio station directories</nl>
   <or>Browser for radio station directories</or>
   <pl>Browser for radio station directories</pl>
   <pt_BR>Navegador para diretórios de estações de rádio</pt_BR>
   <pt>Pesquisar directórios de estações de rádio</pt>
   <ro>Browser for radio station directories</ro>
   <ru>Browser for radio station directories</ru>
   <sk>Browser for radio station directories</sk>
   <sl>Brskalnik po imenikih radijskih postaj</sl>
   <so>Browser for radio station directories</so>
   <sq>Shfletues për lista stacionesh radio</sq>
   <sr>Browser for radio station directories</sr>
   <sv>Läsare för radiostations-kataloger</sv>
   <th>Browser for radio station directories</th>
   <tr>Radyo istasyonu dizinleri için tarayıcı</tr>
   <uk>Browser for radio station directories</uk>
   <vi>Browser for radio station directories</vi>
   <zh_CN>Browser for radio station directories</zh_CN>
   <zh_HK>Browser for radio station directories</zh_HK>
   <zh_TW>Browser for radio station directories</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
streamtuner2
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
streamtuner2
</uninstall_package_names>
</app>
