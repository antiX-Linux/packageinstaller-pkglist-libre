<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
Ceni
</name>

<description>
   <am>Curses user interface for configuring network interfaces</am>
   <ar>Curses user interface for configuring network interfaces</ar>
   <be>Curses user interface for configuring network interfaces</be>
   <bg>Curses user interface for configuring network interfaces</bg>
   <bn>Curses user interface for configuring network interfaces</bn>
   <ca>Interfície d'usuari en curses per configurar interfícies d'usuari</ca>
   <cs>Curses user interface for configuring network interfaces</cs>
   <da>Curses user interface for configuring network interfaces</da>
   <de>Textbasierte Benutzeroberfläche zur Konfiguration von Netzwerkschnittstellen (verwendet die “curses” Programmbibliothek).</de>
   <el>διαμόρφωση διεπαφών δικτύου</el>
   <en>Curses user interface for configuring network interfaces</en>
   <es_ES>Interfaz de usuario Curses para configurar interfaces de red</es_ES>
   <es>Configurador de interfaces de red usando una interfaz curses</es>
   <et>Curses user interface for configuring network interfaces</et>
   <eu>Curses user interface for configuring network interfaces</eu>
   <fa>Curses user interface for configuring network interfaces</fa>
   <fil_PH>Curses user interface for configuring network interfaces</fil_PH>
   <fi>Curses user interface for configuring network interfaces</fi>
   <fr_BE>Interface utilisateur Curses pour la configuration des interfaces réseau</fr_BE>
   <fr>Interface utilisateur Curses pour la configuration des interfaces réseau</fr>
   <gl_ES>Curses user interface for configuring network interfaces</gl_ES>
   <gu>Curses user interface for configuring network interfaces</gu>
   <he_IL>Curses user interface for configuring network interfaces</he_IL>
   <hi>नेटवर्क अंतरफलक विन्यास हेतु Curses उपयोक्ता अंतरफलक</hi>
   <hr>Curses user interface for configuring network interfaces</hr>
   <hu>Curses user interface for configuring network interfaces</hu>
   <id>Curses user interface for configuring network interfaces</id>
   <is>Curses user interface for configuring network interfaces</is>
   <it>Interfaccia utente di Curses per la configurazione delle interfacce di rete</it>
   <ja>ネットワーク・インターフェース設定用のCursesユーザー・インターフェース</ja>
   <kk>Curses user interface for configuring network interfaces</kk>
   <ko>Curses user interface for configuring network interfaces</ko>
   <ku>Curses user interface for configuring network interfaces</ku>
   <lt>Curses user interface for configuring network interfaces</lt>
   <mk>Curses user interface for configuring network interfaces</mk>
   <mr>Curses user interface for configuring network interfaces</mr>
   <nb_NO>Curses user interface for configuring network interfaces</nb_NO>
   <nb>Curses-GUI for oppsett av nettverksgrensesnitt</nb>
   <nl_BE>Curses user interface for configuring network interfaces</nl_BE>
   <nl>Cursus-gebruikersinterface voor het configureren van netwerkinterfaces</nl>
   <or>Curses user interface for configuring network interfaces</or>
   <pl>Curses user interface for configuring network interfaces</pl>
   <pt_BR>Interface de usuário 'Curses' para configuração de interfaces de rede</pt_BR>
   <pt>Interface de utilizador em curses para configuração de interfaces de rede</pt>
   <ro>Curses user interface for configuring network interfaces</ro>
   <ru>Curses user interface for configuring network interfaces</ru>
   <sk>Curses user interface for configuring network interfaces</sk>
   <sl>Uporabniški vmesnik Curses za nastavljanje omrežnih vmesnikov</sl>
   <so>Curses user interface for configuring network interfaces</so>
   <sq>Ndërfaqe përdoruesi në Curses për formësim ndërfaqesh rrjeti</sq>
   <sr>Curses user interface for configuring network interfaces</sr>
   <sv>Curses användargränsnitt för att konfigurera nätverksgränssnitt</sv>
   <th>Curses user interface for configuring network interfaces</th>
   <tr>Ağ arayüzlerini yapılandırmak için kullanıcı arayüzünü lanetler</tr>
   <uk>Curses user interface for configuring network interfaces</uk>
   <vi>Curses user interface for configuring network interfaces</vi>
   <zh_CN>Curses user interface for configuring network interfaces</zh_CN>
   <zh_HK>Curses user interface for configuring network interfaces</zh_HK>
   <zh_TW>Curses user interface for configuring network interfaces</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
ceni
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
ceni
</uninstall_package_names>
</app>
