<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
KDE5
</name>

<description>
   <am>KDE5-full</am>
   <ar>KDE5-full</ar>
   <be>KDE5-full</be>
   <bg>KDE5-full</bg>
   <bn>KDE5-full</bn>
   <ca>KDE5-complet</ca>
   <cs>KDE5-full</cs>
   <da>KDE5-full</da>
   <de>Vollständige Installation der Desktop-Umgebung KDE 5</de>
   <el>Πλήρης εγκατάσταση KDE5</el>
   <en>KDE5-full</en>
   <es_ES>KDE5-completo</es_ES>
   <es>KDE5-completo</es>
   <et>KDE5-full</et>
   <eu>KDE5-full</eu>
   <fa>KDE5-full</fa>
   <fil_PH>KDE5-full</fil_PH>
   <fi>KDE5-kokonainen</fi>
   <fr_BE>KDE5 complet</fr_BE>
   <fr>KDE5 complet</fr>
   <gl_ES>KDE5-full</gl_ES>
   <gu>KDE5-full</gu>
   <he_IL>KDE5-full</he_IL>
   <hi>केडीई5-पूर्ण</hi>
   <hr>KDE5-full</hr>
   <hu>KDE5-full</hu>
   <id>KDE5-full</id>
   <is>KDE5-full</is>
   <it>KDE5-full</it>
   <ja>KDE 5 デスクトップのフルインストール</ja>
   <kk>KDE5-full</kk>
   <ko>KDE5-full</ko>
   <ku>KDE5-full</ku>
   <lt>KDE5-full</lt>
   <mk>KDE5-full</mk>
   <mr>KDE5-full</mr>
   <nb_NO>KDE5-full</nb_NO>
   <nb>KDE5-full</nb>
   <nl_BE>KDE5-full</nl_BE>
   <nl>KDE5-full</nl>
   <or>KDE5-full</or>
   <pl>KDE5-full</pl>
   <pt_BR>Instalação Completa do KDE5-full</pt_BR>
   <pt>Instalação completa do KDE5</pt>
   <ro>KDE5-full</ro>
   <ru>KDE5-full</ru>
   <sk>KDE5-full</sk>
   <sl>KDE5-celota</sl>
   <so>KDE5-full</so>
   <sq>KDE5-i plotë</sq>
   <sr>KDE5-full</sr>
   <sv>KDE5-full</sv>
   <th>KDE5-full</th>
   <tr>KDE5-tam</tr>
   <uk>KDE5-full</uk>
   <vi>KDE5-full</vi>
   <zh_CN>KDE5-full</zh_CN>
   <zh_HK>KDE5-full</zh_HK>
   <zh_TW>KDE5-full</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
kde-full
kwin-x11
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
kde-full
kwin-x11
</uninstall_package_names>

</app>
