<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Malayalam_Default_Firefox_esr
</name>

<description>
   <am>Malayalam localisation of Firefox ESR</am>
   <ar>Malayalam localisation of Firefox ESR</ar>
   <be>Malayalam localisation of Firefox ESR</be>
   <bg>Malayalam localisation of Firefox ESR</bg>
   <bn>Malayalam localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Malai</ca>
   <cs>Malayalam localisation of Firefox ESR</cs>
   <da>Malayalam localisation of Firefox ESR</da>
   <de>Indische Lokalisation für die Sprache “Malayalam” von “Firefox ESR”</de>
   <el>Malayalam για Firefox ESR</el>
   <en>Malayalam localisation of Firefox ESR</en>
   <es_ES>Localización Malayalam de Firefox ESR</es_ES>
   <es>Localización Malayalam de Firefox ESR</es>
   <et>Malayalam localisation of Firefox ESR</et>
   <eu>Malayalam localisation of Firefox ESR</eu>
   <fa>Malayalam localisation of Firefox ESR</fa>
   <fil_PH>Malayalam localisation of Firefox ESR</fil_PH>
   <fi>Malayalam localisation of Firefox ESR</fi>
   <fr_BE>Localisation en malayalam pour Firefox ESR</fr_BE>
   <fr>Localisation en malayalam pour Firefox ESR</fr>
   <gl_ES>Malayalam localisation of Firefox ESR</gl_ES>
   <gu>Malayalam localisation of Firefox ESR</gu>
   <he_IL>Malayalam localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का मलयालम संस्करण</hi>
   <hr>Malayalam localisation of Firefox ESR</hr>
   <hu>Malayalam localisation of Firefox ESR</hu>
   <id>Malayalam localisation of Firefox ESR</id>
   <is>Malayalam localisation of Firefox ESR</is>
   <it>Localizzazione malayalam di Firefox ESR</it>
   <ja>マラヤーラム語版 Firefox ESR</ja>
   <kk>Malayalam localisation of Firefox ESR</kk>
   <ko>Malayalam localisation of Firefox ESR</ko>
   <ku>Malayalam localisation of Firefox ESR</ku>
   <lt>Malayalam localisation of Firefox ESR</lt>
   <mk>Malayalam localisation of Firefox ESR</mk>
   <mr>Malayalam localisation of Firefox ESR</mr>
   <nb_NO>Malayalam localisation of Firefox ESR</nb_NO>
   <nb>Malayalam lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Malayalam localisation of Firefox ESR</nl_BE>
   <nl>Malayalam localisation of Firefox ESR</nl>
   <or>Malayalam localisation of Firefox ESR</or>
   <pl>Malayalam localisation of Firefox ESR</pl>
   <pt_BR>Malaiala Localização para o Firefox ESR</pt_BR>
   <pt>Malaiala Localização para Firefox ESR</pt>
   <ro>Malayalam localisation of Firefox ESR</ro>
   <ru>Malayalam localisation of Firefox ESR</ru>
   <sk>Malayalam localisation of Firefox ESR</sk>
   <sl>Malajalamske krajevne nastavitve za Firefox ESR</sl>
   <so>Malayalam localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në malajalamisht</sq>
   <sr>Malayalam localisation of Firefox ESR</sr>
   <sv>Malayalam lokalisering av Firefox ESR</sv>
   <th>Malayalam localisation of Firefox ESR</th>
   <tr>Firefox ESR Malayalamca yerelleştirmesi</tr>
   <uk>Malayalam localisation of Firefox ESR</uk>
   <vi>Malayalam localisation of Firefox ESR</vi>
   <zh_CN>Malayalam localisation of Firefox ESR</zh_CN>
   <zh_HK>Malayalam localisation of Firefox ESR</zh_HK>
   <zh_TW>Malayalam localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ml
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ml
</uninstall_package_names>

</app>
