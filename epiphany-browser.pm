<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Epiphany Browser
</name>

<description>
   <am>Intuitive web browser - works with very old hardware</am>
   <ar>Intuitive web browser - works with very old hardware</ar>
   <be>Intuitive web browser - works with very old hardware</be>
   <bg>Intuitive web browser - works with very old hardware</bg>
   <bn>Intuitive web browser - works with very old hardware</bn>
   <ca>Navegador web intuïtiu - funciona en maquinari molt vell</ca>
   <cs>Intuitive web browser - works with very old hardware</cs>
   <da>Intuitive web browser - works with very old hardware</da>
   <de>Internetbrowser mit intuitiver Bedienung - funktioniert auf sehr alter Hardware</de>
   <el>Διαισθητικό πρόγραμμα περιήγησης ιστού - λειτουργεί με πολύ παλιό υλικό</el>
   <en>Intuitive web browser - works with very old hardware</en>
   <es_ES>Navegador web intuitivo - funciona con hardware muy antiguo</es_ES>
   <es>Navegador web intuitivo - funciona con hardware muy antiguo</es>
   <et>Intuitive web browser - works with very old hardware</et>
   <eu>Intuitive web browser - works with very old hardware</eu>
   <fa>Intuitive web browser - works with very old hardware</fa>
   <fil_PH>Intuitive web browser - works with very old hardware</fil_PH>
   <fi>Intuitive web browser - works with very old hardware</fi>
   <fr_BE>Navigateur web intuitif - fonctionne avec du matériel très ancien</fr_BE>
   <fr>Navigateur web intuitif - fonctionne avec du matériel très ancien</fr>
   <gl_ES>Intuitive web browser - works with very old hardware</gl_ES>
   <gu>Intuitive web browser - works with very old hardware</gu>
   <he_IL>Intuitive web browser - works with very old hardware</he_IL>
   <hi>एक सहज वेब ब्राउज़र - पुराने हार्डवेयर हेतु उपयुक्त</hi>
   <hr>Intuitive web browser - works with very old hardware</hr>
   <hu>Intuitive web browser - works with very old hardware</hu>
   <id>Intuitive web browser - works with very old hardware</id>
   <is>Intuitive web browser - works with very old hardware</is>
   <it>Browser intuitivo - funziona con hardware molto vecchio</it>
   <ja>直感的に操作できるWebブラウザ - 旧式のハードウェアでも動作可能</ja>
   <kk>Intuitive web browser - works with very old hardware</kk>
   <ko>Intuitive web browser - works with very old hardware</ko>
   <ku>Intuitive web browser - works with very old hardware</ku>
   <lt>Intuitive web browser - works with very old hardware</lt>
   <mk>Intuitive web browser - works with very old hardware</mk>
   <mr>Intuitive web browser - works with very old hardware</mr>
   <nb_NO>Intuitive web browser - works with very old hardware</nb_NO>
   <nb>Intuitiv nettleser som virker med gammel maskinvare</nb>
   <nl_BE>Intuitive web browser - works with very old hardware</nl_BE>
   <nl>Intuïtieve webbrowser - werkt met zeer oude hardware</nl>
   <or>Intuitive web browser - works with very old hardware</or>
   <pl>Intuitive web browser - works with very old hardware</pl>
   <pt_BR>Navegador de internet intuitivo - funciona com equipamento (hardware) muito antigo</pt_BR>
   <pt>Navegador web intuitivo - funciona em equipamento muito antigo</pt>
   <ro>Intuitive web browser - works with very old hardware</ro>
   <ru>Intuitive web browser - works with very old hardware</ru>
   <sk>Intuitive web browser - works with very old hardware</sk>
   <sl>Intuitivni spletni brskalnik - deluje s starejšimi računalniki</sl>
   <so>Intuitive web browser - works with very old hardware</so>
   <sq>Shfletues intuitiv - funksionon me hardware shumë të vjetër</sq>
   <sr>Intuitive web browser - works with very old hardware</sr>
   <sv>Intuitiv webläsare - fungerar med mycket gammal hårdvara</sv>
   <th>Intuitive web browser - works with very old hardware</th>
   <tr>Sezgisel internet tarayıcısı - çok eski donanımlarla çalışır</tr>
   <uk>Intuitive web browser - works with very old hardware</uk>
   <vi>Intuitive web browser - works with very old hardware</vi>
   <zh_CN>Intuitive web browser - works with very old hardware</zh_CN>
   <zh_HK>Intuitive web browser - works with very old hardware</zh_HK>
   <zh_TW>Intuitive web browser - works with very old hardware</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
epiphany-browser
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
epiphany-browser
</uninstall_package_names>
</app>
