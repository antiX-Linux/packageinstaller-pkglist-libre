<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Window Managers
</category>

<name>
IceWM
</name>

<description>
   <am>lightweight window manager</am>
   <ar>lightweight window manager</ar>
   <be>lightweight window manager</be>
   <bg>lightweight window manager</bg>
   <bn>lightweight window manager</bn>
   <ca>Gestor de finestres lleuger</ca>
   <cs>lightweight window manager</cs>
   <da>lightweight window manager</da>
   <de>Einfaches Programm zur Fensterverwaltung der Benutzeroberfläche</de>
   <el>ελαφρύς διαχειριστής παραθύρων</el>
   <en>lightweight window manager</en>
   <es_ES>Gestor de ventanas ligero</es_ES>
   <es>gestor de ventanas ligero</es>
   <et>lightweight window manager</et>
   <eu>lightweight window manager</eu>
   <fa>lightweight window manager</fa>
   <fil_PH>lightweight window manager</fil_PH>
   <fi>lightweight window manager</fi>
   <fr_BE>gestionnaire de fenêtres léger</fr_BE>
   <fr>gestionnaire de fenêtres léger</fr>
   <gl_ES>lightweight window manager</gl_ES>
   <gu>lightweight window manager</gu>
   <he_IL>lightweight window manager</he_IL>
   <hi>सरल विंडो प्रबंधक</hi>
   <hr>lightweight window manager</hr>
   <hu>lightweight window manager</hu>
   <id>lightweight window manager</id>
   <is>lightweight window manager</is>
   <it>window manager leggero</it>
   <ja>軽量なウインドゥマネージャ</ja>
   <kk>lightweight window manager</kk>
   <ko>lightweight window manager</ko>
   <ku>lightweight window manager</ku>
   <lt>lightweight window manager</lt>
   <mk>lightweight window manager</mk>
   <mr>lightweight window manager</mr>
   <nb_NO>lightweight window manager</nb_NO>
   <nb>lettvektig vindusbehandler</nb>
   <nl_BE>lightweight window manager</nl_BE>
   <nl>lightweight window manager</nl>
   <or>lightweight window manager</or>
   <pl>lightweight window manager</pl>
   <pt_BR>Gerenciador de Janelas Leve</pt_BR>
   <pt>Gestor de janelas ligeiro</pt>
   <ro>lightweight window manager</ro>
   <ru>lightweight window manager</ru>
   <sk>lightweight window manager</sk>
   <sl>lahek okenski upravljalnik</sl>
   <so>lightweight window manager</so>
   <sq>përgjegjës dritaresh i peshës së lehtë</sq>
   <sr>lightweight window manager</sr>
   <sv>lättvikts fönsterhanterare</sv>
   <th>lightweight window manager</th>
   <tr>hafif pencere yöneticisi</tr>
   <uk>lightweight window manager</uk>
   <vi>lightweight window manager</vi>
   <zh_CN>lightweight window manager</zh_CN>
   <zh_HK>lightweight window manager</zh_HK>
   <zh_TW>lightweight window manager</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
icewm
icewm-common
icewm-themes-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
icewm
icewm-common
icewm-themes-antix
</uninstall_package_names>
</app>
