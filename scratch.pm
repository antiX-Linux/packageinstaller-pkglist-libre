<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Children
</category>

<name>
Scratch
</name>

<description>
   <am>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</am>
   <ar>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</ar>
   <be>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</be>
   <bg>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</bg>
   <bn>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</bn>
   <ca>Entorn de programació gràfic Scratch del M.I.T (https://scratch.mit.edu/)</ca>
   <cs>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</cs>
   <da>Scratch grafisk programmeringsmiljø fra M.I.T. https://scratch.mit.edu/</da>
   <de>Scratch Graphical Programming Umgebung von M.I.T. https://scratch.mit.edu/</de>
   <el>Περιβάλλον γραφικού προγραμματισμού Scratch από το M.I.T. https://scratch.mit.edu/</el>
   <en>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</en>
   <es_ES>Entorno de Programación de Scratch Gráfico del M.I.T. https://scratch.mit.edu/</es_ES>
   <es>Entorno de Programación de Scratch Gráfico del M.I.T. https://scratch.mit.edu/</es>
   <et>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</et>
   <eu>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</eu>
   <fa>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</fa>
   <fil_PH>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</fil_PH>
   <fi>Scratch on graafinen ohjelmointiympäristö jota kehittää M.I.T. https://scratch.mit.edu/</fi>
   <fr_BE>Environnement graphique de programmation Scratch de M.I.T. https://scratch.mit.edu/</fr_BE>
   <fr>Environnement graphique de programmation Scratch de M.I.T. https://scratch.mit.edu/</fr>
   <gl_ES>Ambiente de programación gráfica Scratch do M.I.T. https://scratch.mit.edu/</gl_ES>
   <gu>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</gu>
   <he_IL>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</he_IL>
   <hi>एमआईटी से स्क्रैच ग्राफ़िकल प्रोग्रामिंग वातावरण https://scratch.mit.edu/</hi>
   <hr>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</hr>
   <hu>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</hu>
   <id>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</id>
   <is>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</is>
   <it>Ambiente di programmazione grafica Scratch dal M.I.T. https://scratch.mit.edu/</it>
   <ja>M.I.T. からのスクラッチ グラフィカル プログラミング環境 https://scratch.mit.edu/</ja>
   <kk>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</kk>
   <ko>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</ko>
   <ku>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</ku>
   <lt>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</lt>
   <mk>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</mk>
   <mr>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</mr>
   <nb_NO>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</nb_NO>
   <nb>Scratch grafisk programmeringsmiljø fra MIT. https://scratch.mit.edu/</nb>
   <nl_BE>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</nl_BE>
   <nl>Scratch Grafische Progammeer Omgeving van M.I.T. https://scratch.mit.edu/</nl>
   <or>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</or>
   <pl>programistyczne środowisko graficzne Scratch od M.I.T. https://scratch.mit.edu/</pl>
   <pt_BR>Ambiente de programação gráfica Scratch do M.I.T. https://scratch.mit.edu/</pt_BR>
   <pt>Ambiente de programação gráfica Scratch do M.I.T. https://scratch.mit.edu/</pt>
   <ro>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</ro>
   <ru>Окружение визуального программирования Scratch https://scratch.mit.edu/</ru>
   <sk>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</sk>
   <sl>Scratch grafično okolje za učenje programiranja (M.I.T. https://scratch.mit.edu/)</sl>
   <so>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</so>
   <sq>Mjedisi Scratch për Programim grafik, nga M.I.T. https://scratch.mit.edu/</sq>
   <sr>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</sr>
   <sv>Scratch Grafisk Programmeringsmiljö från M.I.T. https://scratch.mit.edu/</sv>
   <th>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</th>
   <tr>M.I.T'den Sıfırdan Grafiksel Programlama ortamı https://scratch.mit.edu/</tr>
   <uk>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</uk>
   <vi>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</vi>
   <zh_CN>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</zh_CN>
   <zh_HK>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</zh_HK>
   <zh_TW>Scratch Graphical Programming envirnoment from M.I.T. https://scratch.mit.edu/</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
scratch
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
scratch
</uninstall_package_names>
</app>
