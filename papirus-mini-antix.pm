<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Icons
</category>

<name>
Papirus-Mini antiX
</name>

<description>
   <am>Basic set of Papirus icons for antiX Linux</am>
   <ar>Basic set of Papirus icons for antiX Linux</ar>
   <be>Basic set of Papirus icons for antiX Linux</be>
   <bg>Basic set of Papirus icons for antiX Linux</bg>
   <bn>Basic set of Papirus icons for antiX Linux</bn>
   <ca>Joc bàsic d'icones Papirus per antiX Linux</ca>
   <cs>Basic set of Papirus icons for antiX Linux</cs>
   <da>Basic set of Papirus icons for antiX Linux</da>
   <de>Basisauswahl von Piktogrammen (Ugs.: “Icons”) für antiX im Stil “Papirus”</de>
   <el>Βασικό σύνολο εικονιδίων Papirus για antiX Linux</el>
   <en>Basic set of Papirus icons for antiX Linux</en>
   <es_ES>Conjunto básico de iconos Papirus para antiX Linux</es_ES>
   <es>Conjunto básico de iconos Papirus para antiX Linux</es>
   <et>Basic set of Papirus icons for antiX Linux</et>
   <eu>Basic set of Papirus icons for antiX Linux</eu>
   <fa>Basic set of Papirus icons for antiX Linux</fa>
   <fil_PH>Basic set of Papirus icons for antiX Linux</fil_PH>
   <fi>Basic set of Papirus icons for antiX Linux</fi>
   <fr_BE>Icônes pour antiX dans le style “Papirus” (version basique)</fr_BE>
   <fr>Icônes pour antiX dans le style “Papirus” (version basique)</fr>
   <gl_ES>Basic set of Papirus icons for antiX Linux</gl_ES>
   <gu>Basic set of Papirus icons for antiX Linux</gu>
   <he_IL>Basic set of Papirus icons for antiX Linux</he_IL>
   <hi>एंटी-एक्स लिनक्स हेतु Papirus आइकन का सामान संग्रह</hi>
   <hr>Basic set of Papirus icons for antiX Linux</hr>
   <hu>Basic set of Papirus icons for antiX Linux</hu>
   <id>Basic set of Papirus icons for antiX Linux</id>
   <is>Basic set of Papirus icons for antiX Linux</is>
   <it>Set di base di icone Papirus per antiX Linux</it>
   <ja>AntiX Linux用Papirusアイコンの基本バージョン</ja>
   <kk>Basic set of Papirus icons for antiX Linux</kk>
   <ko>Basic set of Papirus icons for antiX Linux</ko>
   <ku>Basic set of Papirus icons for antiX Linux</ku>
   <lt>Basic set of Papirus icons for antiX Linux</lt>
   <mk>Basic set of Papirus icons for antiX Linux</mk>
   <mr>Basic set of Papirus icons for antiX Linux</mr>
   <nb_NO>Basic set of Papirus icons for antiX Linux</nb_NO>
   <nb>Grunnleggende sett med Papirus-ikoner for antiX Linux</nb>
   <nl_BE>Basic set of Papirus icons for antiX Linux</nl_BE>
   <nl>Basic set of Papirus icons for antiX Linux</nl>
   <or>Basic set of Papirus icons for antiX Linux</or>
   <pl>Basic set of Papirus icons for antiX Linux</pl>
   <pt_BR>Conjunto básico de ícones do Papirus para o antiX Linux</pt_BR>
   <pt>Conjunto básico de ícones Papirus para o antiX Linux</pt>
   <ro>Basic set of Papirus icons for antiX Linux</ro>
   <ru>Basic set of Papirus icons for antiX Linux</ru>
   <sk>Basic set of Papirus icons for antiX Linux</sk>
   <sl>Osnovni nabor Papirus ikon za antiX Linux</sl>
   <so>Basic set of Papirus icons for antiX Linux</so>
   <sq>Grup elementar i ikonave Papirus për antiX Linux</sq>
   <sr>Basic set of Papirus icons for antiX Linux</sr>
   <sv>Bas-set av Papirus ikoner för antiX Linux</sv>
   <th>Basic set of Papirus icons for antiX Linux</th>
   <tr>antiX Linux Papirus simgelerinin temel takımı</tr>
   <uk>Basic set of Papirus icons for antiX Linux</uk>
   <vi>Basic set of Papirus icons for antiX Linux</vi>
   <zh_CN>Basic set of Papirus icons for antiX Linux</zh_CN>
   <zh_HK>Basic set of Papirus icons for antiX Linux</zh_HK>
   <zh_TW>Basic set of Papirus icons for antiX Linux</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
papiris-mini-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
papiris-mini-antix
</uninstall_package_names>
</app>
