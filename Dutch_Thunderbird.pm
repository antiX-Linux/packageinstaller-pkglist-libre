<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Dutch_Thunderbird
</name>

<description>
   <am>Dutch localisation of Thunderbird</am>
   <ar>Dutch localisation of Thunderbird</ar>
   <be>Dutch localisation of Thunderbird</be>
   <bg>Dutch localisation of Thunderbird</bg>
   <bn>Dutch localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Holandès</ca>
   <cs>Dutch localisation of Thunderbird</cs>
   <da>Hollandsk oversættelse af Thunderbird</da>
   <de>Niederländische Lokalisierung von Thunderbird</de>
   <el>Ολλανδικά για το Thunderbird</el>
   <en>Dutch localisation of Thunderbird</en>
   <es_ES>Localización Holandesa de Thunderbird</es_ES>
   <es>Localización Holandés de Thunderbird</es>
   <et>Dutch localisation of Thunderbird</et>
   <eu>Dutch localisation of Thunderbird</eu>
   <fa>Dutch localisation of Thunderbird</fa>
   <fil_PH>Dutch localisation of Thunderbird</fil_PH>
   <fi>Hollantilainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en néerlandais pour Thunderbird</fr_BE>
   <fr>Localisation en néerlandais pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird para neerlandés</gl_ES>
   <gu>Dutch localisation of Thunderbird</gu>
   <he_IL>Dutch localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का डच संस्करण</hi>
   <hr>Dutch localisation of Thunderbird</hr>
   <hu>Dutch localisation of Thunderbird</hu>
   <id>Dutch localisation of Thunderbird</id>
   <is>Dutch localisation of Thunderbird</is>
   <it>Localizzazione olandese di Thunderbird</it>
   <ja>Thunderbird のオランダ語版</ja>
   <kk>Dutch localisation of Thunderbird</kk>
   <ko>Dutch localisation of Thunderbird</ko>
   <ku>Dutch localisation of Thunderbird</ku>
   <lt>Dutch localisation of Thunderbird</lt>
   <mk>Dutch localisation of Thunderbird</mk>
   <mr>Dutch localisation of Thunderbird</mr>
   <nb_NO>Dutch localisation of Thunderbird</nb_NO>
   <nb>Nederlandsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Dutch localisation of Thunderbird</nl_BE>
   <nl>Nederlandse lokalisatie van Thunderbird</nl>
   <or>Dutch localisation of Thunderbird</or>
   <pl>Holenderska lokalizacja Thunderbirda</pl>
   <pt_BR>Holandês Localização para o Thunderbird</pt_BR>
   <pt>Holandês Localização para Thunderbird</pt>
   <ro>Dutch localisation of Thunderbird</ro>
   <ru>Dutch localisation of Thunderbird</ru>
   <sk>Dutch localisation of Thunderbird</sk>
   <sl>Nizozemska lokalizacija za Thunderbird</sl>
   <so>Dutch localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në holandisht</sq>
   <sr>Dutch localisation of Thunderbird</sr>
   <sv>Holländsk lokalisering av Thunderbird</sv>
   <th>Dutch localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Flemenkçe yerelleştirmesi</tr>
   <uk>Dutch локалізація Thunderbird</uk>
   <vi>Dutch localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 荷兰语语言包</zh_CN>
   <zh_HK>Dutch localisation of Thunderbird</zh_HK>
   <zh_TW>Dutch localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-nl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-nl
</uninstall_package_names>

</app>
