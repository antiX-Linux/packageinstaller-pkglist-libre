<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Russian_Thunderbird
</name>

<description>
   <am>Russian localisation of Thunderbird</am>
   <ar>Russian localisation of Thunderbird</ar>
   <be>Russian localisation of Thunderbird</be>
   <bg>Russian localisation of Thunderbird</bg>
   <bn>Russian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Rus</ca>
   <cs>Russian localisation of Thunderbird</cs>
   <da>Russisk oversættelse af Thunderbird</da>
   <de>Russische Lokalisierung von Thunderbird</de>
   <el>Ρωσικά για το Thunderbird</el>
   <en>Russian localisation of Thunderbird</en>
   <es_ES>Localización Rusa de Thunderbird</es_ES>
   <es>Localización Ruso de Thunderbird</es>
   <et>Russian localisation of Thunderbird</et>
   <eu>Russian localisation of Thunderbird</eu>
   <fa>Russian localisation of Thunderbird</fa>
   <fil_PH>Russian localisation of Thunderbird</fil_PH>
   <fi>Venäläinen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en russe pour Thunderbird</fr_BE>
   <fr>Localisation en russe pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao ruso</gl_ES>
   <gu>Russian localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לרוסית</he_IL>
   <hi>थंडरबर्ड का रूसी संस्करण</hi>
   <hr>Russian localisation of Thunderbird</hr>
   <hu>Russian localisation of Thunderbird</hu>
   <id>Russian localisation of Thunderbird</id>
   <is>Russian localisation of Thunderbird</is>
   <it>Localizzazione russa di Thunderbird</it>
   <ja>Thunderbird のロシア語版</ja>
   <kk>Russian localisation of Thunderbird</kk>
   <ko>Russian localisation of Thunderbird</ko>
   <ku>Russian localisation of Thunderbird</ku>
   <lt>Russian localisation of Thunderbird</lt>
   <mk>Russian localisation of Thunderbird</mk>
   <mr>Russian localisation of Thunderbird</mr>
   <nb_NO>Russian localisation of Thunderbird</nb_NO>
   <nb>Russisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Russian localisation of Thunderbird</nl_BE>
   <nl>Russische lokalisatie van Thunderbird</nl>
   <or>Russian localisation of Thunderbird</or>
   <pl>Rosyjska lokalizacja Thunderbirda</pl>
   <pt_BR>Russo Localização para o Thunderbird</pt_BR>
   <pt>Russo Localização para Thunderbird</pt>
   <ro>Russian localisation of Thunderbird</ro>
   <ru>Русский перевод для Thunderbird</ru>
   <sk>Russian localisation of Thunderbird</sk>
   <sl>Ruske krajevne nastavitve za Thunderbird</sl>
   <so>Russian localisation of Thunderbird</so>
   <sq>Përkthimi në rusisht i Thunderbird-it</sq>
   <sr>Russian localisation of Thunderbird</sr>
   <sv>Rysk  lokalisering av Thunderbird</sv>
   <th>Russian localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Rusça yerelleştirmesi</tr>
   <uk>Russian локалізація Thunderbird</uk>
   <vi>Russian localisation of Thunderbird</vi>
   <zh_CN>Russian localisation of Thunderbird</zh_CN>
   <zh_HK>Russian localisation of Thunderbird</zh_HK>
   <zh_TW>Russian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-ru
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-ru
</uninstall_package_names>

</app>
