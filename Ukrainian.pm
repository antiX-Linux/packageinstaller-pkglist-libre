<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Ukrainian
</name>

<description>
   <am>Ukrainian dictionary for hunspell</am>
   <ar>Ukrainian dictionary for hunspell</ar>
   <be>Ukrainian dictionary for hunspell</be>
   <bg>Ukrainian dictionary for hunspell</bg>
   <bn>Ukrainian dictionary for hunspell</bn>
   <ca>Diccionari Ucraïnès per hunspell</ca>
   <cs>Ukrainian dictionary for hunspell</cs>
   <da>Ukrainian dictionary for hunspell</da>
   <de>Ukrainisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Ουκρανικά για hunspell</el>
   <en>Ukrainian dictionary for hunspell</en>
   <es_ES>Diccionario Ucraniano para hunspell</es_ES>
   <es>Diccionario Ucraniano para hunspell</es>
   <et>Ukrainian dictionary for hunspell</et>
   <eu>Ukrainian dictionary for hunspell</eu>
   <fa>Ukrainian dictionary for hunspell</fa>
   <fil_PH>Ukrainian dictionary for hunspell</fil_PH>
   <fi>Ukrainian dictionary for hunspell</fi>
   <fr_BE>Ukrainien dictionnaire pour hunspell</fr_BE>
   <fr>Ukrainien dictionnaire pour hunspell</fr>
   <gl_ES>Ukrainian dictionary for hunspell</gl_ES>
   <gu>Ukrainian dictionary for hunspell</gu>
   <he_IL>Ukrainian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु यूक्रेनी शब्दकोष</hi>
   <hr>Ukrainian dictionary for hunspell</hr>
   <hu>Ukrainian dictionary for hunspell</hu>
   <id>Ukrainian dictionary for hunspell</id>
   <is>Ukrainian dictionary for hunspell</is>
   <it>Dizionario ucraino per hunspell</it>
   <ja>Hunspell 用ウクライナ語辞書</ja>
   <kk>Ukrainian dictionary for hunspell</kk>
   <ko>Ukrainian dictionary for hunspell</ko>
   <ku>Ukrainian dictionary for hunspell</ku>
   <lt>Ukrainian dictionary for hunspell</lt>
   <mk>Ukrainian dictionary for hunspell</mk>
   <mr>Ukrainian dictionary for hunspell</mr>
   <nb_NO>Ukrainian dictionary for hunspell</nb_NO>
   <nb>Ukrainsk ordliste for hunspell</nb>
   <nl_BE>Ukrainian dictionary for hunspell</nl_BE>
   <nl>Ukrainian dictionary for hunspell</nl>
   <or>Ukrainian dictionary for hunspell</or>
   <pl>Ukrainian dictionary for hunspell</pl>
   <pt_BR>Dicionário Ucraniano para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Ucraniano para hunspell</pt>
   <ro>Ukrainian dictionary for hunspell</ro>
   <ru>Ukrainian dictionary for hunspell</ru>
   <sk>Ukrainian dictionary for hunspell</sk>
   <sl>Ukrajinski slovar za hunspell</sl>
   <so>Ukrainian dictionary for hunspell</so>
   <sq>Fjalor ukrainisht për hunspell</sq>
   <sr>Ukrainian dictionary for hunspell</sr>
   <sv>Ukrainsk ordbok för hunspell</sv>
   <th>Ukrainian dictionary for hunspell</th>
   <tr>Hunspell için Ukraynaca sözlük</tr>
   <uk>Ukrainian dictionary for hunspell</uk>
   <vi>Ukrainian dictionary for hunspell</vi>
   <zh_CN>Ukrainian dictionary for hunspell</zh_CN>
   <zh_HK>Ukrainian dictionary for hunspell</zh_HK>
   <zh_TW>Ukrainian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-uk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-uk
</uninstall_package_names>

</app>
