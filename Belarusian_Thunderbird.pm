<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Belarusian_Thunderbird
</name>

<description>
   <am>Belarusian localisation of Thunderbird</am>
   <ar>Belarusian localisation of Thunderbird</ar>
   <be>Belarusian localisation of Thunderbird</be>
   <bg>Belarusian localisation of Thunderbird</bg>
   <bn>Belarusian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Bielorús</ca>
   <cs>Belarusian localisation of Thunderbird</cs>
   <da>Hviderussisk oversættelse af Thunderbird</da>
   <de>Weißrussische Lokalisierung von Thunderbird</de>
   <el>Λευκορώσικα για το Thunderbird</el>
   <en>Belarusian localisation of Thunderbird</en>
   <es_ES>Localización Bielorussa de Thunderbird</es_ES>
   <es>Localización Bielorruso de Thunderbird</es>
   <et>Belarusian localisation of Thunderbird</et>
   <eu>Belarusian localisation of Thunderbird</eu>
   <fa>Belarusian localisation of Thunderbird</fa>
   <fil_PH>Belarusian localisation of Thunderbird</fil_PH>
   <fi>Valko-venäläinen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en biélorusse pour Thunderbird</fr_BE>
   <fr>Localisation en biélorusse pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird para beloruso</gl_ES>
   <gu>Belarusian localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לבלארוסית</he_IL>
   <hi>थंडरबर्ड का बेलारूसी संस्करण</hi>
   <hr>Bjeloruska lokalizacija Thunderbirda</hr>
   <hu>Belarusian localisation of Thunderbird</hu>
   <id>Belarusian localisation of Thunderbird</id>
   <is>Belarusian localisation of Thunderbird</is>
   <it>Localizzazione bielorussa di Thunderbird</it>
   <ja>ベラルーシ語版 Thunderbird</ja>
   <kk>Belarusian localisation of Thunderbird</kk>
   <ko>Belarusian localisation of Thunderbird</ko>
   <ku>Belarusian localisation of Thunderbird</ku>
   <lt>Belarusian localisation of Thunderbird</lt>
   <mk>Belarusian localisation of Thunderbird</mk>
   <mr>Belarusian localisation of Thunderbird</mr>
   <nb_NO>Belarusian localisation of Thunderbird</nb_NO>
   <nb>Hviterussisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Belarusian localisation of Thunderbird</nl_BE>
   <nl>Wit-Russische lokalisatie van Thunderbird</nl>
   <or>Belarusian localisation of Thunderbird</or>
   <pl>Białoruska lokalizacja Thunderbirda</pl>
   <pt_BR>Bielorrusso Localização para o Thunderbird</pt_BR>
   <pt>Bielorrusso Localização para Thunderbird</pt>
   <ro>Belarusian localisation of Thunderbird</ro>
   <ru>Belarusian localisation of Thunderbird</ru>
   <sk>Bieloruská lokalizácia pre Thunderbird</sk>
   <sl>Belarusian localisation of Thunderbird</sl>
   <so>Belarusian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në bjellorusisht</sq>
   <sr>Belarusian localisation of Thunderbird</sr>
   <sv>Belarus lokalisering av Thunderbird</sv>
   <th>Belarusian localisation ของ Thunderbird</th>
   <tr>Thunderbird Beyaz Rusça yerelleştirmesi</tr>
   <uk>Belarusian локалізація Thunderbird</uk>
   <vi>Belarusian localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 白俄罗斯语语言包</zh_CN>
   <zh_HK>Belarusian localisation of Thunderbird</zh_HK>
   <zh_TW>Belarusian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-be
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-be
</uninstall_package_names>

</app>
