<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Lithuanian_Default_Firefox_esr
</name>

<description>
   <am>Lithuanian localisation of Firefox ESR</am>
   <ar>Lithuanian localisation of Firefox ESR</ar>
   <be>Lithuanian localisation of Firefox ESR</be>
   <bg>Lithuanian localisation of Firefox ESR</bg>
   <bn>Lithuanian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Lituà</ca>
   <cs>Lithuanian localisation of Firefox ESR</cs>
   <da>Lithuanian localisation of Firefox ESR</da>
   <de>Litauische Lokalisation von “Firefox ESR”</de>
   <el>Λιθουανικά για Firefox ESR</el>
   <en>Lithuanian localisation of Firefox ESR</en>
   <es_ES>Localización Lituano de Firefox ESR</es_ES>
   <es>Localización Lituano de Firefox ESR</es>
   <et>Lithuanian localisation of Firefox ESR</et>
   <eu>Lithuanian localisation of Firefox ESR</eu>
   <fa>Lithuanian localisation of Firefox ESR</fa>
   <fil_PH>Lithuanian localisation of Firefox ESR</fil_PH>
   <fi>Lithuanian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en lituanien pour Firefox ESR</fr_BE>
   <fr>Localisation en lituanien pour Firefox ESR</fr>
   <gl_ES>Lithuanian localisation of Firefox ESR</gl_ES>
   <gu>Lithuanian localisation of Firefox ESR</gu>
   <he_IL>Lithuanian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का लिथुआनियाई संस्करण</hi>
   <hr>Lithuanian localisation of Firefox ESR</hr>
   <hu>Lithuanian localisation of Firefox ESR</hu>
   <id>Lithuanian localisation of Firefox ESR</id>
   <is>Lithuanian localisation of Firefox ESR</is>
   <it>Localizzazione lithuanian di Firefox ESR</it>
   <ja>リトアニア語版 Firefox ESR</ja>
   <kk>Lithuanian localisation of Firefox ESR</kk>
   <ko>Lithuanian localisation of Firefox ESR</ko>
   <ku>Lithuanian localisation of Firefox ESR</ku>
   <lt>Lithuanian localisation of Firefox ESR</lt>
   <mk>Lithuanian localisation of Firefox ESR</mk>
   <mr>Lithuanian localisation of Firefox ESR</mr>
   <nb_NO>Lithuanian localisation of Firefox ESR</nb_NO>
   <nb>Litauisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Lithuanian localisation of Firefox ESR</nl_BE>
   <nl>Lithuanian localisation of Firefox ESR</nl>
   <or>Lithuanian localisation of Firefox ESR</or>
   <pl>Lithuanian localisation of Firefox ESR</pl>
   <pt_BR>Lituano Localização para o Firefox ESR</pt_BR>
   <pt>Lituano Localização para Firefox ESR</pt>
   <ro>Lithuanian localisation of Firefox ESR</ro>
   <ru>Lithuanian localisation of Firefox ESR</ru>
   <sk>Lithuanian localisation of Firefox ESR</sk>
   <sl>Litvanske krajevne nastavitve za Firefox ESR</sl>
   <so>Lithuanian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në lituanisht</sq>
   <sr>Lithuanian localisation of Firefox ESR</sr>
   <sv>Litauisk lokalisering av Firefox ESR</sv>
   <th>Lithuanian localisation of Firefox ESR</th>
   <tr>Firefox ESR Litvanyaca yerelleştirmesi</tr>
   <uk>Lithuanian localisation of Firefox ESR</uk>
   <vi>Lithuanian localisation of Firefox ESR</vi>
   <zh_CN>Lithuanian localisation of Firefox ESR</zh_CN>
   <zh_HK>Lithuanian localisation of Firefox ESR</zh_HK>
   <zh_TW>Lithuanian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-lt
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-lt
</uninstall_package_names>

</app>
