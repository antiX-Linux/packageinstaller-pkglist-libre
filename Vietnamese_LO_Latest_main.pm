<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Vietnamese_LO_Latest_main
</name>

<description>
   <am>Vietnamese Language Meta-Package for LibreOffice</am>
   <ar>Vietnamese Language Meta-Package for LibreOffice</ar>
   <be>Vietnamese Language Meta-Package for LibreOffice</be>
   <bg>Vietnamese Language Meta-Package for LibreOffice</bg>
   <bn>Vietnamese Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Vietnamita per LibreOffice</ca>
   <cs>Vietnamese Language Meta-Package for LibreOffice</cs>
   <da>Vietnamese Language Meta-Package for LibreOffice</da>
   <de>Vietnamesisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Βιετναμέζικα</el>
   <en>Vietnamese Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Vietnamita para LibreOffice</es_ES>
   <es>Metapaquete de idioma Vietnamita para LibreOffice</es>
   <et>Vietnamese Language Meta-Package for LibreOffice</et>
   <eu>Vietnamese Language Meta-Package for LibreOffice</eu>
   <fa>Vietnamese Language Meta-Package for LibreOffice</fa>
   <fil_PH>Vietnamese Language Meta-Package for LibreOffice</fil_PH>
   <fi>Vietnamilainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue vietnamienne pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue vietnamienne pour LibreOffice</fr>
   <gl_ES>Vietnamita Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Vietnamese Language Meta-Package for LibreOffice</gu>
   <he_IL>Vietnamese Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु वियतनामी भाषा मेटा-पैकेज</hi>
   <hr>Vietnamese Language Meta-Package for LibreOffice</hr>
   <hu>Vietnamese Language Meta-Package for LibreOffice</hu>
   <id>Vietnamese Language Meta-Package for LibreOffice</id>
   <is>Vietnamese Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua vietnamita per LibreOffice</it>
   <ja>LibreOffice用のベトナム語メタパッケージ</ja>
   <kk>Vietnamese Language Meta-Package for LibreOffice</kk>
   <ko>Vietnamese Language Meta-Package for LibreOffice</ko>
   <ku>Vietnamese Language Meta-Package for LibreOffice</ku>
   <lt>Vietnamese Language Meta-Package for LibreOffice</lt>
   <mk>Vietnamese Language Meta-Package for LibreOffice</mk>
   <mr>Vietnamese Language Meta-Package for LibreOffice</mr>
   <nb_NO>Vietnamese Language Meta-Package for LibreOffice</nb_NO>
   <nb>Vietnamesisk språkpakke for LibreOffice</nb>
   <nl_BE>Vietnamese Language Meta-Package for LibreOffice</nl_BE>
   <nl>Vietnamese Taal Meta-Pakket voor LibreOffice</nl>
   <or>Vietnamese Language Meta-Package for LibreOffice</or>
   <pl>Wietnamski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Vietnamita Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Vietnamita Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Vietnamese Language Meta-Package for LibreOffice</ro>
   <ru>Vietnamese Language Meta-Package for LibreOffice</ru>
   <sk>Vietnamese Language Meta-Package for LibreOffice</sk>
   <sl>Vietnamski jezikovni meta-paket za LibreOffice</sl>
   <so>Vietnamese Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në vietnamisht</sq>
   <sr>Vietnamese Language Meta-Package for LibreOffice</sr>
   <sv>Vietnamesiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Vietnamese สำหรับ LibreOffice</th>
   <tr>LibreOffice için Vietnam Dili Üst-Paketi</tr>
   <uk>Vietnamese Language Meta-Package for LibreOffice</uk>
   <vi>Vietnamese Language Meta-Package for LibreOffice</vi>
   <zh_CN>Vietnamese Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Vietnamese Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Vietnamese Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-vi
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-vi
libreoffice-gtk3
</uninstall_package_names>

</app>
