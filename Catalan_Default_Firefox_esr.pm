<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Catalan_Default_Firefox_esr
</name>

<description>
   <am>Catalan localisation of Firefox ESR</am>
   <ar>Catalan localisation of Firefox ESR</ar>
   <be>Catalan localisation of Firefox ESR</be>
   <bg>Catalan localisation of Firefox ESR</bg>
   <bn>Catalan localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Català</ca>
   <cs>Catalan localisation of Firefox ESR</cs>
   <da>Catalan localisation of Firefox ESR</da>
   <de>Katalanische Lokalisation von “Firefox ESR”</de>
   <el>Καταλανικά για Firefox ESR</el>
   <en>Catalan localisation of Firefox ESR</en>
   <es_ES>Localización Catalana de Firefox ESR</es_ES>
   <es>Localización Catalán de Firefox ESR</es>
   <et>Catalan localisation of Firefox ESR</et>
   <eu>Catalan localisation of Firefox ESR</eu>
   <fa>Catalan localisation of Firefox ESR</fa>
   <fil_PH>Catalan localisation of Firefox ESR</fil_PH>
   <fi>Catalan localisation of Firefox ESR</fi>
   <fr_BE>Localisation en catalan pour Firefox ESR</fr_BE>
   <fr>Localisation en catalan pour Firefox ESR</fr>
   <gl_ES>Catalan localisation of Firefox ESR</gl_ES>
   <gu>Catalan localisation of Firefox ESR</gu>
   <he_IL>Catalan localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का कैटलन संस्करण</hi>
   <hr>Catalan localisation of Firefox ESR</hr>
   <hu>Catalan localisation of Firefox ESR</hu>
   <id>Catalan localisation of Firefox ESR</id>
   <is>Catalan localisation of Firefox ESR</is>
   <it>Localizzazione catalan di Firefox ESR</it>
   <ja>カタロニア語版 Firefox ESR</ja>
   <kk>Catalan localisation of Firefox ESR</kk>
   <ko>Catalan localisation of Firefox ESR</ko>
   <ku>Catalan localisation of Firefox ESR</ku>
   <lt>Catalan localisation of Firefox ESR</lt>
   <mk>Catalan localisation of Firefox ESR</mk>
   <mr>Catalan localisation of Firefox ESR</mr>
   <nb_NO>Catalan localisation of Firefox ESR</nb_NO>
   <nb>Katalansk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Catalan localisation of Firefox ESR</nl_BE>
   <nl>Catalaanse lokalisatie van Firefox ESR</nl>
   <or>Catalan localisation of Firefox ESR</or>
   <pl>Catalan localisation of Firefox ESR</pl>
   <pt_BR>Catalão Localização para o Firefox ESR</pt_BR>
   <pt>Catalão Localização para Firefox ESR</pt>
   <ro>Catalan localisation of Firefox ESR</ro>
   <ru>Catalan localisation of Firefox ESR</ru>
   <sk>Catalan localisation of Firefox ESR</sk>
   <sl>Katalonske krajevne nastavitve za Firefox ESR</sl>
   <so>Catalan localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në katalanisht</sq>
   <sr>Catalan localisation of Firefox ESR</sr>
   <sv>Katalansk lokalisering av Firefox ESR</sv>
   <th>Catalan localisation of Firefox ESR</th>
   <tr>Firefox ESR Katalanca yerelleştirmesi</tr>
   <uk>Catalan localisation of Firefox ESR</uk>
   <vi>Catalan localisation of Firefox ESR</vi>
   <zh_CN>Catalan localisation of Firefox ESR</zh_CN>
   <zh_HK>Catalan localisation of Firefox ESR</zh_HK>
   <zh_TW>Catalan localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ca
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ca
</uninstall_package_names>

</app>
