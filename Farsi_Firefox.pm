<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Farsi_Firefox
</name>

<description>
   <am>Farsi localisation of Firefox</am>
   <ar>Farsi localisation of Firefox</ar>
   <be>Farsi localisation of Firefox</be>
   <bg>Farsi localisation of Firefox</bg>
   <bn>Farsi localisation of Firefox</bn>
   <ca>Localització de Firefox en Farsi</ca>
   <cs>Farsi localisation of Firefox</cs>
   <da>Farsi oversættelse af Firefox</da>
   <de>Farsi-Lokalisierung von Firefox</de>
   <el>Φαρσί για το Firefox</el>
   <en>Farsi localisation of Firefox</en>
   <es_ES>Localización Farsi de Firefox</es_ES>
   <es>Localización Farsi de Firefox</es>
   <et>Farsi localisation of Firefox</et>
   <eu>Farsi localisation of Firefox</eu>
   <fa>Farsi localisation of Firefox</fa>
   <fil_PH>Farsi localisation of Firefox</fil_PH>
   <fi>Farsilainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en farsi pour Firefox</fr_BE>
   <fr>Localisation en farsi pour Firefox</fr>
   <gl_ES>Localización de Firefox ao persa</gl_ES>
   <gu>Farsi localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לפרסית</he_IL>
   <hi>फायरफॉक्स का फ़ारसी संस्करण</hi>
   <hr>Farsi localisation of Firefox</hr>
   <hu>Farsi localisation of Firefox</hu>
   <id>Farsi localisation of Firefox</id>
   <is>Farsi localisation of Firefox</is>
   <it>Localizzazione persiana di Firefox</it>
   <ja>Firefox のペルシャ語版</ja>
   <kk>Farsi localisation of Firefox</kk>
   <ko>Farsi localisation of Firefox</ko>
   <ku>Farsi localisation of Firefox</ku>
   <lt>Farsi localisation of Firefox</lt>
   <mk>Farsi localisation of Firefox</mk>
   <mr>Farsi localisation of Firefox</mr>
   <nb_NO>Farsi localisation of Firefox</nb_NO>
   <nb>Farsi lokaltilpassing av Firefox</nb>
   <nl_BE>Farsi localisation of Firefox</nl_BE>
   <nl>Perzische lokalisatie van Firefox</nl>
   <or>Farsi localisation of Firefox</or>
   <pl>Farsi lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Persa Localização para o Firefox</pt_BR>
   <pt>Persa Localização para Firefox</pt>
   <ro>Farsi localisation of Firefox</ro>
   <ru>Локализация Firefox на фарси</ru>
   <sk>Farsi localisation of Firefox</sk>
   <sl>Farsi krajevne nastavitve za Firefox</sl>
   <so>Farsi localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në persisht</sq>
   <sr>Farsi localisation of Firefox</sr>
   <sv>Farsi lokalisering av Firefox</sv>
   <th>Farsi localisation ของ Firefox</th>
   <tr>Firefox'un Farsça yerelleştirmesi</tr>
   <uk>Farsi локалізація Firefox</uk>
   <vi>Farsi localisation of Firefox</vi>
   <zh_CN>Firefox 波斯语语言包</zh_CN>
   <zh_HK>Farsi localisation of Firefox</zh_HK>
   <zh_TW>Farsi localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-fa
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-fa
</uninstall_package_names>
</app>
