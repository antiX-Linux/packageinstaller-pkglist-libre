<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Farsi_LO_Latest_full
</name>

<description>
   <am>Farsi Language Meta-Package for LibreOffice</am>
   <ar>Farsi Language Meta-Package for LibreOffice</ar>
   <be>Farsi Language Meta-Package for LibreOffice</be>
   <bg>Farsi Language Meta-Package for LibreOffice</bg>
   <bn>Farsi Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Farsi per LibreOffice</ca>
   <cs>Farsi Language Meta-Package for LibreOffice</cs>
   <da>Farsi Language Meta-Package for LibreOffice</da>
   <de>Farsi Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Φαρσί</el>
   <en>Farsi Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma farsi para LibreOffice</es_ES>
   <es>Metapaquete de idioma Farsi para LibreOffice</es>
   <et>Farsi Language Meta-Package for LibreOffice</et>
   <eu>Farsi Language Meta-Package for LibreOffice</eu>
   <fa>Farsi Language Meta-Package for LibreOffice</fa>
   <fil_PH>Farsi Language Meta-Package for LibreOffice</fil_PH>
   <fi>Farsilainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet de langue farsie pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue farsie pour LibreOffice</fr>
   <gl_ES>Persa Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Farsi Language Meta-Package for LibreOffice</gu>
   <he_IL>Farsi Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु फ़ारसी भाषा मेटा-पैकेज</hi>
   <hr>Farsi Language Meta-Package for LibreOffice</hr>
   <hu>Farsi Language Meta-Package for LibreOffice</hu>
   <id>Farsi Language Meta-Package for LibreOffice</id>
   <is>Farsi Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua persiana per LibreOffice</it>
   <ja>LibreOffice用のペルシャ語メタパッケージ</ja>
   <kk>Farsi Language Meta-Package for LibreOffice</kk>
   <ko>Farsi Language Meta-Package for LibreOffice</ko>
   <ku>Farsi Language Meta-Package for LibreOffice</ku>
   <lt>Farsi Language Meta-Package for LibreOffice</lt>
   <mk>Farsi Language Meta-Package for LibreOffice</mk>
   <mr>Farsi Language Meta-Package for LibreOffice</mr>
   <nb_NO>Farsi Language Meta-Package for LibreOffice</nb_NO>
   <nb>Farsi språkpakke for LibreOffice</nb>
   <nl_BE>Farsi Language Meta-Package for LibreOffice</nl_BE>
   <nl>Farsisch Taal Meta-Pakket voor LibreOffice</nl>
   <or>Farsi Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Farsi dla LibreOffice</pl>
   <pt_BR>Persa Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Persa Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Farsi Language Meta-Package for LibreOffice</ro>
   <ru>Farsi Language Meta-Package for LibreOffice</ru>
   <sk>Farsi Language Meta-Package for LibreOffice</sk>
   <sl>Farsi jezikovni meta-paket za LibreOffice</sl>
   <so>Farsi Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në persisht</sq>
   <sr>Farsi Language Meta-Package for LibreOffice</sr>
   <sv>Farsi Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Farsi สำหรับ LibreOffice</th>
   <tr>LibreOffice için Farsça Dili Üst-Paketi</tr>
   <uk>Farsi Language Meta-Package for LibreOffice</uk>
   <vi>Farsi Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 波斯语语言包</zh_CN>
   <zh_HK>Farsi Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Farsi Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-fa
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-fa
</uninstall_package_names>

</app>
