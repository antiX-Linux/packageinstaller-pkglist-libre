<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Wallpapers
</category>

<name>
antiX 19 Wallpapers
</name>

<description>
   <am>backgrounds originally supplied with antiX 19</am>
   <ar>backgrounds originally supplied with antiX 19</ar>
   <bg>backgrounds originally supplied with antiX 19</bg>
   <bn>backgrounds originally supplied with antiX 19</bn>
   <ca>Fons de pantalla subministrats amb antiX 19</ca>
   <cs>backgrounds originally supplied with antiX 19</cs>
   <da>baggrunde fra antiX 19</da>
   <de>Bildschirmhintergründe, die ursprünglich mit antiX 19 geliefert wurden</de>
   <el>επιφάνεια εργασίας που παρέχεται αρχικά με antiX 19</el>
   <en>backgrounds originally supplied with antiX 19</en>
   <es>Fondos de escritorio suministrados originalmente con antiX 19</es>
   <et>backgrounds originally supplied with antiX 19</et>
   <eu>backgrounds originally supplied with antiX 19</eu>
   <fa>backgrounds originally supplied with antiX 19</fa>
   <fil_PH>backgrounds originally supplied with antiX 19</fil_PH>
   <fi>backgrounds originally supplied with antiX 19</fi>
   <fr>Fonds d'écran initialement fournis avec antiX 19</fr>
   <he_IL>backgrounds originally supplied with antiX 19</he_IL>
   <hi>backgrounds originally supplied with antiX 19</hi>
   <hr>backgrounds originally supplied with antiX 19</hr>
   <hu>backgrounds originally supplied with antiX 19</hu>
   <id>backgrounds originally supplied with antiX 19</id>
   <is>backgrounds originally supplied with antiX 19</is>
   <it>sfondi originariamente forniti con antiX 19</it>
   <ja_JP>backgrounds originally supplied with antiX 19</ja_JP>
   <ja>backgrounds originally supplied with antiX 19</ja>
   <kk>backgrounds originally supplied with antiX 19</kk>
   <ko>backgrounds originally supplied with antiX 19</ko>
   <lt>backgrounds originally supplied with antiX 19</lt>
   <mk>backgrounds originally supplied with antiX 19</mk>
   <mr>backgrounds originally supplied with antiX 19</mr>
   <nb>backgrounds originally supplied with antiX 19</nb>
   <nl>achtergronden oorspronkelijk geleverd met antiX 19</nl>
   <pl>tła oryginalnie dostarczone z antiX 19</pl>
   <pt_BR>Imagens de fundo originalmente incluídas no antiX 19</pt_BR>
   <pt>Imagens de fundo originalmente incluídas no antiX 19</pt>
   <ro>backgrounds originally supplied with antiX 19</ro>
   <ru>Обои первоначально входившие в antiX 19</ru>
   <sk>backgrounds originally supplied with antiX 19</sk>
   <sl>Izvirna ozadja za antiX 19</sl>
   <sq>backgrounds originally supplied with antiX 19</sq>
   <sr>backgrounds originally supplied with antiX 19</sr>
   <sv>wallpapers ursprungligen medföljande antiX 19</sv>
   <tr>backgrounds originally supplied with antiX 19</tr>
   <uk>backgrounds originally supplied with antiX 19</uk>
   <vi>backgrounds originally supplied with antiX 19</vi>
   <zh_CN>backgrounds originally supplied with antiX 19</zh_CN>
   <zh_TW>backgrounds originally supplied with antiX 19</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
artwork19-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
artwork19-antix
</uninstall_package_names>
</app>
