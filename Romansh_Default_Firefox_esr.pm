<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Romansh_Default_Firefox_esr
</name>

<description>
   <am>Romansh localisation of Firefox ESR</am>
   <ar>Romansh localisation of Firefox ESR</ar>
   <be>Romansh localisation of Firefox ESR</be>
   <bg>Romansh localisation of Firefox ESR</bg>
   <bn>Romansh localisation of Firefox ESR</bn>
   <ca>Localització en Romanx de Firefox ESR</ca>
   <cs>Romansh localisation of Firefox ESR</cs>
   <da>Romansh localisation of Firefox ESR</da>
   <de>Romansh localisation of Firefox ESR</de>
   <el>Romansh για Firefox ESR</el>
   <en>Romansh localisation of Firefox ESR</en>
   <es_ES>Localización en romanche de Firefox ESR</es_ES>
   <es>Localización en romanche de Firefox ESR</es>
   <et>Romansh localisation of Firefox ESR</et>
   <eu>Romansh localisation of Firefox ESR</eu>
   <fa>Romansh localisation of Firefox ESR</fa>
   <fil_PH>Romansh localisation of Firefox ESR</fil_PH>
   <fi>Romansh localisation of Firefox ESR</fi>
   <fr_BE>Localisation romanche pour Firefox ESR</fr_BE>
   <fr>Localisation romanche pour Firefox ESR</fr>
   <gl_ES>Romansh localisation of Firefox ESR</gl_ES>
   <gu>Romansh localisation of Firefox ESR</gu>
   <he_IL>Romansh localisation of Firefox ESR</he_IL>
   <hi>Romansh localisation of Firefox ESR</hi>
   <hr>Romansh localisation of Firefox ESR</hr>
   <hu>Romansh localisation of Firefox ESR</hu>
   <id>Romansh localisation of Firefox ESR</id>
   <is>Romansh localisation of Firefox ESR</is>
   <it>Localizzazione Romansh di Firefox ESR</it>
   <ja>Romansh localisation of Firefox ESR</ja>
   <kk>Romansh localisation of Firefox ESR</kk>
   <ko>Romansh localisation of Firefox ESR</ko>
   <ku>Romansh localisation of Firefox ESR</ku>
   <lt>Romansh localisation of Firefox ESR</lt>
   <mk>Romansh localisation of Firefox ESR</mk>
   <mr>Romansh localisation of Firefox ESR</mr>
   <nb_NO>Romansh localisation of Firefox ESR</nb_NO>
   <nb>Romansh lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Romansh localisation of Firefox ESR</nl_BE>
   <nl>Romansh localisation of Firefox ESR</nl>
   <or>Romansh localisation of Firefox ESR</or>
   <pl>Romansh localisation of Firefox ESR</pl>
   <pt_BR>Romanche Localização para o Firefox ESR</pt_BR>
   <pt>Romanche Localização para Firefox ESR</pt>
   <ro>Romansh localisation of Firefox ESR</ro>
   <ru>Romansh localisation of Firefox ESR</ru>
   <sk>Romansh localisation of Firefox ESR</sk>
   <sl>Retoromanske krajevne nastavitve za Firefox ESR</sl>
   <so>Romansh localisation of Firefox ESR</so>
   <sq>Romansh localisation of Firefox ESR</sq>
   <sr>Romansh localisation of Firefox ESR</sr>
   <sv>Romansk lokalisering av Firefox ESR</sv>
   <th>Romansh localisation of Firefox ESR</th>
   <tr>Romansh localisation of Firefox ESR</tr>
   <uk>Romansh localisation of Firefox ESR</uk>
   <vi>Romansh localisation of Firefox ESR</vi>
   <zh_CN>Romansh localisation of Firefox ESR</zh_CN>
   <zh_HK>Romansh localisation of Firefox ESR</zh_HK>
   <zh_TW>Romansh localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-rm
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-rm
</uninstall_package_names>

</app>
