<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
Play on Linux
</name>

<description>
   <am>Play on Linux</am>
   <ar>Play on Linux</ar>
   <be>Play on Linux</be>
   <bg>Play on Linux</bg>
   <bn>Play on Linux</bn>
   <ca>Play on Linux</ca>
   <cs>Play on Linux</cs>
   <da>Play on Linux</da>
   <de>Ein grafisches Frontend für Wine</de>
   <el>Play on Linux</el>
   <en>Play on Linux</en>
   <es_ES>Reproducir en Linux</es_ES>
   <es>Play on Linux</es>
   <et>Play on Linux</et>
   <eu>Play on Linux</eu>
   <fa>Play on Linux</fa>
   <fil_PH>Play on Linux</fil_PH>
   <fi>Play on Linux - voit pelata monia vain Windowsille tehtyjä pelejä Linux:illa</fi>
   <fr_BE>Play on Linux</fr_BE>
   <fr>Play on Linux</fr>
   <gl_ES>Instalar programas do Windows en sistemas Linux</gl_ES>
   <gu>Play on Linux</gu>
   <he_IL>Play on Linux</he_IL>
   <hi>प्ले ऑन लिनक्स</hi>
   <hr>Play on Linux</hr>
   <hu>Play on Linux</hu>
   <id>Play on Linux</id>
   <is>Play on Linux</is>
   <it>Play on Linux</it>
   <ja>Linux でプレイ</ja>
   <kk>Play on Linux</kk>
   <ko>Play on Linux</ko>
   <ku>Play on Linux</ku>
   <lt>Play on Linux</lt>
   <mk>Play on Linux</mk>
   <mr>Play on Linux</mr>
   <nb_NO>Play on Linux</nb_NO>
   <nb>Spill på Linux</nb>
   <nl_BE>Play on Linux</nl_BE>
   <nl>Speel op Linux</nl>
   <or>Play on Linux</or>
   <pl>umożliwia łatwą instalację i używanie wielu gier i oprogramowania</pl>
   <pt_BR>Play on Linux - Instala ou executa programas do Windows em sistemas operacionais GNU/Linux</pt_BR>
   <pt>Instalar programas do Windows em sistemas Linux</pt>
   <ro>Play on Linux</ro>
   <ru>Надстройка для запуска игр в окружении Wine</ru>
   <sk>Play on Linux</sk>
   <sl>Igraj v Linuxu</sl>
   <so>Play on Linux</so>
   <sq>Play on Linux</sq>
   <sr>Play on Linux</sr>
   <sv>Play on Linux</sv>
   <th>Play on Linux</th>
   <tr>Play on Linux</tr>
   <uk>Play on Linux</uk>
   <vi>Play on Linux</vi>
   <zh_CN>Play on Linux</zh_CN>
   <zh_HK>Play on Linux</zh_HK>
   <zh_TW>Play on Linux</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
playonlinux
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
playonlinux
</uninstall_package_names>
</app>
