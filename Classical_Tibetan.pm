<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Classical Tibetan
</name>

<description>
   <am>Classical Tibetan dictionary for hunspell</am>
   <ar>Classical Tibetan dictionary for hunspell</ar>
   <be>Classical Tibetan dictionary for hunspell</be>
   <bg>Classical Tibetan dictionary for hunspell</bg>
   <bn>Classical Tibetan dictionary for hunspell</bn>
   <ca>Diccionari Tibetà Clàssic per hunspell</ca>
   <cs>Classical Tibetan dictionary for hunspell</cs>
   <da>Classical Tibetan dictionary for hunspell</da>
   <de>Wörterbuch für klassisches Tibetisch für “Hunspell”</de>
   <el>Κλασικό θιβετιανό λεξικό για hunspell</el>
   <en>Classical Tibetan dictionary for hunspell</en>
   <es_ES>Diccionario Tibetano clásico para hunspell</es_ES>
   <es>Diccionario Tibetano clásico para hunspell</es>
   <et>Classical Tibetan dictionary for hunspell</et>
   <eu>Classical Tibetan dictionary for hunspell</eu>
   <fa>Classical Tibetan dictionary for hunspell</fa>
   <fil_PH>Classical Tibetan dictionary for hunspell</fil_PH>
   <fi>Classical Tibetan dictionary for hunspell</fi>
   <fr_BE>Tibétain classique dictionnaire pour hunspell</fr_BE>
   <fr>Tibétain classique dictionnaire pour hunspell</fr>
   <gl_ES>Classical Tibetan dictionary for hunspell</gl_ES>
   <gu>Classical Tibetan dictionary for hunspell</gu>
   <he_IL>Classical Tibetan dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु पारंपरिक तिब्बती शब्दकोष</hi>
   <hr>Classical Tibetan dictionary for hunspell</hr>
   <hu>Classical Tibetan dictionary for hunspell</hu>
   <id>Classical Tibetan dictionary for hunspell</id>
   <is>Classical Tibetan dictionary for hunspell</is>
   <it>Dizionario di Tibetano-classico per hunspell</it>
   <ja>Hunspell 用古代チベット語辞書</ja>
   <kk>Classical Tibetan dictionary for hunspell</kk>
   <ko>Classical Tibetan dictionary for hunspell</ko>
   <ku>Classical Tibetan dictionary for hunspell</ku>
   <lt>Classical Tibetan dictionary for hunspell</lt>
   <mk>Classical Tibetan dictionary for hunspell</mk>
   <mr>Classical Tibetan dictionary for hunspell</mr>
   <nb_NO>Classical Tibetan dictionary for hunspell</nb_NO>
   <nb>Klassisk tibetansk ordliste for hunspell</nb>
   <nl_BE>Classical Tibetan dictionary for hunspell</nl_BE>
   <nl>Klassiek Tibetaans woordenboek voor hunspell</nl>
   <or>Classical Tibetan dictionary for hunspell</or>
   <pl>Classical Tibetan dictionary for hunspell</pl>
   <pt_BR>Dicionário Tibetano Clássico para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Tibetano Clássico para hunspell</pt>
   <ro>Classical Tibetan dictionary for hunspell</ro>
   <ru>Classical Tibetan dictionary for hunspell</ru>
   <sk>Classical Tibetan dictionary for hunspell</sk>
   <sl>Klasično tibetanski slovar za hunspell</sl>
   <so>Classical Tibetan dictionary for hunspell</so>
   <sq>Fjalor tibetanishte klasike për hunspell</sq>
   <sr>Classical Tibetan dictionary for hunspell</sr>
   <sv>Klassisk Tibetansk ordbok för hunspell</sv>
   <th>Classical Tibetan dictionary for hunspell</th>
   <tr>Hunspell için Eski Tibetçe sözlük</tr>
   <uk>Classical Tibetan dictionary for hunspell</uk>
   <vi>Classical Tibetan dictionary for hunspell</vi>
   <zh_CN>Classical Tibetan dictionary for hunspell</zh_CN>
   <zh_HK>Classical Tibetan dictionary for hunspell</zh_HK>
   <zh_TW>Classical Tibetan dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-bo
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-bo
</uninstall_package_names>

</app>
