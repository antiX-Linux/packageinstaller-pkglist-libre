<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Remote Access
</category>

<name>
Remmina
</name>

<description>
   <am>Remote desktop client written in GTK+</am>
   <ar>Remote desktop client written in GTK+</ar>
   <be>Remote desktop client written in GTK+</be>
   <bg>Remote desktop client written in GTK+</bg>
   <bn>Remote desktop client written in GTK+</bn>
   <ca>Client d'escriptori remot escrit en GTK+</ca>
   <cs>Remote desktop client written in GTK+</cs>
   <da>Remote desktop client written in GTK+</da>
   <de>Client für Fernzugriff auf den Desktop (verwendet die GTK Programmbibliothek)</de>
   <el>Πελάτης απομακρυσμένης επιφάνειας εργασίας γραμμένο σε GTK+</el>
   <en>Remote desktop client written in GTK+</en>
   <es_ES>Cliente de escritorio remoto escrito en GTK+</es_ES>
   <es>Cliente de escritorio remoto escrito en GTK+</es>
   <et>Remote desktop client written in GTK+</et>
   <eu>Remote desktop client written in GTK+</eu>
   <fa>Remote desktop client written in GTK+</fa>
   <fil_PH>Remote desktop client written in GTK+</fil_PH>
   <fi>GTK+-pohjainen etätyöpöydänhallinta</fi>
   <fr_BE>Client de bureau à distance écrit en GTK+</fr_BE>
   <fr>Client de bureau à distance écrit en GTK+</fr>
   <gl_ES>Remote desktop client written in GTK+</gl_ES>
   <gu>Remote desktop client written in GTK+</gu>
   <he_IL>Remote desktop client written in GTK+</he_IL>
   <hi>जीटीके+ में रचित दूरस्थ डेस्कटॉप साधन</hi>
   <hr>Remote desktop client written in GTK+</hr>
   <hu>Remote desktop client written in GTK+</hu>
   <id>Remote desktop client written in GTK+</id>
   <is>Remote desktop client written in GTK+</is>
   <it>Client per desktop remoto scritto in GTK +</it>
   <ja>GTK+で作られたリモートデスクトップ・クライアント</ja>
   <kk>Remote desktop client written in GTK+</kk>
   <ko>Remote desktop client written in GTK+</ko>
   <ku>Remote desktop client written in GTK+</ku>
   <lt>Remote desktop client written in GTK+</lt>
   <mk>Remote desktop client written in GTK+</mk>
   <mr>Remote desktop client written in GTK+</mr>
   <nb_NO>Remote desktop client written in GTK+</nb_NO>
   <nb>Klient for eksternt skrivebord skrevet i GTK+</nb>
   <nl_BE>Remote desktop client written in GTK+</nl_BE>
   <nl>Remote desktop client written in GTK+</nl>
   <or>Remote desktop client written in GTK+</or>
   <pl>Remote desktop client written in GTK+</pl>
   <pt_BR>Cliente de área de trabalho remoto escrito em GTK+</pt_BR>
   <pt>Cliente para desktop remoto escrito em GTK+</pt>
   <ro>Remote desktop client written in GTK+</ro>
   <ru>Remote desktop client written in GTK+</ru>
   <sk>Remote desktop client written in GTK+</sk>
   <sl>Odejamlec za oddaljeni dostop napisan v GTK+</sl>
   <so>Remote desktop client written in GTK+</so>
   <sq>Klient desktopi të largët, shkruar në GTK+</sq>
   <sr>Remote desktop client written in GTK+</sr>
   <sv>Fjärrskrivbords-klient skriven i GTK+</sv>
   <th>Remote desktop client written in GTK+</th>
   <tr>GTK+ ile yazılmış uzak masaüstü istemcisi</tr>
   <uk>Remote desktop client written in GTK+</uk>
   <vi>Remote desktop client written in GTK+</vi>
   <zh_CN>Remote desktop client written in GTK+</zh_CN>
   <zh_HK>Remote desktop client written in GTK+</zh_HK>
   <zh_TW>Remote desktop client written in GTK+</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
remmina
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
remmina
</uninstall_package_names>
</app>
