<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Galician
</name>

<description>
   <am>Galician dictionary for hunspell</am>
   <ar>Galician dictionary for hunspell</ar>
   <be>Galician dictionary for hunspell</be>
   <bg>Galician dictionary for hunspell</bg>
   <bn>Galician dictionary for hunspell</bn>
   <ca>Diccionari Gallec per hunspell</ca>
   <cs>Galician dictionary for hunspell</cs>
   <da>Galician dictionary for hunspell</da>
   <de>Galegisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό της Γαλικίας για hunspell</el>
   <en>Galician dictionary for hunspell</en>
   <es_ES>Diccionario Gallego para hunspell</es_ES>
   <es>Diccionario Gallego para hunspell</es>
   <et>Galician dictionary for hunspell</et>
   <eu>Galician dictionary for hunspell</eu>
   <fa>Galician dictionary for hunspell</fa>
   <fil_PH>Galician dictionary for hunspell</fil_PH>
   <fi>Galician dictionary for hunspell</fi>
   <fr_BE>Galicien dictionnaire pour hunspell</fr_BE>
   <fr>Galicien dictionnaire pour hunspell</fr>
   <gl_ES>Galician dictionary for hunspell</gl_ES>
   <gu>Galician dictionary for hunspell</gu>
   <he_IL>Galician dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु गलिसियाई शब्दकोष</hi>
   <hr>Galician dictionary for hunspell</hr>
   <hu>Galician dictionary for hunspell</hu>
   <id>Galician dictionary for hunspell</id>
   <is>Galician dictionary for hunspell</is>
   <it>Dizionario galiziano per hunspell</it>
   <ja>Hunspell 用ガリシア語辞書</ja>
   <kk>Galician dictionary for hunspell</kk>
   <ko>Galician dictionary for hunspell</ko>
   <ku>Galician dictionary for hunspell</ku>
   <lt>Galician dictionary for hunspell</lt>
   <mk>Galician dictionary for hunspell</mk>
   <mr>Galician dictionary for hunspell</mr>
   <nb_NO>Galician dictionary for hunspell</nb_NO>
   <nb>Galisisk ordliste for hunspell</nb>
   <nl_BE>Galician dictionary for hunspell</nl_BE>
   <nl>Galicisch woordenboek voor hunspell</nl>
   <or>Galician dictionary for hunspell</or>
   <pl>Galician dictionary for hunspell</pl>
   <pt_BR>Dicionário Galego para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Galego para hunspell</pt>
   <ro>Galician dictionary for hunspell</ro>
   <ru>Galician dictionary for hunspell</ru>
   <sk>Galician dictionary for hunspell</sk>
   <sl>Galicijski slovar za hunspell</sl>
   <so>Galician dictionary for hunspell</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në galicisht</sq>
   <sr>Galician dictionary for hunspell</sr>
   <sv>Galicisk ordbok för hunspell</sv>
   <th>Galician dictionary for hunspell</th>
   <tr>Hunspell için Galiçyaca sözlük</tr>
   <uk>Galician dictionary for hunspell</uk>
   <vi>Galician dictionary for hunspell</vi>
   <zh_CN>Galician dictionary for hunspell</zh_CN>
   <zh_HK>Galician dictionary for hunspell</zh_HK>
   <zh_TW>Galician dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-gl-es
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-gl-es
</uninstall_package_names>

</app>
