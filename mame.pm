<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
MAME
</name>

<description>
   <en>Multiple Arcade Machine Emulator</en>
</description>

<installable>
all
</installable>

<screenshot>
</screenshot>

<preinstall>

</preinstall>

<install_package_names>
mame
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
mame
</uninstall_package_names>
</app>
