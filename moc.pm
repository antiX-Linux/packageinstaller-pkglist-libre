<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Audio
</category>

<name>
MOC
</name>

<description>
   <am>a console audio player designed to be powerful and easy to use</am>
   <ar>a console audio player designed to be powerful and easy to use</ar>
   <be>a console audio player designed to be powerful and easy to use</be>
   <bg>a console audio player designed to be powerful and easy to use</bg>
   <bn>a console audio player designed to be powerful and easy to use</bn>
   <ca>reproductor d'àudio dissenyat per a ser potent i fàcil d'usar</ca>
   <cs>a console audio player designed to be powerful and easy to use</cs>
   <da>en konsol-lydafspiller som er designet til at være kraftfuld og let at bruge</da>
   <de>Ein Konsolen-Audio-Player, der leistungsstark und einfach zu bedienen ist.</de>
   <el>Κονσόλα ήχου που έχει σχεδιαστεί για να είναι ισχυρή και εύκολη στη χρήση</el>
   <en>a console audio player designed to be powerful and easy to use</en>
   <es_ES>Reproductor de audio para consola de comandos potente y fácil de usar</es_ES>
   <es>Reproductor de audio para consola de comandos potente y fácil de usar</es>
   <et>a console audio player designed to be powerful and easy to use</et>
   <eu>a console audio player designed to be powerful and easy to use</eu>
   <fa>a console audio player designed to be powerful and easy to use</fa>
   <fil_PH>a console audio player designed to be powerful and easy to use</fil_PH>
   <fi>konsoli-äänisoitin joka on suunniteltu tehokkaaksi ja helpoksi käyttää</fi>
   <fr_BE>Un lecteur audio en console puissant et simple d'utilisation</fr_BE>
   <fr>Un lecteur audio en console puissant et simple d'utilisation</fr>
   <gl_ES>reprodutor de audio en consola concibido para ser poderso e fácil de usar</gl_ES>
   <gu>a console audio player designed to be powerful and easy to use</gu>
   <he_IL>a console audio player designed to be powerful and easy to use</he_IL>
   <hi>सक्षम व उपयोग में सरल कंसोल ऑडियो प्लेयर</hi>
   <hr>a console audio player designed to be powerful and easy to use</hr>
   <hu>a console audio player designed to be powerful and easy to use</hu>
   <id>a console audio player designed to be powerful and easy to use</id>
   <is>a console audio player designed to be powerful and easy to use</is>
   <it>un lettore audio da terminale progettato per essere completo e facile da usare</it>
   <ja>パワフルで容易なコンソール用のオーディオプレーヤー</ja>
   <kk>a console audio player designed to be powerful and easy to use</kk>
   <ko>a console audio player designed to be powerful and easy to use</ko>
   <ku>a console audio player designed to be powerful and easy to use</ku>
   <lt>a console audio player designed to be powerful and easy to use</lt>
   <mk>a console audio player designed to be powerful and easy to use</mk>
   <mr>a console audio player designed to be powerful and easy to use</mr>
   <nb_NO>a console audio player designed to be powerful and easy to use</nb_NO>
   <nb>kraftig og enkel terminalbasert lydavspiller</nb>
   <nl_BE>a console audio player designed to be powerful and easy to use</nl_BE>
   <nl>een krachtige en gebruiksvriendelijke audiospeler voor de console</nl>
   <or>a console audio player designed to be powerful and easy to use</or>
   <pl>konsolowy odtwarzacz audio zaprojektowany tak, aby był potężny i łatwy w użyciu</pl>
   <pt_BR>Reprodutor de áudio em console/terminal projetado para ser poderoso e fácil de usar</pt_BR>
   <pt>Reprodutor de áudio em consola/terminal concebido para ser poderoso e fácil de usar</pt>
   <ro>a console audio player designed to be powerful and easy to use</ro>
   <ru>Консольный аудиоплеер созданный для простоты использования и богатого функционала</ru>
   <sk>a console audio player designed to be powerful and easy to use</sk>
   <sl>Zvočni predvajalnik v konzoli, ki je enostaven a zmogljiv</sl>
   <so>a console audio player designed to be powerful and easy to use</so>
   <sq>Një lojtës audiosh që nga konsola, i konceptuar për të qenë i fuqishëm dhe i lehtë për t’u përdorur</sq>
   <sr>a console audio player designed to be powerful and easy to use</sr>
   <sv>en konsol-ljudspelare designad för att vara kraftfull och lättanvänd</sv>
   <th>a console audio player designed to be powerful and easy to use</th>
   <tr>güçlü ve kullanımı kolay olacak şekilde tasarlanmış bir uçbirim ses çaları</tr>
   <uk>консольний програвач аудіо створений що бути потужним та простим у використанні</uk>
   <vi>a console audio player designed to be powerful and easy to use</vi>
   <zh_CN>a console audio player designed to be powerful and easy to use</zh_CN>
   <zh_HK>a console audio player designed to be powerful and easy to use</zh_HK>
   <zh_TW>a console audio player designed to be powerful and easy to use</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
moc
moc-ffmpeg-plugin
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
moc
moc-ffmpeg-plugin
</uninstall_package_names>
</app>
