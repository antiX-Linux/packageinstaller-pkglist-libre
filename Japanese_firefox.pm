<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Japanese_Firefox
</name>

<description>
   <am>Japanese localisation of Firefox</am>
   <ar>Japanese localisation of Firefox</ar>
   <be>Japanese localisation of Firefox</be>
   <bg>Japanese localisation of Firefox</bg>
   <bn>Japanese localisation of Firefox</bn>
   <ca>Localització de Firefox en Japonès</ca>
   <cs>Japanese localisation of Firefox</cs>
   <da>Japansk oversættelse af Firefox</da>
   <de>Japanische Lokalisierung von Firefox</de>
   <el>Ιαπωνικά για το Firefox</el>
   <en>Japanese localisation of Firefox</en>
   <es_ES>Localización Japonesa de Firefox</es_ES>
   <es>Localización Japonés de Firefox</es>
   <et>Japanese localisation of Firefox</et>
   <eu>Japanese localisation of Firefox</eu>
   <fa>Japanese localisation of Firefox</fa>
   <fil_PH>Japanese localisation of Firefox</fil_PH>
   <fi>Japanilainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en japonais pour Firefox</fr_BE>
   <fr>Localisation en japonais pour Firefox</fr>
   <gl_ES>Localización de Firefox ao xaponés</gl_ES>
   <gu>Japanese localisation of Firefox</gu>
   <he_IL>תרגומי Firefox ליפנית</he_IL>
   <hi>फायरफॉक्स का जापानी संस्करण</hi>
   <hr>Japanese localisation of Firefox</hr>
   <hu>Japanese localisation of Firefox</hu>
   <id>Japanese localisation of Firefox</id>
   <is>Japanese localisation of Firefox</is>
   <it>Localizzazione giapponese di Firefox</it>
   <ja>Firefox の日本語版</ja>
   <kk>Japanese localisation of Firefox</kk>
   <ko>Japanese localisation of Firefox</ko>
   <ku>Japanese localisation of Firefox</ku>
   <lt>Japanese localisation of Firefox</lt>
   <mk>Japanese localisation of Firefox</mk>
   <mr>Japanese localisation of Firefox</mr>
   <nb_NO>Japanese localisation of Firefox</nb_NO>
   <nb>Japansk lokaltilpassing av Firefox</nb>
   <nl_BE>Japanese localisation of Firefox</nl_BE>
   <nl>Japanse lokalisatie van Firefox</nl>
   <or>Japanese localisation of Firefox</or>
   <pl>Japońska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Japonês Localização para o Firefox</pt_BR>
   <pt>Japonês Localização para Firefox</pt>
   <ro>Japanese localisation of Firefox</ro>
   <ru>Японская локализация Firefox</ru>
   <sk>Japanese localisation of Firefox</sk>
   <sl>Japonske krajevne nastavitve za Firefox</sl>
   <so>Japanese localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në japonisht</sq>
   <sr>Japanese localisation of Firefox</sr>
   <sv>Japansk lokalisering av Firefox</sv>
   <th>Japanese localisation ของ Firefox</th>
   <tr>Firefox'un Japonca yerelleştirmesi</tr>
   <uk>Japanese локалізація Firefox</uk>
   <vi>Japanese localisation of Firefox</vi>
   <zh_CN>Japanese localisation of Firefox</zh_CN>
   <zh_HK>Japanese localisation of Firefox</zh_HK>
   <zh_TW>Japanese localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-ja
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-ja
</uninstall_package_names>
</app>
