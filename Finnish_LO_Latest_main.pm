<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Finnish_LO_Latest_main
</name>

<description>
   <am>Finnish Language Meta-Package for LibreOffice</am>
   <ar>Finnish Language Meta-Package for LibreOffice</ar>
   <be>Finnish Language Meta-Package for LibreOffice</be>
   <bg>Finnish Language Meta-Package for LibreOffice</bg>
   <bn>Finnish Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Finès per LibreOffice</ca>
   <cs>Finnish Language Meta-Package for LibreOffice</cs>
   <da>Finnish Language Meta-Package for LibreOffice</da>
   <de>Finnisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Φινλανδικά</el>
   <en>Finnish Language Meta-Package for LibreOffice</en>
   <es_ES>Meta Paquete de idioma finlandés para LibreOffice</es_ES>
   <es>Metapaquete de idioma Finés para LibreOffice</es>
   <et>Finnish Language Meta-Package for LibreOffice</et>
   <eu>Finnish Language Meta-Package for LibreOffice</eu>
   <fa>Finnish Language Meta-Package for LibreOffice</fa>
   <fil_PH>Finnish Language Meta-Package for LibreOffice</fil_PH>
   <fi>Suomenkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet de langue finnoise pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue finnoise pour LibreOffice</fr>
   <gl_ES>Finés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Finnish Language Meta-Package for LibreOffice</gu>
   <he_IL>Finnish Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु फिन्निश भाषा मेटा-पैकेज</hi>
   <hr>Finnish Language Meta-Package for LibreOffice</hr>
   <hu>Finnish Language Meta-Package for LibreOffice</hu>
   <id>Finnish Language Meta-Package for LibreOffice</id>
   <is>Finnish Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua finlandese per LibreOffice</it>
   <ja>LibreOffice用のフィンランド語メタパッケージ</ja>
   <kk>Finnish Language Meta-Package for LibreOffice</kk>
   <ko>Finnish Language Meta-Package for LibreOffice</ko>
   <ku>Finnish Language Meta-Package for LibreOffice</ku>
   <lt>Finnish Language Meta-Package for LibreOffice</lt>
   <mk>Finnish Language Meta-Package for LibreOffice</mk>
   <mr>Finnish Language Meta-Package for LibreOffice</mr>
   <nb_NO>Finnish Language Meta-Package for LibreOffice</nb_NO>
   <nb>Finsk språkpakke for LibreOffice</nb>
   <nl_BE>Finnish Language Meta-Package for LibreOffice</nl_BE>
   <nl>Fins Taal Meta-Pakket voor LibreOffice</nl>
   <or>Finnish Language Meta-Package for LibreOffice</or>
   <pl>Fiński meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Finlandês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Finlandês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Finnish Language Meta-Package for LibreOffice</ro>
   <ru>Finnish Language Meta-Package for LibreOffice</ru>
   <sk>Finnish Language Meta-Package for LibreOffice</sk>
   <sl>Finska jezikovni meta-paket za LibreOffice</sl>
   <so>Finnish Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në finlandisht</sq>
   <sr>Finnish Language Meta-Package for LibreOffice</sr>
   <sv>Finskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Finnish สำหรับ LibreOffice</th>
   <tr>LibreOffice için Fince Dili Üst-Paketi</tr>
   <uk>Finnish Language Meta-Package for LibreOffice</uk>
   <vi>Finnish Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 芬兰语语言包</zh_CN>
   <zh_HK>Finnish Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Finnish Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-fi
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-fi
libreoffice-gtk3
</uninstall_package_names>

</app>
