<?xml version="1.0" encoding="UTF-8"?>
<app>


<category>
Screencast
</category>

<name>
SimpleScreenRecorder
</name>

<description>
   <am>a Qt-based screencast recorder</am>
   <ar>a Qt-based screencast recorder</ar>
   <be>a Qt-based screencast recorder</be>
   <bg>a Qt-based screencast recorder</bg>
   <bn>a Qt-based screencast recorder</bn>
   <ca>Enregistrador de pantalla basat en QT</ca>
   <cs>a Qt-based screencast recorder</cs>
   <da>en Qt-baseret screencast-optager</da>
   <de>Ein Qt-basierter Bildschirm-Rekorder</de>
   <el>Καταγραφή εκπομπής με βάση το Qt</el>
   <en>a Qt-based screencast recorder</en>
   <es_ES>Grabador de pantalla basado en Qt</es_ES>
   <es>Grabador de pantalla basado en Qt</es>
   <et>a Qt-based screencast recorder</et>
   <eu>a Qt-based screencast recorder</eu>
   <fa>a Qt-based screencast recorder</fa>
   <fil_PH>a Qt-based screencast recorder</fil_PH>
   <fi>Qt-pohjainen ruudunjako- ja tallennusohjelma</fi>
   <fr_BE>Outil de capture vidéo basé sur Qt</fr_BE>
   <fr>Outil de capture vidéo basé sur Qt</fr>
   <gl_ES>Gravador de vídeo na pantalla baseado en Qt</gl_ES>
   <gu>a Qt-based screencast recorder</gu>
   <he_IL>a Qt-based screencast recorder</he_IL>
   <hi>क्यूटी-आधारित स्क्रीन रिकॉर्डर</hi>
   <hr>a Qt-based screencast recorder</hr>
   <hu>a Qt-based screencast recorder</hu>
   <id>a Qt-based screencast recorder</id>
   <is>a Qt-based screencast recorder</is>
   <it>registratore dello schermo basato su librerie Qt</it>
   <ja>Qt ベースのスクリーンキャスト・レコーダー</ja>
   <kk>a Qt-based screencast recorder</kk>
   <ko>a Qt-based screencast recorder</ko>
   <ku>a Qt-based screencast recorder</ku>
   <lt>a Qt-based screencast recorder</lt>
   <mk>a Qt-based screencast recorder</mk>
   <mr>a Qt-based screencast recorder</mr>
   <nb_NO>a Qt-based screencast recorder</nb_NO>
   <nb>Qt-basert videoopptak av skjerm</nb>
   <nl_BE>a Qt-based screencast recorder</nl_BE>
   <nl>a Qt-gebaseerde screencast recorder</nl>
   <or>a Qt-based screencast recorder</or>
   <pl>rejestrator ekranu oparty na QT</pl>
   <pt_BR>Gravador de tela (captura de áudio e vídeo) baseado em Qt</pt_BR>
   <pt>Gravador de vídeo no ecrã baseado em Qt</pt>
   <ro>a Qt-based screencast recorder</ro>
   <ru>Запись скринкастов</ru>
   <sk>a Qt-based screencast recorder</sk>
   <sl>Qt snemalnik zaslona</sl>
   <so>a Qt-based screencast recorder</so>
   <sq>Regjistrues transmetimi ekrani, bazuar në Qt</sq>
   <sr>a Qt-based screencast recorder</sr>
   <sv>en Qt-baserad skärmbildsinspelare</sv>
   <th>a Qt-based screencast recorder</th>
   <tr>Qt temelli hareketli ekran görüntüsü kaydedici</tr>
   <uk>a Qt-based screencast recorder</uk>
   <vi>a Qt-based screencast recorder</vi>
   <zh_CN>a Qt-based screencast recorder</zh_CN>
   <zh_HK>a Qt-based screencast recorder</zh_HK>
   <zh_TW>a Qt-based screencast recorder</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
simplescreenrecorder
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
simplescreenrecorder
</uninstall_package_names>

</app>
