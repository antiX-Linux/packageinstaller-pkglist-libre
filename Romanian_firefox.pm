<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Romanian_Firfox
</name>

<description>
   <am>Romanian localisation of Firefox</am>
   <ar>Romanian localisation of Firefox</ar>
   <be>Romanian localisation of Firefox</be>
   <bg>Romanian localisation of Firefox</bg>
   <bn>Romanian localisation of Firefox</bn>
   <ca>Localització de Firefox en Romanès</ca>
   <cs>Romanian localisation of Firefox</cs>
   <da>Rumænsk oversættelse af Firefox</da>
   <de>Rumänische Lokalisierung von Firefox</de>
   <el>Ρουμανικά για το Firefox</el>
   <en>Romanian localisation of Firefox</en>
   <es_ES>Localización Rumana de Firefox</es_ES>
   <es>Localización Rumano de Firefox</es>
   <et>Romanian localisation of Firefox</et>
   <eu>Romanian localisation of Firefox</eu>
   <fa>Romanian localisation of Firefox</fa>
   <fil_PH>Romanian localisation of Firefox</fil_PH>
   <fi>Romanialainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en roumain pour Firefox</fr_BE>
   <fr>Localisation en roumain pour Firefox</fr>
   <gl_ES>Localización de Firefox ao romanés</gl_ES>
   <gu>Romanian localisation of Firefox</gu>
   <he_IL>Romanian localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का रोमानियाई संस्करण</hi>
   <hr>Romanian localisation of Firefox</hr>
   <hu>Romanian localisation of Firefox</hu>
   <id>Romanian localisation of Firefox</id>
   <is>Romanian localisation of Firefox</is>
   <it>Localizzazione rumena di Firefox</it>
   <ja>Firefox のルーマニア語版</ja>
   <kk>Romanian localisation of Firefox</kk>
   <ko>Romanian localisation of Firefox</ko>
   <ku>Romanian localisation of Firefox</ku>
   <lt>Romanian localisation of Firefox</lt>
   <mk>Romanian localisation of Firefox</mk>
   <mr>Romanian localisation of Firefox</mr>
   <nb_NO>Romanian localisation of Firefox</nb_NO>
   <nb>Romansk lokaltilpassing av Firefox</nb>
   <nl_BE>Romanian localisation of Firefox</nl_BE>
   <nl>Roemeense lokalisatie van Firefox</nl>
   <or>Romanian localisation of Firefox</or>
   <pl>Rumuńska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Romeno Localização para o Firefox</pt_BR>
   <pt>Romeno Localização para Firefox</pt>
   <ro>Romanian localisation of Firefox</ro>
   <ru>Румынская локализация Firefox</ru>
   <sk>Romanian localisation of Firefox</sk>
   <sl>Romunske krajevne nastavitve za Firefox</sl>
   <so>Romanian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në rumanisht</sq>
   <sr>Romanian localisation of Firefox</sr>
   <sv>Rumänsk lokalisering av Firefox</sv>
   <th>Romanian localisation ของ Firefox</th>
   <tr>Firefox'un Rumence yerelleştirmesi</tr>
   <uk>Romanian локалізація Firefox</uk>
   <vi>Romanian localisation of Firefox</vi>
   <zh_CN>Romanian localisation of Firefox</zh_CN>
   <zh_HK>Romanian localisation of Firefox</zh_HK>
   <zh_TW>Romanian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-ro
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-ro
</uninstall_package_names>
</app>
