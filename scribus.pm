<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Graphics
</category>

<name>
Scribus
</name>

<description>
   <am>a desktop page layout program</am>
   <ar>a desktop page layout program</ar>
   <be>a desktop page layout program</be>
   <bg>a desktop page layout program</bg>
   <bn>a desktop page layout program</bn>
   <ca>Programa de disseny de pàgines d'escriptori</ca>
   <cs>a desktop page layout program</cs>
   <da>et program til skrivebordssidelayout</da>
   <de>Ein Desktop-Seitenlayout-Programm</de>
   <el>Πρόγραμμα διάταξης σελίδας της επιφάνειας εργασίας</el>
   <en>a desktop page layout program</en>
   <es_ES>Programa de maquetación de páginas para escritorio</es_ES>
   <es>Programa de maquetación de páginas para escritorio</es>
   <et>a desktop page layout program</et>
   <eu>a desktop page layout program</eu>
   <fa>a desktop page layout program</fa>
   <fil_PH>a desktop page layout program</fil_PH>
   <fi>työpöytäsivuohjelma ulkoasulle</fi>
   <fr_BE>Programme de mise en page</fr_BE>
   <fr>Programme de mise en page</fr>
   <gl_ES>programa de paxinación electrónica (composición das publicación)</gl_ES>
   <gu>a desktop page layout program</gu>
   <he_IL>a desktop page layout program</he_IL>
   <hi>डेस्कटॉप पृष्ठ अभिन्यास प्रोग्राम</hi>
   <hr>a desktop page layout program</hr>
   <hu>a desktop page layout program</hu>
   <id>a desktop page layout program</id>
   <is>a desktop page layout program</is>
   <it>programma di impaginazione per il desktop</it>
   <ja>デスクトップ ページレイアウトプログラム</ja>
   <kk>a desktop page layout program</kk>
   <ko>a desktop page layout program</ko>
   <ku>a desktop page layout program</ku>
   <lt>a desktop page layout program</lt>
   <mk>a desktop page layout program</mk>
   <mr>a desktop page layout program</mr>
   <nb_NO>a desktop page layout program</nb_NO>
   <nb>datatrykkeprogram (DTP)</nb>
   <nl_BE>a desktop page layout program</nl_BE>
   <nl>a desktop pagina layout programma</nl>
   <or>a desktop page layout program</or>
   <pl>program do składu tekstu (DTP), projektowania publikacji i tworzenia plików PDF</pl>
   <pt_BR>Programa de paginação eletrônica (composição de publicações)</pt_BR>
   <pt>Programa de paginação electrónica (composição de publicações)</pt>
   <ro>a desktop page layout program</ro>
   <ru>Приложение для визуальной вёрстки документов</ru>
   <sk>a desktop page layout program</sk>
   <sl>Namizno urejanje preloma strani</sl>
   <so>a desktop page layout program</so>
   <sq>program desktop skicimi faqesh</sq>
   <sr>a desktop page layout program</sr>
   <sv>ett skrivbords sidlayout-program</sv>
   <th>a desktop page layout program</th>
   <tr>bir masaüstü sayfa düzeni programı</tr>
   <uk>a desktop page layout program</uk>
   <vi>a desktop page layout program</vi>
   <zh_CN>a desktop page layout program</zh_CN>
   <zh_HK>a desktop page layout program</zh_HK>
   <zh_TW>a desktop page layout program</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
scribus
scribus-template
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
scribus
scribus-template
</uninstall_package_names>
</app>
