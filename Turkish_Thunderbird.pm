<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Turkish_Thunderbird
</name>

<description>
   <am>Turkish localisation of Thunderbird</am>
   <ar>Turkish localisation of Thunderbird</ar>
   <be>Turkish localisation of Thunderbird</be>
   <bg>Turkish localisation of Thunderbird</bg>
   <bn>Turkish localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Turc</ca>
   <cs>Turkish localisation of Thunderbird</cs>
   <da>Tyrkisk oversættelse af Thunderbird</da>
   <de>Türkische Lokalisierung von Thunderbird</de>
   <el>Τουρκικά για το Thunderbird</el>
   <en>Turkish localisation of Thunderbird</en>
   <es_ES>Localización Turca de Thunderbird</es_ES>
   <es>Localización Turco de Thunderbird</es>
   <et>Turkish localisation of Thunderbird</et>
   <eu>Turkish localisation of Thunderbird</eu>
   <fa>Turkish localisation of Thunderbird</fa>
   <fil_PH>Turkish localisation of Thunderbird</fil_PH>
   <fi>Turkkilainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation turque pour Thunderbird</fr_BE>
   <fr>Localisation turque pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao turco</gl_ES>
   <gu>Turkish localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לטורקית</he_IL>
   <hi>थंडरबर्ड का तुर्क संस्करण</hi>
   <hr>Turkish localisation of Thunderbird</hr>
   <hu>Turkish localisation of Thunderbird</hu>
   <id>Turkish localisation of Thunderbird</id>
   <is>Turkish localisation of Thunderbird</is>
   <it>Localizzazione turca di Thunderbird</it>
   <ja>Thunderbird のトルコ語版</ja>
   <kk>Turkish localisation of Thunderbird</kk>
   <ko>Turkish localisation of Thunderbird</ko>
   <ku>Turkish localisation of Thunderbird</ku>
   <lt>Turkish localisation of Thunderbird</lt>
   <mk>Turkish localisation of Thunderbird</mk>
   <mr>Turkish localisation of Thunderbird</mr>
   <nb_NO>Turkish localisation of Thunderbird</nb_NO>
   <nb>Tyrkisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Turkish localisation of Thunderbird</nl_BE>
   <nl>Turkse lokalisatie van Thunderbird</nl>
   <or>Turkish localisation of Thunderbird</or>
   <pl>Turecka lokalizacja Thunderbirda</pl>
   <pt_BR>Turco Localização para o Thunderbird</pt_BR>
   <pt>Turco Localização para Thunderbird</pt>
   <ro>Turkish localisation of Thunderbird</ro>
   <ru>Turkish localisation of Thunderbird</ru>
   <sk>Turkish localisation of Thunderbird</sk>
   <sl>Turške krajevne nastavitve za Thunderbird</sl>
   <so>Turkish localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në turqisht</sq>
   <sr>Turkish localisation of Thunderbird</sr>
   <sv>Turkisk lokalisering av Thunderbird</sv>
   <th>Turkish localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Türkçe yerelleştirmesi</tr>
   <uk>Turkish localisation of Thunderbird</uk>
   <vi>Turkish localisation of Thunderbird</vi>
   <zh_CN>Turkish localisation of Thunderbird</zh_CN>
   <zh_HK>Turkish localisation of Thunderbird</zh_HK>
   <zh_TW>Turkish localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-tr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-tr
</uninstall_package_names>

</app>
