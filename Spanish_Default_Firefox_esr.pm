<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Spanish_Default_Firefox_esr
</name>

<description>
   <am>Spanish localisation of Firefox ESR</am>
   <ar>Spanish localisation of Firefox ESR</ar>
   <be>Spanish localisation of Firefox ESR</be>
   <bg>Spanish localisation of Firefox ESR</bg>
   <bn>Spanish localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Castellà</ca>
   <cs>Spanish localisation of Firefox ESR</cs>
   <da>Spanish localisation of Firefox ESR</da>
   <de>Spanische Lokalisation von “Firefox ESR”</de>
   <el>Ισπανικά για Firefox ESR</el>
   <en>Spanish localisation of Firefox ESR</en>
   <es_ES>Localización Español de Firefox ESR</es_ES>
   <es>Localización Español para Firefox ESR</es>
   <et>Spanish localisation of Firefox ESR</et>
   <eu>Spanish localisation of Firefox ESR</eu>
   <fa>Spanish localisation of Firefox ESR</fa>
   <fil_PH>Spanish localisation of Firefox ESR</fil_PH>
   <fi>Spanish localisation of Firefox ESR</fi>
   <fr_BE>Localisation en espagnol pour Firefox ESR</fr_BE>
   <fr>Localisation en espagnol pour Firefox ESR</fr>
   <gl_ES>Spanish localisation of Firefox ESR</gl_ES>
   <gu>Spanish localisation of Firefox ESR</gu>
   <he_IL>Spanish localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का स्पेनिश संस्करण</hi>
   <hr>Spanish localisation of Firefox ESR</hr>
   <hu>Spanish localisation of Firefox ESR</hu>
   <id>Spanish localisation of Firefox ESR</id>
   <is>Spanish localisation of Firefox ESR</is>
   <it>Localizzazione spagnola di Firefox ESR</it>
   <ja>スペイン語版 Firefox ESR</ja>
   <kk>Spanish localisation of Firefox ESR</kk>
   <ko>Spanish localisation of Firefox ESR</ko>
   <ku>Spanish localisation of Firefox ESR</ku>
   <lt>Spanish localisation of Firefox ESR</lt>
   <mk>Spanish localisation of Firefox ESR</mk>
   <mr>Spanish localisation of Firefox ESR</mr>
   <nb_NO>Spanish localisation of Firefox ESR</nb_NO>
   <nb>Spansk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Spanish localisation of Firefox ESR</nl_BE>
   <nl>Spanish localisation of Firefox ESR</nl>
   <or>Spanish localisation of Firefox ESR</or>
   <pl>Spanish localisation of Firefox ESR</pl>
   <pt_BR>Espanhol Localização para o Firefox ESR</pt_BR>
   <pt>Castelhano Localização para Firefox ESR</pt>
   <ro>Spanish localisation of Firefox ESR</ro>
   <ru>Spanish localisation of Firefox ESR</ru>
   <sk>Spanish localisation of Firefox ESR</sk>
   <sl>Španske krajevne nastavitve za Firefox ESR</sl>
   <so>Spanish localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në spanjisht</sq>
   <sr>Spanish localisation of Firefox ESR</sr>
   <sv>Spansk lokalisering av Firefox ESR</sv>
   <th>Spanish localisation of Firefox ESR</th>
   <tr>Firefox ESR İspanyolca yerelleştirmesi</tr>
   <uk>Spanish localisation of Firefox ESR</uk>
   <vi>Spanish localisation of Firefox ESR</vi>
   <zh_CN>Spanish localisation of Firefox ESR</zh_CN>
   <zh_HK>Spanish localisation of Firefox ESR</zh_HK>
   <zh_TW>Spanish localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-es-es
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-es-es
</uninstall_package_names>

</app>
