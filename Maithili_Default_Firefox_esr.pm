<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Maithili_Default_Firefox_esr
</name>

<description>
   <am>Maithili localisation of Firefox ESR</am>
   <ar>Maithili localisation of Firefox ESR</ar>
   <be>Maithili localisation of Firefox ESR</be>
   <bg>Maithili localisation of Firefox ESR</bg>
   <bn>Maithili localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Maithili</ca>
   <cs>Maithili localisation of Firefox ESR</cs>
   <da>Maithili localisation of Firefox ESR</da>
   <de>Indische bzw. Nepalesische Lokalisation für die Sprache “Maithili” von “Firefox ESR”</de>
   <el>Maithili για Firefox ESR</el>
   <en>Maithili localisation of Firefox ESR</en>
   <es_ES>Localización Maithili de Firefox ESR</es_ES>
   <es>Localización Maithili de Firefox ESR</es>
   <et>Maithili localisation of Firefox ESR</et>
   <eu>Maithili localisation of Firefox ESR</eu>
   <fa>Maithili localisation of Firefox ESR</fa>
   <fil_PH>Maithili localisation of Firefox ESR</fil_PH>
   <fi>Maithili localisation of Firefox ESR</fi>
   <fr_BE>Localisation en maithili pour Firefox ESR</fr_BE>
   <fr>Localisation en maithili pour Firefox ESR</fr>
   <gl_ES>Maithili localisation of Firefox ESR</gl_ES>
   <gu>Maithili localisation of Firefox ESR</gu>
   <he_IL>Maithili localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का मैथिली संस्करण</hi>
   <hr>Maithili localisation of Firefox ESR</hr>
   <hu>Maithili localisation of Firefox ESR</hu>
   <id>Maithili localisation of Firefox ESR</id>
   <is>Maithili localisation of Firefox ESR</is>
   <it>Localizzazione maithili di Firefox ESR</it>
   <ja>マイティリー語版 Firefox ESR</ja>
   <kk>Maithili localisation of Firefox ESR</kk>
   <ko>Maithili localisation of Firefox ESR</ko>
   <ku>Maithili localisation of Firefox ESR</ku>
   <lt>Maithili localisation of Firefox ESR</lt>
   <mk>Maithili localisation of Firefox ESR</mk>
   <mr>Maithili localisation of Firefox ESR</mr>
   <nb_NO>Maithili localisation of Firefox ESR</nb_NO>
   <nb>Maithili lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Maithili localisation of Firefox ESR</nl_BE>
   <nl>Maithili localisation of Firefox ESR</nl>
   <or>Maithili localisation of Firefox ESR</or>
   <pl>Maithili localisation of Firefox ESR</pl>
   <pt_BR>Maitili Localização para o Firefox ESR</pt_BR>
   <pt>Maitili Localização para Firefox ESR</pt>
   <ro>Maithili localisation of Firefox ESR</ro>
   <ru>Maithili localisation of Firefox ESR</ru>
   <sk>Maithili localisation of Firefox ESR</sk>
   <sl>Maithilijske  krajevne nastavitve za Firefox ESR</sl>
   <so>Maithili localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në maitilif</sq>
   <sr>Maithili localisation of Firefox ESR</sr>
   <sv>Maithili lokalisering av Firefox ESR</sv>
   <th>Maithili localisation of Firefox ESR</th>
   <tr>Firefox ESR Maithilice yerelleştirmesi</tr>
   <uk>Maithili localisation of Firefox ESR</uk>
   <vi>Maithili localisation of Firefox ESR</vi>
   <zh_CN>Maithili localisation of Firefox ESR</zh_CN>
   <zh_HK>Maithili localisation of Firefox ESR</zh_HK>
   <zh_TW>Maithili localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-mai
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-mai
</uninstall_package_names>

</app>
