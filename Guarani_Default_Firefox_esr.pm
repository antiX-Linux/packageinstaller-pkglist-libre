<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Guarani_Default_Firefox_esr
</name>

<description>
   <am>Guarani localisation of Firefox ESR</am>
   <ar>Guarani localisation of Firefox ESR</ar>
   <be>Guarani localisation of Firefox ESR</be>
   <bg>Guarani localisation of Firefox ESR</bg>
   <bn>Guarani localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Guaraní</ca>
   <cs>Guarani localisation of Firefox ESR</cs>
   <da>Guarani localisation of Firefox ESR</da>
   <de>Lokalisation für die Südamerikanische Sprache “Guaraní” (für Paraguay, Argentinien, Bolivien und Brasilien) von “Firefox ESR”</de>
   <el>Guarani για Firefox ESR</el>
   <en>Guarani localisation of Firefox ESR</en>
   <es_ES>Localización Guaraní de Firefox ESR</es_ES>
   <es>Localización Guaraní de Firefox ESR</es>
   <et>Guarani localisation of Firefox ESR</et>
   <eu>Guarani localisation of Firefox ESR</eu>
   <fa>Guarani localisation of Firefox ESR</fa>
   <fil_PH>Guarani localisation of Firefox ESR</fil_PH>
   <fi>Guarani localisation of Firefox ESR</fi>
   <fr_BE>Localisation en guarani pour Firefox ESR</fr_BE>
   <fr>Localisation en guarani pour Firefox ESR</fr>
   <gl_ES>Guarani localisation of Firefox ESR</gl_ES>
   <gu>Guarani localisation of Firefox ESR</gu>
   <he_IL>Guarani localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का गूरानी संस्करण</hi>
   <hr>Guarani localisation of Firefox ESR</hr>
   <hu>Guarani localisation of Firefox ESR</hu>
   <id>Guarani localisation of Firefox ESR</id>
   <is>Guarani localisation of Firefox ESR</is>
   <it>Localizzazione in lingua guarani di Firefox ESR</it>
   <ja>グァラニ語版 Firefox ESR</ja>
   <kk>Guarani localisation of Firefox ESR</kk>
   <ko>Guarani localisation of Firefox ESR</ko>
   <ku>Guarani localisation of Firefox ESR</ku>
   <lt>Guarani localisation of Firefox ESR</lt>
   <mk>Guarani localisation of Firefox ESR</mk>
   <mr>Guarani localisation of Firefox ESR</mr>
   <nb_NO>Guarani localisation of Firefox ESR</nb_NO>
   <nb>Guarani lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Guarani localisation of Firefox ESR</nl_BE>
   <nl>Guarani localisation of Firefox ESR</nl>
   <or>Guarani localisation of Firefox ESR</or>
   <pl>Guarani localisation of Firefox ESR</pl>
   <pt_BR>Guarani Localização para o Firefox ESR</pt_BR>
   <pt>Guarani Localização para Firefox ESR</pt>
   <ro>Guarani localisation of Firefox ESR</ro>
   <ru>Guarani localisation of Firefox ESR</ru>
   <sk>Guarani localisation of Firefox ESR</sk>
   <sl>Gvaranijske krajevne nastavitve za Firefox ESR</sl>
   <so>Guarani localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në guaranisht</sq>
   <sr>Guarani localisation of Firefox ESR</sr>
   <sv>Guarani lokalisering av Firefox ESR</sv>
   <th>Guarani localisation of Firefox ESR</th>
   <tr>Firefox ESR Guarani Dili yerelleştirmesi</tr>
   <uk>Guarani localisation of Firefox ESR</uk>
   <vi>Guarani localisation of Firefox ESR</vi>
   <zh_CN>Guarani localisation of Firefox ESR</zh_CN>
   <zh_HK>Guarani localisation of Firefox ESR</zh_HK>
   <zh_TW>Guarani localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-gn
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-gn
</uninstall_package_names>

</app>
