<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Romanian_Default_Firefox_esr
</name>

<description>
   <am>Romanian localisation of Firefox ESR</am>
   <ar>Romanian localisation of Firefox ESR</ar>
   <be>Romanian localisation of Firefox ESR</be>
   <bg>Romanian localisation of Firefox ESR</bg>
   <bn>Romanian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Romanès</ca>
   <cs>Romanian localisation of Firefox ESR</cs>
   <da>Romanian localisation of Firefox ESR</da>
   <de>Rumänische Lokalisation von “Firefox ESR”</de>
   <el>Ρουμανικά για Firefox ESR</el>
   <en>Romanian localisation of Firefox ESR</en>
   <es_ES>Localización Rumana de Firefox ESR</es_ES>
   <es>Localización Rumano de Firefox ESR</es>
   <et>Romanian localisation of Firefox ESR</et>
   <eu>Romanian localisation of Firefox ESR</eu>
   <fa>Romanian localisation of Firefox ESR</fa>
   <fil_PH>Romanian localisation of Firefox ESR</fil_PH>
   <fi>Romanian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en roumain pour Firefox ESR</fr_BE>
   <fr>Localisation en roumain pour Firefox ESR</fr>
   <gl_ES>Romanian localisation of Firefox ESR</gl_ES>
   <gu>Romanian localisation of Firefox ESR</gu>
   <he_IL>Romanian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का रोमानियाई संस्करण</hi>
   <hr>Romanian localisation of Firefox ESR</hr>
   <hu>Romanian localisation of Firefox ESR</hu>
   <id>Romanian localisation of Firefox ESR</id>
   <is>Romanian localisation of Firefox ESR</is>
   <it>Localizzazione rumena di Firefox ESR</it>
   <ja>ルーマニア語版 Firefox ESR</ja>
   <kk>Romanian localisation of Firefox ESR</kk>
   <ko>Romanian localisation of Firefox ESR</ko>
   <ku>Romanian localisation of Firefox ESR</ku>
   <lt>Romanian localisation of Firefox ESR</lt>
   <mk>Romanian localisation of Firefox ESR</mk>
   <mr>Romanian localisation of Firefox ESR</mr>
   <nb_NO>Romanian localisation of Firefox ESR</nb_NO>
   <nb>Rumensk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Romanian localisation of Firefox ESR</nl_BE>
   <nl>Romanian localisation of Firefox ESR</nl>
   <or>Romanian localisation of Firefox ESR</or>
   <pl>Romanian localisation of Firefox ESR</pl>
   <pt_BR>Romeno Localização para o Firefox ESR</pt_BR>
   <pt>Romeno Localização para Firefox ESR</pt>
   <ro>Romanian localisation of Firefox ESR</ro>
   <ru>Romanian localisation of Firefox ESR</ru>
   <sk>Romanian localisation of Firefox ESR</sk>
   <sl>Romunske krajevne nastavitve za Firefox ESR</sl>
   <so>Romanian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në rumanisht</sq>
   <sr>Romanian localisation of Firefox ESR</sr>
   <sv>Rumänsk lokalisering av Firefox ESR</sv>
   <th>Romanian localisation of Firefox ESR</th>
   <tr>Firefox ESR Rumence yerelleştirmesi</tr>
   <uk>Romanian localisation of Firefox ESR</uk>
   <vi>Romanian localisation of Firefox ESR</vi>
   <zh_CN>Romanian localisation of Firefox ESR</zh_CN>
   <zh_HK>Romanian localisation of Firefox ESR</zh_HK>
   <zh_TW>Romanian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ro
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ro
</uninstall_package_names>

</app>
