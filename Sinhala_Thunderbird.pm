<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Sinhala_Thunderbird
</name>

<description>
   <am>Sinhala localisation of Thunderbird</am>
   <ar>Sinhala localisation of Thunderbird</ar>
   <be>Sinhala localisation of Thunderbird</be>
   <bg>Sinhala localisation of Thunderbird</bg>
   <bn>Sinhala localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Sinhala</ca>
   <cs>Sinhala localisation of Thunderbird</cs>
   <da>Sinhala localisation of Thunderbird</da>
   <de>Singhalesische Lokalisation von “Thunderbird”</de>
   <el>Sinhala για Thunderbird</el>
   <en>Sinhala localisation of Thunderbird</en>
   <es_ES>Localización Cingalés de Thunderbird</es_ES>
   <es>Localización Cingalés de Thunderbird</es>
   <et>Sinhala localisation of Thunderbird</et>
   <eu>Sinhala localisation of Thunderbird</eu>
   <fa>Sinhala localisation of Thunderbird</fa>
   <fil_PH>Sinhala localisation of Thunderbird</fil_PH>
   <fi>Sinhala localisation of Thunderbird</fi>
   <fr_BE>Localisation en cinghalais pour Thunderbird</fr_BE>
   <fr>Localisation en cinghalais pour Thunderbird</fr>
   <gl_ES>Sinhala localisation of Thunderbird</gl_ES>
   <gu>Sinhala localisation of Thunderbird</gu>
   <he_IL>Sinhala localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का सिंहला संस्करण</hi>
   <hr>Sinhala localisation of Thunderbird</hr>
   <hu>Sinhala localisation of Thunderbird</hu>
   <id>Sinhala localisation of Thunderbird</id>
   <is>Sinhala localisation of Thunderbird</is>
   <it>Localizzazione sinhala di Thunderbird</it>
   <ja>シンハラ語版 Thunderbird</ja>
   <kk>Sinhala localisation of Thunderbird</kk>
   <ko>Sinhala localisation of Thunderbird</ko>
   <ku>Sinhala localisation of Thunderbird</ku>
   <lt>Sinhala localisation of Thunderbird</lt>
   <mk>Sinhala localisation of Thunderbird</mk>
   <mr>Sinhala localisation of Thunderbird</mr>
   <nb_NO>Sinhala localisation of Thunderbird</nb_NO>
   <nb>Sinhala lokaltilpassing av Thunderbird</nb>
   <nl_BE>Sinhala localisation of Thunderbird</nl_BE>
   <nl>Sinhala localisation of Thunderbird</nl>
   <or>Sinhala localisation of Thunderbird</or>
   <pl>Sinhala localisation of Thunderbird</pl>
   <pt_BR>Cingalês Localização para o Thunderbird</pt_BR>
   <pt>Cingalês Localização para Thunderbird</pt>
   <ro>Sinhala localisation of Thunderbird</ro>
   <ru>Sinhala localisation of Thunderbird</ru>
   <sk>Sinhala localisation of Thunderbird</sk>
   <sl>Sinhalske krajevne nastavitve za Thunderbird</sl>
   <so>Sinhala localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në sinhala</sq>
   <sr>Sinhala localisation of Thunderbird</sr>
   <sv>Sinhala lokalisering av Thunderbird</sv>
   <th>Sinhala localisation of Thunderbird</th>
   <tr>Thunderbird'ün Seylanca yerelleştirmesi</tr>
   <uk>Sinhala localisation of Thunderbird</uk>
   <vi>Sinhala localisation of Thunderbird</vi>
   <zh_CN>Sinhala localisation of Thunderbird</zh_CN>
   <zh_HK>Sinhala localisation of Thunderbird</zh_HK>
   <zh_TW>Sinhala localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-si
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-si
</uninstall_package_names>

</app>
