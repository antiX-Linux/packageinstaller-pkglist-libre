<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
German
</name>

<description>
   <am>German dictionary for hunspell</am>
   <ar>German dictionary for hunspell</ar>
   <be>German dictionary for hunspell</be>
   <bg>German dictionary for hunspell</bg>
   <bn>German dictionary for hunspell</bn>
   <ca>Diccionari alemany per hunspell</ca>
   <cs>German dictionary for hunspell</cs>
   <da>German dictionary for hunspell</da>
   <de>German dictionary for hunspell</de>
   <el>Γερμανικό λεξικό για hunspell</el>
   <en>German dictionary for hunspell</en>
   <es_ES>Diccionario de alemán para hunspell</es_ES>
   <es>Diccionario de alemán para hunspell</es>
   <et>German dictionary for hunspell</et>
   <eu>German dictionary for hunspell</eu>
   <fa>German dictionary for hunspell</fa>
   <fil_PH>German dictionary for hunspell</fil_PH>
   <fi>German dictionary for hunspell</fi>
   <fr_BE>Allemand dictionnaire pour hunspell</fr_BE>
   <fr>Allemand dictionnaire pour hunspell</fr>
   <gl_ES>German dictionary for hunspell</gl_ES>
   <gu>German dictionary for hunspell</gu>
   <he_IL>German dictionary for hunspell</he_IL>
   <hi>German dictionary for hunspell</hi>
   <hr>German dictionary for hunspell</hr>
   <hu>German dictionary for hunspell</hu>
   <id>German dictionary for hunspell</id>
   <is>German dictionary for hunspell</is>
   <it>Dizionario German per hunspell</it>
   <ja>German dictionary for hunspell</ja>
   <kk>German dictionary for hunspell</kk>
   <ko>German dictionary for hunspell</ko>
   <ku>German dictionary for hunspell</ku>
   <lt>German dictionary for hunspell</lt>
   <mk>German dictionary for hunspell</mk>
   <mr>German dictionary for hunspell</mr>
   <nb_NO>German dictionary for hunspell</nb_NO>
   <nb>Tysk ordliste for hunspell</nb>
   <nl_BE>German dictionary for hunspell</nl_BE>
   <nl>Duits woordenboek voor hunspell</nl>
   <or>German dictionary for hunspell</or>
   <pl>German dictionary for hunspell</pl>
   <pt_BR>Dicionário Alemão para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Alemão para hunspell</pt>
   <ro>German dictionary for hunspell</ro>
   <ru>German dictionary for hunspell</ru>
   <sk>German dictionary for hunspell</sk>
   <sl>Nemški slovar za hunspell</sl>
   <so>German dictionary for hunspell</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në gjermanisht</sq>
   <sr>German dictionary for hunspell</sr>
   <sv>Tysk ordbok för hunspell</sv>
   <th>German dictionary for hunspell</th>
   <tr>German dictionary for hunspell</tr>
   <uk>German dictionary for hunspell</uk>
   <vi>German dictionary for hunspell</vi>
   <zh_CN>German dictionary for hunspell</zh_CN>
   <zh_HK>German dictionary for hunspell</zh_HK>
   <zh_TW>German dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-de
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-de
</uninstall_package_names>

</app>
