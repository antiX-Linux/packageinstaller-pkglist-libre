<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Nepalese
</name>

<description>
   <am>Nepalese dictionary for hunspell</am>
   <ar>Nepalese dictionary for hunspell</ar>
   <be>Nepalese dictionary for hunspell</be>
   <bg>Nepalese dictionary for hunspell</bg>
   <bn>Nepalese dictionary for hunspell</bn>
   <ca>Diccionari Nepalès per hunspell</ca>
   <cs>Nepalese dictionary for hunspell</cs>
   <da>Nepalese dictionary for hunspell</da>
   <de>Nepali Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Νεπάλ για hunspell</el>
   <en>Nepalese dictionary for hunspell</en>
   <es_ES>Diccionario Nepalí para hunspell</es_ES>
   <es>Diccionario Nepalí para hunspell</es>
   <et>Nepalese dictionary for hunspell</et>
   <eu>Nepalese dictionary for hunspell</eu>
   <fa>Nepalese dictionary for hunspell</fa>
   <fil_PH>Nepalese dictionary for hunspell</fil_PH>
   <fi>Nepalese dictionary for hunspell</fi>
   <fr_BE>Népalais dictionnaire pour hunspell</fr_BE>
   <fr>Népalais dictionnaire pour hunspell</fr>
   <gl_ES>Nepalese dictionary for hunspell</gl_ES>
   <gu>Nepalese dictionary for hunspell</gu>
   <he_IL>Nepalese dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु नेपाली शब्दकोष</hi>
   <hr>Nepalese dictionary for hunspell</hr>
   <hu>Nepalese dictionary for hunspell</hu>
   <id>Nepalese dictionary for hunspell</id>
   <is>Nepalese dictionary for hunspell</is>
   <it>Dizionario nepalese per hunspell</it>
   <ja>Hunspell 用ネパール語辞書</ja>
   <kk>Nepalese dictionary for hunspell</kk>
   <ko>Nepalese dictionary for hunspell</ko>
   <ku>Nepalese dictionary for hunspell</ku>
   <lt>Nepalese dictionary for hunspell</lt>
   <mk>Nepalese dictionary for hunspell</mk>
   <mr>Nepalese dictionary for hunspell</mr>
   <nb_NO>Nepalese dictionary for hunspell</nb_NO>
   <nb>Nepalesisk ordliste for hunspell</nb>
   <nl_BE>Nepalese dictionary for hunspell</nl_BE>
   <nl>Nepalese dictionary for hunspell</nl>
   <or>Nepalese dictionary for hunspell</or>
   <pl>Nepalese dictionary for hunspell</pl>
   <pt_BR>Dicionário Nepali para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Nepalês para hunspell</pt>
   <ro>Nepalese dictionary for hunspell</ro>
   <ru>Nepalese dictionary for hunspell</ru>
   <sk>Nepalese dictionary for hunspell</sk>
   <sl>Nepaljski slovar za hunspell</sl>
   <so>Nepalese dictionary for hunspell</so>
   <sq>Fjalor nepalisht për hunspell</sq>
   <sr>Nepalese dictionary for hunspell</sr>
   <sv>Nepalesisk ordbok för hunspell</sv>
   <th>Nepalese dictionary for hunspell</th>
   <tr>Hunspell için Nepalce sözlük</tr>
   <uk>Nepalese dictionary for hunspell</uk>
   <vi>Nepalese dictionary for hunspell</vi>
   <zh_CN>Nepalese dictionary for hunspell</zh_CN>
   <zh_HK>Nepalese dictionary for hunspell</zh_HK>
   <zh_TW>Nepalese dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-ne
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ne
</uninstall_package_names>

</app>
