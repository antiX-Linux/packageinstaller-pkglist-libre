<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
LaTeX
</category>

<name>
LaTeX-Kile
</name>

<description>
   <am>LaTeX editor</am>
   <ar>LaTeX editor</ar>
   <be>LaTeX editor</be>
   <bg>LaTeX editor</bg>
   <bn>LaTeX editor</bn>
   <ca>Editor LaTeX</ca>
   <cs>LaTeX editor</cs>
   <da>LaTeX editor</da>
   <de>Bearbeitungsprogramm des Textsatz-Softwarepaketes “LaTeX”</de>
   <el>Πρόγραμμα επεξεργασίας LaTeX</el>
   <en>LaTeX editor</en>
   <es_ES>Editor LaTeX</es_ES>
   <es>Editor LaTeX</es>
   <et>LaTeX editor</et>
   <eu>LaTeX editor</eu>
   <fa>LaTeX editor</fa>
   <fil_PH>LaTeX editor</fil_PH>
   <fi>LaTeX-editori</fi>
   <fr_BE>Éditeur LaTeX</fr_BE>
   <fr>Éditeur LaTeX</fr>
   <gl_ES>LaTeX editor</gl_ES>
   <gu>LaTeX editor</gu>
   <he_IL>LaTeX editor</he_IL>
   <hi>LaTeX संपादक</hi>
   <hr>LaTeX editor</hr>
   <hu>LaTeX editor</hu>
   <id>LaTeX editor</id>
   <is>LaTeX editor</is>
   <it>LaTeX editor</it>
   <ja>LaTeX エディタ</ja>
   <kk>LaTeX editor</kk>
   <ko>LaTeX editor</ko>
   <ku>LaTeX editor</ku>
   <lt>LaTeX editor</lt>
   <mk>LaTeX editor</mk>
   <mr>LaTeX editor</mr>
   <nb_NO>LaTeX editor</nb_NO>
   <nb>Redigeringsprogram for LaTeX</nb>
   <nl_BE>LaTeX editor</nl_BE>
   <nl>LaTeX editor</nl>
   <or>LaTeX editor</or>
   <pl>LaTeX editor</pl>
   <pt_BR>LaTeX editor - Ambiente para desenvolvimento rápido de documentos TeX e LaTeX</pt_BR>
   <pt>Editor LaTeX</pt>
   <ro>LaTeX editor</ro>
   <ru>LaTeX editor</ru>
   <sk>LaTeX editor</sk>
   <sl>LaTeX urejevalnik</sl>
   <so>LaTeX editor</so>
   <sq>Përpunues LaTeX-i</sq>
   <sr>LaTeX editor</sr>
   <sv>LaTeX redigerare</sv>
   <th>LaTeX editor</th>
   <tr>LaTeX düzenleyici</tr>
   <uk>LaTeX editor</uk>
   <vi>LaTeX editor</vi>
   <zh_CN>LaTeX editor</zh_CN>
   <zh_HK>LaTeX editor</zh_HK>
   <zh_TW>LaTeX editor</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
kile
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
kile
</uninstall_package_names>

</app>
