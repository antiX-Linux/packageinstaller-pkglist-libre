<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Dutch
</name>

<description>
   <am>Dutch dictionary for hunspell</am>
   <ar>Dutch dictionary for hunspell</ar>
   <be>Dutch dictionary for hunspell</be>
   <bg>Dutch dictionary for hunspell</bg>
   <bn>Dutch dictionary for hunspell</bn>
   <ca>Diccionari holandès per a hunspell</ca>
   <cs>Dutch dictionary for hunspell</cs>
   <da>Dutch dictionary for hunspell</da>
   <de>Niederländisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό στα Ολλανδικά για hunspell</el>
   <en>Dutch dictionary for hunspell</en>
   <es_ES>Diccionario Holandés para hunspell</es_ES>
   <es>Diccionario Holandés para hunspell</es>
   <et>Dutch dictionary for hunspell</et>
   <eu>Dutch dictionary for hunspell</eu>
   <fa>Dutch dictionary for hunspell</fa>
   <fil_PH>Dutch dictionary for hunspell</fil_PH>
   <fi>Dutch dictionary for hunspell</fi>
   <fr_BE>Néerlandais dictionnaire pour hunspell</fr_BE>
   <fr>Néerlandais dictionnaire pour hunspell</fr>
   <gl_ES>Dutch dictionary for hunspell</gl_ES>
   <gu>Dutch dictionary for hunspell</gu>
   <he_IL>Dutch dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु डच शब्दकोष</hi>
   <hr>Dutch dictionary for hunspell</hr>
   <hu>Dutch dictionary for hunspell</hu>
   <id>Dutch dictionary for hunspell</id>
   <is>Dutch dictionary for hunspell</is>
   <it>Dizionario olandese per hunspell</it>
   <ja>Hunspell 用オランダ語辞書</ja>
   <kk>Dutch dictionary for hunspell</kk>
   <ko>Dutch dictionary for hunspell</ko>
   <ku>Dutch dictionary for hunspell</ku>
   <lt>Dutch dictionary for hunspell</lt>
   <mk>Dutch dictionary for hunspell</mk>
   <mr>Dutch dictionary for hunspell</mr>
   <nb_NO>Dutch dictionary for hunspell</nb_NO>
   <nb>Nederlandsk ordliste for hunspell</nb>
   <nl_BE>Dutch dictionary for hunspell</nl_BE>
   <nl>Nederlands woordenboek voor hunspell</nl>
   <or>Dutch dictionary for hunspell</or>
   <pl>Dutch dictionary for hunspell</pl>
   <pt_BR>Dicionário Holandês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Holandês para hunspell</pt>
   <ro>Dutch dictionary for hunspell</ro>
   <ru>Dutch dictionary for hunspell</ru>
   <sk>Dutch dictionary for hunspell</sk>
   <sl>Nizozemski slovar za hunspell</sl>
   <so>Dutch dictionary for hunspell</so>
   <sq>Fjalor holandisht për hunspell</sq>
   <sr>Dutch dictionary for hunspell</sr>
   <sv>Holländsk ordbok för hunspell</sv>
   <th>Dutch dictionary for hunspell</th>
   <tr>Hunspell için Flemenkçe sözlük</tr>
   <uk>Dutch dictionary for hunspell</uk>
   <vi>Dutch dictionary for hunspell</vi>
   <zh_CN>Dutch dictionary for hunspell</zh_CN>
   <zh_HK>Dutch dictionary for hunspell</zh_HK>
   <zh_TW>Dutch dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-nl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-nl
</uninstall_package_names>

</app>
