<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
LaTeX
</category>

<name>
LaTeX-Full
</name>

<description>
   <am>Full LaTeX installation</am>
   <ar>Full LaTeX installation</ar>
   <be>Full LaTeX installation</be>
   <bg>Full LaTeX installation</bg>
   <bn>Full LaTeX installation</bn>
   <ca>Instal·lació completa de LaTeX</ca>
   <cs>Full LaTeX installation</cs>
   <da>Full LaTeX installation</da>
   <de>Vollständige Installation des Textsatz-Softwarepaketes “LaTaX”</de>
   <el>Πλήρης εγκατάσταση LaTeX</el>
   <en>Full LaTeX installation</en>
   <es_ES>Instalación completa de LaTeX</es_ES>
   <es>Instalación completa de LaTeX</es>
   <et>Full LaTeX installation</et>
   <eu>Full LaTeX installation</eu>
   <fa>Full LaTeX installation</fa>
   <fil_PH>Full LaTeX installation</fil_PH>
   <fi>Kokonainen LaTeX-asennus</fi>
   <fr_BE>Installation complète de LaTeX</fr_BE>
   <fr>Installation complète de LaTeX</fr>
   <gl_ES>Full LaTeX installation</gl_ES>
   <gu>Full LaTeX installation</gu>
   <he_IL>Full LaTeX installation</he_IL>
   <hi>पूर्ण LaTeX इंस्टॉल</hi>
   <hr>Full LaTeX installation</hr>
   <hu>Full LaTeX installation</hu>
   <id>Full LaTeX installation</id>
   <is>Full LaTeX installation</is>
   <it>Installazione completa di LaTeX</it>
   <ja>LaTeX のフルインストール</ja>
   <kk>Full LaTeX installation</kk>
   <ko>Full LaTeX installation</ko>
   <ku>Full LaTeX installation</ku>
   <lt>Full LaTeX installation</lt>
   <mk>Full LaTeX installation</mk>
   <mr>Full LaTeX installation</mr>
   <nb_NO>Full LaTeX installation</nb_NO>
   <nb>Full LaTeX-installasjon</nb>
   <nl_BE>Full LaTeX installation</nl_BE>
   <nl>Full LaTeX installation</nl>
   <or>Full LaTeX installation</or>
   <pl>Full LaTeX installation</pl>
   <pt_BR>LaTeX - Instalação Completa</pt_BR>
   <pt>Instalação completa do LateX</pt>
   <ro>Full LaTeX installation</ro>
   <ru>Full LaTeX installation</ru>
   <sk>Full LaTeX installation</sk>
   <sl>Celotna LaTeX namestitev</sl>
   <so>Full LaTeX installation</so>
   <sq>Instalim i plotë LaTeX-i</sq>
   <sr>Full LaTeX installation</sr>
   <sv>Full LaTeX installation</sv>
   <th>Full LaTeX installation</th>
   <tr>Tam LaTeX kurulumu</tr>
   <uk>Full LaTeX installation</uk>
   <vi>Full LaTeX installation</vi>
   <zh_CN>Full LaTeX installation</zh_CN>
   <zh_HK>Full LaTeX installation</zh_HK>
   <zh_TW>Full LaTeX installation</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
texlive
texlive-base
texlive-latex-base
texlive-latex-recommended
texlive-fonts-recommended
preview-latex-style
feynmf
cm-super
context
xdg-utils
texlive-font-utils
texlive-bibtex-extra
texlive-extra-utils
texlive-fonts-extra
texlive-formats-extra
texlive-pictures
texlive-latex-extra
texlive-metapost
texlive-music
texlive-pstricks
texlive-publishers
texlive-science
texlive-games
texlive-humanities
texlive-xetex
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
texlive
texlive-base
texlive-latex-base
texlive-latex-recommended
texlive-fonts-recommended
preview-latex-style
feynmf
cm-super
context
preview-latex-style
xdg-utils
texlive-font-utils
texlive-bibtex-extra
texlive-extra-utils
texlive-fonts-extra
texlive-formats-extra
texlive-pictures
texlive-latex-extra
texlive-metapost
texlive-music
texlive-pstricks
texlive-publishers
texlive-science
texlive-games
texlive-humanities
texlive-xetex
</uninstall_package_names>

</app>
