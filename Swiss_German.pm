<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Swiss (German)
</name>

<description>
   <am>Swiss (German) dictionary for hunspell</am>
   <ar>Swiss (German) dictionary for hunspell</ar>
   <be>Swiss (German) dictionary for hunspell</be>
   <bg>Swiss (German) dictionary for hunspell</bg>
   <bn>Swiss (German) dictionary for hunspell</bn>
   <ca>Diccionari Alemany (Suissa) per hunspell</ca>
   <cs>Swiss (German) dictionary for hunspell</cs>
   <da>Swiss (German) dictionary for hunspell</da>
   <de>Schweizerisches (deutschsprachiges) Wörterbuch für “Hunspell”</de>
   <el>Ελβετικό (Γερμανικό) λεξικό για hunspell</el>
   <en>Swiss (German) dictionary for hunspell</en>
   <es_ES>Diccionario Suizo (Alemán) para hunspell</es_ES>
   <es>Diccionario Suizo (Alemán) para hunspell</es>
   <et>Swiss (German) dictionary for hunspell</et>
   <eu>Swiss (German) dictionary for hunspell</eu>
   <fa>Swiss (German) dictionary for hunspell</fa>
   <fil_PH>Swiss (German) dictionary for hunspell</fil_PH>
   <fi>Swiss (German) dictionary for hunspell</fi>
   <fr_BE>Suisse (allemand) dictionnaire pour hunspell</fr_BE>
   <fr>Suisse (allemand) dictionnaire pour hunspell</fr>
   <gl_ES>Swiss (German) dictionary for hunspell</gl_ES>
   <gu>Swiss (German) dictionary for hunspell</gu>
   <he_IL>Swiss (German) dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु स्विस (जर्मन) शब्दकोष</hi>
   <hr>Swiss (German) dictionary for hunspell</hr>
   <hu>Swiss (German) dictionary for hunspell</hu>
   <id>Swiss (German) dictionary for hunspell</id>
   <is>Swiss (German) dictionary for hunspell</is>
   <it>Dizionario svizzero (tedesco) per hunspell</it>
   <ja>Hunspell 用スイス語（ドイツ）辞書</ja>
   <kk>Swiss (German) dictionary for hunspell</kk>
   <ko>Swiss (German) dictionary for hunspell</ko>
   <ku>Swiss (German) dictionary for hunspell</ku>
   <lt>Swiss (German) dictionary for hunspell</lt>
   <mk>Swiss (German) dictionary for hunspell</mk>
   <mr>Swiss (German) dictionary for hunspell</mr>
   <nb_NO>Swiss (German) dictionary for hunspell</nb_NO>
   <nb>Sveitsisk (tysk) ordliste for hunspell</nb>
   <nl_BE>Swiss (German) dictionary for hunspell</nl_BE>
   <nl>Swiss (German) dictionary for hunspell</nl>
   <or>Swiss (German) dictionary for hunspell</or>
   <pl>Swiss (German) dictionary for hunspell</pl>
   <pt_BR>Dicionário Suíço (Alemão) para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Suiço (Alemão) para hunspell</pt>
   <ro>Swiss (German) dictionary for hunspell</ro>
   <ru>Swiss (German) dictionary for hunspell</ru>
   <sk>Swiss (German) dictionary for hunspell</sk>
   <sl>Švicarski (nemški) slovar za hunspell</sl>
   <so>Swiss (German) dictionary for hunspell</so>
   <sq>Fjalor zviceranishte (gjermanisht) për hunspell</sq>
   <sr>Swiss (German) dictionary for hunspell</sr>
   <sv>Schweizisk (Tysk) ordbok för hunspell</sv>
   <th>Swiss (German) dictionary for hunspell</th>
   <tr>Hunspell için Almanca (İsviçre) sözlük</tr>
   <uk>Swiss (German) dictionary for hunspell</uk>
   <vi>Swiss (German) dictionary for hunspell</vi>
   <zh_CN>Swiss (German) dictionary for hunspell</zh_CN>
   <zh_HK>Swiss (German) dictionary for hunspell</zh_HK>
   <zh_TW>Swiss (German) dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-de-ch
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-de-ch
</uninstall_package_names>

</app>
