<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Chinese_CN_Default_Firefox_esr
</name>

<description>
   <am>Chinese_simplified localisation of Firefox ESR</am>
   <ar>Chinese_simplified localisation of Firefox ESR</ar>
   <be>Chinese_simplified localisation of Firefox ESR</be>
   <bg>Chinese_simplified localisation of Firefox ESR</bg>
   <bn>Chinese_simplified localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Xinès simplificat</ca>
   <cs>Chinese_simplified localisation of Firefox ESR</cs>
   <da>Chinese_simplified localisation of Firefox ESR</da>
   <de>Chinesische Lokalisation (vereinfachte reformierte Standardschriftzeichen) von “Firefox ESR”</de>
   <el>Απλοποιημένα Κινέζικα για Firefox ESR</el>
   <en>Chinese_simplified localisation of Firefox ESR</en>
   <es_ES>Localización en Chino_simplificado de Firefox ESR</es_ES>
   <es>Localización Chino simplificado de Firefox ESR</es>
   <et>Chinese_simplified localisation of Firefox ESR</et>
   <eu>Chinese_simplified localisation of Firefox ESR</eu>
   <fa>Chinese_simplified localisation of Firefox ESR</fa>
   <fil_PH>Chinese_simplified localisation of Firefox ESR</fil_PH>
   <fi>Chinese_simplified localisation of Firefox ESR</fi>
   <fr_BE>Localisation en chinois simplifié pour Firefox ESR</fr_BE>
   <fr>Localisation en chinois simplifié pour Firefox ESR</fr>
   <gl_ES>Chinese_simplified localisation of Firefox ESR</gl_ES>
   <gu>Chinese_simplified localisation of Firefox ESR</gu>
   <he_IL>Chinese_simplified localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का सरल_चीनी संस्करण</hi>
   <hr>Chinese_simplified localisation of Firefox ESR</hr>
   <hu>Chinese_simplified localisation of Firefox ESR</hu>
   <id>Chinese_simplified localisation of Firefox ESR</id>
   <is>Chinese_simplified localisation of Firefox ESR</is>
   <it>Localizzazione in cinese_semplificato di Firefox ESR</it>
   <ja>簡体中国語版 Firefox ESR</ja>
   <kk>Chinese_simplified localisation of Firefox ESR</kk>
   <ko>Chinese_simplified localisation of Firefox ESR</ko>
   <ku>Chinese_simplified localisation of Firefox ESR</ku>
   <lt>Chinese_simplified localisation of Firefox ESR</lt>
   <mk>Chinese_simplified localisation of Firefox ESR</mk>
   <mr>Chinese_simplified localisation of Firefox ESR</mr>
   <nb_NO>Chinese_simplified localisation of Firefox ESR</nb_NO>
   <nb>Kinesisk (forenklet) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Chinese_simplified localisation of Firefox ESR</nl_BE>
   <nl>Chinese (vereenvoudigde) lokalisatie van Firefox ESR</nl>
   <or>Chinese_simplified localisation of Firefox ESR</or>
   <pl>Chinese_simplified localisation of Firefox ESR</pl>
   <pt_BR>Chinês Simplificado Localização para o Firefox ESR</pt_BR>
   <pt>Chinês_simplificado Localização para Firefox ESR</pt>
   <ro>Chinese_simplified localisation of Firefox ESR</ro>
   <ru>Chinese_simplified localisation of Firefox ESR</ru>
   <sk>Chinese_simplified localisation of Firefox ESR</sk>
   <sl>Kitajske (poenostavljeno) krajevne nastavitve za Firefox ESR</sl>
   <so>Chinese_simplified localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në kinezçe të thjeshtuar</sq>
   <sr>Chinese_simplified localisation of Firefox ESR</sr>
   <sv>Kinesisk_förenklad lokalisering av Firefox ESR</sv>
   <th>Chinese_simplified localisation of Firefox ESR</th>
   <tr>Firefox ESR Basitleştirilmiş_Çince yerelleştirmesi</tr>
   <uk>Chinese_simplified localisation of Firefox ESR</uk>
   <vi>Chinese_simplified localisation of Firefox ESR</vi>
   <zh_CN>Chinese_simplified localisation of Firefox ESR</zh_CN>
   <zh_HK>Chinese_simplified localisation of Firefox ESR</zh_HK>
   <zh_TW>Chinese_simplified localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-zh-cn
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-zh-cn
</uninstall_package_names>

</app>
