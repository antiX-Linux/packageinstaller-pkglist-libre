<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Galician_Thunderbird
</name>

<description>
   <am>Galician localisation of Thunderbird</am>
   <ar>Galician localisation of Thunderbird</ar>
   <be>Galician localisation of Thunderbird</be>
   <bg>Galician localisation of Thunderbird</bg>
   <bn>Galician localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Gallec</ca>
   <cs>Galician localisation of Thunderbird</cs>
   <da>Galician localisation of Thunderbird</da>
   <de>Galegische Lokalisation von “Thunderbird”</de>
   <el>Γαλικία για Thunderbird</el>
   <en>Galician localisation of Thunderbird</en>
   <es_ES>Localización Gallego de Thunderbird</es_ES>
   <es>Localización Gallego de Thunderbird</es>
   <et>Galician localisation of Thunderbird</et>
   <eu>Galician localisation of Thunderbird</eu>
   <fa>Galician localisation of Thunderbird</fa>
   <fil_PH>Galician localisation of Thunderbird</fil_PH>
   <fi>Galician localisation of Thunderbird</fi>
   <fr_BE>Localisation en galicien pour Thunderbird</fr_BE>
   <fr>Localisation en galicien pour Thunderbird</fr>
   <gl_ES>Galician localisation of Thunderbird</gl_ES>
   <gu>Galician localisation of Thunderbird</gu>
   <he_IL>Galician localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का गलिसियाई संस्करण</hi>
   <hr>Galician localisation of Thunderbird</hr>
   <hu>Galician localisation of Thunderbird</hu>
   <id>Galician localisation of Thunderbird</id>
   <is>Galician localisation of Thunderbird</is>
   <it>Localizzazione galiziana di Thunderbird</it>
   <ja>ガリシア語版 Thunderbird</ja>
   <kk>Galician localisation of Thunderbird</kk>
   <ko>Galician localisation of Thunderbird</ko>
   <ku>Galician localisation of Thunderbird</ku>
   <lt>Galician localisation of Thunderbird</lt>
   <mk>Galician localisation of Thunderbird</mk>
   <mr>Galician localisation of Thunderbird</mr>
   <nb_NO>Galician localisation of Thunderbird</nb_NO>
   <nb>Galisisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Galician localisation of Thunderbird</nl_BE>
   <nl>Galicische lokalisatie van Thunderbird</nl>
   <or>Galician localisation of Thunderbird</or>
   <pl>Galician localisation of Thunderbird</pl>
   <pt_BR>Galego Localização para o Thunderbird</pt_BR>
   <pt>Galego Localização para Thunderbird</pt>
   <ro>Galician localisation of Thunderbird</ro>
   <ru>Galician localisation of Thunderbird</ru>
   <sk>Galician localisation of Thunderbird</sk>
   <sl>Galicijske krajevne of Thunderbird</sl>
   <so>Galician localisation of Thunderbird</so>
   <sq>Fjalor galicisht për hunspell</sq>
   <sr>Galician localisation of Thunderbird</sr>
   <sv>Galicisk lokalisering av Thunderbird</sv>
   <th>Galician localisation of Thunderbird</th>
   <tr>Thunderbird Galiçyaca yerelleştirmesi</tr>
   <uk>Galician localisation of Thunderbird</uk>
   <vi>Galician localisation of Thunderbird</vi>
   <zh_CN>Galician localisation of Thunderbird</zh_CN>
   <zh_HK>Galician localisation of Thunderbird</zh_HK>
   <zh_TW>Galician localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-gl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-gl
</uninstall_package_names>

</app>
