<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hungarian
</name>

<description>
   <am>Hungarian dictionary for hunspell</am>
   <ar>Hungarian dictionary for hunspell</ar>
   <be>Hungarian dictionary for hunspell</be>
   <bg>Hungarian dictionary for hunspell</bg>
   <bn>Hungarian dictionary for hunspell</bn>
   <ca>Diccionari Hongarès per hunspell</ca>
   <cs>Hungarian dictionary for hunspell</cs>
   <da>Hungarian dictionary for hunspell</da>
   <de>Ungarisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό στα Ουγγρικά για hunspell</el>
   <en>Hungarian dictionary for hunspell</en>
   <es_ES>Diccionario Hungaro para hunspell</es_ES>
   <es>Diccionario Húngaro para hunspell</es>
   <et>Hungarian dictionary for hunspell</et>
   <eu>Hungarian dictionary for hunspell</eu>
   <fa>Hungarian dictionary for hunspell</fa>
   <fil_PH>Hungarian dictionary for hunspell</fil_PH>
   <fi>Hungarian dictionary for hunspell</fi>
   <fr_BE>Hongrois dictionnaire pour hunspell</fr_BE>
   <fr>Hongrois dictionnaire pour hunspell</fr>
   <gl_ES>Hungarian dictionary for hunspell</gl_ES>
   <gu>Hungarian dictionary for hunspell</gu>
   <he_IL>Hungarian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु हंगेरियाई शब्दकोष</hi>
   <hr>Hungarian dictionary for hunspell</hr>
   <hu>Hungarian dictionary for hunspell</hu>
   <id>Hungarian dictionary for hunspell</id>
   <is>Hungarian dictionary for hunspell</is>
   <it>Dizionario ungherese per hunspell</it>
   <ja>Hunspell 用ハンガリー語辞書</ja>
   <kk>Hungarian dictionary for hunspell</kk>
   <ko>Hungarian dictionary for hunspell</ko>
   <ku>Hungarian dictionary for hunspell</ku>
   <lt>Hungarian dictionary for hunspell</lt>
   <mk>Hungarian dictionary for hunspell</mk>
   <mr>Hungarian dictionary for hunspell</mr>
   <nb_NO>Hungarian dictionary for hunspell</nb_NO>
   <nb>Ungarsk ordliste for hunspell</nb>
   <nl_BE>Hungarian dictionary for hunspell</nl_BE>
   <nl>Hungarian dictionary for hunspell</nl>
   <or>Hungarian dictionary for hunspell</or>
   <pl>Hungarian dictionary for hunspell</pl>
   <pt_BR>Dicionário Húngaro para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Hungáro para hunspell</pt>
   <ro>Hungarian dictionary for hunspell</ro>
   <ru>Hungarian dictionary for hunspell</ru>
   <sk>Hungarian dictionary for hunspell</sk>
   <sl>Madžarski slovar za hunspell</sl>
   <so>Hungarian dictionary for hunspell</so>
   <sq>Fjalor hungarisht për hunspell</sq>
   <sr>Hungarian dictionary for hunspell</sr>
   <sv>Ungersk ordbok för hunspell</sv>
   <th>Hungarian dictionary for hunspell</th>
   <tr>Hunspell için Macarca sözlük</tr>
   <uk>Hungarian dictionary for hunspell</uk>
   <vi>Hungarian dictionary for hunspell</vi>
   <zh_CN>Hungarian dictionary for hunspell</zh_CN>
   <zh_HK>Hungarian dictionary for hunspell</zh_HK>
   <zh_TW>Hungarian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-hu
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-hu
</uninstall_package_names>

</app>
