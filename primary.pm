<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Children
</category>

<name>
Primary
</name>

<description>
   <am>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</am>
   <ar>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</ar>
   <be>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</be>
   <bg>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</bg>
   <bn>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</bn>
   <ca>Primària: inclou celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, i marble</ca>
   <cs>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</cs>
   <da>Grundskole. Inkluderer: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype og marble</da>
   <de>Grundschule: Enthält Celestia-Gnome, Gcompris, Laby, Ri-Li, Stellarium, Tuxmath, Tuxpaint, Tuxtype und Marmor.</de>
   <el>Πρωτοβάθμια. Περιλαμβάνει: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype και marble</el>
   <en>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</en>
   <es_ES>Primario. Incluye: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, y marble</es_ES>
   <es>Primario. Incluye: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, y marble</es>
   <et>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</et>
   <eu>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</eu>
   <fa>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</fa>
   <fil_PH>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</fil_PH>
   <fi>Ekaluokka. Sisältää ohjelmat: lestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, ja marble</fi>
   <fr_BE>Primaire. Inclus: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, et marble</fr_BE>
   <fr>Primaire. Inclus: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, et marble</fr>
   <gl_ES>Ensino básico, 1º e 2º ciclos. Inclúe:: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, e marble</gl_ES>
   <gu>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</gu>
   <he_IL>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</he_IL>
   <hi>माध्यमिक विद्यालय। सम्मिलित : celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype व marble</hi>
   <hr>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</hr>
   <hu>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</hu>
   <id>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</id>
   <is>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</is>
   <it>Medie. Include: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, e marble</it>
   <ja>プライマリ。以下を含みます: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, marble</ja>
   <kk>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</kk>
   <ko>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</ko>
   <ku>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</ku>
   <lt>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</lt>
   <mk>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</mk>
   <mr>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</mr>
   <nb_NO>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</nb_NO>
   <nb>Barneskole. Inkluderer celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype og marble</nb>
   <nl_BE>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</nl_BE>
   <nl>Basis. Inclusief: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, en marble</nl>
   <or>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</or>
   <pl>Szkoła podstawowa. Zawiera: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype i marble</pl>
   <pt_BR>Jogos para ensino fundamental. Inclui: celestia-gnome (simulação do espaço), gcompris (suíte educacional), laby (aprender a programar), ri-li (jogo de quebra-cabeça do trem de brinquedo), stellarium (planetário), tuxmath (jogos de matemática), tuxpaint (aprender a desenhar), tuxtype (aprender digitação) e marble (globo terrestre virtual)</pt_BR>
   <pt>Ensino básico, 1º e 2º ciclos. Inclui: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, e marble</pt>
   <ro>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</ro>
   <ru>Начальная школа, включает: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype и marble</ru>
   <sk>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</sk>
   <sl>Osnovna šola. Vsebuje: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype in marble</sl>
   <so>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</so>
   <sq>Fillor. Përfshin: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, dhe marble</sq>
   <sr>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</sr>
   <sv>Primary. Inkluderar: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, och marble</sv>
   <th>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</th>
   <tr>İlkokul. İçeriği: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype ve marble</tr>
   <uk>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</uk>
   <vi>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</vi>
   <zh_CN>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</zh_CN>
   <zh_HK>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</zh_HK>
   <zh_TW>Primary. Includes: celestia-gnome, gcompris, laby, ri-li, stellarium, tuxmath, tuxpaint, tuxtype, and marble</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gcompris-qt
laby
ri-li
stellarium
tuxmath
tuxpaint
tuxtype
marble-qt
marble-maps
marble-plugins
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gcompris-qt
laby
ri-li
stellarium
tuxmath
tuxpaint
tuxtype
marble-qt
marble-maps
marble-plugins
</uninstall_package_names>
</app>
