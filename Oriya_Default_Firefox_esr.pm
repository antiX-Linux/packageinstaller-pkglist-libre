<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Oriya_Default_Firefox_esr
</name>

<description>
   <am>Oriya localisation of Firefox-ESR</am>
   <ar>Oriya localisation of Firefox-ESR</ar>
   <be>Oriya localisation of Firefox-ESR</be>
   <bg>Oriya localisation of Firefox-ESR</bg>
   <bn>Oriya localisation of Firefox-ESR</bn>
   <ca>Localització de Firefox-ESR en Oriya</ca>
   <cs>Oriya localisation of Firefox-ESR</cs>
   <da>Oriya localisation of Firefox-ESR</da>
   <de>Indische Lokalisation von “Firefox-ESR” für die Sprache “Odia”</de>
   <el>Εντοπισμός Oriya του Firefox-ESR</el>
   <en>Oriya localisation of Firefox-ESR</en>
   <es_ES>Localización Oriya de Firefox-ESR</es_ES>
   <es>Localización Oriya de Firefox-ESR</es>
   <et>Oriya localisation of Firefox-ESR</et>
   <eu>Oriya localisation of Firefox-ESR</eu>
   <fa>Oriya localisation of Firefox-ESR</fa>
   <fil_PH>Oriya localisation of Firefox-ESR</fil_PH>
   <fi>Oriya localisation of Firefox-ESR</fi>
   <fr_BE>Localisation en Oriya pour Firefox-ESR</fr_BE>
   <fr>Localisation en Oriya pour Firefox-ESR</fr>
   <gl_ES>Oriya localisation of Firefox-ESR</gl_ES>
   <gu>Oriya localisation of Firefox-ESR</gu>
   <he_IL>Oriya localisation of Firefox-ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का ओड़िया संस्करण</hi>
   <hr>Oriya localisation of Firefox-ESR</hr>
   <hu>Oriya localisation of Firefox-ESR</hu>
   <id>Oriya localisation of Firefox-ESR</id>
   <is>Oriya localisation of Firefox-ESR</is>
   <it>Localizzazione oriya di Firefox ESR</it>
   <ja>オリヤー語版 Firefox-ESR</ja>
   <kk>Oriya localisation of Firefox-ESR</kk>
   <ko>Oriya localisation of Firefox-ESR</ko>
   <ku>Oriya localisation of Firefox-ESR</ku>
   <lt>Oriya localisation of Firefox-ESR</lt>
   <mk>Oriya localisation of Firefox-ESR</mk>
   <mr>Oriya localisation of Firefox-ESR</mr>
   <nb_NO>Oriya localisation of Firefox-ESR</nb_NO>
   <nb>Oriya lokaltilpassing av Firefox-ESR</nb>
   <nl_BE>Oriya localisation of Firefox-ESR</nl_BE>
   <nl>Oriya localisation of Firefox-ESR</nl>
   <or>Oriya localisation of Firefox-ESR</or>
   <pl>Oriya localisation of Firefox-ESR</pl>
   <pt_BR>Dicionário Oriá para hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Oriá Localização para Firefox ESR</pt>
   <ro>Oriya localisation of Firefox-ESR</ro>
   <ru>Oriya localisation of Firefox-ESR</ru>
   <sk>Oriya localisation of Firefox-ESR</sk>
   <sl>Orijske krajevne nastavitve za Firefox-ESR</sl>
   <so>Oriya localisation of Firefox-ESR</so>
   <sq>Oriya localisation of Firefox-ESR</sq>
   <sr>Oriya localisation of Firefox-ESR</sr>
   <sv>Oriya lokalisering av Firefox-ESR</sv>
   <th>Oriya localisation of Firefox-ESR</th>
   <tr>Firefox-ESR Oriyaca yerelleştirmesi</tr>
   <uk>Oriya localisation of Firefox-ESR</uk>
   <vi>Oriya localisation of Firefox-ESR</vi>
   <zh_CN>Oriya localisation of Firefox-ESR</zh_CN>
   <zh_HK>Oriya localisation of Firefox-ESR</zh_HK>
   <zh_TW>Oriya localisation of Firefox-ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-or
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-or
</uninstall_package_names>

</app>
