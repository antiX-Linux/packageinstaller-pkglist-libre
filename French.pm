<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
French
</name>

<description>
   <am>French dictionary for hunspell</am>
   <ar>French dictionary for hunspell</ar>
   <be>French dictionary for hunspell</be>
   <bg>French dictionary for hunspell</bg>
   <bn>French dictionary for hunspell</bn>
   <ca>Diccionari Francès per hunspell</ca>
   <cs>French dictionary for hunspell</cs>
   <da>French dictionary for hunspell</da>
   <de>Französisches Wörterbuch für “Hunspell”</de>
   <el>Γαλλικό λεξικό για hunspell</el>
   <en>French dictionary for hunspell</en>
   <es_ES>Diccionario Francés para hunspell</es_ES>
   <es>Diccionario Francés para hunspell</es>
   <et>French dictionary for hunspell</et>
   <eu>French dictionary for hunspell</eu>
   <fa>French dictionary for hunspell</fa>
   <fil_PH>French dictionary for hunspell</fil_PH>
   <fi>French dictionary for hunspell</fi>
   <fr_BE>Français dictionnaire pour hunspell</fr_BE>
   <fr>Français dictionnaire pour hunspell</fr>
   <gl_ES>French dictionary for hunspell</gl_ES>
   <gu>French dictionary for hunspell</gu>
   <he_IL>French dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु फ्रेंच शब्दकोष</hi>
   <hr>French dictionary for hunspell</hr>
   <hu>French dictionary for hunspell</hu>
   <id>French dictionary for hunspell</id>
   <is>French dictionary for hunspell</is>
   <it>Dizionario francese per hunspell</it>
   <ja>Hunspell 用フランス語辞書</ja>
   <kk>French dictionary for hunspell</kk>
   <ko>French dictionary for hunspell</ko>
   <ku>French dictionary for hunspell</ku>
   <lt>French dictionary for hunspell</lt>
   <mk>French dictionary for hunspell</mk>
   <mr>French dictionary for hunspell</mr>
   <nb_NO>French dictionary for hunspell</nb_NO>
   <nb>Fransk ordliste for hunspell</nb>
   <nl_BE>French dictionary for hunspell</nl_BE>
   <nl>Frans woordenboek voor hunspell</nl>
   <or>French dictionary for hunspell</or>
   <pl>French dictionary for hunspell</pl>
   <pt_BR>Dicionário Francês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Francês para hunspell</pt>
   <ro>French dictionary for hunspell</ro>
   <ru>French dictionary for hunspell</ru>
   <sk>French dictionary for hunspell</sk>
   <sl>Francoski slovar za hunspell</sl>
   <so>French dictionary for hunspell</so>
   <sq>Fjalor frëngjisht për hunspell</sq>
   <sr>French dictionary for hunspell</sr>
   <sv>Fransk ordbok för hunspell</sv>
   <th>French dictionary for hunspell</th>
   <tr>Hunspell için Fransızca sözlük</tr>
   <uk>French dictionary for hunspell</uk>
   <vi>French dictionary for hunspell</vi>
   <zh_CN>French dictionary for hunspell</zh_CN>
   <zh_HK>French dictionary for hunspell</zh_HK>
   <zh_TW>French dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-fr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-fr
</uninstall_package_names>

</app>
