<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Chinese_Input_fcitx
</name>

<description>
   <am>Chinese fcitx</am>
   <ar>Chinese fcitx</ar>
   <be>Chinese fcitx</be>
   <bg>Chinese fcitx</bg>
   <bn>Chinese fcitx</bn>
   <ca>FCTIX en Xinès</ca>
   <cs>Chinese fcitx</cs>
   <da>Kinesisk fcitx</da>
   <de>Chinesisch Fcitx</de>
   <el>Κινέζικα fcitx</el>
   <en>Chinese fcitx</en>
   <es_ES>Cliente de correo liviano Claws Mail</es_ES>
   <es>Fcitx Chino</es>
   <et>Chinese fcitx</et>
   <eu>Chinese fcitx</eu>
   <fa>Chinese fcitx</fa>
   <fil_PH>Chinese fcitx</fil_PH>
   <fi>Kiinalainen fctx</fi>
   <fr_BE>Chinois fcitx</fr_BE>
   <fr>Chinois fcitx</fr>
   <gl_ES>Chinés fcitx</gl_ES>
   <gu>Chinese fcitx</gu>
   <he_IL>Chinese fcitx</he_IL>
   <hi>चीनी fcitx</hi>
   <hr>Chinese fcitx</hr>
   <hu>Chinese fcitx</hu>
   <id>Chinese fcitx</id>
   <is>Chinese fcitx</is>
   <it>Chinese fcitx</it>
   <ja>中国語用 fcitx</ja>
   <kk>Chinese fcitx</kk>
   <ko>Chinese fcitx</ko>
   <ku>Chinese fcitx</ku>
   <lt>Kinų fcitx</lt>
   <mk>Chinese fcitx</mk>
   <mr>Chinese fcitx</mr>
   <nb_NO>Chinese fcitx</nb_NO>
   <nb>Kinesisk fcitx</nb>
   <nl_BE>Chinese fcitx</nl_BE>
   <nl>Chinese fcitx</nl>
   <or>Chinese fcitx</or>
   <pl>Chiński fcitx</pl>
   <pt_BR>fcitx para o idioma Chinês</pt_BR>
   <pt>Chinês fcitx</pt>
   <ro>Chinese fcitx</ro>
   <ru>Chinese fcitx</ru>
   <sk>Chinese fcitx</sk>
   <sl>Kitajski fcitx</sl>
   <so>Chinese fcitx</so>
   <sq>fcitx për kinezçen</sq>
   <sr>Chinese fcitx</sr>
   <sv>Kinesisk fcitx</sv>
   <th>fcitx ภาษาจีน</th>
   <tr>Çince fcitx</tr>
   <uk>Chinese fcitx</uk>
   <vi>Chinese fcitx</vi>
   <zh_CN>中文 fcitx 输入法</zh_CN>
   <zh_HK>Chinese fcitx</zh_HK>
   <zh_TW>Chinese fcitx</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
fcitx
fcitx-config-gtk
fcitx-frontend-all
fcitx-frontend-fbterm
fcitx-tools
fcitx-ui-classic
fcitx-ui-light
fcitx-googlepinyin
fcitx-libpinyin
fcitx-rime
fcitx-sunpinyin
fcitx-chewing
fcitx-table-all
fcitx-frontend-gtk2
fcitx-frontend-gtk3
fcitx-frontend-qt5
im-config
mozc-utils-gui
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
fcitx
fcitx-config-gtk
fcitx-frontend-all
fcitx-frontend-fbterm
fcitx-tools
fcitx-ui-classic
fcitx-ui-light
fcitx-googlepinyin
fcitx-libpinyin
fcitx-rime
fcitx-sunpinyin
fcitx-chewing
fcitx-table-all
fcitx-frontend-gtk3
fcitx-frontend-qt4
fcitx-frontend-qt5
im-config
mozc-utils-gui
</uninstall_package_names>
</app>
