<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Spanish
</name>

<description>
   <am>Spanish dictionary for hunspell</am>
   <ar>Spanish dictionary for hunspell</ar>
   <be>Spanish dictionary for hunspell</be>
   <bg>Spanish dictionary for hunspell</bg>
   <bn>Spanish dictionary for hunspell</bn>
   <ca>Diccionari Castellà per hunspell</ca>
   <cs>Spanish dictionary for hunspell</cs>
   <da>Spanish dictionary for hunspell</da>
   <de>Spanisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Ισπανικά για hunspell</el>
   <en>Spanish dictionary for hunspell</en>
   <es_ES>Diccionario Español para hunspell</es_ES>
   <es>Diccionario Español para hunspell</es>
   <et>Spanish dictionary for hunspell</et>
   <eu>Spanish dictionary for hunspell</eu>
   <fa>Spanish dictionary for hunspell</fa>
   <fil_PH>Spanish dictionary for hunspell</fil_PH>
   <fi>Spanish dictionary for hunspell</fi>
   <fr_BE>Espagnol dictionnaire pour hunspell</fr_BE>
   <fr>Espagnol dictionnaire pour hunspell</fr>
   <gl_ES>Spanish dictionary for hunspell</gl_ES>
   <gu>Spanish dictionary for hunspell</gu>
   <he_IL>Spanish dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु स्पेनिश शब्दकोष</hi>
   <hr>Spanish dictionary for hunspell</hr>
   <hu>Spanish dictionary for hunspell</hu>
   <id>Spanish dictionary for hunspell</id>
   <is>Spanish dictionary for hunspell</is>
   <it>Dizionario spagnolo per hunspell</it>
   <ja>Hunspell 用スペイン語辞書</ja>
   <kk>Spanish dictionary for hunspell</kk>
   <ko>Spanish dictionary for hunspell</ko>
   <ku>Spanish dictionary for hunspell</ku>
   <lt>Spanish dictionary for hunspell</lt>
   <mk>Spanish dictionary for hunspell</mk>
   <mr>Spanish dictionary for hunspell</mr>
   <nb_NO>Spanish dictionary for hunspell</nb_NO>
   <nb>Spansk ordliste for hunspell</nb>
   <nl_BE>Spanish dictionary for hunspell</nl_BE>
   <nl>Spanish dictionary for hunspell</nl>
   <or>Spanish dictionary for hunspell</or>
   <pl>Spanish dictionary for hunspell</pl>
   <pt_BR>Dicionário Espanhol para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Espanhol para hunspell</pt>
   <ro>Spanish dictionary for hunspell</ro>
   <ru>Spanish dictionary for hunspell</ru>
   <sk>Spanish dictionary for hunspell</sk>
   <sl>Španski slovar za hunspell</sl>
   <so>Spanish dictionary for hunspell</so>
   <sq>Fjalor spanjisht për hunspell</sq>
   <sr>Spanish dictionary for hunspell</sr>
   <sv>Spansk ordbok för hunspell</sv>
   <th>Spanish dictionary for hunspell</th>
   <tr>Hunspell için İspanyolca sözlük</tr>
   <uk>Spanish dictionary for hunspell</uk>
   <vi>Spanish dictionary for hunspell</vi>
   <zh_CN>Spanish dictionary for hunspell</zh_CN>
   <zh_HK>Spanish dictionary for hunspell</zh_HK>
   <zh_TW>Spanish dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-es
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-es
</uninstall_package_names>

</app>
