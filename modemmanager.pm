<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
ModemManager
</name>

<description>
   <am>Controls mobile broadband (2G/3G/4G) devices and connections</am>
   <ar>Controls mobile broadband (2G/3G/4G) devices and connections</ar>
   <be>Controls mobile broadband (2G/3G/4G) devices and connections</be>
   <bg>Controls mobile broadband (2G/3G/4G) devices and connections</bg>
   <bn>Controls mobile broadband (2G/3G/4G) devices and connections</bn>
   <ca>Controla dispositius i connexions de banda ampla (2G/3G/4G)</ca>
   <cs>Controls mobile broadband (2G/3G/4G) devices and connections</cs>
   <da>Controls mobile broadband (2G/3G/4G) devices and connections</da>
   <de>Steuert Geräte und Verbindungen mit den Mobilfunkstandards 2G, 3G und 4G.</de>
   <el>Ελέγχει κινητές συσκευές ευρείας ζώνης (2G/3G/4G) και συνδέσεις</el>
   <en>Controls mobile broadband (2G/3G/4G) devices and connections</en>
   <es_ES>Controla dispositivos y conexiones de banda ancha móvil (2G/3G/4G)</es_ES>
   <es>Controla dispositivos y conexiones de banda ancha móvil (2G / 3G / 4G)</es>
   <et>Controls mobile broadband (2G/3G/4G) devices and connections</et>
   <eu>Controls mobile broadband (2G/3G/4G) devices and connections</eu>
   <fa>Controls mobile broadband (2G/3G/4G) devices and connections</fa>
   <fil_PH>Controls mobile broadband (2G/3G/4G) devices and connections</fil_PH>
   <fi>Hallitsee mobiiliverkon (2G/3G/4G) laitteita sekä yhteyksiä</fi>
   <fr_BE>Contrôle les appareils et les connexions à large bande mobile (2G/3G/4G)</fr_BE>
   <fr>Contrôle les appareils et les connexions à large bande mobile (2G/3G/4G)</fr>
   <gl_ES>Controls mobile broadband (2G/3G/4G) devices and connections</gl_ES>
   <gu>Controls mobile broadband (2G/3G/4G) devices and connections</gu>
   <he_IL>Controls mobile broadband (2G/3G/4G) devices and connections</he_IL>
   <hi>मोबाइल ब्रॉडबैंड (2जी/3जी/4जी) उपकरणों व कनेक्शन हेतु नियंत्रण</hi>
   <hr>Controls mobile broadband (2G/3G/4G) devices and connections</hr>
   <hu>Controls mobile broadband (2G/3G/4G) devices and connections</hu>
   <id>Controls mobile broadband (2G/3G/4G) devices and connections</id>
   <is>Controls mobile broadband (2G/3G/4G) devices and connections</is>
   <it>Controlla i dispositivi e le connessioni a banda larga mobile (2G/3G/4G)</it>
   <ja>モバイルブロードバンド（2G/3G/4G）デバイスと接続の制御</ja>
   <kk>Controls mobile broadband (2G/3G/4G) devices and connections</kk>
   <ko>Controls mobile broadband (2G/3G/4G) devices and connections</ko>
   <ku>Controls mobile broadband (2G/3G/4G) devices and connections</ku>
   <lt>Controls mobile broadband (2G/3G/4G) devices and connections</lt>
   <mk>Controls mobile broadband (2G/3G/4G) devices and connections</mk>
   <mr>Controls mobile broadband (2G/3G/4G) devices and connections</mr>
   <nb_NO>Controls mobile broadband (2G/3G/4G) devices and connections</nb_NO>
   <nb>Styrer enheter for mobilt bredbånd (2G/3G/4G) og tilkoblinger</nb>
   <nl_BE>Controls mobile broadband (2G/3G/4G) devices and connections</nl_BE>
   <nl>Controls mobile broadband (2G/3G/4G) devices and connections</nl>
   <or>Controls mobile broadband (2G/3G/4G) devices and connections</or>
   <pl>Controls mobile broadband (2G/3G/4G) devices and connections</pl>
   <pt_BR>Controle de dispositivos e conexões de banda larga móvel (2G/3G/4G)</pt_BR>
   <pt>Controla dispositivos e ligações móveis (2G/3G/4G)</pt>
   <ro>Controls mobile broadband (2G/3G/4G) devices and connections</ro>
   <ru>Controls mobile broadband (2G/3G/4G) devices and connections</ru>
   <sk>Controls mobile broadband (2G/3G/4G) devices and connections</sk>
   <sl>Nadzor and mobilnimi širokopoasovnimi napravami (2G/3G/4G) in povezavami</sl>
   <so>Controls mobile broadband (2G/3G/4G) devices and connections</so>
   <sq>Kontrollon pajisje dhe lidhje celulare (2G/3G/4G)</sq>
   <sr>Controls mobile broadband (2G/3G/4G) devices and connections</sr>
   <sv>Kontrollerar mobila bredbands (2G/3G/4G) enheter och anslutningar</sv>
   <th>Controls mobile broadband (2G/3G/4G) devices and connections</th>
   <tr>Mobil geniş bant (2G/3G/4G) aygıtları ve bağlantıları kontrol eder</tr>
   <uk>Controls mobile broadband (2G/3G/4G) devices and connections</uk>
   <vi>Controls mobile broadband (2G/3G/4G) devices and connections</vi>
   <zh_CN>Controls mobile broadband (2G/3G/4G) devices and connections</zh_CN>
   <zh_HK>Controls mobile broadband (2G/3G/4G) devices and connections</zh_HK>
   <zh_TW>Controls mobile broadband (2G/3G/4G) devices and connections</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
mobile-broadband-provider-info
modemmanager
usb-modeswitch
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
mobile-broadband-provider-info
modemmanager
usb-modeswitch
</uninstall_package_names>
</app>
