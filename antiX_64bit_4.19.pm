<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Kernels
</category>

<name>
Kernel-antiX_64bit_LTS_4.19
</name>

<description>
   <am>antiX Kernel 64 bit  LTS (4.19.276)</am>
   <ar>antiX Kernel 64 bit  LTS (4.19.276)</ar>
   <be>antiX Kernel 64 bit  LTS (4.19.276)</be>
   <bg>antiX Kernel 64 bit  LTS (4.19.276)</bg>
   <bn>antiX Kernel 64 bit  LTS (4.19.276)</bn>
   <ca>Kernel antiX 64 bit LTS (4.19.276)</ca>
   <cs>antiX Kernel 64 bit  LTS (4.19.276)</cs>
   <da>antiX Kernel 64 bit  LTS (4.19.276)</da>
   <de>antiX Kernel 64 bit  LTS (4.19.276)</de>
   <el>antiX πυρήνα 64 bit LTS (4.19.276)</el>
   <en>antiX Kernel 64 bit  LTS (4.19.276)</en>
   <es_ES>Kernel antiX 64 bit LTS (4.19.276)</es_ES>
   <es>Kernel antiX 64 bit LTS (4.19.276)</es>
   <et>antiX Kernel 64 bit  LTS (4.19.276)</et>
   <eu>antiX Kernel 64 bit  LTS (4.19.276)</eu>
   <fa>antiX Kernel 64 bit  LTS (4.19.276)</fa>
   <fil_PH>antiX Kernel 64 bit  LTS (4.19.276)</fil_PH>
   <fi>antiX Kernel 64 bit  LTS (4.19.276)</fi>
   <fr_BE>antiX Noyau 64 bit LTS (4.19.276)</fr_BE>
   <fr>antiX Noyau 64 bit LTS (4.19.276)</fr>
   <gl_ES>antiX Kernel 64 bit  LTS (4.19.276)</gl_ES>
   <gu>antiX Kernel 64 bit  LTS (4.19.276)</gu>
   <he_IL>antiX Kernel 64 bit  LTS (4.19.276)</he_IL>
   <hi>antiX Kernel 64 bit  LTS (4.19.276)</hi>
   <hr>antiX Kernel 64 bit  LTS (4.19.276)</hr>
   <hu>antiX Kernel 64 bit  LTS (4.19.276)</hu>
   <id>antiX Kernel 64 bit  LTS (4.19.276)</id>
   <is>antiX Kernel 64 bit  LTS (4.19.276)</is>
   <it>Kernel antiX LTS 64 bit (4.19.276)</it>
   <ja>antiX Kernel 64 bit  LTS (4.19.276)</ja>
   <kk>antiX Kernel 64 bit  LTS (4.19.276)</kk>
   <ko>antiX Kernel 64 bit  LTS (4.19.276)</ko>
   <ku>antiX Kernel 64 bit  LTS (4.19.276)</ku>
   <lt>antiX Kernel 64 bit  LTS (4.19.276)</lt>
   <mk>antiX Kernel 64 bit  LTS (4.19.276)</mk>
   <mr>antiX Kernel 64 bit  LTS (4.19.276)</mr>
   <nb_NO>antiX Kernel 64 bit  LTS (4.19.276)</nb_NO>
   <nb>antiX-kjerne, 64 biters (4.19.276)</nb>
   <nl_BE>antiX Kernel 64 bit  LTS (4.19.276)</nl_BE>
   <nl>antiX Kernel 64 bit LTS (4.19.276)</nl>
   <or>antiX Kernel 64 bit  LTS (4.19.276)</or>
   <pl>antiX Kernel 64 bit  LTS (4.19.276)</pl>
   <pt_BR>Núcleo (Kernel) do antiX 4.19.276 de 64 bits LTS</pt_BR>
   <pt>Núcleo (Kernel) do antiX de 64 bits LTS (4.19.276)</pt>
   <ro>antiX Kernel 64 bit  LTS (4.19.276)</ro>
   <ru>antiX Kernel 64 bit  LTS (4.19.276)</ru>
   <sk>antiX Kernel 64 bit  LTS (4.19.276)</sk>
   <sl>antiX 64 bitno jedro (4.19.276)</sl>
   <so>antiX Kernel 64 bit  LTS (4.19.276)</so>
   <sq>Kernel antiX-i LTS, 64 bit LTS (4.19.276)</sq>
   <sr>antiX Kernel 64 bit  LTS (4.19.276)</sr>
   <sv>antiX Kärna 64 bit LTS (4.19.276)</sv>
   <th>antiX Kernel 64 bit  LTS (4.19.276)</th>
   <tr>antiX Kernel 64 bit  LTS (4.19.276)</tr>
   <uk>antiX Kernel 64 bit  LTS (4.19.276)</uk>
   <vi>antiX Kernel 64 bit  LTS (4.19.276)</vi>
   <zh_CN>antiX Kernel 64 bit  LTS (4.19.276)</zh_CN>
   <zh_HK>antiX Kernel 64 bit  LTS (4.19.276)</zh_HK>
   <zh_TW>antiX Kernel 64 bit  LTS (4.19.276)</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
linux-image-4.19.276-antix.1-amd64-smp
linux-headers-4.19.276-antix.1-amd64-smp
libelf-dev
libc6-dev
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-4.19.276-antix.1-amd64-smp
linux-headers-4.19.276-antix.1-amd64-smp
</uninstall_package_names>

</app>
