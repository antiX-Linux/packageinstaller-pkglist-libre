<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Audio
</category>

<name>
Qmmp
</name>

<description>
   <am>a winamp/xmms type feature-rich audio player written in Qt</am>
   <ar>a winamp/xmms type feature-rich audio player written in Qt</ar>
   <be>a winamp/xmms type feature-rich audio player written in Qt</be>
   <bg>a winamp/xmms type feature-rich audio player written in Qt</bg>
   <bn>a winamp/xmms type feature-rich audio player written in Qt</bn>
   <ca>Reproductor d'àudio del tipus winamp/xmms amb moltes característiques, escrit en QT</ca>
   <cs>a winamp/xmms type feature-rich audio player written in Qt</cs>
   <da>en lydafspiller med mange funktionaliteter i stil med winamp/xmms, skrevet i Qt</da>
   <de>Ein in Qt geschriebener, funktionsreicher Audio-Player vom Typ Winamp/xmms</de>
   <el>Πρόγραμμα αναπαραγωγής ήχου σαν το winamp/xmms γραμμένο σε Qt</el>
   <en>a winamp/xmms type feature-rich audio player written in Qt</en>
   <es_ES>Reproductor de audio tipo winamp/xmms muy completo escrito en Qt</es_ES>
   <es>Reproductor de audio tipo winamp/xmms muy completo escrito en Qt</es>
   <et>a winamp/xmms type feature-rich audio player written in Qt</et>
   <eu>a winamp/xmms type feature-rich audio player written in Qt</eu>
   <fa>a winamp/xmms type feature-rich audio player written in Qt</fa>
   <fil_PH>a winamp/xmms type feature-rich audio player written in Qt</fil_PH>
   <fi>Winamp/XMMS-tyylinen monipuolinen musiikkisoitin joka on kirjoitettu Qt:llä</fi>
   <fr_BE>Un lecteur audio très complet de type winamp/xmms basé sur Qt</fr_BE>
   <fr>Un lecteur audio très complet de type winamp/xmms basé sur Qt</fr>
   <gl_ES>Reprodutor de radio cheo de funcionalidades, escrito en Qt e semellante ao winamp e ao xmms</gl_ES>
   <gu>a winamp/xmms type feature-rich audio player written in Qt</gu>
   <he_IL>a winamp/xmms type feature-rich audio player written in Qt</he_IL>
   <hi>क्यूटी में निर्मित winamp/xmms समान विशेषताओं से युक्त ऑडियो प्लेयर</hi>
   <hr>a winamp/xmms type feature-rich audio player written in Qt</hr>
   <hu>a winamp/xmms type feature-rich audio player written in Qt</hu>
   <id>a winamp/xmms type feature-rich audio player written in Qt</id>
   <is>a winamp/xmms type feature-rich audio player written in Qt</is>
   <it>lettore audio, scritto in Qt, simile a winamp/xmms, ricco di funzionalità</it>
   <ja>Qt で書かれた winamp/xmms 風の機能豊富なオーディオプレイヤー</ja>
   <kk>a winamp/xmms type feature-rich audio player written in Qt</kk>
   <ko>a winamp/xmms type feature-rich audio player written in Qt</ko>
   <ku>a winamp/xmms type feature-rich audio player written in Qt</ku>
   <lt>a winamp/xmms type feature-rich audio player written in Qt</lt>
   <mk>a winamp/xmms type feature-rich audio player written in Qt</mk>
   <mr>a winamp/xmms type feature-rich audio player written in Qt</mr>
   <nb_NO>a winamp/xmms type feature-rich audio player written in Qt</nb_NO>
   <nb>winamp/xmms-lignende lydavspiller skrevet i Qt</nb>
   <nl_BE>a winamp/xmms type feature-rich audio player written in Qt</nl_BE>
   <nl>een veelzijdige winamp/xmms achtige audio speler geschreven in Qt</nl>
   <or>a winamp/xmms type feature-rich audio player written in Qt</or>
   <pl>funkcjonalny odtwarzacz audio typu Winamp/xmms napisany w Qt</pl>
   <pt_BR>Reprodutor de áudio repleto de funcionalidades, escrito em Qt e semelhante ao Winamp e ao XMMS</pt_BR>
   <pt>Reprodutor de rádio pleno de funcionalidades, escrito em Qt e semelhante ao winamp e ao xmms</pt>
   <ro>a winamp/xmms type feature-rich audio player written in Qt</ro>
   <ru>Многофункциональный аудиоплеер наподобие winamp/xmms</ru>
   <sk>a winamp/xmms type feature-rich audio player written in Qt</sk>
   <sl>Predvajalnik glasbe, ki je podoben winamp/xmms in ima veliko funkcij, pisan v Qt</sl>
   <so>a winamp/xmms type feature-rich audio player written in Qt</so>
   <sq>Një lojtës audio i pasur me veçori, në stil winamp/xmms, shkruar në Qt</sq>
   <sr>a winamp/xmms type feature-rich audio player written in Qt</sr>
   <sv>en winamp/xmms typ ljudspelare rik på möjligheter skriven i Qt</sv>
   <th>a winamp/xmms type feature-rich audio player written in Qt</th>
   <tr>Qt ile yazılmış zengin özelliklere sahip winamp/xmms türü bir ses çalar</tr>
   <uk>a winamp/xmms type feature-rich audio player written in Qt</uk>
   <vi>a winamp/xmms type feature-rich audio player written in Qt</vi>
   <zh_CN>a winamp/xmms type feature-rich audio player written in Qt</zh_CN>
   <zh_HK>a winamp/xmms type feature-rich audio player written in Qt</zh_HK>
   <zh_TW>a winamp/xmms type feature-rich audio player written in Qt</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
qmmp
qmmp-plugin-projectm
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
qmmp
qmmp-plugin-projectm
</uninstall_package_names>
</app>
