<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
File Managers
</category>

<name>
ROXFiler
</name>

<description>
   <am>a fast and powerful graphical file manager</am>
   <ar>a fast and powerful graphical file manager</ar>
   <be>a fast and powerful graphical file manager</be>
   <bg>a fast and powerful graphical file manager</bg>
   <bn>a fast and powerful graphical file manager</bn>
   <ca>Gestor de fitxers gràfic ràpid i potent</ca>
   <cs>a fast and powerful graphical file manager</cs>
   <da>en hurtig og kraftfuld grafisk filhåndtering</da>
   <de>Ein schneller und leistungsfähiger grafischer Dateimanager</de>
   <el>Γρήγορος και ισχυρός διαχειριστής αρχείων</el>
   <en>a fast and powerful graphical file manager</en>
   <es_ES>Gestor gráfico de archivos rápido y potente</es_ES>
   <es>gestor gráfico de archivos rápido y potente</es>
   <et>a fast and powerful graphical file manager</et>
   <eu>a fast and powerful graphical file manager</eu>
   <fa>a fast and powerful graphical file manager</fa>
   <fil_PH>a fast and powerful graphical file manager</fil_PH>
   <fi>nopea ja tehokas graafinen tiedostonhallinta</fi>
   <fr_BE>Un gestionnaire de fichiers rapide et puissant</fr_BE>
   <fr>Un gestionnaire de fichiers rapide et puissant</fr>
   <gl_ES>Xestor de ficheiros gráfico, rápido e poderoso</gl_ES>
   <gu>a fast and powerful graphical file manager</gu>
   <he_IL>a fast and powerful graphical file manager</he_IL>
   <hi>तीव्र व सशक्त ग्राफ़िकल फाइल प्रबंधक</hi>
   <hr>a fast and powerful graphical file manager</hr>
   <hu>a fast and powerful graphical file manager</hu>
   <id>a fast and powerful graphical file manager</id>
   <is>a fast and powerful graphical file manager</is>
   <it>semplice e veloce file manager grafico</it>
   <ja>高速で強力なグラフィカル・ファイルマネージャー</ja>
   <kk>a fast and powerful graphical file manager</kk>
   <ko>a fast and powerful graphical file manager</ko>
   <ku>a fast and powerful graphical file manager</ku>
   <lt>a fast and powerful graphical file manager</lt>
   <mk>a fast and powerful graphical file manager</mk>
   <mr>a fast and powerful graphical file manager</mr>
   <nb_NO>a fast and powerful graphical file manager</nb_NO>
   <nb>rask og kraftig filbehandler</nb>
   <nl_BE>a fast and powerful graphical file manager</nl_BE>
   <nl>een snelle en krachtige grafische bestandsmanager</nl>
   <or>a fast and powerful graphical file manager</or>
   <pl>szybki i potężny graficzny menedżer plików</pl>
   <pt_BR>Gerenciador de arquivos gráfico, rápido e poderoso</pt_BR>
   <pt>Gestor de ficheiros gráfico, rápido e poderoso</pt>
   <ro>a fast and powerful graphical file manager</ro>
   <ru>Быстрый и многофункциональный файловый менеджер</ru>
   <sk>a fast and powerful graphical file manager</sk>
   <sl>Hiter in zmogljiv grafični upravljalnik datotek</sl>
   <so>a fast and powerful graphical file manager</so>
   <sq>Një përgjegjës grafik kartelash, i shpejtë dhe i fuqishëm</sq>
   <sr>a fast and powerful graphical file manager</sr>
   <sv>en snabb och kraftfull grafisk filhanterare</sv>
   <th>โปรแกรมจัดการไฟล์แบบกราฟิกที่รวดเร็วและทรงพลัง</th>
   <tr>hızlı ve güçlü bir grafiksel dosya yöneticisi</tr>
   <uk>a fast and powerful graphical file manager</uk>
   <vi>a fast and powerful graphical file manager</vi>
   <zh_CN>a fast and powerful graphical file manager</zh_CN>
   <zh_HK>a fast and powerful graphical file manager</zh_HK>
   <zh_TW>a fast and powerful graphical file manager</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
rox-filer
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
rox-filer
</uninstall_package_names>
</app>
