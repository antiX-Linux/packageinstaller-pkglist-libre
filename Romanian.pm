<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Romanian
</name>

<description>
   <am>Romanian dictionary for hunspell</am>
   <ar>Romanian dictionary for hunspell</ar>
   <be>Romanian dictionary for hunspell</be>
   <bg>Romanian dictionary for hunspell</bg>
   <bn>Romanian dictionary for hunspell</bn>
   <ca>Diccionari Romanès per hunspell</ca>
   <cs>Romanian dictionary for hunspell</cs>
   <da>Romanian dictionary for hunspell</da>
   <de>Rumänisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό στα ρουμανικά για hunspell</el>
   <en>Romanian dictionary for hunspell</en>
   <es_ES>Diccionario Rumano para hunspell</es_ES>
   <es>Diccionario Rumano para hunspell</es>
   <et>Romanian dictionary for hunspell</et>
   <eu>Romanian dictionary for hunspell</eu>
   <fa>Romanian dictionary for hunspell</fa>
   <fil_PH>Romanian dictionary for hunspell</fil_PH>
   <fi>Romanian dictionary for hunspell</fi>
   <fr_BE>Roumain dictionnaire pour hunspell</fr_BE>
   <fr>Roumain dictionnaire pour hunspell</fr>
   <gl_ES>Romanian dictionary for hunspell</gl_ES>
   <gu>Romanian dictionary for hunspell</gu>
   <he_IL>Romanian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु रोमानियाई शब्दकोष</hi>
   <hr>Romanian dictionary for hunspell</hr>
   <hu>Romanian dictionary for hunspell</hu>
   <id>Romanian dictionary for hunspell</id>
   <is>Romanian dictionary for hunspell</is>
   <it>Dizionario rumeno per hunspell</it>
   <ja>Hunspell 用ルーマニア語辞書</ja>
   <kk>Romanian dictionary for hunspell</kk>
   <ko>Romanian dictionary for hunspell</ko>
   <ku>Romanian dictionary for hunspell</ku>
   <lt>Romanian dictionary for hunspell</lt>
   <mk>Romanian dictionary for hunspell</mk>
   <mr>Romanian dictionary for hunspell</mr>
   <nb_NO>Romanian dictionary for hunspell</nb_NO>
   <nb>Rumensk ordliste for hunspell</nb>
   <nl_BE>Romanian dictionary for hunspell</nl_BE>
   <nl>Romanian dictionary for hunspell</nl>
   <or>Romanian dictionary for hunspell</or>
   <pl>Romanian dictionary for hunspell</pl>
   <pt_BR>Dicionário Romeno para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Romeno para hunspell</pt>
   <ro>Romanian dictionary for hunspell</ro>
   <ru>Romanian dictionary for hunspell</ru>
   <sk>Romanian dictionary for hunspell</sk>
   <sl>Romunski slovar za hunspell</sl>
   <so>Romanian dictionary for hunspell</so>
   <sq>Fjalor rumanisht për hunspell</sq>
   <sr>Romanian dictionary for hunspell</sr>
   <sv>Rumänsk ordbok för hunspell</sv>
   <th>Romanian dictionary for hunspell</th>
   <tr>Hunspell için Rumence sözlük</tr>
   <uk>Romanian dictionary for hunspell</uk>
   <vi>Romanian dictionary for hunspell</vi>
   <zh_CN>Romanian dictionary for hunspell</zh_CN>
   <zh_HK>Romanian dictionary for hunspell</zh_HK>
   <zh_TW>Romanian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-ro
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ro
</uninstall_package_names>

</app>
