<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Frisian_Default_Firefox_esr
</name>

<description>
   <am>Western Frisian (Netherlands) localisation of Firefox ESR</am>
   <ar>Western Frisian (Netherlands) localisation of Firefox ESR</ar>
   <be>Western Frisian (Netherlands) localisation of Firefox ESR</be>
   <bg>Western Frisian (Netherlands) localisation of Firefox ESR</bg>
   <bn>Western Frisian (Netherlands) localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Frisó occidental (Holanda)</ca>
   <cs>Western Frisian (Netherlands) localisation of Firefox ESR</cs>
   <da>Western Frisian (Netherlands) localisation of Firefox ESR</da>
   <de>Niederländische Lokalisation (Westfriesisch) von “Firefox ESR”</de>
   <el>Δυτική Frisian (Ολλανδία) για Firefox ESR</el>
   <en>Western Frisian (Netherlands) localisation of Firefox ESR</en>
   <es_ES>Localización Frisio Occidental (Países Bajos) de Firefox ESR</es_ES>
   <es>Localización Frisio Occidental (Países Bajos) de Firefox ESR</es>
   <et>Western Frisian (Netherlands) localisation of Firefox ESR</et>
   <eu>Western Frisian (Netherlands) localisation of Firefox ESR</eu>
   <fa>Western Frisian (Netherlands) localisation of Firefox ESR</fa>
   <fil_PH>Western Frisian (Netherlands) localisation of Firefox ESR</fil_PH>
   <fi>Western Frisian (Netherlands) localisation of Firefox ESR</fi>
   <fr_BE>Localisation en frison occidental (Pays-Bas) pour Firefox ESR</fr_BE>
   <fr>Localisation en frison occidental (Pays-Bas) pour Firefox ESR</fr>
   <gl_ES>Western Frisian (Netherlands) localisation of Firefox ESR</gl_ES>
   <gu>Western Frisian (Netherlands) localisation of Firefox ESR</gu>
   <he_IL>Western Frisian (Netherlands) localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का पश्चिमी फ्रीसियाई (नीदरलैंड) संस्करण</hi>
   <hr>Western Frisian (Netherlands) localisation of Firefox ESR</hr>
   <hu>Western Frisian (Netherlands) localisation of Firefox ESR</hu>
   <id>Western Frisian (Netherlands) localisation of Firefox ESR</id>
   <is>Western Frisian (Netherlands) localisation of Firefox ESR</is>
   <it>Localizzazione in frisone occidentale (Paesi Bassi) di Firefox ESR</it>
   <ja>西フリジアン語（オランダ）版 Firefox ESR</ja>
   <kk>Western Frisian (Netherlands) localisation of Firefox ESR</kk>
   <ko>Western Frisian (Netherlands) localisation of Firefox ESR</ko>
   <ku>Western Frisian (Netherlands) localisation of Firefox ESR</ku>
   <lt>Western Frisian (Netherlands) localisation of Firefox ESR</lt>
   <mk>Western Frisian (Netherlands) localisation of Firefox ESR</mk>
   <mr>Western Frisian (Netherlands) localisation of Firefox ESR</mr>
   <nb_NO>Western Frisian (Netherlands) localisation of Firefox ESR</nb_NO>
   <nb>Vestfrisisk (nederlandsk) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Western Frisian (Netherlands) localisation of Firefox ESR</nl_BE>
   <nl>West-Friese (Nederland) lokalisatie van Firefox ESR</nl>
   <or>Western Frisian (Netherlands) localisation of Firefox ESR</or>
   <pl>Western Frisian (Netherlands) localisation of Firefox ESR</pl>
   <pt_BR>Frísia Ocidental (Holanda) Localização para o Firefox ESR</pt_BR>
   <pt>Frísio Ocidental (Países Baixos) Localização para Firefox ESR</pt>
   <ro>Western Frisian (Netherlands) localisation of Firefox ESR</ro>
   <ru>Western Frisian (Netherlands) localisation of Firefox ESR</ru>
   <sk>Western Frisian (Netherlands) localisation of Firefox ESR</sk>
   <sl>Zahodno Frizijske (Nizozemska) krajevne nastavitve za Firefox ESR</sl>
   <so>Western Frisian (Netherlands) localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në frizishte perëndimore (Holandë)</sq>
   <sr>Western Frisian (Netherlands) localisation of Firefox ESR</sr>
   <sv>Västfrisisk (Holland) lokalisering av Firefox ESR</sv>
   <th>Western Frisian (Netherlands) localisation of Firefox ESR</th>
   <tr>Firefox ESR Batı Frizye Dili (Hollanda) yerelleştirmesi</tr>
   <uk>Western Frisian (Netherlands) localisation of Firefox ESR</uk>
   <vi>Western Frisian (Netherlands) localisation of Firefox ESR</vi>
   <zh_CN>Western Frisian (Netherlands) localisation of Firefox ESR</zh_CN>
   <zh_HK>Western Frisian (Netherlands) localisation of Firefox ESR</zh_HK>
   <zh_TW>Western Frisian (Netherlands) localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-fy-nl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-fy-nl
</uninstall_package_names>

</app>
