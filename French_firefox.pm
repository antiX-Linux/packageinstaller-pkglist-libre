<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
French_Firefox
</name>

<description>
   <am>French localisation of Firefox</am>
   <ar>French localisation of Firefox</ar>
   <be>French localisation of Firefox</be>
   <bg>French localisation of Firefox</bg>
   <bn>French localisation of Firefox</bn>
   <ca>Localització de Firefox en Francès</ca>
   <cs>French localisation of Firefox</cs>
   <da>Fransk oversættelse af Firefox</da>
   <de>Französische Lokalisierung von Firefox</de>
   <el>Γαλλικά για το Firefox</el>
   <en>French localisation of Firefox</en>
   <es_ES>Localización francesa de Firefox</es_ES>
   <es>Localización Francés de Firefox</es>
   <et>French localisation of Firefox</et>
   <eu>French localisation of Firefox</eu>
   <fa>French localisation of Firefox</fa>
   <fil_PH>French localisation of Firefox</fil_PH>
   <fi>Ranskalainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en français pour Firefox</fr_BE>
   <fr>Localisation en français pour Firefox</fr>
   <gl_ES>Localización de Firefox ao francés</gl_ES>
   <gu>French localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לצרפתית</he_IL>
   <hi>फायरफॉक्स का फ्रेंच संस्करण</hi>
   <hr>French localisation of Firefox</hr>
   <hu>French localisation of Firefox</hu>
   <id>French localisation of Firefox</id>
   <is>French localisation of Firefox</is>
   <it>Localizzazione francese di Firefox</it>
   <ja>Firefox のフランス語版</ja>
   <kk>French localisation of Firefox</kk>
   <ko>French localisation of Firefox</ko>
   <ku>French localisation of Firefox</ku>
   <lt>French localisation of Firefox</lt>
   <mk>French localisation of Firefox</mk>
   <mr>French localisation of Firefox</mr>
   <nb_NO>French localisation of Firefox</nb_NO>
   <nb>Fransk lokaltilpassing av Firefox</nb>
   <nl_BE>French localisation of Firefox</nl_BE>
   <nl>Franse lokalisatie van Firefox</nl>
   <or>French localisation of Firefox</or>
   <pl>Francuska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Francês Localização para o Firefox</pt_BR>
   <pt>Francês Localização para Firefox</pt>
   <ro>French localisation of Firefox</ro>
   <ru>Французская локализация Firefox</ru>
   <sk>French localisation of Firefox</sk>
   <sl>Francoske krajevne nastavitve za Firefox</sl>
   <so>French localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në frëngjisht</sq>
   <sr>French localisation of Firefox</sr>
   <sv>Fransk lokalisering av Firefox</sv>
   <th>French localisation ของ Firefox</th>
   <tr>Firefox'un Fransızca yerelleştirmesi</tr>
   <uk>French локалізація Firefox</uk>
   <vi>French localisation of Firefox</vi>
   <zh_CN>Firefox 法语语言包</zh_CN>
   <zh_HK>French localisation of Firefox</zh_HK>
   <zh_TW>French localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-fr
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-fr
</uninstall_package_names>
</app>
