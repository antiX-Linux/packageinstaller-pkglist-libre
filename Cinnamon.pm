<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
Cinnamon-Standard
</name>

<description>
   <am>Cinnamon desktop - standard install</am>
   <ar>Cinnamon desktop - standard install</ar>
   <be>Cinnamon desktop - standard install</be>
   <bg>Cinnamon desktop - standard install</bg>
   <bn>Cinnamon desktop - standard install</bn>
   <ca>Escriptori Cinnamon - instal·lació estàndard</ca>
   <cs>Cinnamon desktop - standard install</cs>
   <da>Cinnamon desktop - standard install</da>
   <de>“Cinnamon” Desktop-Umgebung, Standardinstallation</de>
   <el>Επιτραπέζιος υπολογιστής Cinnamon - τυπική εγκατάσταση</el>
   <en>Cinnamon desktop - standard install</en>
   <es_ES>Escritorio Cinnamon - instalación estándar</es_ES>
   <es>Escritorio Cinnamon - instalación estándar</es>
   <et>Cinnamon desktop - standard install</et>
   <eu>Cinnamon desktop - standard install</eu>
   <fa>Cinnamon desktop - standard install</fa>
   <fil_PH>Cinnamon desktop - standard install</fil_PH>
   <fi>Cinnamon desktop - standard install</fi>
   <fr_BE>Bureau Cinnamon - installation standard</fr_BE>
   <fr>Bureau Cinnamon - installation standard</fr>
   <gl_ES>Cinnamon desktop - standard install</gl_ES>
   <gu>Cinnamon desktop - standard install</gu>
   <he_IL>Cinnamon desktop - standard install</he_IL>
   <hi>सिनेमन डेस्कटॉप - सामान्य इंस्टॉल</hi>
   <hr>Cinnamon desktop - standard install</hr>
   <hu>Cinnamon desktop - standard install</hu>
   <id>Cinnamon desktop - standard install</id>
   <is>Cinnamon desktop - standard install</is>
   <it>Cinnamon desktop - installazione standard</it>
   <ja>Cinnamonデスクトップの標準インストール</ja>
   <kk>Cinnamon desktop - standard install</kk>
   <ko>Cinnamon desktop - standard install</ko>
   <ku>Cinnamon desktop - standard install</ku>
   <lt>Cinnamon desktop - standard install</lt>
   <mk>Cinnamon desktop - standard install</mk>
   <mr>Cinnamon desktop - standard install</mr>
   <nb_NO>Cinnamon desktop - standard install</nb_NO>
   <nb>Skrivebordet Cinnamon – standard installasjon</nb>
   <nl_BE>Cinnamon desktop - standard install</nl_BE>
   <nl>Cinnamon desktop - standaard installatie</nl>
   <or>Cinnamon desktop - standard install</or>
   <pl>Cinnamon desktop - standard install</pl>
   <pt_BR>Área de Trabalho Cinnamon/Canela - instalação padrão</pt_BR>
   <pt>Ambiente de trabalho Cinnamon - instalação padrão</pt>
   <ro>Cinnamon desktop - standard install</ro>
   <ru>Cinnamon desktop - standard install</ru>
   <sk>Cinnamon desktop - standard install</sk>
   <sl>Cinnamon namizje - standardna namestitev</sl>
   <so>Cinnamon desktop - standard install</so>
   <sq>Desktopi Cinnamon - instalim standard</sq>
   <sr>Cinnamon desktop - standard install</sr>
   <sv>Cinnamon skrivbordsmiljö - standard installation</sv>
   <th>Cinnamon desktop - standard install</th>
   <tr>Cinnamon masaüstü - standart kurulum</tr>
   <uk>Cinnamon desktop - standard install</uk>
   <vi>Cinnamon desktop - standard install</vi>
   <zh_CN>Cinnamon desktop - standard install</zh_CN>
   <zh_HK>Cinnamon desktop - standard install</zh_HK>
   <zh_TW>Cinnamon desktop - standard install</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
cinnamon
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
cinnamon
</uninstall_package_names>
</app>
