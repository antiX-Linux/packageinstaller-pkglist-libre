<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Icelandic
</name>

<description>
   <am>Icelandic dictionary for hunspell</am>
   <ar>Icelandic dictionary for hunspell</ar>
   <be>Icelandic dictionary for hunspell</be>
   <bg>Icelandic dictionary for hunspell</bg>
   <bn>Icelandic dictionary for hunspell</bn>
   <ca>Diccionari islandès per hunspell</ca>
   <cs>Icelandic dictionary for hunspell</cs>
   <da>Icelandic dictionary for hunspell</da>
   <de>Isländisches Wörterbuch für “Hunspell”</de>
   <el>Ισλανδικό λεξικό για το hunspell</el>
   <en>Icelandic dictionary for hunspell</en>
   <es_ES>Diccionario Islandés para hunspell</es_ES>
   <es>Diccionario Islandés para hunspell</es>
   <et>Icelandic dictionary for hunspell</et>
   <eu>Icelandic dictionary for hunspell</eu>
   <fa>Icelandic dictionary for hunspell</fa>
   <fil_PH>Icelandic dictionary for hunspell</fil_PH>
   <fi>Icelandic dictionary for hunspell</fi>
   <fr_BE>Islandais dictionnaire for hunspell</fr_BE>
   <fr>Islandais dictionnaire for hunspell</fr>
   <gl_ES>Icelandic dictionary for hunspell</gl_ES>
   <gu>Icelandic dictionary for hunspell</gu>
   <he_IL>Icelandic dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु आइसलैंडिक शब्दकोष</hi>
   <hr>Icelandic dictionary for hunspell</hr>
   <hu>Icelandic dictionary for hunspell</hu>
   <id>Icelandic dictionary for hunspell</id>
   <is>Icelandic dictionary for hunspell</is>
   <it>Dizionario islandese per hunspell</it>
   <ja>Hunspell 用アイスランド語辞書</ja>
   <kk>Icelandic dictionary for hunspell</kk>
   <ko>Icelandic dictionary for hunspell</ko>
   <ku>Icelandic dictionary for hunspell</ku>
   <lt>Icelandic dictionary for hunspell</lt>
   <mk>Icelandic dictionary for hunspell</mk>
   <mr>Icelandic dictionary for hunspell</mr>
   <nb_NO>Icelandic dictionary for hunspell</nb_NO>
   <nb>Islandsk ordliste for hunspell</nb>
   <nl_BE>Icelandic dictionary for hunspell</nl_BE>
   <nl>Icelandic dictionary for hunspell</nl>
   <or>Icelandic dictionary for hunspell</or>
   <pl>Icelandic dictionary for hunspell</pl>
   <pt_BR>Dicionário Islandês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Islandês para hunspell</pt>
   <ro>Icelandic dictionary for hunspell</ro>
   <ru>Icelandic dictionary for hunspell</ru>
   <sk>Icelandic dictionary for hunspell</sk>
   <sl>Islandski slovar za hunspell</sl>
   <so>Icelandic dictionary for hunspell</so>
   <sq>Fjalor islandisht për hunspell</sq>
   <sr>Icelandic dictionary for hunspell</sr>
   <sv>Isländsk ordbok för hunspell</sv>
   <th>Icelandic dictionary for hunspell</th>
   <tr>Hunspell için İzlandaca sözlük</tr>
   <uk>Icelandic dictionary for hunspell</uk>
   <vi>Icelandic dictionary for hunspell</vi>
   <zh_CN>Icelandic dictionary for hunspell</zh_CN>
   <zh_HK>Icelandic dictionary for hunspell</zh_HK>
   <zh_TW>Icelandic dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-is
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-is
</uninstall_package_names>

</app>
