<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
Blueman
</name>

<description>
   <am>a GTK+ bluetooth management utility</am>
   <ar>a GTK+ bluetooth management utility</ar>
   <be>a GTK+ bluetooth management utility</be>
   <bg>a GTK+ bluetooth management utility</bg>
   <bn>a GTK+ bluetooth management utility</bn>
   <ca>Eina de gestió Bluetooth GTK+</ca>
   <cs>a GTK+ bluetooth management utility</cs>
   <da>et GTK+ bluetooth-håndteringsredskab</da>
   <de>Ein GTK+ Bluetooth-Management-Dienstprogramm</de>
   <el>Βοηθητικό πρόγραμμα διαχείρισης bluetooth</el>
   <en>a GTK+ bluetooth management utility</en>
   <es_ES>Utilidad GTK+ para administrar Bluetooth</es_ES>
   <es>Utilidad GTK+ para administrar Bluetooth</es>
   <et>a GTK+ bluetooth management utility</et>
   <eu>a GTK+ bluetooth management utility</eu>
   <fa>a GTK+ bluetooth management utility</fa>
   <fil_PH>a GTK+ bluetooth management utility</fil_PH>
   <fi>GTK+ bluetooth:in hallintatyökalu</fi>
   <fr_BE>Un utilitaire GTK+ de gestion bluetooth</fr_BE>
   <fr>Un utilitaire GTK+ de gestion bluetooth</fr>
   <gl_ES>Usuario de xestión de bluetooth para uso en GTK+</gl_ES>
   <gu>a GTK+ bluetooth management utility</gu>
   <he_IL>a GTK+ bluetooth management utility</he_IL>
   <hi>जीटीके+ ब्लूटूथ प्रबंधन साधन</hi>
   <hr>a GTK+ bluetooth management utility</hr>
   <hu>a GTK+ bluetooth management utility</hu>
   <id>a GTK+ bluetooth management utility</id>
   <is>a GTK+ bluetooth management utility</is>
   <it>Utility GTK+ di gestione del bluethooth</it>
   <ja>GTK+ bluetooth 管理ユーティリティ</ja>
   <kk>a GTK+ bluetooth management utility</kk>
   <ko>a GTK+ bluetooth management utility</ko>
   <ku>a GTK+ bluetooth management utility</ku>
   <lt>a GTK+ bluetooth management utility</lt>
   <mk>a GTK+ bluetooth management utility</mk>
   <mr>a GTK+ bluetooth management utility</mr>
   <nb_NO>a GTK+ bluetooth management utility</nb_NO>
   <nb>behandling av blåtannstilkoblinger (GTK+)</nb>
   <nl_BE>a GTK+ bluetooth management utility</nl_BE>
   <nl>een GTK+ bluetooth beheer utility</nl>
   <or>a GTK+ bluetooth management utility</or>
   <pl>narzędzie GTK+ do zarządzania bluetooth</pl>
   <pt_BR>Utilitário de gerenciamento de bluetooth para uso em GTK+</pt_BR>
   <pt>Utilitário de gestão de bluetooth para uso em GTK+</pt>
   <ro>a GTK+ bluetooth management utility</ro>
   <ru>GTK+ утилита управления bluetooth</ru>
   <sk>a GTK+ bluetooth management utility</sk>
   <sl>GTK+ orodje za upravljanje bluetooth povezav</sl>
   <so>a GTK+ bluetooth management utility</so>
   <sq>Një mjet GTK+ administrimi bluetooth-i</sq>
   <sr>a GTK+ bluetooth management utility</sr>
   <sv>ett GTK+ bluetooth hanteringsverktyg</sv>
   <th>โปรแกรมจัดการบลูทูธ GTK+</th>
   <tr>bir GTK+ bluetooth yönetim aracı</tr>
   <uk>GTK+ утиліта для bluetooth</uk>
   <vi>một ứng dụng quản lí bluetooth GTK+</vi>
   <zh_CN>GTK+ 蓝牙管理实用工具</zh_CN>
   <zh_HK>a GTK+ bluetooth management utility</zh_HK>
   <zh_TW>GTK +藍牙管理實用程序</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
blueman
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
blueman
</uninstall_package_names>
</app>
