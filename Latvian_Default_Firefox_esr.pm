<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Latvian_Default_Firefox_esr
</name>

<description>
   <am>Latvian localisation of Firefox ESR</am>
   <ar>Latvian localisation of Firefox ESR</ar>
   <be>Latvian localisation of Firefox ESR</be>
   <bg>Latvian localisation of Firefox ESR</bg>
   <bn>Latvian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Letó</ca>
   <cs>Latvian localisation of Firefox ESR</cs>
   <da>Latvian localisation of Firefox ESR</da>
   <de>Lettische Lokalisation von “Firefox ESR”</de>
   <el>Λετονικά για Firefox ESR</el>
   <en>Latvian localisation of Firefox ESR</en>
   <es_ES>Localización Letona de Firefox ESR</es_ES>
   <es>Localización Letón de Firefox ESR</es>
   <et>Latvian localisation of Firefox ESR</et>
   <eu>Latvian localisation of Firefox ESR</eu>
   <fa>Latvian localisation of Firefox ESR</fa>
   <fil_PH>Latvian localisation of Firefox ESR</fil_PH>
   <fi>Latvian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en letton pour Firefox ESR</fr_BE>
   <fr>Localisation en letton pour Firefox ESR</fr>
   <gl_ES>Latvian localisation of Firefox ESR</gl_ES>
   <gu>Latvian localisation of Firefox ESR</gu>
   <he_IL>Latvian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का लात्वियाई संस्करण</hi>
   <hr>Latvian localisation of Firefox ESR</hr>
   <hu>Latvian localisation of Firefox ESR</hu>
   <id>Latvian localisation of Firefox ESR</id>
   <is>Latvian localisation of Firefox ESR</is>
   <it>Localizzazione lettone di Firefox ESR</it>
   <ja>ラトビア語版 Firefox ESR</ja>
   <kk>Latvian localisation of Firefox ESR</kk>
   <ko>Latvian localisation of Firefox ESR</ko>
   <ku>Latvian localisation of Firefox ESR</ku>
   <lt>Latvian localisation of Firefox ESR</lt>
   <mk>Latvian localisation of Firefox ESR</mk>
   <mr>Latvian localisation of Firefox ESR</mr>
   <nb_NO>Latvian localisation of Firefox ESR</nb_NO>
   <nb>Latvisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Latvian localisation of Firefox ESR</nl_BE>
   <nl>Latvian localisation of Firefox ESR</nl>
   <or>Latvian localisation of Firefox ESR</or>
   <pl>Latvian localisation of Firefox ESR</pl>
   <pt_BR>Letão Localização para o Firefox ESR</pt_BR>
   <pt>Letão Localização para Firefox ESR</pt>
   <ro>Latvian localisation of Firefox ESR</ro>
   <ru>Latvian localisation of Firefox ESR</ru>
   <sk>Latvian localisation of Firefox ESR</sk>
   <sl>Latvijske krajevne nastavitve za Firefox ESR</sl>
   <so>Latvian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në letonisht</sq>
   <sr>Latvian localisation of Firefox ESR</sr>
   <sv>Lettländsk lokalisering av Firefox ESR</sv>
   <th>Latvian localisation of Firefox ESR</th>
   <tr>Firefox ESR Letonca yerelleştirmesi</tr>
   <uk>Latvian localisation of Firefox ESR</uk>
   <vi>Latvian localisation of Firefox ESR</vi>
   <zh_CN>Latvian localisation of Firefox ESR</zh_CN>
   <zh_HK>Latvian localisation of Firefox ESR</zh_HK>
   <zh_TW>Latvian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-lv
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-lv
</uninstall_package_names>

</app>
