<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Utility
</category>

<name>
Apt Notifer
</name>

<description>
   <am>Applet that indicates when updates are available</am>
   <ar>Applet that indicates when updates are available</ar>
   <be>Applet that indicates when updates are available</be>
   <bg>Applet that indicates when updates are available</bg>
   <bn>Applet that indicates when updates are available</bn>
   <ca>Miniaplicació que indica quan hi ha actualitzacions</ca>
   <cs>Applet that indicates when updates are available</cs>
   <da>Applet that indicates when updates are available</da>
   <de>Programm zur Benachrichtigung über verfügbare Aktualisierungen</de>
   <el>Applet που υποδεικνύει πότε είναι διαθέσιμες ενημερώσεις</el>
   <en>Applet that indicates when updates are available</en>
   <es_ES>Applet que indica cuándo hay actualizaciones disponibles</es_ES>
   <es>Applet que avisa cuando hay actualizaciones disponibles</es>
   <et>Applet that indicates when updates are available</et>
   <eu>Applet that indicates when updates are available</eu>
   <fa>Applet that indicates when updates are available</fa>
   <fil_PH>Applet that indicates when updates are available</fil_PH>
   <fi>Sovelma joka ilmoittaa kun päivityksiä on saatavilla</fi>
   <fr_BE>Applet qui indique quand les mises à jour sont disponibles</fr_BE>
   <fr>Applet qui indique quand les mises à jour sont disponibles</fr>
   <gl_ES>Applet that indicates when updates are available</gl_ES>
   <gu>Applet that indicates when updates are available</gu>
   <he_IL>Applet that indicates when updates are available</he_IL>
   <hi>अपडेट उपलब्धता सूचक एप्लेट</hi>
   <hr>Applet that indicates when updates are available</hr>
   <hu>Applet that indicates when updates are available</hu>
   <id>Applet that indicates when updates are available</id>
   <is>Applet that indicates when updates are available</is>
   <it>Applet che indica quando gli aggiornamenti sono disponibili</it>
   <ja>アップデートが可能になったことを知らせるアプレット</ja>
   <kk>Applet that indicates when updates are available</kk>
   <ko>Applet that indicates when updates are available</ko>
   <ku>Applet that indicates when updates are available</ku>
   <lt>Applet that indicates when updates are available</lt>
   <mk>Applet that indicates when updates are available</mk>
   <mr>Applet that indicates when updates are available</mr>
   <nb_NO>Applet that indicates when updates are available</nb_NO>
   <nb>Programtillegg som angir om oppdateringer er tilgjengelige</nb>
   <nl_BE>Applet that indicates when updates are available</nl_BE>
   <nl>Applet die aangeeft wanneer updates beschikbaar zijn</nl>
   <or>Applet that indicates when updates are available</or>
   <pl>Applet that indicates when updates are available</pl>
   <pt_BR>Applet que indica quando as atualizações estão disponíveis</pt_BR>
   <pt>Indicador da existência de actualizações</pt>
   <ro>Applet that indicates when updates are available</ro>
   <ru>Applet that indicates when updates are available</ru>
   <sk>Applet that indicates when updates are available</sk>
   <sl>Aplet, ki prikaže, da so na voljo posodobitve</sl>
   <so>Applet that indicates when updates are available</so>
   <sq>Aplikacionth që tregon kur ka përditësime</sq>
   <sr>Applet that indicates when updates are available</sr>
   <sv>Litet program som visar när det finns nya uppdateringar</sv>
   <th>Applet that indicates when updates are available</th>
   <tr>Güncelleme olduğunda gösteren uygulama</tr>
   <uk>Applet that indicates when updates are available</uk>
   <vi>Applet that indicates when updates are available</vi>
   <zh_CN>Applet that indicates when updates are available</zh_CN>
   <zh_HK>Applet that indicates when updates are available</zh_HK>
   <zh_TW>Applet that indicates when updates are available</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
apt-notifier
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
apt-notifier
</uninstall_package_names>
</app>
