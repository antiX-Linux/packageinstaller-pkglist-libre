<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bulgarian_LO_Latest_full
</name>

<description>
   <am>Bulgarian Language Meta-Package for LibreOffice</am>
   <ar>Bulgarian Language Meta-Package for LibreOffice</ar>
   <be>Bulgarian Language Meta-Package for LibreOffice</be>
   <bg>Bulgarian Language Meta-Package for LibreOffice</bg>
   <bn>Bulgarian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Búlgar per LibreOffice</ca>
   <cs>Bulgarian Language Meta-Package for LibreOffice</cs>
   <da>Bulgarian Language Meta-Package for LibreOffice</da>
   <de>Bulgarisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Βουλγαρικά</el>
   <en>Bulgarian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Búlgaro para LibreOffice</es_ES>
   <es>Metapaquete de idioma Búlgaro para LibreOffice</es>
   <et>Bulgarian Language Meta-Package for LibreOffice</et>
   <eu>Bulgarian Language Meta-Package for LibreOffice</eu>
   <fa>Bulgarian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Bulgarian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Bulgarialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de langue bulgare pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de langue bulgare pour LibreOffice</fr>
   <gl_ES>Búlgaro Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Bulgarian Language Meta-Package for LibreOffice</gu>
   <he_IL>Bulgarian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु बल्गेरियाई भाषा मेटा-पैकेज</hi>
   <hr>Bulgarian Language Meta-Package for LibreOffice</hr>
   <hu>Bulgarian Language Meta-Package for LibreOffice</hu>
   <id>Bulgarian Language Meta-Package for LibreOffice</id>
   <is>Bulgarian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua bulgara per LibreOffice</it>
   <ja>LibreOffice 用ブルガリア語メタパッケージ</ja>
   <kk>Bulgarian Language Meta-Package for LibreOffice</kk>
   <ko>Bulgarian Language Meta-Package for LibreOffice</ko>
   <ku>Bulgarian Language Meta-Package for LibreOffice</ku>
   <lt>Bulgarian Language Meta-Package for LibreOffice</lt>
   <mk>Bulgarian Language Meta-Package for LibreOffice</mk>
   <mr>Bulgarian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Bulgarian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Bulgarsk språkpakke for LibreOffice</nb>
   <nl_BE>Bulgarian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Bulgaars Taal Meta-Pakket voor LibreOffice</nl>
   <or>Bulgarian Language Meta-Package for LibreOffice</or>
   <pl>Bułgarski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Búlgaro Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Búlgaro Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Bulgarian Language Meta-Package for LibreOffice</ro>
   <ru>Bulgarian Language Meta-Package for LibreOffice</ru>
   <sk>Bulgarian Language Meta-Package for LibreOffice</sk>
   <sl>Bolgarski jezikovni meta-paket za LibreOffice</sl>
   <so>Bulgarian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në bullgarisht</sq>
   <sr>Bulgarian Language Meta-Package for LibreOffice</sr>
   <sv>Bulgariskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Bulgarian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Bulgarca Dili Üst-Paketi</tr>
   <uk>Bulgarian Language Meta-Package for LibreOffice</uk>
   <vi>Bulgarian Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 保加利亚语语言包</zh_CN>
   <zh_HK>Bulgarian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Bulgarian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-bg
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-bg
</uninstall_package_names>

</app>
