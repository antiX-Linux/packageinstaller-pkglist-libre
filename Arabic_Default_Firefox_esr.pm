<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Arabic_Default_Firefox_esr
</name>

<description>
   <am>Arabic localisation of Firefox-ESR</am>
   <ar>Arabic localisation of Firefox-ESR</ar>
   <be>Arabic localisation of Firefox-ESR</be>
   <bg>Arabic localisation of Firefox-ESR</bg>
   <bn>Arabic localisation of Firefox-ESR</bn>
   <ca>Localització de Firefox-ESR en Àrab</ca>
   <cs>Arabic localisation of Firefox-ESR</cs>
   <da>Arabic localisation of Firefox-ESR</da>
   <de>Arabische Lokalisation des als Standard installierten “Firefox ESR”</de>
   <el>Αραβικά για το Firefox-ESR</el>
   <en>Arabic localisation of Firefox-ESR</en>
   <es_ES>Localización Arabe de Firefox-ESR</es_ES>
   <es>Localización Árabe de Firefox ESR</es>
   <et>Arabic localisation of Firefox-ESR</et>
   <eu>Arabic localisation of Firefox-ESR</eu>
   <fa>Arabic localisation of Firefox-ESR</fa>
   <fil_PH>Arabic localisation of Firefox-ESR</fil_PH>
   <fi>Arabic localisation of Firefox-ESR</fi>
   <fr_BE>Localisation en arabe pour Firefox ESR</fr_BE>
   <fr>Localisation en arabe pour Firefox ESR</fr>
   <gl_ES>Arabic localisation of Firefox-ESR</gl_ES>
   <gu>Arabic localisation of Firefox-ESR</gu>
   <he_IL>Arabic localisation of Firefox-ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का अरबी संस्करण</hi>
   <hr>Arabic localisation of Firefox-ESR</hr>
   <hu>Arabic localisation of Firefox-ESR</hu>
   <id>Arabic localisation of Firefox-ESR</id>
   <is>Arabic localisation of Firefox-ESR</is>
   <it>Localizzazione araba di Firefox ESR</it>
   <ja>アラビア語版 Firefox-ESR</ja>
   <kk>Arabic localisation of Firefox-ESR</kk>
   <ko>Arabic localisation of Firefox-ESR</ko>
   <ku>Arabic localisation of Firefox-ESR</ku>
   <lt>Arabic localisation of Firefox-ESR</lt>
   <mk>Arabic localisation of Firefox-ESR</mk>
   <mr>Arabic localisation of Firefox-ESR</mr>
   <nb_NO>Arabic localisation of Firefox-ESR</nb_NO>
   <nb>Arabisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Arabic localisation of Firefox-ESR</nl_BE>
   <nl>Arabische lokalisatie van Firefox-ESR</nl>
   <or>Arabic localisation of Firefox-ESR</or>
   <pl>Arabic localisation of Firefox-ESR</pl>
   <pt_BR>Árabe Localização para o Firefox ESR</pt_BR>
   <pt>Árabe Localização para Firefox ESR</pt>
   <ro>Arabic localisation of Firefox-ESR</ro>
   <ru>Arabic localisation of Firefox-ESR</ru>
   <sk>Arabic localisation of Firefox-ESR</sk>
   <sl>Arabske krajevne nastavitve za Firefox-ESR</sl>
   <so>Arabic localisation of Firefox-ESR</so>
   <sq>Përkthimi i Firefox-it ESR në arabisht</sq>
   <sr>Arabic localisation of Firefox-ESR</sr>
   <sv>Arabisk lokalisering av  Firefox-ESR</sv>
   <th>Arabic localisation of Firefox-ESR</th>
   <tr>Firefox-ESR Arapça yerelleştirmesi</tr>
   <uk>Arabic localisation of Firefox-ESR</uk>
   <vi>Arabic localisation of Firefox-ESR</vi>
   <zh_CN>Arabic localisation of Firefox-ESR</zh_CN>
   <zh_HK>Arabic localisation of Firefox-ESR</zh_HK>
   <zh_TW>Arabic localisation of Firefox-ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ar
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ar
</uninstall_package_names>

</app>
