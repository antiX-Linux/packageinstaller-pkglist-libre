<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Albanian_Thunderbird
</name>

<description>
   <am>Albanian localisation of Thunderbird</am>
   <ar>Albanian localisation of Thunderbird</ar>
   <be>Albanian localisation of Thunderbird</be>
   <bg>Albanian localisation of Thunderbird</bg>
   <bn>Albanian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Albanès</ca>
   <cs>Albanian localisation of Thunderbird</cs>
   <da>Albansk oversættelse af Thunderbird</da>
   <de>Albanische Lokalisierung von Thunderbird</de>
   <el>Αλβανικά για το Thunderbird</el>
   <en>Albanian localisation of Thunderbird</en>
   <es_ES>Localización Albanes de Thunderbird</es_ES>
   <es>Localización Albanés de Thunderbird</es>
   <et>Albanian localisation of Thunderbird</et>
   <eu>Albanian localisation of Thunderbird</eu>
   <fa>Albanian localisation of Thunderbird</fa>
   <fil_PH>Albanian localisation of Thunderbird</fil_PH>
   <fi>Albanialainen Thunderbird-kielipaketti</fi>
   <fr_BE>Localisation en albanais pour Thunderbird</fr_BE>
   <fr>Localisation en albanais pour Thunderbird</fr>
   <gl_ES>Localización en albanés de Thunderbird</gl_ES>
   <gu>Albanian localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לאלבנית</he_IL>
   <hi>थंडरबर्ड का अल्बानियाई संस्करण</hi>
   <hr>Albanska lokalizacija Thunderbirda</hr>
   <hu>Albanian localisation of Thunderbird</hu>
   <id>Albanian localisation of Thunderbird</id>
   <is>Albanian localisation of Thunderbird</is>
   <it>Localizzazione albanese di Thunderbird</it>
   <ja>アルバニア語版 Thunderbird</ja>
   <kk>Albanian localisation of Thunderbird</kk>
   <ko>Albanian localisation of Thunderbird</ko>
   <ku>Albanian localisation of Thunderbird</ku>
   <lt>Albanian localisation of Thunderbird</lt>
   <mk>Albanian localisation of Thunderbird</mk>
   <mr>Albanian localisation of Thunderbird</mr>
   <nb_NO>Albanian localisation of Thunderbird</nb_NO>
   <nb>Albansk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Albanian localisation of Thunderbird</nl_BE>
   <nl>Albanese lokalisatie van Thunderbird</nl>
   <or>Albanian localisation of Thunderbird</or>
   <pl>Albańska lokalizacja Thunderbirda</pl>
   <pt_BR>Albanês Localização para o Thunderbird</pt_BR>
   <pt>Albanês Localização para Thunderbird</pt>
   <ro>Albanian localisation of Thunderbird</ro>
   <ru>Albanian localisation of Thunderbird</ru>
   <sk>Albanian lokalizácia pre Thunderbird</sk>
   <sl>Albanske krajevne nastavitve za Thunderbird</sl>
   <so>Albanian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në shqip</sq>
   <sr>Albanian localisation of Thunderbird</sr>
   <sv>Albansk lokalisering av Thunderbird</sv>
   <th>Albanian localisation ของ Thunderbird</th>
   <tr>Thunderbird Arnavutça yerelleştirmesi</tr>
   <uk>Albanian локалізація Thunderbird</uk>
   <vi>Albanian localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 阿尔巴尼亚语语言包</zh_CN>
   <zh_HK>Albanian localisation of Thunderbird</zh_HK>
   <zh_TW>Albanian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-sq
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-sq
</uninstall_package_names>

</app>
