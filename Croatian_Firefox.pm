<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Croatian_Firefox
</name>

<description>
   <am>Croatian localisation of Firefox</am>
   <ar>Croatian localisation of Firefox</ar>
   <be>Croatian localisation of Firefox</be>
   <bg>Croatian localisation of Firefox</bg>
   <bn>Croatian localisation of Firefox</bn>
   <ca>Localització de Firefox en Croata</ca>
   <cs>Croatian localisation of Firefox</cs>
   <da>Kroatisk oversættelse af Firefox</da>
   <de>Kroatische Lokalisierung von Firefox</de>
   <el>Κροατικά για το Firefox</el>
   <en>Croatian localisation of Firefox</en>
   <es_ES>Localización Croata de Firefox</es_ES>
   <es>Localización Croata de Firefox</es>
   <et>Croatian localisation of Firefox</et>
   <eu>Croatian localisation of Firefox</eu>
   <fa>Croatian localisation of Firefox</fa>
   <fil_PH>Croatian localisation of Firefox</fil_PH>
   <fi>Kroatialainen Firefox-kotoistus</fi>
   <fr_BE>Localisation croate pour Firefox</fr_BE>
   <fr>Localisation croate pour Firefox</fr>
   <gl_ES>Croata localización para Firefox</gl_ES>
   <gu>Croatian localisation of Firefox</gu>
   <he_IL>Croatian localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का क्रोएशियाई संस्करण</hi>
   <hr>Croatian localisation of Firefox</hr>
   <hu>Croatian localisation of Firefox</hu>
   <id>Croatian localisation of Firefox</id>
   <is>Croatian localisation of Firefox</is>
   <it>Localizzazione croata di Firefox</it>
   <ja>Firefox のクロアチア語版</ja>
   <kk>Croatian localisation of Firefox</kk>
   <ko>Croatian localisation of Firefox</ko>
   <ku>Croatian localisation of Firefox</ku>
   <lt>Croatian localisation of Firefox</lt>
   <mk>Croatian localisation of Firefox</mk>
   <mr>Croatian localisation of Firefox</mr>
   <nb_NO>Croatian localisation of Firefox</nb_NO>
   <nb>Kroatisk lokaltilpassing av Firefox</nb>
   <nl_BE>Croatian localisation of Firefox</nl_BE>
   <nl>Kroatische lokalisatie van Firefox</nl>
   <or>Croatian localisation of Firefox</or>
   <pl>Chorwacka lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Croata Localização para o Firefox</pt_BR>
   <pt>Croata Localização para Firefox</pt>
   <ro>Croatian localisation of Firefox</ro>
   <ru>Хорватская локализация Firefox</ru>
   <sk>Croatian localisation of Firefox</sk>
   <sl>Hrvaške krajevne nastavitve za Firefox</sl>
   <so>Croatian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në kroatisht</sq>
   <sr>Croatian localisation of Firefox</sr>
   <sv>Kroatisk lokalisering av Firefox</sv>
   <th>Croatian localisation ของ Firefox</th>
   <tr>Firefox'un Hırvatça yerelleştirmesi</tr>
   <uk>Croatian локалізація Firefox</uk>
   <vi>Croatian localisation of Firefox</vi>
   <zh_CN>Firefox 克罗地亚语语言包</zh_CN>
   <zh_HK>Croatian localisation of Firefox</zh_HK>
   <zh_TW>Croatian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-hr
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-hr
</uninstall_package_names>
</app>
