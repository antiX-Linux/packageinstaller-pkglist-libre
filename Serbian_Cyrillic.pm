<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Serbian (Cyrillic)
</name>

<description>
   <am>Serbian (Cyrillic) dictionary for hunspell</am>
   <ar>Serbian (Cyrillic) dictionary for hunspell</ar>
   <be>Serbian (Cyrillic) dictionary for hunspell</be>
   <bg>Serbian (Cyrillic) dictionary for hunspell</bg>
   <bn>Serbian (Cyrillic) dictionary for hunspell</bn>
   <ca>Diccionari Serbi (Ciríl·lic) per hunspell</ca>
   <cs>Serbian (Cyrillic) dictionary for hunspell</cs>
   <da>Serbian (Cyrillic) dictionary for hunspell</da>
   <de>Serbisches Wörterbuch (kyrillisch) für “Hunspell”</de>
   <el>Σερβικά (Κυριλλικά) λεξικό για hunspell</el>
   <en>Serbian (Cyrillic) dictionary for hunspell</en>
   <es_ES>Diccionario Serbio (Cirílico) para hunspell</es_ES>
   <es>Diccionario Serbio (Cirílico) para hunspell</es>
   <et>Serbian (Cyrillic) dictionary for hunspell</et>
   <eu>Serbian (Cyrillic) dictionary for hunspell</eu>
   <fa>Serbian (Cyrillic) dictionary for hunspell</fa>
   <fil_PH>Serbian (Cyrillic) dictionary for hunspell</fil_PH>
   <fi>Serbian (Cyrillic) dictionary for hunspell</fi>
   <fr_BE>Serbe (Cyrillique) dictionnaire pour hunspell</fr_BE>
   <fr>Serbe (Cyrillique) dictionnaire pour hunspell</fr>
   <gl_ES>Serbian (Cyrillic) dictionary for hunspell</gl_ES>
   <gu>Serbian (Cyrillic) dictionary for hunspell</gu>
   <he_IL>Serbian (Cyrillic) dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु सर्बियाई (सिरिलिक) शब्दकोष</hi>
   <hr>Serbian (Cyrillic) dictionary for hunspell</hr>
   <hu>Serbian (Cyrillic) dictionary for hunspell</hu>
   <id>Serbian (Cyrillic) dictionary for hunspell</id>
   <is>Serbian (Cyrillic) dictionary for hunspell</is>
   <it>Dizionario serbian (cyrillic) per hunspell</it>
   <ja>Hunspell 用セルビア語（キリル）辞書</ja>
   <kk>Serbian (Cyrillic) dictionary for hunspell</kk>
   <ko>Serbian (Cyrillic) dictionary for hunspell</ko>
   <ku>Serbian (Cyrillic) dictionary for hunspell</ku>
   <lt>Serbian (Cyrillic) dictionary for hunspell</lt>
   <mk>Serbian (Cyrillic) dictionary for hunspell</mk>
   <mr>Serbian (Cyrillic) dictionary for hunspell</mr>
   <nb_NO>Serbian (Cyrillic) dictionary for hunspell</nb_NO>
   <nb>Serbisk (kyrillisk) ordliste for hunspell</nb>
   <nl_BE>Serbian (Cyrillic) dictionary for hunspell</nl_BE>
   <nl>Serbian (Cyrillic) dictionary for hunspell</nl>
   <or>Serbian (Cyrillic) dictionary for hunspell</or>
   <pl>Serbian (Cyrillic) dictionary for hunspell</pl>
   <pt_BR>Dicionário Sérvio (Cirílico) para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Sérvio (Cirílico) para hunspell</pt>
   <ro>Serbian (Cyrillic) dictionary for hunspell</ro>
   <ru>Serbian (Cyrillic) dictionary for hunspell</ru>
   <sk>Serbian (Cyrillic) dictionary for hunspell</sk>
   <sl>Srbski (cirilica) slovar za hunspell</sl>
   <so>Serbian (Cyrillic) dictionary for hunspell</so>
   <sq>Fjalor serbisht (cirilike) për hunspell</sq>
   <sr>Serbian (Cyrillic) dictionary for hunspell</sr>
   <sv>Serbisk (Kyrillisk) ordbok för hunspell</sv>
   <th>Serbian (Cyrillic) dictionary for hunspell</th>
   <tr>Hunspell için Sırpça (Kiril) sözlük</tr>
   <uk>Serbian (Cyrillic) dictionary for hunspell</uk>
   <vi>Serbian (Cyrillic) dictionary for hunspell</vi>
   <zh_CN>Serbian (Cyrillic) dictionary for hunspell</zh_CN>
   <zh_HK>Serbian (Cyrillic) dictionary for hunspell</zh_HK>
   <zh_TW>Serbian (Cyrillic) dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-sr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-sr
</uninstall_package_names>

</app>
