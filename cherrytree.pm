<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Graphics
</category>

<name>
CherryTree
</name>

<description>
   <am>Hierarchical note taking application</am>
   <ar>Hierarchical note taking application</ar>
   <be>Hierarchical note taking application</be>
   <bg>Hierarchical note taking application</bg>
   <bn>Hierarchical note taking application</bn>
   <ca>Aplicació jeràrquica per prendre notes</ca>
   <cs>Hierarchical note taking application</cs>
   <da>Hierarchical note taking application</da>
   <de>Hierarchisch organisiertes Notizbuch</de>
   <el>Ιεραρχική εφαρμογή λήψης σημειώσεων</el>
   <en>Hierarchical note taking application</en>
   <es_ES>Aplicación de toma de notas ligera, rápida y jerárquica</es_ES>
   <es>Aplicación de toma de notas ligera, rápida y jerárquica</es>
   <et>Hierarchical note taking application</et>
   <eu>Hierarchical note taking application</eu>
   <fa>Hierarchical note taking application</fa>
   <fil_PH>Hierarchical note taking application</fil_PH>
   <fi>Hierarchical note taking application</fi>
   <fr_BE>Application de prise de notes hiérarchique</fr_BE>
   <fr>Application de prise de notes hiérarchique</fr>
   <gl_ES>Hierarchical note taking application</gl_ES>
   <gu>Hierarchical note taking application</gu>
   <he_IL>Hierarchical note taking application</he_IL>
   <hi>अनुक्रमण युक्त नोट लेखन अनुप्रयोग</hi>
   <hr>Hierarchical note taking application</hr>
   <hu>Hierarchical note taking application</hu>
   <id>Hierarchical note taking application</id>
   <is>Hierarchical note taking application</is>
   <it>Applicazione per prendere appunti in modo gerarchico</it>
   <ja>階層型ノートアプリ</ja>
   <kk>Hierarchical note taking application</kk>
   <ko>Hierarchical note taking application</ko>
   <ku>Hierarchical note taking application</ku>
   <lt>Hierarchical note taking application</lt>
   <mk>Hierarchical note taking application</mk>
   <mr>Hierarchical note taking application</mr>
   <nb_NO>Hierarchical note taking application</nb_NO>
   <nb>Hierarkisk notisblokk</nb>
   <nl_BE>Hierarchical note taking application</nl_BE>
   <nl>Hiërarchische toepassing voor het maken van notities</nl>
   <or>Hierarchical note taking application</or>
   <pl>Hierarchical note taking application</pl>
   <pt_BR>Aplicativo de anotações hierárquicas</pt_BR>
   <pt>Aplicação de notas hierárquizadas</pt>
   <ro>Hierarchical note taking application</ro>
   <ru>Hierarchical note taking application</ru>
   <sk>Hierarchical note taking application</sk>
   <sl>Program za zajemanje hierarhičnih zapiskov</sl>
   <so>Hierarchical note taking application</so>
   <sq>Aplikacion hierarkik për mbajtje shënimesh</sq>
   <sr>Hierarchical note taking application</sr>
   <sv>Hierarkiskt anteckningsprogram</sv>
   <th>Hierarchical note taking application</th>
   <tr>Sıradüzenli not alma uygulaması</tr>
   <uk>Hierarchical note taking application</uk>
   <vi>Hierarchical note taking application</vi>
   <zh_CN>Hierarchical note taking application</zh_CN>
   <zh_HK>Hierarchical note taking application</zh_HK>
   <zh_TW>Hierarchical note taking application</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
cherrytree
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
cherrytree
</uninstall_package_names>
</app>
