<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
FTP
</category>

<name>
Filezilla
</name>

<description>
   <am>a full-featured FTP client with an easy-to-use GUI</am>
   <ar>a full-featured FTP client with an easy-to-use GUI</ar>
   <be>a full-featured FTP client with an easy-to-use GUI</be>
   <bg>a full-featured FTP client with an easy-to-use GUI</bg>
   <bn>a full-featured FTP client with an easy-to-use GUI</bn>
   <ca>un client de FTP complet amb IGU fàcil d'usar</ca>
   <cs>a full-featured FTP client with an easy-to-use GUI</cs>
   <da>en FTP-klient med det hele med en brugerflade som er let at bruge</da>
   <de>Ein voll ausgestatter FTP-Client mit benutzerfreundliche Oberfläche</de>
   <el>Ένα πλήρες πρόγραμμα FTP με ένα εύχρηστο GUI</el>
   <en>a full-featured FTP client with an easy-to-use GUI</en>
   <es_ES>Un cliente FTP con una interfaz completa y fácil de usar</es_ES>
   <es>Un cliente FTP con una interfaz completa y fácil de usar</es>
   <et>a full-featured FTP client with an easy-to-use GUI</et>
   <eu>a full-featured FTP client with an easy-to-use GUI</eu>
   <fa>a full-featured FTP client with an easy-to-use GUI</fa>
   <fil_PH>a full-featured FTP client with an easy-to-use GUI</fil_PH>
   <fi>Täysiominaisuuksinen FTP-asiakasohjelma helppokäyttöisellä graafisella käyttöliittymällä</fi>
   <fr_BE>Un client FTP très complet avec une interface graphique intuitive</fr_BE>
   <fr>Un client FTP très complet avec une interface graphique intuitive</fr>
   <gl_ES>Cliente FTP completo con interface gráfica intuitiva</gl_ES>
   <gu>a full-featured FTP client with an easy-to-use GUI</gu>
   <he_IL>a full-featured FTP client with an easy-to-use GUI</he_IL>
   <hi>सरल ग्राफ़िकल अंतरफलक युक्त पूर्ण विशेषताओं वाला एफटीपी साधन</hi>
   <hr>a full-featured FTP client with an easy-to-use GUI</hr>
   <hu>a full-featured FTP client with an easy-to-use GUI</hu>
   <id>a full-featured FTP client with an easy-to-use GUI</id>
   <is>a full-featured FTP client with an easy-to-use GUI</is>
   <it>Client Ftp ad interfaccia grafica, completo e di facile uso</it>
   <ja>使いやすい GUI を備えたフル機能の FTP クライアント</ja>
   <kk>a full-featured FTP client with an easy-to-use GUI</kk>
   <ko>a full-featured FTP client with an easy-to-use GUI</ko>
   <ku>a full-featured FTP client with an easy-to-use GUI</ku>
   <lt>a full-featured FTP client with an easy-to-use GUI</lt>
   <mk>a full-featured FTP client with an easy-to-use GUI</mk>
   <mr>a full-featured FTP client with an easy-to-use GUI</mr>
   <nb_NO>a full-featured FTP client with an easy-to-use GUI</nb_NO>
   <nb>FTP-klient med full funksjonalitet og grafisk brukergrensesnitt</nb>
   <nl_BE>a full-featured FTP client with an easy-to-use GUI</nl_BE>
   <nl>een volledig voorzien FTP programma met een makkelijk te gebruiken grafische werkomgeving</nl>
   <or>a full-featured FTP client with an easy-to-use GUI</or>
   <pl>w pełni funkcjonalny klient FTP z łatwym w użyciu interfejsem graficznym</pl>
   <pt_BR>Cliente FTP completo com interface gráfica intuitiva</pt_BR>
   <pt>Cliente FTP completo com interface gráfica intuitiva</pt>
   <ro>a full-featured FTP client with an easy-to-use GUI</ro>
   <ru>Полнофункциональный FTP клиент с удобным GUI</ru>
   <sk>a full-featured FTP client with an easy-to-use GUI</sk>
   <sl>Polnozmogljiv FTP odjemalec z enostavnim uporabniškim vmesnikom</sl>
   <so>a full-featured FTP client with an easy-to-use GUI</so>
   <sq>Një klient FTP i plotë për nga veçoritë, me një GUI të lehtë për t’u përdorur</sq>
   <sr>a full-featured FTP client with an easy-to-use GUI</sr>
   <sv>en fullutrustad FTP klient med lättanvänt grafiskt gränssnitt</sv>
   <th>FTP Client เต็มรูปแบบพร้อม GUI ที่ใช้งานง่าย</th>
   <tr>kullanımı kolay grafik arayüzü olan, tam özellikli bir FTP istemcisi</tr>
   <uk>повнофункціональний FTP-клієнт з простим у використанні графічним інтерфейсом</uk>
   <vi>a full-featured FTP client with an easy-to-use GUI</vi>
   <zh_CN>带有易用 GUI 的全功能 FTP 客户端</zh_CN>
   <zh_HK>a full-featured FTP client with an easy-to-use GUI</zh_HK>
   <zh_TW>a full-featured FTP client with an easy-to-use GUI</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
filezilla
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
filezilla
</uninstall_package_names>
</app>
