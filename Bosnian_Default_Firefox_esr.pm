<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bosnian_Default_Firefox_esr
</name>

<description>
   <am>Bosnian localisation of Firefox ESR</am>
   <ar>Bosnian localisation of Firefox ESR</ar>
   <be>Bosnian localisation of Firefox ESR</be>
   <bg>Bosnian localisation of Firefox ESR</bg>
   <bn>Bosnian localisation of Firefox ESR</bn>
   <ca>Localització en Bosnià de Firefox ESR</ca>
   <cs>Bosnian localisation of Firefox ESR</cs>
   <da>Bosnian localisation of Firefox ESR</da>
   <de>Bosnian localisation of Firefox ESR</de>
   <el>Βοσνιακό για το Firefox ESR</el>
   <en>Bosnian localisation of Firefox ESR</en>
   <es_ES>Localización en bosnio de Firefox ESR</es_ES>
   <es>Localización en bosnio de Firefox ESR</es>
   <et>Bosnian localisation of Firefox ESR</et>
   <eu>Bosnian localisation of Firefox ESR</eu>
   <fa>Bosnian localisation of Firefox ESR</fa>
   <fil_PH>Bosnian localisation of Firefox ESR</fil_PH>
   <fi>Bosnian localisation of Firefox ESR</fi>
   <fr_BE>Localisation bosniaque pour Firefox ESR</fr_BE>
   <fr>Localisation bosniaque pour Firefox ESR</fr>
   <gl_ES>Bosnian localisation of Firefox ESR</gl_ES>
   <gu>Bosnian localisation of Firefox ESR</gu>
   <he_IL>Bosnian localisation of Firefox ESR</he_IL>
   <hi>Bosnian localisation of Firefox ESR</hi>
   <hr>Bosnian localisation of Firefox ESR</hr>
   <hu>Bosnian localisation of Firefox ESR</hu>
   <id>Bosnian localisation of Firefox ESR</id>
   <is>Bosnian localisation of Firefox ESR</is>
   <it>Localizzazione Bosnian di Firefox ESR</it>
   <ja>Bosnian localisation of Firefox ESR</ja>
   <kk>Bosnian localisation of Firefox ESR</kk>
   <ko>Bosnian localisation of Firefox ESR</ko>
   <ku>Bosnian localisation of Firefox ESR</ku>
   <lt>Bosnian localisation of Firefox ESR</lt>
   <mk>Bosnian localisation of Firefox ESR</mk>
   <mr>Bosnian localisation of Firefox ESR</mr>
   <nb_NO>Bosnian localisation of Firefox ESR</nb_NO>
   <nb>Bosnisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Bosnian localisation of Firefox ESR</nl_BE>
   <nl>Bosnische lokalisatie van Firefox ESR</nl>
   <or>Bosnian localisation of Firefox ESR</or>
   <pl>Bosnian localisation of Firefox ESR</pl>
   <pt_BR>Bósnio Localização para o Firefox ESR</pt_BR>
   <pt>Bósnio Localização para Firefox ESR</pt>
   <ro>Bosnian localisation of Firefox ESR</ro>
   <ru>Bosnian localisation of Firefox ESR</ru>
   <sk>Bosnian localisation of Firefox ESR</sk>
   <sl>Bosanske krajevne nastavitve za Firefox ESR</sl>
   <so>Bosnian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në bosnjisht</sq>
   <sr>Bosnian localisation of Firefox ESR</sr>
   <sv>Bosnisk  lokalisering av Firefox ESR</sv>
   <th>Bosnian localisation of Firefox ESR</th>
   <tr>Bosnian localisation of Firefox ESR</tr>
   <uk>Bosnian localisation of Firefox ESR</uk>
   <vi>Bosnian localisation of Firefox ESR</vi>
   <zh_CN>Bosnian localisation of Firefox ESR</zh_CN>
   <zh_HK>Bosnian localisation of Firefox ESR</zh_HK>
   <zh_TW>Bosnian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-bs
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-bs
</uninstall_package_names>

</app>
