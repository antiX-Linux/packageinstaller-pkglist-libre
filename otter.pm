<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Otter
</name>

<description>
   <am>Latest Otter browser</am>
   <ar>Latest Otter browser</ar>
   <bg>Latest Otter browser</bg>
   <bn>Latest Otter browser</bn>
   <ca>Darrer navegador Otter (estable)</ca>
   <cs>Latest Otter browser</cs>
   <da>Seneste Otter-browser (stabil)</da>
   <de>Aktueller Otter-Browser (stabil)</de>
   <el>Τελευταίο πρόγραμμα περιήγησης Otter (σταθερό)</el>
   <en>Latest Otter browser</en>
   <es>El último Otter (estable)</es>
   <et>Latest Otter browser</et>
   <eu>Latest Otter browser</eu>
   <fa>Latest Otter browser</fa>
   <fil_PH>Latest Otter browser</fil_PH>
   <fi>Latest Otter browser</fi>
   <fr>La dernière version de Otter</fr>
   <he_IL>Latest Otter browser</he_IL>
   <hi>Latest Otter browser</hi>
   <hr>Latest Otter browser</hr>
   <hu>Latest Otter browser</hu>
   <id>Latest Otter browser</id>
   <is>Latest Otter browser</is>
   <it>Ultima versione (stabile) del browser Otter</it>
   <ja_JP>Latest Otter browser</ja_JP>
   <ja>Latest Otter browser</ja>
   <kk>Latest Otter browser</kk>
   <ko>Latest Otter browser</ko>
   <lt>Latest Otter browser</lt>
   <mk>Latest Otter browser</mk>
   <mr>Latest Otter browser</mr>
   <nb>Latest Otter browser</nb>
   <nl>Meest recente Otter browser</nl>
   <pl>najnowsza przeglądarka Otter (stabilna)</pl>
   <pt_BR>Versão mais recente (estável) do navegador web Otter</pt_BR>
   <pt>Versão mais recente (estável) do navegador web Otter</pt>
   <ro>Latest Otter browser</ro>
   <ru>Браузер Otter (последняя стабильная версия)</ru>
   <sk>Posledný Otter prehliadač (stabilný)</sk>
   <sl>Zadnja različica googlovega Otter brskalnika</sl>
   <sq>Latest Otter browser</sq>
   <sr>Latest Otter browser</sr>
   <sv>SenasteOtter webbläsare</sv>
   <tr>Latest Otter browser</tr>
   <uk>Крайня стабільна версія браузера Otter</uk>
   <vi>Latest Otter browser</vi>
   <zh_CN>Latest Otter browser</zh_CN>
   <zh_TW>Latest Otter browser</zh_TW>
</description>

<installable>
 all
</installable>

<screenshot></screenshot>

<preinstall>

</preinstall>

<install_package_names>
otter-browser
</install_package_names>


<postinstall>

</postinstall>

<uninstall_package_names>
otter-browser
</uninstall_package_names>

<postuninstall>

</postuninstall>
</app>
