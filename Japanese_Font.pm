<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Japanese_Font
</name>

<description>
   <am>Japanese fonts packages</am>
   <ar>Japanese fonts packages</ar>
   <be>Japanese fonts packages</be>
   <bg>Japanese fonts packages</bg>
   <bn>Japanese fonts packages</bn>
   <ca>Paquets de tipus de lletra japoneses</ca>
   <cs>Japanese fonts packages</cs>
   <da>Japansk skrifttyper-pakke</da>
   <de>Japanische Schriftenpakete</de>
   <el>Ιαπωνικά πακέτα γραμματοσειρών</el>
   <en>Japanese fonts packages</en>
   <es_ES>Paquetes de fuentes japonesas</es_ES>
   <es>Paquetes de fuentes japonesas</es>
   <et>Japanese fonts packages</et>
   <eu>Japanese fonts packages</eu>
   <fa>Japanese fonts packages</fa>
   <fil_PH>Japanese fonts packages</fil_PH>
   <fi>Japanilaiset kirjasinpaketit</fi>
   <fr_BE>Paquets pour polices japonaises</fr_BE>
   <fr>Paquets pour polices japonaises</fr>
   <gl_ES>Paquetes de fontes de xaponés</gl_ES>
   <gu>Japanese fonts packages</gu>
   <he_IL>Japanese fonts packages</he_IL>
   <hi>जापानी मुद्रलिपि पैकेज</hi>
   <hr>Japanese fonts packages</hr>
   <hu>Japanese fonts packages</hu>
   <id>Japanese fonts packages</id>
   <is>Japanese fonts packages</is>
   <it>Pacchetti di fonts per il Giapponese</it>
   <ja>日本語フォントのパッケージ集</ja>
   <kk>Japanese fonts packages</kk>
   <ko>Japanese fonts packages</ko>
   <ku>Japanese fonts packages</ku>
   <lt>Japonų šriftų paketai</lt>
   <mk>Japanese fonts packages</mk>
   <mr>Japanese fonts packages</mr>
   <nb_NO>Japanese fonts packages</nb_NO>
   <nb>Pakke med japanske skrifttyper</nb>
   <nl_BE>Japanese fonts packages</nl_BE>
   <nl>Japanse font pakketten</nl>
   <or>Japanese fonts packages</or>
   <pl>pakiety z japońskimi fontami (czcionkami)</pl>
   <pt_BR>Pacotes de Fontes Japonês</pt_BR>
   <pt>Japonês Pacotes de fontes</pt>
   <ro>Japanese fonts packages</ro>
   <ru>Japanese fonts packages</ru>
   <sk>Japanese fonts packages</sk>
   <sl>Japonski paketi s pisavami</sl>
   <so>Japanese fonts packages</so>
   <sq>Paketa shkronjash japonishteje</sq>
   <sr>Japanese fonts packages</sr>
   <sv>Japanska typsnitts-paket</sv>
   <th>แพ็กเกจฟอนต์ภาษาญี่ปุ่น</th>
   <tr>Japonca yazı tipi paketleri</tr>
   <uk>пакунки японських шрифтів</uk>
   <vi>Japanese fonts packages</vi>
   <zh_CN>Japanese fonts packages</zh_CN>
   <zh_HK>Japanese fonts packages</zh_HK>
   <zh_TW>Japanese fonts packages</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
fonts-ipafont-gothic
fonts-ipafont-mincho
fonts-umeplus
fonts-vlgothic
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
fonts-ipafont-gothic
fonts-ipafont-mincho
fonts-umeplus
fonts-vlgothic
</uninstall_package_names>

</app>
