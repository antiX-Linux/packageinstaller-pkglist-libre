<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Audio
</category>

<name>
Audacity
</name>

<description>
   <am>a multi-track audio editor</am>
   <ar>a multi-track audio editor</ar>
   <be>a multi-track audio editor</be>
   <bg>a multi-track audio editor</bg>
   <bn>a multi-track audio editor</bn>
   <ca>Editor d'àudio multipista</ca>
   <cs>a multi-track audio editor</cs>
   <da>en multispor lydredigering</da>
   <de>Ein Mehrspur-Audio-Editor</de>
   <el>Πρόγραμμα επεξεργασίας ήχου πολλαπλών κομματιών</el>
   <en>a multi-track audio editor</en>
   <es_ES>Editor de audio multi-pistas</es_ES>
   <es>Editor de audio multi-pistas</es>
   <et>a multi-track audio editor</et>
   <eu>a multi-track audio editor</eu>
   <fa>a multi-track audio editor</fa>
   <fil_PH>a multi-track audio editor</fil_PH>
   <fi>Moniraitainen audio-muokkausohjelma</fi>
   <fr_BE>Un éditeur audio multi-pistes</fr_BE>
   <fr>Un éditeur audio multi-pistes</fr>
   <gl_ES>Editor de audio multi-pista</gl_ES>
   <gu>a multi-track audio editor</gu>
   <he_IL>a multi-track audio editor</he_IL>
   <hi>एकाधिक-ट्रैक ऑडियो संपादक</hi>
   <hr>a multi-track audio editor</hr>
   <hu>a multi-track audio editor</hu>
   <id>a multi-track audio editor</id>
   <is>a multi-track audio editor</is>
   <it>Audio editor multi-traccia</it>
   <ja>マルチトラック・オーディオエディター</ja>
   <kk>a multi-track audio editor</kk>
   <ko>a multi-track audio editor</ko>
   <ku>a multi-track audio editor</ku>
   <lt>a multi-track audio editor</lt>
   <mk>a multi-track audio editor</mk>
   <mr>a multi-track audio editor</mr>
   <nb_NO>a multi-track audio editor</nb_NO>
   <nb>lydredigering med flere spor</nb>
   <nl_BE>a multi-track audio editor</nl_BE>
   <nl>een multi-track audio editor</nl>
   <or>a multi-track audio editor</or>
   <pl>wielościeżkowy edytor audio</pl>
   <pt_BR>Editor de áudio multifaixa</pt_BR>
   <pt>Editor de áudio multi-faixa</pt>
   <ro>a multi-track audio editor</ro>
   <ru>Многодорожечный аудио редактор</ru>
   <sk>viackanálový audio editor</sk>
   <sl>večstezni urejevalnik zvoka</sl>
   <so>a multi-track audio editor</so>
   <sq>Një përpunues audio me shumë pista</sq>
   <sr>a multi-track audio editor</sr>
   <sv>en flerspårs ljudredigerare</sv>
   <th>โปรแกรมตัดต่อเสียง Multi-track</th>
   <tr>çok kanallı bir ses düzenleyici</tr>
   <uk>мульти-трековий редактор аудіо</uk>
   <vi>a multi-track audio editor</vi>
   <zh_CN>多轨音频编辑器</zh_CN>
   <zh_HK>a multi-track audio editor</zh_HK>
   <zh_TW>a multi-track audio editor</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
audacity
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
audacity
</uninstall_package_names>
</app>
