<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Italian
</name>

<description>
   <am>Italian dictionary for hunspell</am>
   <ar>Italian dictionary for hunspell</ar>
   <be>Italian dictionary for hunspell</be>
   <bg>Italian dictionary for hunspell</bg>
   <bn>Italian dictionary for hunspell</bn>
   <ca>Diccionari Italià per hunspell</ca>
   <cs>Italian dictionary for hunspell</cs>
   <da>Italian dictionary for hunspell</da>
   <de>Italienisches Wörterbuch für “Hunspell”</de>
   <el>Ιταλικό λεξικό για hunspell</el>
   <en>Italian dictionary for hunspell</en>
   <es_ES>Diccionario Italiano para hunspell</es_ES>
   <es>Diccionario Italiano para hunspell</es>
   <et>Italian dictionary for hunspell</et>
   <eu>Italian dictionary for hunspell</eu>
   <fa>Italian dictionary for hunspell</fa>
   <fil_PH>Italian dictionary for hunspell</fil_PH>
   <fi>Italian dictionary for hunspell</fi>
   <fr_BE>Italien dictionnaire pour hunspell</fr_BE>
   <fr>Italien dictionnaire pour hunspell</fr>
   <gl_ES>Italian dictionary for hunspell</gl_ES>
   <gu>Italian dictionary for hunspell</gu>
   <he_IL>Italian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु इतालवी शब्दकोष</hi>
   <hr>Italian dictionary for hunspell</hr>
   <hu>Italian dictionary for hunspell</hu>
   <id>Italian dictionary for hunspell</id>
   <is>Italian dictionary for hunspell</is>
   <it>Dizionario italiano per hunspell</it>
   <ja>Hunspell 用イタリア語辞書</ja>
   <kk>Italian dictionary for hunspell</kk>
   <ko>Italian dictionary for hunspell</ko>
   <ku>Italian dictionary for hunspell</ku>
   <lt>Italian dictionary for hunspell</lt>
   <mk>Italian dictionary for hunspell</mk>
   <mr>Italian dictionary for hunspell</mr>
   <nb_NO>Italian dictionary for hunspell</nb_NO>
   <nb>Italiensk ordliste for hunspell</nb>
   <nl_BE>Italian dictionary for hunspell</nl_BE>
   <nl>Italian dictionary for hunspell</nl>
   <or>Italian dictionary for hunspell</or>
   <pl>Italian dictionary for hunspell</pl>
   <pt_BR>Dicionário Italiano para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Italiano para hunspell</pt>
   <ro>Italian dictionary for hunspell</ro>
   <ru>Italian dictionary for hunspell</ru>
   <sk>Italian dictionary for hunspell</sk>
   <sl>Italijanski slovar za hunspell</sl>
   <so>Italian dictionary for hunspell</so>
   <sq>Fjalor italisht për hunspell</sq>
   <sr>Italian dictionary for hunspell</sr>
   <sv>Italiensk ordbok för hunspell</sv>
   <th>Italian dictionary for hunspell</th>
   <tr>Hunspell için İtalyanca sözlük</tr>
   <uk>Italian dictionary for hunspell</uk>
   <vi>Italian dictionary for hunspell</vi>
   <zh_CN>Italian dictionary for hunspell</zh_CN>
   <zh_HK>Italian dictionary for hunspell</zh_HK>
   <zh_TW>Italian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-it
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-it
</uninstall_package_names>

</app>
