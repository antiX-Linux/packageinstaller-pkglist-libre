<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Media Converter
</category>

<name>
Asunder
</name>

<description>
   <am>Graphical audio CD ripper and encoder</am>
   <ar>Graphical audio CD ripper and encoder</ar>
   <be>Graphical audio CD ripper and encoder</be>
   <bg>Graphical audio CD ripper and encoder</bg>
   <bn>Graphical audio CD ripper and encoder</bn>
   <ca>Gravador i codificador gràfic de CDs</ca>
   <cs>Graphical audio CD ripper and encoder</cs>
   <da>Graphical audio CD ripper and encoder</da>
   <de>Graphisches Programm zur digitalen Audioextraktion (DAE) und und Kodierung von Audio-CDs (Ugs.: “CD-Ripper“ und “Encoder”)</de>
   <el>CD ripper και κωδικοποιητής</el>
   <en>Graphical audio CD ripper and encoder</en>
   <es_ES>Extractor y codificador grafico de CD de audio</es_ES>
   <es>Extractor y codificador gráfico de CD de audio</es>
   <et>Graphical audio CD ripper and encoder</et>
   <eu>Graphical audio CD ripper and encoder</eu>
   <fa>Graphical audio CD ripper and encoder</fa>
   <fil_PH>Graphical audio CD ripper and encoder</fil_PH>
   <fi>Graafinen ääni-CD rippaus- ja purkamisohjelma</fi>
   <fr_BE>Convertisseur et encodeur graphique de CD audio</fr_BE>
   <fr>Convertisseur et encodeur graphique de CD audio</fr>
   <gl_ES>Graphical audio CD ripper and encoder</gl_ES>
   <gu>Graphical audio CD ripper and encoder</gu>
   <he_IL>Graphical audio CD ripper and encoder</he_IL>
   <hi>ऑडियो सीडी हेतु ग्राफ़िकल रिपर व एनकोडर साधन</hi>
   <hr>Graphical audio CD ripper and encoder</hr>
   <hu>Graphical audio CD ripper and encoder</hu>
   <id>Graphical audio CD ripper and encoder</id>
   <is>Graphical audio CD ripper and encoder</is>
   <it>Rippatore e codificatore grafico di CD audio</it>
   <ja>グラフィカルなオーディオCDリッパー＆エンコーダー</ja>
   <kk>Graphical audio CD ripper and encoder</kk>
   <ko>Graphical audio CD ripper and encoder</ko>
   <ku>Graphical audio CD ripper and encoder</ku>
   <lt>Graphical audio CD ripper and encoder</lt>
   <mk>Graphical audio CD ripper and encoder</mk>
   <mr>Graphical audio CD ripper and encoder</mr>
   <nb_NO>Graphical audio CD ripper and encoder</nb_NO>
   <nb>Grafisk ripping av lyd-CD og komprimering</nb>
   <nl_BE>Graphical audio CD ripper and encoder</nl_BE>
   <nl>Grafische audio-cd-ripper en encoder</nl>
   <or>Graphical audio CD ripper and encoder</or>
   <pl>Graphical audio CD ripper and encoder</pl>
   <pt_BR>Extrator e codificador gráfico de CD de áudio</pt_BR>
   <pt>Ripper e encodificador de audio CDs em modo gráfico</pt>
   <ro>Graphical audio CD ripper and encoder</ro>
   <ru>Graphical audio CD ripper and encoder</ru>
   <sk>Graphical audio CD ripper and encoder</sk>
   <sl>Grafični zajemalnik zvočnih CDjev in kodirnik</sl>
   <so>Graphical audio CD ripper and encoder</so>
   <sq>Përftues grafik pjesësh CD-je audio dhe kodues</sq>
   <sr>Graphical audio CD ripper and encoder</sr>
   <sv>Grafisk audio CD ripper och encoder</sv>
   <th>Graphical audio CD ripper and encoder</th>
   <tr>Grafiksel ses CD'si dönüştürücü ve kodlayıcı</tr>
   <uk>Graphical audio CD ripper and encoder</uk>
   <vi>Graphical audio CD ripper and encoder</vi>
   <zh_CN>Graphical audio CD ripper and encoder</zh_CN>
   <zh_HK>Graphical audio CD ripper and encoder</zh_HK>
   <zh_TW>Graphical audio CD ripper and encoder</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
asunder
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
asunder
</uninstall_package_names>
</app>
