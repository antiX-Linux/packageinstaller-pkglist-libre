<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Turkish_Default_Firefox_esr
</name>

<description>
   <am>Turkish localisation of Firefox ESR</am>
   <ar>Turkish localisation of Firefox ESR</ar>
   <be>Turkish localisation of Firefox ESR</be>
   <bg>Turkish localisation of Firefox ESR</bg>
   <bn>Turkish localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Turc</ca>
   <cs>Turkish localisation of Firefox ESR</cs>
   <da>Turkish localisation of Firefox ESR</da>
   <de>Türkische Lokalisation von “Firefox ESR”</de>
   <el>Τουρκικά για Firefox ESR</el>
   <en>Turkish localisation of Firefox ESR</en>
   <es_ES>Localización Turco de Firefox ESR</es_ES>
   <es>Localización Turco de Firefox ESR</es>
   <et>Turkish localisation of Firefox ESR</et>
   <eu>Turkish localisation of Firefox ESR</eu>
   <fa>Turkish localisation of Firefox ESR</fa>
   <fil_PH>Turkish localisation of Firefox ESR</fil_PH>
   <fi>Turkish localisation of Firefox ESR</fi>
   <fr_BE>Localisation en turque pour Firefox ESR</fr_BE>
   <fr>Localisation en turque pour Firefox ESR</fr>
   <gl_ES>Turkish localisation of Firefox ESR</gl_ES>
   <gu>Turkish localisation of Firefox ESR</gu>
   <he_IL>Turkish localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का तुर्क संस्करण</hi>
   <hr>Turkish localisation of Firefox ESR</hr>
   <hu>Turkish localisation of Firefox ESR</hu>
   <id>Turkish localisation of Firefox ESR</id>
   <is>Turkish localisation of Firefox ESR</is>
   <it>Localizzazione turca di Firefox ESR</it>
   <ja>トルコ語版 Firefox ESR</ja>
   <kk>Turkish localisation of Firefox ESR</kk>
   <ko>Turkish localisation of Firefox ESR</ko>
   <ku>Turkish localisation of Firefox ESR</ku>
   <lt>Turkish localisation of Firefox ESR</lt>
   <mk>Turkish localisation of Firefox ESR</mk>
   <mr>Turkish localisation of Firefox ESR</mr>
   <nb_NO>Turkish localisation of Firefox ESR</nb_NO>
   <nb>Tyrkisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Turkish localisation of Firefox ESR</nl_BE>
   <nl>Turkish localisation of Firefox ESR</nl>
   <or>Turkish localisation of Firefox ESR</or>
   <pl>Turkish localisation of Firefox ESR</pl>
   <pt_BR>Turco Localização para o Firefox ESR</pt_BR>
   <pt>Turco Localização para Firefox ESR</pt>
   <ro>Turkish localisation of Firefox ESR</ro>
   <ru>Turkish localisation of Firefox ESR</ru>
   <sk>Turkish localisation of Firefox ESR</sk>
   <sl>Turške krajevne nastavitve za Firefox ESR</sl>
   <so>Turkish localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në turqisht</sq>
   <sr>Turkish localisation of Firefox ESR</sr>
   <sv>Turkisk lokalisering av Firefox ESR</sv>
   <th>Turkish localisation of Firefox ESR</th>
   <tr>Firefox ESR Türkçe yerelleştirmesi</tr>
   <uk>Turkish localisation of Firefox ESR</uk>
   <vi>Turkish localisation of Firefox ESR</vi>
   <zh_CN>Turkish localisation of Firefox ESR</zh_CN>
   <zh_HK>Turkish localisation of Firefox ESR</zh_HK>
   <zh_TW>Turkish localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-tr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-tr
</uninstall_package_names>

</app>
