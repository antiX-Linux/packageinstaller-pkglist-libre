<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Italian_Default_Firefox_esr
</name>

<description>
   <am>Italian localisation of Firefox ESR</am>
   <ar>Italian localisation of Firefox ESR</ar>
   <be>Italian localisation of Firefox ESR</be>
   <bg>Italian localisation of Firefox ESR</bg>
   <bn>Italian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Italià</ca>
   <cs>Italian localisation of Firefox ESR</cs>
   <da>Italian localisation of Firefox ESR</da>
   <de>Italienische Lokalisation von “Firefox ESR”</de>
   <el>Ιταλικά για Firefox ESR</el>
   <en>Italian localisation of Firefox ESR</en>
   <es_ES>Localización Italiana de Firefox ESR</es_ES>
   <es>Localización Italiano de Firefox ESR</es>
   <et>Italian localisation of Firefox ESR</et>
   <eu>Italian localisation of Firefox ESR</eu>
   <fa>Italian localisation of Firefox ESR</fa>
   <fil_PH>Italian localisation of Firefox ESR</fil_PH>
   <fi>Italian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en italien pour Firefox ESR</fr_BE>
   <fr>Localisation en italien pour Firefox ESR</fr>
   <gl_ES>Italian localisation of Firefox ESR</gl_ES>
   <gu>Italian localisation of Firefox ESR</gu>
   <he_IL>Italian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का इतालवी संस्करण</hi>
   <hr>Italian localisation of Firefox ESR</hr>
   <hu>Italian localisation of Firefox ESR</hu>
   <id>Italian localisation of Firefox ESR</id>
   <is>Italian localisation of Firefox ESR</is>
   <it>Localizzazione italiana di Firefox ESR</it>
   <ja>イタリア語版 Firefox ESR</ja>
   <kk>Italian localisation of Firefox ESR</kk>
   <ko>Italian localisation of Firefox ESR</ko>
   <ku>Italian localisation of Firefox ESR</ku>
   <lt>Italian localisation of Firefox ESR</lt>
   <mk>Italian localisation of Firefox ESR</mk>
   <mr>Italian localisation of Firefox ESR</mr>
   <nb_NO>Italian localisation of Firefox ESR</nb_NO>
   <nb>Italiensk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Italian localisation of Firefox ESR</nl_BE>
   <nl>Italian localisation of Firefox ESR</nl>
   <or>Italian localisation of Firefox ESR</or>
   <pl>Italian localisation of Firefox ESR</pl>
   <pt_BR>Italiano Localização para o Firefox ESR</pt_BR>
   <pt>Italiano Localização para Firefox ESR</pt>
   <ro>Italian localisation of Firefox ESR</ro>
   <ru>Italian localisation of Firefox ESR</ru>
   <sk>Italian localisation of Firefox ESR</sk>
   <sl>Italijanske krajevne nastavitve za Firefox ESR</sl>
   <so>Italian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në italisht</sq>
   <sr>Italian localisation of Firefox ESR</sr>
   <sv>Italiensk lokalisering av Firefox ESR</sv>
   <th>Italian localisation of Firefox ESR</th>
   <tr>Firefox ESR İtalyanca yerelleştirmesi</tr>
   <uk>Italian localisation of Firefox ESR</uk>
   <vi>Italian localisation of Firefox ESR</vi>
   <zh_CN>Italian localisation of Firefox ESR</zh_CN>
   <zh_HK>Italian localisation of Firefox ESR</zh_HK>
   <zh_TW>Italian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-it
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-it
</uninstall_package_names>

</app>
