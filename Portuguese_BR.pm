<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Portugese(BR)
</name>

<description>
   <am>Portuguese (BR) dictionary for hunspell</am>
   <ar>Portuguese (BR) dictionary for hunspell</ar>
   <be>Portuguese (BR) dictionary for hunspell</be>
   <bg>Portuguese (BR) dictionary for hunspell</bg>
   <bn>Portuguese (BR) dictionary for hunspell</bn>
   <ca>Diccionari Portuguès (BR) per hunspell</ca>
   <cs>Portuguese (BR) dictionary for hunspell</cs>
   <da>Portuguese (BR) dictionary for hunspell</da>
   <de>Brasilianisches (portugiesisches) Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Πορτογαλικά (BR) για hunspell</el>
   <en>Portuguese (BR) dictionary for hunspell</en>
   <es_ES>Diccionario Portugués (BR) para hunspell</es_ES>
   <es>Diccionario Portugués (BR) para hunspell</es>
   <et>Portuguese (BR) dictionary for hunspell</et>
   <eu>Portuguese (BR) dictionary for hunspell</eu>
   <fa>Portuguese (BR) dictionary for hunspell</fa>
   <fil_PH>Portuguese (BR) dictionary for hunspell</fil_PH>
   <fi>Portuguese (BR) dictionary for hunspell</fi>
   <fr_BE>Portuguais (Brésil) dictionnaire pour hunspell</fr_BE>
   <fr>Portuguais (Brésil) dictionnaire pour hunspell</fr>
   <gl_ES>Portuguese (BR) dictionary for hunspell</gl_ES>
   <gu>Portuguese (BR) dictionary for hunspell</gu>
   <he_IL>Portuguese (BR) dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु पुर्तगाली (ब्राज़ील) शब्दकोष</hi>
   <hr>Portuguese (BR) dictionary for hunspell</hr>
   <hu>Portuguese (BR) dictionary for hunspell</hu>
   <id>Portuguese (BR) dictionary for hunspell</id>
   <is>Portuguese (BR) dictionary for hunspell</is>
   <it>Dizionario portoghese(BR) per hunspell</it>
   <ja>ブラジル・ポルトガル語用 hunspell</ja>
   <kk>Portuguese (BR) dictionary for hunspell</kk>
   <ko>Portuguese (BR) dictionary for hunspell</ko>
   <ku>Portuguese (BR) dictionary for hunspell</ku>
   <lt>Portuguese (BR) dictionary for hunspell</lt>
   <mk>Portuguese (BR) dictionary for hunspell</mk>
   <mr>Portuguese (BR) dictionary for hunspell</mr>
   <nb_NO>Portuguese (BR) dictionary for hunspell</nb_NO>
   <nb>Brasilsk-portugisisk ordliste for hunspell</nb>
   <nl_BE>Portuguese (BR) dictionary for hunspell</nl_BE>
   <nl>Portuguese (BR) dictionary for hunspell</nl>
   <or>Portuguese (BR) dictionary for hunspell</or>
   <pl>Portuguese (BR) dictionary for hunspell</pl>
   <pt_BR>Dicionário Português (BR) para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Português (BR) para hunspell</pt>
   <ro>Portuguese (BR) dictionary for hunspell</ro>
   <ru>Portuguese (BR) dictionary for hunspell</ru>
   <sk>Portuguese (BR) dictionary for hunspell</sk>
   <sl>Brazilska portugalščina - slovar za hunspell</sl>
   <so>Portuguese (BR) dictionary for hunspell</so>
   <sq>Fjalor portugalisht (BR) për hunspell</sq>
   <sr>Portuguese (BR) dictionary for hunspell</sr>
   <sv>Portugisisk (BR) ordbok för hunspell</sv>
   <th>Portuguese (BR) dictionary for hunspell</th>
   <tr>Hunspell için Brezilya Portekizcesi sözlüğü</tr>
   <uk>Portuguese (BR) dictionary for hunspell</uk>
   <vi>Portuguese (BR) dictionary for hunspell</vi>
   <zh_CN>Portuguese (BR) dictionary for hunspell</zh_CN>
   <zh_HK>Portuguese (BR) dictionary for hunspell</zh_HK>
   <zh_TW>Portuguese (BR) dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
aspell-pt-br
myspell-pt-br
manpages-pt
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
aspell-pt-br
myspell-pt-br
manpages-pt
</uninstall_package_names>

</app>
