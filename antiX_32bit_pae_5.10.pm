<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Kernels
</category>

<name>
Kernel-antiX_32bit_LTS_5.10 pae
</name>

<description>
   <am>antiX Kernel 32 bit  LTS (5.10.197)</am>
   <ar>antiX Kernel 32 bit  LTS (5.10.197)</ar>
   <be>antiX Kernel 32 bit  LTS (5.10.197)</be>
   <bg>antiX Kernel 32 bit  LTS (5.10.197)</bg>
   <bn>antiX Kernel 32 bit  LTS (5.10.197)</bn>
   <ca>Kernel antiX 32 bit LTS (45.10.142-686-àe)</ca>
   <cs>antiX Kernel 32 bit  LTS (5.10.197)</cs>
   <da>antiX Kernel 32 bit  LTS (5.10.197)</da>
   <de>antiX Kernel 32 bit  LTS (5.10.197)</de>
   <el>antiX πυρήνα 32 bit LTS (5.10.197)</el>
   <en>antiX Kernel 32 bit  LTS (5.10.197)</en>
   <es_ES>Kernel antiX 32 bit LTS (5.10.197)</es_ES>
   <es>Kernel antiX 32 bit LTS (5.10.197)</es>
   <et>antiX Kernel 32 bit  LTS (5.10.197)</et>
   <eu>antiX Kernel 32 bit  LTS (5.10.197)</eu>
   <fa>antiX Kernel 32 bit  LTS (5.10.197)</fa>
   <fil_PH>antiX Kernel 32 bit  LTS (5.10.197)</fil_PH>
   <fi>antiX Kernel 32 bit  LTS (5.10.197)</fi>
   <fr_BE>antiX Noyau 32 bit LTS (5.10.197)</fr_BE>
   <fr>antiX Noyau 32 bit LTS (5.10.197)</fr>
   <gl_ES>antiX Kernel 32 bit  LTS (5.10.197)</gl_ES>
   <gu>antiX Kernel 32 bit  LTS (5.10.197)</gu>
   <he_IL>antiX Kernel 32 bit  LTS (5.10.197)</he_IL>
   <hi>antiX Kernel 32 bit  LTS (5.10.197)</hi>
   <hr>antiX Kernel 32 bit  LTS (5.10.197)</hr>
   <hu>antiX Kernel 32 bit  LTS (5.10.197)</hu>
   <id>antiX Kernel 32 bit  LTS (5.10.197)</id>
   <is>antiX Kernel 32 bit  LTS (5.10.197)</is>
   <it>Kernel antiX LTS 32 bit (5.10.197)</it>
   <ja>antiX Kernel 32 bit  LTS (5.10.197)</ja>
   <kk>antiX Kernel 32 bit  LTS (5.10.197)</kk>
   <ko>antiX Kernel 32 bit  LTS (5.10.197)</ko>
   <ku>antiX Kernel 32 bit  LTS (5.10.197)</ku>
   <lt>antiX Kernel 32 bit  LTS (5.10.197)</lt>
   <mk>antiX Kernel 32 bit  LTS (5.10.197)</mk>
   <mr>antiX Kernel 32 bit  LTS (5.10.197)</mr>
   <nb_NO>antiX Kernel 32 bit  LTS (5.10.197)</nb_NO>
   <nb>antiX-kjerne, 32 biters (5.10.197)</nb>
   <nl_BE>antiX Kernel 32 bit  LTS (5.10.197)</nl_BE>
   <nl>antiX Kernel 32 bit LTS (5.10.197)</nl>
   <or>antiX Kernel 32 bit  LTS (5.10.197)</or>
   <pl>antiX Kernel 32 bit  LTS (5.10.197)</pl>
   <pt_BR>Núcleo (Kernel) do antiX 5.10.197 de 32 bits LTS</pt_BR>
   <pt>Núcleo (Kernel) do antiX de 32 bits LTS (5.10.197)</pt>
   <ro>antiX Kernel 32 bit  LTS (5.10.197)</ro>
   <ru>antiX Kernel 32 bit  LTS (5.10.197)</ru>
   <sk>antiX Kernel 32 bit  LTS (5.10.197)</sk>
   <sl>antiX 32 bitno jedro (5.10.197)</sl>
   <so>antiX Kernel 32 bit  LTS (5.10.197)</so>
   <sq>Kernel antiX-i LTS, 32 bit (5.10.197)</sq>
   <sr>antiX Kernel 32 bit  LTS (5.10.197)</sr>
   <sv>antiX Kärna 32 bit LTS (5.10.197)</sv>
   <th>antiX Kernel 32 bit  LTS (5.10.197)</th>
   <tr>antiX Kernel 32 bit  LTS (5.10.197)</tr>
   <uk>antiX Kernel 32 bit  LTS (5.10.197)</uk>
   <vi>antiX Kernel 32 bit  LTS (5.10.197)</vi>
   <zh_CN>antiX Kernel 32 bit  LTS (5.10.197)</zh_CN>
   <zh_HK>antiX Kernel 32 bit  LTS (5.10.197)</zh_HK>
   <zh_TW>antiX Kernel 32 bit  LTS (5.10.197)</zh_TW>
</description>

<installable>
32
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
linux-image-5.10.197-antix.1-686-smp-pae
linux-headers-5.10.197-antix.1-686-smp-pae
libelf-dev
libc6-dev
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-5.10.197-antix.1-686-smp-pae
linux-headers-5.10.197-antix.1-686-smp-pae
</uninstall_package_names>

</app>
