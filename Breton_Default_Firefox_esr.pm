<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Breton_Default_Firefox_esr
</name>

<description>
   <am>Breton localisation of Firefox ESR</am>
   <ar>Breton localisation of Firefox ESR</ar>
   <be>Breton localisation of Firefox ESR</be>
   <bg>Breton localisation of Firefox ESR</bg>
   <bn>Breton localisation of Firefox ESR</bn>
   <ca>Localització en Bretó de Firefox ESR</ca>
   <cs>Breton localisation of Firefox ESR</cs>
   <da>Breton localisation of Firefox ESR</da>
   <de>Breton localisation of Firefox ESR</de>
   <el>Breton για Firefox ESR</el>
   <en>Breton localisation of Firefox ESR</en>
   <es_ES>Localización bretona de Firefox ESR</es_ES>
   <es>Localización bretona de Firefox ESR</es>
   <et>Breton localisation of Firefox ESR</et>
   <eu>Breton localisation of Firefox ESR</eu>
   <fa>Breton localisation of Firefox ESR</fa>
   <fil_PH>Breton localisation of Firefox ESR</fil_PH>
   <fi>Breton localisation of Firefox ESR</fi>
   <fr_BE>Localisation bretonne pour Firefox ESR</fr_BE>
   <fr>Localisation bretonne pour Firefox ESR</fr>
   <gl_ES>Breton localisation of Firefox ESR</gl_ES>
   <gu>Breton localisation of Firefox ESR</gu>
   <he_IL>Breton localisation of Firefox ESR</he_IL>
   <hi>Breton localisation of Firefox ESR</hi>
   <hr>Breton localisation of Firefox ESR</hr>
   <hu>Breton localisation of Firefox ESR</hu>
   <id>Breton localisation of Firefox ESR</id>
   <is>Breton localisation of Firefox ESR</is>
   <it>Localizzazione Breton di Firefox ESR</it>
   <ja>Breton localisation of Firefox ESR</ja>
   <kk>Breton localisation of Firefox ESR</kk>
   <ko>Breton localisation of Firefox ESR</ko>
   <ku>Breton localisation of Firefox ESR</ku>
   <lt>Breton localisation of Firefox ESR</lt>
   <mk>Breton localisation of Firefox ESR</mk>
   <mr>Breton localisation of Firefox ESR</mr>
   <nb_NO>Breton localisation of Firefox ESR</nb_NO>
   <nb>Bretonsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Breton localisation of Firefox ESR</nl_BE>
   <nl>Bretonse lokalisatie van Firefox ESR</nl>
   <or>Breton localisation of Firefox ESR</or>
   <pl>Breton localisation of Firefox ESR</pl>
   <pt_BR>Bretão Localização para o Firefox ESR</pt_BR>
   <pt>Bretão Localização para Firefox ESR</pt>
   <ro>Breton localisation of Firefox ESR</ro>
   <ru>Breton localisation of Firefox ESR</ru>
   <sk>Breton localisation of Firefox ESR</sk>
   <sl>Bretonske krajevne nastavitve za Firefox ESR</sl>
   <so>Breton localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në bretonisht</sq>
   <sr>Breton localisation of Firefox ESR</sr>
   <sv>Bretonsk  lokalisering av Firefox ESR</sv>
   <th>Breton localisation of Firefox ESR</th>
   <tr>Breton localisation of Firefox ESR</tr>
   <uk>Breton localisation of Firefox ESR</uk>
   <vi>Breton localisation of Firefox ESR</vi>
   <zh_CN>Breton localisation of Firefox ESR</zh_CN>
   <zh_HK>Breton localisation of Firefox ESR</zh_HK>
   <zh_TW>Breton localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-br
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-br
</uninstall_package_names>

</app>
