<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
Freeciv
</name>

<description>
   <en>Civilization turn based strategy game</en>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
freeciv
freeciv-server
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
freeciv
freeciv-server
</uninstall_package_names>
</app>
