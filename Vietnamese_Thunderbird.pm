<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Vietnamese_Thunderbird
</name>

<description>
   <am>Vietnamese localisation of Thunderbird</am>
   <ar>Vietnamese localisation of Thunderbird</ar>
   <be>Vietnamese localisation of Thunderbird</be>
   <bg>Vietnamese localisation of Thunderbird</bg>
   <bn>Vietnamese localisation of Thunderbird</bn>
   <ca>Localització en Vietnamita de Thunderbird</ca>
   <cs>Vietnamese localisation of Thunderbird</cs>
   <da>Vietnamese localisation of Thunderbird</da>
   <de>Vietnamesische Lokalisation von “Thunderbird”</de>
   <el>Βιετναμέζικα για Thunderbird</el>
   <en>Vietnamese localisation of Thunderbird</en>
   <es_ES>Localización Vietnamita de Thunderbird</es_ES>
   <es>Localización Vietnamita de Thunderbird</es>
   <et>Vietnamese localisation of Thunderbird</et>
   <eu>Vietnamese localisation of Thunderbird</eu>
   <fa>Vietnamese localisation of Thunderbird</fa>
   <fil_PH>Vietnamese localisation of Thunderbird</fil_PH>
   <fi>Vietnamese localisation of Thunderbird</fi>
   <fr_BE>Localisation en vietnamien pour Thunderbird</fr_BE>
   <fr>Localisation en vietnamien pour Thunderbird</fr>
   <gl_ES>Vietnamese localisation of Thunderbird</gl_ES>
   <gu>Vietnamese localisation of Thunderbird</gu>
   <he_IL>Vietnamese localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का वियतनामी संस्करण</hi>
   <hr>Vietnamese localisation of Thunderbird</hr>
   <hu>Vietnamese localisation of Thunderbird</hu>
   <id>Vietnamese localisation of Thunderbird</id>
   <is>Vietnamese localisation of Thunderbird</is>
   <it>Localizzazione vietnamese di Thunderbird</it>
   <ja>ベトナム語版 Thunderbird</ja>
   <kk>Vietnamese localisation of Thunderbird</kk>
   <ko>Vietnamese localisation of Thunderbird</ko>
   <ku>Vietnamese localisation of Thunderbird</ku>
   <lt>Vietnamese localisation of Thunderbird</lt>
   <mk>Vietnamese localisation of Thunderbird</mk>
   <mr>Vietnamese localisation of Thunderbird</mr>
   <nb_NO>Vietnamese localisation of Thunderbird</nb_NO>
   <nb>Vietnamesisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Vietnamese localisation of Thunderbird</nl_BE>
   <nl>Vietnamese localisation of Thunderbird</nl>
   <or>Vietnamese localisation of Thunderbird</or>
   <pl>Vietnamese localisation of Thunderbird</pl>
   <pt_BR>Vietnamita Localização para o Thunderbird</pt_BR>
   <pt>Vietnamita Localização para Thunderbird</pt>
   <ro>Vietnamese localisation of Thunderbird</ro>
   <ru>Vietnamese localisation of Thunderbird</ru>
   <sk>Vietnamese localisation of Thunderbird</sk>
   <sl>Vietnamske krajevne nastavitve za Thunderbird</sl>
   <so>Vietnamese localisation of Thunderbird</so>
   <sq>Virtualbox</sq>
   <sr>Vietnamese localisation of Thunderbird</sr>
   <sv>Vietnamesisk lokalisering av Thunderbird</sv>
   <th>Vietnamese localisation of Thunderbird</th>
   <tr>Thunderbird Vietnamca yerelleştirmesi</tr>
   <uk>Vietnamese localisation of Thunderbird</uk>
   <vi>Vietnamese localisation of Thunderbird</vi>
   <zh_CN>Vietnamese localisation of Thunderbird</zh_CN>
   <zh_HK>Vietnamese localisation of Thunderbird</zh_HK>
   <zh_TW>Vietnamese localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-vi
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-vi
</uninstall_package_names>

</app>
