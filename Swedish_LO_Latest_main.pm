<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Swedish_LO_Latest_main
</name>

<description>
   <am>Swedish Language Meta-Package for LibreOffice</am>
   <ar>Swedish Language Meta-Package for LibreOffice</ar>
   <be>Swedish Language Meta-Package for LibreOffice</be>
   <bg>Swedish Language Meta-Package for LibreOffice</bg>
   <bn>Swedish Language Meta-Package for LibreOffice</bn>
   <ca>Meta-paquet de llengua en Suec per a LibreOffice</ca>
   <cs>Swedish Language Meta-Package for LibreOffice</cs>
   <da>Svensk sprog-metapakke til LibreOffice</da>
   <de>Schwedisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Σουηδικά</el>
   <en>Swedish Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Idioma Sueco para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Sueco para LibreOffice</es>
   <et>Swedish Language Meta-Package for LibreOffice</et>
   <eu>Swedish Language Meta-Package for LibreOffice</eu>
   <fa>Swedish Language Meta-Package for LibreOffice</fa>
   <fil_PH>Swedish Language Meta-Package for LibreOffice</fil_PH>
   <fi>Ruotsinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue suédoise pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue suédoise pour LibreOffice</fr>
   <gl_ES>Sueco Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Swedish Language Meta-Package for LibreOffice</gu>
   <he_IL>Swedish Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु स्वीडिश भाषा मेटा-पैकेज</hi>
   <hr>Swedish Language Meta-Package for LibreOffice</hr>
   <hu>Swedish Language Meta-Package for LibreOffice</hu>
   <id>Swedish Language Meta-Package for LibreOffice</id>
   <is>Swedish Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua svedese per LibreOffice</it>
   <ja>LibreOffice用のスウェーデン語メタパッケージ</ja>
   <kk>Swedish Language Meta-Package for LibreOffice</kk>
   <ko>Swedish Language Meta-Package for LibreOffice</ko>
   <ku>Swedish Language Meta-Package for LibreOffice</ku>
   <lt>Swedish Language Meta-Package for LibreOffice</lt>
   <mk>Swedish Language Meta-Package for LibreOffice</mk>
   <mr>Swedish Language Meta-Package for LibreOffice</mr>
   <nb_NO>Swedish Language Meta-Package for LibreOffice</nb_NO>
   <nb>Svensk lokaltilpassing av LibreOffice</nb>
   <nl_BE>Swedish Language Meta-Package for LibreOffice</nl_BE>
   <nl>Zweedse Taal Meta-Pakket voor LibreOffice</nl>
   <or>Swedish Language Meta-Package for LibreOffice</or>
   <pl>Szwedzki metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Sueco Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Sueco Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Swedish Language Meta-Package for LibreOffice</ro>
   <ru>Swedish Language Meta-Package for LibreOffice</ru>
   <sk>Swedish Language Meta-Package for LibreOffice</sk>
   <sl>Švedski jezikovni meta-paket za LibreOffice</sl>
   <so>Swedish Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në suedisht</sq>
   <sr>Swedish Language Meta-Package for LibreOffice</sr>
   <sv>Svenskt Språk-Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Swedish สำหรับ LibreOffice</th>
   <tr>LibreOffice için İsveççe Dili Üst-Paketi</tr>
   <uk>Swedish Language Meta-Package for LibreOffice</uk>
   <vi>Swedish Language Meta-Package for LibreOffice</vi>
   <zh_CN>Swedish Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Swedish Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Swedish Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-sv
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-sv
libreoffice-gtk3
</uninstall_package_names>

</app>
