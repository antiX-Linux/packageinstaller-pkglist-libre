<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Songhai_Default_Firefox_esr
</name>

<description>
   <am>Songhai localisation of Firefox ESR</am>
   <ar>Songhai localisation of Firefox ESR</ar>
   <be>Songhai localisation of Firefox ESR</be>
   <bg>Songhai localisation of Firefox ESR</bg>
   <bn>Songhai localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Songhai</ca>
   <cs>Songhai localisation of Firefox ESR</cs>
   <da>Songhai localisation of Firefox ESR</da>
   <de>Afrikanische Lokalisation für die Songhai-Sprachen von “Firefox ESR”</de>
   <el>Songhai για Firefox ESR</el>
   <en>Songhai localisation of Firefox ESR</en>
   <es_ES>Localización Songhai de Firefox ESR</es_ES>
   <es>Localización Songhai de Firefox ESR</es>
   <et>Songhai localisation of Firefox ESR</et>
   <eu>Songhai localisation of Firefox ESR</eu>
   <fa>Songhai localisation of Firefox ESR</fa>
   <fil_PH>Songhai localisation of Firefox ESR</fil_PH>
   <fi>Songhai localisation of Firefox ESR</fi>
   <fr_BE>Localisation en Songhaï (Afrique de l'ouest) pour Firefox ESR</fr_BE>
   <fr>Localisation en Songhaï (Afrique de l'ouest) pour Firefox ESR</fr>
   <gl_ES>Songhai localisation of Firefox ESR</gl_ES>
   <gu>Songhai localisation of Firefox ESR</gu>
   <he_IL>Songhai localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का सांघाई संस्करण</hi>
   <hr>Songhai localisation of Firefox ESR</hr>
   <hu>Songhai localisation of Firefox ESR</hu>
   <id>Songhai localisation of Firefox ESR</id>
   <is>Songhai localisation of Firefox ESR</is>
   <it>Localizzazione songhai di Firefox ESR</it>
   <ja>ソンガイ語版 Firefox ESR</ja>
   <kk>Songhai localisation of Firefox ESR</kk>
   <ko>Songhai localisation of Firefox ESR</ko>
   <ku>Songhai localisation of Firefox ESR</ku>
   <lt>Songhai localisation of Firefox ESR</lt>
   <mk>Songhai localisation of Firefox ESR</mk>
   <mr>Songhai localisation of Firefox ESR</mr>
   <nb_NO>Songhai localisation of Firefox ESR</nb_NO>
   <nb>Songhai lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Songhai localisation of Firefox ESR</nl_BE>
   <nl>Songhai localisation of Firefox ESR</nl>
   <or>Songhai localisation of Firefox ESR</or>
   <pl>Songhai localisation of Firefox ESR</pl>
   <pt_BR>Songai Localização para o Firefox ESR</pt_BR>
   <pt>Songai Localização para Firefox ESR</pt>
   <ro>Songhai localisation of Firefox ESR</ro>
   <ru>Songhai localisation of Firefox ESR</ru>
   <sk>Songhai localisation of Firefox ESR</sk>
   <sl>Songajske krajevne nastavitve za Firefox ESR</sl>
   <so>Songhai localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në songhai</sq>
   <sr>Songhai localisation of Firefox ESR</sr>
   <sv>Songhai lokalisering av Firefox ESR</sv>
   <th>Songhai localisation of Firefox ESR</th>
   <tr>Firefox ESR Songhayca yerelleştirmesi</tr>
   <uk>Songhai localisation of Firefox ESR</uk>
   <vi>Songhai localisation of Firefox ESR</vi>
   <zh_CN>Songhai localisation of Firefox ESR</zh_CN>
   <zh_HK>Songhai localisation of Firefox ESR</zh_HK>
   <zh_TW>Songhai localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-esr-l10n-son
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-son
</uninstall_package_names>

</app>
