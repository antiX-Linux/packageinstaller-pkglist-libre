<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Modern Greek
</name>

<description>
   <am>Greek dictionary for hunspell</am>
   <ar>Greek dictionary for hunspell</ar>
   <be>Greek dictionary for hunspell</be>
   <bg>Greek dictionary for hunspell</bg>
   <bn>Greek dictionary for hunspell</bn>
   <ca>Diccionari Grec per hunspell</ca>
   <cs>Greek dictionary for hunspell</cs>
   <da>Greek dictionary for hunspell</da>
   <de>Griechisches Wörterbuch für “Hunspell”</de>
   <el>Ελληνικό λεξικό για hunspell</el>
   <en>Greek dictionary for hunspell</en>
   <es_ES>Diccionario Griego para hunspell</es_ES>
   <es>Diccionario Griego para hunspell</es>
   <et>Greek dictionary for hunspell</et>
   <eu>Greek dictionary for hunspell</eu>
   <fa>Greek dictionary for hunspell</fa>
   <fil_PH>Greek dictionary for hunspell</fil_PH>
   <fi>Greek dictionary for hunspell</fi>
   <fr_BE>Grec dictionnaire pour hunspell</fr_BE>
   <fr>Grec dictionnaire pour hunspell</fr>
   <gl_ES>Greek dictionary for hunspell</gl_ES>
   <gu>Greek dictionary for hunspell</gu>
   <he_IL>Greek dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु यूनानी शब्दकोष</hi>
   <hr>Greek dictionary for hunspell</hr>
   <hu>Greek dictionary for hunspell</hu>
   <id>Greek dictionary for hunspell</id>
   <is>Greek dictionary for hunspell</is>
   <it>Dizionario greco per hunspell</it>
   <ja>Hunspell 用ギリシャ語辞書</ja>
   <kk>Greek dictionary for hunspell</kk>
   <ko>Greek dictionary for hunspell</ko>
   <ku>Greek dictionary for hunspell</ku>
   <lt>Greek dictionary for hunspell</lt>
   <mk>Greek dictionary for hunspell</mk>
   <mr>Greek dictionary for hunspell</mr>
   <nb_NO>Greek dictionary for hunspell</nb_NO>
   <nb>Gresk ordliste for hunspell</nb>
   <nl_BE>Greek dictionary for hunspell</nl_BE>
   <nl>Greek dictionary for hunspell</nl>
   <or>Greek dictionary for hunspell</or>
   <pl>Greek dictionary for hunspell</pl>
   <pt_BR>Dicionário Grego para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Grego para hunspell</pt>
   <ro>Greek dictionary for hunspell</ro>
   <ru>Greek dictionary for hunspell</ru>
   <sk>Greek dictionary for hunspell</sk>
   <sl>Grški slovar za hunspell</sl>
   <so>Greek dictionary for hunspell</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në greqisht</sq>
   <sr>Greek dictionary for hunspell</sr>
   <sv>Grekisk ordbok för hunspell</sv>
   <th>Greek dictionary for hunspell</th>
   <tr>Hunspell için Yunanca sözlük</tr>
   <uk>Greek dictionary for hunspell</uk>
   <vi>Greek dictionary for hunspell</vi>
   <zh_CN>Greek dictionary for hunspell</zh_CN>
   <zh_HK>Greek dictionary for hunspell</zh_HK>
   <zh_TW>Greek dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-el
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-el
</uninstall_package_names>

</app>
