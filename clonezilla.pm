<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Utility
</category>

<name>
clonezilla
</name>

<description>
   <am>clone and restore disks and partitions</am>
   <ar>clone and restore disks and partitions</ar>
   <be>clone and restore disks and partitions</be>
   <bg>clone and restore disks and partitions</bg>
   <bn>clone and restore disks and partitions</bn>
   <ca>Clona i restaura discos i particions</ca>
   <cs>clone and restore disks and partitions</cs>
   <da>klon og gendan diske og partitioner</da>
   <de>Laufwerke und Partitonen klonen und wieder herstellen.</de>
   <el>Κλωνοποίηση και επαναφορά δίσκων και διαμερισμάτων</el>
   <en>clone and restore disks and partitions</en>
   <es_ES>Clona y restaura discos y particiones</es_ES>
   <es>Clona y restaura discos y particiones</es>
   <et>clone and restore disks and partitions</et>
   <eu>clone and restore disks and partitions</eu>
   <fa>clone and restore disks and partitions</fa>
   <fil_PH>clone and restore disks and partitions</fil_PH>
   <fi>kloonaa ja palauta levyjä ja osioita</fi>
   <fr_BE>Copie et restauration de disques et de partitions</fr_BE>
   <fr>Copie et restauration de disques et de partitions</fr>
   <gl_ES>Clonar e restaurar discos e particións</gl_ES>
   <gu>clone and restore disks and partitions</gu>
   <he_IL>clone and restore disks and partitions</he_IL>
   <hi>डिस्क व विभाजन हेतु प्रतिरूपण व पुनः स्थापना</hi>
   <hr>clone and restore disks and partitions</hr>
   <hu>clone and restore disks and partitions</hu>
   <id>clone and restore disks and partitions</id>
   <is>clone and restore disks and partitions</is>
   <it>clona e ripristina dischi e partizioni</it>
   <ja>ディスクとパーティションのクローンと復元</ja>
   <kk>clone and restore disks and partitions</kk>
   <ko>clone and restore disks and partitions</ko>
   <ku>clone and restore disks and partitions</ku>
   <lt>clone and restore disks and partitions</lt>
   <mk>clone and restore disks and partitions</mk>
   <mr>clone and restore disks and partitions</mr>
   <nb_NO>clone and restore disks and partitions</nb_NO>
   <nb>klon og gjenopprett disker og partisjoner</nb>
   <nl_BE>clone and restore disks and partitions</nl_BE>
   <nl>kloon en herstel schijven en partities</nl>
   <or>clone and restore disks and partitions</or>
   <pl>klonowanie i przywracanie dysków i partycji</pl>
   <pt_BR>Clonar e restaurar discos e partições</pt_BR>
   <pt>Clonar e restaurar discos e partições</pt>
   <ro>clone and restore disks and partitions</ro>
   <ru>Клонирование и восстановление дисков и разделов</ru>
   <sk>clone and restore disks and partitions</sk>
   <sl>kloniranje in obnova diskov in razdelkov</sl>
   <so>clone and restore disks and partitions</so>
   <sq>Klononi dhe riktheni disqe dhe pjesë disqesh</sq>
   <sr>clone and restore disks and partitions</sr>
   <sv>klona och återställ diskar och partitioner</sv>
   <th>clone and restore disks and partitions</th>
   <tr>diskleri ve bölümleri klonlama ve geri yükleme</tr>
   <uk>клонування та відновлення дисків і розділів</uk>
   <vi>clone and restore disks and partitions</vi>
   <zh_CN>克隆或恢复磁盘及分区</zh_CN>
   <zh_HK>clone and restore disks and partitions</zh_HK>
   <zh_TW>clone and restore disks and partitions</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
clonezilla
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
clonezilla
</uninstall_package_names>
</app>
