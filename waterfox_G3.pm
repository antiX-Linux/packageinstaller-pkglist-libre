<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Waterfox G3
</name>

<description>
   <am>Alternative mozilla-based browser with all WebExtensions</am>
   <ar>Alternative mozilla-based browser with all WebExtensions</ar>
   <be>Alternative mozilla-based browser with all WebExtensions</be>
   <bg>Alternative mozilla-based browser with all WebExtensions</bg>
   <bn>Alternative mozilla-based browser with all WebExtensions</bn>
   <ca>Navegador alternatiu basat en Mozilla amb totes les extensions de web</ca>
   <cs>Alternative mozilla-based browser with all WebExtensions</cs>
   <da>Alternative mozilla-based browser with all WebExtensions</da>
   <de>Alternativer Mozilla-basierter Browser mit allen Browsererweiterungen</de>
   <el>Εναλλακτικό πρόγραμμα περιήγησης που βασίζεται σε mozilla με όλες τις επεκτάσεις Web</el>
   <en>Alternative mozilla-based browser with all WebExtensions</en>
   <es_ES>Navegador alternativo basado en mozilla con todas las extensiones web</es_ES>
   <es>Navegador alternativo basado en mozilla con todas las extensiones web</es>
   <et>Alternative mozilla-based browser with all WebExtensions</et>
   <eu>Alternative mozilla-based browser with all WebExtensions</eu>
   <fa>Alternative mozilla-based browser with all WebExtensions</fa>
   <fil_PH>Alternative mozilla-based browser with all WebExtensions</fil_PH>
   <fi>Vaihtoehtoinen Mozilla-pohjainen Internet.selain kaikilla WebExtensions-paketeilla</fi>
   <fr_BE>Navigateur alternatif basé sur Mozilla avec toutes les WebExtensions</fr_BE>
   <fr>Navigateur alternatif basé sur Mozilla avec toutes les WebExtensions</fr>
   <gl_ES>Alternative mozilla-based browser with all WebExtensions</gl_ES>
   <gu>Alternative mozilla-based browser with all WebExtensions</gu>
   <he_IL>Alternative mozilla-based browser with all WebExtensions</he_IL>
   <hi>सभी वेब एक्सटेंशन युक्त एक वैकल्पिक मोज़िला-आधारित ब्राउज़र</hi>
   <hr>Alternative mozilla-based browser with all WebExtensions</hr>
   <hu>Alternative mozilla-based browser with all WebExtensions</hu>
   <id>Alternative mozilla-based browser with all WebExtensions</id>
   <is>Alternative mozilla-based browser with all WebExtensions</is>
   <it>Browser alternativo basato su mozilla con tutte le WebExtensions</it>
   <ja>すべてのWeb拡張機能を備えたモジラベースの代替ブラウザ</ja>
   <kk>Alternative mozilla-based browser with all WebExtensions</kk>
   <ko>Alternative mozilla-based browser with all WebExtensions</ko>
   <ku>Alternative mozilla-based browser with all WebExtensions</ku>
   <lt>Alternative mozilla-based browser with all WebExtensions</lt>
   <mk>Alternative mozilla-based browser with all WebExtensions</mk>
   <mr>Alternative mozilla-based browser with all WebExtensions</mr>
   <nb_NO>Alternative mozilla-based browser with all WebExtensions</nb_NO>
   <nb>Alternativ mozilla-basert nettleser med alle WebExtensions</nb>
   <nl_BE>Alternative mozilla-based browser with all WebExtensions</nl_BE>
   <nl>Alternative mozilla-based browser with all WebExtensions</nl>
   <or>Alternative mozilla-based browser with all WebExtensions</or>
   <pl>Alternative mozilla-based browser with all WebExtensions</pl>
   <pt_BR>Navegador de internet alternativo baseado no Mozilla com todas as WebExtensions</pt_BR>
   <pt>Navegador de Internet alternativo, baseado no mozilla, com todas as WebExtensions</pt>
   <ro>Alternative mozilla-based browser with all WebExtensions</ro>
   <ru>Alternative mozilla-based browser with all WebExtensions</ru>
   <sk>Alternative mozilla-based browser with all WebExtensions</sk>
   <sl>Alternativni na Mozilli temelječ brskalnik z vsemi spletnimi razširitvami WebExtensions</sl>
   <so>Alternative mozilla-based browser with all WebExtensions</so>
   <sq>Përkthimi i Firefox-it ESR në uellsisht</sq>
   <sr>Alternative mozilla-based browser with all WebExtensions</sr>
   <sv>Alternativ mozilla-baserad webbläsare med alla WebExtensions</sv>
   <th>Alternative mozilla-based browser with all WebExtensions</th>
   <tr>Tüm Web Uzantıları ile seçenek bir mozilla tabanlı tarayıcı</tr>
   <uk>Alternative mozilla-based browser with all WebExtensions</uk>
   <vi>Alternative mozilla-based browser with all WebExtensions</vi>
   <zh_CN>Alternative mozilla-based browser with all WebExtensions</zh_CN>
   <zh_HK>Alternative mozilla-based browser with all WebExtensions</zh_HK>
   <zh_TW>Alternative mozilla-based browser with all WebExtensions</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>https://www.waterfoxproject.org/media/img/waterfox/products/desktop/waterfox-browser.40990c516643.svg</screenshot>

<preinstall>
</preinstall>

<install_package_names>
waterfox-g3-kpe
</install_package_names>


<postinstall>
if [ "$(locale |grep LANG|cut -d= -f2 |cut -d_ -f1)" != "en" ]; then
apt-get install waterfox-g3-i18n-$(locale |grep LANG|cut -d= -f2 |cut -d_ -f1)
fi
</postinstall>


<uninstall_package_names>
waterfox-g3-kpe
</uninstall_package_names>
</app>
