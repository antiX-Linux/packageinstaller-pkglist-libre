<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Scottish_LO_Latest_full
</name>

<description>
   <am>Scottish_gaelic Language Meta-Package for LibreOffice</am>
   <ar>Scottish_gaelic Language Meta-Package for LibreOffice</ar>
   <be>Scottish_gaelic Language Meta-Package for LibreOffice</be>
   <bg>Scottish_gaelic Language Meta-Package for LibreOffice</bg>
   <bn>Scottish_gaelic Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Gaèlic Escocès per LibreOffice</ca>
   <cs>Scottish_gaelic Language Meta-Package for LibreOffice</cs>
   <da>Scottish_gaelic Language Meta-Package for LibreOffice</da>
   <de>Schottisch-gälisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Scottish Gaelic</el>
   <en>Scottish_gaelic Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Gaélico Escocés para LibreOffice</es_ES>
   <es>Metapaquete de idioma Gallego Escocés para LibreOffice</es>
   <et>Scottish_gaelic Language Meta-Package for LibreOffice</et>
   <eu>Scottish_gaelic Language Meta-Package for LibreOffice</eu>
   <fa>Scottish_gaelic Language Meta-Package for LibreOffice</fa>
   <fil_PH>Scottish_gaelic Language Meta-Package for LibreOffice</fil_PH>
   <fi>Skotlantilais-gaelilainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue gaélique (Ecosse) pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue gaélique (Ecosse) pour LibreOffice</fr>
   <gl_ES>Gaélico-escocés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Scottish_gaelic Language Meta-Package for LibreOffice</gu>
   <he_IL>Scottish_gaelic Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु स्कॉटिश_गेलिक भाषा मेटा-पैकेज</hi>
   <hr>Scottish_gaelic Language Meta-Package for LibreOffice</hr>
   <hu>Scottish_gaelic Language Meta-Package for LibreOffice</hu>
   <id>Scottish_gaelic Language Meta-Package for LibreOffice</id>
   <is>Scottish_gaelic Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua gaelica scozzese per LibreOffice</it>
   <ja>LibreOffice用の高地スコットランド・ゲール語メタパッケージ</ja>
   <kk>Scottish_gaelic Language Meta-Package for LibreOffice</kk>
   <ko>Scottish_gaelic Language Meta-Package for LibreOffice</ko>
   <ku>Scottish_gaelic Language Meta-Package for LibreOffice</ku>
   <lt>Scottish_gaelic Language Meta-Package for LibreOffice</lt>
   <mk>Scottish_gaelic Language Meta-Package for LibreOffice</mk>
   <mr>Scottish_gaelic Language Meta-Package for LibreOffice</mr>
   <nb_NO>Scottish_gaelic Language Meta-Package for LibreOffice</nb_NO>
   <nb>Skotsk-gælisk språkpakke for LibreOffice</nb>
   <nl_BE>Scottish_gaelic Language Meta-Package for LibreOffice</nl_BE>
   <nl>Schots-Gaelic Taal Meta-Pakket voor LibreOffice</nl>
   <or>Scottish_gaelic Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka gaelickiego szkockiego dla LibreOffice</pl>
   <pt_BR>Gaélico Escocês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Gaélico_escocês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Scottish_gaelic Language Meta-Package for LibreOffice</ro>
   <ru>Scottish_gaelic Language Meta-Package for LibreOffice</ru>
   <sk>Scottish_gaelic Language Meta-Package for LibreOffice</sk>
   <sl>Škotsko gelške jezikovni meta-paket za LibreOffice</sl>
   <so>Scottish_gaelic Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në gaelike skoceze</sq>
   <sr>Scottish_gaelic Language Meta-Package for LibreOffice</sr>
   <sv>Skottsk_gaelic Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Scottish_gaelic สำหรับ LibreOffice</th>
   <tr>LibreOffice için İskoç Dili Üst-Paketi</tr>
   <uk>Scottish_gaelic Language Meta-Package for LibreOffice</uk>
   <vi>Scottish_gaelic Language Meta-Package for LibreOffice</vi>
   <zh_CN>Scottish_gaelic Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Scottish_gaelic Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Scottish_gaelic Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-gd
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-gd
</uninstall_package_names>

</app>
