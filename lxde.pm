<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
LXDE
</name>

<description>
   <am>basic install of LXDE</am>
   <ar>basic install of LXDE</ar>
   <be>basic install of LXDE</be>
   <bg>basic install of LXDE</bg>
   <bn>basic install of LXDE</bn>
   <ca>Instal·lació bàsica de LXDE</ca>
   <cs>basic install of LXDE</cs>
   <da>grundlæggende installation af LXDE</da>
   <de>Grundinstallation von LXDE</de>
   <el>Βασική εγκατάσταση του LXDE</el>
   <en>basic install of LXDE</en>
   <es_ES>Instalación mínima de LXDE</es_ES>
   <es>Instalación mínima de LXDE</es>
   <et>basic install of LXDE</et>
   <eu>basic install of LXDE</eu>
   <fa>basic install of LXDE</fa>
   <fil_PH>basic install of LXDE</fil_PH>
   <fi>LXDE-työpöytäympäristön perusasennus</fi>
   <fr_BE>Installation de base du bureau LXDE</fr_BE>
   <fr>Installation de base du bureau LXDE</fr>
   <gl_ES>Instalación básica do Ambiente de Traballo LXDE</gl_ES>
   <gu>basic install of LXDE</gu>
   <he_IL>התקנה בסיסית של LXDE</he_IL>
   <hi>एलएक्सडीई का सामान्य इंस्टॉल</hi>
   <hr>basic install of LXDE</hr>
   <hu>basic install of LXDE</hu>
   <id>basic install of LXDE</id>
   <is>basic install of LXDE</is>
   <it>installazione base di LXDE</it>
   <ja>LXDE の基本的なインストール</ja>
   <kk>basic install of LXDE</kk>
   <ko>basic install of LXDE</ko>
   <ku>basic install of LXDE</ku>
   <lt>basic install of LXDE</lt>
   <mk>basic install of LXDE</mk>
   <mr>basic install of LXDE</mr>
   <nb_NO>basic install of LXDE</nb_NO>
   <nb>grunnleggende installasjon av LXDE</nb>
   <nl_BE>basic install of LXDE</nl_BE>
   <nl>basisinstallatie van LXDE</nl>
   <or>basic install of LXDE</or>
   <pl>podstawowa instalacja LXDE</pl>
   <pt_BR>Instalação Básica do Ambiente de Trabalho LXDE</pt_BR>
   <pt>Instalação básica do Ambiente de Trabalho LXDE</pt>
   <ro>basic install of LXDE</ro>
   <ru>Базовая установка LXDE</ru>
   <sk>basic install of LXDE</sk>
   <sl>Osnovna namestitev LXDE</sl>
   <so>basic install of LXDE</so>
   <sq>Instalim elementar i LXDE-së</sq>
   <sr>basic install of LXDE</sr>
   <sv>enkel installation av LXDE</sv>
   <th>การติดตั้งพื้นฐานของ LXDE</th>
   <tr>LXDE temel kurulumu</tr>
   <uk>базовий набір стільниці LXDE</uk>
   <vi>basic install of LXDE</vi>
   <zh_CN>basic install of LXDE</zh_CN>
   <zh_HK>basic install of LXDE</zh_HK>
   <zh_TW>basic install of LXDE</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
lxde
consolekit
libpam-ck-connector
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
lxde
consolekit
libpam-ck-connector
</uninstall_package_names>
</app>
