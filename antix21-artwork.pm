<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Wallpapers
</category>

<name>
antiX 21 Wallpapers
</name>

<description>
   <am>backgrounds originally supplied with antiX 21</am>
   <ar>backgrounds originally supplied with antiX 21</ar>
   <bg>backgrounds originally supplied with antiX 21</bg>
   <bn>backgrounds originally supplied with antiX 21</bn>
   <ca>Fons de pantalla subministrats amb antiX 21</ca>
   <cs>backgrounds originally supplied with antiX 21</cs>
   <da>baggrunde fra antiX 21</da>
   <de>Bildschirmhintergründe, die ursprünglich mit antiX 21 geliefert wurden</de>
   <el>επιφάνεια εργασίας που παρέχεται αρχικά με antiX 21</el>
   <en>backgrounds originally supplied with antiX 21</en>
   <es>Fondos de escritorio suministrados originalmente con antiX 21</es>
   <et>backgrounds originally supplied with antiX 21</et>
   <eu>backgrounds originally supplied with antiX 21</eu>
   <fa>backgrounds originally supplied with antiX 21</fa>
   <fil_PH>backgrounds originally supplied with antiX 21</fil_PH>
   <fi>backgrounds originally supplied with antiX 21</fi>
   <fr>Fonds d'écran initialement fournis avec antiX 21</fr>
   <he_IL>backgrounds originally supplied with antiX 21</he_IL>
   <hi>backgrounds originally supplied with antiX 21</hi>
   <hr>backgrounds originally supplied with antiX 21</hr>
   <hu>backgrounds originally supplied with antiX 21</hu>
   <id>backgrounds originally supplied with antiX 21</id>
   <is>backgrounds originally supplied with antiX 21</is>
   <it>sfondi originariamente forniti con antiX 21</it>
   <ja_JP>backgrounds originally supplied with antiX 21</ja_JP>
   <ja>backgrounds originally supplied with antiX 21</ja>
   <kk>backgrounds originally supplied with antiX 21</kk>
   <ko>backgrounds originally supplied with antiX 21</ko>
   <lt>backgrounds originally supplied with antiX 21</lt>
   <mk>backgrounds originally supplied with antiX 21</mk>
   <mr>backgrounds originally supplied with antiX 21</mr>
   <nb>backgrounds originally supplied with antiX 21</nb>
   <nl>achtergronden oorspronkelijk geleverd met antiX 21</nl>
   <pl>tła oryginalnie dostarczone z antiX 21</pl>
   <pt_BR>Imagens de fundo originalmente incluídas no antiX 21</pt_BR>
   <pt>Imagens de fundo originalmente incluídas no antiX 21</pt>
   <ro>backgrounds originally supplied with antiX 21</ro>
   <ru>Обои первоначально входившие в antiX 21</ru>
   <sk>backgrounds originally supplied with antiX 21</sk>
   <sl>Izvirna ozadja za antiX 21</sl>
   <sq>backgrounds originally supplied with antiX 21</sq>
   <sr>backgrounds originally supplied with antiX 21</sr>
   <sv>wallpapers ursprungligen medföljande antiX 21</sv>
   <tr>backgrounds originally supplied with antiX 21</tr>
   <uk>backgrounds originally supplied with antiX 21</uk>
   <vi>backgrounds originally supplied with antiX 21</vi>
   <zh_CN>backgrounds originally supplied with antiX 21</zh_CN>
   <zh_TW>backgrounds originally supplied with antiX 21</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
artwork21-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
artwork21-antix
</uninstall_package_names>
</app>
