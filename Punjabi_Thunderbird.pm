<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Punjabi_Thunderbird
</name>

<description>
   <am>Punjabi localisation of Thunderbird</am>
   <ar>Punjabi localisation of Thunderbird</ar>
   <be>Punjabi localisation of Thunderbird</be>
   <bg>Punjabi localisation of Thunderbird</bg>
   <bn>Punjabi localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Punjabí</ca>
   <cs>Punjabi localisation of Thunderbird</cs>
   <da>Punjabi localisation of Thunderbird</da>
   <de>Pakistanische und Indische Lokalisation für die Sprache “Pandschabisch” von “Thunderbird”</de>
   <el>Εντοπισμός του Thunderbird στα Παντζάμπι</el>
   <en>Punjabi localisation of Thunderbird</en>
   <es_ES>Localización Punjabi de Thunderbird</es_ES>
   <es>Localización Punjabi de Thunderbird</es>
   <et>Punjabi localisation of Thunderbird</et>
   <eu>Punjabi localisation of Thunderbird</eu>
   <fa>Punjabi localisation of Thunderbird</fa>
   <fil_PH>Punjabi localisation of Thunderbird</fil_PH>
   <fi>Punjabi localisation of Thunderbird</fi>
   <fr_BE>Localisation en pendjabi (Inde) pour Thunderbird</fr_BE>
   <fr>Localisation en pendjabi (Inde) pour Thunderbird</fr>
   <gl_ES>Punjabi localisation of Thunderbird</gl_ES>
   <gu>Punjabi localisation of Thunderbird</gu>
   <he_IL>Punjabi localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का पंजाबी संस्करण</hi>
   <hr>Punjabi localisation of Thunderbird</hr>
   <hu>Punjabi localisation of Thunderbird</hu>
   <id>Punjabi localisation of Thunderbird</id>
   <is>Punjabi localisation of Thunderbird</is>
   <it>Localizzazione punjabi di Thunderbird</it>
   <ja>パンジャーブ語版 Thunderbird</ja>
   <kk>Punjabi localisation of Thunderbird</kk>
   <ko>Punjabi localisation of Thunderbird</ko>
   <ku>Punjabi localisation of Thunderbird</ku>
   <lt>Punjabi localisation of Thunderbird</lt>
   <mk>Punjabi localisation of Thunderbird</mk>
   <mr>Punjabi localisation of Thunderbird</mr>
   <nb_NO>Punjabi localisation of Thunderbird</nb_NO>
   <nb>Punjabi lokaltilpassing av Thunderbird</nb>
   <nl_BE>Punjabi localisation of Thunderbird</nl_BE>
   <nl>Punjabi localisation of Thunderbird</nl>
   <or>Punjabi localisation of Thunderbird</or>
   <pl>Punjabi localisation of Thunderbird</pl>
   <pt_BR>Punjabi Localização para o Thunderbird</pt_BR>
   <pt>Panjabi Localização para Thunderbird</pt>
   <ro>Punjabi localisation of Thunderbird</ro>
   <ru>Punjabi localisation of Thunderbird</ru>
   <sk>Punjabi localisation of Thunderbird</sk>
   <sl>Pundžabi krajevne nastavitve za Thunderbird</sl>
   <so>Punjabi localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në panxhabi</sq>
   <sr>Punjabi localisation of Thunderbird</sr>
   <sv>Punjabi lokalisering av Thunderbird</sv>
   <th>Punjabi localisation of Thunderbird</th>
   <tr>Thunderbird'ün Pencapça yerelleştirmesi</tr>
   <uk>Punjabi localisation of Thunderbird</uk>
   <vi>Punjabi localisation of Thunderbird</vi>
   <zh_CN>Punjabi localisation of Thunderbird</zh_CN>
   <zh_HK>Punjabi localisation of Thunderbird</zh_HK>
   <zh_TW>Punjabi localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-pa-in
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-pa-in
</uninstall_package_names>

</app>
