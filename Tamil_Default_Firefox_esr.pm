<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Tamil_Default_Firefox_esr
</name>

<description>
   <am>Tamil localisation of Firefox ESR</am>
   <ar>Tamil localisation of Firefox ESR</ar>
   <be>Tamil localisation of Firefox ESR</be>
   <bg>Tamil localisation of Firefox ESR</bg>
   <bn>Tamil localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Tàmil</ca>
   <cs>Tamil localisation of Firefox ESR</cs>
   <da>Tamil localisation of Firefox ESR</da>
   <de>Indische Lokalisation für die tamilische Sprache von “Firefox ESR”</de>
   <el>Ταμίλ για Firefox ESR</el>
   <en>Tamil localisation of Firefox ESR</en>
   <es_ES>Localización Tamil de Firefox ESR</es_ES>
   <es>Localización Tamil de Firefox ESR</es>
   <et>Tamil localisation of Firefox ESR</et>
   <eu>Tamil localisation of Firefox ESR</eu>
   <fa>Tamil localisation of Firefox ESR</fa>
   <fil_PH>Tamil localisation of Firefox ESR</fil_PH>
   <fi>Tamil localisation of Firefox ESR</fi>
   <fr_BE>Localisation en tamoul (Inde) pour Firefox ESR</fr_BE>
   <fr>Localisation en tamoul (Inde) pour Firefox ESR</fr>
   <gl_ES>Tamil localisation of Firefox ESR</gl_ES>
   <gu>Tamil localisation of Firefox ESR</gu>
   <he_IL>Tamil localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का तमिल संस्करण</hi>
   <hr>Tamil localisation of Firefox ESR</hr>
   <hu>Tamil localisation of Firefox ESR</hu>
   <id>Tamil localisation of Firefox ESR</id>
   <is>Tamil localisation of Firefox ESR</is>
   <it>Localizzazione tamil di Firefox ESR</it>
   <ja>タミル語版 Firefox ESR</ja>
   <kk>Tamil localisation of Firefox ESR</kk>
   <ko>Tamil localisation of Firefox ESR</ko>
   <ku>Tamil localisation of Firefox ESR</ku>
   <lt>Tamil localisation of Firefox ESR</lt>
   <mk>Tamil localisation of Firefox ESR</mk>
   <mr>Tamil localisation of Firefox ESR</mr>
   <nb_NO>Tamil localisation of Firefox ESR</nb_NO>
   <nb>Tamilsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Tamil localisation of Firefox ESR</nl_BE>
   <nl>Tamil localisation of Firefox ESR</nl>
   <or>Tamil localisation of Firefox ESR</or>
   <pl>Tamil localisation of Firefox ESR</pl>
   <pt_BR>Tamil Localização para o Firefox ESR</pt_BR>
   <pt>Tamil Localização para Firefox ESR</pt>
   <ro>Tamil localisation of Firefox ESR</ro>
   <ru>Tamil localisation of Firefox ESR</ru>
   <sk>Tamil localisation of Firefox ESR</sk>
   <sl>Tamilske krajevne nastavitve za Firefox ESR</sl>
   <so>Tamil localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në tamile</sq>
   <sr>Tamil localisation of Firefox ESR</sr>
   <sv>Tamil lokalisering av Firefox ESR</sv>
   <th>Tamil localisation of Firefox ESR</th>
   <tr>Firefox ESR Tamilce yerelleştirmesi</tr>
   <uk>Tamil localisation of Firefox ESR</uk>
   <vi>Tamil localisation of Firefox ESR</vi>
   <zh_CN>Tamil localisation of Firefox ESR</zh_CN>
   <zh_HK>Tamil localisation of Firefox ESR</zh_HK>
   <zh_TW>Tamil localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ta
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ta
</uninstall_package_names>

</app>
