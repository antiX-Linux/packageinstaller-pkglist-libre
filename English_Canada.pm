<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English (Canada)
</name>

<description>
   <am>English (Canadian) dictionary for hunspell</am>
   <ar>English (Canadian) dictionary for hunspell</ar>
   <be>English (Canadian) dictionary for hunspell</be>
   <bg>English (Canadian) dictionary for hunspell</bg>
   <bn>English (Canadian) dictionary for hunspell</bn>
   <ca>Diccionari Anglès (Canadenc) per hunspell</ca>
   <cs>English (Canadian) dictionary for hunspell</cs>
   <da>English (Canadian) dictionary for hunspell</da>
   <de>Englisches (kanadisches) Wörterbuch für “Hunspell”</de>
   <el>Αγγλικό (καναδικό) λεξικό για hunspell</el>
   <en>English (Canadian) dictionary for hunspell</en>
   <es_ES>Diccionario Ingles (Canadiense) para hunspell</es_ES>
   <es>Diccionario Inglés (Canadiense) para hunspell</es>
   <et>English (Canadian) dictionary for hunspell</et>
   <eu>English (Canadian) dictionary for hunspell</eu>
   <fa>English (Canadian) dictionary for hunspell</fa>
   <fil_PH>English (Canadian) dictionary for hunspell</fil_PH>
   <fi>English (Canadian) dictionary for hunspell</fi>
   <fr_BE>Anglais (canadien) dictionnaire pour hunspell</fr_BE>
   <fr>Anglais (canadien) dictionnaire pour hunspell</fr>
   <gl_ES>English (Canadian) dictionary for hunspell</gl_ES>
   <gu>English (Canadian) dictionary for hunspell</gu>
   <he_IL>English (Canadian) dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु अंग्रेजी (कनेडियाई) शब्दकोष</hi>
   <hr>English (Canadian) dictionary for hunspell</hr>
   <hu>English (Canadian) dictionary for hunspell</hu>
   <id>English (Canadian) dictionary for hunspell</id>
   <is>English (Canadian) dictionary for hunspell</is>
   <it>Dizionario inglese (canadese) per hunspell</it>
   <ja>Hunspell 用英語（カナダ）辞書</ja>
   <kk>English (Canadian) dictionary for hunspell</kk>
   <ko>English (Canadian) dictionary for hunspell</ko>
   <ku>English (Canadian) dictionary for hunspell</ku>
   <lt>English (Canadian) dictionary for hunspell</lt>
   <mk>English (Canadian) dictionary for hunspell</mk>
   <mr>English (Canadian) dictionary for hunspell</mr>
   <nb_NO>English (Canadian) dictionary for hunspell</nb_NO>
   <nb>Engelsk (kanadisk) ordliste for hunspell</nb>
   <nl_BE>English (Canadian) dictionary for hunspell</nl_BE>
   <nl>Engels (Canadees) woordenboek voor hunspell</nl>
   <or>English (Canadian) dictionary for hunspell</or>
   <pl>English (Canadian) dictionary for hunspell</pl>
   <pt_BR>Dicionário Inglês (Canadense) para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Inglês (Canadiano) para hunspell</pt>
   <ro>English (Canadian) dictionary for hunspell</ro>
   <ru>English (Canadian) dictionary for hunspell</ru>
   <sk>English (Canadian) dictionary for hunspell</sk>
   <sl>Angleški (kanadska) slovar za hunspell</sl>
   <so>English (Canadian) dictionary for hunspell</so>
   <sq>Fjalor anglisht  (Kanada) për hunspell</sq>
   <sr>English (Canadian) dictionary for hunspell</sr>
   <sv>Engelsk (Kanadensisk) ordbok för hunspell</sv>
   <th>English (Canadian) dictionary for hunspell</th>
   <tr>Hunspell için İngilizce (Kanada) sözlüğü</tr>
   <uk>English (Canadian) dictionary for hunspell</uk>
   <vi>English (Canadian) dictionary for hunspell</vi>
   <zh_CN>English (Canadian) dictionary for hunspell</zh_CN>
   <zh_HK>English (Canadian) dictionary for hunspell</zh_HK>
   <zh_TW>English (Canadian) dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-en-ca
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-en-ca
</uninstall_package_names>

</app>
