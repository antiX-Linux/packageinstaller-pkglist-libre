<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Brazilian Portuguese
</name>

<description>
   <am>Brazilian Portuguese dictionary for hunspell</am>
   <ar>Brazilian Portuguese dictionary for hunspell</ar>
   <be>Brazilian Portuguese dictionary for hunspell</be>
   <bg>Brazilian Portuguese dictionary for hunspell</bg>
   <bn>Brazilian Portuguese dictionary for hunspell</bn>
   <ca>Diccionari Portuguès (Brasil) per hunspell</ca>
   <cs>Brazilian Portuguese dictionary for hunspell</cs>
   <da>Brazilian Portuguese dictionary for hunspell</da>
   <de>Wörterbuch für brasilianisches Portugiesisch für “Hunspell”</de>
   <el>Λεξικό Πορτογαλικά Βραζιλίας για hunspell</el>
   <en>Brazilian Portuguese dictionary for hunspell</en>
   <es_ES>Diccionario Portugués Brasileño para hunspell</es_ES>
   <es>Diccionario Portugués Brasileño para hunspell</es>
   <et>Brazilian Portuguese dictionary for hunspell</et>
   <eu>Brazilian Portuguese dictionary for hunspell</eu>
   <fa>Brazilian Portuguese dictionary for hunspell</fa>
   <fil_PH>Brazilian Portuguese dictionary for hunspell</fil_PH>
   <fi>Brazilian Portuguese dictionary for hunspell</fi>
   <fr_BE>Portuguais (Brésil) dictionnaire pour hunspell</fr_BE>
   <fr>Portuguais (Brésil) dictionnaire pour hunspell</fr>
   <gl_ES>Brazilian Portuguese dictionary for hunspell</gl_ES>
   <gu>Brazilian Portuguese dictionary for hunspell</gu>
   <he_IL>Brazilian Portuguese dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु पुर्तगाली पुर्तगाल शब्दकोष</hi>
   <hr>Brazilian Portuguese dictionary for hunspell</hr>
   <hu>Brazilian Portuguese dictionary for hunspell</hu>
   <id>Brazilian Portuguese dictionary for hunspell</id>
   <is>Brazilian Portuguese dictionary for hunspell</is>
   <it>Dizionario portoghese-brasiliano per hunspell</it>
   <ja>Hunspell 用ブラジル・ポルトガル語辞書</ja>
   <kk>Brazilian Portuguese dictionary for hunspell</kk>
   <ko>Brazilian Portuguese dictionary for hunspell</ko>
   <ku>Brazilian Portuguese dictionary for hunspell</ku>
   <lt>Brazilian Portuguese dictionary for hunspell</lt>
   <mk>Brazilian Portuguese dictionary for hunspell</mk>
   <mr>Brazilian Portuguese dictionary for hunspell</mr>
   <nb_NO>Brazilian Portuguese dictionary for hunspell</nb_NO>
   <nb>Brasilsk-portugisisk ordliste for hunspell</nb>
   <nl_BE>Brazilian Portuguese dictionary for hunspell</nl_BE>
   <nl>Braziliaans Portugees woordenboek voor hunspell</nl>
   <or>Brazilian Portuguese dictionary for hunspell</or>
   <pl>Brazilian Portuguese dictionary for hunspell</pl>
   <pt_BR>Dicionário Português do Brasil para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Português do Brasil para hunspell</pt>
   <ro>Brazilian Portuguese dictionary for hunspell</ro>
   <ru>Brazilian Portuguese dictionary for hunspell</ru>
   <sk>Brazilian Portuguese dictionary for hunspell</sk>
   <sl>Brazilska portugalščina - slovar za hunspell</sl>
   <so>Brazilian Portuguese dictionary for hunspell</so>
   <sq>Fjalor portugalishte Brazili për hunspell</sq>
   <sr>Brazilian Portuguese dictionary for hunspell</sr>
   <sv>Brasiliansk-Portugisisk ordbok för hunspell</sv>
   <th>Brazilian Portuguese dictionary for hunspell</th>
   <tr>Hunspell için Brezilya Portekizcesi sözlüğü</tr>
   <uk>Brazilian Portuguese dictionary for hunspell</uk>
   <vi>Brazilian Portuguese dictionary for hunspell</vi>
   <zh_CN>Brazilian Portuguese dictionary for hunspell</zh_CN>
   <zh_HK>Brazilian Portuguese dictionary for hunspell</zh_HK>
   <zh_TW>Brazilian Portuguese dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-pt-br
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-pt-br
</uninstall_package_names>

</app>
