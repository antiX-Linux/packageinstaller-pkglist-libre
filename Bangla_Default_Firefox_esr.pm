<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bangla_Default_Firefox_esr
</name>

<description>
   <am>Bengali (Bangladesh) localisation of Firefox ESR</am>
   <ar>Bengali (Bangladesh) localisation of Firefox ESR</ar>
   <be>Bengali (Bangladesh) localisation of Firefox ESR</be>
   <bg>Bengali (Bangladesh) localisation of Firefox ESR</bg>
   <bn>Bengali (Bangladesh) localisation of Firefox ESR</bn>
   <ca>Localització de Firefox ESR en Bengalí (Bangladesh)</ca>
   <cs>Bengali (Bangladesh) localisation of Firefox ESR</cs>
   <da>Bengali (Bangladesh) localisation of Firefox ESR</da>
   <de>Bengali (Bangladesh) localisation of Firefox ESR</de>
   <el>Μπενγκάλι (Μπαγκλαντές) για Firefox ESR</el>
   <en>Bengali (Bangladesh) localisation of Firefox ESR</en>
   <es_ES>Localización en bengalí (Bangladesh) de Firefox ESR</es_ES>
   <es>Localización en bengalí (Bangladesh) de Firefox ESR</es>
   <et>Bengali (Bangladesh) localisation of Firefox ESR</et>
   <eu>Bengali (Bangladesh) localisation of Firefox ESR</eu>
   <fa>Bengali (Bangladesh) localisation of Firefox ESR</fa>
   <fil_PH>Bengali (Bangladesh) localisation of Firefox ESR</fil_PH>
   <fi>Bengali (Bangladesh) localisation of Firefox ESR</fi>
   <fr_BE>Localisation en bengali (Bangladesh) pour Firefox ESR</fr_BE>
   <fr>Localisation en bengali (Bangladesh) pour Firefox ESR</fr>
   <gl_ES>Bengali (Bangladesh) localisation of Firefox ESR</gl_ES>
   <gu>Bengali (Bangladesh) localisation of Firefox ESR</gu>
   <he_IL>Bengali (Bangladesh) localisation of Firefox ESR</he_IL>
   <hi>Bengali (Bangladesh) localisation of Firefox ESR</hi>
   <hr>Bengali (Bangladesh) localisation of Firefox ESR</hr>
   <hu>Bengali (Bangladesh) localisation of Firefox ESR</hu>
   <id>Bengali (Bangladesh) localisation of Firefox ESR</id>
   <is>Bengali (Bangladesh) localisation of Firefox ESR</is>
   <it>Localizzazione Bengali (Bangladesh) di Firefox ESR</it>
   <ja>Bengali (Bangladesh) localisation of Firefox ESR</ja>
   <kk>Bengali (Bangladesh) localisation of Firefox ESR</kk>
   <ko>Bengali (Bangladesh) localisation of Firefox ESR</ko>
   <ku>Bengali (Bangladesh) localisation of Firefox ESR</ku>
   <lt>Bengali (Bangladesh) localisation of Firefox ESR</lt>
   <mk>Bengali (Bangladesh) localisation of Firefox ESR</mk>
   <mr>Bengali (Bangladesh) localisation of Firefox ESR</mr>
   <nb_NO>Bengali (Bangladesh) localisation of Firefox ESR</nb_NO>
   <nb>Bengali (Bangladesh) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Bengali (Bangladesh) localisation of Firefox ESR</nl_BE>
   <nl>Bengaalse (Bangladesh) lokalisatie van Firefox ESR</nl>
   <or>Bengali (Bangladesh) localisation of Firefox ESR</or>
   <pl>Bengali (Bangladesh) localisation of Firefox ESR</pl>
   <pt_BR>Bengali (Bangladesh) Localização para Firefox ESR</pt_BR>
   <pt>Bengali (Bangladesh) Localização para Firefox ESR</pt>
   <ro>Bengali (Bangladesh) localisation of Firefox ESR</ro>
   <ru>Bengali (Bangladesh) localisation of Firefox ESR</ru>
   <sk>Bengali (Bangladesh) localisation of Firefox ESR</sk>
   <sl>Bengalske (Bangladeš) krajevne nastavitve za Firefox ESR</sl>
   <so>Bengali (Bangladesh) localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në bengalisht (Bangladesh)</sq>
   <sr>Bengali (Bangladesh) localisation of Firefox ESR</sr>
   <sv>Bengali (Bangladesh) lokalisering av Firefox ESR</sv>
   <th>Bengali (Bangladesh) localisation of Firefox ESR</th>
   <tr>Bengali (Bangladesh) localisation of Firefox ESR</tr>
   <uk>Bengali (Bangladesh) localisation of Firefox ESR</uk>
   <vi>Bengali (Bangladesh) localisation of Firefox ESR</vi>
   <zh_CN>Bengali (Bangladesh) localisation of Firefox ESR</zh_CN>
   <zh_HK>Bengali (Bangladesh) localisation of Firefox ESR</zh_HK>
   <zh_TW>Bengali (Bangladesh) localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-bn-bd
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-bn-bd
</uninstall_package_names>

</app>
