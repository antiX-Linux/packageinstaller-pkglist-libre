<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Arabic_Firefox
</name>

<description>
   <am>Arabic localisation of Firefox</am>
   <ar>Arabic localisation of Firefox</ar>
   <be>Arabic localisation of Firefox</be>
   <bg>Arabic localisation of Firefox</bg>
   <bn>Arabic localisation of Firefox</bn>
   <ca>Localització de Firefox en Àrab</ca>
   <cs>Arabic localisation of Firefox</cs>
   <da>Arabisk oversættelse af Firefox</da>
   <de>Arabische Lokalisierung von Firefox</de>
   <el>Αραβικά για το Firefox</el>
   <en>Arabic localisation of Firefox</en>
   <es_ES>Localización Arabe de Firefox</es_ES>
   <es>Localización Árabe de Firefox</es>
   <et>Arabic localisation of Firefox</et>
   <eu>Arabic localisation of Firefox</eu>
   <fa>Arabic localisation of Firefox</fa>
   <fil_PH>Arabic localisation of Firefox</fil_PH>
   <fi>Arabialainen Firefox-kotoistus</fi>
   <fr_BE>Localisation arabe pour Firefox</fr_BE>
   <fr>Localisation arabe pour Firefox</fr>
   <gl_ES>Firefox localizado en árabe</gl_ES>
   <gu>Arabic localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לערבית</he_IL>
   <hi>फायरफॉक्स का अरबी अनुवाद</hi>
   <hr>Arapska lokalizacija Firefoxa</hr>
   <hu>Arabic localisation of Firefox</hu>
   <id>Arabic localisation of Firefox</id>
   <is>Arabic localisation of Firefox</is>
   <it>Localizzazione araba di Firefox</it>
   <ja>アラビア語版 Firefox</ja>
   <kk>Arabic localisation of Firefox</kk>
   <ko>Arabic localisation of Firefox</ko>
   <ku>Arabic localisation of Firefox</ku>
   <lt>Arabic localisation of Firefox</lt>
   <mk>Arabic localisation of Firefox</mk>
   <mr>Arabic localisation of Firefox</mr>
   <nb_NO>Arabic localisation of Firefox</nb_NO>
   <nb>Arabisk lokaltilpassing av Firefox</nb>
   <nl_BE>Arabic localisation of Firefox</nl_BE>
   <nl>Arabische lokalisatie van Firefox</nl>
   <or>Arabic localisation of Firefox</or>
   <pl>Arabska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Árabe Localização para o Firefox</pt_BR>
   <pt>Árabe Localização para Firefox</pt>
   <ro>Arabic localisation of Firefox</ro>
   <ru>Арабская локализация Firefox</ru>
   <sk>Arabic lokalizácia pre Firefox</sk>
   <sl>Arabske krajevne nastavitve za Firefox</sl>
   <so>Arabic localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në arabisht</sq>
   <sr>Arabic localisation of Firefox</sr>
   <sv>Arabisk lokalisering av  Firefox</sv>
   <th>Arabic localisation ของ Firefox</th>
   <tr>Firefox Arapça yerelleştirmesi</tr>
   <uk>Arabic localisation of Firefox</uk>
   <vi>Arabic localisation of Firefox</vi>
   <zh_CN>Firefox 阿拉伯语语言包</zh_CN>
   <zh_HK>Arabic localisation of Firefox</zh_HK>
   <zh_TW>Arabic localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-ar
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-ar
</uninstall_package_names>
</app>
