<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Amharic_LO_Latest_main
</name>

<description>
   <am>Amharic Language Meta-Package for LibreOffice</am>
   <ar>Amharic Language Meta-Package for LibreOffice</ar>
   <be>Amharic Language Meta-Package for LibreOffice</be>
   <bg>Amharic Language Meta-Package for LibreOffice</bg>
   <bn>Amharic Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Amharic per LibreOffice</ca>
   <cs>Amharic Language Meta-Package for LibreOffice</cs>
   <da>Amharic Language Meta-Package for LibreOffice</da>
   <de>Amharisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Amharic</el>
   <en>Amharic Language Meta-Package for LibreOffice</en>
   <es_ES>Metapaquete de idioma amárico para LibreOffice</es_ES>
   <es>Metapaquete de idioma Amárico para LibreOffice</es>
   <et>Amharic Language Meta-Package for LibreOffice</et>
   <eu>Amharic Language Meta-Package for LibreOffice</eu>
   <fa>Amharic Language Meta-Package for LibreOffice</fa>
   <fil_PH>Amharic Language Meta-Package for LibreOffice</fil_PH>
   <fi>Amharalainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de langue amharique pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de langue amharique pour LibreOffice</fr>
   <gl_ES>Amárico Meta-paquete de idioma para LibreOffice</gl_ES>
   <gu>Amharic Language Meta-Package for LibreOffice</gu>
   <he_IL>חבילת על לאמהרית עבור LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु अम्हारिक भाषा मेटा-पैकेज</hi>
   <hr>Amharic Language Meta-Package for LibreOffice</hr>
   <hu>Amharic Language Meta-Package for LibreOffice</hu>
   <id>Amharic Language Meta-Package for LibreOffice</id>
   <is>Amharic Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua amarica per LibreOffice</it>
   <ja>LibreOffice用のアムハラ語メタパッケージ</ja>
   <kk>Amharic Language Meta-Package for LibreOffice</kk>
   <ko>Amharic Language Meta-Package for LibreOffice</ko>
   <ku>Amharic Language Meta-Package for LibreOffice</ku>
   <lt>Amharic Language Meta-Package for LibreOffice</lt>
   <mk>Amharic Language Meta-Package for LibreOffice</mk>
   <mr>Amharic Language Meta-Package for LibreOffice</mr>
   <nb_NO>Amharic Language Meta-Package for LibreOffice</nb_NO>
   <nb>Amharisk språkpakke for LibreOffice</nb>
   <nl_BE>Amharic Language Meta-Package for LibreOffice</nl_BE>
   <nl>Amharisch Taal Meta-Pakket voor LibreOffice</nl>
   <or>Amharic Language Meta-Package for LibreOffice</or>
   <pl>Amharski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Amárica Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Amárico Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Amharic Language Meta-Package for LibreOffice</ro>
   <ru>Amharic Language Meta-Package for LibreOffice</ru>
   <sk>Amharic Language Meta-Package for LibreOffice</sk>
   <sl>Amharični jezikovni meta-paket za LibreOffice</sl>
   <so>Amharic Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në amharisht</sq>
   <sr>Amharic Language Meta-Package for LibreOffice</sr>
   <sv>Amhariskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Amharic สำหรับ LibreOffice</th>
   <tr>LibreOffice için Amharik Dili Üst-Paketi</tr>
   <uk>Amharic Language Meta-Package for LibreOffice</uk>
   <vi>Amharic Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 阿姆哈拉语语言包</zh_CN>
   <zh_HK>Amharic Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Amharic Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-am
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-am
libreoffice-gtk3
</uninstall_package_names>

</app>
