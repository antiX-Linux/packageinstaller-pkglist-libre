<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Office
</category>

<name>
Abiword
</name>

<description>
   <am>lightweight word processor</am>
   <ar>lightweight word processor</ar>
   <be>lightweight word processor</be>
   <bg>lightweight word processor</bg>
   <bn>lightweight word processor</bn>
   <ca>Processador de text lleuger</ca>
   <cs>lightweight word processor</cs>
   <da>letvægts tegnbehandler</da>
   <de>Eine voll funktionsfähige und effiziente Textverarbeitung</de>
   <el>Ελαφρύς επεξεργαστής κειμένου</el>
   <en>lightweight word processor</en>
   <es_ES>Procesador de textos liviano</es_ES>
   <es>Procesador de textos liviano</es>
   <et>lightweight word processor</et>
   <eu>lightweight word processor</eu>
   <fa>lightweight word processor</fa>
   <fil_PH>lightweight word processor</fil_PH>
   <fi>kevyt tekstinkäsittelyohjelma</fi>
   <fr_BE>Traitement de texte léger</fr_BE>
   <fr>Traitement de texte léger</fr>
   <gl_ES>Procesador de texto simple e lixeiro</gl_ES>
   <gu>lightweight word processor</gu>
   <he_IL>מעבד תמלילים קליל</he_IL>
   <hi>सरल वर्ड प्रोसेसर</hi>
   <hr>lightweight word processor</hr>
   <hu>lightweight word processor</hu>
   <id>lightweight word processor</id>
   <is>lightweight word processor</is>
   <it>Editor di testo leggero</it>
   <ja>軽量級のワープロ</ja>
   <kk>lightweight word processor</kk>
   <ko>lightweight word processor</ko>
   <ku>lightweight word processor</ku>
   <lt>lightweight word processor</lt>
   <mk>lightweight word processor</mk>
   <mr>lightweight word processor</mr>
   <nb_NO>lightweight word processor</nb_NO>
   <nb>lettvektig tekstbehandler</nb>
   <nl_BE>lightweight word processor</nl_BE>
   <nl>lichtgewicht word processor</nl>
   <or>lightweight word processor</or>
   <pl>lekki edytor tekstu</pl>
   <pt_BR>Processador de texto simples e leve</pt_BR>
   <pt>Processador de texto simples e ligeiro</pt>
   <ro>lightweight word processor</ro>
   <ru>Легковесный текстовый редактор</ru>
   <sk>ľahký textový editor</sk>
   <sl>lahek urejevalnik besedil</sl>
   <so>lightweight word processor</so>
   <sq>Fjalëpërpunues i peshës së lehtë</sq>
   <sr>lightweight word processor</sr>
   <sv>lättvikts ordbehandlare</sv>
   <th>Word processor น้ำหนักเบา</th>
   <tr>hafif kelime işlemci</tr>
   <uk>легкий текстовий процесор</uk>
   <vi>trình xử lí văn bản gọn nhẹ</vi>
   <zh_CN>轻量级文字处理工具</zh_CN>
   <zh_HK>lightweight word processor</zh_HK>
   <zh_TW>lightweight word processor</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
abiword
abiword-common
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
abiword
abiword-common
</uninstall_package_names>
</app>
