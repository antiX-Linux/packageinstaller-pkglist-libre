<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Children
</category>

<name>
Preschool
</name>

<description>
   <am>Pre-School. Includes: gamine, gcompris and tuxpaint</am>
   <ar>Pre-School. Includes: gamine, gcompris and tuxpaint</ar>
   <be>Pre-School. Includes: gamine, gcompris and tuxpaint</be>
   <bg>Pre-School. Includes: gamine, gcompris and tuxpaint</bg>
   <bn>Pre-School. Includes: gamine, gcompris and tuxpaint</bn>
   <ca>Pre-escolar: inclou gamine, gcompris i tuxpaint</ca>
   <cs>Pre-School. Includes: gamine, gcompris and tuxpaint</cs>
   <da>Førskole. Inkluderer: gamine, gcompris og tuxpaint</da>
   <de>Vorschule: Inklusive Gamine, Gcompris und Tuxpaint</de>
   <el>Προσχολικά. Περιλαμβάνει: gamine, gcompris και tuxpaint</el>
   <en>Pre-School. Includes: gamine, gcompris and tuxpaint</en>
   <es_ES>Preescolar. Incluye: gamine, gcompris y tuxpaint.</es_ES>
   <es>Preescolar. Incluye: gamine, gcompris y tuxpaint.</es>
   <et>Pre-School. Includes: gamine, gcompris and tuxpaint</et>
   <eu>Pre-School. Includes: gamine, gcompris and tuxpaint</eu>
   <fa>Pre-School. Includes: gamine, gcompris and tuxpaint</fa>
   <fil_PH>Pre-School. Includes: gamine, gcompris and tuxpaint</fil_PH>
   <fi>Esikoulu. Mukana tulee: gamine, gcompris ja tuxpaint</fi>
   <fr_BE>Maternelle. Inclus: gamine, gcompris et tuxpaint</fr_BE>
   <fr>Maternelle. Inclus: gamine, gcompris et tuxpaint</fr>
   <gl_ES>Prescolar. Inclúe: gamine, gcompris e tuxpaint</gl_ES>
   <gu>Pre-School. Includes: gamine, gcompris and tuxpaint</gu>
   <he_IL>Pre-School. Includes: gamine, gcompris and tuxpaint</he_IL>
   <hi>शिशु विद्यालय। सम्मिलित : gamine, gcompris and tuxpaint</hi>
   <hr>Pre-School. Includes: gamine, gcompris and tuxpaint</hr>
   <hu>Pre-School. Includes: gamine, gcompris and tuxpaint</hu>
   <id>Pre-School. Includes: gamine, gcompris and tuxpaint</id>
   <is>Pre-School. Includes: gamine, gcompris and tuxpaint</is>
   <it>Elementari. Include: gamine, gcompris e tuxpaint</it>
   <ja>プレスクール。これは gamine、gcompris、tuxpaint を含みます</ja>
   <kk>Pre-School. Includes: gamine, gcompris and tuxpaint</kk>
   <ko>Pre-School. Includes: gamine, gcompris and tuxpaint</ko>
   <ku>Pre-School. Includes: gamine, gcompris and tuxpaint</ku>
   <lt>Pre-School. Includes: gamine, gcompris and tuxpaint</lt>
   <mk>Pre-School. Includes: gamine, gcompris and tuxpaint</mk>
   <mr>Pre-School. Includes: gamine, gcompris and tuxpaint</mr>
   <nb_NO>Pre-School. Includes: gamine, gcompris and tuxpaint</nb_NO>
   <nb>Førskole. Inkluderer gamine, gcompris og tuxpaint</nb>
   <nl_BE>Pre-School. Includes: gamine, gcompris and tuxpaint</nl_BE>
   <nl>Pre-School. Inclusief: gamine, gcompris en tuxpaint</nl>
   <or>Pre-School. Includes: gamine, gcompris and tuxpaint</or>
   <pl>Przedszkole. Zawiera: gamine, gcompris i tuxpaint</pl>
   <pt_BR>Jogos para ensino infantil/pré-escola. Inclui: gamine (colorir), gcompris (suíte educacional) e tuxpaint (aprender a desenhar)</pt_BR>
   <pt>Pré-escola/infantil. Inclui: gamine, gcompris e tuxpaint</pt>
   <ro>Pre-School. Includes: gamine, gcompris and tuxpaint</ro>
   <ru>Приложения для дошкольников: gamine, gcompris и tuxpaint</ru>
   <sk>Pre-School. Includes: gamine, gcompris and tuxpaint</sk>
   <sl>Predšola. Vsebuje: gamine, gcompris in tuxpaint</sl>
   <so>Pre-School. Includes: gamine, gcompris and tuxpaint</so>
   <sq>Parashkollor. Përfshin: gamine, gcompris dhe tuxpaint</sq>
   <sr>Pre-School. Includes: gamine, gcompris and tuxpaint</sr>
   <sv>Pre-School. Inkluderar: gamine, gcompris och tuxpaint</sv>
   <th>Pre-School. Includes: gamine, gcompris and tuxpaint</th>
   <tr>Okul Öncesi. İçeriği: gamine, gcompris ve tuxpaint</tr>
   <uk>Pre-School. Includes: gamine, gcompris and tuxpaint</uk>
   <vi>Pre-School. Includes: gamine, gcompris and tuxpaint</vi>
   <zh_CN>Pre-School. Includes: gamine, gcompris and tuxpaint</zh_CN>
   <zh_HK>Pre-School. Includes: gamine, gcompris and tuxpaint</zh_HK>
   <zh_TW>Pre-School. Includes: gamine, gcompris and tuxpaint</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gamine
gcompris-qt
tuxpaint
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gamine
gcompris-qt
tuxpaint
</uninstall_package_names>
</app>
