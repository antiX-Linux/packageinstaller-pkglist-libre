<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Package Management
</category>

<name>
synaptic
</name>

<description>
   <am>Graphical package management program for apt</am>
   <ar>Graphical package management program for apt</ar>
   <be>Graphical package management program for apt</be>
   <bg>Graphical package management program for apt</bg>
   <bn>Graphical package management program for apt</bn>
   <ca>Paquet gràfic per gestió de programari amb apt</ca>
   <cs>Graphical package management program for apt</cs>
   <da>Graphical package management program for apt</da>
   <de>Graphische Installationspaketeverwaltung (verwendet “apt”)</de>
   <el>Πρόγραμμα διαχείρισης πακέτων για apt</el>
   <en>Graphical package management program for apt</en>
   <es_ES>Programa de gestión de paquetes gráficos para apt</es_ES>
   <es>Programa de gestión de paquetes gráficos para apt</es>
   <et>Graphical package management program for apt</et>
   <eu>Graphical package management program for apt</eu>
   <fa>Graphical package management program for apt</fa>
   <fil_PH>Graphical package management program for apt</fil_PH>
   <fi>Graphical package management program for apt</fi>
   <fr_BE>Système graphique de gestion des paquets basé sur “apt”</fr_BE>
   <fr>Système graphique de gestion des paquets basé sur “apt”</fr>
   <gl_ES>Graphical package management program for apt</gl_ES>
   <gu>Graphical package management program for apt</gu>
   <he_IL>Graphical package management program for apt</he_IL>
   <hi>Apt हेतु ग्राफ़िकल पैकेज प्रबंधन प्रोग्राम</hi>
   <hr>Graphical package management program for apt</hr>
   <hu>Graphical package management program for apt</hu>
   <id>Graphical package management program for apt</id>
   <is>Graphical package management program for apt</is>
   <it>Programma grafico di gestione dei pacchetti per apt</it>
   <ja>apt用のグラフィカルなパッケージ管理プログラム</ja>
   <kk>Graphical package management program for apt</kk>
   <ko>Graphical package management program for apt</ko>
   <ku>Graphical package management program for apt</ku>
   <lt>Graphical package management program for apt</lt>
   <mk>Graphical package management program for apt</mk>
   <mr>Graphical package management program for apt</mr>
   <nb_NO>Graphical package management program for apt</nb_NO>
   <nb>Grafisk pakkebehandlingsprogram for apt</nb>
   <nl_BE>Graphical package management program for apt</nl_BE>
   <nl>Graphical package management program for apt</nl>
   <or>Graphical package management program for apt</or>
   <pl>Graphical package management program for apt</pl>
   <pt_BR>Programa de gerenciamento gráfico de pacotes para apt</pt_BR>
   <pt>Gestor de pacotes gráfico para apt</pt>
   <ro>Graphical package management program for apt</ro>
   <ru>Graphical package management program for apt</ru>
   <sk>Graphical package management program for apt</sk>
   <sl>Grafični upravljalnik paketov za apt</sl>
   <so>Graphical package management program for apt</so>
   <sq>Program grafik administrimi paketash për apt</sq>
   <sr>Graphical package management program for apt</sr>
   <sv>Grafiskt pakethanterings-program för apt</sv>
   <th>Graphical package management program for apt</th>
   <tr>Apt için grafiksel paket yönetim programı</tr>
   <uk>Graphical package management program for apt</uk>
   <vi>Graphical package management program for apt</vi>
   <zh_CN>Graphical package management program for apt</zh_CN>
   <zh_HK>Graphical package management program for apt</zh_HK>
   <zh_TW>Graphical package management program for apt</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
synaptic
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
synaptic
</uninstall_package_names>
</app>
