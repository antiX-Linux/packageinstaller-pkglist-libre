<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Czech_Firefox
</name>

<description>
   <am>Czech localisation of Firefox</am>
   <ar>Czech localisation of Firefox</ar>
   <be>Czech localisation of Firefox</be>
   <bg>Czech localisation of Firefox</bg>
   <bn>Czech localisation of Firefox</bn>
   <ca>Localització de Firefox en Txec</ca>
   <cs>Czech localisation of Firefox</cs>
   <da>Tjekkisk oversættelse af Firefox</da>
   <de>Tschechische Lokalisierung von Firefox</de>
   <el>Τσέχικα για το Firefox</el>
   <en>Czech localisation of Firefox</en>
   <es_ES>Localización Checa de Firefox</es_ES>
   <es>Localización Checo de Firefox</es>
   <et>Czech localisation of Firefox</et>
   <eu>Czech localisation of Firefox</eu>
   <fa>Czech localisation of Firefox</fa>
   <fil_PH>Czech localisation of Firefox</fil_PH>
   <fi>Tsekkiläinen Firefox-kotoistus</fi>
   <fr_BE>Localisation en tchèque pour Firefox</fr_BE>
   <fr>Localisation en tchèque pour Firefox</fr>
   <gl_ES>Localización de Firefox en checo</gl_ES>
   <gu>Czech localisation of Firefox</gu>
   <he_IL>Czech localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का चेक संस्करण</hi>
   <hr>Czech localisation of Firefox</hr>
   <hu>Czech localisation of Firefox</hu>
   <id>Czech localisation of Firefox</id>
   <is>Czech localisation of Firefox</is>
   <it>Localizzazione in ceco di Firefox</it>
   <ja>Firefox のチェコ語版</ja>
   <kk>Czech localisation of Firefox</kk>
   <ko>Czech localisation of Firefox</ko>
   <ku>Czech localisation of Firefox</ku>
   <lt>Czech localisation of Firefox</lt>
   <mk>Czech localisation of Firefox</mk>
   <mr>Czech localisation of Firefox</mr>
   <nb_NO>Czech localisation of Firefox</nb_NO>
   <nb>Tsjekkisk lokaltilpassing av Firefox</nb>
   <nl_BE>Czech localisation of Firefox</nl_BE>
   <nl>Tjechische lokalisatie van Firefox</nl>
   <or>Czech localisation of Firefox</or>
   <pl>Czeska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Checo Localização para o Firefox</pt_BR>
   <pt>Checo Localização para Firefox</pt>
   <ro>Czech localisation of Firefox</ro>
   <ru>Чешская локализация Firefox</ru>
   <sk>Česká lokalizácia Firefoxu</sk>
   <sl>Češke krajevne nastavitve za Firefox</sl>
   <so>Czech localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në çekisht</sq>
   <sr>Czech localisation of Firefox</sr>
   <sv>Tjeckisk lokalisering av Firefox</sv>
   <th>Czech localisation ของ Firefox</th>
   <tr>Firefox Çekce yerelleştirmesi</tr>
   <uk>Czech локалізація Firefox</uk>
   <vi>Czech localisation of Firefox</vi>
   <zh_CN>Firefox 捷克语语言包</zh_CN>
   <zh_HK>Czech localisation of Firefox</zh_HK>
   <zh_TW>Czech localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-cs
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-cs
</uninstall_package_names>
</app>
