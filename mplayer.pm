<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Video
</category>

<name>
mplayer
</name>

<description>
   <am>a powerful multimedia player and much more</am>
   <ar>a powerful multimedia player and much more</ar>
   <be>a powerful multimedia player and much more</be>
   <bg>a powerful multimedia player and much more</bg>
   <bn>a powerful multimedia player and much more</bn>
   <ca>potent reproductor multimèdia i molt més</ca>
   <cs>a powerful multimedia player and much more</cs>
   <da>en kraftfuld medieafspiller og meget mere</da>
   <de>Ein leistungsstarker Multimedia-Player und vieles mehr</de>
   <el>Ισχυρό πρόγραμμα αναπαραγωγής πολυμέσων και πολλά άλλα</el>
   <en>a powerful multimedia player and much more</en>
   <es_ES>Potente reproductor multimedia y mucho más</es_ES>
   <es>Potente reproductor multimedia y mucho más</es>
   <et>a powerful multimedia player and much more</et>
   <eu>a powerful multimedia player and much more</eu>
   <fa>a powerful multimedia player and much more</fa>
   <fil_PH>a powerful multimedia player and much more</fil_PH>
   <fi>tehokas multimediasoitin ja paljon muuta</fi>
   <fr_BE>Un puissant lecteur multimédia et bien plus</fr_BE>
   <fr>Un puissant lecteur multimédia et bien plus</fr>
   <gl_ES>Poderoso reprodutor de multimedia e moito máis</gl_ES>
   <gu>a powerful multimedia player and much more</gu>
   <he_IL>a powerful multimedia player and much more</he_IL>
   <hi>एक सशक्त मल्टीमीडिया प्लेयर एवं बहुत कुछ</hi>
   <hr>a powerful multimedia player and much more</hr>
   <hu>a powerful multimedia player and much more</hu>
   <id>a powerful multimedia player and much more</id>
   <is>a powerful multimedia player and much more</is>
   <it>potente lettore multimediale e molto altro</it>
   <ja>強力なマルチメディアプレーヤーなど</ja>
   <kk>a powerful multimedia player and much more</kk>
   <ko>a powerful multimedia player and much more</ko>
   <ku>a powerful multimedia player and much more</ku>
   <lt>a powerful multimedia player and much more</lt>
   <mk>a powerful multimedia player and much more</mk>
   <mr>a powerful multimedia player and much more</mr>
   <nb_NO>a powerful multimedia player and much more</nb_NO>
   <nb>kraftig multimedieavspiller og mye mere</nb>
   <nl_BE>a powerful multimedia player and much more</nl_BE>
   <nl>een krachtige multimediaspeler en nog veel meer</nl>
   <or>a powerful multimedia player and much more</or>
   <pl>potężny odtwarzacz multimedialny i wiele więcej</pl>
   <pt_BR>Poderoso reprodutor de multimídia e muito mais</pt_BR>
   <pt>Poderoso reprodutor de multimédia e muito mais</pt>
   <ro>a powerful multimedia player and much more</ro>
   <ru>Продвинутый медиаплеер и даже больше</ru>
   <sk>a powerful multimedia player and much more</sk>
   <sl>Zmogljiv večpredstavnostni predvajalnik in mnogo več</sl>
   <so>a powerful multimedia player and much more</so>
   <sq>Një lojtës i fuqishëm multimediash dhe shumë më tepër</sq>
   <sr>a powerful multimedia player and much more</sr>
   <sv>en kraftfull multimediaspelare och mycket mer</sv>
   <th>a powerful multimedia player and much more</th>
   <tr>güçlü bir çokluortam oynatıcısı ve daha fazlası</tr>
   <uk>потужний мультимедія програвач і не тільки</uk>
   <vi>a powerful multimedia player and much more</vi>
   <zh_CN>a powerful multimedia player and much more</zh_CN>
   <zh_HK>a powerful multimedia player and much more</zh_HK>
   <zh_TW>a powerful multimedia player and much more</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
mplayer
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
mplayer
</uninstall_package_names>
</app>
