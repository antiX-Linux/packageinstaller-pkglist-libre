<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Kernels
</category>

<name>
Kernel-Debian_32bit_PAE
</name>

<description>
   <am>Fallback Debian 6.1 32bit PAE linux kernel</am>
   <ar>Fallback Debian 6.1 32bit PAE linux kernel</ar>
   <be>Fallback Debian 6.1 32bit PAE linux kernel</be>
   <bg>Fallback Debian 6.1 32bit PAE linux kernel</bg>
   <bn>Fallback Debian 6.1 32bit PAE linux kernel</bn>
   <ca>Kernel linux Debian 6.1 32bit PAE Fallback</ca>
   <cs>Fallback Debian 6.1 32bit PAE linux kernel</cs>
   <da>Fallback Debian 6.1 32bit PAE linux kernel</da>
   <de>Fallback Debian 6.1 32bit PAE linux kernel</de>
   <el>Debian 6.1 32bit linux kernel PAE</el>
   <en>Fallback Debian 6.1 32bit PAE linux kernel</en>
   <es_ES>Kernel Linux alternativo Debian 6.1 32bit PAE</es_ES>
   <es>Kernel Linux alternativo Debian 6.1 32bit PAE</es>
   <et>Fallback Debian 6.1 32bit PAE linux kernel</et>
   <eu>Fallback Debian 6.1 32bit PAE linux kernel</eu>
   <fa>Fallback Debian 6.1 32bit PAE linux kernel</fa>
   <fil_PH>Fallback Debian 6.1 32bit PAE linux kernel</fil_PH>
   <fi>Fallback Debian 6.1 32bit PAE linux kernel</fi>
   <fr_BE>Noyau linux Fallback Debian 6.1 32bit PAE</fr_BE>
   <fr>Noyau linux Fallback Debian 6.1 32bit PAE</fr>
   <gl_ES>Fallback Debian 6.1 32bit PAE linux kernel</gl_ES>
   <gu>Fallback Debian 6.1 32bit PAE linux kernel</gu>
   <he_IL>Fallback Debian 6.1 32bit PAE linux kernel</he_IL>
   <hi>Fallback Debian 6.1 32bit PAE linux kernel</hi>
   <hr>Fallback Debian 6.1 32bit PAE linux kernel</hr>
   <hu>Fallback Debian 6.1 32bit PAE linux kernel</hu>
   <id>Fallback Debian 6.1 32bit PAE linux kernel</id>
   <is>Fallback Debian 6.1 32bit PAE linux kernel</is>
   <it>Kernel Linux Debian Fallback 6.1 32bit PAE</it>
   <ja>Fallback Debian 6.1 32bit PAE linux kernel</ja>
   <kk>Fallback Debian 6.1 32bit PAE linux kernel</kk>
   <ko>Fallback Debian 6.1 32bit PAE linux kernel</ko>
   <ku>Fallback Debian 6.1 32bit PAE linux kernel</ku>
   <lt>Fallback Debian 6.1 32bit PAE linux kernel</lt>
   <mk>Fallback Debian 6.1 32bit PAE linux kernel</mk>
   <mr>Fallback Debian 6.1 32bit PAE linux kernel</mr>
   <nb_NO>Fallback Debian 6.1 32bit PAE linux kernel</nb_NO>
   <nb>Siste utvei – Debian 6.1 32-biters PAE Linux-kjerne</nb>
   <nl_BE>Fallback Debian 6.1 32bit PAE linux kernel</nl_BE>
   <nl>Fallback Debian 6.1 32bit PAE linux kernel</nl>
   <or>Fallback Debian 6.1 32bit PAE linux kernel</or>
   <pl>Fallback Debian 6.1 32bit PAE linux kernel</pl>
   <pt_BR>Fallback do Debian 6.1 de 32 bits PAE linux kernel</pt_BR>
   <pt>Núcleo (Kernel) linux Debian 6.1 32bit PAE de reserva</pt>
   <ro>Fallback Debian 6.1 32bit PAE linux kernel</ro>
   <ru>Fallback Debian 6.1 32bit PAE linux kernel</ru>
   <sk>Fallback Debian 6.1 32bit PAE linux kernel</sk>
   <sl>Starejše Debian 6.1 32bit PAE linux jedro</sl>
   <so>Fallback Debian 6.1 32bit PAE linux kernel</so>
   <sq>Fallback Debian 6.1 32bit PAE linux kernel</sq>
   <sr>Fallback Debian 6.1 32bit PAE linux kernel</sr>
   <sv>Fallback Debian 6.1 32bit PAE linux-kärna</sv>
   <th>Fallback Debian 6.1 32bit PAE linux kernel</th>
   <tr>Fallback Debian 6.1 32bit PAE linux kernel</tr>
   <uk>Fallback Debian 6.1 32bit PAE linux kernel</uk>
   <vi>Fallback Debian 6.1 32bit PAE linux kernel</vi>
   <zh_CN>Fallback Debian 6.1 32bit PAE linux kernel</zh_CN>
   <zh_HK>Fallback Debian 6.1 32bit PAE linux kernel</zh_HK>
   <zh_TW>Fallback Debian 6.1 32bit PAE linux kernel</zh_TW>
</description>

<installable>
32
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
linux-image-6.1.0-12-686-pae
linux-headers-6.1.0-12-686-pae
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-6.1.0-12-686-pae
linux-headers-6.1.0-12-686-pae
</uninstall_package_names>

</app>
