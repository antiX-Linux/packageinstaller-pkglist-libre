<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Telugu
</name>

<description>
   <am>Telugu dictionary for hunspell</am>
   <ar>Telugu dictionary for hunspell</ar>
   <be>Telugu dictionary for hunspell</be>
   <bg>Telugu dictionary for hunspell</bg>
   <bn>Telugu dictionary for hunspell</bn>
   <ca>Diccionari Telugu per hunspell</ca>
   <cs>Telugu dictionary for hunspell</cs>
   <da>Telugu dictionary for hunspell</da>
   <de>Indisches (telugisches) Wörterbuch für “Hunspell”</de>
   <el>Τελούγκου λεξικό για hunspell</el>
   <en>Telugu dictionary for hunspell</en>
   <es_ES>Diccionario Telugu para hunspell</es_ES>
   <es>Diccionario Telugu para hunspell</es>
   <et>Telugu dictionary for hunspell</et>
   <eu>Telugu dictionary for hunspell</eu>
   <fa>Telugu dictionary for hunspell</fa>
   <fil_PH>Telugu dictionary for hunspell</fil_PH>
   <fi>Telugu dictionary for hunspell</fi>
   <fr_BE>Telugu dictionnaire pour hunspell</fr_BE>
   <fr>Telugu dictionnaire pour hunspell</fr>
   <gl_ES>Telugu dictionary for hunspell</gl_ES>
   <gu>Telugu dictionary for hunspell</gu>
   <he_IL>Telugu dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु तेलुगू शब्दकोष</hi>
   <hr>Telugu dictionary for hunspell</hr>
   <hu>Telugu dictionary for hunspell</hu>
   <id>Telugu dictionary for hunspell</id>
   <is>Telugu dictionary for hunspell</is>
   <it>Dizionario telugu per hunspell</it>
   <ja>Hunspell 用テルグ語辞書</ja>
   <kk>Telugu dictionary for hunspell</kk>
   <ko>Telugu dictionary for hunspell</ko>
   <ku>Telugu dictionary for hunspell</ku>
   <lt>Telugu dictionary for hunspell</lt>
   <mk>Telugu dictionary for hunspell</mk>
   <mr>Telugu dictionary for hunspell</mr>
   <nb_NO>Telugu dictionary for hunspell</nb_NO>
   <nb>Telugu ordliste for hunspell</nb>
   <nl_BE>Telugu dictionary for hunspell</nl_BE>
   <nl>Telugu dictionary for hunspell</nl>
   <or>Telugu dictionary for hunspell</or>
   <pl>Telugu dictionary for hunspell</pl>
   <pt_BR>Dicionário Telugo para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Telego para hunspell</pt>
   <ro>Telugu dictionary for hunspell</ro>
   <ru>Telugu dictionary for hunspell</ru>
   <sk>Telugu dictionary for hunspell</sk>
   <sl>Teluški slovar za hunspell</sl>
   <so>Telugu dictionary for hunspell</so>
   <sq>Fjalor në telugu për hunspell</sq>
   <sr>Telugu dictionary for hunspell</sr>
   <sv>Telugu ordbok för hunspell</sv>
   <th>Telugu dictionary for hunspell</th>
   <tr>Hunspell için Teluguca sözlük</tr>
   <uk>Telugu dictionary for hunspell</uk>
   <vi>Telugu dictionary for hunspell</vi>
   <zh_CN>Telugu dictionary for hunspell</zh_CN>
   <zh_HK>Telugu dictionary for hunspell</zh_HK>
   <zh_TW>Telugu dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-te
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-te
</uninstall_package_names>

</app>
