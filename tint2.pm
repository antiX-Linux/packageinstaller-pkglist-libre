<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Docks
</category>

<name>
tint2
</name>

<description>
   <am>Simple panel/taskbar</am>
   <ar>Simple panel/taskbar</ar>
   <be>Simple panel/taskbar</be>
   <bg>Simple panel/taskbar</bg>
   <bn>Simple panel/taskbar</bn>
   <ca>Barra de tasques/plafó senzill</ca>
   <cs>Simple panel/taskbar</cs>
   <da>Simple panel/taskbar</da>
   <de>Simple panel/taskbar</de>
   <el>Απλό πλαίσιο/γραμμή εργασιών</el>
   <en>Simple panel/taskbar</en>
   <es_ES>Panel/barra de tareas sencilla</es_ES>
   <es>Panel/barra de tareas sencilla</es>
   <et>Simple panel/taskbar</et>
   <eu>Simple panel/taskbar</eu>
   <fa>Simple panel/taskbar</fa>
   <fil_PH>Simple panel/taskbar</fil_PH>
   <fi>Simple panel/taskbar</fi>
   <fr_BE>Panneau ou barre de tâches simple</fr_BE>
   <fr>Panneau ou barre de tâches simple</fr>
   <gl_ES>Simple panel/taskbar</gl_ES>
   <gu>Simple panel/taskbar</gu>
   <he_IL>Simple panel/taskbar</he_IL>
   <hi>Simple panel/taskbar</hi>
   <hr>Simple panel/taskbar</hr>
   <hu>Simple panel/taskbar</hu>
   <id>Simple panel/taskbar</id>
   <is>Simple panel/taskbar</is>
   <it>Pannello/barra delle applicazioni semplice</it>
   <ja>Simple panel/taskbar</ja>
   <kk>Simple panel/taskbar</kk>
   <ko>Simple panel/taskbar</ko>
   <ku>Simple panel/taskbar</ku>
   <lt>Simple panel/taskbar</lt>
   <mk>Simple panel/taskbar</mk>
   <mr>Simple panel/taskbar</mr>
   <nb_NO>Simple panel/taskbar</nb_NO>
   <nb>Enkelt programpanel</nb>
   <nl_BE>Simple panel/taskbar</nl_BE>
   <nl>Simple panel/taskbar</nl>
   <or>Simple panel/taskbar</or>
   <pl>Simple panel/taskbar</pl>
   <pt_BR>Painel ou barra de tarefas simples</pt_BR>
   <pt>Painel/barra de tarefas simples</pt>
   <ro>Simple panel/taskbar</ro>
   <ru>Simple panel/taskbar</ru>
   <sk>Simple panel/taskbar</sk>
   <sl>Preprost pano/opravilna vrstica</sl>
   <so>Simple panel/taskbar</so>
   <sq>Panel/shtyllë aktesh i thjeshtë</sq>
   <sr>Simple panel/taskbar</sr>
   <sv>Enkelt panel/aktivitetsfält</sv>
   <th>Simple panel/taskbar</th>
   <tr>Simple panel/taskbar</tr>
   <uk>Simple panel/taskbar</uk>
   <vi>Simple panel/taskbar</vi>
   <zh_CN>Simple panel/taskbar</zh_CN>
   <zh_HK>Simple panel/taskbar</zh_HK>
   <zh_TW>Simple panel/taskbar</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
tint2
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
tint2
</uninstall_package_names>
</app>
