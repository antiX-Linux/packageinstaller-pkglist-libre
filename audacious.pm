<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Audio
</category>

<name>
Audacious
</name>

<description>
   <am>lightweight winamp/xmms like audio player</am>
   <ar>lightweight winamp/xmms like audio player</ar>
   <be>lightweight winamp/xmms like audio player</be>
   <bg>lightweight winamp/xmms like audio player</bg>
   <bn>lightweight winamp/xmms like audio player</bn>
   <ca>Reproductor lleuger d'àudio estil winamp/xmms</ca>
   <cs>lightweight winamp/xmms like audio player</cs>
   <da>letvægts winamp/xmms ligesom lydafspiller</da>
   <de>Leichtgewichtiger Audioplayer ähnlich zu Winamp/Xmms</de>
   <el>Ελαφρύ πρόγραμμα αναπαραγωγής ήχου όπως winamp/xmms</el>
   <en>lightweight winamp/xmms like audio player</en>
   <es_ES>Reproductor de audio liviano, estilo winamp/xmms</es_ES>
   <es>Reproductor de audio liviano, estilo winamp/xmms</es>
   <et>lightweight winamp/xmms like audio player</et>
   <eu>lightweight winamp/xmms like audio player</eu>
   <fa>lightweight winamp/xmms like audio player</fa>
   <fil_PH>lightweight winamp/xmms like audio player</fil_PH>
   <fi>Keveä Winamp:in/XMMS:n kaltainen äänisoitin</fi>
   <fr_BE>Lecteur audio léger de type winamp/xmms</fr_BE>
   <fr>Lecteur audio léger de type winamp/xmms</fr>
   <gl_ES>Reprodutor de audio simple semellante aos reprodutores winamp e xmms</gl_ES>
   <gu>lightweight winamp/xmms like audio player</gu>
   <he_IL>lightweight winamp/xmms like audio player</he_IL>
   <hi>winamp/xmms के समान सरल ऑडियो प्लेयर</hi>
   <hr>lightweight winamp/xmms like audio player</hr>
   <hu>lightweight winamp/xmms like audio player</hu>
   <id>lightweight winamp/xmms like audio player</id>
   <is>lightweight winamp/xmms like audio player</is>
   <it>Riproduttore di tracce audio, leggero come winamp/xmms</it>
   <ja>Winamp/XMMS に似た軽量なオーディオプレーヤー</ja>
   <kk>lightweight winamp/xmms like audio player</kk>
   <ko>lightweight winamp/xmms like audio player</ko>
   <ku>lightweight winamp/xmms like audio player</ku>
   <lt>lightweight winamp/xmms like audio player</lt>
   <mk>lightweight winamp/xmms like audio player</mk>
   <mr>lightweight winamp/xmms like audio player</mr>
   <nb_NO>lightweight winamp/xmms like audio player</nb_NO>
   <nb>lettvektig winamp/xmms-lignende lydspiller</nb>
   <nl_BE>lightweight winamp/xmms like audio player</nl_BE>
   <nl>lichtgewicht audio speler als winamp/xmms</nl>
   <or>lightweight winamp/xmms like audio player</or>
   <pl>lekki odtwarzacz audio podobny do winamp/xmms</pl>
   <pt_BR>Reprodutor de áudio simples e semelhante aos reprodutores Winamp e XMMS</pt_BR>
   <pt>Reprodutor de áudio simples semelhante aos reprodutores winamp e xmms</pt>
   <ro>lightweight winamp/xmms like audio player</ro>
   <ru>Легковесный аудиоплеер в духе winamp/xmms</ru>
   <sk>ľahký prehrávač hudby ako winamp/xmms</sk>
   <sl>lahek predvajalnik, ki je podoben predvalniku winamp/xmms</sl>
   <so>lightweight winamp/xmms like audio player</so>
   <sq>Lojtës audio i peshës së lehtë, i ngjashëm me winamp/xmms</sq>
   <sr>lightweight winamp/xmms like audio player</sr>
   <sv>lättvikts winamp/xmms-liknande ljudspelare</sv>
   <th>โปรแกรมเล่นเสียงน้ำหนักเบา คล้าย winamp/xmms</th>
   <tr>winamp/xmms benzeri hafif bir ses çalar</tr>
   <uk>легкий аудіо програвач схожий на winamp/xmms</uk>
   <vi>trình nghe nhạc gọn nhẹ tương tự winamp/xmms</vi>
   <zh_CN>轻量级类 winamp/xmms 音频播放器</zh_CN>
   <zh_HK>lightweight winamp/xmms like audio player</zh_HK>
   <zh_TW>lightweight winamp/xmms like audio player</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
audacious
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
audacious
</uninstall_package_names>
</app>
