<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Icons
</category>

<name>
Moka/Faba Icon Themes
</name>

<description>
   <am>a comprehensive icon set with a uniform flat "rounded square" look</am>
   <ar>a comprehensive icon set with a uniform flat "rounded square" look</ar>
   <be>a comprehensive icon set with a uniform flat "rounded square" look</be>
   <bg>a comprehensive icon set with a uniform flat "rounded square" look</bg>
   <bn>a comprehensive icon set with a uniform flat "rounded square" look</bn>
   <ca>joc d'icones extens amb un aspecte uniforme de "quadrat arrodonit"</ca>
   <cs>a comprehensive icon set with a uniform flat "rounded square" look</cs>
   <da>et omfattende ikonsæt med et ensartet fladt "afrundende firkanter"-look</da>
   <de>Ein umfangreiches Icon-Set in einheitlich flacher "runder quadratischer" Optik</de>
   <el>Ολοκληρωμένο σύνολο εικονιδίων με ομοιόμορφη επίπεδη "στρογγυλεμένη τετράγωνη" όψη</el>
   <en>a comprehensive icon set with a uniform flat "rounded square" look</en>
   <es_ES>Completo set de íconos con un estilo plano y uniforme, "cuadrado redondeado"</es_ES>
   <es>Completo set de íconos con un estilo plano y uniforme, "cuadrado redondeado"</es>
   <et>a comprehensive icon set with a uniform flat "rounded square" look</et>
   <eu>a comprehensive icon set with a uniform flat "rounded square" look</eu>
   <fa>a comprehensive icon set with a uniform flat "rounded square" look</fa>
   <fil_PH>a comprehensive icon set with a uniform flat "rounded square" look</fil_PH>
   <fi>kattava kuvakevalikoima yleisluontoisella litteällä "pyöristetyn neliön" ulkoasulla</fi>
   <fr_BE>Un jeu d'icônes clair à l'apparence uniforme flat "rounded square"</fr_BE>
   <fr>Un jeu d'icônes clair à l'apparence uniforme flat "rounded square"</fr>
   <gl_ES>conxunto de iconas abranxente con aspecto "cadrado redondeado" simple e uniforme</gl_ES>
   <gu>a comprehensive icon set with a uniform flat "rounded square" look</gu>
   <he_IL>a comprehensive icon set with a uniform flat "rounded square" look</he_IL>
   <hi>समानरूपी 'गोलाकार वर्ग' स्वरुप युक्त एक व्यापक आइकन संग्रह</hi>
   <hr>a comprehensive icon set with a uniform flat "rounded square" look</hr>
   <hu>a comprehensive icon set with a uniform flat "rounded square" look</hu>
   <id>a comprehensive icon set with a uniform flat "rounded square" look</id>
   <is>a comprehensive icon set with a uniform flat "rounded square" look</is>
   <it>un set di icone completo con un look uniforme a "quadrato arrotondato"</it>
   <ja>統一された、フラットで丸みを帯びた四角い外観のアイコンの完全なセット</ja>
   <kk>a comprehensive icon set with a uniform flat "rounded square" look</kk>
   <ko>a comprehensive icon set with a uniform flat "rounded square" look</ko>
   <ku>a comprehensive icon set with a uniform flat "rounded square" look</ku>
   <lt>a comprehensive icon set with a uniform flat "rounded square" look</lt>
   <mk>a comprehensive icon set with a uniform flat "rounded square" look</mk>
   <mr>a comprehensive icon set with a uniform flat "rounded square" look</mr>
   <nb_NO>a comprehensive icon set with a uniform flat "rounded square" look</nb_NO>
   <nb>omfattende ikonsamling designet som flate avrundede firkanter</nb>
   <nl_BE>a comprehensive icon set with a uniform flat "rounded square" look</nl_BE>
   <nl>een uitgebreide icoonenset met een uniform plat "rond vierkant" uiterlijk</nl>
   <or>a comprehensive icon set with a uniform flat "rounded square" look</or>
   <pl>obszerny zestaw ikon o jednolitym płaskim wyglądzie "zaokrąglonego kwadratu"</pl>
   <pt_BR>Conjunto de ícones abrangente com aspecto "quadrado arredondado" simples e uniforme</pt_BR>
   <pt>Conjunto de ícones abrangente com aspecto "quadrado arredondado" simples e uniforme</pt>
   <ro>a comprehensive icon set with a uniform flat "rounded square" look</ro>
   <ru>Обширный набор иконок оформленных в стиле "скругленный квадрат"</ru>
   <sk>a comprehensive icon set with a uniform flat "rounded square" look</sk>
   <sl>Širok nabor ikon z edinstvenim videzom "zaokroženih pravokotnikov"</sl>
   <so>a comprehensive icon set with a uniform flat "rounded square" look</so>
   <sq>a comprehensive icon set with a uniform flat "rounded square" look</sq>
   <sr>a comprehensive icon set with a uniform flat "rounded square" look</sr>
   <sv>ett omfattande ikon-set med en uniform platt "rundad fyrkant" look</sv>
   <th>a comprehensive icon set with a uniform flat "rounded square" look</th>
   <tr>tek tip düz "yuvarlatılmış kare" görünümü ile kapsamlı bir simge seti</tr>
   <uk>всебічний набір піктограм в пласкому стилі "округлий квадрат"</uk>
   <vi>a comprehensive icon set with a uniform flat "rounded square" look</vi>
   <zh_CN>a comprehensive icon set with a uniform flat "rounded square" look</zh_CN>
   <zh_HK>a comprehensive icon set with a uniform flat "rounded square" look</zh_HK>
   <zh_TW>a comprehensive icon set with a uniform flat "rounded square" look</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
moka-icon-theme
faba-icon-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
moka-icon-theme
faba-icon-theme
</uninstall_package_names>
</app>
