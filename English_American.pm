<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English (American)
</name>

<description>
   <am>English (American) dictionary for hunspell</am>
   <ar>English (American) dictionary for hunspell</ar>
   <be>English (American) dictionary for hunspell</be>
   <bg>English (American) dictionary for hunspell</bg>
   <bn>English (American) dictionary for hunspell</bn>
   <ca>Diccionari Anglès (Americà) per hunspell</ca>
   <cs>English (American) dictionary for hunspell</cs>
   <da>English (American) dictionary for hunspell</da>
   <de>Englisches (amerikanisches) Wörterbuch für “Hunspell”</de>
   <el>Αγγλικό (Αμερικανικό) λεξικό για hunspell</el>
   <en>English (American) dictionary for hunspell</en>
   <es_ES>Diccionario Ingles (Americano) para hunspell</es_ES>
   <es>Diccionario Inglés (Americano) para hunspell</es>
   <et>English (American) dictionary for hunspell</et>
   <eu>English (American) dictionary for hunspell</eu>
   <fa>English (American) dictionary for hunspell</fa>
   <fil_PH>English (American) dictionary for hunspell</fil_PH>
   <fi>English (American) dictionary for hunspell</fi>
   <fr_BE>Anglais (américain) dictionnaire pour hunspell</fr_BE>
   <fr>Anglais (américain) dictionnaire pour hunspell</fr>
   <gl_ES>English (American) dictionary for hunspell</gl_ES>
   <gu>English (American) dictionary for hunspell</gu>
   <he_IL>English (American) dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु अंग्रेजी (अमेरिकी) शब्दकोष</hi>
   <hr>English (American) dictionary for hunspell</hr>
   <hu>English (American) dictionary for hunspell</hu>
   <id>English (American) dictionary for hunspell</id>
   <is>English (American) dictionary for hunspell</is>
   <it>Dizionario inglese (americano) per hunspell</it>
   <ja>Hunspell 用英語（アメリカ）辞書</ja>
   <kk>English (American) dictionary for hunspell</kk>
   <ko>English (American) dictionary for hunspell</ko>
   <ku>English (American) dictionary for hunspell</ku>
   <lt>English (American) dictionary for hunspell</lt>
   <mk>English (American) dictionary for hunspell</mk>
   <mr>English (American) dictionary for hunspell</mr>
   <nb_NO>English (American) dictionary for hunspell</nb_NO>
   <nb>Engelsk (amerikansk) ordliste for hunspell</nb>
   <nl_BE>English (American) dictionary for hunspell</nl_BE>
   <nl>Engels (Amerikaans) woordenboek voor hunspell</nl>
   <or>English (American) dictionary for hunspell</or>
   <pl>English (American) dictionary for hunspell</pl>
   <pt_BR>Dicionário Inglês (Americano) para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Inglês (Americano) para hunspell</pt>
   <ro>English (American) dictionary for hunspell</ro>
   <ru>English (American) dictionary for hunspell</ru>
   <sk>English (American) dictionary for hunspell</sk>
   <sl>Angleški (ameriška) slovar za hunspell</sl>
   <so>English (American) dictionary for hunspell</so>
   <sq>Fjalor anglisht (Amerike) për hunspell</sq>
   <sr>English (American) dictionary for hunspell</sr>
   <sv>Engelsk (Amerikansk) ordbok för hunspell</sv>
   <th>English (American) dictionary for hunspell</th>
   <tr>Hunspell için İngilizce (Amerikan) sözlüğü</tr>
   <uk>English (American) dictionary for hunspell</uk>
   <vi>English (American) dictionary for hunspell</vi>
   <zh_CN>English (American) dictionary for hunspell</zh_CN>
   <zh_HK>English (American) dictionary for hunspell</zh_HK>
   <zh_TW>English (American) dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-en-us
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-en-us
</uninstall_package_names>

</app>
