<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Remote Access
</category>

<name>
1-to-1 Assistance
</name>

<description>
   <am>Remote access help app from antiX</am>
   <ar>Remote access help app from antiX</ar>
   <be>Remote access help app from antiX</be>
   <bg>Remote access help app from antiX</bg>
   <bn>Remote access help app from antiX</bn>
   <ca>Eina d'ajuda d'accés remot d'antiX</ca>
   <cs>Remote access help app from antiX</cs>
   <da>Fjernadgang hjælpeprogram til antiX</da>
   <de>Anwendungshilfe von antiX für die Fernwartung</de>
   <el>Εφαρμογή απομακρυσμένης πρόσβασης από το antiX</el>
   <en>Remote access help app from antiX</en>
   <es_ES>Aplicación de ayuda de acceso remoto desde AntiX</es_ES>
   <es>Aplicación de antiX para dar soporte por acceso remoto</es>
   <et>Remote access help app from antiX</et>
   <eu>Remote access help app from antiX</eu>
   <fa>Remote access help app from antiX</fa>
   <fil_PH>Remote access help app from antiX</fil_PH>
   <fi>Etäyhteyden apusovellus antiX-kehittäjäryhmältä</fi>
   <fr_BE>Application d'aide pour l'accès à distance d'antiX</fr_BE>
   <fr>Application d'aide pour l'accès à distance d'antiX</fr>
   <gl_ES>Aplicativo do antiX para axuda por acceso remoto</gl_ES>
   <gu>Remote access help app from antiX</gu>
   <he_IL>יישומון סיוע מרחוק מבית antiX</he_IL>
   <hi>एंटी-एक्स द्वारा दूरस्थ अभिगम सहायता अनुप्रयोग</hi>
   <hr>Remote access help app from antiX</hr>
   <hu>Remote access help app from antiX</hu>
   <id>Remote access help app from antiX</id>
   <is>Remote access help app from antiX</is>
   <it>App da antiX, che permette un accesso remoto</it>
   <ja>antiX リモートアクセスのアプリヘルプ</ja>
   <kk>Remote access help app from antiX</kk>
   <ko>Remote access help app from antiX</ko>
   <ku>Remote access help app from antiX</ku>
   <lt>Nuotolinės prieigos pagalbos programa iš antiX</lt>
   <mk>Remote access help app from antiX</mk>
   <mr>Remote access help app from antiX</mr>
   <nb_NO>Remote access help app from antiX</nb_NO>
   <nb>Fjernstyringsprogram fra antiX</nb>
   <nl_BE>Remote access help app from antiX</nl_BE>
   <nl>Afstand toegankelijke hulp app van antiX</nl>
   <or>Remote access help app from antiX</or>
   <pl>aplikacja zdalnej pomocy od antiX</pl>
   <pt_BR>Aplicativo de ajuda para acesso remoto do antiX</pt_BR>
   <pt>Aplicação do antiX para ajuda por acesso remoto</pt>
   <ro>Remote access help app from antiX</ro>
   <ru>Приложение для помощи через удаленный доступ</ru>
   <sk>Remote access help app from antiX</sk>
   <sl>antiX pomoč za oddaljeni dostop</sl>
   <so>Remote access help app from antiX</so>
   <sq>Aplikacion ndihme për hyrje së largëti, nga antiX</sq>
   <sr>Remote access help app from antiX</sr>
   <sv>Hjälp-app för fjärråtkomst från antiX</sv>
   <th>แอปพลิเคชันการช่วยเหลือระยะไกลจาก antiX</th>
   <tr>AntiX'ten uzaktan erişim yardımı uygulaması</tr>
   <uk>Програма віддаленої підтримки від antiX</uk>
   <vi>Remote access help app from antiX</vi>
   <zh_CN>antiX 中的远程访问帮助程序</zh_CN>
   <zh_HK>Remote access help app from antiX</zh_HK>
   <zh_TW>Remote access help app from antiX</zh_TW>
</description>

<installable>
32,64
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
1-to-1-assistance-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
1-to-1-assistance-antix
</uninstall_package_names>
</app>
