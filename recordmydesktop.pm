<?xml version="1.0" encoding="UTF-8"?>
<app>


<category>
Screencast
</category>

<name>
recordmydesktop
</name>

<description>
   <am>a desktop session recorder</am>
   <ar>a desktop session recorder</ar>
   <be>a desktop session recorder</be>
   <bg>a desktop session recorder</bg>
   <bn>a desktop session recorder</bn>
   <ca>Enregistrador de sessions d'escriptori</ca>
   <cs>a desktop session recorder</cs>
   <da>en skrivebordssessionsoptager</da>
   <de>Ein Desktop-Session-Rekorder</de>
   <el>Καταγραφέας περιόδων σύνδεσης του γραφικού περιβάλλοντος</el>
   <en>a desktop session recorder</en>
   <es_ES>Realiza capturas de video de tu escritorio</es_ES>
   <es>Realiza capturas de video de tu escritorio</es>
   <et>a desktop session recorder</et>
   <eu>a desktop session recorder</eu>
   <fa>a desktop session recorder</fa>
   <fil_PH>a desktop session recorder</fil_PH>
   <fi>istunnon tallennusohjelma</fi>
   <fr_BE>Un enregistreur de session de bureau</fr_BE>
   <fr>Un enregistreur de session de bureau</fr>
   <gl_ES>Rexista en vídeo a sesión de traballo conforme evolúe no pantalla</gl_ES>
   <gu>a desktop session recorder</gu>
   <he_IL>a desktop session recorder</he_IL>
   <hi>डेस्कटॉप सत्र रिकॉर्डिंग साधन</hi>
   <hr>a desktop session recorder</hr>
   <hu>a desktop session recorder</hu>
   <id>a desktop session recorder</id>
   <is>a desktop session recorder</is>
   <it>registra una sessione del desktop</it>
   <ja>デスクトップセッション レコーダー</ja>
   <kk>a desktop session recorder</kk>
   <ko>a desktop session recorder</ko>
   <ku>a desktop session recorder</ku>
   <lt>a desktop session recorder</lt>
   <mk>a desktop session recorder</mk>
   <mr>a desktop session recorder</mr>
   <nb_NO>a desktop session recorder</nb_NO>
   <nb>opptak av skrivebordsøkt</nb>
   <nl_BE>a desktop session recorder</nl_BE>
   <nl>een desktop sessie recorder</nl>
   <or>a desktop session recorder</or>
   <pl>program do nagrywania obrazu z pulpitu i ekranu komputera</pl>
   <pt_BR>Gravador de ações da tela da sessão da área de trabalho</pt_BR>
   <pt>Regista em vídeo a sessão de trabalho conforme evolui no ecrã</pt>
   <ro>a desktop session recorder</ro>
   <ru>Запись сеанса рабочего стола</ru>
   <sk>a desktop session recorder</sk>
   <sl>Snemalnik namizja</sl>
   <so>a desktop session recorder</so>
   <sq>Një regjistrues sesioni desktopi</sq>
   <sr>a desktop session recorder</sr>
   <sv>en skrivbordssessions-inspelare</sv>
   <th>โปรแกรมอัดเซสชันเดสก์ท็อป</th>
   <tr>masaüstü oturum kaydedici</tr>
   <uk>a desktop session recorder</uk>
   <vi>a desktop session recorder</vi>
   <zh_CN>a desktop session recorder</zh_CN>
   <zh_HK>a desktop session recorder</zh_HK>
   <zh_TW>a desktop session recorder</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
recordmydesktop
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
recordmydesktop
</uninstall_package_names>

</app>
