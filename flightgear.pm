<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
Flight Gear
</name>

<description>
   <en>Flight Gear Flight Simulator</en>
</description>

<installable>
all
</installable>

<screenshot>
</screenshot>

<preinstall>

</preinstall>

<install_package_names>
flightgear
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
flightgear
</uninstall_package_names>
</app>
