<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Portugese_LO_Latest_main
</name>

<description>
   <am>Portuguese(PT) Language Meta-Package for LibreOffice</am>
   <ar>Portuguese(PT) Language Meta-Package for LibreOffice</ar>
   <be>Portuguese(PT) Language Meta-Package for LibreOffice</be>
   <bg>Portuguese(PT) Language Meta-Package for LibreOffice</bg>
   <bn>Portuguese(PT) Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet de llengua Portuguesa (PT) per LibreOffice</ca>
   <cs>Portuguese(PT) Language Meta-Package for LibreOffice</cs>
   <da>Portuguese(PT) Language Meta-Package for LibreOffice</da>
   <de>Portugiesisches (PT) Sprach-Metapaket für “LibreOffice”</de>
   <el>Μετα-πακέτο γλωσσών στα Πορτογαλικά (PT) για το LibreOffice</el>
   <en>Portuguese(PT) Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Portugués (PT) para LibreOffice</es_ES>
   <es>Metapaquete de idioma Portugués (PT) para LibreOffice</es>
   <et>Portuguese(PT) Language Meta-Package for LibreOffice</et>
   <eu>Portuguese(PT) Language Meta-Package for LibreOffice</eu>
   <fa>Portuguese(PT) Language Meta-Package for LibreOffice</fa>
   <fil_PH>Portuguese(PT) Language Meta-Package for LibreOffice</fil_PH>
   <fi>Portuguese(PT) Language Meta-Package for LibreOffice</fi>
   <fr_BE>Méta-paquet de langue portugaise pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue portugaise pour LibreOffice</fr>
   <gl_ES>Portuguese(PT) Language Meta-Package for LibreOffice</gl_ES>
   <gu>Portuguese(PT) Language Meta-Package for LibreOffice</gu>
   <he_IL>Portuguese(PT) Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु पुर्तगाली (पुर्तगाल) भाषा मेटा-पैकेज</hi>
   <hr>Portuguese(PT) Language Meta-Package for LibreOffice</hr>
   <hu>Portuguese(PT) Language Meta-Package for LibreOffice</hu>
   <id>Portuguese(PT) Language Meta-Package for LibreOffice</id>
   <is>Portuguese(PT) Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua portoghese(PT) per LibreOffice</it>
   <ja>LibreOffice用ポルトガル語（ポルトガル）メタパッケージ</ja>
   <kk>Portuguese(PT) Language Meta-Package for LibreOffice</kk>
   <ko>Portuguese(PT) Language Meta-Package for LibreOffice</ko>
   <ku>Portuguese(PT) Language Meta-Package for LibreOffice</ku>
   <lt>Portuguese(PT) Language Meta-Package for LibreOffice</lt>
   <mk>Portuguese(PT) Language Meta-Package for LibreOffice</mk>
   <mr>Portuguese(PT) Language Meta-Package for LibreOffice</mr>
   <nb_NO>Portuguese(PT) Language Meta-Package for LibreOffice</nb_NO>
   <nb>Portugisisk språkpakke for LibreOffice</nb>
   <nl_BE>Portuguese(PT) Language Meta-Package for LibreOffice</nl_BE>
   <nl>Portuguese(PT) Language Meta-Package for LibreOffice</nl>
   <or>Portuguese(PT) Language Meta-Package for LibreOffice</or>
   <pl>Portuguese(PT) Language Meta-Package for LibreOffice</pl>
   <pt_BR>Português (PT) Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Português (PT) Meta-pacote de idioma para o LibreOffice</pt>
   <ro>Portuguese(PT) Language Meta-Package for LibreOffice</ro>
   <ru>Portuguese(PT) Language Meta-Package for LibreOffice</ru>
   <sk>Portuguese(PT) Language Meta-Package for LibreOffice</sk>
   <sl>Portugalski (PT) jezikovni meta-paket za LibreOffice</sl>
   <so>Portuguese(PT) Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në portugalisht (PT)</sq>
   <sr>Portuguese(PT) Language Meta-Package for LibreOffice</sr>
   <sv>Portugisiskt(PT) Språk Meta-Paket för LibreOffice</sv>
   <th>Portuguese(PT) Language Meta-Package for LibreOffice</th>
   <tr>LibreOffice için Portekizce(PT) Dili Üst-Paketi</tr>
   <uk>Portuguese(PT) Language Meta-Package for LibreOffice</uk>
   <vi>Portuguese(PT) Language Meta-Package for LibreOffice</vi>
   <zh_CN>Portuguese(PT) Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Portuguese(PT) Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Portuguese(PT) Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-pt
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-pt
libreoffice-gtk3
</uninstall_package_names>

</app>
