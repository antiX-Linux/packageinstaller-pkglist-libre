<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Argentina_Default_Firefox_esr
</name>

<description>
   <am>Spanish (Argentina) localisation of Firefox-ESR</am>
   <ar>Spanish (Argentina) localisation of Firefox-ESR</ar>
   <be>Spanish (Argentina) localisation of Firefox-ESR</be>
   <bg>Spanish (Argentina) localisation of Firefox-ESR</bg>
   <bn>Spanish (Argentina) localisation of Firefox-ESR</bn>
   <ca>Localització de Firefox-ESR en Castellà d'Argentina</ca>
   <cs>Spanish (Argentina) localisation of Firefox-ESR</cs>
   <da>Spanish (Argentina) localisation of Firefox-ESR</da>
   <de>Lokalisation für argentinisches Spanisch von “Firefox ESR”</de>
   <el>Ισπανικά (Αργεντινή) για το Firefox-ESR</el>
   <en>Spanish (Argentina) localisation of Firefox-ESR</en>
   <es_ES>Localización en Español (Argentina) de Firefox-ESR</es_ES>
   <es>Localización Español (Argentina) de Firefox ESR</es>
   <et>Spanish (Argentina) localisation of Firefox-ESR</et>
   <eu>Spanish (Argentina) localisation of Firefox-ESR</eu>
   <fa>Spanish (Argentina) localisation of Firefox-ESR</fa>
   <fil_PH>Spanish (Argentina) localisation of Firefox-ESR</fil_PH>
   <fi>Spanish (Argentina) localisation of Firefox-ESR</fi>
   <fr_BE>Localisation en espagnol (Argentine) pour Firefox ESR</fr_BE>
   <fr>Localisation en espagnol (Argentine) pour Firefox ESR</fr>
   <gl_ES>Spanish (Argentina) localisation of Firefox-ESR</gl_ES>
   <gu>Spanish (Argentina) localisation of Firefox-ESR</gu>
   <he_IL>Spanish (Argentina) localisation of Firefox-ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का स्पेनिश(अर्जेंटीना) संस्करण</hi>
   <hr>Spanish (Argentina) localisation of Firefox-ESR</hr>
   <hu>Spanish (Argentina) localisation of Firefox-ESR</hu>
   <id>Spanish (Argentina) localisation of Firefox-ESR</id>
   <is>Spanish (Argentina) localisation of Firefox-ESR</is>
   <it>Localizzazione spagnola (Argentina) di Firefox ESR</it>
   <ja>スペイン語（アルゼンチン）版 Firefox-ESR</ja>
   <kk>Spanish (Argentina) localisation of Firefox-ESR</kk>
   <ko>Spanish (Argentina) localisation of Firefox-ESR</ko>
   <ku>Spanish (Argentina) localisation of Firefox-ESR</ku>
   <lt>Spanish (Argentina) localisation of Firefox-ESR</lt>
   <mk>Spanish (Argentina) localisation of Firefox-ESR</mk>
   <mr>Spanish (Argentina) localisation of Firefox-ESR</mr>
   <nb_NO>Spanish (Argentina) localisation of Firefox-ESR</nb_NO>
   <nb>Spansk (argentinsk) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Spanish (Argentina) localisation of Firefox-ESR</nl_BE>
   <nl>Spaanse (Argentinië) lokalisatie van Firefox-ESR</nl>
   <or>Spanish (Argentina) localisation of Firefox-ESR</or>
   <pl>Spanish (Argentina) localisation of Firefox-ESR</pl>
   <pt_BR>Espanhol (Argentina) Localização para o Firefox ESR</pt_BR>
   <pt>Castelhano (Argentina) Localização para Firefox ESR</pt>
   <ro>Spanish (Argentina) localisation of Firefox-ESR</ro>
   <ru>Spanish (Argentina) localisation of Firefox-ESR</ru>
   <sk>Spanish (Argentina) localisation of Firefox-ESR</sk>
   <sl>Španske (Argentina) krajevne nastavitve za Firefox-ESR</sl>
   <so>Spanish (Argentina) localisation of Firefox-ESR</so>
   <sq>Përkthimi i Firefox-it ESR në spanjisht (Argjentinë)</sq>
   <sr>Spanish (Argentina) localisation of Firefox-ESR</sr>
   <sv>Spansk (Argentina) lokalisering av Firefox</sv>
   <th>Spanish (Argentina) localisation of Firefox-ESR</th>
   <tr>Firefox-ESR İspanyolca (Arjantin) yerelleştirmesi</tr>
   <uk>Spanish (Argentina) localisation of Firefox-ESR</uk>
   <vi>Spanish (Argentina) localisation of Firefox-ESR</vi>
   <zh_CN>Spanish (Argentina) localisation of Firefox-ESR</zh_CN>
   <zh_HK>Spanish (Argentina) localisation of Firefox-ESR</zh_HK>
   <zh_TW>Spanish (Argentina) localisation of Firefox-ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-es-ar
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-es-ar
</uninstall_package_names>

</app>
