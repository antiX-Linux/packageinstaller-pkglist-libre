<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Sinhala_Default_Firefox_esr
</name>

<description>
   <am>Sinhala localisation of Firefox ESR</am>
   <ar>Sinhala localisation of Firefox ESR</ar>
   <be>Sinhala localisation of Firefox ESR</be>
   <bg>Sinhala localisation of Firefox ESR</bg>
   <bn>Sinhala localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Sinhala</ca>
   <cs>Sinhala localisation of Firefox ESR</cs>
   <da>Sinhala localisation of Firefox ESR</da>
   <de>Singhalesische Lokalisation von “Firefox ESR”</de>
   <el>Sinhala για Firefox ESR</el>
   <en>Sinhala localisation of Firefox ESR</en>
   <es_ES>Localización Cingalés de Firefox ESR</es_ES>
   <es>Localización Cingalés de Firefox ESR</es>
   <et>Sinhala localisation of Firefox ESR</et>
   <eu>Sinhala localisation of Firefox ESR</eu>
   <fa>Sinhala localisation of Firefox ESR</fa>
   <fil_PH>Sinhala localisation of Firefox ESR</fil_PH>
   <fi>Sinhala localisation of Firefox ESR</fi>
   <fr_BE>Localisation en cinghalais pour Firefox ESR</fr_BE>
   <fr>Localisation en cinghalais pour Firefox ESR</fr>
   <gl_ES>Sinhala localisation of Firefox ESR</gl_ES>
   <gu>Sinhala localisation of Firefox ESR</gu>
   <he_IL>Sinhala localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का सिंहला संस्करण</hi>
   <hr>Sinhala localisation of Firefox ESR</hr>
   <hu>Sinhala localisation of Firefox ESR</hu>
   <id>Sinhala localisation of Firefox ESR</id>
   <is>Sinhala localisation of Firefox ESR</is>
   <it>Localizzazione sinhala di Firefox ESR</it>
   <ja>シンハラ語版 Firefox ESR</ja>
   <kk>Sinhala localisation of Firefox ESR</kk>
   <ko>Sinhala localisation of Firefox ESR</ko>
   <ku>Sinhala localisation of Firefox ESR</ku>
   <lt>Sinhala localisation of Firefox ESR</lt>
   <mk>Sinhala localisation of Firefox ESR</mk>
   <mr>Sinhala localisation of Firefox ESR</mr>
   <nb_NO>Sinhala localisation of Firefox ESR</nb_NO>
   <nb>Sinhala lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Sinhala localisation of Firefox ESR</nl_BE>
   <nl>Sinhala localisation of Firefox ESR</nl>
   <or>Sinhala localisation of Firefox ESR</or>
   <pl>Sinhala localisation of Firefox ESR</pl>
   <pt_BR>Cingalês Localização para o Firefox ESR</pt_BR>
   <pt>Cingalês Localização para Firefox ESR</pt>
   <ro>Sinhala localisation of Firefox ESR</ro>
   <ru>Sinhala localisation of Firefox ESR</ru>
   <sk>Sinhala localisation of Firefox ESR</sk>
   <sl>Sinhalske krajevne nastavitve za Firefox ESR</sl>
   <so>Sinhala localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në sinhala</sq>
   <sr>Sinhala localisation of Firefox ESR</sr>
   <sv>Sinhala lokalisering av Firefox ESR</sv>
   <th>Sinhala localisation of Firefox ESR</th>
   <tr>Firefox ESR Seylanca yerelleştirmesi</tr>
   <uk>Sinhala localisation of Firefox ESR</uk>
   <vi>Sinhala localisation of Firefox ESR</vi>
   <zh_CN>Sinhala localisation of Firefox ESR</zh_CN>
   <zh_HK>Sinhala localisation of Firefox ESR</zh_HK>
   <zh_TW>Sinhala localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-esr-l10n-si
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-si
</uninstall_package_names>

</app>
