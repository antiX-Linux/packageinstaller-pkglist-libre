<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
German_Default_Firefox_esr
</name>

<description>
   <am>German localisation of Firefox ESR</am>
   <ar>German localisation of Firefox ESR</ar>
   <be>German localisation of Firefox ESR</be>
   <bg>German localisation of Firefox ESR</bg>
   <bn>German localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Alemany</ca>
   <cs>German localisation of Firefox ESR</cs>
   <da>German localisation of Firefox ESR</da>
   <de>Deutsche Lokalisation von “Firefox ESR”</de>
   <el>Γερμανικά για τον Firefox ESR</el>
   <en>German localisation of Firefox ESR</en>
   <es_ES>Localización Alemán de Firefox ESR</es_ES>
   <es>Localización Alemán de Firefox ESR</es>
   <et>German localisation of Firefox ESR</et>
   <eu>German localisation of Firefox ESR</eu>
   <fa>German localisation of Firefox ESR</fa>
   <fil_PH>German localisation of Firefox ESR</fil_PH>
   <fi>German localisation of Firefox ESR</fi>
   <fr_BE>Localisation en allemand pour Firefox ESR</fr_BE>
   <fr>Localisation en allemand pour Firefox ESR</fr>
   <gl_ES>German localisation of Firefox ESR</gl_ES>
   <gu>German localisation of Firefox ESR</gu>
   <he_IL>German localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का जर्मन संस्करण</hi>
   <hr>German localisation of Firefox ESR</hr>
   <hu>German localisation of Firefox ESR</hu>
   <id>German localisation of Firefox ESR</id>
   <is>German localisation of Firefox ESR</is>
   <it>Localizzazione tedesca di Firefox ESR</it>
   <ja>ドイツ語版 Firefox ESR</ja>
   <kk>German localisation of Firefox ESR</kk>
   <ko>German localisation of Firefox ESR</ko>
   <ku>German localisation of Firefox ESR</ku>
   <lt>German localisation of Firefox ESR</lt>
   <mk>German localisation of Firefox ESR</mk>
   <mr>German localisation of Firefox ESR</mr>
   <nb_NO>German localisation of Firefox ESR</nb_NO>
   <nb>Tysk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>German localisation of Firefox ESR</nl_BE>
   <nl>Duitse lokalisatie van Firefox ESR</nl>
   <or>German localisation of Firefox ESR</or>
   <pl>German localisation of Firefox ESR</pl>
   <pt_BR>Alemão Localização para o Firefox ESR</pt_BR>
   <pt>Alemão Localização para Firefox ESR</pt>
   <ro>German localisation of Firefox ESR</ro>
   <ru>German localisation of Firefox ESR</ru>
   <sk>German localisation of Firefox ESR</sk>
   <sl>Nemške krajevne nastavitve za Firefox ESR</sl>
   <so>German localisation of Firefox ESR</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në gjeorgjisht</sq>
   <sr>German localisation of Firefox ESR</sr>
   <sv>Tysk lokalisering av Firefox ESR</sv>
   <th>German localisation of Firefox ESR</th>
   <tr>Firefox ESR Almanca yerelleştirmesi</tr>
   <uk>German localisation of Firefox ESR</uk>
   <vi>German localisation of Firefox ESR</vi>
   <zh_CN>German localisation of Firefox ESR</zh_CN>
   <zh_HK>German localisation of Firefox ESR</zh_HK>
   <zh_TW>German localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-de
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-de
</uninstall_package_names>

</app>
