<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Dutch_Default_Firefox_esr
</name>

<description>
   <am>Dutch localisation of Firefox ESR</am>
   <ar>Dutch localisation of Firefox ESR</ar>
   <be>Dutch localisation of Firefox ESR</be>
   <bg>Dutch localisation of Firefox ESR</bg>
   <bn>Dutch localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Holandès</ca>
   <cs>Dutch localisation of Firefox ESR</cs>
   <da>Dutch localisation of Firefox ESR</da>
   <de>Niederländische Lokalisation von “Firefox ESR”</de>
   <el>Ολλανδικά για Firefox ESR</el>
   <en>Dutch localisation of Firefox ESR</en>
   <es_ES>Localización Holandés de Firefox ESR</es_ES>
   <es>Localización Holandés de Firefox ESR</es>
   <et>Dutch localisation of Firefox ESR</et>
   <eu>Dutch localisation of Firefox ESR</eu>
   <fa>Dutch localisation of Firefox ESR</fa>
   <fil_PH>Dutch localisation of Firefox ESR</fil_PH>
   <fi>Dutch localisation of Firefox ESR</fi>
   <fr_BE>Localisation en néerlandais pour Firefox ESR</fr_BE>
   <fr>Localisation en néerlandais pour Firefox ESR</fr>
   <gl_ES>Dutch localisation of Firefox ESR</gl_ES>
   <gu>Dutch localisation of Firefox ESR</gu>
   <he_IL>Dutch localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का डच संस्करण</hi>
   <hr>Dutch localisation of Firefox ESR</hr>
   <hu>Dutch localisation of Firefox ESR</hu>
   <id>Dutch localisation of Firefox ESR</id>
   <is>Dutch localisation of Firefox ESR</is>
   <it>Localizzazione olandese di Firefox ESR</it>
   <ja>オランダ語版 Firefox ESR</ja>
   <kk>Dutch localisation of Firefox ESR</kk>
   <ko>Dutch localisation of Firefox ESR</ko>
   <ku>Dutch localisation of Firefox ESR</ku>
   <lt>Dutch localisation of Firefox ESR</lt>
   <mk>Dutch localisation of Firefox ESR</mk>
   <mr>Dutch localisation of Firefox ESR</mr>
   <nb_NO>Dutch localisation of Firefox ESR</nb_NO>
   <nb>Nederlandsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Dutch localisation of Firefox ESR</nl_BE>
   <nl>Nederlandse lokalisatie van Firefox ESR</nl>
   <or>Dutch localisation of Firefox ESR</or>
   <pl>Dutch localisation of Firefox ESR</pl>
   <pt_BR>Holandês Localização para o Firefox ESR</pt_BR>
   <pt>Holandês Localização para Firefox ESR</pt>
   <ro>Dutch localisation of Firefox ESR</ro>
   <ru>Dutch localisation of Firefox ESR</ru>
   <sk>Dutch localisation of Firefox ESR</sk>
   <sl>Nizozemske krajevne nastavitve za Firefox ESR</sl>
   <so>Dutch localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në holandisht</sq>
   <sr>Dutch localisation of Firefox ESR</sr>
   <sv>Holländsk lokalisering av Firefox ESR</sv>
   <th>Dutch localisation of Firefox ESR</th>
   <tr>Firefox ESR Flemenkçe yerelleştirmesi</tr>
   <uk>Dutch localisation of Firefox ESR</uk>
   <vi>Dutch localisation of Firefox ESR</vi>
   <zh_CN>Dutch localisation of Firefox ESR</zh_CN>
   <zh_HK>Dutch localisation of Firefox ESR</zh_HK>
   <zh_TW>Dutch localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-nl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-nl
</uninstall_package_names>

</app>
