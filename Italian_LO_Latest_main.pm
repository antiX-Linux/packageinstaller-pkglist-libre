<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Italian_LO_Latest_main
</name>

<description>
   <am>Italian LibreOffice Language Meta-Package</am>
   <ar>Italian LibreOffice Language Meta-Package</ar>
   <be>Italian LibreOffice Language Meta-Package</be>
   <bg>Italian LibreOffice Language Meta-Package</bg>
   <bn>Italian LibreOffice Language Meta-Package</bn>
   <ca>Metapaquet de llenguatge italià per a LibreOffice</ca>
   <cs>Italian LibreOffice Language Meta-Package</cs>
   <da>Italiensk LibreOffice sprog-metapakke</da>
   <de>Italienisches LibreOffice Meta-Paket</de>
   <el>LibreOffice στα Ιταλικά</el>
   <en>Italian LibreOffice Language Meta-Package</en>
   <es_ES>Meta-Paquete de Idioma Italiano LibreOffice</es_ES>
   <es>Metapaquete de Idioma Italiano LibreOffice</es>
   <et>Italian LibreOffice Language Meta-Package</et>
   <eu>Italian LibreOffice Language Meta-Package</eu>
   <fa>Italian LibreOffice Language Meta-Package</fa>
   <fil_PH>Italian LibreOffice Language Meta-Package</fil_PH>
   <fi>Italialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue italienne pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue italienne pour LibreOffice</fr>
   <gl_ES>Italiano Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Italian LibreOffice Language Meta-Package</gu>
   <he_IL>Italian LibreOffice Language Meta-Package</he_IL>
   <hi>लिब्रे-ऑफिस हेतु इतालवी भाषा मेटा-पैकेज</hi>
   <hr>Italian LibreOffice Language Meta-Package</hr>
   <hu>Italian LibreOffice Language Meta-Package</hu>
   <id>Italian LibreOffice Language Meta-Package</id>
   <is>Italian LibreOffice Language Meta-Package</is>
   <it>Meta-pacchetto della lingua italiana per LibreOffice</it>
   <ja>イタリア語 LibreOffice 言語メタパッケージ</ja>
   <kk>Italian LibreOffice Language Meta-Package</kk>
   <ko>Italian LibreOffice Language Meta-Package</ko>
   <ku>Italian LibreOffice Language Meta-Package</ku>
   <lt>Italian LibreOffice Language Meta-Package</lt>
   <mk>Italian LibreOffice Language Meta-Package</mk>
   <mr>Italian LibreOffice Language Meta-Package</mr>
   <nb_NO>Italian LibreOffice Language Meta-Package</nb_NO>
   <nb>Italiensk språkpakke for LibreOffice</nb>
   <nl_BE>Italian LibreOffice Language Meta-Package</nl_BE>
   <nl>Italiaanse LibreOffice Taal Meta-Pakket</nl>
   <or>Italian LibreOffice Language Meta-Package</or>
   <pl>Włoski metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Italiano Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Italiano Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Italian LibreOffice Language Meta-Package</ro>
   <ru>Italian LibreOffice Language Meta-Package</ru>
   <sk>Italian LibreOffice Language Meta-Package</sk>
   <sl>Itlaijanski jezikovni metapaket za LibreOffice</sl>
   <so>Italian LibreOffice Language Meta-Package</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në italisht</sq>
   <sr>Italian LibreOffice Language Meta-Package</sr>
   <sv>Italienskt LibreOffice Språk-Meta-Paket</sv>
   <th>Meta-Package ภาษา Italian สำหรับ LibreOffice</th>
   <tr>LibreOffice için İtalyanca Dili Üst-Paketi</tr>
   <uk>Italian LibreOffice Language Meta-Package</uk>
   <vi>Italian LibreOffice Language Meta-Package</vi>
   <zh_CN>Italian LibreOffice Language Meta-Package</zh_CN>
   <zh_HK>Italian LibreOffice Language Meta-Package</zh_HK>
   <zh_TW>Italian LibreOffice Language Meta-Package</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-it
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-it
libreoffice-gtk3
</uninstall_package_names>

</app>
