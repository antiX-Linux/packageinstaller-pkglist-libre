<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Turkish_Firefox
</name>

<description>
   <am>Turkish localisation of Firefox</am>
   <ar>Turkish localisation of Firefox</ar>
   <be>Turkish localisation of Firefox</be>
   <bg>Turkish localisation of Firefox</bg>
   <bn>Turkish localisation of Firefox</bn>
   <ca>Localització de Firefox en Turc</ca>
   <cs>Turkish localisation of Firefox</cs>
   <da>Tyrkisk oversættelse af Firefox</da>
   <de>Türkische Lokalisierung von Firefox</de>
   <el>Τουρκικά για το Firefox</el>
   <en>Turkish localisation of Firefox</en>
   <es_ES>Localización Turca de Firefox</es_ES>
   <es>Localización Turco de Firefox</es>
   <et>Turkish localisation of Firefox</et>
   <eu>Turkish localisation of Firefox</eu>
   <fa>Turkish localisation of Firefox</fa>
   <fil_PH>Turkish localisation of Firefox</fil_PH>
   <fi>Turkkilainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en turque pour Firefox</fr_BE>
   <fr>Localisation en turque pour Firefox</fr>
   <gl_ES>Localización de Firefox ao turco</gl_ES>
   <gu>Turkish localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לטורקית</he_IL>
   <hi>फायरफॉक्स का तुर्क संस्करण</hi>
   <hr>Turkish localisation of Firefox</hr>
   <hu>Turkish localisation of Firefox</hu>
   <id>Turkish localisation of Firefox</id>
   <is>Turkish localisation of Firefox</is>
   <it>Localizzazione turca di Firefox</it>
   <ja>Firefox のトルコ語版</ja>
   <kk>Turkish localisation of Firefox</kk>
   <ko>Turkish localisation of Firefox</ko>
   <ku>Turkish localisation of Firefox</ku>
   <lt>Turkish localisation of Firefox</lt>
   <mk>Turkish localisation of Firefox</mk>
   <mr>Turkish localisation of Firefox</mr>
   <nb_NO>Turkish localisation of Firefox</nb_NO>
   <nb>Tyrkisk lokaltilpassing av Firefox</nb>
   <nl_BE>Turkish localisation of Firefox</nl_BE>
   <nl>Turkse lokalisatie van Firefox</nl>
   <or>Turkish localisation of Firefox</or>
   <pl>Turecka lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Turco Localização para o Firefox</pt_BR>
   <pt>Turco Localização para Firefox</pt>
   <ro>Turkish localisation of Firefox</ro>
   <ru>Турецкая локализация Firefox</ru>
   <sk>Turkish localisation of Firefox</sk>
   <sl>Turške krajevne nastavitve za Firefox</sl>
   <so>Turkish localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në turqisht</sq>
   <sr>Turkish localisation of Firefox</sr>
   <sv>Turkisk lokalisering av Firefox</sv>
   <th>Turkish localisation ของ Firefox</th>
   <tr>Firefox'un Türkçe yerelleştirmesi</tr>
   <uk>Turkish localisation of Firefox</uk>
   <vi>Turkish localisation of Firefox</vi>
   <zh_CN>Turkish localisation of Firefox</zh_CN>
   <zh_HK>Turkish localisation of Firefox</zh_HK>
   <zh_TW>Turkish localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-l10n-tr
</install_package_names>

<postinstall>
</postinstall>

<uninstall_package_names>
firefox-l10n-tr
</uninstall_package_names>

</app>
