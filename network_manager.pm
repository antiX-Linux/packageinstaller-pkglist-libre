<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
Network Manager
</name>

<description>
   <am>System network service to manage network devices and connections</am>
   <ar>System network service to manage network devices and connections</ar>
   <be>System network service to manage network devices and connections</be>
   <bg>System network service to manage network devices and connections</bg>
   <bn>System network service to manage network devices and connections</bn>
   <ca>Servei de xarxa del sistema per gestionar dispositius i connexions</ca>
   <cs>System network service to manage network devices and connections</cs>
   <da>System network service to manage network devices and connections</da>
   <de>Netzwerk-Dienstprogramm für die Verwaltung von Netzwerkgeräten und Verbindungen</de>
   <el>Υπηρεσία δικτύου συστήματος για τη διαχείριση συσκευών δικτύου και συνδέσεων</el>
   <en>System network service to manage network devices and connections</en>
   <es_ES>Servicio de red del sistema para administrar dispositivos y conexiones de red</es_ES>
   <es>Servicio de red del sistema para administrar dispositivos y conexiones de red</es>
   <et>System network service to manage network devices and connections</et>
   <eu>System network service to manage network devices and connections</eu>
   <fa>System network service to manage network devices and connections</fa>
   <fil_PH>System network service to manage network devices and connections</fil_PH>
   <fi>Järjestelmän verkkopalvelu verkon laitteiden ja yhteyksien hallintaan</fi>
   <fr_BE>Service réseau du système pour gérer les périphériques et les connexions du réseau</fr_BE>
   <fr>Service réseau du système pour gérer les périphériques et les connexions du réseau</fr>
   <gl_ES>System network service to manage network devices and connections</gl_ES>
   <gu>System network service to manage network devices and connections</gu>
   <he_IL>System network service to manage network devices and connections</he_IL>
   <hi>नेटवर्क उपकरणों व कनेक्शन के प्रबंधन हेतु सिस्टम नेटवर्क सेवा</hi>
   <hr>System network service to manage network devices and connections</hr>
   <hu>System network service to manage network devices and connections</hu>
   <id>System network service to manage network devices and connections</id>
   <is>System network service to manage network devices and connections</is>
   <it>Servizio di rete di sistema per gestire i dispositivi e le connessioni di rete</it>
   <ja>ネットワーク機器や接続を管理するシステム・ネットワークサービス</ja>
   <kk>System network service to manage network devices and connections</kk>
   <ko>System network service to manage network devices and connections</ko>
   <ku>System network service to manage network devices and connections</ku>
   <lt>System network service to manage network devices and connections</lt>
   <mk>System network service to manage network devices and connections</mk>
   <mr>System network service to manage network devices and connections</mr>
   <nb_NO>System network service to manage network devices and connections</nb_NO>
   <nb>Nettverkstjeneste for behandling av nettverksenheter og tilkoblinger</nb>
   <nl_BE>System network service to manage network devices and connections</nl_BE>
   <nl>System network service to manage network devices and connections</nl>
   <or>System network service to manage network devices and connections</or>
   <pl>System network service to manage network devices and connections</pl>
   <pt_BR>Serviço de rede do sistema para gerenciar dispositivos e conexões de rede</pt_BR>
   <pt>Serviço de sistema para gerir dispositivos e ligações de rede</pt>
   <ro>System network service to manage network devices and connections</ro>
   <ru>System network service to manage network devices and connections</ru>
   <sk>System network service to manage network devices and connections</sk>
   <sl>Sistemska omrežna storitev za upravljanje omrežnih naprav in povezav</sl>
   <so>System network service to manage network devices and connections</so>
   <sq>Shërbim rrjeti sistemi për të administruar pajisje dhe lidhje rrjeti</sq>
   <sr>System network service to manage network devices and connections</sr>
   <sv>System-nätverks service för att hantera nätverksenheter och anslutningar</sv>
   <th>System network service to manage network devices and connections</th>
   <tr>Ağ aygıtlarını ve bağlantılarını yönetmek için sistem ağ hizmeti</tr>
   <uk>System network service to manage network devices and connections</uk>
   <vi>System network service to manage network devices and connections</vi>
   <zh_CN>System network service to manage network devices and connections</zh_CN>
   <zh_HK>System network service to manage network devices and connections</zh_HK>
   <zh_TW>System network service to manage network devices and connections</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
network-manager
network-manager-gnome
network-manager-openconnect
network-manager-openconnect-gnome
network-manager-openvpn
network-manager-openvpn-gnome
network-manager-pptp
network-manager-vpnc
mobile-broadband-provider-info
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
network-manager
network-manager-gnome
network-manager-openconnect
network-manager-openconnect-gnome
network-manager-openvpn
network-manager-openvpn-gnome
network-manager-pptp
network-manager-vpnc
mobile-broadband-provider-info
</uninstall_package_names>
</app>
