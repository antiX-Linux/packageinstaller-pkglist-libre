<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Norwegian_Thunderbird
</name>

<description>
   <am>Norwegian localisation of Thunderbird</am>
   <ar>Norwegian localisation of Thunderbird</ar>
   <be>Norwegian localisation of Thunderbird</be>
   <bg>Norwegian localisation of Thunderbird</bg>
   <bn>Norwegian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Noruec</ca>
   <cs>Norwegian localisation of Thunderbird</cs>
   <da>Norsk oversættelse af Thunderbird</da>
   <de>Norwegische Lokalisierung von Thunderbird</de>
   <el>Νορβηγικά για το Thunderbird</el>
   <en>Norwegian localisation of Thunderbird</en>
   <es_ES>Localización Noruega de Thunderbird</es_ES>
   <es>Localización Noruego de Thunderbird</es>
   <et>Norwegian localisation of Thunderbird</et>
   <eu>Norwegian localisation of Thunderbird</eu>
   <fa>Norwegian localisation of Thunderbird</fa>
   <fil_PH>Norwegian localisation of Thunderbird</fil_PH>
   <fi>Norjalainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en norvégien pour Thunderbird</fr_BE>
   <fr>Localisation en norvégien pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao noruego</gl_ES>
   <gu>Norwegian localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לנורווגית</he_IL>
   <hi>थंडरबर्ड का नार्वेजियन संस्करण</hi>
   <hr>Norwegian localisation of Thunderbird</hr>
   <hu>Norwegian localisation of Thunderbird</hu>
   <id>Norwegian localisation of Thunderbird</id>
   <is>Norwegian localisation of Thunderbird</is>
   <it>Localizzazione norvegese di Thunderbird</it>
   <ja>Thunderbird のノルウェー語版</ja>
   <kk>Norwegian localisation of Thunderbird</kk>
   <ko>Norwegian localisation of Thunderbird</ko>
   <ku>Norwegian localisation of Thunderbird</ku>
   <lt>Norwegian localisation of Thunderbird</lt>
   <mk>Norwegian localisation of Thunderbird</mk>
   <mr>Norwegian localisation of Thunderbird</mr>
   <nb_NO>Norwegian localisation of Thunderbird</nb_NO>
   <nb>Norsk bokmålsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Norwegian localisation of Thunderbird</nl_BE>
   <nl>Noorse lokalisatie van Thunderbird</nl>
   <or>Norwegian localisation of Thunderbird</or>
   <pl>Norweska lokalizacja Thunderbirda</pl>
   <pt_BR>Norueguês Localização para o Thunderbird</pt_BR>
   <pt>Norueguês Localização para Thunderbird</pt>
   <ro>Norwegian localisation of Thunderbird</ro>
   <ru>Norwegian localisation of Thunderbird</ru>
   <sk>Norwegian localisation of Thunderbird</sk>
   <sl>Norveške krajevne nastavitve za Thunderbird</sl>
   <so>Norwegian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në norvegjisht</sq>
   <sr>Norwegian localisation of Thunderbird</sr>
   <sv>Norsk lokalisering av Thunderbird</sv>
   <th>Norwegian localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Norveççe yerelleştirmesi</tr>
   <uk>Norwegian локалізація Thunderbird</uk>
   <vi>Norwegian localisation of Thunderbird</vi>
   <zh_CN>Norwegian localisation of Thunderbird</zh_CN>
   <zh_HK>Norwegian localisation of Thunderbird</zh_HK>
   <zh_TW>Norwegian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-xpi-nb-no
</install_package_names>

<postinstall>
</postinstall>

<uninstall_package_names>
thunderbird-l10n-xpi-nb-no
</uninstall_package_names>

</app>
