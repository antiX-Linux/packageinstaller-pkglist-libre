<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Macedonian_Default_Firefox_esr
</name>

<description>
   <am>Macedonian localisation of Firefox ESR</am>
   <ar>Macedonian localisation of Firefox ESR</ar>
   <be>Macedonian localisation of Firefox ESR</be>
   <bg>Macedonian localisation of Firefox ESR</bg>
   <bn>Macedonian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Macedoni</ca>
   <cs>Macedonian localisation of Firefox ESR</cs>
   <da>Macedonian localisation of Firefox ESR</da>
   <de>Mazedonische Lokalisation von “Firefox ESR”</de>
   <el>Mακεδονικά για Firefox ESR</el>
   <en>Macedonian localisation of Firefox ESR</en>
   <es_ES>Localización Macedonio de Firefox ESR</es_ES>
   <es>Localización Macedonio de Firefox ESR</es>
   <et>Macedonian localisation of Firefox ESR</et>
   <eu>Macedonian localisation of Firefox ESR</eu>
   <fa>Macedonian localisation of Firefox ESR</fa>
   <fil_PH>Macedonian localisation of Firefox ESR</fil_PH>
   <fi>Macedonian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en macédonien pour Firefox ESR</fr_BE>
   <fr>Localisation en macédonien pour Firefox ESR</fr>
   <gl_ES>Macedonian localisation of Firefox ESR</gl_ES>
   <gu>Macedonian localisation of Firefox ESR</gu>
   <he_IL>Macedonian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का मकदूनियाई संस्करण</hi>
   <hr>Macedonian localisation of Firefox ESR</hr>
   <hu>Macedonian localisation of Firefox ESR</hu>
   <id>Macedonian localisation of Firefox ESR</id>
   <is>Macedonian localisation of Firefox ESR</is>
   <it>Localizzazione macedonian di Firefox ESR</it>
   <ja>マケドニア語版 Firefox ESR</ja>
   <kk>Macedonian localisation of Firefox ESR</kk>
   <ko>Macedonian localisation of Firefox ESR</ko>
   <ku>Macedonian localisation of Firefox ESR</ku>
   <lt>Macedonian localisation of Firefox ESR</lt>
   <mk>Macedonian localisation of Firefox ESR</mk>
   <mr>Macedonian localisation of Firefox ESR</mr>
   <nb_NO>Macedonian localisation of Firefox ESR</nb_NO>
   <nb>Makedonsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Macedonian localisation of Firefox ESR</nl_BE>
   <nl>Macedonian localisation of Firefox ESR</nl>
   <or>Macedonian localisation of Firefox ESR</or>
   <pl>Macedonian localisation of Firefox ESR</pl>
   <pt_BR>Macedônio Localização para o Firefox ESR</pt_BR>
   <pt>Macedónio Localização para Firefox ESR</pt>
   <ro>Macedonian localisation of Firefox ESR</ro>
   <ru>Macedonian localisation of Firefox ESR</ru>
   <sk>Macedonian localisation of Firefox ESR</sk>
   <sl>Makedonske krajevne nastavitve za Firefox ESR</sl>
   <so>Macedonian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në maqedonisht</sq>
   <sr>Macedonian localisation of Firefox ESR</sr>
   <sv>Makedonisk lokalisering av Firefox ESR</sv>
   <th>Macedonian localisation of Firefox ESR</th>
   <tr>Firefox ESR Makedonca yerelleştirmesi</tr>
   <uk>Macedonian localisation of Firefox ESR</uk>
   <vi>Macedonian localisation of Firefox ESR</vi>
   <zh_CN>Macedonian localisation of Firefox ESR</zh_CN>
   <zh_HK>Macedonian localisation of Firefox ESR</zh_HK>
   <zh_TW>Macedonian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-mk
</install_package_names>

<postinstall>
</postinstall>

<uninstall_package_names>
firefox-esr-l10n-mk
</uninstall_package_names>

</app>
