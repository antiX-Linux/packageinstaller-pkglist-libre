<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Asturian_Thunderbird
</name>

<description>
   <am>Asturian localisation of Thunderbird</am>
   <ar>Asturian localisation of Thunderbird</ar>
   <be>Asturian localisation of Thunderbird</be>
   <bg>Asturian localisation of Thunderbird</bg>
   <bn>Asturian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Asturià</ca>
   <cs>Asturian localisation of Thunderbird</cs>
   <da>Asturian localisation of Thunderbird</da>
   <de>Asturleonesische Lokalisation von “Thunderbird”</de>
   <el>Asturian για Thunderbird</el>
   <en>Asturian localisation of Thunderbird</en>
   <es_ES>Localización Asturiana de Thunderbird</es_ES>
   <es>Localización Asturiano de Thunderbird</es>
   <et>Asturian localisation of Thunderbird</et>
   <eu>Asturian localisation of Thunderbird</eu>
   <fa>Asturian localisation of Thunderbird</fa>
   <fil_PH>Asturian localisation of Thunderbird</fil_PH>
   <fi>Asturian localisation of Thunderbird</fi>
   <fr_BE>Localisation en asturien pour Thunderbird</fr_BE>
   <fr>Localisation en asturien pour Thunderbird</fr>
   <gl_ES>Asturian localisation of Thunderbird</gl_ES>
   <gu>Asturian localisation of Thunderbird</gu>
   <he_IL>Asturian localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का अस्तुरियाई संस्करण</hi>
   <hr>Asturian localisation of Thunderbird</hr>
   <hu>Asturian localisation of Thunderbird</hu>
   <id>Asturian localisation of Thunderbird</id>
   <is>Asturian localisation of Thunderbird</is>
   <it>Localizzazione asturiana di Thunderbird</it>
   <ja>アストゥリアス語版 Thunderbird</ja>
   <kk>Asturian localisation of Thunderbird</kk>
   <ko>Asturian localisation of Thunderbird</ko>
   <ku>Asturian localisation of Thunderbird</ku>
   <lt>Asturian localisation of Thunderbird</lt>
   <mk>Asturian localisation of Thunderbird</mk>
   <mr>Asturian localisation of Thunderbird</mr>
   <nb_NO>Asturian localisation of Thunderbird</nb_NO>
   <nb>Asturisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Asturian localisation of Thunderbird</nl_BE>
   <nl>Asturiaans lokalisatie van Thunderbird</nl>
   <or>Asturian localisation of Thunderbird</or>
   <pl>Asturian localisation of Thunderbird</pl>
   <pt_BR>Asturiano Localização para o Thunderbird</pt_BR>
   <pt>Asturiano Localização para Thunderbird</pt>
   <ro>Asturian localisation of Thunderbird</ro>
   <ru>Asturian localisation of Thunderbird</ru>
   <sk>Asturian localisation of Thunderbird</sk>
   <sl>Asturijske krajevne nastavitve za Thunderbird</sl>
   <so>Asturian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në asturisht</sq>
   <sr>Asturian localisation of Thunderbird</sr>
   <sv>Asturisk lokalisering av Thunderbird</sv>
   <th>Asturian localisation of Thunderbird</th>
   <tr>Thunderbird Asturyan Dili yerelleştirmesi</tr>
   <uk>Asturian localisation of Thunderbird</uk>
   <vi>Asturian localisation of Thunderbird</vi>
   <zh_CN>Asturian localisation of Thunderbird</zh_CN>
   <zh_HK>Asturian localisation of Thunderbird</zh_HK>
   <zh_TW>Asturian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-ast
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-ast
</uninstall_package_names>

</app>
