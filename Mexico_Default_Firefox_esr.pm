<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Mexico_Default_Firefox_esr
</name>

<description>
   <am>Spanish (Mexico) localisation of Firefox ESR</am>
   <ar>Spanish (Mexico) localisation of Firefox ESR</ar>
   <be>Spanish (Mexico) localisation of Firefox ESR</be>
   <bg>Spanish (Mexico) localisation of Firefox ESR</bg>
   <bn>Spanish (Mexico) localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Castellà (Mèxic)</ca>
   <cs>Spanish (Mexico) localisation of Firefox ESR</cs>
   <da>Spanish (Mexico) localisation of Firefox ESR</da>
   <de>Lokalisation für mexikanisches Spanisch von “Firefox ESR”</de>
   <el>Ισπανικά (Μεξικό) για Firefox ESR</el>
   <en>Spanish (Mexico) localisation of Firefox ESR</en>
   <es_ES>Localización en Español (Mexico) de Firefox ESR</es_ES>
   <es>Localización Español (México) de Firefox ESR</es>
   <et>Spanish (Mexico) localisation of Firefox ESR</et>
   <eu>Spanish (Mexico) localisation of Firefox ESR</eu>
   <fa>Spanish (Mexico) localisation of Firefox ESR</fa>
   <fil_PH>Spanish (Mexico) localisation of Firefox ESR</fil_PH>
   <fi>Spanish (Mexico) localisation of Firefox ESR</fi>
   <fr_BE>Localisation en espagnol (Mexique) pour Firefox ESR</fr_BE>
   <fr>Localisation en espagnol (Mexique) pour Firefox ESR</fr>
   <gl_ES>Spanish (Mexico) localisation of Firefox ESR</gl_ES>
   <gu>Spanish (Mexico) localisation of Firefox ESR</gu>
   <he_IL>Spanish (Mexico) localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का स्पेनिश (मेक्सिको) संस्करण</hi>
   <hr>Spanish (Mexico) localisation of Firefox ESR</hr>
   <hu>Spanish (Mexico) localisation of Firefox ESR</hu>
   <id>Spanish (Mexico) localisation of Firefox ESR</id>
   <is>Spanish (Mexico) localisation of Firefox ESR</is>
   <it>Localizzazione spagnola (Messico) di Firefox ESR</it>
   <ja>スペイン語（メキシコ）版 Firefox ESR</ja>
   <kk>Spanish (Mexico) localisation of Firefox ESR</kk>
   <ko>Spanish (Mexico) localisation of Firefox ESR</ko>
   <ku>Spanish (Mexico) localisation of Firefox ESR</ku>
   <lt>Spanish (Mexico) localisation of Firefox ESR</lt>
   <mk>Spanish (Mexico) localisation of Firefox ESR</mk>
   <mr>Spanish (Mexico) localisation of Firefox ESR</mr>
   <nb_NO>Spanish (Mexico) localisation of Firefox ESR</nb_NO>
   <nb>Spansk (Mexico) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Spanish (Mexico) localisation of Firefox ESR</nl_BE>
   <nl>Spanish (Mexico) localisation of Firefox ESR</nl>
   <or>Spanish (Mexico) localisation of Firefox ESR</or>
   <pl>Spanish (Mexico) localisation of Firefox ESR</pl>
   <pt_BR>Espanhol (México) Localização para o Firefox ESR</pt_BR>
   <pt>Castelhano (México) Localização para Firefox ESR</pt>
   <ro>Spanish (Mexico) localisation of Firefox ESR</ro>
   <ru>Spanish (Mexico) localisation of Firefox ESR</ru>
   <sk>Spanish (Mexico) localisation of Firefox ESR</sk>
   <sl>Španske (Mehika) krajevne nastavitve za Firefox-ESR</sl>
   <so>Spanish (Mexico) localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në spanjisht (Meksikë)</sq>
   <sr>Spanish (Mexico) localisation of Firefox ESR</sr>
   <sv>Spansk (Mexiko) lokalisering av Firefox ESR</sv>
   <th>Spanish (Mexico) localisation of Firefox ESR</th>
   <tr>Firefox ESR İspanyolca (Meksika) yerelleştirmesi</tr>
   <uk>Spanish (Mexico) localisation of Firefox ESR</uk>
   <vi>Spanish (Mexico) localisation of Firefox ESR</vi>
   <zh_CN>Spanish (Mexico) localisation of Firefox ESR</zh_CN>
   <zh_HK>Spanish (Mexico) localisation of Firefox ESR</zh_HK>
   <zh_TW>Spanish (Mexico) localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-es-mx
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-es-mx
</uninstall_package_names>

</app>
