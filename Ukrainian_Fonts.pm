<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Ukrainian_Fonts
</name>

<description>
   <am>Ukrainian fonts packages</am>
   <ar>Ukrainian fonts packages</ar>
   <be>Ukrainian fonts packages</be>
   <bg>Ukrainian fonts packages</bg>
   <bn>Ukrainian fonts packages</bn>
   <ca>Paquets de tipus de lletra Ciríl·lics (Ucrainès)</ca>
   <cs>Ukrainian fonts packages</cs>
   <da>Ukrainsk skrifttyper-pakke</da>
   <de>Ukrainische Schriftenpakete</de>
   <el>Ουκρανικά πακέτα γραμματοσειρών</el>
   <en>Ukrainian fonts packages</en>
   <es_ES>Paquetes de fuentes ucranianas</es_ES>
   <es>Paquetes de fuentes ucranianas</es>
   <et>Ukrainian fonts packages</et>
   <eu>Ukrainian fonts packages</eu>
   <fa>Ukrainian fonts packages</fa>
   <fil_PH>Ukrainian fonts packages</fil_PH>
   <fi>Ukrainalaiset kirjasinpaketit</fi>
   <fr_BE>Paquets pour polices ukrainiennes</fr_BE>
   <fr>Paquets pour polices ukrainiennes</fr>
   <gl_ES>Paquetes de fontes ucraniano</gl_ES>
   <gu>Ukrainian fonts packages</gu>
   <he_IL>Ukrainian fonts packages</he_IL>
   <hi>यूक्रेनी मुद्रलिपि पैकेज</hi>
   <hr>Ukrainian fonts packages</hr>
   <hu>Ukrainian fonts packages</hu>
   <id>Ukrainian fonts packages</id>
   <is>Ukrainian fonts packages</is>
   <it>Pacchetti di fonts per l'Ucraino</it>
   <ja>ウクライナ語のフォントパッケージ</ja>
   <kk>Ukrainian fonts packages</kk>
   <ko>Ukrainian fonts packages</ko>
   <ku>Ukrainian fonts packages</ku>
   <lt>Ukrainian fonts packages</lt>
   <mk>Ukrainian fonts packages</mk>
   <mr>Ukrainian fonts packages</mr>
   <nb_NO>Ukrainian fonts packages</nb_NO>
   <nb>Pakke med ukrainske skrifttyper</nb>
   <nl_BE>Ukrainian fonts packages</nl_BE>
   <nl>Oekraïense font pakketten</nl>
   <or>Ukrainian fonts packages</or>
   <pl>pakiety z ukraińskimi fontami (czcionkami)</pl>
   <pt_BR>Pacotes de Fontes Ucraniano</pt_BR>
   <pt>Ucraniano Pacotes de fontes</pt>
   <ro>Ukrainian fonts packages</ro>
   <ru>Ukrainian fonts packages</ru>
   <sk>Ukrainian fonts packages</sk>
   <sl>Ukrajinski paketi pisav</sl>
   <so>Ukrainian fonts packages</so>
   <sq>Paketa shkronjash për ukrainishten</sq>
   <sr>Ukrainian fonts packages</sr>
   <sv>Ukrainska typsnitts-paket</sv>
   <th>แพ็กเกจฟอนต์ภาษา Ukrainian</th>
   <tr>Ukraynaca yazı tipi paketleri</tr>
   <uk>Ukrainian fonts packages</uk>
   <vi>Ukrainian fonts packages</vi>
   <zh_CN>Ukrainian fonts packages</zh_CN>
   <zh_HK>Ukrainian fonts packages</zh_HK>
   <zh_TW>Ukrainian fonts packages</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
xfonts-cronyx-100dpi
xfonts-cronyx-75dpi
xfonts-cronyx-isocyr-100dpi
xfonts-cronyx-isocyr-75dpi
xfonts-cronyx-isocyr-misc
xfonts-cronyx-koi8r-100dpi
xfonts-cronyx-koi8r-75dpi
xfonts-cronyx-koi8r-misc
xfonts-cronyx-misc
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
xfonts-cronyx-100dpi
xfonts-cronyx-75dpi
xfonts-cronyx-isocyr-100dpi
xfonts-cronyx-isocyr-75dpi
xfonts-cronyx-isocyr-misc
xfonts-cronyx-koi8r-100dpi
xfonts-cronyx-koi8r-75dpi
xfonts-cronyx-koi8r-misc
xfonts-cronyx-misc
</uninstall_package_names>

</app>
