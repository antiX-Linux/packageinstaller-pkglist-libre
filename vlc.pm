<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Video
</category>

<name>
VLC
</name>

<description>
   <am>VideoLAN project's media player</am>
   <ar>VideoLAN project's media player</ar>
   <be>VideoLAN project's media player</be>
   <bg>VideoLAN project's media player</bg>
   <bn>VideoLAN project's media player</bn>
   <ca>Reproductor de mèdia del projecte VideoLAN</ca>
   <cs>VideoLAN project's media player</cs>
   <da>VideoLAN project's media player</da>
   <de>VLC Medienwiedergabeprogramm des VideoLAN Projektes</de>
   <el>Το πρόγραμμα αναπαραγωγής πολυμέσων του έργου VideoLAN</el>
   <en>VideoLAN project's media player</en>
   <es_ES>Reproductor multimedia del proyecto VideoLAN</es_ES>
   <es>Reproductor multimedia del proyecto VideoLAN</es>
   <et>VideoLAN project's media player</et>
   <eu>VideoLAN project's media player</eu>
   <fa>VideoLAN project's media player</fa>
   <fil_PH>VideoLAN project's media player</fil_PH>
   <fi>VideoLAN projektin mediasoitin</fi>
   <fr_BE>Lecteur multimédia du projet VLC</fr_BE>
   <fr>Lecteur multimédia du projet VLC</fr>
   <gl_ES>VideoLAN project's media player</gl_ES>
   <gu>VideoLAN project's media player</gu>
   <he_IL>VideoLAN project's media player</he_IL>
   <hi>VideoLAN परियोजना का मीडिया प्लेयर</hi>
   <hr>VideoLAN project's media player</hr>
   <hu>VideoLAN project's media player</hu>
   <id>VideoLAN project's media player</id>
   <is>VideoLAN project's media player</is>
   <it>Lettore multimediale del progetto VideoLAN</it>
   <ja>VideoLAN プロジェクトのメディアプレーヤ</ja>
   <kk>VideoLAN project's media player</kk>
   <ko>VideoLAN project's media player</ko>
   <ku>VideoLAN project's media player</ku>
   <lt>VideoLAN project's media player</lt>
   <mk>VideoLAN project's media player</mk>
   <mr>VideoLAN project's media player</mr>
   <nb_NO>VideoLAN project's media player</nb_NO>
   <nb>VideoLAN-prosjektets mediespiller</nb>
   <nl_BE>VideoLAN project's media player</nl_BE>
   <nl>VideoLAN project's media player</nl>
   <or>VideoLAN project's media player</or>
   <pl>VideoLAN project's media player</pl>
   <pt_BR>Media player do projeto VideoLAN</pt_BR>
   <pt>Reprodutor multimédia do projeto VideoLAN</pt>
   <ro>VideoLAN project's media player</ro>
   <ru>VideoLAN project's media player</ru>
   <sk>VideoLAN project's media player</sk>
   <sl>Predvajalnik projekta VideoLAN</sl>
   <so>VideoLAN project's media player</so>
   <sq>Lojtësi media i projektit VideoLAN</sq>
   <sr>VideoLAN project's media player</sr>
   <sv>VideoLAN project's mediaspelare</sv>
   <th>VideoLAN project's media player</th>
   <tr>VideoLAN projesinin ortam oynatıcısı</tr>
   <uk>VideoLAN project's media player</uk>
   <vi>VideoLAN project's media player</vi>
   <zh_CN>VideoLAN project's media player</zh_CN>
   <zh_HK>VideoLAN project's media player</zh_HK>
   <zh_TW>VideoLAN project's media player</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
vlc
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
vlc
</uninstall_package_names>
</app>
