<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Japanese_Input_fcitx
</name>

<description>
   <am>Japanese fcitx</am>
   <ar>Japanese fcitx</ar>
   <be>Japanese fcitx</be>
   <bg>Japanese fcitx</bg>
   <bn>Japanese fcitx</bn>
   <ca>Fcitx en italià</ca>
   <cs>Japanese fcitx</cs>
   <da>Japansk fcitx</da>
   <de>Japanisch Fcitx</de>
   <el>Ιαπωνικά fcitx</el>
   <en>Japanese fcitx</en>
   <es_ES>Fcitx Japonés</es_ES>
   <es>Fcitx Japonés</es>
   <et>Japanese fcitx</et>
   <eu>Japanese fcitx</eu>
   <fa>Japanese fcitx</fa>
   <fil_PH>Japanese fcitx</fil_PH>
   <fi>Japanilainen fcitx</fi>
   <fr_BE>Japonais fcitx</fr_BE>
   <fr>Japonais fcitx</fr>
   <gl_ES>Xaponés fcitx</gl_ES>
   <gu>Japanese fcitx</gu>
   <he_IL>Japanese fcitx</he_IL>
   <hi>जापानी Fcitx</hi>
   <hr>Japanese fcitx</hr>
   <hu>Japanese fcitx</hu>
   <id>Japanese fcitx</id>
   <is>Japanese fcitx</is>
   <it>Fcitx per la lingua giapponese</it>
   <ja>日本語 fcitx 入力</ja>
   <kk>Japanese fcitx</kk>
   <ko>Japanese fcitx</ko>
   <ku>Japanese fcitx</ku>
   <lt>Japonų fcitx</lt>
   <mk>Japanese fcitx</mk>
   <mr>Japanese fcitx</mr>
   <nb_NO>Japanese fcitx</nb_NO>
   <nb>Japansk fcitx</nb>
   <nl_BE>Japanese fcitx</nl_BE>
   <nl>Japanse fcitx</nl>
   <or>Japanese fcitx</or>
   <pl>Japoński fcitx</pl>
   <pt_BR>fcitx para o idioma Japonês</pt_BR>
   <pt>Japonês fcitx</pt>
   <ro>Japanese fcitx</ro>
   <ru>Japanese fcitx</ru>
   <sk>Japanese fcitx</sk>
   <sl>Japonski fcitx</sl>
   <so>Japanese fcitx</so>
   <sq>fcitx për japonishten</sq>
   <sr>Japanese fcitx</sr>
   <sv>Japansk fcitx</sv>
   <th>fcitx ภาษาญี่ปุ่น</th>
   <tr>Japonca fcitx</tr>
   <uk>Japanese fcitx</uk>
   <vi>Japanese fcitx</vi>
   <zh_CN>日语 fcitx 输入法</zh_CN>
   <zh_HK>Japanese fcitx</zh_HK>
   <zh_TW>Japanese fcitx</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
fcitx
fcitx-config-gtk
fcitx-frontend-all
fcitx-frontend-fbterm
fcitx-tools
fcitx-ui-classic
fcitx-ui-light
fcitx-anthy
fcitx-mozc
fcitx-table-all
fcitx-frontend-gtk3
fcitx-frontend-qt5
im-config
mozc-utils-gui
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
fcitx
fcitx-config-gtk
fcitx-frontend-all
fcitx-frontend-fbterm
fcitx-tools
fcitx-ui-classic
fcitx-ui-light
fcitx-anthy
fcitx-mozc
fcitx-table-all
fcitx-frontend-gtk2
fcitx-frontend-gtk3
fcitx-frontend-qt5
im-config
mozc-utils-gui
</uninstall_package_names>
</app>
