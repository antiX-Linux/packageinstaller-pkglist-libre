<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Development
</category>

<name>
QT5 Dev Environment
</name>

<description>
   <am>QT5 Development Environment</am>
   <ar>QT5 Development Environment</ar>
   <be>QT5 Development Environment</be>
   <bg>QT5 Development Environment</bg>
   <bn>QT5 Development Environment</bn>
   <ca>Entorn de desenvolupament per QT5</ca>
   <cs>QT5 Development Environment</cs>
   <da>QT5-udviklingsmiljø</da>
   <de>Qt5 Entwicklungsumgebung</de>
   <el>Περιβάλλον ανάπτυξης του QT5</el>
   <en>QT5 Development Environment</en>
   <es_ES>Entorno de desarrollo QT5</es_ES>
   <es>Entorno de desarrollo QT5</es>
   <et>QT5 Development Environment</et>
   <eu>QT5 Development Environment</eu>
   <fa>QT5 Development Environment</fa>
   <fil_PH>QT5 Development Environment</fil_PH>
   <fi>QT5-kehitysympäristö</fi>
   <fr_BE>Environnement de développement QT5</fr_BE>
   <fr>Environnement de développement QT5</fr>
   <gl_ES>QT5 Development Environment</gl_ES>
   <gu>QT5 Development Environment</gu>
   <he_IL>QT5 Development Environment</he_IL>
   <hi>क्यूटी5 सॉफ्टवेयर विकास वातावरण</hi>
   <hr>QT5 Development Environment</hr>
   <hu>QT5 Development Environment</hu>
   <id>QT5 Development Environment</id>
   <is>QT5 Development Environment</is>
   <it>Ambiente di sviluppo QT5</it>
   <ja>QT5 開発環境</ja>
   <kk>QT5 Development Environment</kk>
   <ko>QT5 Development Environment</ko>
   <ku>QT5 Development Environment</ku>
   <lt>QT5 plėtojimo aplinka</lt>
   <mk>QT5 Development Environment</mk>
   <mr>QT5 Development Environment</mr>
   <nb_NO>QT5 Development Environment</nb_NO>
   <nb>Utviklingsmiljø for Qt 5</nb>
   <nl_BE>QT5 Development Environment</nl_BE>
   <nl>QT5 Ontwikkelingsomgeving</nl>
   <or>QT5 Development Environment</or>
   <pl>środowisko programistyczne QT5</pl>
   <pt_BR>Ambiente de Desenvolvimento QT5</pt_BR>
   <pt>Ambiente de Desenvolvimento QT5</pt>
   <ro>QT5 Development Environment</ro>
   <ru>Среда разработки под QT-5</ru>
   <sk>QT5 Development Environment</sk>
   <sl>QT5 razvojno okolje</sl>
   <so>QT5 Development Environment</so>
   <sq>Mjedis Zhvillimesh QT5</sq>
   <sr>QT5 Development Environment</sr>
   <sv>QT5 Utvecklingsmiljö</sv>
   <th>QT5 Development Environment</th>
   <tr>QT5 Geliştirme Ortamı</tr>
   <uk>QT5 Development Environment</uk>
   <vi>QT5 Development Environment</vi>
   <zh_CN>QT5 Development Environment</zh_CN>
   <zh_HK>QT5 Development Environment</zh_HK>
   <zh_TW>QT5 Development Environment</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
qtcreator
qttools5-dev
qttools5-dev-tools
qtdeclarative5-dev-tools
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
qtcreator
qttools5-dev
qttools5-dev-tools
qtdeclarative5-dev-tools
</uninstall_package_names>
</app>
