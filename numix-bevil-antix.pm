<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Icons
</category>

<name>
Numix Bevel antiX
</name>

<description>
   <am>Numix Bevil icons for antiX Linux</am>
   <ar>Numix Bevil icons for antiX Linux</ar>
   <be>Numix Bevil icons for antiX Linux</be>
   <bg>Numix Bevil icons for antiX Linux</bg>
   <bn>Numix Bevil icons for antiX Linux</bn>
   <ca>Icones Numix Bevil per antiX Linux</ca>
   <cs>Numix Bevil icons for antiX Linux</cs>
   <da>Numix Bevil icons for antiX Linux</da>
   <de>Piktogramme (Ugs.: “Icons”) für antiX im Stil “Numix Bevil”</de>
   <el>Numix Bevil εικονίδια για antiX Linux</el>
   <en>Numix Bevil icons for antiX Linux</en>
   <es_ES>Iconos Numix Bevil para antiX Linux</es_ES>
   <es>Iconos Numix Bevil para antiX Linux</es>
   <et>Numix Bevil icons for antiX Linux</et>
   <eu>Numix Bevil icons for antiX Linux</eu>
   <fa>Numix Bevil icons for antiX Linux</fa>
   <fil_PH>Numix Bevil icons for antiX Linux</fil_PH>
   <fi>Numix Bevil kuvakkeet antiX Linux:ille</fi>
   <fr_BE>Icônes pour antiX dans le style "Numix Bevil"</fr_BE>
   <fr>Icônes pour antiX dans le style "Numix Bevil"</fr>
   <gl_ES>Numix Bevil icons for antiX Linux</gl_ES>
   <gu>Numix Bevil icons for antiX Linux</gu>
   <he_IL>Numix Bevil icons for antiX Linux</he_IL>
   <hi>एंटी-एक्स लिनक्स हेतु Numix Bevel आइकन</hi>
   <hr>Numix Bevil icons for antiX Linux</hr>
   <hu>Numix Bevil icons for antiX Linux</hu>
   <id>Numix Bevil icons for antiX Linux</id>
   <is>Numix Bevil icons for antiX Linux</is>
   <it>Icone Numix Bevil per antiX Linux</it>
   <ja>AntiX Linux用 Numix Bevil アイコン</ja>
   <kk>Numix Bevil icons for antiX Linux</kk>
   <ko>Numix Bevil icons for antiX Linux</ko>
   <ku>Numix Bevil icons for antiX Linux</ku>
   <lt>Numix Bevil icons for antiX Linux</lt>
   <mk>Numix Bevil icons for antiX Linux</mk>
   <mr>Numix Bevil icons for antiX Linux</mr>
   <nb_NO>Numix Bevil icons for antiX Linux</nb_NO>
   <nb>Numix Bevil-ikoner for antiX Linux</nb>
   <nl_BE>Numix Bevil icons for antiX Linux</nl_BE>
   <nl>Numix Bevil icons for antiX Linux</nl>
   <or>Numix Bevil icons for antiX Linux</or>
   <pl>Numix Bevil icons for antiX Linux</pl>
   <pt_BR>Ícones Numix Bevil para o antiX Linux</pt_BR>
   <pt>Icones Numix Bevil para o antiX Linux</pt>
   <ro>Numix Bevil icons for antiX Linux</ro>
   <ru>Numix Bevil icons for antiX Linux</ru>
   <sk>Numix Bevil icons for antiX Linux</sk>
   <sl>Numix Bevill ikone za antiX Linux</sl>
   <so>Numix Bevil icons for antiX Linux</so>
   <sq>Ikona Numix Bevil për antiX Linux</sq>
   <sr>Numix Bevil icons for antiX Linux</sr>
   <sv>Numix Bevil ikoner för antiX Linux</sv>
   <th>Numix Bevil icons for antiX Linux</th>
   <tr>antiX Linux için Numix Bevil simgeleri</tr>
   <uk>Numix Bevil icons for antiX Linux</uk>
   <vi>Numix Bevil icons for antiX Linux</vi>
   <zh_CN>Numix Bevil icons for antiX Linux</zh_CN>
   <zh_HK>Numix Bevil icons for antiX Linux</zh_HK>
   <zh_TW>Numix Bevil icons for antiX Linux</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
numix-bevel-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
numix-bevel-antix
</uninstall_package_names>
</app>
