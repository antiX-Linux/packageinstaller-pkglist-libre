<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Kazakh
</name>

<description>
   <am>Kazakh dictionary for hunspell</am>
   <ar>Kazakh dictionary for hunspell</ar>
   <be>Kazakh dictionary for hunspell</be>
   <bg>Kazakh dictionary for hunspell</bg>
   <bn>Kazakh dictionary for hunspell</bn>
   <ca>Diccionari Kazach per hunspell</ca>
   <cs>Kazakh dictionary for hunspell</cs>
   <da>Kazakh dictionary for hunspell</da>
   <de>Kasachisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Καζακστάν για hunspell</el>
   <en>Kazakh dictionary for hunspell</en>
   <es_ES>Diccionario Kazajo para hunspell</es_ES>
   <es>Diccionario Kazajo para hunspell</es>
   <et>Kazakh dictionary for hunspell</et>
   <eu>Kazakh dictionary for hunspell</eu>
   <fa>Kazakh dictionary for hunspell</fa>
   <fil_PH>Kazakh dictionary for hunspell</fil_PH>
   <fi>Kazakh dictionary for hunspell</fi>
   <fr_BE>Kazakh dictionnaire pour hunspell</fr_BE>
   <fr>Kazakh dictionnaire pour hunspell</fr>
   <gl_ES>Kazakh dictionary for hunspell</gl_ES>
   <gu>Kazakh dictionary for hunspell</gu>
   <he_IL>Kazakh dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु कज़ाख शब्दकोष</hi>
   <hr>Kazakh dictionary for hunspell</hr>
   <hu>Kazakh dictionary for hunspell</hu>
   <id>Kazakh dictionary for hunspell</id>
   <is>Kazakh dictionary for hunspell</is>
   <it>Dizionario kazako per hunspell</it>
   <ja>Hunspell 用カザフ語辞書</ja>
   <kk>Kazakh dictionary for hunspell</kk>
   <ko>Kazakh dictionary for hunspell</ko>
   <ku>Kazakh dictionary for hunspell</ku>
   <lt>Kazakh dictionary for hunspell</lt>
   <mk>Kazakh dictionary for hunspell</mk>
   <mr>Kazakh dictionary for hunspell</mr>
   <nb_NO>Kazakh dictionary for hunspell</nb_NO>
   <nb>Kazahk ordliste for hunspell</nb>
   <nl_BE>Kazakh dictionary for hunspell</nl_BE>
   <nl>Kazakh dictionary for hunspell</nl>
   <or>Kazakh dictionary for hunspell</or>
   <pl>Kazakh dictionary for hunspell</pl>
   <pt_BR>Dicionário Cazaque para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Kazakh para hunspell</pt>
   <ro>Kazakh dictionary for hunspell</ro>
   <ru>Kazakh dictionary for hunspell</ru>
   <sk>Kazakh dictionary for hunspell</sk>
   <sl>Kazahski slovar za hunspell</sl>
   <so>Kazakh dictionary for hunspell</so>
   <sq>Fjalor kazakistanisht për hunspell</sq>
   <sr>Kazakh dictionary for hunspell</sr>
   <sv>Kazakh ordbok för hunspell</sv>
   <th>Kazakh dictionary for hunspell</th>
   <tr>Hunspell için Kazakça sözlük</tr>
   <uk>Kazakh dictionary for hunspell</uk>
   <vi>Kazakh dictionary for hunspell</vi>
   <zh_CN>Kazakh dictionary for hunspell</zh_CN>
   <zh_HK>Kazakh dictionary for hunspell</zh_HK>
   <zh_TW>Kazakh dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-kk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-kk
</uninstall_package_names>

</app>
