<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Spanish_Firefox
</name>

<description>
   <am>Spanish localisation of Firefox</am>
   <ar>Spanish localisation of Firefox</ar>
   <be>Spanish localisation of Firefox</be>
   <bg>Spanish localisation of Firefox</bg>
   <bn>Spanish localisation of Firefox</bn>
   <ca>Localització de Firefox en Castellà</ca>
   <cs>Spanish localisation of Firefox</cs>
   <da>Spansk oversættelse af Firefox</da>
   <de>Spanische Lokalisierung von Firefox</de>
   <el>Ισπανικά για το Firefox</el>
   <en>Spanish localisation of Firefox</en>
   <es_ES>Localización Española para Firefox</es_ES>
   <es>Localización Español para Firefox</es>
   <et>Spanish localisation of Firefox</et>
   <eu>Spanish localisation of Firefox</eu>
   <fa>Spanish localisation of Firefox</fa>
   <fil_PH>Spanish localisation of Firefox</fil_PH>
   <fi>Espanjalainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en espagnol pour Firefox</fr_BE>
   <fr>Localisation en espagnol pour Firefox</fr>
   <gl_ES>Localización de Firefox ao español</gl_ES>
   <gu>Spanish localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לספרדית</he_IL>
   <hi>फायरफॉक्स का स्पेनिश संस्करण</hi>
   <hr>Spanish localisation of Firefox</hr>
   <hu>Spanish localisation of Firefox</hu>
   <id>Spanish localisation of Firefox</id>
   <is>Spanish localisation of Firefox</is>
   <it>Localizzazione spagnola di Firefox</it>
   <ja>Firefox のスペイン語版</ja>
   <kk>Spanish localisation of Firefox</kk>
   <ko>Spanish localisation of Firefox</ko>
   <ku>Spanish localisation of Firefox</ku>
   <lt>Spanish localisation of Firefox</lt>
   <mk>Spanish localisation of Firefox</mk>
   <mr>Spanish localisation of Firefox</mr>
   <nb_NO>Spanish localisation of Firefox</nb_NO>
   <nb>Spansk lokaltilpassing av Firefox</nb>
   <nl_BE>Spanish localisation of Firefox</nl_BE>
   <nl>Spaanse lokalisatie van Firefox</nl>
   <or>Spanish localisation of Firefox</or>
   <pl>Hiszpańska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Espanhol Localização para o Firefox</pt_BR>
   <pt>Castelhano Localização para Firefox</pt>
   <ro>Spanish localisation of Firefox</ro>
   <ru>Испанская локализация Firefox</ru>
   <sk>Spanish localisation of Firefox</sk>
   <sl>Španske krajevne nastavitve za Firefox</sl>
   <so>Spanish localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në spanjisht</sq>
   <sr>Spanish localisation of Firefox</sr>
   <sv>Spansk lokalisering av Firefox </sv>
   <th>Spanish localisation ของ Firefox</th>
   <tr>Firefox'un İspanyolca yerelleştirmesi</tr>
   <uk>Spanish localisation of Firefox</uk>
   <vi>Spanish localisation of Firefox</vi>
   <zh_CN>Spanish localisation of Firefox</zh_CN>
   <zh_HK>Spanish localisation of Firefox</zh_HK>
   <zh_TW>Spanish localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-es-es
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-es-es
</uninstall_package_names>
</app>
