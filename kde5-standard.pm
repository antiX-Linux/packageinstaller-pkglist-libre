<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
KDE5-standard
</name>

<description>
   <am>Installs kde-standard, kde-plasma-desktop</am>
   <ar>Installs kde-standard, kde-plasma-desktop</ar>
   <be>Installs kde-standard, kde-plasma-desktop</be>
   <bg>Installs kde-standard, kde-plasma-desktop</bg>
   <bn>Installs kde-standard, kde-plasma-desktop</bn>
   <ca>Instal·la kde-standard i kde-plasma-desktop</ca>
   <cs>Installs kde-standard, kde-plasma-desktop</cs>
   <da>Installerer kde-standard, kde-plasma-desktop</da>
   <de>Installiert KDE-standard, KDE-plasma-desktop</de>
   <el>Εγκατάσταση του kde-standard, kde-plasma-desktop</el>
   <en>Installs kde-standard, kde-plasma-desktop</en>
   <es_ES>Instala kde-standard, kde-escritorio-plasma</es_ES>
   <es>Instala kde-standard, kde-escritorio-plasma</es>
   <et>Installs kde-standard, kde-plasma-desktop</et>
   <eu>Installs kde-standard, kde-plasma-desktop</eu>
   <fa>Installs kde-standard, kde-plasma-desktop</fa>
   <fil_PH>Installs kde-standard, kde-plasma-desktop</fil_PH>
   <fi>Asentaa työpöydät kde-standard, kde-plasma-desktop</fi>
   <fr_BE>Installation de kde-standard, kde-plasma-desktop</fr_BE>
   <fr>Installation de kde-standard, kde-plasma-desktop</fr>
   <gl_ES>Instala os paquetes kde-standard e kde-plasma-desktop</gl_ES>
   <gu>Installs kde-standard, kde-plasma-desktop</gu>
   <he_IL>Installs kde-standard, kde-plasma-desktop</he_IL>
   <hi>केडीई-मानक, केडीई-प्लाज़्मा-डेस्कटॉप इंस्टॉल होगा</hi>
   <hr>Installs kde-standard, kde-plasma-desktop</hr>
   <hu>Installs kde-standard, kde-plasma-desktop</hu>
   <id>Installs kde-standard, kde-plasma-desktop</id>
   <is>Installs kde-standard, kde-plasma-desktop</is>
   <it>Installa kde-standard, kde-plasma-desktop</it>
   <ja>KDE 標準デスクトップ、KDE プラズマデスクトップをインストールします</ja>
   <kk>Installs kde-standard, kde-plasma-desktop</kk>
   <ko>Installs kde-standard, kde-plasma-desktop</ko>
   <ku>Installs kde-standard, kde-plasma-desktop</ku>
   <lt>Installs kde-standard, kde-plasma-desktop</lt>
   <mk>Installs kde-standard, kde-plasma-desktop</mk>
   <mr>Installs kde-standard, kde-plasma-desktop</mr>
   <nb_NO>Installs kde-standard, kde-plasma-desktop</nb_NO>
   <nb>Installerer kde-standard, kde-plasma-desktop</nb>
   <nl_BE>Installs kde-standard, kde-plasma-desktop</nl_BE>
   <nl>Installeert kde-standard, kde-plasma-desktop</nl>
   <or>Installs kde-standard, kde-plasma-desktop</or>
   <pl>instaluje kde-standard, kde-plasma-desktop</pl>
   <pt_BR>Instalação dos pacotes kde-standard e kde-plasma-desktop</pt_BR>
   <pt>Instala os pacotes kde-standard e kde-plasma-desktop</pt>
   <ro>Installs kde-standard, kde-plasma-desktop</ro>
   <ru>Устанавливает kde-standart, kde-plasma-desktop</ru>
   <sk>Installs kde-standard, kde-plasma-desktop</sk>
   <sl>Namesti kde-standard in kde-plasma-desktop</sl>
   <so>Installs kde-standard, kde-plasma-desktop</so>
   <sq>Instalon kde-standard, kde-plasma-desktop</sq>
   <sr>Installs kde-standard, kde-plasma-desktop</sr>
   <sv>Installerar kde-standard, kde-plasma-desktop</sv>
   <th>ติดตั้ง kde-standard, kde-plasma-desktop</th>
   <tr>Kde-plasma-masaüstü, kde-standart yükler</tr>
   <uk>встановлює набори kde-standard, kde-plasma-desktop</uk>
   <vi>Installs kde-standard, kde-plasma-desktop</vi>
   <zh_CN>Installs kde-standard, kde-plasma-desktop</zh_CN>
   <zh_HK>Installs kde-standard, kde-plasma-desktop</zh_HK>
   <zh_TW>Installs kde-standard, kde-plasma-desktop</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
kde-standard
kwin-x11
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
kde-standard
kwin-x11
</uninstall_package_names>
</app>
