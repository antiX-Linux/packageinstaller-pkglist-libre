<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Japanese_Default_Firefox_esr
</name>

<description>
   <am>Japanese localisation of Firefox ESR</am>
   <ar>Japanese localisation of Firefox ESR</ar>
   <be>Japanese localisation of Firefox ESR</be>
   <bg>Japanese localisation of Firefox ESR</bg>
   <bn>Japanese localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Japonès</ca>
   <cs>Japanese localisation of Firefox ESR</cs>
   <da>Japanese localisation of Firefox ESR</da>
   <de>Japanische Lokalisation von “Firefox ESR”</de>
   <el>Ιαπωνικά για Firefox ESR</el>
   <en>Japanese localisation of Firefox ESR</en>
   <es_ES>Localización Japones de Firefox ESR</es_ES>
   <es>Localización Japonés de Firefox ESR</es>
   <et>Japanese localisation of Firefox ESR</et>
   <eu>Japanese localisation of Firefox ESR</eu>
   <fa>Japanese localisation of Firefox ESR</fa>
   <fil_PH>Japanese localisation of Firefox ESR</fil_PH>
   <fi>Japanese localisation of Firefox ESR</fi>
   <fr_BE>Localisation en japonais pour Firefox ESR</fr_BE>
   <fr>Localisation en japonais pour Firefox ESR</fr>
   <gl_ES>Japanese localisation of Firefox ESR</gl_ES>
   <gu>Japanese localisation of Firefox ESR</gu>
   <he_IL>Japanese localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का जापानी संस्करण</hi>
   <hr>Japanese localisation of Firefox ESR</hr>
   <hu>Japanese localisation of Firefox ESR</hu>
   <id>Japanese localisation of Firefox ESR</id>
   <is>Japanese localisation of Firefox ESR</is>
   <it>Localizzazione giapponese di Firefox ESR</it>
   <ja>日本語版 Firefox ESR</ja>
   <kk>Japanese localisation of Firefox ESR</kk>
   <ko>Japanese localisation of Firefox ESR</ko>
   <ku>Japanese localisation of Firefox ESR</ku>
   <lt>Japanese localisation of Firefox ESR</lt>
   <mk>Japanese localisation of Firefox ESR</mk>
   <mr>Japanese localisation of Firefox ESR</mr>
   <nb_NO>Japanese localisation of Firefox ESR</nb_NO>
   <nb>Japansk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Japanese localisation of Firefox ESR</nl_BE>
   <nl>Japanese localisation of Firefox ESR</nl>
   <or>Japanese localisation of Firefox ESR</or>
   <pl>Japanese localisation of Firefox ESR</pl>
   <pt_BR>Japonês Localização para o Firefox ESR</pt_BR>
   <pt>Japonês Localização para Firefox ESR</pt>
   <ro>Japanese localisation of Firefox ESR</ro>
   <ru>Japanese localisation of Firefox ESR</ru>
   <sk>Japanese localisation of Firefox ESR</sk>
   <sl>Japonske krajevne nastavitve za Firefox ESR</sl>
   <so>Japanese localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në japonisht</sq>
   <sr>Japanese localisation of Firefox ESR</sr>
   <sv>Japansk lokalisering av Firefox ESR</sv>
   <th>Japanese localisation of Firefox ESR</th>
   <tr>Firefox ESR Japonca yerelleştirmesi</tr>
   <uk>Japanese localisation of Firefox ESR</uk>
   <vi>Japanese localisation of Firefox ESR</vi>
   <zh_CN>Japanese localisation of Firefox ESR</zh_CN>
   <zh_HK>Japanese localisation of Firefox ESR</zh_HK>
   <zh_TW>Japanese localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ja
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ja
</uninstall_package_names>

</app>
