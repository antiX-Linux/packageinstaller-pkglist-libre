<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Japanese_Thunderbird
</name>

<description>
   <am>Japanese localisation of Thunderbird</am>
   <ar>Japanese localisation of Thunderbird</ar>
   <be>Japanese localisation of Thunderbird</be>
   <bg>Japanese localisation of Thunderbird</bg>
   <bn>Japanese localisation of Thunderbird</bn>
   <ca>Localització en Japonès de Thunderbird</ca>
   <cs>Japanese localisation of Thunderbird</cs>
   <da>Japansk oversættelse af Thunderbird</da>
   <de>Japanische Lokalisierung von Thunderbird</de>
   <el>Ιαπωνικά για το Thunderbird</el>
   <en>Japanese localisation of Thunderbird</en>
   <es_ES>Localización Japonesa de Thunderbird</es_ES>
   <es>Localización Japonés de Thunderbird</es>
   <et>Japanese localisation of Thunderbird</et>
   <eu>Japanese localisation of Thunderbird</eu>
   <fa>Japanese localisation of Thunderbird</fa>
   <fil_PH>Japanese localisation of Thunderbird</fil_PH>
   <fi>Japanilainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en japonais pour Thunderbird</fr_BE>
   <fr>Localisation en japonais pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao xaponés</gl_ES>
   <gu>Japanese localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird ליפנית</he_IL>
   <hi>थंडरबर्ड का जापानी संस्करण</hi>
   <hr>Japanese localisation of Thunderbird</hr>
   <hu>Japanese localisation of Thunderbird</hu>
   <id>Japanese localisation of Thunderbird</id>
   <is>Japanese localisation of Thunderbird</is>
   <it>Localizzazione giapponese di Thunderbird</it>
   <ja>Thunderbird の日本語版</ja>
   <kk>Japanese localisation of Thunderbird</kk>
   <ko>Japanese localisation of Thunderbird</ko>
   <ku>Japanese localisation of Thunderbird</ku>
   <lt>Japanese localisation of Thunderbird</lt>
   <mk>Japanese localisation of Thunderbird</mk>
   <mr>Japanese localisation of Thunderbird</mr>
   <nb_NO>Japanese localisation of Thunderbird</nb_NO>
   <nb>Japansk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Japanese localisation of Thunderbird</nl_BE>
   <nl>Japanse lokalisatie van Thunderbird</nl>
   <or>Japanese localisation of Thunderbird</or>
   <pl>Japońska lokalizacja Thunderbirda</pl>
   <pt_BR>Japonês Localização para o Thunderbird</pt_BR>
   <pt>Japonês Localização para Thunderbird</pt>
   <ro>Japanese localisation of Thunderbird</ro>
   <ru>Japanese localisation of Thunderbird</ru>
   <sk>Japanese localisation of Thunderbird</sk>
   <sl>Japonske krajevne nastavitve za Thunderbird</sl>
   <so>Japanese localisation of Thunderbird</so>
   <sq>Përkthimi në japonisht i Thunderbird-it</sq>
   <sr>Japanese localisation of Thunderbird</sr>
   <sv>Japansk lokalisering av Thunderbird</sv>
   <th>Japanese localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Japonca yerelleştirmesi</tr>
   <uk>Japanese локалізація Thunderbird</uk>
   <vi>Japanese localisation of Thunderbird</vi>
   <zh_CN>Japanese localisation of Thunderbird</zh_CN>
   <zh_HK>Japanese localisation of Thunderbird</zh_HK>
   <zh_TW>Japanese localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-ja
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-ja
</uninstall_package_names>

</app>
