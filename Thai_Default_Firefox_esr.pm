<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Thai_Default_Firefox_esr
</name>

<description>
   <am>Thai localisation of Firefox ESR</am>
   <ar>Thai localisation of Firefox ESR</ar>
   <be>Thai localisation of Firefox ESR</be>
   <bg>Thai localisation of Firefox ESR</bg>
   <bn>Thai localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Thai</ca>
   <cs>Thai localisation of Firefox ESR</cs>
   <da>Thai localisation of Firefox ESR</da>
   <de>Thailändische Lokalisation von “Firefox ESR”</de>
   <el>Thai for Firefox ESR</el>
   <en>Thai localisation of Firefox ESR</en>
   <es_ES>Localización Tailandés de Firefox ESR</es_ES>
   <es>Localización Tailandés de Firefox ESR</es>
   <et>Thai localisation of Firefox ESR</et>
   <eu>Thai localisation of Firefox ESR</eu>
   <fa>Thai localisation of Firefox ESR</fa>
   <fil_PH>Thai localisation of Firefox ESR</fil_PH>
   <fi>Thai localisation of Firefox ESR</fi>
   <fr_BE>Localisation en thaïlandais pour Firefox ESR</fr_BE>
   <fr>Localisation en thaïlandais pour Firefox ESR</fr>
   <gl_ES>Thai localisation of Firefox ESR</gl_ES>
   <gu>Thai localisation of Firefox ESR</gu>
   <he_IL>Thai localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का थाई संस्करण</hi>
   <hr>Thai localisation of Firefox ESR</hr>
   <hu>Thai localisation of Firefox ESR</hu>
   <id>Thai localisation of Firefox ESR</id>
   <is>Thai localisation of Firefox ESR</is>
   <it>Localizzazione thai di Firefox ESR</it>
   <ja>タイ語版 Firefox ESR</ja>
   <kk>Thai localisation of Firefox ESR</kk>
   <ko>Thai localisation of Firefox ESR</ko>
   <ku>Thai localisation of Firefox ESR</ku>
   <lt>Thai localisation of Firefox ESR</lt>
   <mk>Thai localisation of Firefox ESR</mk>
   <mr>Thai localisation of Firefox ESR</mr>
   <nb_NO>Thai localisation of Firefox ESR</nb_NO>
   <nb>Thai lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Thai localisation of Firefox ESR</nl_BE>
   <nl>Thai localisation of Firefox ESR</nl>
   <or>Thai localisation of Firefox ESR</or>
   <pl>Thai localisation of Firefox ESR</pl>
   <pt_BR>Tailandês Localização para o Firefox ESR</pt_BR>
   <pt>Tailandês Localização para Firefox ESR</pt>
   <ro>Thai localisation of Firefox ESR</ro>
   <ru>Thai localisation of Firefox ESR</ru>
   <sk>Thai localisation of Firefox ESR</sk>
   <sl>Tajske krajevne nastavitve za Firefox ESR</sl>
   <so>Thai localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në tajlandisht</sq>
   <sr>Thai localisation of Firefox ESR</sr>
   <sv>Thai lokalisering av Firefox ESR</sv>
   <th>Thai localisation of Firefox ESR</th>
   <tr>Firefox ESR Tayca yerelleştirmesi</tr>
   <uk>Thai localisation of Firefox ESR</uk>
   <vi>Thai localisation of Firefox ESR</vi>
   <zh_CN>Thai localisation of Firefox ESR</zh_CN>
   <zh_HK>Thai localisation of Firefox ESR</zh_HK>
   <zh_TW>Thai localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-th
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-th
</uninstall_package_names>

</app>
