<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
KDE5-lite
</name>

<description>
   <am>Minimal install of KDE5</am>
   <ar>Minimal install of KDE5</ar>
   <be>Minimal install of KDE5</be>
   <bg>Minimal install of KDE5</bg>
   <bn>Minimal install of KDE5</bn>
   <ca>Instal·lació mínima de KDE5</ca>
   <cs>Minimal install of KDE5</cs>
   <da>Minimal install of KDE5</da>
   <de>Minimalistische Installation der Desktop-Umgebung KDE 5</de>
   <el>Ελάχιστη εγκατάσταση του KDE5</el>
   <en>Minimal install of KDE5</en>
   <es_ES>Instalación mínima de KDE5</es_ES>
   <es>Instalación mínima de KDE5</es>
   <et>Minimal install of KDE5</et>
   <eu>Minimal install of KDE5</eu>
   <fa>Minimal install of KDE5</fa>
   <fil_PH>Minimal install of KDE5</fil_PH>
   <fi>KDE5-työpöytäympäristön vähimmäisasennus</fi>
   <fr_BE>Installation minimale de KDE5</fr_BE>
   <fr>Installation minimale de KDE5</fr>
   <gl_ES>Minimal install of KDE5</gl_ES>
   <gu>Minimal install of KDE5</gu>
   <he_IL>Minimal install of KDE5</he_IL>
   <hi>केडीई5 का संक्षिप्त इंस्टॉल</hi>
   <hr>Minimal install of KDE5</hr>
   <hu>Minimal install of KDE5</hu>
   <id>Minimal install of KDE5</id>
   <is>Minimal install of KDE5</is>
   <it>Installazione minimale di KDE5</it>
   <ja>KDE 5 デスクトップの最小インストール</ja>
   <kk>Minimal install of KDE5</kk>
   <ko>Minimal install of KDE5</ko>
   <ku>Minimal install of KDE5</ku>
   <lt>Minimal install of KDE5</lt>
   <mk>Minimal install of KDE5</mk>
   <mr>Minimal install of KDE5</mr>
   <nb_NO>Minimal install of KDE5</nb_NO>
   <nb>Minimal installasjon av KDE5</nb>
   <nl_BE>Minimal install of KDE5</nl_BE>
   <nl>Minimal install of KDE5</nl>
   <or>Minimal install of KDE5</or>
   <pl>Minimal install of KDE5</pl>
   <pt_BR>Instalação Mínima do KDE5-minimal</pt_BR>
   <pt>Instalação mínima do KDE5</pt>
   <ro>Minimal install of KDE5</ro>
   <ru>Minimal install of KDE5</ru>
   <sk>Minimal install of KDE5</sk>
   <sl>Minimalna nameestitev KDE5</sl>
   <so>Minimal install of KDE5</so>
   <sq>Instalim minimal i KDE5-s</sq>
   <sr>Minimal install of KDE5</sr>
   <sv>Minimal installation av KDE5</sv>
   <th>Minimal install of KDE5</th>
   <tr>KDE5 en küçük kurulumu</tr>
   <uk>Minimal install of KDE5</uk>
   <vi>Minimal install of KDE5</vi>
   <zh_CN>Minimal install of KDE5</zh_CN>
   <zh_HK>Minimal install of KDE5</zh_HK>
   <zh_TW>Minimal install of KDE5</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
kde-plasma-desktop
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
kde-plasma-desktop
</uninstall_package_names>
</app>
