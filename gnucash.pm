<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Office
</category>

<name>
GnuCash
</name>

<description>
   <am>personal and small-business financial-accounting software</am>
   <ar>personal and small-business financial-accounting software</ar>
   <be>personal and small-business financial-accounting software</be>
   <bg>personal and small-business financial-accounting software</bg>
   <bn>personal and small-business financial-accounting software</bn>
   <ca>programari de comptabilitat personal o per petites empreses</ca>
   <cs>personal and small-business financial-accounting software</cs>
   <da>finansstyringssoftware til personer og mindre virksomheder</da>
   <de>Personal- und Kleinunternehmer-Finanzbuchhaltungssoftware</de>
   <el>Λογισμικό χρηματοοικονομικής λογιστικής και προσωπικού για μικρές επιχειρήσεις</el>
   <en>personal and small-business financial-accounting software</en>
   <es_ES>Software de contabilidad financiera personal y de pequeñas empresas</es_ES>
   <es>Software de contabilidad financiera personal y de pequeñas empresas</es>
   <et>personal and small-business financial-accounting software</et>
   <eu>personal and small-business financial-accounting software</eu>
   <fa>personal and small-business financial-accounting software</fa>
   <fil_PH>personal and small-business financial-accounting software</fil_PH>
   <fi>henkilökohtaiseen ja liikekäyttöön tarkoitettu kirjanpito-ohjelma</fi>
   <fr_BE>Logiciel de comptabilité personnelle et pour petites entreprises</fr_BE>
   <fr>Logiciel de comptabilité personnelle et pour petites entreprises</fr>
   <gl_ES>Software de contabilidade-finanzas para pequenas empresas e uso persoal</gl_ES>
   <gu>personal and small-business financial-accounting software</gu>
   <he_IL>personal and small-business financial-accounting software</he_IL>
   <hi>व्यक्तिगत व लघु-उद्योग हेतु वित्तीय-लेखांकन सॉफ्टवेयर</hi>
   <hr>personal and small-business financial-accounting software</hr>
   <hu>personal and small-business financial-accounting software</hu>
   <id>personal and small-business financial-accounting software</id>
   <is>personal and small-business financial-accounting software</is>
   <it>Contabilità personale e per piccola impresa</it>
   <ja>個人や中小企業向けの財務会計ソフト</ja>
   <kk>personal and small-business financial-accounting software</kk>
   <ko>personal and small-business financial-accounting software</ko>
   <ku>personal and small-business financial-accounting software</ku>
   <lt>personal and small-business financial-accounting software</lt>
   <mk>personal and small-business financial-accounting software</mk>
   <mr>personal and small-business financial-accounting software</mr>
   <nb_NO>personal and small-business financial-accounting software</nb_NO>
   <nb>regnskapsprogram for husholdninger og små bedrifter</nb>
   <nl_BE>personal and small-business financial-accounting software</nl_BE>
   <nl>software voor de financiële boekhouding van particulieren en kleine bedrijven</nl>
   <or>personal and small-business financial-accounting software</or>
   <pl>oprogramowanie finansowo-księgowe dla osób prywatnych i małych firm</pl>
   <pt_BR>Programa/Software de contabilidade financeira para pequenas empresas e para uso pessoal</pt_BR>
   <pt>Software de contabilidade-finanças para pequenas empresas e uso pessoal</pt>
   <ro>personal and small-business financial-accounting software</ro>
   <ru>Приложение финансового учета для персонального и малого бизнеса</ru>
   <sk>personal and small-business financial-accounting software</sk>
   <sl>finančno-računovodska programska oprema za osebno rabo in mala podjetja</sl>
   <so>personal and small-business financial-accounting software</so>
   <sq>Program financash dhe kontabiliteti në nivel personal dhe biznesi të vogël</sq>
   <sr>personal and small-business financial-accounting software</sr>
   <sv>personlig och småföretags finansiell räkenskaps-mjukvara</sv>
   <th>personal and small-business financial-accounting software</th>
   <tr>kişisel ve küçük işletme finansal muhasebe yazılımı</tr>
   <uk>програма обліку фінансів для малого бізнесу та домашнього господарства</uk>
   <vi>personal and small-business financial-accounting software</vi>
   <zh_CN>个人与小企业的财务会计软件</zh_CN>
   <zh_HK>personal and small-business financial-accounting software</zh_HK>
   <zh_TW>personal and small-business financial-accounting software</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gnucash
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gnucash
</uninstall_package_names>
</app>
