<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Messaging
</category>

<name>
1-to-1 Voice
</name>

<description>
   <am>voice chat between two pc's via encrypted mumble</am>
   <ar>voice chat between two pc's via encrypted mumble</ar>
   <be>voice chat between two pc's via encrypted mumble</be>
   <bg>voice chat between two pc's via encrypted mumble</bg>
   <bn>voice chat between two pc's via encrypted mumble</bn>
   <ca>Xat de veu entre dos PCs amb mumble encriptat</ca>
   <cs>voice chat between two pc's via encrypted mumble</cs>
   <da>stemmechat mellem to pc'er via krypteret mumble</da>
   <de>Verschlüsselte Privatgespräche mittels Mumble über SSL</de>
   <el>Φωνητική συνομιλία μεταξύ δύο ηλεκτρονικών υπολογιστών μέσω κρυπτογραφημένων mumble</el>
   <en>voice chat between two pc's via encrypted mumble</en>
   <es_ES>Chat de voz entre dos PCs vía mumble encriptado</es_ES>
   <es>Chat de voz entre dos PCs vía mumble encriptado</es>
   <et>voice chat between two pc's via encrypted mumble</et>
   <eu>voice chat between two pc's via encrypted mumble</eu>
   <fa>voice chat between two pc's via encrypted mumble</fa>
   <fil_PH>voice chat between two pc's via encrypted mumble</fil_PH>
   <fi>Äänikeskustelu kahden PC:n välillä salauksella varustetun Mumble:n kautta</fi>
   <fr_BE>Échanges vocaux chiffrés entre deux PC via mumble</fr_BE>
   <fr>Échanges vocaux chiffrés entre deux PC via mumble</fr>
   <gl_ES>Chat de voz entre dous computadore vía mumble encriptado</gl_ES>
   <gu>voice chat between two pc's via encrypted mumble</gu>
   <he_IL>שיחה קולית בין שני מחשבים דרך mumble מוצפן</he_IL>
   <hi>दो कंप्यूटर के बीच एन्क्रिप्टेड मंबल द्वारा ध्वनि चैट</hi>
   <hr>voice chat between two pc's via encrypted mumble</hr>
   <hu>voice chat between two pc's via encrypted mumble</hu>
   <id>voice chat between two pc's via encrypted mumble</id>
   <is>voice chat between two pc's via encrypted mumble</is>
   <it>Chat voce tra due pc attraverso mumble in forma criptata</it>
   <ja>暗号化したつぶやきを経由した、2PC間のチャット</ja>
   <kk>voice chat between two pc's via encrypted mumble</kk>
   <ko>voice chat between two pc's via encrypted mumble</ko>
   <ku>voice chat between two pc's via encrypted mumble</ku>
   <lt>voice chat between two pc's via encrypted mumble</lt>
   <mk>voice chat between two pc's via encrypted mumble</mk>
   <mr>voice chat between two pc's via encrypted mumble</mr>
   <nb_NO>voice chat between two pc's via encrypted mumble</nb_NO>
   <nb>nett-telefoni mellom to pc-er via kryptert mumble</nb>
   <nl_BE>voice chat between two pc's via encrypted mumble</nl_BE>
   <nl>voice chat tussen twee PCs via beveiligde mumble</nl>
   <or>voice chat between two pc's via encrypted mumble</or>
   <pl>czat głosowy pomiędzy dwoma komputerami szyfrowany przez mumble</pl>
   <pt_BR>Bate-papo de voz entre dois computadores via mumble criptografado/encriptado</pt_BR>
   <pt>Chat de voz entre dois computadores via mumble encriptado</pt>
   <ro>voice chat between two pc's via encrypted mumble</ro>
   <ru>Голосовой чат между двумя PC через шифрованный канал</ru>
   <sk>voice chat between two pc's via encrypted mumble</sk>
   <sl>Glasovni pogovor med dvema računalnikoma preko šifriranega Mumble</sl>
   <so>voice chat between two pc's via encrypted mumble</so>
   <sq>Fjalosje me zë mes dy kompjuterash, përmes Mumble-i të fshehtëzuar</sq>
   <sr>voice chat between two pc's via encrypted mumble</sr>
   <sv>röstchatt mellan två pc's via encrypted mumble</sv>
   <th>แชทเสียงระหว่างคอมพิวเตอร์ 2 เครื่องด้วย encrypted mumble</th>
   <tr>şifreli mumble ile iki bilgisayar arasında sesli söyleşi</tr>
   <uk>зашифрований голосовий чат між двома ПК</uk>
   <vi>voice chat between two pc's via encrypted mumble</vi>
   <zh_CN>在两台电脑之间通过加密 mumble 进行语音聊天</zh_CN>
   <zh_HK>voice chat between two pc's via encrypted mumble</zh_HK>
   <zh_TW>語音聊天在兩台電腦間加密的聲訊</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
1-to-1-voice-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
1-to-1-voice-antix
</uninstall_package_names>
</app>
