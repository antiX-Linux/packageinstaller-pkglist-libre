<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
SuperTuxKart
</name>

<description>
   <am>Mario kart style racing</am>
   <ar>Mario kart style racing</ar>
   <be>Mario kart style racing</be>
   <bg>Mario kart style racing</bg>
   <bn>Mario kart style racing</bn>
   <ca>Curses del tipus Mario Kart</ca>
   <cs>Mario kart style racing</cs>
   <da>Racerspil i stil med Mario kart</da>
   <de>Mario-Kart-Rennen</de>
   <el>Mario kart style racing</el>
   <en>Mario kart style racing</en>
   <es_ES>Juego de carreras estilo Mario kart</es_ES>
   <es>Juego de carreras estilo Mario kart</es>
   <et>Mario kart style racing</et>
   <eu>Mario kart style racing</eu>
   <fa>Mario kart style racing</fa>
   <fil_PH>Mario kart style racing</fil_PH>
   <fi>Mario Kartin tyylinen kilpa-ajopeli</fi>
   <fr_BE>Jeu de courses de type Mario-kart</fr_BE>
   <fr>Jeu de courses de type Mario-kart</fr>
   <gl_ES>Xogo de vídeo de carreiras de karts, do tipo Marío kart</gl_ES>
   <gu>Mario kart style racing</gu>
   <he_IL>Mario kart style racing</he_IL>
   <hi>मारिओ कार्ट स्टाइल रेस</hi>
   <hr>Mario kart style racing</hr>
   <hu>Mario kart style racing</hu>
   <id>Mario kart style racing</id>
   <is>Mario kart style racing</is>
   <it>gioco di corse con i kart stile SuperMario</it>
   <ja>マリオカート風のレースゲーム</ja>
   <kk>Mario kart style racing</kk>
   <ko>Mario kart style racing</ko>
   <ku>Mario kart style racing</ku>
   <lt>Mario kart style racing</lt>
   <mk>Mario kart style racing</mk>
   <mr>Mario kart style racing</mr>
   <nb_NO>Mario kart style racing</nb_NO>
   <nb>Super Mario Kart-lignende kappkjøring</nb>
   <nl_BE>Mario kart style racing</nl_BE>
   <nl>Mario kart stijl racen</nl>
   <or>Mario kart style racing</or>
   <pl>wyścigi w stylu Mario Kart</pl>
   <pt_BR>Jogo de vídeo de corridas de carte, estilo o jogo Mario Kart</pt_BR>
   <pt>Jogo de vídeo de corridas de karts, do tipo Mario kart</pt>
   <ro>Mario kart style racing</ro>
   <ru>Гонки на картах в стиле Mario</ru>
   <sk>Mario kart style racing</sk>
   <sl>Mario kartingu podobna  igra</sl>
   <so>Mario kart style racing</so>
   <sq>Gara me makina kart në stil Mario</sq>
   <sr>Mario kart style racing</sr>
   <sv>Mario kart stil racing</sv>
   <th>Mario kart style racing</th>
   <tr>Mario kart tarzı yarış</tr>
   <uk>Mario kart style racing</uk>
   <vi>Mario kart style racing</vi>
   <zh_CN>Mario kart style racing</zh_CN>
   <zh_HK>Mario kart style racing</zh_HK>
   <zh_TW>Mario kart style racing</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
supertuxkart
supertuxkart-data
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
supertuxkart
supertuxkart-data
</uninstall_package_names>
</app>
