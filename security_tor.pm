<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
Tor and Privoxy
</name>

<description>
   <am>Tor and Privoxy</am>
   <ar>Tor and Privoxy</ar>
   <be>Tor and Privoxy</be>
   <bg>Tor and Privoxy</bg>
   <bn>Tor and Privoxy</bn>
   <ca>Tor i Privoxy</ca>
   <cs>Tor and Privoxy</cs>
   <da>Tor og Privoxy</da>
   <de>Tor und Privoxy</de>
   <el>Tor και Privoxy</el>
   <en>Tor and Privoxy</en>
   <es_ES>Tor y Privoxy</es_ES>
   <es>Tor y Privoxy</es>
   <et>Tor and Privoxy</et>
   <eu>Tor and Privoxy</eu>
   <fa>Tor and Privoxy</fa>
   <fil_PH>Tor and Privoxy</fil_PH>
   <fi>Tor ja Privoxy</fi>
   <fr_BE>Tor et Privoxy</fr_BE>
   <fr>Tor et Privoxy</fr>
   <gl_ES>Tor and Privoxy</gl_ES>
   <gu>Tor and Privoxy</gu>
   <he_IL>Tor and Privoxy</he_IL>
   <hi>Tor व Privoxy</hi>
   <hr>Tor and Privoxy</hr>
   <hu>Tor and Privoxy</hu>
   <id>Tor and Privoxy</id>
   <is>Tor and Privoxy</is>
   <it>Tor e Privoxy</it>
   <ja>Tor と Privoxy</ja>
   <kk>Tor and Privoxy</kk>
   <ko>Tor and Privoxy</ko>
   <ku>Tor and Privoxy</ku>
   <lt>Tor ir Privoxy</lt>
   <mk>Tor and Privoxy</mk>
   <mr>Tor and Privoxy</mr>
   <nb_NO>Tor and Privoxy</nb_NO>
   <nb>Tor og Privoxy</nb>
   <nl_BE>Tor and Privoxy</nl_BE>
   <nl>Tor en Privoxy</nl>
   <or>Tor and Privoxy</or>
   <pl>przeglądarka Tor i serwer proxy Privoxy</pl>
   <pt_BR>Tor e Privoxy - Anonimato, privacidade e segurança ao navegar na internet</pt_BR>
   <pt>Tor and Privoxy</pt>
   <ro>Tor and Privoxy</ro>
   <ru>Tor и Privoxy - обеспечение анонимности и фильтрации веб</ru>
   <sk>Tor and Privoxy</sk>
   <sl>Tor in Privoxy</sl>
   <so>Tor and Privoxy</so>
   <sq>Tor dhe Privoxy</sq>
   <sr>Tor and Privoxy</sr>
   <sv>Tor och Privoxy</sv>
   <th>Tor และ Privoxy</th>
   <tr>Tor ve Privoxy</tr>
   <uk>Tor and Privoxy</uk>
   <vi>Tor and Privoxy</vi>
   <zh_CN>Tor and Privoxy</zh_CN>
   <zh_HK>Tor and Privoxy</zh_HK>
   <zh_TW>Tor and Privoxy</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
tor
privoxy
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
tor
privoxy
</uninstall_package_names>
</app>
