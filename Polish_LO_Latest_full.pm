<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Polish_LO_Latest_full
</name>

<description>
   <am>Polish Language Meta-Package for LibreOffice</am>
   <ar>Polish Language Meta-Package for LibreOffice</ar>
   <be>Polish Language Meta-Package for LibreOffice</be>
   <bg>Polish Language Meta-Package for LibreOffice</bg>
   <bn>Polish Language Meta-Package for LibreOffice</bn>
   <ca>Meta-paquet de llengua per LibreOffice en Polonès</ca>
   <cs>Polish Language Meta-Package for LibreOffice</cs>
   <da>Polsk sprog-metapakke til LibreOffice</da>
   <de>Polnisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Πολωνικά</el>
   <en>Polish Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Idioma Polaco para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Polaco para LibreOffice</es>
   <et>Polish Language Meta-Package for LibreOffice</et>
   <eu>Polish Language Meta-Package for LibreOffice</eu>
   <fa>Polish Language Meta-Package for LibreOffice</fa>
   <fil_PH>Polish Language Meta-Package for LibreOffice</fil_PH>
   <fi>Puolalainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue polonaise pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue polonaise pour LibreOffice</fr>
   <gl_ES>Polaco Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Polish Language Meta-Package for LibreOffice</gu>
   <he_IL>Polish Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु पोलिश भाषा मेटा-पैकेज</hi>
   <hr>Polish Language Meta-Package for LibreOffice</hr>
   <hu>Polish Language Meta-Package for LibreOffice</hu>
   <id>Polish Language Meta-Package for LibreOffice</id>
   <is>Polish Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua polacca per LibreOffice</it>
   <ja>LibreOffice用ポーランド語メタパッケージ</ja>
   <kk>Polish Language Meta-Package for LibreOffice</kk>
   <ko>Polish Language Meta-Package for LibreOffice</ko>
   <ku>Polish Language Meta-Package for LibreOffice</ku>
   <lt>Polish Language Meta-Package for LibreOffice</lt>
   <mk>Polish Language Meta-Package for LibreOffice</mk>
   <mr>Polish Language Meta-Package for LibreOffice</mr>
   <nb_NO>Polish Language Meta-Package for LibreOffice</nb_NO>
   <nb>Polsk språkpakke for LibreOffice</nb>
   <nl_BE>Polish Language Meta-Package for LibreOffice</nl_BE>
   <nl>Poolse Taal Meta-Pakket voor LibreOffice</nl>
   <or>Polish Language Meta-Package for LibreOffice</or>
   <pl>Polski metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Polonês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Polaco Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Polish Language Meta-Package for LibreOffice</ro>
   <ru>Polish Language Meta-Package for LibreOffice</ru>
   <sk>Polish Language Meta-Package for LibreOffice</sk>
   <sl>Poljski jezikovni meta-paket za LibreOffice</sl>
   <so>Polish Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në polonisht</sq>
   <sr>Polish Language Meta-Package for LibreOffice</sr>
   <sv>Polskt Språk Meta-Paket för LibreOffice </sv>
   <th>Meta-Package ภาษา Polish สำหรับ LibreOffice</th>
   <tr>LibreOffice için Lehçe Dili Üst-Paketi</tr>
   <uk>Polish Language Meta-Package for LibreOffice</uk>
   <vi>Polish Language Meta-Package for LibreOffice</vi>
   <zh_CN>Polish Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Polish Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Polish Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-pl
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-pl
</uninstall_package_names>

</app>
