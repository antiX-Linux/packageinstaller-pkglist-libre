<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Swedish
</name>

<description>
   <am>Swedish dictionary for hunspell</am>
   <ar>Swedish dictionary for hunspell</ar>
   <be>Swedish dictionary for hunspell</be>
   <bg>Swedish dictionary for hunspell</bg>
   <bn>Swedish dictionary for hunspell</bn>
   <ca>Diccionari Suec per hunspell</ca>
   <cs>Swedish dictionary for hunspell</cs>
   <da>Swedish dictionary for hunspell</da>
   <de>Schwedisches Wörterbuch für “Hunspell”</de>
   <el>Σουηδικά λεξικό για hunspell</el>
   <en>Swedish dictionary for hunspell</en>
   <es_ES>Diccionario Sueco para hunspell</es_ES>
   <es>Diccionario Sueco para hunspell</es>
   <et>Swedish dictionary for hunspell</et>
   <eu>Swedish dictionary for hunspell</eu>
   <fa>Swedish dictionary for hunspell</fa>
   <fil_PH>Swedish dictionary for hunspell</fil_PH>
   <fi>Swedish dictionary for hunspell</fi>
   <fr_BE>Suédois dictionnaire pour hunspell</fr_BE>
   <fr>Suédois dictionnaire pour hunspell</fr>
   <gl_ES>Swedish dictionary for hunspell</gl_ES>
   <gu>Swedish dictionary for hunspell</gu>
   <he_IL>Swedish dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु स्वीडिश शब्दकोष</hi>
   <hr>Swedish dictionary for hunspell</hr>
   <hu>Swedish dictionary for hunspell</hu>
   <id>Swedish dictionary for hunspell</id>
   <is>Swedish dictionary for hunspell</is>
   <it>Dizionario svedese per hunspell</it>
   <ja>Hunspell 用スウェーデン語辞書</ja>
   <kk>Swedish dictionary for hunspell</kk>
   <ko>Swedish dictionary for hunspell</ko>
   <ku>Swedish dictionary for hunspell</ku>
   <lt>Swedish dictionary for hunspell</lt>
   <mk>Swedish dictionary for hunspell</mk>
   <mr>Swedish dictionary for hunspell</mr>
   <nb_NO>Swedish dictionary for hunspell</nb_NO>
   <nb>Svensk ordliste for hunspell</nb>
   <nl_BE>Swedish dictionary for hunspell</nl_BE>
   <nl>Swedish dictionary for hunspell</nl>
   <or>Swedish dictionary for hunspell</or>
   <pl>Swedish dictionary for hunspell</pl>
   <pt_BR>Dicionário Sueco para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Sueco para hunspell</pt>
   <ro>Swedish dictionary for hunspell</ro>
   <ru>Swedish dictionary for hunspell</ru>
   <sk>Swedish dictionary for hunspell</sk>
   <sl>Švedski slovar za hunspell</sl>
   <so>Swedish dictionary for hunspell</so>
   <sq>Fjalor suedisht për hunspell</sq>
   <sr>Swedish dictionary for hunspell</sr>
   <sv>Svensk ordbok för hunspell</sv>
   <th>Swedish dictionary for hunspell</th>
   <tr>Hunspell için İsveççe sözlük</tr>
   <uk>Swedish dictionary for hunspell</uk>
   <vi>Swedish dictionary for hunspell</vi>
   <zh_CN>Swedish dictionary for hunspell</zh_CN>
   <zh_HK>Swedish dictionary for hunspell</zh_HK>
   <zh_TW>Swedish dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-sv
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-sv
</uninstall_package_names>

</app>
