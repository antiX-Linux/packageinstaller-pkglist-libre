<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Docks
</category>

<name>
Plank
</name>

<description>
   <am>Simple but good looking dock</am>
   <ar>Simple but good looking dock</ar>
   <be>Simple but good looking dock</be>
   <bg>Simple but good looking dock</bg>
   <bn>Simple but good looking dock</bn>
   <ca>Dock senzill però amb bon aspecte</ca>
   <cs>Simple but good looking dock</cs>
   <da>Simpel med flot dok</da>
   <de>Einfaches, aber gut aussehendes Dock</de>
   <el>Απλό αλλά όμορφο dock</el>
   <en>Simple but good looking dock</en>
   <es_ES>dock simple y atractivo</es_ES>
   <es>dock simple y atractivo</es>
   <et>Simple but good looking dock</et>
   <eu>Simple but good looking dock</eu>
   <fa>Simple but good looking dock</fa>
   <fil_PH>Simple but good looking dock</fil_PH>
   <fi>Yksinkertainen mutta hyvännäköinen telakka</fi>
   <fr_BE>Un dock sobre et agréable</fr_BE>
   <fr>Un dock sobre et agréable</fr>
   <gl_ES>Dock simple mais elegante</gl_ES>
   <gu>Simple but good looking dock</gu>
   <he_IL>Simple but good looking dock</he_IL>
   <hi>सरल परन्तु आकर्षक डॉक</hi>
   <hr>Simple but good looking dock</hr>
   <hu>Simple but good looking dock</hu>
   <id>Simple but good looking dock</id>
   <is>Simple but good looking dock</is>
   <it>barra dock semplice ma bella</it>
   <ja>シンプルで見栄えの良いドック</ja>
   <kk>Simple but good looking dock</kk>
   <ko>Simple but good looking dock</ko>
   <ku>Simple but good looking dock</ku>
   <lt>Simple but good looking dock</lt>
   <mk>Simple but good looking dock</mk>
   <mr>Simple but good looking dock</mr>
   <nb_NO>Simple but good looking dock</nb_NO>
   <nb>Enkel, men pen programmeny</nb>
   <nl_BE>Simple but good looking dock</nl_BE>
   <nl>Eenvoudig maar goed uitziende dock</nl>
   <or>Simple but good looking dock</or>
   <pl>prosty, ale dobrze wyglądający panel dokujący</pl>
   <pt_BR>Doca (dock) simples mas elegante</pt_BR>
   <pt>Doca simples mas elegante</pt>
   <ro>Simple but good looking dock</ro>
   <ru>Простой но стильно выглядящий док</ru>
   <sk>Simple but good looking dock</sk>
   <sl>Enostaven a lep dock</sl>
   <so>Simple but good looking dock</so>
   <sq>Simple but good looking dock</sq>
   <sr>Simple but good looking dock</sr>
   <sv>Enkel men snygg docka</sv>
   <th>Dock ที่เรียบง่ายแต่ดูดี</th>
   <tr>Basit ama güzel görünümlü panel</tr>
   <uk>простий, але гарний док</uk>
   <vi>Simple but good looking dock</vi>
   <zh_CN>Simple but good looking dock</zh_CN>
   <zh_HK>Simple but good looking dock</zh_HK>
   <zh_TW>Simple but good looking dock</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
plank
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
plank
</uninstall_package_names>
</app>
