<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Slovenian_Thunderbird
</name>

<description>
   <am>Slovenian localisation of Thunderbird</am>
   <ar>Slovenian localisation of Thunderbird</ar>
   <be>Slovenian localisation of Thunderbird</be>
   <bg>Slovenian localisation of Thunderbird</bg>
   <bn>Slovenian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Eslovè</ca>
   <cs>Slovenian localisation of Thunderbird</cs>
   <da>Slovensk oversættelse af Thunderbird</da>
   <de>Slowenische Lokalisierung von Thunderbird</de>
   <el>Σλοβενικά για το Thunderbird</el>
   <en>Slovenian localisation of Thunderbird</en>
   <es_ES>Localización Eslovena de Thunderbird</es_ES>
   <es>Localización Esloveno de Thunderbird</es>
   <et>Slovenian localisation of Thunderbird</et>
   <eu>Slovenian localisation of Thunderbird</eu>
   <fa>Slovenian localisation of Thunderbird</fa>
   <fil_PH>Slovenian localisation of Thunderbird</fil_PH>
   <fi>Slovenialainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en slovène pour Thunderbird</fr_BE>
   <fr>Localisation en slovène pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao esloveno</gl_ES>
   <gu>Slovenian localisation of Thunderbird</gu>
   <he_IL>Slovenian localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का स्लोवेनियाई संस्करण</hi>
   <hr>Slovenian localisation of Thunderbird</hr>
   <hu>Slovenian localisation of Thunderbird</hu>
   <id>Slovenian localisation of Thunderbird</id>
   <is>Slovenian localisation of Thunderbird</is>
   <it>Localizzazione slovena di Thunderbird</it>
   <ja>Thunderbird のスロベニア語版</ja>
   <kk>Slovenian localisation of Thunderbird</kk>
   <ko>Slovenian localisation of Thunderbird</ko>
   <ku>Slovenian localisation of Thunderbird</ku>
   <lt>Slovenian localisation of Thunderbird</lt>
   <mk>Slovenian localisation of Thunderbird</mk>
   <mr>Slovenian localisation of Thunderbird</mr>
   <nb_NO>Slovenian localisation of Thunderbird</nb_NO>
   <nb>Slovensk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Slovenian localisation of Thunderbird</nl_BE>
   <nl>Sloveense lokalisatie van Thunderbird</nl>
   <or>Slovenian localisation of Thunderbird</or>
   <pl>Słoweńska lokalizacja Thunderbirda</pl>
   <pt_BR>Esloveno Localização para o Thunderbird</pt_BR>
   <pt>Esloveno Localização para Thunderbird</pt>
   <ro>Slovenian localisation of Thunderbird</ro>
   <ru>Slovenian localisation of Thunderbird</ru>
   <sk>Slovenian localisation of Thunderbird</sk>
   <sl>Slovenske krajevne nastavitve za Thunderbird</sl>
   <so>Slovenian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në sllovenisht</sq>
   <sr>Slovenian localisation of Thunderbird</sr>
   <sv>Slovensk lokalisering av Thunderbird</sv>
   <th>Slovenian localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Slovence yerelleştirmesi</tr>
   <uk>Slovenian localisation of Thunderbird</uk>
   <vi>Slovenian localisation of Thunderbird</vi>
   <zh_CN>Slovenian localisation of Thunderbird</zh_CN>
   <zh_HK>Slovenian localisation of Thunderbird</zh_HK>
   <zh_TW>Slovenian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-sl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-sl
</uninstall_package_names>

</app>
