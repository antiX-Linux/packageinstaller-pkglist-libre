<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English medical
</name>

<description>
   <am>English medical dictionary for hunspell</am>
   <ar>English medical dictionary for hunspell</ar>
   <be>English medical dictionary for hunspell</be>
   <bg>English medical dictionary for hunspell</bg>
   <bn>English medical dictionary for hunspell</bn>
   <ca>Diccionari Anglès mèdic per hunspell</ca>
   <cs>English medical dictionary for hunspell</cs>
   <da>English medical dictionary for hunspell</da>
   <de>Englisches medizinisches Wörterbuch für “Hunspell”</de>
   <el>Αγγλικό ιατρικό λεξικό για το hunspell</el>
   <en>English medical dictionary for hunspell</en>
   <es_ES>Diccionario médico inglés para hunspell</es_ES>
   <es>Diccionario médico Inglés para hunspell</es>
   <et>English medical dictionary for hunspell</et>
   <eu>English medical dictionary for hunspell</eu>
   <fa>English medical dictionary for hunspell</fa>
   <fil_PH>English medical dictionary for hunspell</fil_PH>
   <fi>English medical dictionary for hunspell</fi>
   <fr_BE>Anglais médical dictionnaire pour hunspell</fr_BE>
   <fr>Anglais médical dictionnaire pour hunspell</fr>
   <gl_ES>English medical dictionary for hunspell</gl_ES>
   <gu>English medical dictionary for hunspell</gu>
   <he_IL>English medical dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु चिकित्सा अंग्रेजी शब्दकोष</hi>
   <hr>English medical dictionary for hunspell</hr>
   <hu>English medical dictionary for hunspell</hu>
   <id>English medical dictionary for hunspell</id>
   <is>English medical dictionary for hunspell</is>
   <it>Dizionario di inglese-medico per hunspell</it>
   <ja>Hunspell 用の英語医学辞書</ja>
   <kk>English medical dictionary for hunspell</kk>
   <ko>English medical dictionary for hunspell</ko>
   <ku>English medical dictionary for hunspell</ku>
   <lt>English medical dictionary for hunspell</lt>
   <mk>English medical dictionary for hunspell</mk>
   <mr>English medical dictionary for hunspell</mr>
   <nb_NO>English medical dictionary for hunspell</nb_NO>
   <nb>Engelsk medisinsk ordliste for hunspell</nb>
   <nl_BE>English medical dictionary for hunspell</nl_BE>
   <nl>Engels medisch woordenboek voor hunspell</nl>
   <or>English medical dictionary for hunspell</or>
   <pl>English medical dictionary for hunspell</pl>
   <pt_BR>Dicionário Médico em Inglês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário médico Inglês para hunspell</pt>
   <ro>English medical dictionary for hunspell</ro>
   <ru>English medical dictionary for hunspell</ru>
   <sk>English medical dictionary for hunspell</sk>
   <sl>Angleški medicinski slovar za hunspell</sl>
   <so>English medical dictionary for hunspell</so>
   <sq>Fjalor anglisht mjekësor për hunspell</sq>
   <sr>English medical dictionary for hunspell</sr>
   <sv>Engelsk medicinsk ordbok för hunspell</sv>
   <th>English medical dictionary for hunspell</th>
   <tr>Hunspell için İngilizce tıp sözlüğü</tr>
   <uk>English medical dictionary for hunspell</uk>
   <vi>English medical dictionary for hunspell</vi>
   <zh_CN>English medical dictionary for hunspell</zh_CN>
   <zh_HK>English medical dictionary for hunspell</zh_HK>
   <zh_TW>English medical dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-en-med
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-en-med
</uninstall_package_names>

</app>
