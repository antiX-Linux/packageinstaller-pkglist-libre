<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Korean_LO_Latest_main
</name>

<description>
   <am>Korean localisation of LibreOffice</am>
   <ar>Korean localisation of LibreOffice</ar>
   <be>Korean localisation of LibreOffice</be>
   <bg>Korean localisation of LibreOffice</bg>
   <bn>Korean localisation of LibreOffice</bn>
   <ca>Localització en Coreà per a LibreOffice</ca>
   <cs>Korean localisation of LibreOffice</cs>
   <da>Koreansk oversættelse af LibreOffice</da>
   <de>Koreanische Lokalisierung von LibreOffice</de>
   <el>LibreOffice στα Κορεατικά</el>
   <en>Korean localisation of LibreOffice</en>
   <es_ES>Localización Coreana de LibreOffice</es_ES>
   <es>Localización Coreano de LibreOffice</es>
   <et>Korean localisation of LibreOffice</et>
   <eu>Korean localisation of LibreOffice</eu>
   <fa>Korean localisation of LibreOffice</fa>
   <fil_PH>Korean localisation of LibreOffice</fil_PH>
   <fi>Korealainen kotoistus LibreOffice:lle</fi>
   <fr_BE>Localisation en coréen pour LibreOffice</fr_BE>
   <fr>Localisation en coréen pour LibreOffice</fr>
   <gl_ES>Localización do LibreOffice ao coreano</gl_ES>
   <gu>Korean localisation of LibreOffice</gu>
   <he_IL>תרגומי LibreOffice לקוריאנית</he_IL>
   <hi>लिब्रे-ऑफिस का कोरियाई संस्करण</hi>
   <hr>Korean localisation of LibreOffice</hr>
   <hu>Korean localisation of LibreOffice</hu>
   <id>Korean localisation of LibreOffice</id>
   <is>Korean localisation of LibreOffice</is>
   <it>Localizzazione coreana di LibreOffice</it>
   <ja>LibreOffice の韓国（朝鮮）語版</ja>
   <kk>Korean localisation of LibreOffice</kk>
   <ko>Korean localisation of LibreOffice</ko>
   <ku>Korean localisation of LibreOffice</ku>
   <lt>Korean localisation of LibreOffice</lt>
   <mk>Korean localisation of LibreOffice</mk>
   <mr>Korean localisation of LibreOffice</mr>
   <nb_NO>Korean localisation of LibreOffice</nb_NO>
   <nb>Koreansk lokaltilpassing av LibreOffice</nb>
   <nl_BE>Korean localisation of LibreOffice</nl_BE>
   <nl>Koreaanse lokalisatie van LibreOffice</nl>
   <or>Korean localisation of LibreOffice</or>
   <pl>Koreańska lokalizacja LibreOffice</pl>
   <pt_BR>Coreano Localização para o LibreOffice</pt_BR>
   <pt>Coreano Localização para LibreOffice</pt>
   <ro>Korean localisation of LibreOffice</ro>
   <ru>Korean localisation of LibreOffice</ru>
   <sk>Korean localisation of LibreOffice</sk>
   <sl>Korejske krajevne nastavitve za LibreOffice</sl>
   <so>Korean localisation of LibreOffice</so>
   <sq>Përkthim i LibreOffice-it në koreançe</sq>
   <sr>Korean localisation of LibreOffice</sr>
   <sv>Koreansk lokalisering av LibreOffice</sv>
   <th>Korean localisation ของ LibreOffice</th>
   <tr>LibreOffice Korece yerelleştirmesi</tr>
   <uk>Korean локалізація LibreOffice</uk>
   <vi>Korean localisation of LibreOffice</vi>
   <zh_CN>Korean localisation of LibreOffice</zh_CN>
   <zh_HK>Korean localisation of LibreOffice</zh_HK>
   <zh_TW>Korean localisation of LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ko
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ko
libreoffice-gtk3
</uninstall_package_names>

</app>
