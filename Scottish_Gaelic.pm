<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Scottish Gaelic
</name>

<description>
   <am>Scottish Gaelic dictionary for hunspell</am>
   <ar>Scottish Gaelic dictionary for hunspell</ar>
   <be>Scottish Gaelic dictionary for hunspell</be>
   <bg>Scottish Gaelic dictionary for hunspell</bg>
   <bn>Scottish Gaelic dictionary for hunspell</bn>
   <ca>Diccionari Gaèlic per hunspell</ca>
   <cs>Scottish Gaelic dictionary for hunspell</cs>
   <da>Scottish Gaelic dictionary for hunspell</da>
   <de>Scottish Gaelic dictionary for hunspell</de>
   <el>Λεξικό Σκωτίας Γαελικά για hunspell</el>
   <en>Scottish Gaelic dictionary for hunspell</en>
   <es_ES>Diccionario gaélico escocés para hunspell</es_ES>
   <es>Diccionario gaélico escocés para hunspell</es>
   <et>Scottish Gaelic dictionary for hunspell</et>
   <eu>Scottish Gaelic dictionary for hunspell</eu>
   <fa>Scottish Gaelic dictionary for hunspell</fa>
   <fil_PH>Scottish Gaelic dictionary for hunspell</fil_PH>
   <fi>Scottish Gaelic dictionary for hunspell</fi>
   <fr_BE>Gaélique écossais dictionnaire pour hunspell</fr_BE>
   <fr>Gaélique écossais dictionnaire pour hunspell</fr>
   <gl_ES>Scottish Gaelic dictionary for hunspell</gl_ES>
   <gu>Scottish Gaelic dictionary for hunspell</gu>
   <he_IL>Scottish Gaelic dictionary for hunspell</he_IL>
   <hi>Scottish Gaelic dictionary for hunspell</hi>
   <hr>Scottish Gaelic dictionary for hunspell</hr>
   <hu>Scottish Gaelic dictionary for hunspell</hu>
   <id>Scottish Gaelic dictionary for hunspell</id>
   <is>Scottish Gaelic dictionary for hunspell</is>
   <it>Dizionario Scottish Gaelic per hunspell</it>
   <ja>Scottish Gaelic dictionary for hunspell</ja>
   <kk>Scottish Gaelic dictionary for hunspell</kk>
   <ko>Scottish Gaelic dictionary for hunspell</ko>
   <ku>Scottish Gaelic dictionary for hunspell</ku>
   <lt>Scottish Gaelic dictionary for hunspell</lt>
   <mk>Scottish Gaelic dictionary for hunspell</mk>
   <mr>Scottish Gaelic dictionary for hunspell</mr>
   <nb_NO>Scottish Gaelic dictionary for hunspell</nb_NO>
   <nb>Skotsk-gælisk ordliste for hunspell</nb>
   <nl_BE>Scottish Gaelic dictionary for hunspell</nl_BE>
   <nl>Scottish Gaelic dictionary for hunspell</nl>
   <or>Scottish Gaelic dictionary for hunspell</or>
   <pl>Scottish Gaelic dictionary for hunspell</pl>
   <pt_BR>Dicionário Gaélico Escocês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Gaélico da Escócia para hunspell</pt>
   <ro>Scottish Gaelic dictionary for hunspell</ro>
   <ru>Scottish Gaelic dictionary for hunspell</ru>
   <sk>Scottish Gaelic dictionary for hunspell</sk>
   <sl>Škotsko gelški slovar za hunspell</sl>
   <so>Scottish Gaelic dictionary for hunspell</so>
   <sq>Fjalor gaelikeje skoceze për hunspell</sq>
   <sr>Scottish Gaelic dictionary for hunspell</sr>
   <sv>Skottsk Gaelisk ordbok för hunspell</sv>
   <th>Scottish Gaelic dictionary for hunspell</th>
   <tr>Scottish Gaelic dictionary for hunspell</tr>
   <uk>Scottish Gaelic dictionary for hunspell</uk>
   <vi>Scottish Gaelic dictionary for hunspell</vi>
   <zh_CN>Scottish Gaelic dictionary for hunspell</zh_CN>
   <zh_HK>Scottish Gaelic dictionary for hunspell</zh_HK>
   <zh_TW>Scottish Gaelic dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-gd
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-gd
</uninstall_package_names>

</app>
