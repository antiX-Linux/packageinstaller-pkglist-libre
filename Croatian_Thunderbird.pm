<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Croatian_Thunderbird
</name>

<description>
   <am>Croatian localisation of thunderbird</am>
   <ar>Croatian localisation of thunderbird</ar>
   <be>Croatian localisation of thunderbird</be>
   <bg>Croatian localisation of thunderbird</bg>
   <bn>Croatian localisation of thunderbird</bn>
   <ca>Localització de Thunderbird en Croata</ca>
   <cs>Croatian localisation of thunderbird</cs>
   <da>Kroatisk oversættelse af Thunderbird</da>
   <de>Kroatische Lokalisierung von Thunderbird</de>
   <el>Κροατικά για το thunderbird</el>
   <en>Croatian localisation of thunderbird</en>
   <es_ES>Localización Croata de Thunderbird</es_ES>
   <es>Localización Croata de Thunderbird</es>
   <et>Croatian localisation of thunderbird</et>
   <eu>Croatian localisation of thunderbird</eu>
   <fa>Croatian localisation of thunderbird</fa>
   <fil_PH>Croatian localisation of thunderbird</fil_PH>
   <fi>Kroatialainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en croate pour Thunderbird</fr_BE>
   <fr>Localisation en croate pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird para croata</gl_ES>
   <gu>Croatian localisation of thunderbird</gu>
   <he_IL>Croatian localisation of thunderbird</he_IL>
   <hi>थंडरबर्ड का क्रोएशियाई संस्करण</hi>
   <hr>Croatian localisation of thunderbird</hr>
   <hu>Croatian localisation of thunderbird</hu>
   <id>Croatian localisation of thunderbird</id>
   <is>Croatian localisation of thunderbird</is>
   <it>Localizzazione croata di Thunderbird</it>
   <ja>Thunderbird のクロアチア語版</ja>
   <kk>Croatian localisation of thunderbird</kk>
   <ko>Croatian localisation of thunderbird</ko>
   <ku>Croatian localisation of thunderbird</ku>
   <lt>Croatian localisation of thunderbird</lt>
   <mk>Croatian localisation of thunderbird</mk>
   <mr>Croatian localisation of thunderbird</mr>
   <nb_NO>Croatian localisation of thunderbird</nb_NO>
   <nb>Kroatisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Croatian localisation of thunderbird</nl_BE>
   <nl>Kroatische lokalisatie van Thunderbird</nl>
   <or>Croatian localisation of thunderbird</or>
   <pl>Chorwacka lokalizacja Thunderbirda</pl>
   <pt_BR>Croata Localização para o Thunderbird</pt_BR>
   <pt>Croata Localização para Thunderbird</pt>
   <ro>Croatian localisation of thunderbird</ro>
   <ru>Croatian localisation of thunderbird</ru>
   <sk>Croatian localisation of thunderbird</sk>
   <sl>Hrvaške krajevne nastavitve za thunderbird</sl>
   <so>Croatian localisation of thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në kroatisht</sq>
   <sr>Croatian localisation of thunderbird</sr>
   <sv>Kroatisk lokalisering av thunderbird</sv>
   <th>Croatian localisation ของ Thunderbird</th>
   <tr>Thunderbird Hırvatça yerelleştirmesi</tr>
   <uk>Croatian локалізація thunderbird</uk>
   <vi>Croatian localisation of thunderbird</vi>
   <zh_CN>Thunderbird 克罗地亚语语言包</zh_CN>
   <zh_HK>Croatian localisation of thunderbird</zh_HK>
   <zh_TW>Croatian localisation of thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-hr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-hr
</uninstall_package_names>

</app>
