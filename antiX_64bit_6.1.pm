<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Kernels
</category>

<name>
Kernel-antiX_64bit__LTS_6.1
</name>

<description>
   <am>antiX Kernel 64 bit  LTS (6.1.55)</am>
   <ar>antiX Kernel 64 bit  LTS (6.1.55)</ar>
   <be>antiX Kernel 64 bit  LTS (6.1.55)</be>
   <bg>antiX Kernel 64 bit  LTS (6.1.55)</bg>
   <bn>antiX Kernel 64 bit  LTS (6.1.55)</bn>
   <ca>Kernel antiX 64 bit LTS (6.1.55)</ca>
   <cs>antiX Kernel 64 bit  LTS (6.1.55)</cs>
   <da>antiX Kernel 64 bit  LTS (6.1.55)</da>
   <de>antiX Kernel 64 bit  LTS (6.1.55)</de>
   <el>antiX πυρήνα 64 bit (6.1.55)</el>
   <en>antiX Kernel 64 bit  LTS (6.1.55)</en>
   <es_ES>Kernel antiX 64 bit LTS (6.1.55)</es_ES>
   <es>Kernel antiX 64 bit LTS (6.1.55)</es>
   <et>antiX Kernel 64 bit  LTS (6.1.55)</et>
   <eu>antiX Kernel 64 bit  LTS (6.1.55)</eu>
   <fa>antiX Kernel 64 bit  LTS (6.1.55)</fa>
   <fil_PH>antiX Kernel 64 bit  LTS (6.1.55)</fil_PH>
   <fi>antiX Kernel 64 bit  LTS (6.1.55)</fi>
   <fr_BE>antiX Noyau 64 bit LTS (6.1.55)</fr_BE>
   <fr>antiX Noyau 64 bit LTS (6.1.55)</fr>
   <gl_ES>antiX Kernel 64 bit  LTS (6.1.55)</gl_ES>
   <gu>antiX Kernel 64 bit  LTS (6.1.55)</gu>
   <he_IL>antiX Kernel 64 bit  LTS (6.1.55)</he_IL>
   <hi>antiX Kernel 64 bit  LTS (6.1.55)</hi>
   <hr>antiX Kernel 64 bit  LTS (6.1.55)</hr>
   <hu>antiX Kernel 64 bit  LTS (6.1.55)</hu>
   <id>antiX Kernel 64 bit  LTS (6.1.55)</id>
   <is>antiX Kernel 64 bit  LTS (6.1.55)</is>
   <it>Kernel antiX LTS 64 bit (6.1.55)</it>
   <ja>antiX Kernel 64 bit  LTS (6.1.55)</ja>
   <kk>antiX Kernel 64 bit  LTS (6.1.55)</kk>
   <ko>antiX Kernel 64 bit  LTS (6.1.55)</ko>
   <ku>antiX Kernel 64 bit  LTS (6.1.55)</ku>
   <lt>antiX Kernel 64 bit  LTS (6.1.55)</lt>
   <mk>antiX Kernel 64 bit  LTS (6.1.55)</mk>
   <mr>antiX Kernel 64 bit  LTS (6.1.55)</mr>
   <nb_NO>antiX Kernel 64 bit  LTS (6.1.55)</nb_NO>
   <nb>antiX-kjerne, 64 biters (6.1.55)</nb>
   <nl_BE>antiX Kernel 64 bit  LTS (6.1.55)</nl_BE>
   <nl>antiX Kernel 64 bit LTS (6.1.55)</nl>
   <or>antiX Kernel 64 bit  LTS (6.1.55)</or>
   <pl>antiX Kernel 64 bit  LTS (6.1.55)</pl>
   <pt_BR>Núcleo (Kernel) do antiX 6.1.55 de 64 bits LTS</pt_BR>
   <pt>Núcleo (Kernel) do antiX de 64 bits LTS (6.1.55)</pt>
   <ro>antiX Kernel 64 bit  LTS (6.1.55)</ro>
   <ru>antiX Kernel 64 bit  LTS (6.1.55)</ru>
   <sk>antiX Kernel 64 bit  LTS (6.1.55)</sk>
   <sl>antiX 64 bitno jedro (6.1.55)</sl>
   <so>antiX Kernel 64 bit  LTS (6.1.55)</so>
   <sq>Kernel antiX-i LTS, 64 bit (6.1.55)</sq>
   <sr>antiX Kernel 64 bit  LTS (6.1.55)</sr>
   <sv>antiX Kärna 64 bit LTS (6.1.55)</sv>
   <th>antiX Kernel 64 bit  LTS (6.1.55)</th>
   <tr>antiX Kernel 64 bit  LTS (6.1.55)</tr>
   <uk>antiX Kernel 64 bit  LTS (6.1.55)</uk>
   <vi>antiX Kernel 64 bit  LTS (6.1.55)</vi>
   <zh_CN>antiX Kernel 64 bit  LTS (6.1.55)</zh_CN>
   <zh_HK>antiX Kernel 64 bit  LTS (6.1.55)</zh_HK>
   <zh_TW>antiX Kernel 64 bit  LTS (6.1.55)</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
linux-image-6.1.55-antix.1-amd64-smp
linux-headers-6.1.55-antix.1-amd64-smp
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-6.1.55-antix.1-amd64-smp
linux-headers-6.1.55-antix.1-amd64-smp
</uninstall_package_names>

</app>
