<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Palemoon
</name>

<description>
   <am>Palemoon from the mx repo</am>
   <ar>Palemoon from the mx repo</ar>
   <be>Palemoon from the mx repo</be>
   <bg>Palemoon from the mx repo</bg>
   <bn>Palemoon from the mx repo</bn>
   <ca>Palemoon del dipòsit de MX</ca>
   <cs>Palemoon from the mx repo</cs>
   <da>Palemoon mx-softwarekilden</da>
   <de>Pale Moon aus dem MX-Repo</de>
   <el>Palemoon από το mx repo</el>
   <en>Palemoon from the mx repo</en>
   <es_ES>Palemoon desde el repositorio de MX</es_ES>
   <es>Palemoon desde el repositorio de MX</es>
   <et>Palemoon from the mx repo</et>
   <eu>Palemoon from the mx repo</eu>
   <fa>Palemoon from the mx repo</fa>
   <fil_PH>Palemoon from the mx repo</fil_PH>
   <fi>Palemoon-selain MX-ohjelmavarastosta</fi>
   <fr_BE>Palemoon du dépôt MX</fr_BE>
   <fr>Palemoon du dépôt MX</fr>
   <gl_ES>Navegador web Palemoon do repositorio mx repo</gl_ES>
   <gu>Palemoon from the mx repo</gu>
   <he_IL>Palemoon from the mx repo</he_IL>
   <hi>एमएक्स पैकेज-संग्रह से पेलमून</hi>
   <hr>Palemoon from the mx repo</hr>
   <hu>Palemoon from the mx repo</hu>
   <id>Palemoon from the mx repo</id>
   <is>Palemoon from the mx repo</is>
   <it>Palemoon dal repo mx</it>
   <ja>Pale Moon ブラウザー（MX repo より）</ja>
   <kk>Palemoon from the mx repo</kk>
   <ko>Palemoon from the mx repo</ko>
   <ku>Palemoon from the mx repo</ku>
   <lt>Palemoon iš mx saugyklos</lt>
   <mk>Palemoon from the mx repo</mk>
   <mr>Palemoon from the mx repo</mr>
   <nb_NO>Palemoon from the mx repo</nb_NO>
   <nb>Palemoon fra mx-pakkearkiv</nb>
   <nl_BE>Palemoon from the mx repo</nl_BE>
   <nl>Palemoon uit de mx pakketbron</nl>
   <or>Palemoon from the mx repo</or>
   <pl>Palemoon z repozytorium MX</pl>
   <pt_BR>Palemoon - Navegador de internet do repositório do MX</pt_BR>
   <pt>Navegador web Palemoon do repositório mx repo</pt>
   <ro>Palemoon from the mx repo</ro>
   <ru>Браузер Palemoon из репозитория mx</ru>
   <sk>Palemoon from the mx repo</sk>
   <sl>Palemoon iz mx repozitorija</sl>
   <so>Palemoon from the mx repo</so>
   <sq>Palemoon prej depos mx</sq>
   <sr>Palemoon from the mx repo</sr>
   <sv>Palemoon från mx repo</sv>
   <th>Palemoon จาก MX Repo</th>
   <tr>MX Depo'dan Palemoon</tr>
   <uk>Palemoon зі сховища mx</uk>
   <vi>Palemoon from the mx repo</vi>
   <zh_CN>Palemoon from the mx repo</zh_CN>
   <zh_HK>Palemoon from the mx repo</zh_HK>
   <zh_TW>Palemoon from the mx repo</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
palemoon
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
palemoon
</uninstall_package_names>
</app>
