<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Messaging
</category>

<name>
Pidgin
</name>

<description>
   <am>an easy to use and free chat client</am>
   <ar>an easy to use and free chat client</ar>
   <be>an easy to use and free chat client</be>
   <bg>an easy to use and free chat client</bg>
   <bn>an easy to use and free chat client</bn>
   <ca>client de xat lliure i fàcil d'usar</ca>
   <cs>an easy to use and free chat client</cs>
   <da>en letanvendelig og fri chat-klient</da>
   <de>Ein einfach zu bedienender und kostenloser Chat-Client</de>
   <el>Δωρεάν και εύκολο στη χρήση πρόγραμμα συνομιλίας</el>
   <en>an easy to use and free chat client</en>
   <es_ES>Un cliente de chat fácil de usar y gratuito.</es_ES>
   <es>Un cliente de chat fácil de usar y gratuito.</es>
   <et>an easy to use and free chat client</et>
   <eu>an easy to use and free chat client</eu>
   <fa>an easy to use and free chat client</fa>
   <fil_PH>an easy to use and free chat client</fil_PH>
   <fi>ilmainen, vapaa ja helppokäyttöinen chat-asiakasohjelma</fi>
   <fr_BE>Un client de discussion libre et facile d'utilisation</fr_BE>
   <fr>Un client de discussion libre et facile d'utilisation</fr>
   <gl_ES>Cliente de chat libre e fácil de usar</gl_ES>
   <gu>an easy to use and free chat client</gu>
   <he_IL>an easy to use and free chat client</he_IL>
   <hi>उपयोग में सरल व निःशुल्क चैट साधन</hi>
   <hr>an easy to use and free chat client</hr>
   <hu>an easy to use and free chat client</hu>
   <id>an easy to use and free chat client</id>
   <is>an easy to use and free chat client</is>
   <it>client chat libero e di facile uso</it>
   <ja>やさしくて無料のチャットクライアント</ja>
   <kk>an easy to use and free chat client</kk>
   <ko>an easy to use and free chat client</ko>
   <ku>an easy to use and free chat client</ku>
   <lt>an easy to use and free chat client</lt>
   <mk>an easy to use and free chat client</mk>
   <mr>an easy to use and free chat client</mr>
   <nb_NO>an easy to use and free chat client</nb_NO>
   <nb>enkelt lynmeldingsprogram</nb>
   <nl_BE>an easy to use and free chat client</nl_BE>
   <nl>een gebruiksvriendelijk en gratis chatprogramma</nl>
   <or>an easy to use and free chat client</or>
   <pl>łatwy w użyciu i darmowy klient czatu</pl>
   <pt_BR>Cliente de bate-papo (chat) livre e fácil de usar</pt_BR>
   <pt>Cliente de 'chat' livre e fácil de usar</pt>
   <ro>an easy to use and free chat client</ro>
   <ru>Простой в использовании многопротокольный клиент мгновенных сообщений</ru>
   <sk>an easy to use and free chat client</sk>
   <sl>Brezplačen in enostaven odjemalec za klepet</sl>
   <so>an easy to use and free chat client</so>
   <sq>Një klient fjalosjesh i lehtë për t’u përdorur dhe i lirë</sq>
   <sr>an easy to use and free chat client</sr>
   <sv>en lättanvänd och fri chattklient</sv>
   <th>Chat client ที่ใช้งานง่ายและฟรี</th>
   <tr>kullanımı kolay ve ücretsiz bir söyleşi istemcisi</tr>
   <uk>легкий у користуванні та вільний чат клієнт</uk>
   <vi>an easy to use and free chat client</vi>
   <zh_CN>an easy to use and free chat client</zh_CN>
   <zh_HK>an easy to use and free chat client</zh_HK>
   <zh_TW>an easy to use and free chat client</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
pidgin
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
pidgin
</uninstall_package_names>
</app>
