<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
MATE
</name>

<description>
   <am>basic install of MATE desktop</am>
   <ar>basic install of MATE desktop</ar>
   <be>basic install of MATE desktop</be>
   <bg>basic install of MATE desktop</bg>
   <bn>basic install of MATE desktop</bn>
   <ca>Instal·lació bàsica de l'escriptori MATE</ca>
   <cs>basic install of MATE desktop</cs>
   <da>grundlæggende installation af MATE-skrivebordet</da>
   <de>Basisinstallation von MATE Desktop</de>
   <el>Βασική εγκατάσταση της επιφάνειας εργασίας MATE</el>
   <en>basic install of MATE desktop</en>
   <es_ES>Instalación mínima del escritorio MATE</es_ES>
   <es>Instalación mínima del escritorio MATE</es>
   <et>basic install of MATE desktop</et>
   <eu>basic install of MATE desktop</eu>
   <fa>basic install of MATE desktop</fa>
   <fil_PH>basic install of MATE desktop</fil_PH>
   <fi>MATE-työpöydän perusasennus</fi>
   <fr_BE>Installation de base du bureau MATE</fr_BE>
   <fr>Installation de base du bureau MATE</fr>
   <gl_ES>instalación básica do escritorio MATE</gl_ES>
   <gu>basic install of MATE desktop</gu>
   <he_IL>התקנה בסיסית של שולחן העבודה MATE</he_IL>
   <hi>मैटे डेस्कटॉप का सामान्य इंस्टॉल</hi>
   <hr>basic install of MATE desktop</hr>
   <hu>basic install of MATE desktop</hu>
   <id>basic install of MATE desktop</id>
   <is>basic install of MATE desktop</is>
   <it>installazione base del desktop MATE</it>
   <ja>MATE デスクトップの基本インストール</ja>
   <kk>basic install of MATE desktop</kk>
   <ko>basic install of MATE desktop</ko>
   <ku>basic install of MATE desktop</ku>
   <lt>basic install of MATE desktop</lt>
   <mk>basic install of MATE desktop</mk>
   <mr>basic install of MATE desktop</mr>
   <nb_NO>basic install of MATE desktop</nb_NO>
   <nb>grunnleggende installasjon av skrivebordsmiljøet MATE</nb>
   <nl_BE>basic install of MATE desktop</nl_BE>
   <nl>basisinstallatie van de MATE desktop</nl>
   <or>basic install of MATE desktop</or>
   <pl>podstawowa instalacja środowiska MATE</pl>
   <pt_BR>Instalação Básica do Ambiente de Trabalho MATE</pt_BR>
   <pt>Instalação básica do Ambiente de Trabalho MATE</pt>
   <ro>basic install of MATE desktop</ro>
   <ru>Базовая установка окружения MATE</ru>
   <sk>basic install of MATE desktop</sk>
   <sl>Osnovna namestitev MATE namizja</sl>
   <so>basic install of MATE desktop</so>
   <sq>Instalim elementar i desktopit MATE</sq>
   <sr>basic install of MATE desktop</sr>
   <sv>enkel installation av MATE skrivbord</sv>
   <th>การติดตั้งพื้นฐานของ MATE</th>
   <tr>MATE masaüstü temel kurulumu</tr>
   <uk>базовий набір стільниці MATE</uk>
   <vi>basic install of MATE desktop</vi>
   <zh_CN>basic install of MATE desktop</zh_CN>
   <zh_HK>basic install of MATE desktop</zh_HK>
   <zh_TW>basic install of MATE desktop</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
mate-core
mate-desktop-environment
gvfs
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
mate-core
mate-desktop-environment
</uninstall_package_names>
</app>
