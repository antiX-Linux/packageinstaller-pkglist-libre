<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Aragonese_Default_Firefox_esr
</name>

<description>
   <am>Aragonese localisation of Firefox-ESR</am>
   <ar>Aragonese localisation of Firefox-ESR</ar>
   <be>Aragonese localisation of Firefox-ESR</be>
   <bg>Aragonese localisation of Firefox-ESR</bg>
   <bn>Aragonese localisation of Firefox-ESR</bn>
   <ca>Localització de Firefox-ESR en Aragonès</ca>
   <cs>Aragonese localisation of Firefox-ESR</cs>
   <da>Aragonese localisation of Firefox-ESR</da>
   <de>Aragonesische Lokalisation des als Standard installierten “Firefox ESR”</de>
   <el>Αραγονικά για το Firefox-ESR</el>
   <en>Aragonese localisation of Firefox-ESR</en>
   <es_ES>Localización Aragonés de Firefox-ESR</es_ES>
   <es>Localización Aragonés de Firefox ESR</es>
   <et>Aragonese localisation of Firefox-ESR</et>
   <eu>Aragonese localisation of Firefox-ESR</eu>
   <fa>Aragonese localisation of Firefox-ESR</fa>
   <fil_PH>Aragonese localisation of Firefox-ESR</fil_PH>
   <fi>Aragonese localisation of Firefox-ESR</fi>
   <fr_BE>Localisation en aragonais pour Firefox ESR</fr_BE>
   <fr>Localisation en aragonais pour Firefox ESR</fr>
   <gl_ES>Aragonese localisation of Firefox-ESR</gl_ES>
   <gu>Aragonese localisation of Firefox-ESR</gu>
   <he_IL>Aragonese localisation of Firefox-ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का आरागोन संस्करण</hi>
   <hr>Aragonese localisation of Firefox-ESR</hr>
   <hu>Aragonese localisation of Firefox-ESR</hu>
   <id>Aragonese localisation of Firefox-ESR</id>
   <is>Aragonese localisation of Firefox-ESR</is>
   <it>Localizzazione aragonese di Firefox ESR</it>
   <ja>アラゴン語版 Firefox-ESR</ja>
   <kk>Aragonese localisation of Firefox-ESR</kk>
   <ko>Aragonese localisation of Firefox-ESR</ko>
   <ku>Aragonese localisation of Firefox-ESR</ku>
   <lt>Aragonese localisation of Firefox-ESR</lt>
   <mk>Aragonese localisation of Firefox-ESR</mk>
   <mr>Aragonese localisation of Firefox-ESR</mr>
   <nb_NO>Aragonese localisation of Firefox-ESR</nb_NO>
   <nb>Aragonesisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Aragonese localisation of Firefox-ESR</nl_BE>
   <nl>Aragonese lokalisatie van Firefox-ESR</nl>
   <or>Aragonese localisation of Firefox-ESR</or>
   <pl>Aragonese localisation of Firefox-ESR</pl>
   <pt_BR>Aragonês Localização para o Firefox ESR</pt_BR>
   <pt>Aragonês Localização para Firefox ESR</pt>
   <ro>Aragonese localisation of Firefox-ESR</ro>
   <ru>Aragonese localisation of Firefox-ESR</ru>
   <sk>Aragonese localisation of Firefox-ESR</sk>
   <sl>Aragonske krajevne nastavitve za Firefox-ESR</sl>
   <so>Aragonese localisation of Firefox-ESR</so>
   <sq>Përkthimi i Firefox-it ESR aragonisht</sq>
   <sr>Aragonese localisation of Firefox-ESR</sr>
   <sv>Aragonsk lokalisering av Firefox-ESR</sv>
   <th>Aragonese localisation of Firefox-ESR</th>
   <tr>Firefox-ESR Aragonca yerelleştirmesi</tr>
   <uk>Aragonese localisation of Firefox-ESR</uk>
   <vi>Aragonese localisation of Firefox-ESR</vi>
   <zh_CN>Aragonese localisation of Firefox-ESR</zh_CN>
   <zh_HK>Aragonese localisation of Firefox-ESR</zh_HK>
   <zh_TW>Aragonese localisation of Firefox-ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-an
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-an
</uninstall_package_names>

</app>
