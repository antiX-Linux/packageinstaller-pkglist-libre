<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
LaTeX
</category>

<name>
LaTeX-Basic
</name>

<description>
   <am>Basic LaTeX installation</am>
   <ar>Basic LaTeX installation</ar>
   <be>Basic LaTeX installation</be>
   <bg>Basic LaTeX installation</bg>
   <bn>Basic LaTeX installation</bn>
   <ca>Instal·lació bàsica de LaTeX</ca>
   <cs>Basic LaTeX installation</cs>
   <da>Basic LaTeX installation</da>
   <de>Basisinstallation des Textsatz-Softwarepaketes “LaTaX”</de>
   <el>Βασική εγκατάσταση LaTeX</el>
   <en>Basic LaTeX installation</en>
   <es_ES>Instalación básica de LaTeX</es_ES>
   <es>Instalación básica de LaTeX</es>
   <et>Basic LaTeX installation</et>
   <eu>Basic LaTeX installation</eu>
   <fa>Basic LaTeX installation</fa>
   <fil_PH>Basic LaTeX installation</fil_PH>
   <fi>Perusmuotoinen LaTeX-asennus</fi>
   <fr_BE>Installation basique de LaTeX</fr_BE>
   <fr>Installation basique de LaTeX</fr>
   <gl_ES>Basic LaTeX installation</gl_ES>
   <gu>Basic LaTeX installation</gu>
   <he_IL>Basic LaTeX installation</he_IL>
   <hi>सामान्य LaTeX इंस्टॉल</hi>
   <hr>Basic LaTeX installation</hr>
   <hu>Basic LaTeX installation</hu>
   <id>Basic LaTeX installation</id>
   <is>Basic LaTeX installation</is>
   <it>Installazione base di LaTeX</it>
   <ja>LaTeX の基本インストール</ja>
   <kk>Basic LaTeX installation</kk>
   <ko>Basic LaTeX installation</ko>
   <ku>Basic LaTeX installation</ku>
   <lt>Basic LaTeX installation</lt>
   <mk>Basic LaTeX installation</mk>
   <mr>Basic LaTeX installation</mr>
   <nb_NO>Basic LaTeX installation</nb_NO>
   <nb>Grunnleggende LaTeX-installasjon</nb>
   <nl_BE>Basic LaTeX installation</nl_BE>
   <nl>Basic LaTeX installation</nl>
   <or>Basic LaTeX installation</or>
   <pl>Basic LaTeX installation</pl>
   <pt_BR>LaTeX - Instalação Básica</pt_BR>
   <pt>Instalação básica do LaTeX</pt>
   <ro>Basic LaTeX installation</ro>
   <ru>Basic LaTeX installation</ru>
   <sk>Basic LaTeX installation</sk>
   <sl>Osnovna LaTeX namestitev</sl>
   <so>Basic LaTeX installation</so>
   <sq>Instalim elementar LaTeX-i</sq>
   <sr>Basic LaTeX installation</sr>
   <sv>Basisk LaTeX installation</sv>
   <th>Basic LaTeX installation</th>
   <tr>Temel LaTeX kurulumu</tr>
   <uk>Basic LaTeX installation</uk>
   <vi>Basic LaTeX installation</vi>
   <zh_CN>Basic LaTeX installation</zh_CN>
   <zh_HK>Basic LaTeX installation</zh_HK>
   <zh_TW>Basic LaTeX installation</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
texlive
texlive-base
texlive-latex-base
texlive-latex-recommended
texlive-fonts-recommended
preview-latex-style
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
texlive
texlive-base
texlive-latex-base
texlive-latex-recommended
texlive-fonts-recommended
preview-latex-style
</uninstall_package_names>

</app>
