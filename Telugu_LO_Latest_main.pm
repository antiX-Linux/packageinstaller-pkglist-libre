<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Telugu_LO_Latest_main
</name>

<description>
   <am>Telugu Language Meta-Package for LibreOffice</am>
   <ar>Telugu Language Meta-Package for LibreOffice</ar>
   <be>Telugu Language Meta-Package for LibreOffice</be>
   <bg>Telugu Language Meta-Package for LibreOffice</bg>
   <bn>Telugu Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Telugu per LibreOffice</ca>
   <cs>Telugu Language Meta-Package for LibreOffice</cs>
   <da>Telugu Language Meta-Package for LibreOffice</da>
   <de>Telugu Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Telugu</el>
   <en>Telugu Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Telugu para LibreOffice</es_ES>
   <es>Metapaquete de idioma Telugu para LibreOffice</es>
   <et>Telugu Language Meta-Package for LibreOffice</et>
   <eu>Telugu Language Meta-Package for LibreOffice</eu>
   <fa>Telugu Language Meta-Package for LibreOffice</fa>
   <fil_PH>Telugu Language Meta-Package for LibreOffice</fil_PH>
   <fi>Telugunkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de langue Telougou pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de langue Telougou pour LibreOffice</fr>
   <gl_ES>Telego Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Telugu Language Meta-Package for LibreOffice</gu>
   <he_IL>Telugu Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु तेलुगू भाषा मेटा-पैकेज</hi>
   <hr>Telugu Language Meta-Package for LibreOffice</hr>
   <hu>Telugu Language Meta-Package for LibreOffice</hu>
   <id>Telugu Language Meta-Package for LibreOffice</id>
   <is>Telugu Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua telugu per LibreOffice</it>
   <ja>LibreOffice用のテルグ語メタパッケージ</ja>
   <kk>Telugu Language Meta-Package for LibreOffice</kk>
   <ko>Telugu Language Meta-Package for LibreOffice</ko>
   <ku>Telugu Language Meta-Package for LibreOffice</ku>
   <lt>Telugu Language Meta-Package for LibreOffice</lt>
   <mk>Telugu Language Meta-Package for LibreOffice</mk>
   <mr>Telugu Language Meta-Package for LibreOffice</mr>
   <nb_NO>Telugu Language Meta-Package for LibreOffice</nb_NO>
   <nb>Telugu språkpakke for LibreOffice</nb>
   <nl_BE>Telugu Language Meta-Package for LibreOffice</nl_BE>
   <nl>Telugu Taal Meta-Pakket voor LibreOffice</nl>
   <or>Telugu Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Telugu dla LibreOffice</pl>
   <pt_BR>Telugo Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Telego Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Telugu Language Meta-Package for LibreOffice</ro>
   <ru>Telugu Language Meta-Package for LibreOffice</ru>
   <sk>Telugu Language Meta-Package for LibreOffice</sk>
   <sl>Teluški jezikovni meta-paket za LibreOffice</sl>
   <so>Telugu Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në telugu</sq>
   <sr>Telugu Language Meta-Package for LibreOffice</sr>
   <sv>Telugu Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Telugu สำหรับ LibreOffice</th>
   <tr>LibreOffice için Telugu Dili Üst-Paketi</tr>
   <uk>Telugu Language Meta-Package for LibreOffice</uk>
   <vi>Telugu Language Meta-Package for LibreOffice</vi>
   <zh_CN>Telugu Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Telugu Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Telugu Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-te
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-te
libreoffice-gtk3
</uninstall_package_names>

</app>
