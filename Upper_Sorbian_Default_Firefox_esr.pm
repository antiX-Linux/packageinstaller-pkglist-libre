<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Upper_Sorbian_Default_Firefox_esr
</name>

<description>
   <am>Upper Sorbian localisation of Firefox ESR</am>
   <ar>Upper Sorbian localisation of Firefox ESR</ar>
   <be>Upper Sorbian localisation of Firefox ESR</be>
   <bg>Upper Sorbian localisation of Firefox ESR</bg>
   <bn>Upper Sorbian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Alt-Serbi</ca>
   <cs>Upper Sorbian localisation of Firefox ESR</cs>
   <da>Upper Sorbian localisation of Firefox ESR</da>
   <de>Deutsche Lokalisation für die obersorbische Sprache von Firefox “ESR”</de>
   <el>Upper Sorbian για Firefox ESR</el>
   <en>Upper Sorbian localisation of Firefox ESR</en>
   <es_ES>Localización Sorbio superior de Firefox ESR</es_ES>
   <es>Localización Sorbio superior de Firefox ESR</es>
   <et>Upper Sorbian localisation of Firefox ESR</et>
   <eu>Upper Sorbian localisation of Firefox ESR</eu>
   <fa>Upper Sorbian localisation of Firefox ESR</fa>
   <fil_PH>Upper Sorbian localisation of Firefox ESR</fil_PH>
   <fi>Upper Sorbian localisation of Firefox ESR</fi>
   <fr_BE>Localisation de l'allemand en haut sorabe pour Firefox ESR</fr_BE>
   <fr>Localisation de l'allemand en haut sorabe pour Firefox ESR</fr>
   <gl_ES>Upper Sorbian localisation of Firefox ESR</gl_ES>
   <gu>Upper Sorbian localisation of Firefox ESR</gu>
   <he_IL>Upper Sorbian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का ऊपरी सोर्बियाई संस्करण</hi>
   <hr>Upper Sorbian localisation of Firefox ESR</hr>
   <hu>Upper Sorbian localisation of Firefox ESR</hu>
   <id>Upper Sorbian localisation of Firefox ESR</id>
   <is>Upper Sorbian localisation of Firefox ESR</is>
   <it>Localizzazione upper sorbian di Firefox ESR</it>
   <ja>高地ソルブ語版 Firefox ESR</ja>
   <kk>Upper Sorbian localisation of Firefox ESR</kk>
   <ko>Upper Sorbian localisation of Firefox ESR</ko>
   <ku>Upper Sorbian localisation of Firefox ESR</ku>
   <lt>Upper Sorbian localisation of Firefox ESR</lt>
   <mk>Upper Sorbian localisation of Firefox ESR</mk>
   <mr>Upper Sorbian localisation of Firefox ESR</mr>
   <nb_NO>Upper Sorbian localisation of Firefox ESR</nb_NO>
   <nb>Øvre sorbisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Upper Sorbian localisation of Firefox ESR</nl_BE>
   <nl>Upper Sorbian localisation of Firefox ESR</nl>
   <or>Upper Sorbian localisation of Firefox ESR</or>
   <pl>Upper Sorbian localisation of Firefox ESR</pl>
   <pt_BR>Alto Sorábio Localização para o Firefox ESR</pt_BR>
   <pt>Alto Sórbio Localização para Firefox ESR</pt>
   <ro>Upper Sorbian localisation of Firefox ESR</ro>
   <ru>Upper Sorbian localisation of Firefox ESR</ru>
   <sk>Upper Sorbian localisation of Firefox ESR</sk>
   <sl>Gornje lužiško srbske krajevne nastavitve za Firefox ESR</sl>
   <so>Upper Sorbian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në uzbeke</sq>
   <sr>Upper Sorbian localisation of Firefox ESR</sr>
   <sv>Övre Sorbisk lokalisering av Firefox ESR</sv>
   <th>Upper Sorbian localisation of Firefox ESR</th>
   <tr>Firefox ESR Yüksek Sırpça yerelleştirmesi</tr>
   <uk>Upper Sorbian localisation of Firefox ESR</uk>
   <vi>Upper Sorbian localisation of Firefox ESR</vi>
   <zh_CN>Upper Sorbian localisation of Firefox ESR</zh_CN>
   <zh_HK>Upper Sorbian localisation of Firefox ESR</zh_HK>
   <zh_TW>Upper Sorbian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-esr-l10n-hsb
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-hsb
</uninstall_package_names>

</app>
