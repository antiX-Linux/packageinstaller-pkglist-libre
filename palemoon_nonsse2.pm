<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Palemoon - non sse2 version - 32 bit only
</name>

<description>
   <am>Palemoon lightweight browser for PIV, PIII, PII</am>
   <ar>Palemoon lightweight browser for PIV, PIII, PII</ar>
   <be>Palemoon lightweight browser for PIV, PIII, PII</be>
   <bg>Palemoon lightweight browser for PIV, PIII, PII</bg>
   <bn>Palemoon lightweight browser for PIV, PIII, PII</bn>
   <ca>Navegador lleuger Palemoon per PIV, PIII, PII</ca>
   <cs>Palemoon lightweight browser for PIV, PIII, PII</cs>
   <da>Palemoon lightweight browser for PIV, PIII, PII</da>
   <de>Palemoon, ein Browser in Leichtbauweise, geeignet für die Computer der Prozessorgeneration PIV, PIII, und PII.</de>
   <el>Ελαφρύ πρόγραμμα περιήγησης Palemoon για PIV, PIII, PII</el>
   <en>Palemoon lightweight browser for PIV, PIII, PII</en>
   <es_ES>Navegador ligero Palemoon para PIV, PIII, PII</es_ES>
   <es>Navegador ligero Palemoon para PIV, PIII, PII</es>
   <et>Palemoon lightweight browser for PIV, PIII, PII</et>
   <eu>Palemoon lightweight browser for PIV, PIII, PII</eu>
   <fa>Palemoon lightweight browser for PIV, PIII, PII</fa>
   <fil_PH>Palemoon lightweight browser for PIV, PIII, PII</fil_PH>
   <fi>Palemoon lightweight browser for PIV, PIII, PII</fi>
   <fr_BE>Palemoon navigateur léger pour PIV, PIII, PII</fr_BE>
   <fr>Palemoon navigateur léger pour PIV, PIII, PII</fr>
   <gl_ES>Palemoon lightweight browser for PIV, PIII, PII</gl_ES>
   <gu>Palemoon lightweight browser for PIV, PIII, PII</gu>
   <he_IL>Palemoon lightweight browser for PIV, PIII, PII</he_IL>
   <hi>PIV, PIII, PII हेतु पेलमून सरल ब्राउज़र</hi>
   <hr>Palemoon lightweight browser for PIV, PIII, PII</hr>
   <hu>Palemoon lightweight browser for PIV, PIII, PII</hu>
   <id>Palemoon lightweight browser for PIV, PIII, PII</id>
   <is>Palemoon lightweight browser for PIV, PIII, PII</is>
   <it>Palemoon, un browser leggero per PIV, PIII, PII</it>
   <ja>PIV、PIII、PII用の軽量ブラウザ Pale Moon</ja>
   <kk>Palemoon lightweight browser for PIV, PIII, PII</kk>
   <ko>Palemoon lightweight browser for PIV, PIII, PII</ko>
   <ku>Palemoon lightweight browser for PIV, PIII, PII</ku>
   <lt>Palemoon lightweight browser for PIV, PIII, PII</lt>
   <mk>Palemoon lightweight browser for PIV, PIII, PII</mk>
   <mr>Palemoon lightweight browser for PIV, PIII, PII</mr>
   <nb_NO>Palemoon lightweight browser for PIV, PIII, PII</nb_NO>
   <nb>Palemoon, lettvektig nettleser for Pentium IV, III og II</nb>
   <nl_BE>Palemoon lightweight browser for PIV, PIII, PII</nl_BE>
   <nl>Palemoon lightweight browser for PIV, PIII, PII</nl>
   <or>Palemoon lightweight browser for PIV, PIII, PII</or>
   <pl>Palemoon lightweight browser for PIV, PIII, PII</pl>
   <pt_BR>Palemoon - Navegador de internet leve para PII, PIII, PIV</pt_BR>
   <pt>Navegador ligeiro Palemoon para PIV. PIII. PII</pt>
   <ro>Palemoon lightweight browser for PIV, PIII, PII</ro>
   <ru>Palemoon lightweight browser for PIV, PIII, PII</ru>
   <sk>Palemoon lightweight browser for PIV, PIII, PII</sk>
   <sl>Pelemoon lahek brskalnik za PIV, PIII, PII</sl>
   <so>Palemoon lightweight browser for PIV, PIII, PII</so>
   <sq>Shfletuesi Palemoon i peshës së lehtë për PIV, PIII, PII</sq>
   <sr>Palemoon lightweight browser for PIV, PIII, PII</sr>
   <sv>Palemoon lättviktswebäsare för PIV, PIII, PII</sv>
   <th>Palemoon lightweight browser for PIV, PIII, PII</th>
   <tr>PII, PIII, ve PIV için Palemoon hafif tarayıcı</tr>
   <uk>Palemoon lightweight browser for PIV, PIII, PII</uk>
   <vi>Palemoon lightweight browser for PIV, PIII, PII</vi>
   <zh_CN>Palemoon lightweight browser for PIV, PIII, PII</zh_CN>
   <zh_HK>Palemoon lightweight browser for PIV, PIII, PII</zh_HK>
   <zh_TW>Palemoon lightweight browser for PIV, PIII, PII</zh_TW>
</description>

<installable>
32
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
palemoon-nonsse2
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
palemoon-nonsse2
</uninstall_package_names>
</app>
