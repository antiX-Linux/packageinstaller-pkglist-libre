<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Esperanto_Default_Firefox_esr
</name>

<description>
   <am>Esperanto localisation of Firefox ESR</am>
   <ar>Esperanto localisation of Firefox ESR</ar>
   <be>Esperanto localisation of Firefox ESR</be>
   <bg>Esperanto localisation of Firefox ESR</bg>
   <bn>Esperanto localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Esperanto</ca>
   <cs>Esperanto localisation of Firefox ESR</cs>
   <da>Esperanto localisation of Firefox ESR</da>
   <de>Lokalisation für “Esperanto” von “Firefox ESR”</de>
   <el>Esperanto για Firefox ESR</el>
   <en>Esperanto localisation of Firefox ESR</en>
   <es_ES>Localización Esperanto de Firefox ESR</es_ES>
   <es>Localización Esperanto de Firefox ESR</es>
   <et>Esperanto localisation of Firefox ESR</et>
   <eu>Esperanto localisation of Firefox ESR</eu>
   <fa>Esperanto localisation of Firefox ESR</fa>
   <fil_PH>Esperanto localisation of Firefox ESR</fil_PH>
   <fi>Esperanto localisation of Firefox ESR</fi>
   <fr_BE>Localisation en espéranto pour Firefox ESR</fr_BE>
   <fr>Localisation en espéranto pour Firefox ESR</fr>
   <gl_ES>Esperanto localisation of Firefox ESR</gl_ES>
   <gu>Esperanto localisation of Firefox ESR</gu>
   <he_IL>Esperanto localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का एस्पेरांतो संस्करण</hi>
   <hr>Esperanto localisation of Firefox ESR</hr>
   <hu>Esperanto localisation of Firefox ESR</hu>
   <id>Esperanto localisation of Firefox ESR</id>
   <is>Esperanto localisation of Firefox ESR</is>
   <it>Localizzazione in esperanto di Firefox ESR</it>
   <ja>エスペラント語版 Firefox ESR</ja>
   <kk>Esperanto localisation of Firefox ESR</kk>
   <ko>Esperanto localisation of Firefox ESR</ko>
   <ku>Esperanto localisation of Firefox ESR</ku>
   <lt>Esperanto localisation of Firefox ESR</lt>
   <mk>Esperanto localisation of Firefox ESR</mk>
   <mr>Esperanto localisation of Firefox ESR</mr>
   <nb_NO>Esperanto localisation of Firefox ESR</nb_NO>
   <nb>Esperanto lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Esperanto localisation of Firefox ESR</nl_BE>
   <nl>Esperanto lokalisatie van Firefox ESR</nl>
   <or>Esperanto localisation of Firefox ESR</or>
   <pl>Esperanto localisation of Firefox ESR</pl>
   <pt_BR>Esperanto Localização para o Firefox ESR</pt_BR>
   <pt>Esperanto Localização para Firefox ESR</pt>
   <ro>Esperanto localisation of Firefox ESR</ro>
   <ru>Esperanto localisation of Firefox ESR</ru>
   <sk>Esperanto localisation of Firefox ESR</sk>
   <sl>Esperanto krajevne nastavitve za Firefox ESR</sl>
   <so>Esperanto localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në esperanto</sq>
   <sr>Esperanto localisation of Firefox ESR</sr>
   <sv>Esperanto lokalisering av Firefox ESR</sv>
   <th>Esperanto localisation of Firefox ESR</th>
   <tr>Firefox ESR Esperanto yerelleştirmesi</tr>
   <uk>Esperanto localisation of Firefox ESR</uk>
   <vi>Esperanto localisation of Firefox ESR</vi>
   <zh_CN>Esperanto localisation of Firefox ESR</zh_CN>
   <zh_HK>Esperanto localisation of Firefox ESR</zh_HK>
   <zh_TW>Esperanto localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-eo
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-eo
</uninstall_package_names>

</app>
