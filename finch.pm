<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Messaging
</category>

<name>
Finch
</name>

<description>
   <am>a text-console-based, modular instant messaging client</am>
   <ar>a text-console-based, modular instant messaging client</ar>
   <be>a text-console-based, modular instant messaging client</be>
   <bg>a text-console-based, modular instant messaging client</bg>
   <bn>a text-console-based, modular instant messaging client</bn>
   <ca>un client de missatgeria instantània basat en terminal</ca>
   <cs>a text-console-based, modular instant messaging client</cs>
   <da>En tekstkonsol-baseret, modulær strakssamtale-klient</da>
   <de>Ein modularer Instant Messaging-Client in der Textconsole</de>
   <el>Πρόγραμμα άμεσων μηνυμάτων σε κονσόλα κειμένου</el>
   <en>a text-console-based, modular instant messaging client</en>
   <es_ES>Cliente de mensajería instantánea modular para línea de comandos</es_ES>
   <es>Cliente de mensajería instantánea modular para línea de comandos</es>
   <et>a text-console-based, modular instant messaging client</et>
   <eu>a text-console-based, modular instant messaging client</eu>
   <fa>a text-console-based, modular instant messaging client</fa>
   <fil_PH>a text-console-based, modular instant messaging client</fil_PH>
   <fi>tekstipäätepohjainen, modulaarinen pikaviestitin</fi>
   <fr_BE>Un client console-texte modulaire de messagerie instantanée</fr_BE>
   <fr>Un client console-texte modulaire de messagerie instantanée</fr>
   <gl_ES>Cliente de mensaxes instantáneas modular, por consola</gl_ES>
   <gu>a text-console-based, modular instant messaging client</gu>
   <he_IL>a text-console-based, modular instant messaging client</he_IL>
   <hi>टेक्स्ट-कंसोल-आधारित, अनुखण्डीय त्वरित संदेश साधन</hi>
   <hr>a text-console-based, modular instant messaging client</hr>
   <hu>a text-console-based, modular instant messaging client</hu>
   <id>a text-console-based, modular instant messaging client</id>
   <is>a text-console-based, modular instant messaging client</is>
   <it>Client di messaggistica istantanea modulare con interfaccia basata sul terminale</it>
   <ja>テキストコンソールベースのモジュール式インスタントメッセージ・クライアント</ja>
   <kk>a text-console-based, modular instant messaging client</kk>
   <ko>a text-console-based, modular instant messaging client</ko>
   <ku>a text-console-based, modular instant messaging client</ku>
   <lt>a text-console-based, modular instant messaging client</lt>
   <mk>a text-console-based, modular instant messaging client</mk>
   <mr>a text-console-based, modular instant messaging client</mr>
   <nb_NO>a text-console-based, modular instant messaging client</nb_NO>
   <nb>modulær lynmeldingsklient for terminal</nb>
   <nl_BE>a text-console-based, modular instant messaging client</nl_BE>
   <nl>een modulaire client voor instant messaging op basis van tekstconsoles</nl>
   <or>a text-console-based, modular instant messaging client</or>
   <pl>oparty na konsoli tekstowej, modułowy klient wiadomości błyskawicznych (komunikator)</pl>
   <pt_BR>Cliente de mensagens instantâneas modular baseado em console de texto console/terminal</pt_BR>
   <pt>Cliente de mensagens instantâneas modular, por consola/terminal</pt>
   <ro>a text-console-based, modular instant messaging client</ro>
   <ru>Консольный модульный клиент мгновенных сообщений</ru>
   <sk>a text-console-based, modular instant messaging client</sk>
   <sl>Na tekstovni konzoli temelječ, modularni odjemalec za instantno sporočanje</sl>
   <so>a text-console-based, modular instant messaging client</so>
   <sq>Një klient mesazhesh të atypëratyshme bazuar në konsolë teksti, modular</sq>
   <sr>a text-console-based, modular instant messaging client</sr>
   <sv>en textkonsol-baserad modulär snabbmeddelande-klient</sv>
   <th>Instant messaging client แบบ text-console-based</th>
   <tr>uçbirim metin tabanlı, birimsel bir anlık iletişim istemcisi</tr>
   <uk>консольний модульний клієнт миттєвих повідомлень</uk>
   <vi>a text-console-based, modular instant messaging client</vi>
   <zh_CN>基于文本控制台的模块化即时通讯客户端</zh_CN>
   <zh_HK>a text-console-based, modular instant messaging client</zh_HK>
   <zh_TW>a text-console-based, modular instant messaging client</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
finch
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
finch
</uninstall_package_names>
</app>
