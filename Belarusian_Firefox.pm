<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Belarusian_Firefox
</name>

<description>
   <am>Belarusian localisation of Firefox</am>
   <ar>Belarusian localisation of Firefox</ar>
   <be>Belarusian localisation of Firefox</be>
   <bg>Belarusian localisation of Firefox</bg>
   <bn>Belarusian localisation of Firefox</bn>
   <ca>Localització de Firefox en Bielorús</ca>
   <cs>Belarusian localisation of Firefox</cs>
   <da>Hviderussisk oversættelse af Firefox</da>
   <de>Weißrussische Lokalisierung von Firefox</de>
   <el>Λευκορώσικα για το Firefox</el>
   <en>Belarusian localisation of Firefox</en>
   <es_ES>Localización Bielorrusa de Firefox</es_ES>
   <es>Localización Bielorruso de Firefox</es>
   <et>Belarusian localisation of Firefox</et>
   <eu>Belarusian localisation of Firefox</eu>
   <fa>Belarusian localisation of Firefox</fa>
   <fil_PH>Belarusian localisation of Firefox</fil_PH>
   <fi>Valko-venäläinen Firefox-kotoistus</fi>
   <fr_BE>Localisation en biélorusse pour Firefox</fr_BE>
   <fr>Localisation en biélorusse pour Firefox</fr>
   <gl_ES>Localización de Firefox en beloruso</gl_ES>
   <gu>Belarusian localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לבלארוסית</he_IL>
   <hi>फायरफॉक्स का बेलारूसी संस्करण</hi>
   <hr>Bjeloruska lokalizacija Firefoxa</hr>
   <hu>Belarusian localisation of Firefox</hu>
   <id>Belarusian localisation of Firefox</id>
   <is>Belarusian localisation of Firefox</is>
   <it>Localizzazione bielorussa di Firefox</it>
   <ja>ベラルーシ語版 Firefox</ja>
   <kk>Belarusian localisation of Firefox</kk>
   <ko>Belarusian localisation of Firefox</ko>
   <ku>Belarusian localisation of Firefox</ku>
   <lt>Belarusian localisation of Firefox</lt>
   <mk>Belarusian localisation of Firefox</mk>
   <mr>Belarusian localisation of Firefox</mr>
   <nb_NO>Belarusian localisation of Firefox</nb_NO>
   <nb>Hviterussisk lokaltilpassing av Firefox</nb>
   <nl_BE>Belarusian localisation of Firefox</nl_BE>
   <nl>Wit-Russische lokalisatie van Firefox</nl>
   <or>Belarusian localisation of Firefox</or>
   <pl>Białoruska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Bielorrusso Localização para o Firefox</pt_BR>
   <pt>Bielorrusso Localização para Firefox</pt>
   <ro>Belarusian localisation of Firefox</ro>
   <ru>Белорусская локализация Firefox</ru>
   <sk>Bieloruská lokalizácia pre Firefox</sk>
   <sl>Belarusian localisation of Firefox</sl>
   <so>Belarusian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në bjellorusisht</sq>
   <sr>Belarusian localisation of Firefox</sr>
   <sv>Belarus lokalisering av Firefox</sv>
   <th>Belarusian localisation ของ Firefox</th>
   <tr>Firefox Beyaz Rusça yerelleştirmesi</tr>
   <uk>Belarusian локалізація Firefox</uk>
   <vi>Belarusian localisation of Firefox</vi>
   <zh_CN>Firefox 白俄罗斯语语言包</zh_CN>
   <zh_HK>Belarusian localisation of Firefox</zh_HK>
   <zh_TW>Belarusian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-be
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-be
</uninstall_package_names>
</app>
