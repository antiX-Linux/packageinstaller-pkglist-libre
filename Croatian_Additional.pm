<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Croatian_Additional
</name>

<description>
   <am>Additional Croatian Meta-Package</am>
   <ar>Additional Croatian Meta-Package</ar>
   <be>Additional Croatian Meta-Package</be>
   <bg>Additional Croatian Meta-Package</bg>
   <bn>Additional Croatian Meta-Package</bn>
   <ca>Meta-paquet addicional en Croata</ca>
   <cs>Additional Croatian Meta-Package</cs>
   <da>Yderlige kroatisk metapakke</da>
   <de>Zusätzliches kroatisches Sprach-Meta-Paket</de>
   <el>Επιπλέον Κροατική γλώσσα</el>
   <en>Additional Croatian Meta-Package</en>
   <es_ES>Meta paquete adicional Croata</es_ES>
   <es>Metapaquete Croata adicional</es>
   <et>Additional Croatian Meta-Package</et>
   <eu>Additional Croatian Meta-Package</eu>
   <fa>Additional Croatian Meta-Package</fa>
   <fil_PH>Additional Croatian Meta-Package</fil_PH>
   <fi>Lisäyksellinen metatieto-paketti kroatiankielelle</fi>
   <fr_BE>Méta-Paquet additionnel croate</fr_BE>
   <fr>Méta-Paquet additionnel croate</fr>
   <gl_ES>Croata meta-paquete adicional</gl_ES>
   <gu>Additional Croatian Meta-Package</gu>
   <he_IL>Additional Croatian Meta-Package</he_IL>
   <hi>अतिरिक्त क्रोशियाई भाषा मेटा-पैकेज</hi>
   <hr>Additional Croatian Meta-Package</hr>
   <hu>Additional Croatian Meta-Package</hu>
   <id>Additional Croatian Meta-Package</id>
   <is>Additional Croatian Meta-Package</is>
   <it>Meta-pacchetto aggiuntivo della lingua croata</it>
   <ja>クロアチア語の追加メタパッケージ</ja>
   <kk>Additional Croatian Meta-Package</kk>
   <ko>Additional Croatian Meta-Package</ko>
   <ku>Additional Croatian Meta-Package</ku>
   <lt>Papildomas kroatų metapaketas</lt>
   <mk>Additional Croatian Meta-Package</mk>
   <mr>Additional Croatian Meta-Package</mr>
   <nb_NO>Additional Croatian Meta-Package</nb_NO>
   <nb>Mere kroatisk, metapakke</nb>
   <nl_BE>Additional Croatian Meta-Package</nl_BE>
   <nl>Toegevoegd Kroatisch Meta-Pakket</nl>
   <or>Additional Croatian Meta-Package</or>
   <pl>Chorwacki dodatkowy metapakiet</pl>
   <pt_BR>Croata Meta-Pacote Adicional</pt_BR>
   <pt>Croata Meta-Pacote Adicional</pt>
   <ro>Additional Croatian Meta-Package</ro>
   <ru>Additional Croatian Meta-Package</ru>
   <sk>Additional Croatian Meta-Package</sk>
   <sl>Dodatni hrvaški metapaketi</sl>
   <so>Additional Croatian Meta-Package</so>
   <sq>Meta-Paketë shtesë për kroatishten</sq>
   <sr>Additional Croatian Meta-Package</sr>
   <sv>Kroatiskt Tillägg Meta-Paket</sv>
   <th>Meta-Package ภาษา Croatian เพิ่มเติม</th>
   <tr>Fazladan Hırvatça Üst-Paketi</tr>
   <uk>Додатковий мета-пакунок з мовою Croatian</uk>
   <vi>Additional Croatian Meta-Package</vi>
   <zh_CN>额外的克罗地亚语的元软件包</zh_CN>
   <zh_HK>Additional Croatian Meta-Package</zh_HK>
   <zh_TW>Additional Croatian Meta-Package</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
language-env
maint-guide
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
language-env
maint-guide
</uninstall_package_names>

</app>
