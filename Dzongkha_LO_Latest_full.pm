<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Dzongkha_LO_Latest_full
</name>

<description>
   <am>Dzongkha Language Meta-Package for LibreOffice</am>
   <ar>Dzongkha Language Meta-Package for LibreOffice</ar>
   <be>Dzongkha Language Meta-Package for LibreOffice</be>
   <bg>Dzongkha Language Meta-Package for LibreOffice</bg>
   <bn>Dzongkha Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Dzongkha per LibreOffice</ca>
   <cs>Dzongkha Language Meta-Package for LibreOffice</cs>
   <da>Dzongkha Language Meta-Package for LibreOffice</da>
   <de>Dzongkha Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Dzongkha</el>
   <en>Dzongkha Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Dzongkha para LibreOffice</es_ES>
   <es>Metapaquete de Dzongkha Language para LibreOffice</es>
   <et>Dzongkha Language Meta-Package for LibreOffice</et>
   <eu>Dzongkha Language Meta-Package for LibreOffice</eu>
   <fa>Dzongkha Language Meta-Package for LibreOffice</fa>
   <fil_PH>Dzongkha Language Meta-Package for LibreOffice</fil_PH>
   <fi>Dzongkhan kielen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue Dzongkha pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue Dzongkha pour LibreOffice</fr>
   <gl_ES>Dzonga Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Dzongkha Language Meta-Package for LibreOffice</gu>
   <he_IL>Dzongkha Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु जोंगखा भाषा मेटा-पैकेज</hi>
   <hr>Dzongkha Language Meta-Package for LibreOffice</hr>
   <hu>Dzongkha Language Meta-Package for LibreOffice</hu>
   <id>Dzongkha Language Meta-Package for LibreOffice</id>
   <is>Dzongkha Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua dzongkha per LibreOffice</it>
   <ja>LibreOffice用のゾンカ語メタパッケージ</ja>
   <kk>Dzongkha Language Meta-Package for LibreOffice</kk>
   <ko>Dzongkha Language Meta-Package for LibreOffice</ko>
   <ku>Dzongkha Language Meta-Package for LibreOffice</ku>
   <lt>Dzongkha Language Meta-Package for LibreOffice</lt>
   <mk>Dzongkha Language Meta-Package for LibreOffice</mk>
   <mr>Dzongkha Language Meta-Package for LibreOffice</mr>
   <nb_NO>Dzongkha Language Meta-Package for LibreOffice</nb_NO>
   <nb>Dzongkha språkpakke for LibreOffice</nb>
   <nl_BE>Dzongkha Language Meta-Package for LibreOffice</nl_BE>
   <nl>Dzongkha Taal Meta-Pakket voor LibreOffice</nl>
   <or>Dzongkha Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Dzongkha dla LibreOffice</pl>
   <pt_BR>Butanês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Dzonga Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Dzongkha Language Meta-Package for LibreOffice</ro>
   <ru>Dzongkha Language Meta-Package for LibreOffice</ru>
   <sk>Dzongkha Language Meta-Package for LibreOffice</sk>
   <sl>Dzongha jezikovni meta-paket za LibreOffice</sl>
   <so>Dzongkha Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në xonga</sq>
   <sr>Dzongkha Language Meta-Package for LibreOffice</sr>
   <sv>Dzongkha Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Dzongkha สำหรับ LibreOffice</th>
   <tr>LibreOffice için Bhutan Dili Üst-Paketi</tr>
   <uk>Dzongkha Language Meta-Package for LibreOffice</uk>
   <vi>Dzongkha Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 不丹语语言包</zh_CN>
   <zh_HK>Dzongkha Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Dzongkha Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-dz
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-dz
</uninstall_package_names>

</app>
