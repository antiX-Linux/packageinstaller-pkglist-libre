<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
German_LO_Latest_main
</name>

<description>
   <am>German Language Meta-Package for LibreOffice</am>
   <ar>German Language Meta-Package for LibreOffice</ar>
   <be>German Language Meta-Package for LibreOffice</be>
   <bg>German Language Meta-Package for LibreOffice</bg>
   <bn>German Language Meta-Package for LibreOffice</bn>
   <ca>Meta-paquet d'alemany per LibreOffice</ca>
   <cs>German Language Meta-Package for LibreOffice</cs>
   <da>Tysk sprog-metapakke til LibreOffice</da>
   <de>Deutsches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Γερμανικά</el>
   <en>German Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Idioma Alemán para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Alemán para LibreOffice</es>
   <et>German Language Meta-Package for LibreOffice</et>
   <eu>German Language Meta-Package for LibreOffice</eu>
   <fa>German Language Meta-Package for LibreOffice</fa>
   <fil_PH>German Language Meta-Package for LibreOffice</fil_PH>
   <fi>Saksankielinen kielipaketti LibreOffice:lle </fi>
   <fr_BE>Méta-paquet langue allemande pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue allemande pour LibreOffice</fr>
   <gl_ES>Alemán Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>German Language Meta-Package for LibreOffice</gu>
   <he_IL>German Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु जर्मन भाषा मेटा-पैकेज</hi>
   <hr>German Language Meta-Package for LibreOffice</hr>
   <hu>German Language Meta-Package for LibreOffice</hu>
   <id>German Language Meta-Package for LibreOffice</id>
   <is>German Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua tedesca per LibreOffice</it>
   <ja>LibreOffice用のドイツ語メタパッケージ</ja>
   <kk>German Language Meta-Package for LibreOffice</kk>
   <ko>German Language Meta-Package for LibreOffice</ko>
   <ku>German Language Meta-Package for LibreOffice</ku>
   <lt>German Language Meta-Package for LibreOffice</lt>
   <mk>German Language Meta-Package for LibreOffice</mk>
   <mr>German Language Meta-Package for LibreOffice</mr>
   <nb_NO>German Language Meta-Package for LibreOffice</nb_NO>
   <nb>Tysk språkpakke for LibreOffice</nb>
   <nl_BE>German Language Meta-Package for LibreOffice</nl_BE>
   <nl>Duitse Taal Meta-Pakket voor LibreOffice</nl>
   <or>German Language Meta-Package for LibreOffice</or>
   <pl>Niemiecki metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Alemão Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Alemão Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>German Language Meta-Package for LibreOffice</ro>
   <ru>German Language Meta-Package for LibreOffice</ru>
   <sk>German Language Meta-Package for LibreOffice</sk>
   <sl>Nemški jezikovni meta-paket za LibreOffice</sl>
   <so>German Language Meta-Package for LibreOffice</so>
   <sq>Përkthimi i Firefox-it në gjermanisht</sq>
   <sr>German Language Meta-Package for LibreOffice</sr>
   <sv>Tyskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา German สำหรับ LibreOffice</th>
   <tr>LibreOffice için Almanca Dili Üst-Paketi</tr>
   <uk>German Language Meta-Package for LibreOffice</uk>
   <vi>German Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 德语语言包</zh_CN>
   <zh_HK>German Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>German Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-de
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-de
libreoffice-gtk3
</uninstall_package_names>

</app>
