<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Chile_Default_Firefox_esr
</name>

<description>
   <am>Spanish (Chile) localisation of Firefox ESR</am>
   <ar>Spanish (Chile) localisation of Firefox ESR</ar>
   <be>Spanish (Chile) localisation of Firefox ESR</be>
   <bg>Spanish (Chile) localisation of Firefox ESR</bg>
   <bn>Spanish (Chile) localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Castellà (Chile)</ca>
   <cs>Spanish (Chile) localisation of Firefox ESR</cs>
   <da>Spanish (Chile) localisation of Firefox ESR</da>
   <de>Lokalisation für chilenisches Spanisch von “Firefox ESR”</de>
   <el>Ισπανικά (Χιλή) για Firefox ESR</el>
   <en>Spanish (Chile) localisation of Firefox ESR</en>
   <es_ES>Localización en Español (Chile) de Firefox ESR</es_ES>
   <es>Localización Español (Chile) de Firefox ESR</es>
   <et>Spanish (Chile) localisation of Firefox ESR</et>
   <eu>Spanish (Chile) localisation of Firefox ESR</eu>
   <fa>Spanish (Chile) localisation of Firefox ESR</fa>
   <fil_PH>Spanish (Chile) localisation of Firefox ESR</fil_PH>
   <fi>Spanish (Chile) localisation of Firefox ESR</fi>
   <fr_BE>Localisation en espagnol (Chili) pour Firefox ESR</fr_BE>
   <fr>Localisation en espagnol (Chili) pour Firefox ESR</fr>
   <gl_ES>Spanish (Chile) localisation of Firefox ESR</gl_ES>
   <gu>Spanish (Chile) localisation of Firefox ESR</gu>
   <he_IL>Spanish (Chile) localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का स्पेनिश(चिली) संस्करण</hi>
   <hr>Spanish (Chile) localisation of Firefox ESR</hr>
   <hu>Spanish (Chile) localisation of Firefox ESR</hu>
   <id>Spanish (Chile) localisation of Firefox ESR</id>
   <is>Spanish (Chile) localisation of Firefox ESR</is>
   <it>Localizzazione spagnola (Cile) di Firefox ESR</it>
   <ja>スペイン語（チリ）版 Firefox ESR</ja>
   <kk>Spanish (Chile) localisation of Firefox ESR</kk>
   <ko>Spanish (Chile) localisation of Firefox ESR</ko>
   <ku>Spanish (Chile) localisation of Firefox ESR</ku>
   <lt>Spanish (Chile) localisation of Firefox ESR</lt>
   <mk>Spanish (Chile) localisation of Firefox ESR</mk>
   <mr>Spanish (Chile) localisation of Firefox ESR</mr>
   <nb_NO>Spanish (Chile) localisation of Firefox ESR</nb_NO>
   <nb>Spansk (chilensk) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Spanish (Chile) localisation of Firefox ESR</nl_BE>
   <nl>Spaanse (Chili) lokalisatie van Firefox ESR</nl>
   <or>Spanish (Chile) localisation of Firefox ESR</or>
   <pl>Spanish (Chile) localisation of Firefox ESR</pl>
   <pt_BR>Espanhol (Chile) Localização para o Firefox ESR</pt_BR>
   <pt>Castelhano (Chile) Localização para Firefox ESR</pt>
   <ro>Spanish (Chile) localisation of Firefox ESR</ro>
   <ru>Spanish (Chile) localisation of Firefox ESR</ru>
   <sk>Spanish (Chile) localisation of Firefox ESR</sk>
   <sl>Španske (Čile) krajevne nastavitve za Firefox-ESR</sl>
   <so>Spanish (Chile) localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në spanjisht (Kili)</sq>
   <sr>Spanish (Chile) localisation of Firefox ESR</sr>
   <sv>Spansk (Chile) lokalisering av Firefox ESR</sv>
   <th>Spanish (Chile) localisation of Firefox ESR</th>
   <tr>Firefox ESR İspanyolca (Şili) yerelleştirmesi</tr>
   <uk>Spanish (Chile) localisation of Firefox ESR</uk>
   <vi>Spanish (Chile) localisation of Firefox ESR</vi>
   <zh_CN>Spanish (Chile) localisation of Firefox ESR</zh_CN>
   <zh_HK>Spanish (Chile) localisation of Firefox ESR</zh_HK>
   <zh_TW>Spanish (Chile) localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-es-cl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-es-cl
</uninstall_package_names>

</app>
