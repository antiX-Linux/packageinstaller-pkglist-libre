<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Norwegian_Firefox
</name>

<description>
   <am>Norwegian localisation of Firefox</am>
   <ar>Norwegian localisation of Firefox</ar>
   <be>Norwegian localisation of Firefox</be>
   <bg>Norwegian localisation of Firefox</bg>
   <bn>Norwegian localisation of Firefox</bn>
   <ca>Localització de Firefox en Noruec</ca>
   <cs>Norwegian localisation of Firefox</cs>
   <da>Norsk oversættelse af Firefox</da>
   <de>Norwegische Lokalisierung von Firefox</de>
   <el>Νορβηγικά για το Firefox</el>
   <en>Norwegian localisation of Firefox</en>
   <es_ES>Localización Noruega de Firefox</es_ES>
   <es>Localización Noruego de Firefox</es>
   <et>Norwegian localisation of Firefox</et>
   <eu>Norwegian localisation of Firefox</eu>
   <fa>Norwegian localisation of Firefox</fa>
   <fil_PH>Norwegian localisation of Firefox</fil_PH>
   <fi>Norjalainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en norvégien pour Firefox</fr_BE>
   <fr>Localisation en norvégien pour Firefox</fr>
   <gl_ES>Localización de Firefox ao noruego</gl_ES>
   <gu>Norwegian localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לנורווגית</he_IL>
   <hi>फायरफॉक्स का नार्वेजियन संस्करण</hi>
   <hr>Norwegian localisation of Firefox</hr>
   <hu>Norwegian localisation of Firefox</hu>
   <id>Norwegian localisation of Firefox</id>
   <is>Norwegian localisation of Firefox</is>
   <it>Localizzazione norvegese di Firefox</it>
   <ja>Firefox のノルウェー語版</ja>
   <kk>Norwegian localisation of Firefox</kk>
   <ko>Norwegian localisation of Firefox</ko>
   <ku>Norwegian localisation of Firefox</ku>
   <lt>Norwegian localisation of Firefox</lt>
   <mk>Norwegian localisation of Firefox</mk>
   <mr>Norwegian localisation of Firefox</mr>
   <nb_NO>Norwegian localisation of Firefox</nb_NO>
   <nb>Norsk bokmålsk lokaltilpassing av Firefox</nb>
   <nl_BE>Norwegian localisation of Firefox</nl_BE>
   <nl>Noorse lokalisatie van Firefox</nl>
   <or>Norwegian localisation of Firefox</or>
   <pl>Norweska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Norueguês Localização para o Firefox</pt_BR>
   <pt>Norueguês Localização para Firefox</pt>
   <ro>Norwegian localisation of Firefox</ro>
   <ru>Норвежская локализация Firefox</ru>
   <sk>Norwegian localisation of Firefox</sk>
   <sl>Norveške krajevne nastavitve za Firefox</sl>
   <so>Norwegian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në norvegjisht</sq>
   <sr>Norwegian localisation of Firefox</sr>
   <sv>Norsk lokalisering av Firefox </sv>
   <th>Norwegian localisation ของ Firefox</th>
   <tr>Firefox'un Norveççe yerelleştirmesi</tr>
   <uk>Norwegian локалізація Firefox</uk>
   <vi>Norwegian localisation of Firefox</vi>
   <zh_CN>Norwegian localisation of Firefox</zh_CN>
   <zh_HK>Norwegian localisation of Firefox</zh_HK>
   <zh_TW>Norwegian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-nb-no
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-nb-no
</uninstall_package_names>
</app>
