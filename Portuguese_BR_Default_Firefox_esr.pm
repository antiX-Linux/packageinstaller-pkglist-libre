<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Portuguese(BR)_Default_Firefox_esr
</name>

<description>
   <am>Portuguese(BR) localisation of Firefox ESR</am>
   <ar>Portuguese(BR) localisation of Firefox ESR</ar>
   <be>Portuguese(BR) localisation of Firefox ESR</be>
   <bg>Portuguese(BR) localisation of Firefox ESR</bg>
   <bn>Portuguese(BR) localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Portuguès (Brasil)</ca>
   <cs>Portuguese(BR) localisation of Firefox ESR</cs>
   <da>Portuguese(BR) localisation of Firefox ESR</da>
   <de>Lokalisation von “Firefox ESR” für brasilianisches Portugiesisch</de>
   <el>Πορτογαλικά (BR) εντοπισμός του Firefox ESR</el>
   <en>Portuguese(BR) localisation of Firefox ESR</en>
   <es_ES>Localización Portugués (BR) de Firefox ESR</es_ES>
   <es>Localización Portugués (BR) de Firefox ESR</es>
   <et>Portuguese(BR) localisation of Firefox ESR</et>
   <eu>Portuguese(BR) localisation of Firefox ESR</eu>
   <fa>Portuguese(BR) localisation of Firefox ESR</fa>
   <fil_PH>Portuguese(BR) localisation of Firefox ESR</fil_PH>
   <fi>Portuguese(BR) localisation of Firefox ESR</fi>
   <fr_BE>Localisation en portugais (Brésil) pour Firefox ESR</fr_BE>
   <fr>Localisation en portugais (Brésil) pour Firefox ESR</fr>
   <gl_ES>Portuguese(BR) localisation of Firefox ESR</gl_ES>
   <gu>Portuguese(BR) localisation of Firefox ESR</gu>
   <he_IL>Portuguese(BR) localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का पुर्तगाली (ब्राज़ील) संस्करण</hi>
   <hr>Portuguese(BR) localisation of Firefox ESR</hr>
   <hu>Portuguese(BR) localisation of Firefox ESR</hu>
   <id>Portuguese(BR) localisation of Firefox ESR</id>
   <is>Portuguese(BR) localisation of Firefox ESR</is>
   <it>Localizzazione portoghese(BR) di Firefox ESR</it>
   <ja>ポルトガル語（ブラジル）版 Firefox ESR</ja>
   <kk>Portuguese(BR) localisation of Firefox ESR</kk>
   <ko>Portuguese(BR) localisation of Firefox ESR</ko>
   <ku>Portuguese(BR) localisation of Firefox ESR</ku>
   <lt>Portuguese(BR) localisation of Firefox ESR</lt>
   <mk>Portuguese(BR) localisation of Firefox ESR</mk>
   <mr>Portuguese(BR) localisation of Firefox ESR</mr>
   <nb_NO>Portuguese(BR) localisation of Firefox ESR</nb_NO>
   <nb>Portugisisk (BR) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Portuguese(BR) localisation of Firefox ESR</nl_BE>
   <nl>Portuguese(BR) localisation of Firefox ESR</nl>
   <or>Portuguese(BR) localisation of Firefox ESR</or>
   <pl>Portuguese(BR) localisation of Firefox ESR</pl>
   <pt_BR>Português (BR) Localização para o Firefox ESR</pt_BR>
   <pt>Português(BR) Localização para Firefox ESR</pt>
   <ro>Portuguese(BR) localisation of Firefox ESR</ro>
   <ru>Portuguese(BR) localisation of Firefox ESR</ru>
   <sk>Portuguese(BR) localisation of Firefox ESR</sk>
   <sl>Portugalske (BR) krajevne nastavitve za Firefox-ESR</sl>
   <so>Portuguese(BR) localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në portugalishte (BR)</sq>
   <sr>Portuguese(BR) localisation of Firefox ESR</sr>
   <sv>Portugisisk(BR) lokalisering för Firefox ESR</sv>
   <th>Portuguese(BR) localisation of Firefox ESR</th>
   <tr>Firefox'un Brezilya Portekizcesi yerelleştirmesi</tr>
   <uk>Portuguese(BR) localisation of Firefox ESR</uk>
   <vi>Portuguese(BR) localisation of Firefox ESR</vi>
   <zh_CN>Portuguese(BR) localisation of Firefox ESR</zh_CN>
   <zh_HK>Portuguese(BR) localisation of Firefox ESR</zh_HK>
   <zh_TW>Portuguese(BR) localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-pt-br
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-pt-br
</uninstall_package_names>

</app>
