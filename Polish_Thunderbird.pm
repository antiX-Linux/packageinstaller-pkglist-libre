<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Polish_Thunderbird
</name>

<description>
   <am>Polish localisation of Thunderbird</am>
   <ar>Polish localisation of Thunderbird</ar>
   <be>Polish localisation of Thunderbird</be>
   <bg>Polish localisation of Thunderbird</bg>
   <bn>Polish localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Polonès</ca>
   <cs>Polish localisation of Thunderbird</cs>
   <da>Polsk oversættelse af Thunderbird</da>
   <de>Polnische Lokalisierung von Thunderbird</de>
   <el>Πολωνικά για το Thunderbird</el>
   <en>Polish localisation of Thunderbird</en>
   <es_ES>Localización Polaca de Thunderbird</es_ES>
   <es>Localización Polaco de Thunderbird</es>
   <et>Polish localisation of Thunderbird</et>
   <eu>Polish localisation of Thunderbird</eu>
   <fa>Polish localisation of Thunderbird</fa>
   <fil_PH>Polish localisation of Thunderbird</fil_PH>
   <fi>Puolalainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en polonais pour Thunderbird</fr_BE>
   <fr>Localisation en polonais pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao polaco</gl_ES>
   <gu>Polish localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לפולנית</he_IL>
   <hi>थंडरबर्ड का पोलिश संस्करण</hi>
   <hr>Polish localisation of Thunderbird</hr>
   <hu>Polish localisation of Thunderbird</hu>
   <id>Polish localisation of Thunderbird</id>
   <is>Polish localisation of Thunderbird</is>
   <it>Localizzazione polacca di Thunderbird</it>
   <ja>Thunderbird のポーランド語版</ja>
   <kk>Polish localisation of Thunderbird</kk>
   <ko>Polish localisation of Thunderbird</ko>
   <ku>Polish localisation of Thunderbird</ku>
   <lt>Polish localisation of Thunderbird</lt>
   <mk>Polish localisation of Thunderbird</mk>
   <mr>Polish localisation of Thunderbird</mr>
   <nb_NO>Polish localisation of Thunderbird</nb_NO>
   <nb>Polsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Polish localisation of Thunderbird</nl_BE>
   <nl>Poolse lokalisatie van Thunderbird</nl>
   <or>Polish localisation of Thunderbird</or>
   <pl>Polska lokalizacja Thunderbirda</pl>
   <pt_BR>Polonês Localização para o Thunderbird</pt_BR>
   <pt>Polaco Localização para Thunderbird</pt>
   <ro>Polish localisation of Thunderbird</ro>
   <ru>Polish localisation of Thunderbird</ru>
   <sk>Polish localisation of Thunderbird</sk>
   <sl>Poljske krajevne nastavitve za Thunderbird</sl>
   <so>Polish localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në polonisht</sq>
   <sr>Polish localisation of Thunderbird</sr>
   <sv>Polsk lokalisering av Thunderbird</sv>
   <th>Polish localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Lehçe yerelleştirmesi</tr>
   <uk>Polish локалізація Thunderbird</uk>
   <vi>Polish localisation of Thunderbird</vi>
   <zh_CN>Polish localisation of Thunderbird</zh_CN>
   <zh_HK>Polish localisation of Thunderbird</zh_HK>
   <zh_TW>Polish localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-pl
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-pl
</uninstall_package_names>

</app>
