<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English (GB)
</name>

<description>
   <am>English (GB) dictionary for hunspell</am>
   <ar>English (GB) dictionary for hunspell</ar>
   <be>English (GB) dictionary for hunspell</be>
   <bg>English (GB) dictionary for hunspell</bg>
   <bn>English (GB) dictionary for hunspell</bn>
   <ca>Diccionari Anglès (GB) per hunspell</ca>
   <cs>English (GB) dictionary for hunspell</cs>
   <da>English (GB) dictionary for hunspell</da>
   <de>Englisches (großbritannisches) Wörterbuch für “Hunspell”</de>
   <el>Αγγλικό (GB) λεξικό για hunspell</el>
   <en>English (GB) dictionary for hunspell</en>
   <es_ES>Diccionario Ingles (GB) para hunspell</es_ES>
   <es>Diccionario Inglés (GB) para hunspell</es>
   <et>English (GB) dictionary for hunspell</et>
   <eu>English (GB) dictionary for hunspell</eu>
   <fa>English (GB) dictionary for hunspell</fa>
   <fil_PH>English (GB) dictionary for hunspell</fil_PH>
   <fi>English (GB) dictionary for hunspell</fi>
   <fr_BE>Anglais (GB) dictionnaire pour hunspell</fr_BE>
   <fr>Anglais (GB) dictionnaire pour hunspell</fr>
   <gl_ES>English (GB) dictionary for hunspell</gl_ES>
   <gu>English (GB) dictionary for hunspell</gu>
   <he_IL>English (GB) dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु अंग्रेजी (ब्रिटेन) शब्दकोष</hi>
   <hr>English (GB) dictionary for hunspell</hr>
   <hu>English (GB) dictionary for hunspell</hu>
   <id>English (GB) dictionary for hunspell</id>
   <is>English (GB) dictionary for hunspell</is>
   <it>Dizionario inglese (GB) per hunspell</it>
   <ja>Hunspell 用英語（イギリス）辞書</ja>
   <kk>English (GB) dictionary for hunspell</kk>
   <ko>English (GB) dictionary for hunspell</ko>
   <ku>English (GB) dictionary for hunspell</ku>
   <lt>English (GB) dictionary for hunspell</lt>
   <mk>English (GB) dictionary for hunspell</mk>
   <mr>English (GB) dictionary for hunspell</mr>
   <nb_NO>English (GB) dictionary for hunspell</nb_NO>
   <nb>Engelsk (britisk) ordliste for hunspell</nb>
   <nl_BE>English (GB) dictionary for hunspell</nl_BE>
   <nl>Engels (GB) woordenboek voor hunspell</nl>
   <or>English (GB) dictionary for hunspell</or>
   <pl>English (GB) dictionary for hunspell</pl>
   <pt_BR>Dicionário Inglês da Grã-Bretanha para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Inglês (GB) para hunspell</pt>
   <ro>English (GB) dictionary for hunspell</ro>
   <ru>English (GB) dictionary for hunspell</ru>
   <sk>English (GB) dictionary for hunspell</sk>
   <sl>Angleški (VB) slovar za hunspell</sl>
   <so>English (GB) dictionary for hunspell</so>
   <sq>Fjalor anglisht (Britani e Madhe) për hunspell</sq>
   <sr>English (GB) dictionary for hunspell</sr>
   <sv>Engelsk (GB) ordbok för hunspell</sv>
   <th>English (GB) dictionary for hunspell</th>
   <tr>Huspell için İngilizce (Britanya) sözlük</tr>
   <uk>English (GB) dictionary for hunspell</uk>
   <vi>English (GB) dictionary for hunspell</vi>
   <zh_CN>English (GB) dictionary for hunspell</zh_CN>
   <zh_HK>English (GB) dictionary for hunspell</zh_HK>
   <zh_TW>English (GB) dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-en-gb
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-en-gb
</uninstall_package_names>

</app>
