<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Chinese_Input
</name>

<description>
   <am>Chinese ibus</am>
   <ar>Chinese ibus</ar>
   <be>Chinese ibus</be>
   <bg>Chinese ibus</bg>
   <bn>Chinese ibus</bn>
   <ca>IBUS en Xinès</ca>
   <cs>Chinese ibus</cs>
   <da>Kinesisk ibus</da>
   <de>Chinesischer IBus</de>
   <el>Κινέζικα ibus</el>
   <en>Chinese ibus</en>
   <es_ES>ibus Chino</es_ES>
   <es>ibus Chino</es>
   <et>Chinese ibus</et>
   <eu>Chinese ibus</eu>
   <fa>Chinese ibus</fa>
   <fil_PH>Chinese ibus</fil_PH>
   <fi>Kiinalainen ibus</fi>
   <fr_BE>Chinois ibus</fr_BE>
   <fr>Chinois ibus</fr>
   <gl_ES>Chinés ibus</gl_ES>
   <gu>Chinese ibus</gu>
   <he_IL>Chinese ibus</he_IL>
   <hi>चीनी ibus</hi>
   <hr>Chinese ibus</hr>
   <hu>Chinese ibus</hu>
   <id>Chinese ibus</id>
   <is>Chinese ibus</is>
   <it>Chinese ibus</it>
   <ja>中国語用 ibus</ja>
   <kk>Chinese ibus</kk>
   <ko>Chinese ibus</ko>
   <ku>Chinese ibus</ku>
   <lt>Kinų ibus</lt>
   <mk>Chinese ibus</mk>
   <mr>Chinese ibus</mr>
   <nb_NO>Chinese ibus</nb_NO>
   <nb>Kinesisk ibus</nb>
   <nl_BE>Chinese ibus</nl_BE>
   <nl>Chinese ibus</nl>
   <or>Chinese ibus</or>
   <pl>Chiński ibus</pl>
   <pt_BR>ibus para o idioma Chinês</pt_BR>
   <pt>Chinês ibus</pt>
   <ro>Chinese ibus</ro>
   <ru>Chinese ibus</ru>
   <sk>Chinese ibus</sk>
   <sl>Kitajski ibus</sl>
   <so>Chinese ibus</so>
   <sq>ibus për kinezçen</sq>
   <sr>Chinese ibus</sr>
   <sv>Kinesisk ibus</sv>
   <th>ibus ภาษาจีน</th>
   <tr>Çince ibus</tr>
   <uk>Chinese ibus</uk>
   <vi>Chinese ibus</vi>
   <zh_CN>中文 ibus 输入法</zh_CN>
   <zh_HK>Chinese ibus</zh_HK>
   <zh_TW>Chinese ibus</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
fonts-arphic-uming
im-config
ibus-pinyin
ibus-gtk
manpages-zh
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
fonts-arphic-uming
im-config
ibus-pinyin
ibus-gtk
manpages-zh
</uninstall_package_names>

</app>
