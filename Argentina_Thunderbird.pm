<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Argentina_Thunderbird
</name>

<description>
   <am>Spanish (Argentina) localisation of Thunderbird</am>
   <ar>Spanish (Argentina) localisation of Thunderbird</ar>
   <be>Spanish (Argentina) localisation of Thunderbird</be>
   <bg>Spanish (Argentina) localisation of Thunderbird</bg>
   <bn>Spanish (Argentina) localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Castellà d'Argentina</ca>
   <cs>Spanish (Argentina) localisation of Thunderbird</cs>
   <da>Spanish (Argentina) localisation of Thunderbird</da>
   <de>Lokalisation für argentinisches Spanisch von “Thunderbird”</de>
   <el>Ισπανικά (Αργεντινή) για το Thunderbird</el>
   <en>Spanish (Argentina) localisation of Thunderbird</en>
   <es_ES>Localización Español (Argentina) para Thunderbird</es_ES>
   <es>Localización Español (Argentina) de Thunderbird</es>
   <et>Spanish (Argentina) localisation of Thunderbird</et>
   <eu>Spanish (Argentina) localisation of Thunderbird</eu>
   <fa>Spanish (Argentina) localisation of Thunderbird</fa>
   <fil_PH>Spanish (Argentina) localisation of Thunderbird</fil_PH>
   <fi>Spanish (Argentina) localisation of Thunderbird</fi>
   <fr_BE>Localisation en espagnol (Argentine) pour Thunderbird</fr_BE>
   <fr>Localisation en espagnol (Argentine) pour Thunderbird</fr>
   <gl_ES>Spanish (Argentina) localisation of Thunderbird</gl_ES>
   <gu>Spanish (Argentina) localisation of Thunderbird</gu>
   <he_IL>Spanish (Argentina) localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का स्पेनिश (अर्जेंटीना) संस्करण</hi>
   <hr>Spanish (Argentina) localisation of Thunderbird</hr>
   <hu>Spanish (Argentina) localisation of Thunderbird</hu>
   <id>Spanish (Argentina) localisation of Thunderbird</id>
   <is>Spanish (Argentina) localisation of Thunderbird</is>
   <it>Localizzazione spagnola (Argentina) di Thunderbird</it>
   <ja>スペイン語（アルゼンチン）版 Thunderbird</ja>
   <kk>Spanish (Argentina) localisation of Thunderbird</kk>
   <ko>Spanish (Argentina) localisation of Thunderbird</ko>
   <ku>Spanish (Argentina) localisation of Thunderbird</ku>
   <lt>Spanish (Argentina) localisation of Thunderbird</lt>
   <mk>Spanish (Argentina) localisation of Thunderbird</mk>
   <mr>Spanish (Argentina) localisation of Thunderbird</mr>
   <nb_NO>Spanish (Argentina) localisation of Thunderbird</nb_NO>
   <nb>Spansk (argentinsk) lokaltilpassing av Thunderbird</nb>
   <nl_BE>Spanish (Argentina) localisation of Thunderbird</nl_BE>
   <nl>Spaanse (Argentinië) lokalisatie van Thunderbird</nl>
   <or>Spanish (Argentina) localisation of Thunderbird</or>
   <pl>Spanish (Argentina) localisation of Thunderbird</pl>
   <pt_BR>Espanhol (Argentina) Localização para o Thunderbird</pt_BR>
   <pt>Castelhano (Argentina) Localização para Thunderbird</pt>
   <ro>Spanish (Argentina) localisation of Thunderbird</ro>
   <ru>Spanish (Argentina) localisation of Thunderbird</ru>
   <sk>Spanish (Argentina) localisation of Thunderbird</sk>
   <sl>Španske (Argentina) krajevne nastavitve za Thunderbird</sl>
   <so>Spanish (Argentina) localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në spanjisht (Argjentinë)</sq>
   <sr>Spanish (Argentina) localisation of Thunderbird</sr>
   <sv>Spansk (Argentina) lokalisering av Thunderbird</sv>
   <th>Spanish (Argentina) localisation of Thunderbird</th>
   <tr>Thunderbird İspanyolca (Arjantin) yerelleştirmesi</tr>
   <uk>Spanish (Argentina) localisation of Thunderbird</uk>
   <vi>Spanish (Argentina) localisation of Thunderbird</vi>
   <zh_CN>Spanish (Argentina) localisation of Thunderbird</zh_CN>
   <zh_HK>Spanish (Argentina) localisation of Thunderbird</zh_HK>
   <zh_TW>Spanish (Argentina) localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-es-ar
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-es-ar
</uninstall_package_names>

</app>
