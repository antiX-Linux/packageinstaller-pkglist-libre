<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English_US_LO_Latest_main
</name>

<description>
   <am>US English Help for LibreOffice</am>
   <ar>US English Help for LibreOffice</ar>
   <be>US English Help for LibreOffice</be>
   <bg>US English Help for LibreOffice</bg>
   <bn>US English Help for LibreOffice</bn>
   <ca>Ajuda en anglès (USA) per LibreOffice</ca>
   <cs>US English Help for LibreOffice</cs>
   <da>Engelsk (USA) hjælp til LibreOffice</da>
   <de>Englische (US) Hilfe für LibreOffice</de>
   <el>Βοήθεια για το LibreOffice στα Αγγλικά(USA)</el>
   <en>US English Help for LibreOffice</en>
   <es_ES>Ayuda Inglesa (EE.UU.) para LiberOffice</es_ES>
   <es>Ayuda Inglés (EE.UU.) para LiberOffice</es>
   <et>US English Help for LibreOffice</et>
   <eu>US English Help for LibreOffice</eu>
   <fa>US English Help for LibreOffice</fa>
   <fil_PH>US English Help for LibreOffice</fil_PH>
   <fi>US-englantilainen apuopas LibreOffice:lle</fi>
   <fr_BE>Aide anglais_US pour LibreOffice</fr_BE>
   <fr>Aide anglais_US pour LibreOffice</fr>
   <gl_ES>Axuda para LibreOffice en inglés de EEUU</gl_ES>
   <gu>US English Help for LibreOffice</gu>
   <he_IL>US English Help for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु अमेरिकी अंग्रेजी में सहायता</hi>
   <hr>US English Help for LibreOffice</hr>
   <hu>US English Help for LibreOffice</hu>
   <id>US English Help for LibreOffice</id>
   <is>US English Help for LibreOffice</is>
   <it>Guida in inglese US per LibreOffice</it>
   <ja>LibreOffice のアメリカ英語ヘルプ</ja>
   <kk>US English Help for LibreOffice</kk>
   <ko>US English Help for LibreOffice</ko>
   <ku>US English Help for LibreOffice</ku>
   <lt>US English Help for LibreOffice</lt>
   <mk>US English Help for LibreOffice</mk>
   <mr>US English Help for LibreOffice</mr>
   <nb_NO>US English Help for LibreOffice</nb_NO>
   <nb>Amerikansk engelsk hjelpetekst for LibreOffice</nb>
   <nl_BE>US English Help for LibreOffice</nl_BE>
   <nl>US Engelse Hulp voor LibreOffice</nl>
   <or>US English Help for LibreOffice</or>
   <pl>Angielski Amerykański pomoc dla LibreOffice</pl>
   <pt_BR>Inglês dos EUA Ajuda para o LibreOffice</pt_BR>
   <pt>Inglês US Help para LibreOffice</pt>
   <ro>US English Help for LibreOffice</ro>
   <ru>US English Help for LibreOffice</ru>
   <sk>US English Help for LibreOffice</sk>
   <sl>US English Help for LibreOffice</sl>
   <so>US English Help for LibreOffice</so>
   <sq>Ndihmë për LibreOffice-in në anglishte të ShBA-ve</sq>
   <sr>US English Help for LibreOffice</sr>
   <sv>US Engelsk Hjälp för LibreOffice</sv>
   <th>ความช่วยเหลือภาษา US English สำหรับ LibreOffice</th>
   <tr>LibreOffice için Amerikan İngilizcesi Yardımı</tr>
   <uk>US English довідка LibreOffice</uk>
   <vi>US English Help for LibreOffice</vi>
   <zh_CN>LibreOffice 美式英语帮助</zh_CN>
   <zh_HK>US English Help for LibreOffice</zh_HK>
   <zh_TW>US English Help for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-help-en-us
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-help-en-us
</uninstall_package_names>

</app>
