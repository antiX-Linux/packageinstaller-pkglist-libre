<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Afrikaan_Default_Firefox_esr
</name>

<description>
   <am>Afrikaan localisation of Firefox-ESR</am>
   <ar>Afrikaan localisation of Firefox-ESR</ar>
   <be>Afrikaan localisation of Firefox-ESR</be>
   <bg>Afrikaan localisation of Firefox-ESR</bg>
   <bn>Afrikaan localisation of Firefox-ESR</bn>
   <ca>Localització de Firefox-ESR en Afrikaans</ca>
   <cs>Afrikaan localisation of Firefox-ESR</cs>
   <da>Afrikaan localisation of Firefox-ESR</da>
   <de>Lokalisation für die südafrikanische Sprache “Kapholländisch” von “Firefox ESR”</de>
   <el>Αφρικανικά για το Firefox-ESR</el>
   <en>Afrikaan localisation of Firefox-ESR</en>
   <es_ES>Localización africana de Firefox-ESR</es_ES>
   <es>Localización Africano de Firefox ESR</es>
   <et>Afrikaan localisation of Firefox-ESR</et>
   <eu>Afrikaan localisation of Firefox-ESR</eu>
   <fa>Afrikaan localisation of Firefox-ESR</fa>
   <fil_PH>Afrikaan localisation of Firefox-ESR</fil_PH>
   <fi>Afrikaansinkielinen Firefox-ESR-kielipaketti</fi>
   <fr_BE>Localisation en afrikaans pour Firefox ESR</fr_BE>
   <fr>Localisation en afrikaans pour Firefox ESR</fr>
   <gl_ES>Afrikaan localisation of Firefox-ESR</gl_ES>
   <gu>Afrikaan localisation of Firefox-ESR</gu>
   <he_IL>Afrikaan localisation of Firefox-ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का अफ्रीकी डच संस्करण</hi>
   <hr>Afrikaan localisation of Firefox-ESR</hr>
   <hu>Afrikaan localisation of Firefox-ESR</hu>
   <id>Afrikaan localisation of Firefox-ESR</id>
   <is>Afrikaan localisation of Firefox-ESR</is>
   <it>Localizzazione in afrikaans di Firefox ESR</it>
   <ja>Firefox-ESRのアフリカーンス語版</ja>
   <kk>Afrikaan localisation of Firefox-ESR</kk>
   <ko>Afrikaan localisation of Firefox-ESR</ko>
   <ku>Afrikaan localisation of Firefox-ESR</ku>
   <lt>Afrikaan localisation of Firefox-ESR</lt>
   <mk>Afrikaan localisation of Firefox-ESR</mk>
   <mr>Afrikaan localisation of Firefox-ESR</mr>
   <nb_NO>Afrikaan localisation of Firefox-ESR</nb_NO>
   <nb>Afrikaans lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Afrikaan localisation of Firefox-ESR</nl_BE>
   <nl>Afrikaanse lokalisatie van Firefox-ESR</nl>
   <or>Afrikaan localisation of Firefox-ESR</or>
   <pl>Afrikaan localisation of Firefox-ESR</pl>
   <pt_BR>Africâner Localização para o Firefox ESR</pt_BR>
   <pt>Africander Localização para Firefox ESR</pt>
   <ro>Afrikaan localisation of Firefox-ESR</ro>
   <ru>Afrikaan localisation of Firefox-ESR</ru>
   <sk>Afrikaan localisation of Firefox-ESR</sk>
   <sl>Afrikkanske krajevne nastavitve privzeto nameščenega Firefox ESR</sl>
   <so>Afrikaan localisation of Firefox-ESR</so>
   <sq>Përkthim në afrikaans i Firefox-it ESR</sq>
   <sr>Afrikaan localisation of Firefox-ESR</sr>
   <sv>Afrikaan lokalisering av Firefox-ESR</sv>
   <th>Afrikaan localisation of Firefox-ESR</th>
   <tr>Firefox-ESR Afrika yerelleştirmesi</tr>
   <uk>Afrikaan localisation of Firefox-ESR</uk>
   <vi>Afrikaan localisation of Firefox-ESR</vi>
   <zh_CN>Afrikaan localisation of Firefox-ESR</zh_CN>
   <zh_HK>Afrikaan localisation of Firefox-ESR</zh_HK>
   <zh_TW>Afrikaan localisation of Firefox-ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-af
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-af
</uninstall_package_names>

</app>
