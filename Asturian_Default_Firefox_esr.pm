<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Asturian_Default_Firefox_esr
</name>

<description>
   <am>Asturian localisation of Firefox-ESR</am>
   <ar>Asturian localisation of Firefox-ESR</ar>
   <be>Asturian localisation of Firefox-ESR</be>
   <bg>Asturian localisation of Firefox-ESR</bg>
   <bn>Asturian localisation of Firefox-ESR</bn>
   <ca>Localització de Firefox-ESR en Asturià</ca>
   <cs>Asturian localisation of Firefox-ESR</cs>
   <da>Asturian localisation of Firefox-ESR</da>
   <de>Asturleonesische Lokalisation von “Firefox ESR”</de>
   <el>Asturian για Firefox-ESR</el>
   <en>Asturian localisation of Firefox-ESR</en>
   <es_ES>Localización Asturiana de Firefox-ESR</es_ES>
   <es>Localización Asturiano de Firefox ESR</es>
   <et>Asturian localisation of Firefox-ESR</et>
   <eu>Asturian localisation of Firefox-ESR</eu>
   <fa>Asturian localisation of Firefox-ESR</fa>
   <fil_PH>Asturian localisation of Firefox-ESR</fil_PH>
   <fi>Asturian localisation of Firefox-ESR</fi>
   <fr_BE>Localisation en asturien pour Firefox ESR</fr_BE>
   <fr>Localisation en asturien pour Firefox ESR</fr>
   <gl_ES>Asturian localisation of Firefox-ESR</gl_ES>
   <gu>Asturian localisation of Firefox-ESR</gu>
   <he_IL>Asturian localisation of Firefox-ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का अस्तुरियाई संस्करण</hi>
   <hr>Asturian localisation of Firefox-ESR</hr>
   <hu>Asturian localisation of Firefox-ESR</hu>
   <id>Asturian localisation of Firefox-ESR</id>
   <is>Asturian localisation of Firefox-ESR</is>
   <it>Localizzazione asturian di Firefox ESR</it>
   <ja>アストゥリアス語版 Firefox-ESR</ja>
   <kk>Asturian localisation of Firefox-ESR</kk>
   <ko>Asturian localisation of Firefox-ESR</ko>
   <ku>Asturian localisation of Firefox-ESR</ku>
   <lt>Asturian localisation of Firefox-ESR</lt>
   <mk>Asturian localisation of Firefox-ESR</mk>
   <mr>Asturian localisation of Firefox-ESR</mr>
   <nb_NO>Asturian localisation of Firefox-ESR</nb_NO>
   <nb>Asturisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Asturian localisation of Firefox-ESR</nl_BE>
   <nl>Asturiaans lokalisatie van Firefox-ESR</nl>
   <or>Asturian localisation of Firefox-ESR</or>
   <pl>Asturian localisation of Firefox-ESR</pl>
   <pt_BR>Asturiano Localização para o Firefox ESR</pt_BR>
   <pt>Asturiano Localização para Firefox ESR</pt>
   <ro>Asturian localisation of Firefox-ESR</ro>
   <ru>Asturian localisation of Firefox-ESR</ru>
   <sk>Asturian localisation of Firefox-ESR</sk>
   <sl>Asturijske krajevne nastavitve za Firefox-ESR</sl>
   <so>Asturian localisation of Firefox-ESR</so>
   <sq>Përkthimi i Firefox-it ESR në asturisht</sq>
   <sr>Asturian localisation of Firefox-ESR</sr>
   <sv>Asturisk lokalisering av Firefox-ESR</sv>
   <th>Asturian localisation of Firefox-ESR</th>
   <tr>Firefox-ESR Asturyan Dili yerelleştirmesi</tr>
   <uk>Asturian localisation of Firefox-ESR</uk>
   <vi>Asturian localisation of Firefox-ESR</vi>
   <zh_CN>Asturian localisation of Firefox-ESR</zh_CN>
   <zh_HK>Asturian localisation of Firefox-ESR</zh_HK>
   <zh_TW>Asturian localisation of Firefox-ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ast
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ast
</uninstall_package_names>

</app>
