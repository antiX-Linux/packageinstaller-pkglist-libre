<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Occitan_LO_Latest_main
</name>

<description>
   <am>Occitan Language Meta-Package for LibreOffice</am>
   <ar>Occitan Language Meta-Package for LibreOffice</ar>
   <be>Occitan Language Meta-Package for LibreOffice</be>
   <bg>Occitan Language Meta-Package for LibreOffice</bg>
   <bn>Occitan Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Occità per LibreOffice</ca>
   <cs>Occitan Language Meta-Package for LibreOffice</cs>
   <da>Occitan Language Meta-Package for LibreOffice</da>
   <de>Okzitanisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Occitan</el>
   <en>Occitan Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Occitano para LibreOffice</es_ES>
   <es>Metapaquete de idioma Occitano para LibreOffice</es>
   <et>Occitan Language Meta-Package for LibreOffice</et>
   <eu>Occitan Language Meta-Package for LibreOffice</eu>
   <fa>Occitan Language Meta-Package for LibreOffice</fa>
   <fil_PH>Occitan Language Meta-Package for LibreOffice</fil_PH>
   <fi>Oksitaaninen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue occitane pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue occitane pour LibreOffice</fr>
   <gl_ES>Occitano Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Occitan Language Meta-Package for LibreOffice</gu>
   <he_IL>Occitan Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु ओसीटान भाषा मेटा-पैकेज</hi>
   <hr>Occitan Language Meta-Package for LibreOffice</hr>
   <hu>Occitan Language Meta-Package for LibreOffice</hu>
   <id>Occitan Language Meta-Package for LibreOffice</id>
   <is>Occitan Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua occitana per LibreOffice</it>
   <ja>LibreOffice用のオック語メタパッケージ</ja>
   <kk>Occitan Language Meta-Package for LibreOffice</kk>
   <ko>Occitan Language Meta-Package for LibreOffice</ko>
   <ku>Occitan Language Meta-Package for LibreOffice</ku>
   <lt>Occitan Language Meta-Package for LibreOffice</lt>
   <mk>Occitan Language Meta-Package for LibreOffice</mk>
   <mr>Occitan Language Meta-Package for LibreOffice</mr>
   <nb_NO>Occitan Language Meta-Package for LibreOffice</nb_NO>
   <nb>Oksitansk språkpakke for LibreOffice</nb>
   <nl_BE>Occitan Language Meta-Package for LibreOffice</nl_BE>
   <nl>Occitan Taal Meta-Pakket voor LibreOffice</nl>
   <or>Occitan Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Occitan dla LibreOffice</pl>
   <pt_BR>Occitana Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Occitano Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Occitan Language Meta-Package for LibreOffice</ro>
   <ru>Occitan Language Meta-Package for LibreOffice</ru>
   <sk>Occitan Language Meta-Package for LibreOffice</sk>
   <sl>Okitanski jezikovni meta-paket za LibreOffice</sl>
   <so>Occitan Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në oçitanisht</sq>
   <sr>Occitan Language Meta-Package for LibreOffice</sr>
   <sv>Occitan Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Occitan สำหรับ LibreOffice</th>
   <tr>LibreOffice için Oksitanca Dili Üst-Paketi</tr>
   <uk>Occitan Language Meta-Package for LibreOffice</uk>
   <vi>Occitan Language Meta-Package for LibreOffice</vi>
   <zh_CN>Occitan Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Occitan Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Occitan Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-oc
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-oc
libreoffice-gtk3
</uninstall_package_names>

</app>
