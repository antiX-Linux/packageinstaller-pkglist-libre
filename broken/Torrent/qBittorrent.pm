<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Torrent
</category>

<name>
Latest qBittorrent
</name>

<description>
   <am>a QT4 feature rich but lightweight client that is very similar to uTorrent</am>
   <ar>a QT4 feature rich but lightweight client that is very similar to uTorrent</ar>
   <be>a QT4 feature rich but lightweight client that is very similar to uTorrent</be>
   <bg>a QT4 feature rich but lightweight client that is very similar to uTorrent</bg>
   <bn>a QT4 feature rich but lightweight client that is very similar to uTorrent</bn>
   <ca>Un client potent però lleuger en QT4 molt semblant a uTorrent</ca>
   <cs>a QT4 feature rich but lightweight client that is very similar to uTorrent</cs>
   <da>En QT4 letvægts klient med mange funktionaliteter som minder meget om uTorrent</da>
   <de>Ein Qt4-Client mit vielen Funktionen, der dem µTorrent sehr ähnlich ist.</de>
   <el>Χαρακτηριστικά πλούσιο αλλά και ελαφρύ πρόγραμμα σε QT4, παρόμοιο με το uTorrent</el>
   <en>a QT4 feature rich but lightweight client that is very similar to uTorrent</en>
   <es_ES>Cliente completo y liviano muy similar a uTorrent que utiliza QT4</es_ES>
   <es>Cliente completo y liviano muy similar a uTorrent que utiliza QT4</es>
   <et>a QT4 feature rich but lightweight client that is very similar to uTorrent</et>
   <eu>a QT4 feature rich but lightweight client that is very similar to uTorrent</eu>
   <fa>a QT4 feature rich but lightweight client that is very similar to uTorrent</fa>
   <fil_PH>a QT4 feature rich but lightweight client that is very similar to uTorrent</fil_PH>
   <fi>QT4-pohjainen monikäyttöinen mutta kevyt asiakasohjelma joka on hyvin samankaltainen kuin uTorrent</fi>
   <fr_BE>Un client léger et complet basé sur QT4 et très similaire à uTorrent</fr_BE>
   <fr>Un client léger et complet basé sur QT4 et très similaire à uTorrent</fr>
   <gl_ES>Cliente de torrent para a tecnoloxía QT4, cheo de funcionalidades mas lixeiro, moi semellante ao uTorrent</gl_ES>
   <gu>a QT4 feature rich but lightweight client that is very similar to uTorrent</gu>
   <he_IL>a QT4 feature rich but lightweight client that is very similar to uTorrent</he_IL>
   <hi>uTorrent समान क्यूटी4 विशेषताओं से युक्त परन्तु सरल साधन</hi>
   <hr>a QT4 feature rich but lightweight client that is very similar to uTorrent</hr>
   <hu>a QT4 feature rich but lightweight client that is very similar to uTorrent</hu>
   <id>a QT4 feature rich but lightweight client that is very similar to uTorrent</id>
   <is>a QT4 feature rich but lightweight client that is very similar to uTorrent</is>
   <it>client molto simile a uTorrent, ricco di funzionalità ma leggero, con interfaccia in QT4</it>
   <ja>uTorrent に非常に似て QT4 の機能が豊富で軽量なクライアント</ja>
   <kk>a QT4 feature rich but lightweight client that is very similar to uTorrent</kk>
   <ko>a QT4 feature rich but lightweight client that is very similar to uTorrent</ko>
   <ku>a QT4 feature rich but lightweight client that is very similar to uTorrent</ku>
   <lt>a QT4 feature rich but lightweight client that is very similar to uTorrent</lt>
   <mk>a QT4 feature rich but lightweight client that is very similar to uTorrent</mk>
   <mr>a QT4 feature rich but lightweight client that is very similar to uTorrent</mr>
   <nb_NO>a QT4 feature rich but lightweight client that is very similar to uTorrent</nb_NO>
   <nb>Qt 4-basert BitTorrent-klient, lettvektig, men med mye funksjonalitet</nb>
   <nl_BE>a QT4 feature rich but lightweight client that is very similar to uTorrent</nl_BE>
   <nl>een QT4 veelzijdig maar lichtgewicht programma die veel gelijkenis vertoont met uTorrent</nl>
   <or>a QT4 feature rich but lightweight client that is very similar to uTorrent</or>
   <pl>lekki i funkcjonalny klient oparty o QT4, bardzo podobny do uTorrent</pl>
   <pt_BR>Cliente de torrent para a tecnologia QT4, repleto de funcionalidades, leve e rápido, semelhante ao uTorrent</pt_BR>
   <pt>Cliente de torrent para a tecnologia QT4, pleno de funcionalidades mas ligeiro, muito semelhante ao uTorrent</pt>
   <ro>a QT4 feature rich but lightweight client that is very similar to uTorrent</ro>
   <ru>Мощный и легкий Bittorrent клиент на QT-4 похожий на uTorrent</ru>
   <sk>a QT4 feature rich but lightweight client that is very similar to uTorrent</sk>
   <sl>QT4 torrent odjemalec, ki je zelo podoben uTorrentu</sl>
   <so>a QT4 feature rich but lightweight client that is very similar to uTorrent</so>
   <sq>Një klient plot veçori, por i peshës së lehtë, bazuar në QT4, që është shumë i ngjashëm me uTorrent</sq>
   <sr>a QT4 feature rich but lightweight client that is very similar to uTorrent</sr>
   <sv>en Qt4 lättvikts klient rik på möjligheter som är mycket lik uTorrent</sv>
   <th>a QT4 feature rich but lightweight client that is very similar to uTorrent</th>
   <tr>uTorrent'e çok benzeyen QT4 özelliği zengin ancak hafif bir istemci</tr>
   <uk>a QT4 feature rich but lightweight client that is very similar to uTorrent</uk>
   <vi>a QT4 feature rich but lightweight client that is very similar to uTorrent</vi>
   <zh_CN>a QT4 feature rich but lightweight client that is very similar to uTorrent</zh_CN>
   <zh_HK>a QT4 feature rich but lightweight client that is very similar to uTorrent</zh_HK>
   <zh_TW>a QT4 feature rich but lightweight client that is very similar to uTorrent</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E1C726CD
echo "deb http://mxrepo.com/mx/repo/ bullseye main non-free">/etc/apt/sources.list.d/pitemp1.list
sleep .1
echo "deb http://mxrepo.com/mx/testrepo/ bullseye test ">/etc/apt/sources.list.d/pitemp.list
apt-get update
</preinstall>

<install_package_names>
-t mx qbittorrent
</install_package_names>


<postinstall>
rm /etc/apt/sources.list.d/pitemp.list
rm /etc/apt/sources.list.d/pitemp1.list
apt-get update
</postinstall>


<uninstall_package_names>
qbittorrent
</uninstall_package_names>
</app>
