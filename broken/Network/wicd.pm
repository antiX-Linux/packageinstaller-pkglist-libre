<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
wicd
</name>

<description>
   <am>Lightweight general-purpose network configuration server</am>
   <ar>Lightweight general-purpose network configuration server</ar>
   <be>Lightweight general-purpose network configuration server</be>
   <bg>Lightweight general-purpose network configuration server</bg>
   <bn>Lightweight general-purpose network configuration server</bn>
   <ca>Servidor de configuració de xarxa lleuger i de propòsit general</ca>
   <cs>Lightweight general-purpose network configuration server</cs>
   <da>Lightweight general-purpose network configuration server</da>
   <de>Lightweight general-purpose network configuration server</de>
   <el>Ελαφρύς διακομιστής διαμόρφωσης δικτύου γενικής χρήσης</el>
   <en>Lightweight general-purpose network configuration server</en>
   <es_ES>Servidor de configuración de red ligero y de uso general</es_ES>
   <es>Servidor de configuración de red ligero y de uso general</es>
   <et>Lightweight general-purpose network configuration server</et>
   <eu>Lightweight general-purpose network configuration server</eu>
   <fa>Lightweight general-purpose network configuration server</fa>
   <fil_PH>Lightweight general-purpose network configuration server</fil_PH>
   <fi>Lightweight general-purpose network configuration server</fi>
   <fr_BE>Serveur de configuration réseau léger et polyvalent</fr_BE>
   <fr>Serveur de configuration réseau léger et polyvalent</fr>
   <gl_ES>Lightweight general-purpose network configuration server</gl_ES>
   <gu>Lightweight general-purpose network configuration server</gu>
   <he_IL>Lightweight general-purpose network configuration server</he_IL>
   <hi>Lightweight general-purpose network configuration server</hi>
   <hr>Lightweight general-purpose network configuration server</hr>
   <hu>Lightweight general-purpose network configuration server</hu>
   <id>Lightweight general-purpose network configuration server</id>
   <is>Lightweight general-purpose network configuration server</is>
   <it>Server leggero di configurazione di rete per uso generico</it>
   <ja>Lightweight general-purpose network configuration server</ja>
   <kk>Lightweight general-purpose network configuration server</kk>
   <ko>Lightweight general-purpose network configuration server</ko>
   <ku>Lightweight general-purpose network configuration server</ku>
   <lt>Lightweight general-purpose network configuration server</lt>
   <mk>Lightweight general-purpose network configuration server</mk>
   <mr>Lightweight general-purpose network configuration server</mr>
   <nb_NO>Lightweight general-purpose network configuration server</nb_NO>
   <nb>Lettvektig tjener for generelt nettverksoppsett</nb>
   <nl_BE>Lightweight general-purpose network configuration server</nl_BE>
   <nl>Lightweight general-purpose network configuration server</nl>
   <or>Lightweight general-purpose network configuration server</or>
   <pl>Lightweight general-purpose network configuration server</pl>
   <pt_BR>Servidor de configuração de rede leve de uso geral</pt_BR>
   <pt>Servidor de configuração de rede ligeiro e de uso geral</pt>
   <ro>Lightweight general-purpose network configuration server</ro>
   <ru>Lightweight general-purpose network configuration server</ru>
   <sk>Lightweight general-purpose network configuration server</sk>
   <sl>Laheknastavitveni strežnik omrežja za splošne namene</sl>
   <so>Lightweight general-purpose network configuration server</so>
   <sq>Lightweight general-purpose network configuration server</sq>
   <sr>Lightweight general-purpose network configuration server</sr>
   <sv>Lättvikts nätverkskonfigurations-server för alla ändamål</sv>
   <th>Lightweight general-purpose network configuration server</th>
   <tr>Lightweight general-purpose network configuration server</tr>
   <uk>Lightweight general-purpose network configuration server</uk>
   <vi>Lightweight general-purpose network configuration server</vi>
   <zh_CN>Lightweight general-purpose network configuration server</zh_CN>
   <zh_HK>Lightweight general-purpose network configuration server</zh_HK>
   <zh_TW>Lightweight general-purpose network configuration server</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
wicd
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
wicd
</uninstall_package_names>
</app>
