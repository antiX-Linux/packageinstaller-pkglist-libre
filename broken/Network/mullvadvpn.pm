<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>
Mullvad VPN
</name>

<description>
   <am>Mullvad VPN and sysVinit scripts</am>
   <ar>Mullvad VPN and sysVinit scripts</ar>
   <be>Mullvad VPN and sysVinit scripts</be>
   <bg>Mullvad VPN and sysVinit scripts</bg>
   <bn>Mullvad VPN and sysVinit scripts</bn>
   <ca>Scripts Mullvad VPN i sysVinit</ca>
   <cs>Mullvad VPN and sysVinit scripts</cs>
   <da>Mullvad VPN and sysVinit scripts</da>
   <de>Mullvad VPN und sysVinit Scripte</de>
   <el>Σενάρια του Mullvad VPN και sysVinit</el>
   <en>Mullvad VPN and sysVinit scripts</en>
   <es_ES>Mullvad VPN y scripts sysVinit</es_ES>
   <es>Mullvad VPN y scripts sysVinit</es>
   <et>Mullvad VPN and sysVinit scripts</et>
   <eu>Mullvad VPN and sysVinit scripts</eu>
   <fa>Mullvad VPN and sysVinit scripts</fa>
   <fil_PH>Mullvad VPN and sysVinit scripts</fil_PH>
   <fi>Mullvad VPN ja sysVinit script-ohjelmakoodisyötteet</fi>
   <fr_BE>Scripts pour Mullvad VPN et sysVinit</fr_BE>
   <fr>Scripts pour Mullvad VPN et sysVinit</fr>
   <gl_ES>Scripts para sysVinit e VPN Mullvad</gl_ES>
   <gu>Mullvad VPN and sysVinit scripts</gu>
   <he_IL>Mullvad VPN and sysVinit scripts</he_IL>
   <hi>Mullvad वीपीएन व sysVinit स्क्रिप्ट</hi>
   <hr>Mullvad VPN and sysVinit scripts</hr>
   <hu>Mullvad VPN and sysVinit scripts</hu>
   <id>Mullvad VPN and sysVinit scripts</id>
   <is>Mullvad VPN and sysVinit scripts</is>
   <it>Mullvad VPN e script sysVinit</it>
   <ja>Mullvad VPN と sysVinit スクリプト</ja>
   <kk>Mullvad VPN and sysVinit scripts</kk>
   <ko>Mullvad VPN and sysVinit scripts</ko>
   <ku>Mullvad VPN and sysVinit scripts</ku>
   <lt>Mullvad VPN and sysVinit scripts</lt>
   <mk>Mullvad VPN and sysVinit scripts</mk>
   <mr>Mullvad VPN and sysVinit scripts</mr>
   <nb_NO>Mullvad VPN and sysVinit scripts</nb_NO>
   <nb>Mullvad VPN- og sysVinit-skript</nb>
   <nl_BE>Mullvad VPN and sysVinit scripts</nl_BE>
   <nl>Mullvad VPN en sysVinit scripts</nl>
   <or>Mullvad VPN and sysVinit scripts</or>
   <pl>Mullvad VPN and sysVinit scripts</pl>
   <pt_BR>Scripts para sysVinit e Mullvad VPN</pt_BR>
   <pt>Scripts para sysVinit e VPN Mullvad</pt>
   <ro>Mullvad VPN and sysVinit scripts</ro>
   <ru>VPN и SysVinit - скрипты от Mullvad</ru>
   <sk>Mullvad VPN and sysVinit scripts</sk>
   <sl>Mullvad VPN in syVinit skripte</sl>
   <so>Mullvad VPN and sysVinit scripts</so>
   <sq>Mullvad VPN and sysVinit scripts</sq>
   <sr>Mullvad VPN and sysVinit scripts</sr>
   <sv>Mullvad VPN och sysVinit skript</sv>
   <th>Mullvad VPN และสคริปต์ sysVinit</th>
   <tr>Mullvad VPN ve sysVinit betikleri</tr>
   <uk>Mullvad VPN and sysVinit scripts</uk>
   <vi>Mullvad VPN and sysVinit scripts</vi>
   <zh_CN>Mullvad VPN and sysVinit scripts</zh_CN>
   <zh_HK>Mullvad VPN and sysVinit scripts</zh_HK>
   <zh_TW>Mullvad VPN and sysVinit scripts</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
mullvadvpn-sysvinit-compat
mullvadvpn-downloader-installer
</install_package_names>

<postinstall>
# run Mullvad installer
INSTALL=/usr/share/mullvad-downloader-installer/install_mullvadvpn.sh
if [ -x $INSTALL ]; then
/usr/share/mullvad-downloader-installer/install_mullvadvpn.sh
fi

</postinstall>

<uninstall_package_names>
mullvadvpn-sysvinit-compat
mullvadvpn-downloader-installer
mullvad-vpn
</uninstall_package_names>
</app>
