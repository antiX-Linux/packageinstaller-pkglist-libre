<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Kernel
</category>

<name>
Kernel-antiX_64bit_legacy_LTS_meltdown-patched
</name>

<description>
   <am>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</am>
   <ar>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</ar>
   <be>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</be>
   <bg>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</bg>
   <bn>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</bn>
   <ca>kernel antiX  64 bit LTS (4.4.0-296) apedaçat per Meltdown, Spectre i Foreshadow</ca>
   <cs>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</cs>
   <da>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</da>
   <de>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</de>
   <el>antiX πυρήνα 64 bit Meltdown, Spectre  και Foreshadow patched LTS (4.4.0-296)</el>
   <en>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</en>
   <es_ES>Kernel antiX 64 bit Meltdown, Spectre y Foreshadow parcheados LTS (4.4.0-296)</es_ES>
   <es>Kernel antiX 64 bit Meltdown, Spectre y Foreshadow parcheado LTS (4.4.0-296)</es>
   <et>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</et>
   <eu>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</eu>
   <fa>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</fa>
   <fil_PH>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</fil_PH>
   <fi>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</fi>
   <fr_BE>antiX Noyau 64 bit Meltdown, Spectre and Foreshadow patché LTS (4.4.0-296)</fr_BE>
   <fr>antiX Noyau 64 bit Meltdown, Spectre and Foreshadow patché LTS (4.4.0-296)</fr>
   <gl_ES>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</gl_ES>
   <gu>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</gu>
   <he_IL>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</he_IL>
   <hi>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</hi>
   <hr>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</hr>
   <hu>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</hu>
   <id>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</id>
   <is>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</is>
   <it>Kernel antiX LTS 64 bit con patch Meltdown, Spectre e Foreshadow (4.4.0-296)</it>
   <ja>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</ja>
   <kk>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</kk>
   <ko>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</ko>
   <ku>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</ku>
   <lt>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</lt>
   <mk>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</mk>
   <mr>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</mr>
   <nb_NO>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</nb_NO>
   <nb>antiX-kjerne, 64 biters Meltdown, Spectre og Foreshadow-fikset, langtidsstøttet (4.4.0-296)</nb>
   <nl_BE>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</nl_BE>
   <nl>antiX Kernel 64 bit Meltdown, Spectre en Foreshadow patched LTS (4.4.0-296)</nl>
   <or>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</or>
   <pl>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</pl>
   <pt_BR>Núcleo (Kernel) do antiX 4.4.0-296 de 64 bits LTS com as correções para Meltdown, Spectre e Foreshadow</pt_BR>
   <pt>Núcleo (Kernel) do antiX de 64 bits LTS (4.4.0-296) com correcções para Meltdown, Spectre e Foreshadow</pt>
   <ro>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</ro>
   <ru>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</ru>
   <sk>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</sk>
   <sl>antiX 64 bitno jedro  Meltdown, Spectre and Foreshadow zakrpano z dolgoročno podporo (4.4.0-296)</sl>
   <so>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</so>
   <sq>Kernel antiX-i LTS, 64 bit, arnuar për Meltdown, Spectre dhe Foreshadow (4.4.0-296)</sq>
   <sr>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</sr>
   <sv>antiX Kärna 64 bit Meltdown, Spectre och Foreshadow fixad LTS (4.4.0-296)</sv>
   <th>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</th>
   <tr>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</tr>
   <uk>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</uk>
   <vi>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</vi>
   <zh_CN>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</zh_CN>
   <zh_HK>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</zh_HK>
   <zh_TW>antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.4.0-296)</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
linux-image-4.4.0-296-antix.1-amd64-smp
linux-headers-4.4.0-296-antix.1-amd64-smp
libelf-dev
libc6-dev
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-4.4.0-296-antix.1-amd64-smp
linux-headers-4.4.0-296-antix.1-amd64-smp
</uninstall_package_names>

</app>
