<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Kernel
</category>

<name>
Kernel-antiX_32bit_pae_meltdown_patched
</name>

<description>
   <am>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</am>
   <ar>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</ar>
   <be>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</be>
   <bg>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</bg>
   <bn>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</bn>
   <ca>Kernel antiX 32 bit LTS (4.9.0-326-486) apedaçat per Meltdown, Spectre i Foreshadow</ca>
   <cs>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</cs>
   <da>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</da>
   <de>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</de>
   <el>antiX πυρήνα 32 bit Meltdown, Spectre  και Foreshadow patched LTS (4.9.326-686-pae)</el>
   <en>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</en>
   <es_ES>Kernel antiX 32 bit Meltdown, Spectre y Foreshadow parcheados LTS (4.9.0-326-686-pae)</es_ES>
   <es>Kernel antiX 32 bit Meltdown, Spectre y Foreshadow parcheado LTS (4.9.0-326-686-pae)</es>
   <et>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</et>
   <eu>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</eu>
   <fa>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</fa>
   <fil_PH>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</fil_PH>
   <fi>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</fi>
   <fr_BE>antiX Noyau 32 bit Meltdown, Spectre and Foreshadow patché LTS (4.9.0-326-686-pae)</fr_BE>
   <fr>antiX Noyau 32 bit Meltdown, Spectre and Foreshadow patché LTS (4.9.0-326-686-pae)</fr>
   <gl_ES>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</gl_ES>
   <gu>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</gu>
   <he_IL>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</he_IL>
   <hi>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</hi>
   <hr>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</hr>
   <hu>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</hu>
   <id>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</id>
   <is>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</is>
   <it>Kernel antiX LTS 32 bit con patch Meltdown, Spectre e Foreshadow (4.9.0-326-686-pae)</it>
   <ja>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</ja>
   <kk>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</kk>
   <ko>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</ko>
   <ku>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</ku>
   <lt>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</lt>
   <mk>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</mk>
   <mr>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</mr>
   <nb_NO>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</nb_NO>
   <nb>antiX-kjerne, 32 biters Meltdown, Spectre og Foreshadow-fikset, langtidsstøttet (4.9.0-326-686-pae)</nb>
   <nl_BE>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</nl_BE>
   <nl>antiX Kernel 32 bit Meltdown, Spectre en Foreshadow gepatchte LTS (4.9.0-326-686-pae)</nl>
   <or>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</or>
   <pl>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</pl>
   <pt_BR>Núcleo (Kernel) do antiX 4.9.0-326-686-pae de 32 bits com as correções para Meltdown, Spectre e Foreshadow</pt_BR>
   <pt>Núcleo (Kernel) do antiX de 32 bits LTS (4.9.0-326-686-pae) com correcções para Meltdown, Spectre e Foreshadow</pt>
   <ro>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</ro>
   <ru>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</ru>
   <sk>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</sk>
   <sl>antiX 32 bitno jedro  Meltdown, Spectre and Foreshadow zakrpano z dolgoročno podporo (4.9.0-326-686-pae)</sl>
   <so>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</so>
   <sq>Kernel antiX-i LTS, 32 bit, arnuar për Meltdown, Spectre dhe Foreshadow (4.9.0-326-686-pae)</sq>
   <sr>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</sr>
   <sv>antiX Kärna 32 bit Meltdown, Spectre och Foreshadow fixad LTS (4.9.0-326-686-pae)</sv>
   <th>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</th>
   <tr>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</tr>
   <uk>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</uk>
   <vi>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</vi>
   <zh_CN>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</zh_CN>
   <zh_HK>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</zh_HK>
   <zh_TW>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-686-pae)</zh_TW>
</description>

<installable>
32
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
linux-image-4.9.0-326-antix.1-686-smp-pae
linux-headers-4.9.0-326-antix.1-686-smp-pae
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-4.9.0-326-antix.1-686-smp-pae
linux-headers-4.9.0-326-antix.1-686-smp-pae
</uninstall_package_names>

</app>
