<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Kernel
</category>

<name>
Kernel-antiX_32bit_meltdown_patched
</name>

<description>
   <am>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</am>
   <ar>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</ar>
   <be>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</be>
   <bg>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</bg>
   <bn>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</bn>
   <ca>Kernel antiX bit LTS (4.9.0-326-486) apedaçat per Meltdown, Spectre i Foreshadow</ca>
   <cs>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</cs>
   <da>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</da>
   <de>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</de>
   <el>antiX πυρήνα 32 bit Meltdown, Spectre  και Foreshadow patched LTS (4.9.0-326-486)</el>
   <en>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</en>
   <es_ES>Kernel antiX 32 bit Meltdown, Spectre y Foreshadow parcheados LTS (4.9.0-326-486)</es_ES>
   <es>Kernel antiX 32 bit Meltdown, Spectre y Foreshadow parcheado LTS (4.9.0-326-486)</es>
   <et>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</et>
   <eu>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</eu>
   <fa>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</fa>
   <fil_PH>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</fil_PH>
   <fi>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</fi>
   <fr_BE>antiX Noyau 32 bit Meltdown, Spectre and Foreshadow patché LTS (4.9.0-326-486)</fr_BE>
   <fr>antiX Noyau 32 bit Meltdown, Spectre and Foreshadow patché LTS (4.9.0-326-486)</fr>
   <gl_ES>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</gl_ES>
   <gu>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</gu>
   <he_IL>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</he_IL>
   <hi>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</hi>
   <hr>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</hr>
   <hu>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</hu>
   <id>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</id>
   <is>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</is>
   <it>Kernel antiX LTS 32 bit con patch Meltdown, Spectre e Foreshadow (4.9.0-326-486)</it>
   <ja>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</ja>
   <kk>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</kk>
   <ko>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</ko>
   <ku>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</ku>
   <lt>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</lt>
   <mk>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</mk>
   <mr>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</mr>
   <nb_NO>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</nb_NO>
   <nb>antiX-kjerne, 32 biters Meltdown, Spectre og Foreshadow-fikset, langtidsstøttet (4.9.0-326-486)</nb>
   <nl_BE>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</nl_BE>
   <nl>antiX Kernel 32 bit Meltdown, Spectre en Foreshadow patched LTS (4.9.0-326-486)</nl>
   <or>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</or>
   <pl>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</pl>
   <pt_BR>Núcleo (Kernel) do antiX 4.9.0-326-486 de 32 bits LTS com as correções para Meltdown, Spectre e Foreshadow</pt_BR>
   <pt>Núcleo (Kernel) do antiX de 32 bits LTS (4.9.0-326-486) com correcções para Meltdown, Spectre e Foreshadow</pt>
   <ro>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</ro>
   <ru>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</ru>
   <sk>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</sk>
   <sl>antiX 32 bitno jedro  Meltdown, Spectre and Foreshadow zakrpano z dolgoročno podporo (4.9.0-326-486)</sl>
   <so>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</so>
   <sq>Kernel antiX-i LTS, 32 bit, arnuar për Meltdown, Spectre dhe Foreshadow (4.9.0-326-486)</sq>
   <sr>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</sr>
   <sv>antiX Kärna 32 bit Meltdown, Spectre och Foreshadow fixad LTS (4.9.0-326-486)</sv>
   <th>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</th>
   <tr>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</tr>
   <uk>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</uk>
   <vi>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</vi>
   <zh_CN>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</zh_CN>
   <zh_HK>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</zh_HK>
   <zh_TW>antiX Kernel 32 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.0-326-486)</zh_TW>
</description>

<installable>
32
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
linux-image-4.9.0-326-antix.1-486-smp
linux-headers-4.9.0-326-antix.1-486-smp
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-4.9.0-326-antix.1-486-smp
linux-headers-4.9.0-326-antix.1-486-smp
</uninstall_package_names>

</app>
