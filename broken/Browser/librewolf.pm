<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
LibreWolf
</name>

<description>
   <am>Latest LibreWolf browser</am>
   <ar>Latest LibreWolf browser</ar>
   <bg>Latest LibreWolf browser</bg>
   <bn>Latest LibreWolf browser</bn>
   <ca>Darrer navegador LibreWolf (estable)</ca>
   <cs>Latest LibreWolf browser</cs>
   <da>Seneste LibreWolf-browser (stabil)</da>
   <de>Aktueller LibreWolf-Browser (stabil)</de>
   <el>Τελευταίο πρόγραμμα περιήγησης LibreWolf (σταθερό)</el>
   <en>Latest LibreWolf browser</en>
   <es>El último LibreWolf (estable)</es>
   <et>Latest LibreWolf browser</et>
   <eu>Latest LibreWolf browser</eu>
   <fa>Latest LibreWolf browser</fa>
   <fil_PH>Latest LibreWolf browser</fil_PH>
   <fi>Latest LibreWolf browser</fi>
   <fr>La dernière version de LibreWolf</fr>
   <he_IL>Latest LibreWolf browser</he_IL>
   <hi>Latest LibreWolf browser</hi>
   <hr>Latest LibreWolf browser</hr>
   <hu>Latest LibreWolf browser</hu>
   <id>Latest LibreWolf browser</id>
   <is>Latest LibreWolf browser</is>
   <it>Ultima versione (stabile) del browser LibreWolf</it>
   <ja_JP>Latest LibreWolf browser</ja_JP>
   <ja>Latest LibreWolf browser</ja>
   <kk>Latest LibreWolf browser</kk>
   <ko>Latest LibreWolf browser</ko>
   <lt>Latest LibreWolf browser</lt>
   <mk>Latest LibreWolf browser</mk>
   <mr>Latest LibreWolf browser</mr>
   <nb>Latest LibreWolf browser</nb>
   <nl>Meest recente LibreWolf browser</nl>
   <pl>najnowsza przeglądarka LibreWolf (stabilna)</pl>
   <pt_BR>Versão mais recente (estável) do navegador web LibreWolf</pt_BR>
   <pt>Versão mais recente (estável) do navegador web LibreWolf</pt>
   <ro>Latest LibreWolf browser</ro>
   <ru>Браузер LibreWolf (последняя стабильная версия)</ru>
   <sk>Posledný LibreWolf prehliadač (stabilný)</sk>
   <sl>Zadnja različica googlovega LibreWolf brskalnika</sl>
   <sq>Latest LibreWolf browser</sq>
   <sr>Latest LibreWolf browser</sr>
   <sv>SenasteLibreWolf webbläsare</sv>
   <tr>Latest LibreWolf browser</tr>
   <uk>Крайня стабільна версія браузера LibreWolf</uk>
   <vi>Latest LibreWolf browser</vi>
   <zh_CN>Latest LibreWolf browser</zh_CN>
   <zh_TW>Latest LibreWolf browser</zh_TW>
</description>

<installable>
 64
</installable>

<screenshot></screenshot>

<preinstall>
wget https://deb.librewolf.net/keyring.gpg -O /etc/apt/trusted.gpg.d/librewolf.gpg
echo "deb [arch=amd64] http://deb.librewolf.net bookworm main">/etc/apt/sources.list.d/librewolf.list
apt-get update
</preinstall>

<install_package_names>
librewolf
</install_package_names>


<postinstall>

</postinstall>

<uninstall_package_names>
librewolf
</uninstall_package_names>

<postuninstall>

</postuninstall>
</app>
