<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Ungoogled Chromium
</name>

<description>
   <am>Latest Ungoogled Chromium browser and language pack</am>
   <ar>Latest Ungoogled Chromium browser and language pack</ar>
   <be>Latest Ungoogled Chromium browser and language pack</be>
   <bg>Latest Ungoogled Chromium browser and language pack</bg>
   <bn>Latest Ungoogled Chromium browser and language pack</bn>
   <ca>Darrer navegador Chromium 'des-googlat' i paquet de llengua</ca>
   <cs>Latest Ungoogled Chromium browser and language pack</cs>
   <da>Latest Ungoogled Chromium browser and language pack</da>
   <de>Latest Ungoogled Chromium browser and language pack</de>
   <el>Το τελευταίο πρόγραμμα περιήγησης Ungoogled Chromium και το πακέτο γλωσσών του</el>
   <en>Latest Ungoogled Chromium browser and language pack</en>
   <es_ES>Último navegador Ungoogled Chromium y paquete de idiomas</es_ES>
   <es>Último navegador Ungoogled Chromium y paquete de idiomas</es>
   <et>Latest Ungoogled Chromium browser and language pack</et>
   <eu>Latest Ungoogled Chromium browser and language pack</eu>
   <fa>Latest Ungoogled Chromium browser and language pack</fa>
   <fil_PH>Latest Ungoogled Chromium browser and language pack</fil_PH>
   <fi>Latest Ungoogled Chromium browser and language pack</fi>
   <fr_BE>Dernier navigateur Chromium non googlé et son pack de langue</fr_BE>
   <fr>Dernier navigateur Chromium non googlé et son pack de langue</fr>
   <gl_ES>Latest Ungoogled Chromium browser and language pack</gl_ES>
   <gu>Latest Ungoogled Chromium browser and language pack</gu>
   <he_IL>Latest Ungoogled Chromium browser and language pack</he_IL>
   <hi>Latest Ungoogled Chromium browser and language pack</hi>
   <hr>Latest Ungoogled Chromium browser and language pack</hr>
   <hu>Latest Ungoogled Chromium browser and language pack</hu>
   <id>Latest Ungoogled Chromium browser and language pack</id>
   <is>Latest Ungoogled Chromium browser and language pack</is>
   <it>Ultimi browser e pacchetto localizzazione di Chromium senza Google</it>
   <ja>Latest Ungoogled Chromium browser and language pack</ja>
   <kk>Latest Ungoogled Chromium browser and language pack</kk>
   <ko>Latest Ungoogled Chromium browser and language pack</ko>
   <ku>Latest Ungoogled Chromium browser and language pack</ku>
   <lt>Latest Ungoogled Chromium browser and language pack</lt>
   <mk>Latest Ungoogled Chromium browser and language pack</mk>
   <mr>Latest Ungoogled Chromium browser and language pack</mr>
   <nb_NO>Latest Ungoogled Chromium browser and language pack</nb_NO>
   <nb>Chromium (uten Google) og språkpakke, seneste utgivelse</nb>
   <nl_BE>Latest Ungoogled Chromium browser and language pack</nl_BE>
   <nl>Latest Ungoogled Chromium browser and language pack</nl>
   <or>Latest Ungoogled Chromium browser and language pack</or>
   <pl>Latest Ungoogled Chromium browser and language pack</pl>
   <pt_BR>Ungoogled Chromium - Versão mais recente do navegador de internet com o pacote de idiomas</pt_BR>
   <pt>Versão mais recente do navegador Chromium Ungoogled e respectivo pacote de idiomas</pt>
   <ro>Latest Ungoogled Chromium browser and language pack</ro>
   <ru>Latest Ungoogled Chromium browser and language pack</ru>
   <sk>Latest Ungoogled Chromium browser and language pack</sk>
   <sl>Zadnja različica jezikovnega paketa za brskalnik Chromium</sl>
   <so>Latest Ungoogled Chromium browser and language pack</so>
   <sq>Përkthimi i Firefox-it ESR në sorbishte të sipërme</sq>
   <sr>Latest Ungoogled Chromium browser and language pack</sr>
   <sv>Senaste Ungoogled Chromium webbläsare med språkpaket</sv>
   <th>Latest Ungoogled Chromium browser and language pack</th>
   <tr>Latest Ungoogled Chromium browser and language pack</tr>
   <uk>Latest Ungoogled Chromium browser and language pack</uk>
   <vi>Latest Ungoogled Chromium browser and language pack</vi>
   <zh_CN>Latest Ungoogled Chromium browser and language pack</zh_CN>
   <zh_HK>Latest Ungoogled Chromium browser and language pack</zh_HK>
   <zh_TW>Latest Ungoogled Chromium browser and language pack</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
ungoogled-chromium
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
ungoogled-chromium
</uninstall_package_names>
</app>
