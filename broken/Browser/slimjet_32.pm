<?xml version="1.0"?>
<app>

<category>
Browser
</category>

<name>
Slimjet
</name>

<description>
   <am>Latest Slimjet Browser (run again for updates)</am>
   <ar>Latest Slimjet Browser (run again for updates)</ar>
   <be>Latest Slimjet Browser (run again for updates)</be>
   <bg>Latest Slimjet Browser (run again for updates)</bg>
   <bn>Latest Slimjet Browser (run again for updates)</bn>
   <ca>Darrer navegador Slimjet (torneu a executar-lo per actualitzacions)</ca>
   <cs>Latest Slimjet Browser (run again for updates)</cs>
   <da>Seneste Slimjet-browser (kør igen for opdateringer)</da>
   <de>Neuester Slimjet Browser (zur Aktualisierung erneut ausführen)</de>
   <el>Το τελευταίο πρόγραμμα περιήγησης Slimjet (εκτελείται ξανά για ενημερώσεις)</el>
   <en>Latest Slimjet Browser (run again for updates)</en>
   <es_ES>El Último Navegador Slimjet (ejecutar de nuevo para las actualizaciones)</es_ES>
   <es>El Último Navegador Slimjet (ejecutar de nuevo para las actualizaciones)</es>
   <et>Latest Slimjet Browser (run again for updates)</et>
   <eu>Latest Slimjet Browser (run again for updates)</eu>
   <fa>Latest Slimjet Browser (run again for updates)</fa>
   <fil_PH>Latest Slimjet Browser (run again for updates)</fil_PH>
   <fi>Viimeisin Slimjet-selain (aja uudelleen päivitystentarkistusta varten)</fi>
   <fr_BE>La dernière version du navigateur Slimjet (relancer pour mise à jour)</fr_BE>
   <fr>La dernière version du navigateur Slimjet (relancer pour mise à jour)</fr>
   <gl_ES>Navegador web Slimjet, versión máis recente (para obter actualizacións, se disponibles, volver a executar)</gl_ES>
   <gu>Latest Slimjet Browser (run again for updates)</gu>
   <he_IL>Latest Slimjet Browser (run again for updates)</he_IL>
   <hi>नवीनतम स्लिमजेट ब्राउज़र (अपडेट हेतु पुनः आरंभ करें)</hi>
   <hr>Latest Slimjet Browser (run again for updates)</hr>
   <hu>Latest Slimjet Browser (run again for updates)</hu>
   <id>Latest Slimjet Browser (run again for updates)</id>
   <is>Latest Slimjet Browser (run again for updates)</is>
   <it>Ultima versione del browser Slimjet (avvia nuovamente per gli aggiornamenti)</it>
   <ja>最新の Slimjet ブラウザ (アップデートのため再度実行してください)</ja>
   <kk>Latest Slimjet Browser (run again for updates)</kk>
   <ko>Latest Slimjet Browser (run again for updates)</ko>
   <ku>Latest Slimjet Browser (run again for updates)</ku>
   <lt>Latest Slimjet Browser (run again for updates)</lt>
   <mk>Latest Slimjet Browser (run again for updates)</mk>
   <mr>Latest Slimjet Browser (run again for updates)</mr>
   <nb_NO>Latest Slimjet Browser (run again for updates)</nb_NO>
   <nb>Seneste Slimjet-nettleser (kjør igjen for oppdateringer)</nb>
   <nl_BE>Latest Slimjet Browser (run again for updates)</nl_BE>
   <nl>Meest recente Slimjet Browser (nogmaals uitvoeren voor updates)</nl>
   <or>Latest Slimjet Browser (run again for updates)</or>
   <pl>najnowsza przeglądarka Slimjet (uruchom ponownie w celu aktualizacji)</pl>
   <pt_BR>Slimjet - Versão mais recente do navegador de internet (execute novamente para obter as atualizações disponíveis)</pt_BR>
   <pt>Navegador web Slimjet, versão mais recente (para obter actualizações, se disponíveis, voltar a executar)</pt>
   <ro>Latest Slimjet Browser (run again for updates)</ro>
   <ru>Браузер Slimjet последней версии (перезапустите для установки обновлений)</ru>
   <sk>Latest Slimjet Browser (run again for updates)</sk>
   <sl>Zadnja različica Slimjet brskalnika</sl>
   <so>Latest Slimjet Browser (run again for updates)</so>
   <sq>Shfletuesi Më i Ri Slimjet (xhirojeni sërish, për përditësime)</sq>
   <sr>Latest Slimjet Browser (run again for updates)</sr>
   <sv>Senaste Slimjet Webbläsare (kör igen för uppdateringar)</sv>
   <th>เบราว์เซอร์ Slimjet ล่าสุด (รันอีกครั้งเพื่ออัปเดต)</th>
   <tr>En son Slimjet Tarayıcı (güncellemeler için tekrar çalıştırın)</tr>
   <uk>Latest Slimjet Browser (run again for updates)</uk>
   <vi>Latest Slimjet Browser (run again for updates)</vi>
   <zh_CN>Latest Slimjet Browser (run again for updates)</zh_CN>
   <zh_HK>Latest Slimjet Browser (run again for updates)</zh_HK>
   <zh_TW>Latest Slimjet Browser (run again for updates)</zh_TW>
</description>

<installable>
32
</installable>

<screenshot>https://www.slimjet.com/en/images/newtab.jpg</screenshot>

<preinstall>
wget http://www.slimjet.com/release/slimjet_i386.deb
dpkg -i slimjet*.deb
apt-get -f install
</preinstall>

<install_package_names>

</install_package_names>


<postinstall>
rm slimjet*.deb
</postinstall>


<uninstall_package_names>
slimjet
</uninstall_package_names>
</app>
