<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Firefox
</name>

<description>
   <am>Latest Firefox</am>
   <ar>Latest Firefox</ar>
   <be>Latest Firefox</be>
   <bg>Latest Firefox</bg>
   <bn>Latest Firefox</bn>
   <ca>Darrer Firefox</ca>
   <cs>Latest Firefox</cs>
   <da>Seneste Firefox</da>
   <de>Neuester Firefox</de>
   <el>Το τελευταίο Firefox</el>
   <en>Latest Firefox</en>
   <es_ES>Ultimo Firefox</es_ES>
   <es>Firefox actual</es>
   <et>Latest Firefox</et>
   <eu>Latest Firefox</eu>
   <fa>Latest Firefox</fa>
   <fil_PH>Latest Firefox</fil_PH>
   <fi>Viimeisin Firefox-versio</fi>
   <fr_BE>La dernière version de Firefox</fr_BE>
   <fr>La dernière version de Firefox</fr>
   <gl_ES>Versión máis recente do Firefox</gl_ES>
   <gu>Latest Firefox</gu>
   <he_IL>Latest Firefox</he_IL>
   <hi>फायरफॉक्स का नवीनतम संस्करण</hi>
   <hr>Latest Firefox</hr>
   <hu>Latest Firefox</hu>
   <id>Latest Firefox</id>
   <is>Latest Firefox</is>
   <it>Ultima versione di Firefox</it>
   <ja>最新の Firefox</ja>
   <kk>Latest Firefox</kk>
   <ko>Latest Firefox</ko>
   <ku>Latest Firefox</ku>
   <lt>Naujausia Firefox</lt>
   <mk>Latest Firefox</mk>
   <mr>Latest Firefox</mr>
   <nb_NO>Latest Firefox</nb_NO>
   <nb>Seneste Firefox</nb>
   <nl_BE>Latest Firefox</nl_BE>
   <nl>Meest recente Firefox</nl>
   <or>Latest Firefox</or>
   <pl>najnowszy Firefox</pl>
   <pt_BR>Firefox - Versão mais recente do navegador de internet</pt_BR>
   <pt>Versão mais recente do Firefox</pt>
   <ro>Latest Firefox</ro>
   <ru>Браузер Firefox последней версии</ru>
   <sk>Posledný Firefox</sk>
   <sl>Zadnja različica Firefox-a</sl>
   <so>Latest Firefox</so>
   <sq>Firefox më i ri</sq>
   <sr>Latest Firefox</sr>
   <sv>Senaste Firefox</sv>
   <th>Firefox รุ่นล่าสุด</th>
   <tr>En son Firefox</tr>
   <uk>Крайній Firefox</uk>
   <vi>Latest Firefox</vi>
   <zh_CN>最新版 Firefox</zh_CN>
   <zh_HK>Latest Firefox</zh_HK>
   <zh_TW>Latest Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox
</uninstall_package_names>
</app>
