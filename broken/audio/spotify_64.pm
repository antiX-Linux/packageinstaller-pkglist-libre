<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>
Spotify
</name>

<description>
   <am>Spotify from Spotify Testing Repo</am>
   <ar>Spotify from Spotify Testing Repo</ar>
   <be>Spotify from Spotify Testing Repo</be>
   <bg>Spotify from Spotify Testing Repo</bg>
   <bn>Spotify from Spotify Testing Repo</bn>
   <ca>Spotify del dipòsit Spotify Testing</ca>
   <cs>Spotify from Spotify Testing Repo</cs>
   <da>Spotify fra Spotify testing-softwarekilde</da>
   <de>Spotify vom Spotify Testing Repo</de>
   <el>Spotify από το Spotify Test Repo</el>
   <en>Spotify from Spotify Testing Repo</en>
   <es_ES>Spotify de los Repositorios de Prueba de Spotify</es_ES>
   <es>Spotify de los repositorios Testing de Spotify</es>
   <et>Spotify from Spotify Testing Repo</et>
   <eu>Spotify from Spotify Testing Repo</eu>
   <fa>Spotify from Spotify Testing Repo</fa>
   <fil_PH>Spotify from Spotify Testing Repo</fil_PH>
   <fi>Spotify ohjelmavarastosta Spotify testaushaara</fi>
   <fr_BE>Spotify du dépôt de test Spotify</fr_BE>
   <fr>Spotify du dépôt de test Spotify</fr>
   <gl_ES>Spotify do repositorio Spotify Testing Repo</gl_ES>
   <gu>Spotify from Spotify Testing Repo</gu>
   <he_IL>Spotify from Spotify Testing Repo</he_IL>
   <hi>Spotify परीक्षण पैकेज-संग्रह से Spotify</hi>
   <hr>Spotify from Spotify Testing Repo</hr>
   <hu>Spotify from Spotify Testing Repo</hu>
   <id>Spotify from Spotify Testing Repo</id>
   <is>Spotify from Spotify Testing Repo</is>
   <it>Spotify dal repo testing di Spotify</it>
   <ja>Spotify テストレポの Spotify</ja>
   <kk>Spotify from Spotify Testing Repo</kk>
   <ko>Spotify from Spotify Testing Repo</ko>
   <ku>Spotify from Spotify Testing Repo</ku>
   <lt>Spotify from Spotify Testing Repo</lt>
   <mk>Spotify from Spotify Testing Repo</mk>
   <mr>Spotify from Spotify Testing Repo</mr>
   <nb_NO>Spotify from Spotify Testing Repo</nb_NO>
   <nb>Spotify fra Spotify Testing-pakkearkiv</nb>
   <nl_BE>Spotify from Spotify Testing Repo</nl_BE>
   <nl>Spotify uit Spotify Testing Pakketbron</nl>
   <or>Spotify from Spotify Testing Repo</or>
   <pl>Spotify z repozytorium testowego Spotify</pl>
   <pt_BR>Spotify (serviços de streaming de músicas) do Repositório de Teste/Test</pt_BR>
   <pt>Spotify do repositório Spotify Testing Repo</pt>
   <ro>Spotify from Spotify Testing Repo</ro>
   <ru>Сервис потокового аудио включающий более 30 млн. треков</ru>
   <sk>Spotify from Spotify Testing Repo</sk>
   <sl>Spotify iz Spotify testnega repozitorija</sl>
   <so>Spotify from Spotify Testing Repo</so>
   <sq>Spotify prej Depos së Testimve të Spotify-it</sq>
   <sr>Spotify from Spotify Testing Repo</sr>
   <sv>Spotify från Spotify Testing Förråd</sv>
   <th>Spotify จาก Spotify Testing Repo</th>
   <tr>Spotify Test Deposundan Spotify</tr>
   <uk>Spotify from Spotify Testing Repo</uk>
   <vi>Spotify from Spotify Testing Repo</vi>
   <zh_CN>Spotify from Spotify Testing Repo</zh_CN>
   <zh_HK>Spotify from Spotify Testing Repo</zh_HK>
   <zh_TW>Spotify from Spotify Testing Repo</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add -
echo "deb http://repository.spotify.com stable non-free">/etc/apt/sources.list.d/spotify.list
apt-get update
</preinstall>

<install_package_names>
spotify-client
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
spotify-client
</uninstall_package_names>
</app>
