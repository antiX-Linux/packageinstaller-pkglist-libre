<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hebrew_Thunderbird
</name>

<description>
   <am>Hebrew localisation of Thunderbird</am>
   <ar>Hebrew localisation of Thunderbird</ar>
   <be>Hebrew localisation of Thunderbird</be>
   <bg>Hebrew localisation of Thunderbird</bg>
   <bn>Hebrew localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Hebreu</ca>
   <cs>Hebrew localisation of Thunderbird</cs>
   <da>Hebrew localisation of Thunderbird</da>
   <de>Hebräische Lokalisation für “Thunderbird”</de>
   <el>Εβραϊκά για το Thunderbird</el>
   <en>Hebrew localisation of Thunderbird</en>
   <es_ES>Localización Hebreo de Thunderbird</es_ES>
   <es>Localización Hebreo de Thunderbird</es>
   <et>Hebrew localisation of Thunderbird</et>
   <eu>Hebrew localisation of Thunderbird</eu>
   <fa>Hebrew localisation of Thunderbird</fa>
   <fil_PH>Hebrew localisation of Thunderbird</fil_PH>
   <fi>Hebrew localisation of Thunderbird</fi>
   <fr_BE>Localisation en hébreu pour Thunderbird</fr_BE>
   <fr>Localisation en hébreu pour Thunderbird</fr>
   <gl_ES>Hebrew localisation of Thunderbird</gl_ES>
   <gu>Hebrew localisation of Thunderbird</gu>
   <he_IL>Hebrew localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का हिब्रू संस्करण</hi>
   <hr>Hebrew localisation of Thunderbird</hr>
   <hu>Hebrew localisation of Thunderbird</hu>
   <id>Hebrew localisation of Thunderbird</id>
   <is>Hebrew localisation of Thunderbird</is>
   <it>Localizzazione ebraica di Thunderbird</it>
   <ja>ヘブライ語版 Thunderbird</ja>
   <kk>Hebrew localisation of Thunderbird</kk>
   <ko>Hebrew localisation of Thunderbird</ko>
   <ku>Hebrew localisation of Thunderbird</ku>
   <lt>Hebrew localisation of Thunderbird</lt>
   <mk>Hebrew localisation of Thunderbird</mk>
   <mr>Hebrew localisation of Thunderbird</mr>
   <nb_NO>Hebrew localisation of Thunderbird</nb_NO>
   <nb>Hebraisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Hebrew localisation of Thunderbird</nl_BE>
   <nl>Hebrew localisation of Thunderbird</nl>
   <or>Hebrew localisation of Thunderbird</or>
   <pl>Hebrew localisation of Thunderbird</pl>
   <pt_BR>Hebraico Localização para o Thunderbird</pt_BR>
   <pt>Hebreu Localização para Thunderbird</pt>
   <ro>Hebrew localisation of Thunderbird</ro>
   <ru>Hebrew localisation of Thunderbird</ru>
   <sk>Hebrew localisation of Thunderbird</sk>
   <sl>Hebrejske krajevne nastavitve za Thunderbird</sl>
   <so>Hebrew localisation of Thunderbird</so>
   <sq>Fjalor hebraisht për hunspell</sq>
   <sr>Hebrew localisation of Thunderbird</sr>
   <sv>Hebreisk lokalisering av Thunderbird</sv>
   <th>Hebrew localisation of Thunderbird</th>
   <tr>Thunderbird'ün İbranice yerelleştirmesi</tr>
   <uk>Hebrew localisation of Thunderbird</uk>
   <vi>Hebrew localisation of Thunderbird</vi>
   <zh_CN>Hebrew localisation of Thunderbird</zh_CN>
   <zh_HK>Hebrew localisation of Thunderbird</zh_HK>
   <zh_TW>Hebrew localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-he
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-he
</uninstall_package_names>

</app>
