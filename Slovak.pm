<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Slovak
</name>

<description>
   <am>Slovak dictionary for hunspell</am>
   <ar>Slovak dictionary for hunspell</ar>
   <be>Slovak dictionary for hunspell</be>
   <bg>Slovak dictionary for hunspell</bg>
   <bn>Slovak dictionary for hunspell</bn>
   <ca>Diccionari Eslovac per hunspell</ca>
   <cs>Slovak dictionary for hunspell</cs>
   <da>Slovak dictionary for hunspell</da>
   <de>Slowakisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό στα σλοβακικά για το hunspell</el>
   <en>Slovak dictionary for hunspell</en>
   <es_ES>Diccionario Eslovaco para hunspell</es_ES>
   <es>Diccionario Eslovaco para hunspell</es>
   <et>Slovak dictionary for hunspell</et>
   <eu>Slovak dictionary for hunspell</eu>
   <fa>Slovak dictionary for hunspell</fa>
   <fil_PH>Slovak dictionary for hunspell</fil_PH>
   <fi>Slovak dictionary for hunspell</fi>
   <fr_BE>Slovaque dictionnaire pour hunspell</fr_BE>
   <fr>Slovaque dictionnaire pour hunspell</fr>
   <gl_ES>Slovak dictionary for hunspell</gl_ES>
   <gu>Slovak dictionary for hunspell</gu>
   <he_IL>Slovak dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु स्लोवाकियाई शब्दकोष</hi>
   <hr>Slovak dictionary for hunspell</hr>
   <hu>Slovak dictionary for hunspell</hu>
   <id>Slovak dictionary for hunspell</id>
   <is>Slovak dictionary for hunspell</is>
   <it>Dizionario slovak per hunspell</it>
   <ja>Hunspell 用スロバキア語辞書</ja>
   <kk>Slovak dictionary for hunspell</kk>
   <ko>Slovak dictionary for hunspell</ko>
   <ku>Slovak dictionary for hunspell</ku>
   <lt>Slovak dictionary for hunspell</lt>
   <mk>Slovak dictionary for hunspell</mk>
   <mr>Slovak dictionary for hunspell</mr>
   <nb_NO>Slovak dictionary for hunspell</nb_NO>
   <nb>Slovakisk ordliste for hunspell</nb>
   <nl_BE>Slovak dictionary for hunspell</nl_BE>
   <nl>Slovak dictionary for hunspell</nl>
   <or>Slovak dictionary for hunspell</or>
   <pl>Slovak dictionary for hunspell</pl>
   <pt_BR>Dicionário Eslovaco para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Eslovaco para hunspell</pt>
   <ro>Slovak dictionary for hunspell</ro>
   <ru>Slovak dictionary for hunspell</ru>
   <sk>Slovak dictionary for hunspell</sk>
   <sl>Slovaški slovar za hunspell</sl>
   <so>Slovak dictionary for hunspell</so>
   <sq>Fjalor sllovakisht për hunspell</sq>
   <sr>Slovak dictionary for hunspell</sr>
   <sv>Slovakisk ordbok för hunspell</sv>
   <th>Slovak dictionary for hunspell</th>
   <tr>Hunspell için Slovakça sözlük</tr>
   <uk>Slovak dictionary for hunspell</uk>
   <vi>Slovak dictionary for hunspell</vi>
   <zh_CN>Slovak dictionary for hunspell</zh_CN>
   <zh_HK>Slovak dictionary for hunspell</zh_HK>
   <zh_TW>Slovak dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-sk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-sk
</uninstall_package_names>

</app>
