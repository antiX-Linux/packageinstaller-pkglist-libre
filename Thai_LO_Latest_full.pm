<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Thai_LO_Latest_full
</name>

<description>
   <am>Thai Language Meta-Package for LibreOffice</am>
   <ar>Thai Language Meta-Package for LibreOffice</ar>
   <be>Thai Language Meta-Package for LibreOffice</be>
   <bg>Thai Language Meta-Package for LibreOffice</bg>
   <bn>Thai Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Thai per LibreOffice</ca>
   <cs>Thai Language Meta-Package for LibreOffice</cs>
   <da>Thai Language Meta-Package for LibreOffice</da>
   <de>Thai Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Ταϊλανδέζικα</el>
   <en>Thai Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma tailandés para LibreOffice</es_ES>
   <es>Metapaquete de idioma Tailandés para LibreOffice</es>
   <et>Thai Language Meta-Package for LibreOffice</et>
   <eu>Thai Language Meta-Package for LibreOffice</eu>
   <fa>Thai Language Meta-Package for LibreOffice</fa>
   <fil_PH>Thai Language Meta-Package for LibreOffice</fil_PH>
   <fi>Thainkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue thaïlandaise pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue thaïlandaise pour LibreOffice</fr>
   <gl_ES>Tailandés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Thai Language Meta-Package for LibreOffice</gu>
   <he_IL>Thai Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु थाई भाषा मेटा-पैकेज</hi>
   <hr>Thai Language Meta-Package for LibreOffice</hr>
   <hu>Thai Language Meta-Package for LibreOffice</hu>
   <id>Thai Language Meta-Package for LibreOffice</id>
   <is>Thai Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua tailandese per LibreOffice</it>
   <ja>LibreOffice用のタイ語メタパッケージ</ja>
   <kk>Thai Language Meta-Package for LibreOffice</kk>
   <ko>Thai Language Meta-Package for LibreOffice</ko>
   <ku>Thai Language Meta-Package for LibreOffice</ku>
   <lt>Thai Language Meta-Package for LibreOffice</lt>
   <mk>Thai Language Meta-Package for LibreOffice</mk>
   <mr>Thai Language Meta-Package for LibreOffice</mr>
   <nb_NO>Thai Language Meta-Package for LibreOffice</nb_NO>
   <nb>Thai språkpakke for LibreOffice</nb>
   <nl_BE>Thai Language Meta-Package for LibreOffice</nl_BE>
   <nl>Thais Taal Meta-Pakket voor LibreOffice</nl>
   <or>Thai Language Meta-Package for LibreOffice</or>
   <pl>Tajski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Tailandês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Tailandês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Thai Language Meta-Package for LibreOffice</ro>
   <ru>Thai Language Meta-Package for LibreOffice</ru>
   <sk>Thai Language Meta-Package for LibreOffice</sk>
   <sl>Tajski jezikovni meta-paket za LibreOffice</sl>
   <so>Thai Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në tajlandisht</sq>
   <sr>Thai Language Meta-Package for LibreOffice</sr>
   <sv>Thai Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษาไทยสำหรับ LibreOffice</th>
   <tr>LibreOffice için Tayca Dili Üst-Paketi</tr>
   <uk>Thai Language Meta-Package for LibreOffice</uk>
   <vi>Thai Language Meta-Package for LibreOffice</vi>
   <zh_CN>Thai Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Thai Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Thai Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-th
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-th
</uninstall_package_names>

</app>
