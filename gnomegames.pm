<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
Simple Card Games
</name>

<description>
   <am>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</am>
   <ar>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</ar>
   <be>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</be>
   <bg>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</bg>
   <bn>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</bn>
   <ca>Jocs de cartes i trencaclosques senzills (solitari, cors, mines, etc.)</ca>
   <cs>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</cs>
   <da>Simple kortspil og puzzlespil til skrivebordet (7-kabale, hjerterfri, minestryger osv.)</da>
   <de>Einfache Desktop-Karten- und Puzzlespiele (Solitaire, Herzen, Minen, etc.)</de>
   <el>Παιχνίδια καρτών και παζλ (solitare, hearts, mines, etc)</el>
   <en>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</en>
   <es_ES>Juegos de cartas y rompecabezas para sobremesa (solitario, corazones, minas, etc.)</es_ES>
   <es>Juegos de cartas y rompecabezas para sobremesa (solitario, corazones, minas, etc.)</es>
   <et>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</et>
   <eu>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</eu>
   <fa>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</fa>
   <fil_PH>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</fil_PH>
   <fi>Yksinkertaisia kortti- ja pulmapelejä työpöydälle (pasianssi, hertta, miinaharava, jne)</fi>
   <fr_BE>Jeux de cartes et puzzles (solitaire, la dame de pique, mines, etc)</fr_BE>
   <fr>Jeux de cartes et puzzles (solitaire, la dame de pique, mines, etc)</fr>
   <gl_ES>Xogos simples (solitario, corazóns, minas, etc)</gl_ES>
   <gu>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</gu>
   <he_IL>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</he_IL>
   <hi>सरल डेस्कटॉप कार्ड व पहेली खेल (सॉलिटेयर, हर्ट्स, माइंस इत्यादि)</hi>
   <hr>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</hr>
   <hu>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</hu>
   <id>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</id>
   <is>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</is>
   <it>Semplici giochi di carte e puzzles (solitario, cuori, mine, ecc.)</it>
   <ja>やさしいカードゲームやパズルゲーム（ソリテアやハート、地雷ゲームなど）</ja>
   <kk>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</kk>
   <ko>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</ko>
   <ku>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</ku>
   <lt>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</lt>
   <mk>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</mk>
   <mr>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</mr>
   <nb_NO>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</nb_NO>
   <nb>Enkle kort- og hjernetrimspill (kabal, hjerter, minesveiper osv.)</nb>
   <nl_BE>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</nl_BE>
   <nl>eenvoudige Desktop Kaart en Puzzel Spellen (solitaire, hearts, mines, etc)</nl>
   <or>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</or>
   <pl>proste gry karciane i łamigłówki (solitare, hearts, mines itp.)</pl>
   <pt_BR>Jogos Simples de Área de Trabalho de Cartas e Quebra-cabeça (paciência/solitare, corações/hearts, minas/mines, etc)</pt_BR>
   <pt>Jogos simples (solitare, hearts, mines, etc)</pt>
   <ro>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</ro>
   <ru>Простые карточные игры и головоломки (солитер, червы, сапёр, и т.д.)</ru>
   <sk>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</sk>
   <sl>Preprosta namizna igra s kartami in miselne igre )solitaire, srca, mine, itd.)</sl>
   <so>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</so>
   <sq>Instalim shumë minimal i Gnome-s</sq>
   <sr>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</sr>
   <sv>Enkla skrivbords kort och pusselspel (patiens, hjärter, minor, etc)</sv>
   <th>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</th>
   <tr>Basit Masaüstü Kart ve Yapboz Oyunları (solitare, hearts, mines, vs.)</tr>
   <uk>прості настільні карткові ігри та головоломки (solitare, hearts, mines та інші)</uk>
   <vi>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</vi>
   <zh_CN>简单的桌面卡牌与智力游戏（solitare、hearts、mines等）</zh_CN>
   <zh_HK>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</zh_HK>
   <zh_TW>Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gnome-games
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gnome-games
</uninstall_package_names>
</app>
