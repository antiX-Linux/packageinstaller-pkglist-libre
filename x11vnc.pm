<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Remote Access
</category>

<name>
x11vnc
</name>

<description>
   <am>VNC server for X window environments</am>
   <ar>VNC server for X window environments</ar>
   <be>VNC server for X window environments</be>
   <bg>VNC server for X window environments</bg>
   <bn>VNC server for X window environments</bn>
   <ca>Servidor VNC per entorns X-window</ca>
   <cs>VNC server for X window environments</cs>
   <da>VNC-server til X-vinduesmiljøer</da>
   <de>VNC-Server für X Window-Umgebungen</de>
   <el>VNC server για περιβάλλον παραθύρων X.</el>
   <en>VNC server for X window environments</en>
   <es_ES>Servidor VNC para entornos de ventanas X</es_ES>
   <es>Servidor VNC para el entornos de ventanas X</es>
   <et>VNC server for X window environments</et>
   <eu>VNC server for X window environments</eu>
   <fa>VNC server for X window environments</fa>
   <fil_PH>VNC server for X window environments</fil_PH>
   <fi>VNC-palvelin X-ikkunointiympäristöihin</fi>
   <fr_BE>Serveur VNC pour environnements X window</fr_BE>
   <fr>Serveur VNC pour environnements X window</fr>
   <gl_ES>Servidor VNC para ambientes de lapelas "X"</gl_ES>
   <gu>VNC server for X window environments</gu>
   <he_IL>VNC server for X window environments</he_IL>
   <hi>एक्स विंडो वातावरण हेतु वीएनसी सर्वर</hi>
   <hr>VNC server for X window environments</hr>
   <hu>VNC server for X window environments</hu>
   <id>VNC server for X window environments</id>
   <is>VNC server for X window environments</is>
   <it>Server VNC per ambienti X Window</it>
   <ja>X Window 環境用のVNCサーバー</ja>
   <kk>VNC server for X window environments</kk>
   <ko>VNC server for X window environments</ko>
   <ku>VNC server for X window environments</ku>
   <lt>VNC server for X window environments</lt>
   <mk>VNC server for X window environments</mk>
   <mr>VNC server for X window environments</mr>
   <nb_NO>VNC server for X window environments</nb_NO>
   <nb>VNC-tjener for X Window System-miljøer</nb>
   <nl_BE>VNC server for X window environments</nl_BE>
   <nl>VNC server voor X window omgevingen</nl>
   <or>VNC server for X window environments</or>
   <pl>Serwer VNC dla środowisk X Window</pl>
   <pt_BR>Servidor VNC para ambientes de janelas "X"</pt_BR>
   <pt>Servidor VNC para ambientes de janelas "X"</pt>
   <ro>VNC server for X window environments</ro>
   <ru>VNC сервер для оконной среды X Window</ru>
   <sk>VNC server for X window environments</sk>
   <sl>VNC strežnik za X window okolja</sl>
   <so>VNC server for X window environments</so>
   <sq>Shërbyes VNC për mjedise me dritare X</sq>
   <sr>VNC server for X window environments</sr>
   <sv>VNC server för X window miljöer</sv>
   <th>VNC server for X window environments</th>
   <tr>X pencere ortamları için VNC sunucusu</tr>
   <uk>VNC server for X window environments</uk>
   <vi>VNC server for X window environments</vi>
   <zh_CN>VNC server for X window environments</zh_CN>
   <zh_HK>VNC server for X window environments</zh_HK>
   <zh_TW>VNC server for X window environments</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
x11vnc
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
x11vnc
</uninstall_package_names>
</app>
