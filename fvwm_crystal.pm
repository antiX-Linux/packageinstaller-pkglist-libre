<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Window Managers
</category>

<name>
fvwm_crystal
</name>

<description>
   <am>fvwm-crystal window manager</am>
   <ar>fvwm-crystal window manager</ar>
   <be>fvwm-crystal window manager</be>
   <bg>fvwm-crystal window manager</bg>
   <bn>fvwm-crystal window manager</bn>
   <ca>Gestor de finestres fvwm-crystal</ca>
   <cs>fvwm-crystal window manager</cs>
   <da>fvwm-crystal window manager</da>
   <de>transparenter “Chrystal” Stil für die Fensterverwaltung “FVWM”</de>
   <el>διαχειριστής παραθύρων fvwm-crystal</el>
   <en>fvwm-crystal window manager</en>
   <es_ES>Gestor de ventanas fvwm-crystal</es_ES>
   <es>gestor de ventanas fvwm-crystal</es>
   <et>fvwm-crystal window manager</et>
   <eu>fvwm-crystal window manager</eu>
   <fa>fvwm-crystal window manager</fa>
   <fil_PH>fvwm-crystal window manager</fil_PH>
   <fi>fvwm-crystal window manager</fi>
   <fr_BE>fvwm-crystal gestionnaire de fenêtres</fr_BE>
   <fr>fvwm-crystal gestionnaire de fenêtres</fr>
   <gl_ES>fvwm-crystal window manager</gl_ES>
   <gu>fvwm-crystal window manager</gu>
   <he_IL>fvwm-crystal window manager</he_IL>
   <hi>एफवी-डब्लूएम क्रिस्टल सरल विंडो प्रबंधक</hi>
   <hr>fvwm-crystal window manager</hr>
   <hu>fvwm-crystal window manager</hu>
   <id>fvwm-crystal window manager</id>
   <is>fvwm-crystal window manager</is>
   <it>fvwm-crystal window manager</it>
   <ja>FVWM-Crystal ウィンドウマネージャ</ja>
   <kk>fvwm-crystal window manager</kk>
   <ko>fvwm-crystal window manager</ko>
   <ku>fvwm-crystal window manager</ku>
   <lt>fvwm-crystal window manager</lt>
   <mk>fvwm-crystal window manager</mk>
   <mr>fvwm-crystal window manager</mr>
   <nb_NO>fvwm-crystal window manager</nb_NO>
   <nb>Vindusbehandleren fvwm-crystal</nb>
   <nl_BE>fvwm-crystal window manager</nl_BE>
   <nl>fvwm-crystal venster beheerder</nl>
   <or>fvwm-crystal window manager</or>
   <pl>fvwm-crystal window manager</pl>
   <pt_BR>fvwm-crystal gerenciador de janelas</pt_BR>
   <pt>Gestor de janelas fvwm-crystal</pt>
   <ro>fvwm-crystal window manager</ro>
   <ru>fvwm-crystal window manager</ru>
   <sk>fvwm-crystal window manager</sk>
   <sl>fvwm-crystal okenski upravljalnik</sl>
   <so>fvwm-crystal window manager</so>
   <sq>Përgjegjësi fvwm-crystal i dritareve</sq>
   <sr>fvwm-crystal window manager</sr>
   <sv>fvwm-crystal fönsterhanterare</sv>
   <th>fvwm-crystal window manager</th>
   <tr>fvwm-crystal pencere yöneticisi</tr>
   <uk>fvwm-crystal window manager</uk>
   <vi>fvwm-crystal window manager</vi>
   <zh_CN>fvwm-crystal window manager</zh_CN>
   <zh_HK>fvwm-crystal window manager</zh_HK>
   <zh_TW>fvwm-crystal window manager</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
fvwm
fvwm-crystal
fvwm-icons
trayer
quodlibet
xterm
xscreensaver
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
fvwm
fvwm-crystal
fvwm-icons
trayer
quodlibet
xterm
xscreensaver
</uninstall_package_names>

</app>
