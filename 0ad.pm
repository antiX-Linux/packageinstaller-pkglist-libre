<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
0 A.D.
</name>

<description>
   <en>Real-time strategy game of ancient warfare</en>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
0ad
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
0ad
</uninstall_package_names>
</app>
