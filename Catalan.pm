<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Catalan
</name>

<description>
   <am>Catalan dictionary for hunspell</am>
   <ar>Catalan dictionary for hunspell</ar>
   <be>Catalan dictionary for hunspell</be>
   <bg>Catalan dictionary for hunspell</bg>
   <bn>Catalan dictionary for hunspell</bn>
   <ca>Diccionari Català per hunspell</ca>
   <cs>Catalan dictionary for hunspell</cs>
   <da>Catalan dictionary for hunspell</da>
   <de>Katalanisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό καταλανικά για hunspell</el>
   <en>Catalan dictionary for hunspell</en>
   <es_ES>Diccionario Catalan para hunspell</es_ES>
   <es>Diccionario Catalán para hunspell</es>
   <et>Catalan dictionary for hunspell</et>
   <eu>Catalan dictionary for hunspell</eu>
   <fa>Catalan dictionary for hunspell</fa>
   <fil_PH>Catalan dictionary for hunspell</fil_PH>
   <fi>Catalan dictionary for hunspell</fi>
   <fr_BE>Catalan dictionnaire pour hunspell</fr_BE>
   <fr>Catalan dictionnaire pour hunspell</fr>
   <gl_ES>Catalan dictionary for hunspell</gl_ES>
   <gu>Catalan dictionary for hunspell</gu>
   <he_IL>Catalan dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु कैटलन शब्दकोष</hi>
   <hr>Catalan dictionary for hunspell</hr>
   <hu>Catalan dictionary for hunspell</hu>
   <id>Catalan dictionary for hunspell</id>
   <is>Catalan dictionary for hunspell</is>
   <it>Dizionario catalano per hunspell</it>
   <ja>Hunspell 用カタロニア語辞書</ja>
   <kk>Catalan dictionary for hunspell</kk>
   <ko>Catalan dictionary for hunspell</ko>
   <ku>Catalan dictionary for hunspell</ku>
   <lt>Catalan dictionary for hunspell</lt>
   <mk>Catalan dictionary for hunspell</mk>
   <mr>Catalan dictionary for hunspell</mr>
   <nb_NO>Catalan dictionary for hunspell</nb_NO>
   <nb>Katalansk ordliste for hunspell</nb>
   <nl_BE>Catalan dictionary for hunspell</nl_BE>
   <nl>Catalaans woordenboek voor hunspell</nl>
   <or>Catalan dictionary for hunspell</or>
   <pl>Catalan dictionary for hunspell</pl>
   <pt_BR>Dicionário Catalão para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Catalão para hunspell</pt>
   <ro>Catalan dictionary for hunspell</ro>
   <ru>Catalan dictionary for hunspell</ru>
   <sk>Catalan dictionary for hunspell</sk>
   <sl>Katalonski slovar za hunspell</sl>
   <so>Catalan dictionary for hunspell</so>
   <sq>Fjalor katalanisht për hunspell</sq>
   <sr>Catalan dictionary for hunspell</sr>
   <sv>Katalansk ordbok för hunspell</sv>
   <th>Catalan dictionary for hunspell</th>
   <tr>Hunspell için Katalanca sözlük</tr>
   <uk>Catalan dictionary for hunspell</uk>
   <vi>Catalan dictionary for hunspell</vi>
   <zh_CN>Catalan dictionary for hunspell</zh_CN>
   <zh_HK>Catalan dictionary for hunspell</zh_HK>
   <zh_TW>Catalan dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-ca
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ca
</uninstall_package_names>

</app>
