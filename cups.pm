<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Printing
</category>

<name>
CUPS
</name>

<description>
   <am>CUPS Printing system</am>
   <ar>CUPS Printing system</ar>
   <be>CUPS Printing system</be>
   <bg>CUPS Printing system</bg>
   <bn>CUPS Printing system</bn>
   <ca>Sistema d'impressió CUPS</ca>
   <cs>CUPS Printing system</cs>
   <da>CUPS Printing system</da>
   <de>CUPS Drucksystemsoftware</de>
   <el>Σύστημα εκτύπωσης CUPS</el>
   <en>CUPS Printing system</en>
   <es_ES>Sistema CUPS para impresión</es_ES>
   <es>Sistema CUPS para impresión</es>
   <et>CUPS Printing system</et>
   <eu>CUPS Printing system</eu>
   <fa>CUPS Printing system</fa>
   <fil_PH>CUPS Printing system</fil_PH>
   <fi>CUPS Printing system</fi>
   <fr_BE>CUPS Système d'impression</fr_BE>
   <fr>CUPS Système d'impression</fr>
   <gl_ES>CUPS Printing system</gl_ES>
   <gu>CUPS Printing system</gu>
   <he_IL>CUPS Printing system</he_IL>
   <hi>CUPS प्रिंट सिस्टम</hi>
   <hr>CUPS Printing system</hr>
   <hu>CUPS Printing system</hu>
   <id>CUPS Printing system</id>
   <is>CUPS Printing system</is>
   <it>CUPS sistema di stampa</it>
   <ja>CUPS 印刷システム</ja>
   <kk>CUPS Printing system</kk>
   <ko>CUPS Printing system</ko>
   <ku>CUPS Printing system</ku>
   <lt>CUPS Printing system</lt>
   <mk>CUPS Printing system</mk>
   <mr>CUPS Printing system</mr>
   <nb_NO>CUPS Printing system</nb_NO>
   <nb>CUPS utskriftssystem</nb>
   <nl_BE>CUPS Printing system</nl_BE>
   <nl>CUPS-afdruksysteem</nl>
   <or>CUPS Printing system</or>
   <pl>CUPS Printing system</pl>
   <pt_BR>Sistema de Impressão CUPS</pt_BR>
   <pt>Sistema de impressão CUPS (Common Unix Printing System)</pt>
   <ro>CUPS Printing system</ro>
   <ru>CUPS Printing system</ru>
   <sk>CUPS Printing system</sk>
   <sl>CUPS sistem za tiskanje</sl>
   <so>CUPS Printing system</so>
   <sq>Sistemi CUPS për shtypje</sq>
   <sr>CUPS Printing system</sr>
   <sv>CUPS Skrivarsystem</sv>
   <th>CUPS Printing system</th>
   <tr>CUPS Yazdırma sistemi</tr>
   <uk>CUPS Printing system</uk>
   <vi>CUPS Printing system</vi>
   <zh_CN>CUPS Printing system</zh_CN>
   <zh_HK>CUPS Printing system</zh_HK>
   <zh_TW>CUPS Printing system</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
cups
cups-filters
cups-pdf
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
cups
cups-filters
cups-pdf
</uninstall_package_names>
</app>
