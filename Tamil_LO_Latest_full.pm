<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Tamil_LO_Latest_full
</name>

<description>
   <am>Tamil Language Meta-Package for LibreOffice</am>
   <ar>Tamil Language Meta-Package for LibreOffice</ar>
   <be>Tamil Language Meta-Package for LibreOffice</be>
   <bg>Tamil Language Meta-Package for LibreOffice</bg>
   <bn>Tamil Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Tàmil per LibreOffice</ca>
   <cs>Tamil Language Meta-Package for LibreOffice</cs>
   <da>Tamil Language Meta-Package for LibreOffice</da>
   <de>Tamilisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Tamil</el>
   <en>Tamil Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Idioma Tamil para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Tamil para LibreOffice</es>
   <et>Tamil Language Meta-Package for LibreOffice</et>
   <eu>Tamil Language Meta-Package for LibreOffice</eu>
   <fa>Tamil Language Meta-Package for LibreOffice</fa>
   <fil_PH>Tamil Language Meta-Package for LibreOffice</fil_PH>
   <fi>Tamilinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue tamoule (Inde) pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue tamoule (Inde) pour LibreOffice</fr>
   <gl_ES>Tamil Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Tamil Language Meta-Package for LibreOffice</gu>
   <he_IL>Tamil Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु तमिल भाषा मेटा-पैकेज</hi>
   <hr>Tamil Language Meta-Package for LibreOffice</hr>
   <hu>Tamil Language Meta-Package for LibreOffice</hu>
   <id>Tamil Language Meta-Package for LibreOffice</id>
   <is>Tamil Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua tamil per LibreOffice</it>
   <ja>LibreOffice用のタミル語メタパッケージ</ja>
   <kk>Tamil Language Meta-Package for LibreOffice</kk>
   <ko>Tamil Language Meta-Package for LibreOffice</ko>
   <ku>Tamil Language Meta-Package for LibreOffice</ku>
   <lt>Tamil Language Meta-Package for LibreOffice</lt>
   <mk>Tamil Language Meta-Package for LibreOffice</mk>
   <mr>Tamil Language Meta-Package for LibreOffice</mr>
   <nb_NO>Tamil Language Meta-Package for LibreOffice</nb_NO>
   <nb>Tamil språkpakke for LibreOffice</nb>
   <nl_BE>Tamil Language Meta-Package for LibreOffice</nl_BE>
   <nl>Tamilse Taal Meta-Pakket voor LibreOffice</nl>
   <or>Tamil Language Meta-Package for LibreOffice</or>
   <pl>Tamilski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Tamil Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Tamil Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Tamil Language Meta-Package for LibreOffice</ro>
   <ru>Tamil Language Meta-Package for LibreOffice</ru>
   <sk>Tamil Language Meta-Package for LibreOffice</sk>
   <sl>Tamilski jezikovni meta-paket za LibreOffice</sl>
   <so>Tamil Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në tamile</sq>
   <sr>Tamil Language Meta-Package for LibreOffice</sr>
   <sv>Tamil Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Tamil สำหรับ LibreOffice</th>
   <tr>LibreOffice için Tamil Dili Üst-Paketi</tr>
   <uk>Tamil Language Meta-Package for LibreOffice</uk>
   <vi>Tamil Language Meta-Package for LibreOffice</vi>
   <zh_CN>Tamil Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Tamil Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Tamil Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-ta
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-ta
</uninstall_package_names>

</app>
