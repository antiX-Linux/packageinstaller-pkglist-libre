<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
German_Firefox
</name>

<description>
   <am>German localisation of Firefox</am>
   <ar>German localisation of Firefox</ar>
   <be>German localisation of Firefox</be>
   <bg>German localisation of Firefox</bg>
   <bn>German localisation of Firefox</bn>
   <ca>Localització de Firefox en Alemany</ca>
   <cs>German localisation of Firefox</cs>
   <da>Tysk oversættelse af Firefox</da>
   <de>Deutsche Lokalisierung von Firefox</de>
   <el>Γερμανικά για το Firefox</el>
   <en>German localisation of Firefox</en>
   <es_ES>Localización Alemana de Firefox</es_ES>
   <es>Localización Alemán de Firefox</es>
   <et>German localisation of Firefox</et>
   <eu>German localisation of Firefox</eu>
   <fa>German localisation of Firefox</fa>
   <fil_PH>German localisation of Firefox</fil_PH>
   <fi>Saksalainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en allemand pour Firefox</fr_BE>
   <fr>Localisation en allemand pour Firefox</fr>
   <gl_ES>Localización de Firefox ao alemán</gl_ES>
   <gu>German localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לגרמנית</he_IL>
   <hi>फायरफॉक्स का जर्मन संस्करण</hi>
   <hr>German localisation of Firefox</hr>
   <hu>German localisation of Firefox</hu>
   <id>German localisation of Firefox</id>
   <is>German localisation of Firefox</is>
   <it>Localizzazione tedesca di Firefox</it>
   <ja>Firefox のドイツ語版</ja>
   <kk>German localisation of Firefox</kk>
   <ko>German localisation of Firefox</ko>
   <ku>German localisation of Firefox</ku>
   <lt>German localisation of Firefox</lt>
   <mk>German localisation of Firefox</mk>
   <mr>German localisation of Firefox</mr>
   <nb_NO>German localisation of Firefox</nb_NO>
   <nb>Tysk lokaltilpassing av Firefox</nb>
   <nl_BE>German localisation of Firefox</nl_BE>
   <nl>Duitse lokalisatie van Firefox</nl>
   <or>German localisation of Firefox</or>
   <pl>Niemiecka lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Alemão Localização para o Firefox</pt_BR>
   <pt>Alemão Localização para Firefox</pt>
   <ro>German localisation of Firefox</ro>
   <ru>Немецкая локализация Firefox</ru>
   <sk>German localisation of Firefox</sk>
   <sl>Nemške krajevne nastavitve za Firefox ESR</sl>
   <so>German localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it ESR në gjermanisht</sq>
   <sr>German localisation of Firefox</sr>
   <sv>Tysk lokalisering av Firefox</sv>
   <th>German localisation ของ Firefox</th>
   <tr>Firefox'un Almanca yerelleştirmesi</tr>
   <uk>German локалізація of Firefox</uk>
   <vi>German localisation of Firefox</vi>
   <zh_CN>Firefox 德语语言包</zh_CN>
   <zh_HK>German localisation of Firefox</zh_HK>
   <zh_TW>German localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-de
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-de
</uninstall_package_names>
</app>
