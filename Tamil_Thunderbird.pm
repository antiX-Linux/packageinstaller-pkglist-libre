<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Tamil_Thunderbird
</name>

<description>
   <am>Tamil localisation of Thunderbird</am>
   <ar>Tamil localisation of Thunderbird</ar>
   <be>Tamil localisation of Thunderbird</be>
   <bg>Tamil localisation of Thunderbird</bg>
   <bn>Tamil localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Tàmil</ca>
   <cs>Tamil localisation of Thunderbird</cs>
   <da>Tamil localisation of Thunderbird</da>
   <de>Indische Lokalisation für die tamilische Sprache von “Thunderbird”</de>
   <el>Ταμίλ για Thunderbird</el>
   <en>Tamil localisation of Thunderbird</en>
   <es_ES>Localización Tamil de Thunderbird</es_ES>
   <es>Localización Tamil de Thunderbird</es>
   <et>Tamil localisation of Thunderbird</et>
   <eu>Tamil localisation of Thunderbird</eu>
   <fa>Tamil localisation of Thunderbird</fa>
   <fil_PH>Tamil localisation of Thunderbird</fil_PH>
   <fi>Tamil localisation of Thunderbird</fi>
   <fr_BE>Localisation en tamoul (Inde) pour Thunderbird</fr_BE>
   <fr>Localisation en tamoul (Inde) pour Thunderbird</fr>
   <gl_ES>Tamil localisation of Thunderbird</gl_ES>
   <gu>Tamil localisation of Thunderbird</gu>
   <he_IL>Tamil localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का तमिल संस्करण</hi>
   <hr>Tamil localisation of Thunderbird</hr>
   <hu>Tamil localisation of Thunderbird</hu>
   <id>Tamil localisation of Thunderbird</id>
   <is>Tamil localisation of Thunderbird</is>
   <it>Localizzazione tamil di Thunderbird</it>
   <ja>タミル語版 Thunderbird</ja>
   <kk>Tamil localisation of Thunderbird</kk>
   <ko>Tamil localisation of Thunderbird</ko>
   <ku>Tamil localisation of Thunderbird</ku>
   <lt>Tamil localisation of Thunderbird</lt>
   <mk>Tamil localisation of Thunderbird</mk>
   <mr>Tamil localisation of Thunderbird</mr>
   <nb_NO>Tamil localisation of Thunderbird</nb_NO>
   <nb>Tamilsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Tamil localisation of Thunderbird</nl_BE>
   <nl>Tamil localisation of Thunderbird</nl>
   <or>Tamil localisation of Thunderbird</or>
   <pl>Tamil localisation of Thunderbird</pl>
   <pt_BR>Tamil Localização para o Thunderbird</pt_BR>
   <pt>Tamil Localização para Thunderbird</pt>
   <ro>Tamil localisation of Thunderbird</ro>
   <ru>Tamil localisation of Thunderbird</ru>
   <sk>Tamil localisation of Thunderbird</sk>
   <sl>Tamilske krajevne nastavitve za Thunderbird</sl>
   <so>Tamil localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në tamile</sq>
   <sr>Tamil localisation of Thunderbird</sr>
   <sv>Tamil lokalisering av Thunderbird</sv>
   <th>Tamil localisation of Thunderbird</th>
   <tr>Thunderbird Tamilce yerelleştirmesi</tr>
   <uk>Tamil localisation of Thunderbird</uk>
   <vi>Tamil localisation of Thunderbird</vi>
   <zh_CN>Tamil localisation of Thunderbird</zh_CN>
   <zh_HK>Tamil localisation of Thunderbird</zh_HK>
   <zh_TW>Tamil localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-ta-lk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-ta-lk
</uninstall_package_names>

</app>
