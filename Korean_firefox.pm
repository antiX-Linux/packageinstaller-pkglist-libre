<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Korean_Firefox
</name>

<description>
   <am>Korean localisation of Firefox</am>
   <ar>Korean localisation of Firefox</ar>
   <be>Korean localisation of Firefox</be>
   <bg>Korean localisation of Firefox</bg>
   <bn>Korean localisation of Firefox</bn>
   <ca>Localització de Firefox en Coreà</ca>
   <cs>Korean localisation of Firefox</cs>
   <da>Koreansk oversættelse af Firefox</da>
   <de>Koreanische Lokalisierung von Firefox</de>
   <el>Κορεατικά για το Firefox</el>
   <en>Korean localisation of Firefox</en>
   <es_ES>Localización Coreana de Firefox</es_ES>
   <es>Localización Coreano de Firefox</es>
   <et>Korean localisation of Firefox</et>
   <eu>Korean localisation of Firefox</eu>
   <fa>Korean localisation of Firefox</fa>
   <fil_PH>Korean localisation of Firefox</fil_PH>
   <fi>Korealainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en coréen pour Firefox</fr_BE>
   <fr>Localisation en coréen pour Firefox</fr>
   <gl_ES>Localizción do FIrefox ao corean</gl_ES>
   <gu>Korean localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לקוריאנית</he_IL>
   <hi>फायरफॉक्स का कोरियाई संस्करण</hi>
   <hr>Korean localisation of Firefox</hr>
   <hu>Korean localisation of Firefox</hu>
   <id>Korean localisation of Firefox</id>
   <is>Korean localisation of Firefox</is>
   <it>Localizzazione coreana di Firefox</it>
   <ja>Firefox の韓国（朝鮮）語版</ja>
   <kk>Korean localisation of Firefox</kk>
   <ko>Korean localisation of Firefox</ko>
   <ku>Korean localisation of Firefox</ku>
   <lt>Korean localisation of Firefox</lt>
   <mk>Korean localisation of Firefox</mk>
   <mr>Korean localisation of Firefox</mr>
   <nb_NO>Korean localisation of Firefox</nb_NO>
   <nb>Koreansk lokaltilpassing av Firefox</nb>
   <nl_BE>Korean localisation of Firefox</nl_BE>
   <nl>Koreaanse lokalisatie van Firefox</nl>
   <or>Korean localisation of Firefox</or>
   <pl>Koreańska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Coreano Localização para o Firefox</pt_BR>
   <pt>Coreano Localização para Firefox</pt>
   <ro>Korean localisation of Firefox</ro>
   <ru>Корейская локализация Firefox</ru>
   <sk>Korean localisation of Firefox</sk>
   <sl>Korejske krajevne nastavitve za Firefox</sl>
   <so>Korean localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në koreançe</sq>
   <sr>Korean localisation of Firefox</sr>
   <sv>Koreansk lokalisering av Firefox</sv>
   <th>Korean localisation ของ Firefox</th>
   <tr>Firefox'un Korece yerelleştirmesi</tr>
   <uk>Korean локалізація Firefox</uk>
   <vi>Korean localisation of Firefox</vi>
   <zh_CN>Korean localisation of Firefox</zh_CN>
   <zh_HK>Korean localisation of Firefox</zh_HK>
   <zh_TW>Korean localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-ko
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-ko
</uninstall_package_names>
</app>
