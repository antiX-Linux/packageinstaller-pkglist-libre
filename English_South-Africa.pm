<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English (South Africa)
</name>

<description>
   <am>English (South Africa) dictionary for hunspell</am>
   <ar>English (South Africa) dictionary for hunspell</ar>
   <be>English (South Africa) dictionary for hunspell</be>
   <bg>English (South Africa) dictionary for hunspell</bg>
   <bn>English (South Africa) dictionary for hunspell</bn>
   <ca>Diccionari Anglès (SA) per hunspell</ca>
   <cs>English (South Africa) dictionary for hunspell</cs>
   <da>English (South Africa) dictionary for hunspell</da>
   <de>Englisches (südafrikanisches) Wörterbuch für “Hunspell”</de>
   <el>Αγγλικό (Νότια Αφρική) λεξικό για hunspell</el>
   <en>English (South Africa) dictionary for hunspell</en>
   <es_ES>Diccionario Inglés (Sudáfrica) para hunspell</es_ES>
   <es>Diccionario Inglés (Sudáfrica) para hunspell</es>
   <et>English (South Africa) dictionary for hunspell</et>
   <eu>English (South Africa) dictionary for hunspell</eu>
   <fa>English (South Africa) dictionary for hunspell</fa>
   <fil_PH>English (South Africa) dictionary for hunspell</fil_PH>
   <fi>English (South Africa) dictionary for hunspell</fi>
   <fr_BE>Anglais (Afrique du Sud) dictionnaire pour hunspell</fr_BE>
   <fr>Anglais (Afrique du Sud) dictionnaire pour hunspell</fr>
   <gl_ES>English (South Africa) dictionary for hunspell</gl_ES>
   <gu>English (South Africa) dictionary for hunspell</gu>
   <he_IL>English (South Africa) dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु अंग्रेजी (दक्षिण अफ्रीकी) शब्दकोष</hi>
   <hr>English (South Africa) dictionary for hunspell</hr>
   <hu>English (South Africa) dictionary for hunspell</hu>
   <id>English (South Africa) dictionary for hunspell</id>
   <is>English (South Africa) dictionary for hunspell</is>
   <it>Dizionario inglese (Sud Africa) per hunspell</it>
   <ja>Hunspell 用英語（南アフリカ）辞書</ja>
   <kk>English (South Africa) dictionary for hunspell</kk>
   <ko>English (South Africa) dictionary for hunspell</ko>
   <ku>English (South Africa) dictionary for hunspell</ku>
   <lt>English (South Africa) dictionary for hunspell</lt>
   <mk>English (South Africa) dictionary for hunspell</mk>
   <mr>English (South Africa) dictionary for hunspell</mr>
   <nb_NO>English (South Africa) dictionary for hunspell</nb_NO>
   <nb>Engelsk (sør-afrikansk) ordliste for hunspell</nb>
   <nl_BE>English (South Africa) dictionary for hunspell</nl_BE>
   <nl>Engels (Zuid-Afrika) woordenboek voor hunspell</nl>
   <or>English (South Africa) dictionary for hunspell</or>
   <pl>English (South Africa) dictionary for hunspell</pl>
   <pt_BR>Dicionário Inglês (África do Sul) para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Inglês (África do Sul) para hunspell</pt>
   <ro>English (South Africa) dictionary for hunspell</ro>
   <ru>English (South Africa) dictionary for hunspell</ru>
   <sk>English (South Africa) dictionary for hunspell</sk>
   <sl>Angleški (južnoafriška) slovar za hunspell</sl>
   <so>English (South Africa) dictionary for hunspell</so>
   <sq>Fjalor anglisht (Afrikë e Jugut) për hunspell</sq>
   <sr>English (South Africa) dictionary for hunspell</sr>
   <sv>Engelsk (Sydafrika) ordbok för hunspell</sv>
   <th>English (South Africa) dictionary for hunspell</th>
   <tr>Hunspell için İngilizce (Güney Afrika) sözlüğü</tr>
   <uk>English (South Africa) dictionary for hunspell</uk>
   <vi>English (South Africa) dictionary for hunspell</vi>
   <zh_CN>English (South Africa) dictionary for hunspell</zh_CN>
   <zh_HK>English (South Africa) dictionary for hunspell</zh_HK>
   <zh_TW>English (South Africa) dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-en-za
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-en-za
</uninstall_package_names>

</app>
