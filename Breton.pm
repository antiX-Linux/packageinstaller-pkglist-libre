<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Breton
</name>

<description>
   <am>Breton dictionary for hunspell</am>
   <ar>Breton dictionary for hunspell</ar>
   <be>Breton dictionary for hunspell</be>
   <bg>Breton dictionary for hunspell</bg>
   <bn>Breton dictionary for hunspell</bn>
   <ca>Diccionari Bretó per hunspell</ca>
   <cs>Breton dictionary for hunspell</cs>
   <da>Breton dictionary for hunspell</da>
   <de>Bretonisches Wörterbuch für “Hunspell”</de>
   <el>Βρετονικό λεξικό για hunspell</el>
   <en>Breton dictionary for hunspell</en>
   <es_ES>Diccionario Breton para hunspell</es_ES>
   <es>Diccionario Bretón para hunspell</es>
   <et>Breton dictionary for hunspell</et>
   <eu>Breton dictionary for hunspell</eu>
   <fa>Breton dictionary for hunspell</fa>
   <fil_PH>Breton dictionary for hunspell</fil_PH>
   <fi>Breton dictionary for hunspell</fi>
   <fr_BE>Breton dictionnaire pour hunspell</fr_BE>
   <fr>Breton dictionnaire pour hunspell</fr>
   <gl_ES>Breton dictionary for hunspell</gl_ES>
   <gu>Breton dictionary for hunspell</gu>
   <he_IL>Breton dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु ब्रेटन शब्दकोष</hi>
   <hr>Breton dictionary for hunspell</hr>
   <hu>Breton dictionary for hunspell</hu>
   <id>Breton dictionary for hunspell</id>
   <is>Breton dictionary for hunspell</is>
   <it>Dizionario bretone per hunspell</it>
   <ja>Hunspell 用ブルトン語辞書</ja>
   <kk>Breton dictionary for hunspell</kk>
   <ko>Breton dictionary for hunspell</ko>
   <ku>Breton dictionary for hunspell</ku>
   <lt>Breton dictionary for hunspell</lt>
   <mk>Breton dictionary for hunspell</mk>
   <mr>Breton dictionary for hunspell</mr>
   <nb_NO>Breton dictionary for hunspell</nb_NO>
   <nb>Bretonsk ordliste for hunspell</nb>
   <nl_BE>Breton dictionary for hunspell</nl_BE>
   <nl>Bretons woordenboek voor hunspell</nl>
   <or>Breton dictionary for hunspell</or>
   <pl>Breton dictionary for hunspell</pl>
   <pt_BR>Dicionário Bretão para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Bretão para hunspell</pt>
   <ro>Breton dictionary for hunspell</ro>
   <ru>Breton dictionary for hunspell</ru>
   <sk>Breton dictionary for hunspell</sk>
   <sl>Bretonski slovar za hunspell</sl>
   <so>Breton dictionary for hunspell</so>
   <sq>Fjalor bretonisht për hunspell</sq>
   <sr>Breton dictionary for hunspell</sr>
   <sv>Bretonsk ordbok för hunspell</sv>
   <th>Breton dictionary for hunspell</th>
   <tr>Hunspell için Bretonca sözlük</tr>
   <uk>Breton dictionary for hunspell</uk>
   <vi>Breton dictionary for hunspell</vi>
   <zh_CN>Breton dictionary for hunspell</zh_CN>
   <zh_HK>Breton dictionary for hunspell</zh_HK>
   <zh_TW>Breton dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-br
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-br
</uninstall_package_names>

</app>
