<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Graphics
</category>

<name>
Latest GIMP Full
</name>

<description>
   <am>advanced picture editor- installs GIMP, help and plugins</am>
   <ar>advanced picture editor- installs GIMP, help and plugins</ar>
   <be>advanced picture editor- installs GIMP, help and plugins</be>
   <bg>advanced picture editor- installs GIMP, help and plugins</bg>
   <bn>advanced picture editor- installs GIMP, help and plugins</bn>
   <ca>editor gràfic avançat; instal·la GIMP, l'ajuda i connectors</ca>
   <cs>advanced picture editor- installs GIMP, help and plugins</cs>
   <da>avanceret billedredigering - installerer GIMP, hjælp og plugins</da>
   <de>Erweiterter Bildeditor - installiert GIMP, Hilfe und Plugins</de>
   <el>Επεξεργαστής εικόνων - εγκαθιστά το GIMP, τη βοήθεια και τα πρόσθετα</el>
   <en>advanced picture editor- installs GIMP, help and plugins</en>
   <es_ES>Editor de imágenes avanzado. Instala GIMP, ayuda y plugins.</es_ES>
   <es>Editor de imágenes avanzado. Instala GIMP, ayuda y plugins.</es>
   <et>advanced picture editor- installs GIMP, help and plugins</et>
   <eu>advanced picture editor- installs GIMP, help and plugins</eu>
   <fa>advanced picture editor- installs GIMP, help and plugins</fa>
   <fil_PH>advanced picture editor- installs GIMP, help and plugins</fil_PH>
   <fi>edistynyt kuvankäsittely - asentaa GIMP:in, apuoppaan ja liitännäiset</fi>
   <fr_BE>Éditeur d'image avancé - installe Gimp, l'aide et les plugins</fr_BE>
   <fr>Éditeur d'image avancé - installe Gimp, l'aide et les plugins</fr>
   <gl_ES>Editor de imaxe avanzado - instala o GIMP, axuda e suplementos</gl_ES>
   <gu>advanced picture editor- installs GIMP, help and plugins</gu>
   <he_IL>advanced picture editor- installs GIMP, help and plugins</he_IL>
   <hi>विस्तृत चित्र संपादक - GIMP, सहायता व प्लगिन इंस्टॉल होगा</hi>
   <hr>advanced picture editor- installs GIMP, help and plugins</hr>
   <hu>advanced picture editor- installs GIMP, help and plugins</hu>
   <id>advanced picture editor- installs GIMP, help and plugins</id>
   <is>advanced picture editor- installs GIMP, help and plugins</is>
   <it>Editor di immagini avanzato - installa GIMP, guida e plugins</it>
   <ja>高度な画像エディター - GIMP、ヘルプ、プラグインをインストールします</ja>
   <kk>advanced picture editor- installs GIMP, help and plugins</kk>
   <ko>advanced picture editor- installs GIMP, help and plugins</ko>
   <ku>advanced picture editor- installs GIMP, help and plugins</ku>
   <lt>išplėstinis paveikslų redaktorius - įdiegia GIMP, žinyną ir įskiepius</lt>
   <mk>advanced picture editor- installs GIMP, help and plugins</mk>
   <mr>advanced picture editor- installs GIMP, help and plugins</mr>
   <nb_NO>advanced picture editor- installs GIMP, help and plugins</nb_NO>
   <nb>avansert bilderedigering – installerer GIMP, hjelpetekst og programtillegg</nb>
   <nl_BE>advanced picture editor- installs GIMP, help and plugins</nl_BE>
   <nl>geavanceerde foto-editor - installeert GIMP, help en plugins</nl>
   <or>advanced picture editor- installs GIMP, help and plugins</or>
   <pl>zaawansowany edytor grafiki i zdjęć - instaluje program GIMP, wtyczki i pliki pomocy</pl>
   <pt_BR>Editor de imagem avançado - instala o GIMP, ajuda e suplementos (plugins)</pt_BR>
   <pt>Editor de imagem avançado - instala o GIMP, ajuda e suplementos (plugins)</pt>
   <ro>advanced picture editor- installs GIMP, help and plugins</ro>
   <ru>Продвинутый графический редактор - установка GIMP, справка и плагины</ru>
   <sk>advanced picture editor- installs GIMP, help and plugins</sk>
   <sl>napredni urejevalnik slik - namesti GIMP, pomoč in vsatvke</sl>
   <so>advanced picture editor- installs GIMP, help and plugins</so>
   <sq>Përpunues i thelluar fotosh - instalon vetëm GIMP-in</sq>
   <sr>advanced picture editor- installs GIMP, help and plugins</sr>
   <sv>avancerad bildredigerare - installerar GIMP, hjälp och plugins</sv>
   <th>โปรแกรมตัดต่อถาพขั้นสูง - ติดตั้ง GIMP, ความช่วยเหลือ และ ปลั๊กอิน</th>
   <tr>gelişmiş resim düzenleyici - yalnızca GIMP yükler</tr>
   <uk>розширений редактор зображень - встановлює GIMP, довідку та плагіни</uk>
   <vi>advanced picture editor- installs GIMP, help and plugins</vi>
   <zh_CN>高级图像编辑器 - 安装 GIMP，包括帮助文件和插件</zh_CN>
   <zh_HK>advanced picture editor- installs GIMP, help and plugins</zh_HK>
   <zh_TW>advanced picture editor- installs GIMP, help and plugins</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gimp-data
gimp-data-extras
gimp-cbmplugs
gimp-dds
gimp-gluas
gimp-gmic
gimp-lensfun
gimp-texturize
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gimp
gimp-data
gimp-data-extras
gimp-cbmplugs
gimp-dds
gimp-gluas
gimp-gmic
gimp-lensfun
gimp-texturize
</uninstall_package_names>
</app>
