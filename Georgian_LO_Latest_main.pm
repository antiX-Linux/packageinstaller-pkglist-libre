<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Georgian_LO_Latest_main
</name>

<description>
   <am>Georgian Language Meta-Package for LibreOffice</am>
   <ar>Georgian Language Meta-Package for LibreOffice</ar>
   <be>Georgian Language Meta-Package for LibreOffice</be>
   <bg>Georgian Language Meta-Package for LibreOffice</bg>
   <bn>Georgian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Georgià per LibreOffice</ca>
   <cs>Georgian Language Meta-Package for LibreOffice</cs>
   <da>Georgian Language Meta-Package for LibreOffice</da>
   <de>Georgisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Γεωργιανά</el>
   <en>Georgian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Georgiano para LibreOffice</es_ES>
   <es>Metapaquete de idioma Georgiano para LibreOffice</es>
   <et>Georgian Language Meta-Package for LibreOffice</et>
   <eu>Georgian Language Meta-Package for LibreOffice</eu>
   <fa>Georgian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Georgian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Georgialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet de langue géorgienne pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue géorgienne pour LibreOffice</fr>
   <gl_ES>Xeorxiano Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Georgian Language Meta-Package for LibreOffice</gu>
   <he_IL>Georgian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु जॉर्जियन भाषा मेटा-पैकेज</hi>
   <hr>Georgian Language Meta-Package for LibreOffice</hr>
   <hu>Georgian Language Meta-Package for LibreOffice</hu>
   <id>Georgian Language Meta-Package for LibreOffice</id>
   <is>Georgian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua georgiana per LibreOffice</it>
   <ja>LibreOffice用のグルジア語メタパッケージ</ja>
   <kk>Georgian Language Meta-Package for LibreOffice</kk>
   <ko>Georgian Language Meta-Package for LibreOffice</ko>
   <ku>Georgian Language Meta-Package for LibreOffice</ku>
   <lt>Georgian Language Meta-Package for LibreOffice</lt>
   <mk>Georgian Language Meta-Package for LibreOffice</mk>
   <mr>Georgian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Georgian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Georgisk språkpakke for LibreOffice</nb>
   <nl_BE>Georgian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Georgisch Taal Meta-Pakket voor LibreOffice</nl>
   <or>Georgian Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka gruzińskiego dla LibreOffice</pl>
   <pt_BR>Georgiana Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Georgiano Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Georgian Language Meta-Package for LibreOffice</ro>
   <ru>Georgian Language Meta-Package for LibreOffice</ru>
   <sk>Georgian Language Meta-Package for LibreOffice</sk>
   <sl>Gruzijski jezikovni meta-paket za LibreOffice</sl>
   <so>Georgian Language Meta-Package for LibreOffice</so>
   <sq>Përpunues i shpejtë dhe i peshës së lehtë, për IDE dhe tekst</sq>
   <sr>Georgian Language Meta-Package for LibreOffice</sr>
   <sv>Georgiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Georgian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Gürcüce Dili Üst-Paketi</tr>
   <uk>Georgian Language Meta-Package for LibreOffice</uk>
   <vi>Georgian Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 格鲁吉亚语语言包</zh_CN>
   <zh_HK>Georgian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Georgian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ka
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ka
libreoffice-gtk3
</uninstall_package_names>

</app>
