<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Arabic
</name>

<description>
   <am>Arabic dictionary for hunspell</am>
   <ar>Arabic dictionary for hunspell</ar>
   <be>Arabic dictionary for hunspell</be>
   <bg>Arabic dictionary for hunspell</bg>
   <bn>Arabic dictionary for hunspell</bn>
   <ca>Diccionari Àrab per hunspell</ca>
   <cs>Arabic dictionary for hunspell</cs>
   <da>Arabic dictionary for hunspell</da>
   <de>Arabisches Wörterbuch für “Hunspell”</de>
   <el>Αραβικό λεξικό για hunspell</el>
   <en>Arabic dictionary for hunspell</en>
   <es_ES>Diccionario árabe para hunspell</es_ES>
   <es>Diccionario Árabe para hunspell</es>
   <et>Arabic dictionary for hunspell</et>
   <eu>Arabic dictionary for hunspell</eu>
   <fa>Arabic dictionary for hunspell</fa>
   <fil_PH>Arabic dictionary for hunspell</fil_PH>
   <fi>Arabic dictionary for hunspell</fi>
   <fr_BE>Arabe dictionnaire pour hunspell</fr_BE>
   <fr>Arabe dictionnaire pour hunspell</fr>
   <gl_ES>Arabic dictionary for hunspell</gl_ES>
   <gu>Arabic dictionary for hunspell</gu>
   <he_IL>Arabic dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु अरबी शब्दकोष</hi>
   <hr>Arabic dictionary for hunspell</hr>
   <hu>Arabic dictionary for hunspell</hu>
   <id>Arabic dictionary for hunspell</id>
   <is>Arabic dictionary for hunspell</is>
   <it>Dizionario arabo per hunspell</it>
   <ja>Hunspell 用アラビア語辞書</ja>
   <kk>Arabic dictionary for hunspell</kk>
   <ko>Arabic dictionary for hunspell</ko>
   <ku>Arabic dictionary for hunspell</ku>
   <lt>Arabic dictionary for hunspell</lt>
   <mk>Arabic dictionary for hunspell</mk>
   <mr>Arabic dictionary for hunspell</mr>
   <nb_NO>Arabic dictionary for hunspell</nb_NO>
   <nb>Arabisk ordliste for hunspell</nb>
   <nl_BE>Arabic dictionary for hunspell</nl_BE>
   <nl>Arabisch woordenboek voor hunspell</nl>
   <or>Arabic dictionary for hunspell</or>
   <pl>Arabic dictionary for hunspell</pl>
   <pt_BR>Dicionário Árabe para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Árabe para hunspell</pt>
   <ro>Arabic dictionary for hunspell</ro>
   <ru>Arabic dictionary for hunspell</ru>
   <sk>Arabic dictionary for hunspell</sk>
   <sl>Arabski slovar za hunspell</sl>
   <so>Arabic dictionary for hunspell</so>
   <sq>Fjalor arabisht për hunspell</sq>
   <sr>Arabic dictionary for hunspell</sr>
   <sv>Arabisk ordbok för hunspell</sv>
   <th>Arabic dictionary for hunspell</th>
   <tr>Hunspell için arapça sözlük</tr>
   <uk>Arabic dictionary for hunspell</uk>
   <vi>Arabic dictionary for hunspell</vi>
   <zh_CN>Arabic dictionary for hunspell</zh_CN>
   <zh_HK>Arabic dictionary for hunspell</zh_HK>
   <zh_TW>Arabic dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-ar
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ar
</uninstall_package_names>

</app>
