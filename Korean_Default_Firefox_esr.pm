<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Korean_Default_Firefox_esr
</name>

<description>
   <am>Korean localisation of Firefox ESR</am>
   <ar>Korean localisation of Firefox ESR</ar>
   <be>Korean localisation of Firefox ESR</be>
   <bg>Korean localisation of Firefox ESR</bg>
   <bn>Korean localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Coreà</ca>
   <cs>Korean localisation of Firefox ESR</cs>
   <da>Korean localisation of Firefox ESR</da>
   <de>Koreanische Lokalisation von “Firefox ESR”</de>
   <el>Κορεάτικα για Firefox ESR</el>
   <en>Korean localisation of Firefox ESR</en>
   <es_ES>Localización Coreano de Firefox ESR</es_ES>
   <es>Localización Coreano de Firefox ESR</es>
   <et>Korean localisation of Firefox ESR</et>
   <eu>Korean localisation of Firefox ESR</eu>
   <fa>Korean localisation of Firefox ESR</fa>
   <fil_PH>Korean localisation of Firefox ESR</fil_PH>
   <fi>Korean localisation of Firefox ESR</fi>
   <fr_BE>Localisation en coréen pour Firefox ESR</fr_BE>
   <fr>Localisation en coréen pour Firefox ESR</fr>
   <gl_ES>Korean localisation of Firefox ESR</gl_ES>
   <gu>Korean localisation of Firefox ESR</gu>
   <he_IL>Korean localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का कोरियाई संस्करण</hi>
   <hr>Korean localisation of Firefox ESR</hr>
   <hu>Korean localisation of Firefox ESR</hu>
   <id>Korean localisation of Firefox ESR</id>
   <is>Korean localisation of Firefox ESR</is>
   <it>Localizzazione coreana di Firefox ESR</it>
   <ja>韓国（朝鮮）語版 Firefox ESR</ja>
   <kk>Korean localisation of Firefox ESR</kk>
   <ko>Korean localisation of Firefox ESR</ko>
   <ku>Korean localisation of Firefox ESR</ku>
   <lt>Korean localisation of Firefox ESR</lt>
   <mk>Korean localisation of Firefox ESR</mk>
   <mr>Korean localisation of Firefox ESR</mr>
   <nb_NO>Korean localisation of Firefox ESR</nb_NO>
   <nb>Koreansk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Korean localisation of Firefox ESR</nl_BE>
   <nl>Korean localisation of Firefox ESR</nl>
   <or>Korean localisation of Firefox ESR</or>
   <pl>Korean localisation of Firefox ESR</pl>
   <pt_BR>Coreano Localização para o Firefox ESR</pt_BR>
   <pt>Coreano Localização para Firefox ESR</pt>
   <ro>Korean localisation of Firefox ESR</ro>
   <ru>Korean localisation of Firefox ESR</ru>
   <sk>Korean localisation of Firefox ESR</sk>
   <sl>Korejske krajevne nastavitve za Firefox ESR</sl>
   <so>Korean localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në koreançe</sq>
   <sr>Korean localisation of Firefox ESR</sr>
   <sv>Koreansk lokalisering av Firefox ESR</sv>
   <th>Korean localisation of Firefox ESR</th>
   <tr>Firefox ESR Korece yerelleştirmesi</tr>
   <uk>Korean localisation of Firefox ESR</uk>
   <vi>Korean localisation of Firefox ESR</vi>
   <zh_CN>Korean localisation of Firefox ESR</zh_CN>
   <zh_HK>Korean localisation of Firefox ESR</zh_HK>
   <zh_TW>Korean localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ko
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ko
</uninstall_package_names>

</app>
