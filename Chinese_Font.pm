<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Chinese_Font
</name>

<description>
   <am>Chinese fonts packages</am>
   <ar>Chinese fonts packages</ar>
   <be>Chinese fonts packages</be>
   <bg>Chinese fonts packages</bg>
   <bn>Chinese fonts packages</bn>
   <ca>Paquets de tipus de lletra en Xinès</ca>
   <cs>Chinese fonts packages</cs>
   <da>Kinesisk skrifttyper-pakke</da>
   <de>Chinesische Schriftenpakete</de>
   <el>Πακέτα κινεζικών γραμματοσειρών</el>
   <en>Chinese fonts packages</en>
   <es_ES>Paquetes de fuentes chinas</es_ES>
   <es>Paquetes de fuentes chinas</es>
   <et>Chinese fonts packages</et>
   <eu>Chinese fonts packages</eu>
   <fa>Chinese fonts packages</fa>
   <fil_PH>Chinese fonts packages</fil_PH>
   <fi>Kiinalaiset kirjasinpaketit</fi>
   <fr_BE>Paquets pour polices chinoises</fr_BE>
   <fr>Paquets pour polices chinoises</fr>
   <gl_ES>Paquetes de fontes do chinés</gl_ES>
   <gu>Chinese fonts packages</gu>
   <he_IL>חבילות גופנים סיניים</he_IL>
   <hi>चीनी मुद्रलिपि पैकेज</hi>
   <hr>Chinese fonts packages</hr>
   <hu>Chinese fonts packages</hu>
   <id>Chinese fonts packages</id>
   <is>Chinese fonts packages</is>
   <it>Pacchetti di fonts per il cinese</it>
   <ja>中国語用のフォントパッケージ</ja>
   <kk>Chinese fonts packages</kk>
   <ko>Chinese fonts packages</ko>
   <ku>Chinese fonts packages</ku>
   <lt>Kinų šriftų paketai</lt>
   <mk>Chinese fonts packages</mk>
   <mr>Chinese fonts packages</mr>
   <nb_NO>Chinese fonts packages</nb_NO>
   <nb>Pakke med kinesiske skrifttyper</nb>
   <nl_BE>Chinese fonts packages</nl_BE>
   <nl>Chinese font pakketten</nl>
   <or>Chinese fonts packages</or>
   <pl>pakiety z chińskimi fontami (czcionkami)</pl>
   <pt_BR>Pacotes de Fontes Chinês</pt_BR>
   <pt>Chinês Pacotes de fontes</pt>
   <ro>Chinese fonts packages</ro>
   <ru>Пакеты китайских шрифтов</ru>
   <sk>Chinese fonts packages</sk>
   <sl>Kitajski paketi s pisavami</sl>
   <so>Chinese fonts packages</so>
   <sq>Paketash shkronjash për kinezçen</sq>
   <sr>Chinese fonts packages</sr>
   <sv>Kinesiska typsnitts-paket</sv>
   <th>แพ็กเกจฟอนต์ภาษาจีน</th>
   <tr>Çince yazı tipi paketleri</tr>
   <uk>Пакунок з Китайськими шрифтами</uk>
   <vi>Chinese fonts packages</vi>
   <zh_CN>中文字体包</zh_CN>
   <zh_HK>Chinese fonts packages</zh_HK>
   <zh_TW>Chinese fonts packages</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
xfonts-wqy
fonts-arphic-ukai
fonts-arphic-uming
fonts-wqy-microhei
fonts-wqy-zenhei
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
xfonts-wqy
fonts-arphic-ukai
fonts-arphic-uming
fonts-wqy-microhei
fonts-wqy-zenhei
</uninstall_package_names>

</app>
