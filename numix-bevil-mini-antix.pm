<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Icons
</category>

<name>
Numix Bevel Mini antiX
</name>

<description>
   <am>Basic set of Numix Bevel icons for antiX Linux</am>
   <ar>Basic set of Numix Bevel icons for antiX Linux</ar>
   <be>Basic set of Numix Bevel icons for antiX Linux</be>
   <bg>Basic set of Numix Bevel icons for antiX Linux</bg>
   <bn>Basic set of Numix Bevel icons for antiX Linux</bn>
   <ca>Joc bàsic d'icones Numix Bevel per antiX Linux</ca>
   <cs>Basic set of Numix Bevel icons for antiX Linux</cs>
   <da>Basic set of Numix Bevel icons for antiX Linux</da>
   <de>Basis-Auswahl von Piktogrammen (Ugs.: “Icons”) für antiX im Stil “Numix Bevel”</de>
   <el>Βασικό σύνολο εικονιδίων Numix Bevel για antiX Linux</el>
   <en>Basic set of Numix Bevel icons for antiX Linux</en>
   <es_ES>Conjunto básico de iconos de Numix Bevel para antiX Linux</es_ES>
   <es>Conjunto básico de iconos de Numix Bevel para antiX Linux</es>
   <et>Basic set of Numix Bevel icons for antiX Linux</et>
   <eu>Basic set of Numix Bevel icons for antiX Linux</eu>
   <fa>Basic set of Numix Bevel icons for antiX Linux</fa>
   <fil_PH>Basic set of Numix Bevel icons for antiX Linux</fil_PH>
   <fi>Basic set of Numix Bevel icons for antiX Linux</fi>
   <fr_BE>Icônes pour antiX dans le style "Numix Bevel" (kit de base)</fr_BE>
   <fr>Icônes pour antiX dans le style "Numix Bevel" (kit de base)</fr>
   <gl_ES>Basic set of Numix Bevel icons for antiX Linux</gl_ES>
   <gu>Basic set of Numix Bevel icons for antiX Linux</gu>
   <he_IL>Basic set of Numix Bevel icons for antiX Linux</he_IL>
   <hi>एंटी-एक्स लिनक्स हेतु Numix Bevel आइकन का सामान संग्रह</hi>
   <hr>Basic set of Numix Bevel icons for antiX Linux</hr>
   <hu>Basic set of Numix Bevel icons for antiX Linux</hu>
   <id>Basic set of Numix Bevel icons for antiX Linux</id>
   <is>Basic set of Numix Bevel icons for antiX Linux</is>
   <it>Set di base di icone Numix Bevel per antiX Linux</it>
   <ja>antiX Linux用 Numix Bevel アイコンの基本セット</ja>
   <kk>Basic set of Numix Bevel icons for antiX Linux</kk>
   <ko>Basic set of Numix Bevel icons for antiX Linux</ko>
   <ku>Basic set of Numix Bevel icons for antiX Linux</ku>
   <lt>Basic set of Numix Bevel icons for antiX Linux</lt>
   <mk>Basic set of Numix Bevel icons for antiX Linux</mk>
   <mr>Basic set of Numix Bevel icons for antiX Linux</mr>
   <nb_NO>Basic set of Numix Bevel icons for antiX Linux</nb_NO>
   <nb>Grunnleggende sett med Numix Bevel-ikoner for antiX Linux</nb>
   <nl_BE>Basic set of Numix Bevel icons for antiX Linux</nl_BE>
   <nl>Basic set of Numix Bevel icons for antiX Linux</nl>
   <or>Basic set of Numix Bevel icons for antiX Linux</or>
   <pl>Basic set of Numix Bevel icons for antiX Linux</pl>
   <pt_BR>Conjunto básico de ícones Numix Bevel para o antiX Linux</pt_BR>
   <pt>Conjunto básico de ícones Numix Bevel para o antiX Linux</pt>
   <ro>Basic set of Numix Bevel icons for antiX Linux</ro>
   <ru>Basic set of Numix Bevel icons for antiX Linux</ru>
   <sk>Basic set of Numix Bevel icons for antiX Linux</sk>
   <sl>Osnovni nabor Numix Bevill ikon za antiX Linux</sl>
   <so>Basic set of Numix Bevel icons for antiX Linux</so>
   <sq>Grup elementar ikonash Numix Bevel për antiX Linux</sq>
   <sr>Basic set of Numix Bevel icons for antiX Linux</sr>
   <sv>Bas-set av Numix Bevel ikoner för antiX Linux</sv>
   <th>Basic set of Numix Bevel icons for antiX Linux</th>
   <tr>antiX Linux Numix Bevel simgelerinin temel takımı</tr>
   <uk>Basic set of Numix Bevel icons for antiX Linux</uk>
   <vi>Basic set of Numix Bevel icons for antiX Linux</vi>
   <zh_CN>Basic set of Numix Bevel icons for antiX Linux</zh_CN>
   <zh_HK>Basic set of Numix Bevel icons for antiX Linux</zh_HK>
   <zh_TW>Basic set of Numix Bevel icons for antiX Linux</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
numix-bevel-mini-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
numix-bevel-mini-antix
</uninstall_package_names>
</app>
