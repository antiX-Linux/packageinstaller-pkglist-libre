<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Vietnamese_Default_Firefox_esr
</name>

<description>
   <am>Vietnamese localisation Firefox ESR</am>
   <ar>Vietnamese localisation Firefox ESR</ar>
   <be>Vietnamese localisation Firefox ESR</be>
   <bg>Vietnamese localisation Firefox ESR</bg>
   <bn>Vietnamese localisation Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Vietnamita</ca>
   <cs>Vietnamese localisation Firefox ESR</cs>
   <da>Vietnamese localisation Firefox ESR</da>
   <de>Vietnamesische Lokalisation von “Firefox ESR”</de>
   <el>Βιετναμέζικα για Firefox ESR</el>
   <en>Vietnamese localisation Firefox ESR</en>
   <es_ES>Localización Vietnamita de Firefox ESR</es_ES>
   <es>Localización Vietnamita de Firefox ESR</es>
   <et>Vietnamese localisation Firefox ESR</et>
   <eu>Vietnamese localisation Firefox ESR</eu>
   <fa>Vietnamese localisation Firefox ESR</fa>
   <fil_PH>Vietnamese localisation Firefox ESR</fil_PH>
   <fi>Vietnamese localisation Firefox ESR</fi>
   <fr_BE>Localisation en vietnamien pour Firefox ESR</fr_BE>
   <fr>Localisation en vietnamien pour Firefox ESR</fr>
   <gl_ES>Vietnamese localisation Firefox ESR</gl_ES>
   <gu>Vietnamese localisation Firefox ESR</gu>
   <he_IL>Vietnamese localisation Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का वियतनामी संस्करण</hi>
   <hr>Vietnamese localisation Firefox ESR</hr>
   <hu>Vietnamese localisation Firefox ESR</hu>
   <id>Vietnamese localisation Firefox ESR</id>
   <is>Vietnamese localisation Firefox ESR</is>
   <it>Localizzazione vietnamita di Firefox ESR</it>
   <ja>ベトナム語版 Firefox ESR</ja>
   <kk>Vietnamese localisation Firefox ESR</kk>
   <ko>Vietnamese localisation Firefox ESR</ko>
   <ku>Vietnamese localisation Firefox ESR</ku>
   <lt>Vietnamese localisation Firefox ESR</lt>
   <mk>Vietnamese localisation Firefox ESR</mk>
   <mr>Vietnamese localisation Firefox ESR</mr>
   <nb_NO>Vietnamese localisation Firefox ESR</nb_NO>
   <nb>Vietnamesisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Vietnamese localisation Firefox ESR</nl_BE>
   <nl>Vietnamese localisation Firefox ESR</nl>
   <or>Vietnamese localisation Firefox ESR</or>
   <pl>Vietnamese localisation Firefox ESR</pl>
   <pt_BR>Vietnamita Localização para o Firefox ESR</pt_BR>
   <pt>Vietnamita Localização para Firefox ESR</pt>
   <ro>Vietnamese localisation Firefox ESR</ro>
   <ru>Vietnamese localisation Firefox ESR</ru>
   <sk>Vietnamese localisation Firefox ESR</sk>
   <sl>Vietnamske krajevne nastavitve za Firefox-ESR</sl>
   <so>Vietnamese localisation Firefox ESR</so>
   <sq>Përkthim i Firefox-it ESR në vietnamisht</sq>
   <sr>Vietnamese localisation Firefox ESR</sr>
   <sv>Vietnamesisk lokalisering av Firefox ESR</sv>
   <th>Vietnamese localisation Firefox ESR</th>
   <tr>Firefox ESR Vietnamca yerelleştirmesi</tr>
   <uk>Vietnamese localisation Firefox ESR</uk>
   <vi>Vietnamese localisation Firefox ESR</vi>
   <zh_CN>Vietnamese localisation Firefox ESR</zh_CN>
   <zh_HK>Vietnamese localisation Firefox ESR</zh_HK>
   <zh_TW>Vietnamese localisation Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-vi
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-vi
</uninstall_package_names>

</app>
