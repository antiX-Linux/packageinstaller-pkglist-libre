<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Chinese_CN_LO_main
</name>

<description>
   <am>Chinese_simplified localisation of LibreOffice</am>
   <ar>Chinese_simplified localisation of LibreOffice</ar>
   <be>Chinese_simplified localisation of LibreOffice</be>
   <bg>Chinese_simplified localisation of LibreOffice</bg>
   <bn>Chinese_simplified localisation of LibreOffice</bn>
   <ca>Localització de LibreOffice en Xinès simplificat</ca>
   <cs>Chinese_simplified localisation of LibreOffice</cs>
   <da>Kinesisk (forenklet) oversættelse af LibreOffice</da>
   <de>Chinesisch (vereinfacht) Lokalisierung von LibreOffice</de>
   <el>LibreOffice στα Κινεζικά</el>
   <en>Chinese_simplified localisation of LibreOffice</en>
   <es_ES>Localización Chino simplificado de LibreOffice</es_ES>
   <es>Localización Chino simplificado de LibreOffice</es>
   <et>Chinese_simplified localisation of LibreOffice</et>
   <eu>Chinese_simplified localisation of LibreOffice</eu>
   <fa>Chinese_simplified localisation of LibreOffice</fa>
   <fil_PH>Chinese_simplified localisation of LibreOffice</fil_PH>
   <fi>Yksinkertaistetun kiinankielen kotoistus LibreOffice:lle</fi>
   <fr_BE>Localisation en chinois-simplifié pour LibreOffice</fr_BE>
   <fr>Localisation en chinois-simplifié pour LibreOffice</fr>
   <gl_ES>Localización de LibreOffice en chinés simplificado</gl_ES>
   <gu>Chinese_simplified localisation of LibreOffice</gu>
   <he_IL>Chinese_simplified localisation of LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस का सरल चीनी संस्करण</hi>
   <hr>Chinese_simplified localisation of LibreOffice</hr>
   <hu>Chinese_simplified localisation of LibreOffice</hu>
   <id>Chinese_simplified localisation of LibreOffice</id>
   <is>Chinese_simplified localisation of LibreOffice</is>
   <it>Localizzazione in cinese_semplificato di LibreOffice</it>
   <ja>LibreOffice の簡体中国語版</ja>
   <kk>Chinese_simplified localisation of LibreOffice</kk>
   <ko>Chinese_simplified localisation of LibreOffice</ko>
   <ku>Chinese_simplified localisation of LibreOffice</ku>
   <lt>Chinese_simplified localisation of LibreOffice</lt>
   <mk>Chinese_simplified localisation of LibreOffice</mk>
   <mr>Chinese_simplified localisation of LibreOffice</mr>
   <nb_NO>Chinese_simplified localisation of LibreOffice</nb_NO>
   <nb>Kinesisk (forenklet) lokaltilpassing av LibreOffice</nb>
   <nl_BE>Chinese_simplified localisation of LibreOffice</nl_BE>
   <nl>Vereenvoudigd Chinese lokalisatie van LibreOffice</nl>
   <or>Chinese_simplified localisation of LibreOffice</or>
   <pl>Chiński uproszczony lokalizacja LibreOffice</pl>
   <pt_BR>Chinês Simplificado Localização para o LibreOffice</pt_BR>
   <pt>Chinês_simplificado Localização para LibreOffice</pt>
   <ro>Chinese_simplified localisation of LibreOffice</ro>
   <ru>Chinese_simplified localisation of LibreOffice</ru>
   <sk>Chinese_simplified localisation of LibreOffice</sk>
   <sl>Kitajske (poenostavljeno) krajevne nastavitve za LibreOffice</sl>
   <so>Chinese_simplified localisation of LibreOffice</so>
   <sq>Përkthimi i LibreOffice-it në kinezçe të thjeshtuar</sq>
   <sr>Chinese_simplified localisation of LibreOffice</sr>
   <sv>Kinesisk_förenklad lokalisering av LibreOffice</sv>
   <th>Chinese_CN localisation ของ LibreOffice</th>
   <tr>LibreOffice'in Basitleştirilmiş_Çince yerelleştirmesi</tr>
   <uk>Chinese_simplified локалізація LibreOffice</uk>
   <vi>Chinese_simplified localisation of LibreOffice</vi>
   <zh_CN>LibreOffice 简体中文语言包</zh_CN>
   <zh_HK>Chinese_simplified localisation of LibreOffice</zh_HK>
   <zh_TW>Chinese_simplified localisation of LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-zh-cn
libreoffice-help-zh-cn
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-zh-cn
libreoffice-help-zh-cn
libreoffice-gtk3
</uninstall_package_names>

</app>
