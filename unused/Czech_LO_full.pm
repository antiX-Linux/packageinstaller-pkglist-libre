<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Czech_LO_full
</name>

<description>
   <am>Czech LibreOffice Language Meta-Package</am>
   <ar>Czech LibreOffice Language Meta-Package</ar>
   <be>Czech LibreOffice Language Meta-Package</be>
   <bg>Czech LibreOffice Language Meta-Package</bg>
   <bn>Czech LibreOffice Language Meta-Package</bn>
   <ca>Localització de LibreOffice en Txec</ca>
   <cs>Czech LibreOffice Language Meta-Package</cs>
   <da>Tjekkisk LibreOffice sprog-metapakke</da>
   <de>Tschechisches LibreOffice Meta-Paket</de>
   <el>Libreoffice στα Τσέχικα</el>
   <en>Czech LibreOffice Language Meta-Package</en>
   <es_ES>Meta-Paquete de Idioma Checo LibreOffice</es_ES>
   <es>Metapaquete de Idioma Checo LibreOffice</es>
   <et>Czech LibreOffice Language Meta-Package</et>
   <eu>Czech LibreOffice Language Meta-Package</eu>
   <fa>Czech LibreOffice Language Meta-Package</fa>
   <fil_PH>Czech LibreOffice Language Meta-Package</fil_PH>
   <fi>Tsekkiläinen metatieto-paketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet langue tchèque pour LibreOffice</fr_BE>
   <fr>Méta-Paquet langue tchèque pour LibreOffice</fr>
   <gl_ES>Checo Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Czech LibreOffice Language Meta-Package</gu>
   <he_IL>Czech LibreOffice Language Meta-Package</he_IL>
   <hi>लिब्रे-ऑफिस हेतु चेक भाषा मेटा-पैकेज</hi>
   <hr>Czech LibreOffice Language Meta-Package</hr>
   <hu>Czech LibreOffice Language Meta-Package</hu>
   <id>Czech LibreOffice Language Meta-Package</id>
   <is>Czech LibreOffice Language Meta-Package</is>
   <it>Meta-pacchetto della lingua ceca per LibreOffice</it>
   <ja>チェコ語のLibreOffice言語メタパッケージ</ja>
   <kk>Czech LibreOffice Language Meta-Package</kk>
   <ko>Czech LibreOffice Language Meta-Package</ko>
   <ku>Czech LibreOffice Language Meta-Package</ku>
   <lt>Czech LibreOffice Language Meta-Package</lt>
   <mk>Czech LibreOffice Language Meta-Package</mk>
   <mr>Czech LibreOffice Language Meta-Package</mr>
   <nb_NO>Czech LibreOffice Language Meta-Package</nb_NO>
   <nb>Tsjekkisk språkpakke for LibreOffice</nb>
   <nl_BE>Czech LibreOffice Language Meta-Package</nl_BE>
   <nl>Tjechische LibreOffice Taal Meta-Pakket</nl>
   <or>Czech LibreOffice Language Meta-Package</or>
   <pl>Czeski metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Checo Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Checo Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Czech LibreOffice Language Meta-Package</ro>
   <ru>Czech LibreOffice Language Meta-Package</ru>
   <sk>Czech LibreOffice Language Meta-Package</sk>
   <sl>Češki jezikovni metapaket za LibreOffice</sl>
   <so>Czech LibreOffice Language Meta-Package</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në çekisht</sq>
   <sr>Czech LibreOffice Language Meta-Package</sr>
   <sv>Tjeckisk LibreOffice Språk Meta-Paket</sv>
   <th>Meta-Package ภาษา Czech สำหรับ LibreOffice</th>
   <tr>LibreOffice Çekçe Dili Üst-Paketi</tr>
   <uk>Czech LibreOffice Language Meta-Package</uk>
   <vi>Czech LibreOffice Language Meta-Package</vi>
   <zh_CN>LibreOffice 捷克语语言包</zh_CN>
   <zh_HK>Czech LibreOffice Language Meta-Package</zh_HK>
   <zh_TW>Czech LibreOffice Language Meta-Package</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-cs
libreoffice-help-cs
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-cs
libreoffice-help-cs
libreoffice-gtk3
</uninstall_package_names>

</app>
