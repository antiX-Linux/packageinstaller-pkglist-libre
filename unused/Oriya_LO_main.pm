<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Oriya_LO_main
</name>

<description>
   <am>Oriya Language Meta-Package for LibreOffice</am>
   <ar>Oriya Language Meta-Package for LibreOffice</ar>
   <be>Oriya Language Meta-Package for LibreOffice</be>
   <bg>Oriya Language Meta-Package for LibreOffice</bg>
   <bn>Oriya Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Oriya per LibreOffice</ca>
   <cs>Oriya Language Meta-Package for LibreOffice</cs>
   <da>Oriya Language Meta-Package for LibreOffice</da>
   <de>Indisches Metapaket von “LibreOffice” für die Sprache “Odia”</de>
   <el>LibreOffice στα Oriya</el>
   <en>Oriya Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Oriya para LibreOffice</es_ES>
   <es>Metapaquete de idioma Oriya para LibreOffice</es>
   <et>Oriya Language Meta-Package for LibreOffice</et>
   <eu>Oriya Language Meta-Package for LibreOffice</eu>
   <fa>Oriya Language Meta-Package for LibreOffice</fa>
   <fil_PH>Oriya Language Meta-Package for LibreOffice</fil_PH>
   <fi>Oriya Language Meta-Package for LibreOffice</fi>
   <fr_BE>Méta-Paquet de langue oriya (Inde) pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de langue oriya (Inde) pour LibreOffice</fr>
   <gl_ES>Oriya Language Meta-Package for LibreOffice</gl_ES>
   <gu>Oriya Language Meta-Package for LibreOffice</gu>
   <he_IL>Oriya Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु ओड़िया भाषा मेटा-पैकेज</hi>
   <hr>Oriya Language Meta-Package for LibreOffice</hr>
   <hu>Oriya Language Meta-Package for LibreOffice</hu>
   <id>Oriya Language Meta-Package for LibreOffice</id>
   <is>Oriya Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto linguaggio oriya per LibreOffice</it>
   <ja>LibreOffice用オリヤー語メタパッケージ</ja>
   <kk>Oriya Language Meta-Package for LibreOffice</kk>
   <ko>Oriya Language Meta-Package for LibreOffice</ko>
   <ku>Oriya Language Meta-Package for LibreOffice</ku>
   <lt>Oriya Language Meta-Package for LibreOffice</lt>
   <mk>Oriya Language Meta-Package for LibreOffice</mk>
   <mr>Oriya Language Meta-Package for LibreOffice</mr>
   <nb_NO>Oriya Language Meta-Package for LibreOffice</nb_NO>
   <nb>Oriyansk språkpakke for LibreOffice</nb>
   <nl_BE>Oriya Language Meta-Package for LibreOffice</nl_BE>
   <nl>Oriya Language Meta-Package for LibreOffice</nl>
   <or>Oriya Language Meta-Package for LibreOffice</or>
   <pl>Oriya Language Meta-Package for LibreOffice</pl>
   <pt_BR>Oriá Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Oriá Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Oriya Language Meta-Package for LibreOffice</ro>
   <ru>Oriya Language Meta-Package for LibreOffice</ru>
   <sk>Oriya Language Meta-Package for LibreOffice</sk>
   <sl>Orijski jezikovni meta-paket za LibreOffice</sl>
   <so>Oriya Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në orijase</sq>
   <sr>Oriya Language Meta-Package for LibreOffice</sr>
   <sv>Oriya Språk Meta-Paket för LibreOffice</sv>
   <th>Oriya Language Meta-Package for LibreOffice</th>
   <tr>LibreOffice için Odia Dili Üst-Paketi</tr>
   <uk>Oriya Language Meta-Package for LibreOffice</uk>
   <vi>Oriya Language Meta-Package for LibreOffice</vi>
   <zh_CN>Oriya Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Oriya Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Oriya Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-or
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-or
libreoffice-gtk3
</uninstall_package_names>

</app>
