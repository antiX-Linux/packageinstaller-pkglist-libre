<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Tsonga_LO_full
</name>

<description>
   <am>Tsonga Language Meta-Package for LibreOffice</am>
   <ar>Tsonga Language Meta-Package for LibreOffice</ar>
   <be>Tsonga Language Meta-Package for LibreOffice</be>
   <bg>Tsonga Language Meta-Package for LibreOffice</bg>
   <bn>Tsonga Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Tsonga per LibreOffice</ca>
   <cs>Tsonga Language Meta-Package for LibreOffice</cs>
   <da>Tsonga Language Meta-Package for LibreOffice</da>
   <de>Tsonga Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Tsonga</el>
   <en>Tsonga Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Tsonga para LibreOffice</es_ES>
   <es>Metapaquete idioma Songa para LibreOffice</es>
   <et>Tsonga Language Meta-Package for LibreOffice</et>
   <eu>Tsonga Language Meta-Package for LibreOffice</eu>
   <fa>Tsonga Language Meta-Package for LibreOffice</fa>
   <fil_PH>Tsonga Language Meta-Package for LibreOffice</fil_PH>
   <fi>Tsongankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue Tsonga pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue Tsonga pour LibreOffice</fr>
   <gl_ES>Tsonga Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Tsonga Language Meta-Package for LibreOffice</gu>
   <he_IL>Tsonga Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु सोंगा भाषा मेटा-पैकेज</hi>
   <hr>Tsonga Language Meta-Package for LibreOffice</hr>
   <hu>Tsonga Language Meta-Package for LibreOffice</hu>
   <id>Tsonga Language Meta-Package for LibreOffice</id>
   <is>Tsonga Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua tsonga per LibreOffice</it>
   <ja>LibreOffice用のツォンガ語メタパッケージ</ja>
   <kk>Tsonga Language Meta-Package for LibreOffice</kk>
   <ko>Tsonga Language Meta-Package for LibreOffice</ko>
   <ku>Tsonga Language Meta-Package for LibreOffice</ku>
   <lt>Tsonga Language Meta-Package for LibreOffice</lt>
   <mk>Tsonga Language Meta-Package for LibreOffice</mk>
   <mr>Tsonga Language Meta-Package for LibreOffice</mr>
   <nb_NO>Tsonga Language Meta-Package for LibreOffice</nb_NO>
   <nb>Tsonga språkpakke for LibreOffice</nb>
   <nl_BE>Tsonga Language Meta-Package for LibreOffice</nl_BE>
   <nl>Tsonga Taal Meta-Pakket voor LibreOffice</nl>
   <or>Tsonga Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Tsonga dla LibreOffice</pl>
   <pt_BR>Tsonga Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Tsonga Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Tsonga Language Meta-Package for LibreOffice</ro>
   <ru>Tsonga Language Meta-Package for LibreOffice</ru>
   <sk>Tsonga Language Meta-Package for LibreOffice</sk>
   <sl>Tsongški jezikovni meta-paket za LibreOffice</sl>
   <so>Tsonga Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në conga</sq>
   <sr>Tsonga Language Meta-Package for LibreOffice</sr>
   <sv>Tsonga Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Tsonga สำหรับ LibreOffice</th>
   <tr>LibreOffice için Tsonga Dili Üst-Paketi</tr>
   <uk>Tsonga Language Meta-Package for LibreOffice</uk>
   <vi>Tsonga Language Meta-Package for LibreOffice</vi>
   <zh_CN>Tsonga Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Tsonga Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Tsonga Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-ts
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-ts
libreoffice-gtk3
</uninstall_package_names>

</app>
