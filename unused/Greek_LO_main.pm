<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Greek_LO_main
</name>

<description>
   <am>Greek Language Meta-Package for LibreOffice</am>
   <ar>Greek Language Meta-Package for LibreOffice</ar>
   <be>Greek Language Meta-Package for LibreOffice</be>
   <bg>Greek Language Meta-Package for LibreOffice</bg>
   <bn>Greek Language Meta-Package for LibreOffice</bn>
   <ca>Meta-paquet de llengua grega per LibreOffice</ca>
   <cs>Greek Language Meta-Package for LibreOffice</cs>
   <da>Græsk sprog-metapakke til LibreOffice</da>
   <de>Griechisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Ελληνικά</el>
   <en>Greek Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Idioma Griego para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Griego para LibreOffice</es>
   <et>Greek Language Meta-Package for LibreOffice</et>
   <eu>Greek Language Meta-Package for LibreOffice</eu>
   <fa>Greek Language Meta-Package for LibreOffice</fa>
   <fil_PH>Greek Language Meta-Package for LibreOffice</fil_PH>
   <fi>Kreikankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue grecque pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue grecque pour LibreOffice</fr>
   <gl_ES>Grego Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Greek Language Meta-Package for LibreOffice</gu>
   <he_IL>Greek Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु यूनानी भाषा मेटा-पैकेज</hi>
   <hr>Greek Language Meta-Package for LibreOffice</hr>
   <hu>Greek Language Meta-Package for LibreOffice</hu>
   <id>Greek Language Meta-Package for LibreOffice</id>
   <is>Greek Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua greca per LibreOffice</it>
   <ja>LibreOffice用のギリシャ語メタパッケージ</ja>
   <kk>Greek Language Meta-Package for LibreOffice</kk>
   <ko>Greek Language Meta-Package for LibreOffice</ko>
   <ku>Greek Language Meta-Package for LibreOffice</ku>
   <lt>Greek Language Meta-Package for LibreOffice</lt>
   <mk>Greek Language Meta-Package for LibreOffice</mk>
   <mr>Greek Language Meta-Package for LibreOffice</mr>
   <nb_NO>Greek Language Meta-Package for LibreOffice</nb_NO>
   <nb>Gresk språkpakke for LibreOffice</nb>
   <nl_BE>Greek Language Meta-Package for LibreOffice</nl_BE>
   <nl>Griekse Taal Meta-Pakket voor LibreOffice</nl>
   <or>Greek Language Meta-Package for LibreOffice</or>
   <pl>Grecki metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Grego Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Grego Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Greek Language Meta-Package for LibreOffice</ro>
   <ru>Greek Language Meta-Package for LibreOffice</ru>
   <sk>Greek Language Meta-Package for LibreOffice</sk>
   <sl>Grški jezikovni meta-paket za LibreOffice</sl>
   <so>Greek Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në greqisht</sq>
   <sr>Greek Language Meta-Package for LibreOffice</sr>
   <sv>Grekiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Greek สำหรับ LibreOffice</th>
   <tr>LibreOffice için Yunanca Dili Üst-Paketi</tr>
   <uk>Greek Language Meta-Package for LibreOffice</uk>
   <vi>Greek Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 希腊语语言包</zh_CN>
   <zh_HK>Greek Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Greek Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-el
libreoffice-help-el
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-el
libreoffice-help-el
libreoffice-gtk3
</uninstall_package_names>

</app>
