<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Kurmanji_LO_main
</name>

<description>
   <am>Kurmanji Language Meta-Package for LibreOffice</am>
   <ar>Kurmanji Language Meta-Package for LibreOffice</ar>
   <be>Kurmanji Language Meta-Package for LibreOffice</be>
   <bg>Kurmanji Language Meta-Package for LibreOffice</bg>
   <bn>Kurmanji Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Kurmanji per LibreOffice</ca>
   <cs>Kurmanji Language Meta-Package for LibreOffice</cs>
   <da>Kurmanji Language Meta-Package for LibreOffice</da>
   <de>Kurmanji Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Kurmanji</el>
   <en>Kurmanji Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma kurmanji para LibreOffice</es_ES>
   <es>Metapaquete de idioma Kurmanji para LibreOffice</es>
   <et>Kurmanji Language Meta-Package for LibreOffice</et>
   <eu>Kurmanji Language Meta-Package for LibreOffice</eu>
   <fa>Kurmanji Language Meta-Package for LibreOffice</fa>
   <fil_PH>Kurmanji Language Meta-Package for LibreOffice</fil_PH>
   <fi>Kurmandzinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue kurmanji pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue kurmanji pour LibreOffice</fr>
   <gl_ES>Curmanji Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Kurmanji Language Meta-Package for LibreOffice</gu>
   <he_IL>Kurmanji Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु कुरमानी भाषा मेटा-पैकेज</hi>
   <hr>Kurmanji Language Meta-Package for LibreOffice</hr>
   <hu>Kurmanji Language Meta-Package for LibreOffice</hu>
   <id>Kurmanji Language Meta-Package for LibreOffice</id>
   <is>Kurmanji Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua kurmanji per LibreOffice</it>
   <ja>LibreOffice用のクルマンジ言語メタパッケージ</ja>
   <kk>Kurmanji Language Meta-Package for LibreOffice</kk>
   <ko>Kurmanji Language Meta-Package for LibreOffice</ko>
   <ku>Kurmanji Language Meta-Package for LibreOffice</ku>
   <lt>Kurmanji Language Meta-Package for LibreOffice</lt>
   <mk>Kurmanji Language Meta-Package for LibreOffice</mk>
   <mr>Kurmanji Language Meta-Package for LibreOffice</mr>
   <nb_NO>Kurmanji Language Meta-Package for LibreOffice</nb_NO>
   <nb>Kurmanji språkpakke for LibreOffice</nb>
   <nl_BE>Kurmanji Language Meta-Package for LibreOffice</nl_BE>
   <nl>Kurmanji Taal Meta-Pakket voor LibreOffice</nl>
   <or>Kurmanji Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Kurmanji dla LibreOffice</pl>
   <pt_BR>Curmânji Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Curmanji Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Kurmanji Language Meta-Package for LibreOffice</ro>
   <ru>Kurmanji Language Meta-Package for LibreOffice</ru>
   <sk>Kurmanji Language Meta-Package for LibreOffice</sk>
   <sl>Kurmanjski jezikovni meta-paket za LibreOffice</sl>
   <so>Kurmanji Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në kurmanxhi</sq>
   <sr>Kurmanji Language Meta-Package for LibreOffice</sr>
   <sv>Kurmanji Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Kurmanji สำหรับ LibreOffice</th>
   <tr>LibreOffice için Kürtçe Dili Üst-Paketi</tr>
   <uk>Kurmanji Language Meta-Package for LibreOffice</uk>
   <vi>Kurmanji Language Meta-Package for LibreOffice</vi>
   <zh_CN>Kurmanji Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Kurmanji Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Kurmanji Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-kmr
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-kmr
libreoffice-gtk3
</uninstall_package_names>

</app>
