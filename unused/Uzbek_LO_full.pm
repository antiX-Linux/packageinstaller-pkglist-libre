<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Uzbek_LO_full
</name>

<description>
   <am>Uzbek Language Meta-Package for LibreOffice</am>
   <ar>Uzbek Language Meta-Package for LibreOffice</ar>
   <be>Uzbek Language Meta-Package for LibreOffice</be>
   <bg>Uzbek Language Meta-Package for LibreOffice</bg>
   <bn>Uzbek Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Uzbek per LibreOffice</ca>
   <cs>Uzbek Language Meta-Package for LibreOffice</cs>
   <da>Uzbek Language Meta-Package for LibreOffice</da>
   <de>Usbekisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Uzbek</el>
   <en>Uzbek Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Uzbeko para LibreOffice</es_ES>
   <es>Metapaquete de idioma Uzbeko para LibreOffice</es>
   <et>Uzbek Language Meta-Package for LibreOffice</et>
   <eu>Uzbek Language Meta-Package for LibreOffice</eu>
   <fa>Uzbek Language Meta-Package for LibreOffice</fa>
   <fil_PH>Uzbek Language Meta-Package for LibreOffice</fil_PH>
   <fi>Uzbekinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue ouzbèque pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue ouzbèque pour LibreOffice</fr>
   <gl_ES>Uzbeco Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Uzbek Language Meta-Package for LibreOffice</gu>
   <he_IL>Uzbek Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु उज़्बेक भाषा मेटा-पैकेज</hi>
   <hr>Uzbek Language Meta-Package for LibreOffice</hr>
   <hu>Uzbek Language Meta-Package for LibreOffice</hu>
   <id>Uzbek Language Meta-Package for LibreOffice</id>
   <is>Uzbek Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua uzbeka per LibreOffice</it>
   <ja>LibreOffice用のウズベク語メタパッケージ</ja>
   <kk>Uzbek Language Meta-Package for LibreOffice</kk>
   <ko>Uzbek Language Meta-Package for LibreOffice</ko>
   <ku>Uzbek Language Meta-Package for LibreOffice</ku>
   <lt>Uzbek Language Meta-Package for LibreOffice</lt>
   <mk>Uzbek Language Meta-Package for LibreOffice</mk>
   <mr>Uzbek Language Meta-Package for LibreOffice</mr>
   <nb_NO>Uzbek Language Meta-Package for LibreOffice</nb_NO>
   <nb>Uzbekisk språkpakke for LibreOffice</nb>
   <nl_BE>Uzbek Language Meta-Package for LibreOffice</nl_BE>
   <nl>Oezbeekse Taal Meta-Pakket voor LibreOffice</nl>
   <or>Uzbek Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka uzbeckiego dla LibreOffice</pl>
   <pt_BR>Usbeque Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Uzbeque Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Uzbek Language Meta-Package for LibreOffice</ro>
   <ru>Uzbek Language Meta-Package for LibreOffice</ru>
   <sk>Uzbek Language Meta-Package for LibreOffice</sk>
   <sl>Uzbeški jezikovni meta-paket za LibreOffice</sl>
   <so>Uzbek Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në uzbeke</sq>
   <sr>Uzbek Language Meta-Package for LibreOffice</sr>
   <sv>Uzbekiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Uzbek สำหรับ LibreOffice</th>
   <tr>LibreOffice için Özbekçe Dili Üst-Paketi</tr>
   <uk>Uzbek Language Meta-Package for LibreOffice</uk>
   <vi>Uzbek Language Meta-Package for LibreOffice</vi>
   <zh_CN>Uzbek Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Uzbek Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Uzbek Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-uz
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-uz
libreoffice-gtk3
</uninstall_package_names>

</app>
