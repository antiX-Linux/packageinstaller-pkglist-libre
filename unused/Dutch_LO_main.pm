<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Dutch_LO_main
</name>

<description>
   <am>Dutch LibreOffice Language Meta-Package</am>
   <ar>Dutch LibreOffice Language Meta-Package</ar>
   <be>Dutch LibreOffice Language Meta-Package</be>
   <bg>Dutch LibreOffice Language Meta-Package</bg>
   <bn>Dutch LibreOffice Language Meta-Package</bn>
   <ca>Meta-paquet per LibreOffice en Holandès</ca>
   <cs>Dutch LibreOffice Language Meta-Package</cs>
   <da>Hollandsk LibreOffice sprog-metapakke</da>
   <de>Niederländisches LibreOffice Meta-Paket</de>
   <el>Libreoffice στα Ολλανδικά</el>
   <en>Dutch LibreOffice Language Meta-Package</en>
   <es_ES>Meta-Paquete de Idioma Holandés de LibreOffice</es_ES>
   <es>Metapaquete de Idioma Holandés LibreOffice</es>
   <et>Dutch LibreOffice Language Meta-Package</et>
   <eu>Dutch LibreOffice Language Meta-Package</eu>
   <fa>Dutch LibreOffice Language Meta-Package</fa>
   <fil_PH>Dutch LibreOffice Language Meta-Package</fil_PH>
   <fi>Hollanninkielinen metatieto-paketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet langue néerlandaise pour LibreOffice</fr_BE>
   <fr>Méta-Paquet langue néerlandaise pour LibreOffice</fr>
   <gl_ES>Neerlandés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Dutch LibreOffice Language Meta-Package</gu>
   <he_IL>Dutch LibreOffice Language Meta-Package</he_IL>
   <hi>लिब्रे-ऑफिस हेतु डच भाषा मेटा-पैकेज</hi>
   <hr>Dutch LibreOffice Language Meta-Package</hr>
   <hu>Dutch LibreOffice Language Meta-Package</hu>
   <id>Dutch LibreOffice Language Meta-Package</id>
   <is>Dutch LibreOffice Language Meta-Package</is>
   <it>Meta-pacchetto della lingua olandese per LibreOffice</it>
   <ja>オランダ語の LibreOffice言語メタパッケージ</ja>
   <kk>Dutch LibreOffice Language Meta-Package</kk>
   <ko>Dutch LibreOffice Language Meta-Package</ko>
   <ku>Dutch LibreOffice Language Meta-Package</ku>
   <lt>Dutch LibreOffice Language Meta-Package</lt>
   <mk>Dutch LibreOffice Language Meta-Package</mk>
   <mr>Dutch LibreOffice Language Meta-Package</mr>
   <nb_NO>Dutch LibreOffice Language Meta-Package</nb_NO>
   <nb>Nederlandsk språkpakke for LibreOffice</nb>
   <nl_BE>Dutch LibreOffice Language Meta-Package</nl_BE>
   <nl>Nederlandse LibreOffice Taal Meta-Pakket</nl>
   <or>Dutch LibreOffice Language Meta-Package</or>
   <pl>Holenderski metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Holandês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Holandês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Dutch LibreOffice Language Meta-Package</ro>
   <ru>Dutch LibreOffice Language Meta-Package</ru>
   <sk>Dutch LibreOffice Language Meta-Package</sk>
   <sl>Dutch LibreOffice Language Meta-Package</sl>
   <so>Dutch LibreOffice Language Meta-Package</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në holandisht</sq>
   <sr>Dutch LibreOffice Language Meta-Package</sr>
   <sv>Holländska LibreOffice Språk Meta-Paket</sv>
   <th>Meta-Package ภาษา Dutch สำหรับ LibreOffice</th>
   <tr>Flemenkçe LibreOffice Dil Üst-Paketi</tr>
   <uk>Dutch LibreOffice Language Meta-Package</uk>
   <vi>Dutch LibreOffice Language Meta-Package</vi>
   <zh_CN>LibreOffice 荷兰语语言包</zh_CN>
   <zh_HK>Dutch LibreOffice Language Meta-Package</zh_HK>
   <zh_TW>Dutch LibreOffice Language Meta-Package</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-nl
libreoffice-help-nl
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-nl
libreoffice-help-nl
libreoffice-gtk3
</uninstall_package_names>

</app>
