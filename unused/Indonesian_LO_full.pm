<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Indonesian_LO_full
</name>

<description>
   <am>Indonesian Language Meta-Package for LibreOffice</am>
   <ar>Indonesian Language Meta-Package for LibreOffice</ar>
   <be>Indonesian Language Meta-Package for LibreOffice</be>
   <bg>Indonesian Language Meta-Package for LibreOffice</bg>
   <bn>Indonesian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Indonesi per LibreOffice</ca>
   <cs>Indonesian Language Meta-Package for LibreOffice</cs>
   <da>Indonesian Language Meta-Package for LibreOffice</da>
   <de>Indonesisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Ινδονησιακά</el>
   <en>Indonesian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Indonesio para LibreOffice</es_ES>
   <es>Metapaquete de idioma Indonesio para LibreOffice</es>
   <et>Indonesian Language Meta-Package for LibreOffice</et>
   <eu>Indonesian Language Meta-Package for LibreOffice</eu>
   <fa>Indonesian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Indonesian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Indonesialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue indonésienne pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue indonésienne pour LibreOffice</fr>
   <gl_ES>Indonesio Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Indonesian Language Meta-Package for LibreOffice</gu>
   <he_IL>Indonesian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु इन्डोनेशियाई भाषा मेटा-पैकेज</hi>
   <hr>Indonesian Language Meta-Package for LibreOffice</hr>
   <hu>Indonesian Language Meta-Package for LibreOffice</hu>
   <id>Indonesian Language Meta-Package for LibreOffice</id>
   <is>Indonesian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua indonesiana per LibreOffice</it>
   <ja>LibreOffice用のインドネシア語メタパッケージ</ja>
   <kk>Indonesian Language Meta-Package for LibreOffice</kk>
   <ko>Indonesian Language Meta-Package for LibreOffice</ko>
   <ku>Indonesian Language Meta-Package for LibreOffice</ku>
   <lt>Indonesian Language Meta-Package for LibreOffice</lt>
   <mk>Indonesian Language Meta-Package for LibreOffice</mk>
   <mr>Indonesian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Indonesian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Indonesisk språkpakke for LibreOffice</nb>
   <nl_BE>Indonesian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Indonesisch Taal Meta-Pakket voor LibreOffice</nl>
   <or>Indonesian Language Meta-Package for LibreOffice</or>
   <pl>Indonezyjski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Indonésio Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Indonésio Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Indonesian Language Meta-Package for LibreOffice</ro>
   <ru>Indonesian Language Meta-Package for LibreOffice</ru>
   <sk>Indonesian Language Meta-Package for LibreOffice</sk>
   <sl>Indonezijski jezikovni meta-paket za LibreOffice</sl>
   <so>Indonesian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në indonezisht</sq>
   <sr>Indonesian Language Meta-Package for LibreOffice</sr>
   <sv>Indonesiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Indonesian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Endonezce Dili Üst-Paketi</tr>
   <uk>Indonesian Language Meta-Package for LibreOffice</uk>
   <vi>Indonesian Language Meta-Package for LibreOffice</vi>
   <zh_CN>Indonesian Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Indonesian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Indonesian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-id
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-id
libreoffice-gtk3
</uninstall_package_names>

</app>
