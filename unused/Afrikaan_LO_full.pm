<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Afrikaan_LO_full
</name>

<description>
   <am>Afrikaans Language Meta-Package for LibreOffice</am>
   <ar>Afrikaans Language Meta-Package for LibreOffice</ar>
   <be>Afrikaans Language Meta-Package for LibreOffice</be>
   <bg>Afrikaans Language Meta-Package for LibreOffice</bg>
   <bn>Afrikaans Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Afrikaans per LibreOffice</ca>
   <cs>Afrikaans Language Meta-Package for LibreOffice</cs>
   <da>Afrikaans Language Meta-Package for LibreOffice</da>
   <de>Afrikaans Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Αφρικανικά</el>
   <en>Afrikaans Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma africano para LibreOffice</es_ES>
   <es>Metapaquete de idioma Africano para LibreOffice</es>
   <et>Afrikaans Language Meta-Package for LibreOffice</et>
   <eu>Afrikaans Language Meta-Package for LibreOffice</eu>
   <fa>Afrikaans Language Meta-Package for LibreOffice</fa>
   <fil_PH>Afrikaans Language Meta-Package for LibreOffice</fil_PH>
   <fi>Afrikaansin kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de langue afrikanns pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de langue afrikanns pour LibreOffice</fr>
   <gl_ES>Africánder meta-paquete de idioma para LiberOffice</gl_ES>
   <gu>Afrikaans Language Meta-Package for LibreOffice</gu>
   <he_IL>חבילת על לשפת אפריקאנס עבור LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु अफ़्रीकी डच भाषा मेटा-पैकेज</hi>
   <hr>Afrikaans Language Meta-Package for LibreOffice</hr>
   <hu>Afrikaans Language Meta-Package for LibreOffice</hu>
   <id>Afrikaans Language Meta-Package for LibreOffice</id>
   <is>Afrikaans Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua afrikaans per LibreOffice</it>
   <ja>LibreOffice 用のアフリカーンス語のメタパッケージ</ja>
   <kk>Afrikaans Language Meta-Package for LibreOffice</kk>
   <ko>Afrikaans Language Meta-Package for LibreOffice</ko>
   <ku>Afrikaans Language Meta-Package for LibreOffice</ku>
   <lt>Afrikaans Language Meta-Package for LibreOffice</lt>
   <mk>Afrikaans Language Meta-Package for LibreOffice</mk>
   <mr>Afrikaans Language Meta-Package for LibreOffice</mr>
   <nb_NO>Afrikaans Language Meta-Package for LibreOffice</nb_NO>
   <nb>Afrikaans språkpakke for LibreOffice</nb>
   <nl_BE>Afrikaans Language Meta-Package for LibreOffice</nl_BE>
   <nl>Afrikaans Taal Meta-Pakket voor LibreOffice</nl>
   <or>Afrikaans Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Afrikaans dla LibreOffice</pl>
   <pt_BR>Africâner Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Meta-Pacote de Idioma Africander para o LibreOffice</pt>
   <ro>Afrikaans Language Meta-Package for LibreOffice</ro>
   <ru>Afrikaans Language Meta-Package for LibreOffice</ru>
   <sk>Afrikaans Language Meta-Package for LibreOffice</sk>
   <sl>Afrikaan jezikovni meta-paket za LibreOffice</sl>
   <so>Afrikaans Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në afrikaans</sq>
   <sr>Afrikaans Language Meta-Package for LibreOffice</sr>
   <sv>Afrikaans Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Afrikaans สำหรับ LibreOffice</th>
   <tr>LibreOffice için Afrikaan Dili Üst-Paketi</tr>
   <uk>Afrikaans Language Meta-Package for LibreOffice</uk>
   <vi>Afrikaans Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 南非语语言包</zh_CN>
   <zh_HK>Afrikaans Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Afrikaans Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-af
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-af
</uninstall_package_names>

</app>
