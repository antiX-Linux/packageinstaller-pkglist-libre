<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Tajik_LO_main
</name>

<description>
   <am>Tajik Language Meta-Package for LibreOffice</am>
   <ar>Tajik Language Meta-Package for LibreOffice</ar>
   <be>Tajik Language Meta-Package for LibreOffice</be>
   <bg>Tajik Language Meta-Package for LibreOffice</bg>
   <bn>Tajik Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Tadjik per LibreOffice</ca>
   <cs>Tajik Language Meta-Package for LibreOffice</cs>
   <da>Tajik Language Meta-Package for LibreOffice</da>
   <de>Tadschikisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Tajik</el>
   <en>Tajik Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Tayiko para LibreOffice</es_ES>
   <es>Metapaquete de idioma Tayiko para LibreOffice</es>
   <et>Tajik Language Meta-Package for LibreOffice</et>
   <eu>Tajik Language Meta-Package for LibreOffice</eu>
   <fa>Tajik Language Meta-Package for LibreOffice</fa>
   <fil_PH>Tajik Language Meta-Package for LibreOffice</fil_PH>
   <fi>Tadzikinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue tadjike pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue tadjike pour LibreOffice</fr>
   <gl_ES>Tajique Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Tajik Language Meta-Package for LibreOffice</gu>
   <he_IL>Tajik Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु ताजिक भाषा मेटा-पैकेज</hi>
   <hr>Tajik Language Meta-Package for LibreOffice</hr>
   <hu>Tajik Language Meta-Package for LibreOffice</hu>
   <id>Tajik Language Meta-Package for LibreOffice</id>
   <is>Tajik Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua tagica per LibreOffice</it>
   <ja>LibreOffice用のタジク語メタパッケージ</ja>
   <kk>Tajik Language Meta-Package for LibreOffice</kk>
   <ko>Tajik Language Meta-Package for LibreOffice</ko>
   <ku>Tajik Language Meta-Package for LibreOffice</ku>
   <lt>Tajik Language Meta-Package for LibreOffice</lt>
   <mk>Tajik Language Meta-Package for LibreOffice</mk>
   <mr>Tajik Language Meta-Package for LibreOffice</mr>
   <nb_NO>Tajik Language Meta-Package for LibreOffice</nb_NO>
   <nb>Tajik språkpakke for LibreOffice</nb>
   <nl_BE>Tajik Language Meta-Package for LibreOffice</nl_BE>
   <nl>Tajik Taal Meta-Pakket voor LibreOffice</nl>
   <or>Tajik Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka tadżyckiego dla LibreOffice</pl>
   <pt_BR>Tajique Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Tajique Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Tajik Language Meta-Package for LibreOffice</ro>
   <ru>Tajik Language Meta-Package for LibreOffice</ru>
   <sk>Tajik Language Meta-Package for LibreOffice</sk>
   <sl>Tadžiški jezikovni meta-paket za LibreOffice</sl>
   <so>Tajik Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në taxhike</sq>
   <sr>Tajik Language Meta-Package for LibreOffice</sr>
   <sv>Tajik Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Tajik สำหรับ LibreOffice</th>
   <tr>LibreOffice için Tacikçe Dili Üst-Paketi</tr>
   <uk>Tajik Language Meta-Package for LibreOffice</uk>
   <vi>Tajik Language Meta-Package for LibreOffice</vi>
   <zh_CN>Tajik Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Tajik Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Tajik Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-tg
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-tg
libreoffice-gtk3
</uninstall_package_names>

</app>
