<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
SAfrica_LO_main
</name>

<description>
   <am>Language Meta-Package for LibreOffice all 11 South African languages</am>
   <ar>Language Meta-Package for LibreOffice all 11 South African languages</ar>
   <be>Language Meta-Package for LibreOffice all 11 South African languages</be>
   <bg>Language Meta-Package for LibreOffice all 11 South African languages</bg>
   <bn>Language Meta-Package for LibreOffice all 11 South African languages</bn>
   <ca>Metapaquet dels 11 llenguatges Sudafricans per LibreOffice</ca>
   <cs>Language Meta-Package for LibreOffice all 11 South African languages</cs>
   <da>Language Meta-Package for LibreOffice all 11 South African languages</da>
   <de>LibreOffice Metapaket für alle 11 Südafrikanische Sprachen</de>
   <el>Μετα-πακέτο γλωσσών για το LibreOffice και οι 11 γλώσσες της Νότιας Αφρικής</el>
   <en>Language Meta-Package for LibreOffice all 11 South African languages</en>
   <es_ES>Meta-Paquete de idiomas para LibreOffice en los 11 idiomas sudafricanos</es_ES>
   <es>Metapaquete de idiomas para LibreOffice en los 11 idiomas sudafricanos</es>
   <et>Language Meta-Package for LibreOffice all 11 South African languages</et>
   <eu>Language Meta-Package for LibreOffice all 11 South African languages</eu>
   <fa>Language Meta-Package for LibreOffice all 11 South African languages</fa>
   <fil_PH>Language Meta-Package for LibreOffice all 11 South African languages</fil_PH>
   <fi>Language Meta-Package for LibreOffice all 11 South African languages</fi>
   <fr_BE>Métapackage linguistique pour Libre-Office, contenant les 11 langues de l'Afrique du Sud</fr_BE>
   <fr>Métapackage linguistique pour Libre-Office, contenant les 11 langues de l'Afrique du Sud</fr>
   <gl_ES>Language Meta-Package for LibreOffice all 11 South African languages</gl_ES>
   <gu>Language Meta-Package for LibreOffice all 11 South African languages</gu>
   <he_IL>Language Meta-Package for LibreOffice all 11 South African languages</he_IL>
   <hi>लिब्रे-ऑफिस हेतु सभी 11 दक्षिण अफ्रीकी भाषाओं का मेटा-पैकेज</hi>
   <hr>Language Meta-Package for LibreOffice all 11 South African languages</hr>
   <hu>Language Meta-Package for LibreOffice all 11 South African languages</hu>
   <id>Language Meta-Package for LibreOffice all 11 South African languages</id>
   <is>Language Meta-Package for LibreOffice all 11 South African languages</is>
   <it>Meta-pacchetto linguaggio per LibreOffice di tutti gli 11 linguaggi south african</it>
   <ja>南アフリカの全11言語用 LibreOffice 言語メタパッケージ</ja>
   <kk>Language Meta-Package for LibreOffice all 11 South African languages</kk>
   <ko>Language Meta-Package for LibreOffice all 11 South African languages</ko>
   <ku>Language Meta-Package for LibreOffice all 11 South African languages</ku>
   <lt>Language Meta-Package for LibreOffice all 11 South African languages</lt>
   <mk>Language Meta-Package for LibreOffice all 11 South African languages</mk>
   <mr>Language Meta-Package for LibreOffice all 11 South African languages</mr>
   <nb_NO>Language Meta-Package for LibreOffice all 11 South African languages</nb_NO>
   <nb>Språkpakke for LibreOffice med alle 11 sør-afrikanske språk</nb>
   <nl_BE>Language Meta-Package for LibreOffice all 11 South African languages</nl_BE>
   <nl>Language Meta-Package for LibreOffice all 11 South African languages</nl>
   <or>Language Meta-Package for LibreOffice all 11 South African languages</or>
   <pl>Language Meta-Package for LibreOffice all 11 South African languages</pl>
   <pt_BR>Meta-Pacote de Idioma para o LibreOffice de todos os 11 idiomas Sul-Africanos</pt_BR>
   <pt>Meta-Pacote de Idiomas para LibreOffice, todas as 11 línguas sul-africanas</pt>
   <ro>Language Meta-Package for LibreOffice all 11 South African languages</ro>
   <ru>Language Meta-Package for LibreOffice all 11 South African languages</ru>
   <sk>Language Meta-Package for LibreOffice all 11 South African languages</sk>
   <sl>Jezikovni meta-paket za vseh 11 južnoafriških jezikov</sl>
   <so>Language Meta-Package for LibreOffice all 11 South African languages</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice, për krejt 11 gjuhët e Afrikës së Jugut</sq>
   <sr>Language Meta-Package for LibreOffice all 11 South African languages</sr>
   <sv>Språk Meta-Paket för LibreOffice alla 11 Sydafrikanska språk</sv>
   <th>Language Meta-Package for LibreOffice all 11 South African languages</th>
   <tr>LibreOffice için 11 Güney Afrika dilinin tümü Üst-Paketi</tr>
   <uk>Language Meta-Package for LibreOffice all 11 South African languages</uk>
   <vi>Language Meta-Package for LibreOffice all 11 South African languages</vi>
   <zh_CN>Language Meta-Package for LibreOffice all 11 South African languages</zh_CN>
   <zh_HK>Language Meta-Package for LibreOffice all 11 South African languages</zh_HK>
   <zh_TW>Language Meta-Package for LibreOffice all 11 South African languages</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-za
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-za
libreoffice-gtk3
</uninstall_package_names>

</app>
