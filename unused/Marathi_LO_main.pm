<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Marathi_LO_main
</name>

<description>
   <am>Marathi Language Meta-Package for LibreOffice</am>
   <ar>Marathi Language Meta-Package for LibreOffice</ar>
   <be>Marathi Language Meta-Package for LibreOffice</be>
   <bg>Marathi Language Meta-Package for LibreOffice</bg>
   <bn>Marathi Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Marathi per LibreOffice</ca>
   <cs>Marathi Language Meta-Package for LibreOffice</cs>
   <da>Marathi Language Meta-Package for LibreOffice</da>
   <de>Marathi Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Marathi</el>
   <en>Marathi Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Maratí para LibreOffice</es_ES>
   <es>Metapaquete de idioma Maratí para LibreOffice</es>
   <et>Marathi Language Meta-Package for LibreOffice</et>
   <eu>Marathi Language Meta-Package for LibreOffice</eu>
   <fa>Marathi Language Meta-Package for LibreOffice</fa>
   <fil_PH>Marathi Language Meta-Package for LibreOffice</fil_PH>
   <fi>Marathinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue marathi pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue marathi pour LibreOffice</fr>
   <gl_ES>Marata Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Marathi Language Meta-Package for LibreOffice</gu>
   <he_IL>Marathi Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु मराठी भाषा मेटा-पैकेज</hi>
   <hr>Marathi Language Meta-Package for LibreOffice</hr>
   <hu>Marathi Language Meta-Package for LibreOffice</hu>
   <id>Marathi Language Meta-Package for LibreOffice</id>
   <is>Marathi Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua marathi per LibreOffice</it>
   <ja>LibreOffice用のマラーティー語メタパッケージ</ja>
   <kk>Marathi Language Meta-Package for LibreOffice</kk>
   <ko>Marathi Language Meta-Package for LibreOffice</ko>
   <ku>Marathi Language Meta-Package for LibreOffice</ku>
   <lt>Marathi Language Meta-Package for LibreOffice</lt>
   <mk>Marathi Language Meta-Package for LibreOffice</mk>
   <mr>Marathi Language Meta-Package for LibreOffice</mr>
   <nb_NO>Marathi Language Meta-Package for LibreOffice</nb_NO>
   <nb>Marathi språkpakke for LibreOffice</nb>
   <nl_BE>Marathi Language Meta-Package for LibreOffice</nl_BE>
   <nl>Marathi Taal Meta-Pakket voor LibreOffice</nl>
   <or>Marathi Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Marathi dla LibreOffice</pl>
   <pt_BR>Marata Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Marata Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Marathi Language Meta-Package for LibreOffice</ro>
   <ru>Marathi Language Meta-Package for LibreOffice</ru>
   <sk>Marathi Language Meta-Package for LibreOffice</sk>
   <sl>Maratski jezikovni meta-paket za LibreOffice</sl>
   <so>Marathi Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në marati</sq>
   <sr>Marathi Language Meta-Package for LibreOffice</sr>
   <sv>Marathi Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Marathi สำหรับ LibreOffice</th>
   <tr>LibreOffice için Marathi Dili Üst-Paketi</tr>
   <uk>Marathi Language Meta-Package for LibreOffice</uk>
   <vi>Marathi Language Meta-Package for LibreOffice</vi>
   <zh_CN>Marathi Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Marathi Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Marathi Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-mr
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-mr
libreoffice-gtk3
</uninstall_package_names>

</app>
