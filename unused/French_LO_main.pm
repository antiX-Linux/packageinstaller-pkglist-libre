<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
French_LO_main
</name>

<description>
   <am>French Language Meta-Package for LibreOffice</am>
   <ar>French Language Meta-Package for LibreOffice</ar>
   <be>French Language Meta-Package for LibreOffice</be>
   <bg>French Language Meta-Package for LibreOffice</bg>
   <bn>French Language Meta-Package for LibreOffice</bn>
   <ca>Meta paquet per LibreOffice en llengua francesa</ca>
   <cs>French Language Meta-Package for LibreOffice</cs>
   <da>Fransk sprog-metapakke til LibreOffice</da>
   <de>Französisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Γαλλικά</el>
   <en>French Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Idioma Francés para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Francés para LibreOffice</es>
   <et>French Language Meta-Package for LibreOffice</et>
   <eu>French Language Meta-Package for LibreOffice</eu>
   <fa>French Language Meta-Package for LibreOffice</fa>
   <fil_PH>French Language Meta-Package for LibreOffice</fil_PH>
   <fi>Ranskalainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue française pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue française pour LibreOffice</fr>
   <gl_ES>Francés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>French Language Meta-Package for LibreOffice</gu>
   <he_IL>French Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु फ्रेंच भाषा मेटा-पैकेज</hi>
   <hr>French Language Meta-Package for LibreOffice</hr>
   <hu>French Language Meta-Package for LibreOffice</hu>
   <id>French Language Meta-Package for LibreOffice</id>
   <is>French Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua francese per LibreOffice</it>
   <ja>LibreOffice用のフランス語メタパッケージ</ja>
   <kk>French Language Meta-Package for LibreOffice</kk>
   <ko>French Language Meta-Package for LibreOffice</ko>
   <ku>French Language Meta-Package for LibreOffice</ku>
   <lt>French Language Meta-Package for LibreOffice</lt>
   <mk>French Language Meta-Package for LibreOffice</mk>
   <mr>French Language Meta-Package for LibreOffice</mr>
   <nb_NO>French Language Meta-Package for LibreOffice</nb_NO>
   <nb>Fransk språkpakke for LibreOffice</nb>
   <nl_BE>French Language Meta-Package for LibreOffice</nl_BE>
   <nl>Franse Taal Meta-Pakket voor LibreOffice</nl>
   <or>French Language Meta-Package for LibreOffice</or>
   <pl>Francuski metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Francês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Francês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>French Language Meta-Package for LibreOffice</ro>
   <ru>French Language Meta-Package for LibreOffice</ru>
   <sk>French Language Meta-Package for LibreOffice</sk>
   <sl>Fancoski jezikovni meta-paket za LibreOffice</sl>
   <so>French Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në frëngjisht</sq>
   <sr>French Language Meta-Package for LibreOffice</sr>
   <sv>Franskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา French สำหรับ LibreOffice</th>
   <tr>LibreOffice için Fransızca Dili Üst-Paketi</tr>
   <uk>French Language Meta-Package for LibreOffice</uk>
   <vi>French Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 法语语言包</zh_CN>
   <zh_HK>French Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>French Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-fr
libreoffice-help-fr
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-fr
libreoffice-help-fr
libreoffice-gtk3
</uninstall_package_names>

</app>
