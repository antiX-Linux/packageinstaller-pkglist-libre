<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Portuguese(BR)_LO_full
</name>

<description>
   <am>Portuguese(BR) Language Meta-Package for LibreOffice</am>
   <ar>Portuguese(BR) Language Meta-Package for LibreOffice</ar>
   <be>Portuguese(BR) Language Meta-Package for LibreOffice</be>
   <bg>Portuguese(BR) Language Meta-Package for LibreOffice</bg>
   <bn>Portuguese(BR) Language Meta-Package for LibreOffice</bn>
   <ca>Meta-paquet de llengua per LibreOffice en Portuguès (BR)</ca>
   <cs>Portuguese(BR) Language Meta-Package for LibreOffice</cs>
   <da>Portuguese(BR) Language Meta-Package for LibreOffice</da>
   <de>Sprach-Metapaket für “LibreOffice” für brasilianisches Portugiesisch</de>
   <el>Μετα-πακέτο γλωσσών στα Πορτογαλικά (BR) για το LibreOffice</el>
   <en>Portuguese(BR) Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Portugués (BR) para LibreOffice</es_ES>
   <es>Metapaquete de idioma Portugués (BR) para LibreOffice</es>
   <et>Portuguese(BR) Language Meta-Package for LibreOffice</et>
   <eu>Portuguese(BR) Language Meta-Package for LibreOffice</eu>
   <fa>Portuguese(BR) Language Meta-Package for LibreOffice</fa>
   <fil_PH>Portuguese(BR) Language Meta-Package for LibreOffice</fil_PH>
   <fi>Portuguese(BR) Language Meta-Package for LibreOffice</fi>
   <fr_BE>Méta-paquet langue portugaise(Brésil) pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue portugaise(Brésil) pour LibreOffice</fr>
   <gl_ES>Portuguese(BR) Language Meta-Package for LibreOffice</gl_ES>
   <gu>Portuguese(BR) Language Meta-Package for LibreOffice</gu>
   <he_IL>Portuguese(BR) Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु पुर्तगाली (ब्राज़ील) भाषा मेटा-पैकेज</hi>
   <hr>Portuguese(BR) Language Meta-Package for LibreOffice</hr>
   <hu>Portuguese(BR) Language Meta-Package for LibreOffice</hu>
   <id>Portuguese(BR) Language Meta-Package for LibreOffice</id>
   <is>Portuguese(BR) Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua portoghese(BR) per LibreOffice</it>
   <ja>LibreOffice用ポルトガル語（ポルトガル）メタパッケージ</ja>
   <kk>Portuguese(BR) Language Meta-Package for LibreOffice</kk>
   <ko>Portuguese(BR) Language Meta-Package for LibreOffice</ko>
   <ku>Portuguese(BR) Language Meta-Package for LibreOffice</ku>
   <lt>Portuguese(BR) Language Meta-Package for LibreOffice</lt>
   <mk>Portuguese(BR) Language Meta-Package for LibreOffice</mk>
   <mr>Portuguese(BR) Language Meta-Package for LibreOffice</mr>
   <nb_NO>Portuguese(BR) Language Meta-Package for LibreOffice</nb_NO>
   <nb>Brasiliansk portugisisk språkpakke for LibreOffice</nb>
   <nl_BE>Portuguese(BR) Language Meta-Package for LibreOffice</nl_BE>
   <nl>Portuguese(BR) Language Meta-Package for LibreOffice</nl>
   <or>Portuguese(BR) Language Meta-Package for LibreOffice</or>
   <pl>Portuguese(BR) Language Meta-Package for LibreOffice</pl>
   <pt_BR>Português (BR) Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Português(BR) Meta-pacote de idioma para o LibreOffice</pt>
   <ro>Portuguese(BR) Language Meta-Package for LibreOffice</ro>
   <ru>Portuguese(BR) Language Meta-Package for LibreOffice</ru>
   <sk>Portuguese(BR) Language Meta-Package for LibreOffice</sk>
   <sl>Portugalski (BR) jezikovni meta-paket za LibreOffice</sl>
   <so>Portuguese(BR) Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në portugalisht (BR)</sq>
   <sr>Portuguese(BR) Language Meta-Package for LibreOffice</sr>
   <sv>Portugisiskt(BR) Språk Meta-Paket för LibreOffice</sv>
   <th>Portuguese(BR) Language Meta-Package for LibreOffice</th>
   <tr>LibreOffice için Portekizce(BR) Dili Üst-Paketi</tr>
   <uk>Portuguese(BR) Language Meta-Package for LibreOffice</uk>
   <vi>Portuguese(BR) Language Meta-Package for LibreOffice</vi>
   <zh_CN>Portuguese(BR) Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Portuguese(BR) Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Portuguese(BR) Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-pt-br
libreoffice-help-pt-br
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-pt-br
libreoffice-help-pt-br
libreoffice-gtk3
</uninstall_package_names>

</app>
