<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Guarani_LO_full
</name>

<description>
   <am>Guarani Language Meta-Package for LibreOffice</am>
   <ar>Guarani Language Meta-Package for LibreOffice</ar>
   <be>Guarani Language Meta-Package for LibreOffice</be>
   <bg>Guarani Language Meta-Package for LibreOffice</bg>
   <bn>Guarani Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Guaraní per LibreOffice</ca>
   <cs>Guarani Language Meta-Package for LibreOffice</cs>
   <da>Guarani Language Meta-Package for LibreOffice</da>
   <de>Guarani Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Guarani</el>
   <en>Guarani Language Meta-Package for LibreOffice</en>
   <es_ES>Metapaquete de idioma Guaraní para LibreOffice</es_ES>
   <es>Metapaquete de idioma Guaraní para LibreOffice</es>
   <et>Guarani Language Meta-Package for LibreOffice</et>
   <eu>Guarani Language Meta-Package for LibreOffice</eu>
   <fa>Guarani Language Meta-Package for LibreOffice</fa>
   <fil_PH>Guarani Language Meta-Package for LibreOffice</fil_PH>
   <fi>Guaraninkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue guarani pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue guarani pour LibreOffice</fr>
   <gl_ES>Guaraní Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Guarani Language Meta-Package for LibreOffice</gu>
   <he_IL>Guarani Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु गूरानी भाषा मेटा-पैकेज</hi>
   <hr>Guarani Language Meta-Package for LibreOffice</hr>
   <hu>Guarani Language Meta-Package for LibreOffice</hu>
   <id>Guarani Language Meta-Package for LibreOffice</id>
   <is>Guarani Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua guarani per LibreOffice</it>
   <ja>LibreOffice用のグァラニ言語メタパッケージ</ja>
   <kk>Guarani Language Meta-Package for LibreOffice</kk>
   <ko>Guarani Language Meta-Package for LibreOffice</ko>
   <ku>Guarani Language Meta-Package for LibreOffice</ku>
   <lt>Guarani Language Meta-Package for LibreOffice</lt>
   <mk>Guarani Language Meta-Package for LibreOffice</mk>
   <mr>Guarani Language Meta-Package for LibreOffice</mr>
   <nb_NO>Guarani Language Meta-Package for LibreOffice</nb_NO>
   <nb>Guaraní språkpakke for LibreOffice</nb>
   <nl_BE>Guarani Language Meta-Package for LibreOffice</nl_BE>
   <nl>Guarani Taal Meta-Pakket voor LibreOffice</nl>
   <or>Guarani Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Guarani dla LibreOffice</pl>
   <pt_BR>Guarani Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Guarani Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Guarani Language Meta-Package for LibreOffice</ro>
   <ru>Guarani Language Meta-Package for LibreOffice</ru>
   <sk>Guarani Language Meta-Package for LibreOffice</sk>
   <sl>Guaranski jezikovni meta-paket za LibreOffice</sl>
   <so>Guarani Language Meta-Package for LibreOffice</so>
   <sq>Përkthimi i Firefox-it ESR në guaranisht</sq>
   <sr>Guarani Language Meta-Package for LibreOffice</sr>
   <sv>Guarani Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Guarani สำหรับ LibreOffice</th>
   <tr>LibreOffice için Guarani Dili Üst-Paketi</tr>
   <uk>Guarani Language Meta-Package for LibreOffice</uk>
   <vi>Guarani Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 瓜拉尼语语言包</zh_CN>
   <zh_HK>Guarani Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Guarani Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-gug
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-gug
libreoffice-gtk3
</uninstall_package_names>

</app>
