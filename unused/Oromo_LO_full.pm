<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Oromo_LO_full
</name>

<description>
   <am>Oromo Language Meta-Package for LibreOffice</am>
   <ar>Oromo Language Meta-Package for LibreOffice</ar>
   <be>Oromo Language Meta-Package for LibreOffice</be>
   <bg>Oromo Language Meta-Package for LibreOffice</bg>
   <bn>Oromo Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Oromo per LibreOffice</ca>
   <cs>Oromo Language Meta-Package for LibreOffice</cs>
   <da>Oromo Language Meta-Package for LibreOffice</da>
   <de>Oromo Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Oromo</el>
   <en>Oromo Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Oromo para LibreOffice</es_ES>
   <es>Metapaquete Oromo Language para LibreOffice</es>
   <et>Oromo Language Meta-Package for LibreOffice</et>
   <eu>Oromo Language Meta-Package for LibreOffice</eu>
   <fa>Oromo Language Meta-Package for LibreOffice</fa>
   <fil_PH>Oromo Language Meta-Package for LibreOffice</fil_PH>
   <fi>Oromonkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue oromo pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue oromo pour LibreOffice</fr>
   <gl_ES>Oromo Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Oromo Language Meta-Package for LibreOffice</gu>
   <he_IL>Oromo Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु ओरोमो भाषा मेटा-पैकेज</hi>
   <hr>Oromo Language Meta-Package for LibreOffice</hr>
   <hu>Oromo Language Meta-Package for LibreOffice</hu>
   <id>Oromo Language Meta-Package for LibreOffice</id>
   <is>Oromo Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua oromonica per LibreOffice</it>
   <ja>LibreOffice用オロモ言語メタパッケージ</ja>
   <kk>Oromo Language Meta-Package for LibreOffice</kk>
   <ko>Oromo Language Meta-Package for LibreOffice</ko>
   <ku>Oromo Language Meta-Package for LibreOffice</ku>
   <lt>Oromo Language Meta-Package for LibreOffice</lt>
   <mk>Oromo Language Meta-Package for LibreOffice</mk>
   <mr>Oromo Language Meta-Package for LibreOffice</mr>
   <nb_NO>Oromo Language Meta-Package for LibreOffice</nb_NO>
   <nb>Oromo språkpakke for LibreOffice</nb>
   <nl_BE>Oromo Language Meta-Package for LibreOffice</nl_BE>
   <nl>Oromo Taal Meta-Pakket voor LibreOffice</nl>
   <or>Oromo Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Oromo dla LibreOffice</pl>
   <pt_BR>Oromo Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Oromo Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Oromo Language Meta-Package for LibreOffice</ro>
   <ru>Oromo Language Meta-Package for LibreOffice</ru>
   <sk>Oromo Language Meta-Package for LibreOffice</sk>
   <sl>Oromski jezikovni meta-paket za LibreOffice</sl>
   <so>Oromo Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në oromoisht</sq>
   <sr>Oromo Language Meta-Package for LibreOffice</sr>
   <sv>Oromo Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Oromo สำหรับ LibreOffice</th>
   <tr>LibreOffice için Oromo Dili Üst-Paketi</tr>
   <uk>Oromo Language Meta-Package for LibreOffice</uk>
   <vi>Oromo Language Meta-Package for LibreOffice</vi>
   <zh_CN>Oromo Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Oromo Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Oromo Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-om
libreoffice-help-om
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-om
libreoffice-help-om
libreoffice-gtk3
</uninstall_package_names>

</app>
