<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Gaelic_LO_full
</name>

<description>
   <am>Gaelic Language Meta-Package for LibreOffice</am>
   <ar>Gaelic Language Meta-Package for LibreOffice</ar>
   <be>Gaelic Language Meta-Package for LibreOffice</be>
   <bg>Gaelic Language Meta-Package for LibreOffice</bg>
   <bn>Gaelic Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Gaèlic per LibreOffice</ca>
   <cs>Gaelic Language Meta-Package for LibreOffice</cs>
   <da>Gaelic Language Meta-Package for LibreOffice</da>
   <de>Gälisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Gaelic</el>
   <en>Gaelic Language Meta-Package for LibreOffice</en>
   <es_ES>Metapaquete de idioma Gaélico para LibreOffice</es_ES>
   <es>Metapaquete de idioma Gaélico para LibreOffice</es>
   <et>Gaelic Language Meta-Package for LibreOffice</et>
   <eu>Gaelic Language Meta-Package for LibreOffice</eu>
   <fa>Gaelic Language Meta-Package for LibreOffice</fa>
   <fil_PH>Gaelic Language Meta-Package for LibreOffice</fil_PH>
   <fi>Gaelinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue gaélique pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue gaélique pour LibreOffice</fr>
   <gl_ES>Gaélico Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Gaelic Language Meta-Package for LibreOffice</gu>
   <he_IL>Gaelic Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु गेलिक भाषा मेटा-पैकेज</hi>
   <hr>Gaelic Language Meta-Package for LibreOffice</hr>
   <hu>Gaelic Language Meta-Package for LibreOffice</hu>
   <id>Gaelic Language Meta-Package for LibreOffice</id>
   <is>Gaelic Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua gaelica per LibreOffice</it>
   <ja>LibreOffice用のゲール語メタパッケージ</ja>
   <kk>Gaelic Language Meta-Package for LibreOffice</kk>
   <ko>Gaelic Language Meta-Package for LibreOffice</ko>
   <ku>Gaelic Language Meta-Package for LibreOffice</ku>
   <lt>Gaelic Language Meta-Package for LibreOffice</lt>
   <mk>Gaelic Language Meta-Package for LibreOffice</mk>
   <mr>Gaelic Language Meta-Package for LibreOffice</mr>
   <nb_NO>Gaelic Language Meta-Package for LibreOffice</nb_NO>
   <nb>Gælisk språkpakke for LibreOffice</nb>
   <nl_BE>Gaelic Language Meta-Package for LibreOffice</nl_BE>
   <nl>Gaelic Taal Meta-Pakket voor LibreOffice</nl>
   <or>Gaelic Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka gaelickiego dla LibreOffice</pl>
   <pt_BR>Gaélica Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Gaélico Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Gaelic Language Meta-Package for LibreOffice</ro>
   <ru>Gaelic Language Meta-Package for LibreOffice</ru>
   <sk>Gaelic Language Meta-Package for LibreOffice</sk>
   <sl>Galski jezikovni meta-paket za LibreOffice</sl>
   <so>Gaelic Language Meta-Package for LibreOffice</so>
   <sq>Meta-paketë gjuhësore për LibreOffice-in në gaelike</sq>
   <sr>Gaelic Language Meta-Package for LibreOffice</sr>
   <sv>Gaeliskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Gaelic สำหรับ LibreOffice</th>
   <tr>LibreOffice için Keltçe Dili Üst-Paketi</tr>
   <uk>Gaelic Language Meta-Package for LibreOffice</uk>
   <vi>Gaelic Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 盖尔语语言包</zh_CN>
   <zh_HK>Gaelic Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Gaelic Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-ga
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-ga
libreoffice-gtk3
</uninstall_package_names>

</app>
