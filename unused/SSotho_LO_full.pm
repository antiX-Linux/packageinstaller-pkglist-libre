<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
SSotho_LO_full
</name>

<description>
   <am>Southern_sotho Language Meta-Package for LibreOffice</am>
   <ar>Southern_sotho Language Meta-Package for LibreOffice</ar>
   <be>Southern_sotho Language Meta-Package for LibreOffice</be>
   <bg>Southern_sotho Language Meta-Package for LibreOffice</bg>
   <bn>Southern_sotho Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet de Southern_sotho per LibreOffice</ca>
   <cs>Southern_sotho Language Meta-Package for LibreOffice</cs>
   <da>Southern_sotho Language Meta-Package for LibreOffice</da>
   <de>Süd-Sotho Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Νοτιοαφρικανικά</el>
   <en>Southern_sotho Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete del idioma Sotho_del_Sur para LibreOffice</es_ES>
   <es>Metapaquete de idiomas Sotho_sur para LibreOffice</es>
   <et>Southern_sotho Language Meta-Package for LibreOffice</et>
   <eu>Southern_sotho Language Meta-Package for LibreOffice</eu>
   <fa>Southern_sotho Language Meta-Package for LibreOffice</fa>
   <fil_PH>Southern_sotho Language Meta-Package for LibreOffice</fil_PH>
   <fi>Eteläsothonkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de la langue sotho du Sud pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de la langue sotho du Sud pour LibreOffice</fr>
   <gl_ES>Sotho sureste Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Southern_sotho Language Meta-Package for LibreOffice</gu>
   <he_IL>Southern_sotho Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु दक्षिणी_सोथो मेटा-पैकेज</hi>
   <hr>Southern_sotho Language Meta-Package for LibreOffice</hr>
   <hu>Southern_sotho Language Meta-Package for LibreOffice</hu>
   <id>Southern_sotho Language Meta-Package for LibreOffice</id>
   <is>Southern_sotho Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua sotho del sud per LibreOffice</it>
   <ja>LibreOffice用の南ソト語メタパッケージ</ja>
   <kk>Southern_sotho Language Meta-Package for LibreOffice</kk>
   <ko>Southern_sotho Language Meta-Package for LibreOffice</ko>
   <ku>Southern_sotho Language Meta-Package for LibreOffice</ku>
   <lt>Southern_sotho Language Meta-Package for LibreOffice</lt>
   <mk>Southern_sotho Language Meta-Package for LibreOffice</mk>
   <mr>Southern_sotho Language Meta-Package for LibreOffice</mr>
   <nb_NO>Southern_sotho Language Meta-Package for LibreOffice</nb_NO>
   <nb>Sørsotho språkpakke for LibreOffice</nb>
   <nl_BE>Southern_sotho Language Meta-Package for LibreOffice</nl_BE>
   <nl>Zuid-sotho Taal Meta-Pakket voor LibreOffice</nl>
   <or>Southern_sotho Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Southern_sotho dla LibreOffice</pl>
   <pt_BR>Sesoto Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Sotho meridional Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Southern_sotho Language Meta-Package for LibreOffice</ro>
   <ru>Southern_sotho Language Meta-Package for LibreOffice</ru>
   <sk>Southern_sotho Language Meta-Package for LibreOffice</sk>
   <sl>Južno-sotski jezikovni meta-paket za LibreOffice</sl>
   <so>Southern_sotho Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në soto jugore</sq>
   <sr>Southern_sotho Language Meta-Package for LibreOffice</sr>
   <sv>Southern_sotho Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Southern_sotho สำหรับ LibreOffice</th>
   <tr>LibreOffice için Güney sotho Dili Üst-Paketi</tr>
   <uk>Southern_sotho Language Meta-Package for LibreOffice</uk>
   <vi>Southern_sotho Language Meta-Package for LibreOffice</vi>
   <zh_CN>Southern_sotho Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Southern_sotho Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Southern_sotho Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-st
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-st
libreoffice-gtk3
</uninstall_package_names>

</app>
