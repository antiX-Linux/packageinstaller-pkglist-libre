<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Asturian_LO_full
</name>

<description>
   <am>Asturian Language Meta-Package for LibreOffice</am>
   <ar>Asturian Language Meta-Package for LibreOffice</ar>
   <be>Asturian Language Meta-Package for LibreOffice</be>
   <bg>Asturian Language Meta-Package for LibreOffice</bg>
   <bn>Asturian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Asturià per LibreOffice</ca>
   <cs>Asturian Language Meta-Package for LibreOffice</cs>
   <da>Asturian Language Meta-Package for LibreOffice</da>
   <de>Asturisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Asturian</el>
   <en>Asturian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de lengua asturiana para LibreOffice</es_ES>
   <es>Metapaquete de lengua Asturiana para LibreOffice</es>
   <et>Asturian Language Meta-Package for LibreOffice</et>
   <eu>Asturian Language Meta-Package for LibreOffice</eu>
   <fa>Asturian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Asturian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Asturialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet de langue asturienne pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue asturienne pour LibreOffice</fr>
   <gl_ES>Asturiano Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Asturian Language Meta-Package for LibreOffice</gu>
   <he_IL>Asturian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु अस्तुरियन भाषा मेटा-पैकेज</hi>
   <hr>Asturian Language Meta-Package for LibreOffice</hr>
   <hu>Asturian Language Meta-Package for LibreOffice</hu>
   <id>Asturian Language Meta-Package for LibreOffice</id>
   <is>Asturian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua asturiana per LibreOffice</it>
   <ja>LibreOffice用のアストゥリアス語メタパッケージ</ja>
   <kk>Asturian Language Meta-Package for LibreOffice</kk>
   <ko>Asturian Language Meta-Package for LibreOffice</ko>
   <ku>Asturian Language Meta-Package for LibreOffice</ku>
   <lt>Asturian Language Meta-Package for LibreOffice</lt>
   <mk>Asturian Language Meta-Package for LibreOffice</mk>
   <mr>Asturian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Asturian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Asturiansk språkpakke for LibreOffice</nb>
   <nl_BE>Asturian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Asturiaans Taal Meta-Pakket voor LibreOffice​</nl>
   <or>Asturian Language Meta-Package for LibreOffice</or>
   <pl>Asturyjski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Asturiano Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Asturiano Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Asturian Language Meta-Package for LibreOffice</ro>
   <ru>Asturian Language Meta-Package for LibreOffice</ru>
   <sk>Asturian Language Meta-Package for LibreOffice</sk>
   <sl>Asturijanski jezikovni meta-paket za LibreOffice</sl>
   <so>Asturian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në asturisht</sq>
   <sr>Asturian Language Meta-Package for LibreOffice</sr>
   <sv>Asturiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Asturian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Asturyan Dili Üst-Paketi</tr>
   <uk>Asturian Language Meta-Package for LibreOffice</uk>
   <vi>Asturian Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 阿斯图里亚斯语语言包</zh_CN>
   <zh_HK>Asturian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Asturian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-ast
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-ast
</uninstall_package_names>

</app>
