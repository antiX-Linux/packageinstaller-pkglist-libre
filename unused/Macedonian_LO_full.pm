<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Macedonian_LO_full
</name>

<description>
   <am>Macedonian Language Meta-Package for LibreOffice</am>
   <ar>Macedonian Language Meta-Package for LibreOffice</ar>
   <be>Macedonian Language Meta-Package for LibreOffice</be>
   <bg>Macedonian Language Meta-Package for LibreOffice</bg>
   <bn>Macedonian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Macedoni per LibreOffice</ca>
   <cs>Macedonian Language Meta-Package for LibreOffice</cs>
   <da>Macedonian Language Meta-Package for LibreOffice</da>
   <de>Mazedonisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Μακεδονικά</el>
   <en>Macedonian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Macedonio para LibreOffice</es_ES>
   <es>Metapaquete de idioma Macedonio para LibreOffice</es>
   <et>Macedonian Language Meta-Package for LibreOffice</et>
   <eu>Macedonian Language Meta-Package for LibreOffice</eu>
   <fa>Macedonian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Macedonian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Makedonialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue macédonienne pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue macédonienne pour LibreOffice</fr>
   <gl_ES>Macedonio Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Macedonian Language Meta-Package for LibreOffice</gu>
   <he_IL>Macedonian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु मकदूनियाई भाषा मेटा-पैकेज</hi>
   <hr>Macedonian Language Meta-Package for LibreOffice</hr>
   <hu>Macedonian Language Meta-Package for LibreOffice</hu>
   <id>Macedonian Language Meta-Package for LibreOffice</id>
   <is>Macedonian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua macedone per LibreOffice</it>
   <ja>LibreOffice用のマケドニア語メタパッケージ</ja>
   <kk>Macedonian Language Meta-Package for LibreOffice</kk>
   <ko>Macedonian Language Meta-Package for LibreOffice</ko>
   <ku>Macedonian Language Meta-Package for LibreOffice</ku>
   <lt>Macedonian Language Meta-Package for LibreOffice</lt>
   <mk>Macedonian Language Meta-Package for LibreOffice</mk>
   <mr>Macedonian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Macedonian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Makedonsk språkpakke for LibreOffice</nb>
   <nl_BE>Macedonian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Macedonisch Taal Meta-Pakket voor LibreOffice</nl>
   <or>Macedonian Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka macedońskiego dla LibreOffice</pl>
   <pt_BR>Macedônio Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Macedónio Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Macedonian Language Meta-Package for LibreOffice</ro>
   <ru>Macedonian Language Meta-Package for LibreOffice</ru>
   <sk>Macedonian Language Meta-Package for LibreOffice</sk>
   <sl>Makedonski jezikovni meta-paket za LibreOffice</sl>
   <so>Macedonian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në maqedonisht</sq>
   <sr>Macedonian Language Meta-Package for LibreOffice</sr>
   <sv>Macedoniskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Macedonian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Makedonca Dili Üst-Paketi</tr>
   <uk>Macedonian Language Meta-Package for LibreOffice</uk>
   <vi>Macedonian Language Meta-Package for LibreOffice</vi>
   <zh_CN>Macedonian Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Macedonian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Macedonian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-mk
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-mk
libreoffice-gtk3
</uninstall_package_names>

</app>
