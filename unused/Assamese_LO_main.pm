<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Assamese_LO_main
</name>

<description>
   <am>Assamese Language Meta-Package for LibreOffice</am>
   <ar>Assamese Language Meta-Package for LibreOffice</ar>
   <be>Assamese Language Meta-Package for LibreOffice</be>
   <bg>Assamese Language Meta-Package for LibreOffice</bg>
   <bn>Assamese Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Assamès per LibreOffice</ca>
   <cs>Assamese Language Meta-Package for LibreOffice</cs>
   <da>Assamese Language Meta-Package for LibreOffice</da>
   <de>Assamisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Assamese</el>
   <en>Assamese Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Lenguaje Asames para LibreOffice</es_ES>
   <es>Metapaquete de idioma Asamés para LibreOffice</es>
   <et>Assamese Language Meta-Package for LibreOffice</et>
   <eu>Assamese Language Meta-Package for LibreOffice</eu>
   <fa>Assamese Language Meta-Package for LibreOffice</fa>
   <fil_PH>Assamese Language Meta-Package for LibreOffice</fil_PH>
   <fi>Assamilainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue assamaise pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue assamaise pour LibreOffice</fr>
   <gl_ES>Asamés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Assamese Language Meta-Package for LibreOffice</gu>
   <he_IL>Assamese Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु असमी भाषा मेटा-पैकेज</hi>
   <hr>Assamese Language Meta-Package for LibreOffice</hr>
   <hu>Assamese Language Meta-Package for LibreOffice</hu>
   <id>Assamese Language Meta-Package for LibreOffice</id>
   <is>Assamese Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua assamese per LibreOffice</it>
   <ja>LibreOffice用のアッサム語メタパッケージ</ja>
   <kk>Assamese Language Meta-Package for LibreOffice</kk>
   <ko>Assamese Language Meta-Package for LibreOffice</ko>
   <ku>Assamese Language Meta-Package for LibreOffice</ku>
   <lt>Assamese Language Meta-Package for LibreOffice</lt>
   <mk>Assamese Language Meta-Package for LibreOffice</mk>
   <mr>Assamese Language Meta-Package for LibreOffice</mr>
   <nb_NO>Assamese Language Meta-Package for LibreOffice</nb_NO>
   <nb>Assamesisk språkpakke for LibreOffice</nb>
   <nl_BE>Assamese Language Meta-Package for LibreOffice</nl_BE>
   <nl>Assamese Taal Meta-Pakket voor LibreOffice​</nl>
   <or>Assamese Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Assamese dla LibreOffice</pl>
   <pt_BR>Assamês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Assamês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Assamese Language Meta-Package for LibreOffice</ro>
   <ru>Assamese Language Meta-Package for LibreOffice</ru>
   <sk>Assamese Language Meta-Package for LibreOffice</sk>
   <sl>Asameški jezikovni meta-paket za LibreOffice</sl>
   <so>Assamese Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në asamisht</sq>
   <sr>Assamese Language Meta-Package for LibreOffice</sr>
   <sv>Assamesiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Asamese สำหรับ LibreOffice</th>
   <tr>LibreOffice için Assamese Dili Üst-Paketi</tr>
   <uk>Assamese Language Meta-Package for LibreOffice</uk>
   <vi>Assamese Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 阿萨姆语语言包</zh_CN>
   <zh_HK>Assamese Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Assamese Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-as
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-as
libreoffice-gtk3
</uninstall_package_names>

</app>
