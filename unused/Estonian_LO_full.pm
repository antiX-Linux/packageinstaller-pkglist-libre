<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Estonian_LO_full
</name>

<description>
   <am>Estonian Language Meta-Package for LibreOffice</am>
   <ar>Estonian Language Meta-Package for LibreOffice</ar>
   <be>Estonian Language Meta-Package for LibreOffice</be>
   <bg>Estonian Language Meta-Package for LibreOffice</bg>
   <bn>Estonian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Estonià per LibreOffice</ca>
   <cs>Estonian Language Meta-Package for LibreOffice</cs>
   <da>Estonian Language Meta-Package for LibreOffice</da>
   <de>Estnisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Εσθονικά</el>
   <en>Estonian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Estonio para LibreOffice</es_ES>
   <es>Metapaquete de idioma Estonio para LibreOffice</es>
   <et>Estonian Language Meta-Package for LibreOffice</et>
   <eu>Estonian Language Meta-Package for LibreOffice</eu>
   <fa>Estonian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Estonian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Eestiläinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet de langue estonienne pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue estonienne pour LibreOffice</fr>
   <gl_ES>Estonio Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Estonian Language Meta-Package for LibreOffice</gu>
   <he_IL>Estonian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु एस्टोनियाई भाषा मेटा-पैकेज</hi>
   <hr>Estonian Language Meta-Package for LibreOffice</hr>
   <hu>Estonian Language Meta-Package for LibreOffice</hu>
   <id>Estonian Language Meta-Package for LibreOffice</id>
   <is>Estonian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua estone per LibreOffice</it>
   <ja>LibreOffice用エストニア語メタパッケージ</ja>
   <kk>Estonian Language Meta-Package for LibreOffice</kk>
   <ko>Estonian Language Meta-Package for LibreOffice</ko>
   <ku>Estonian Language Meta-Package for LibreOffice</ku>
   <lt>Estonian Language Meta-Package for LibreOffice</lt>
   <mk>Estonian Language Meta-Package for LibreOffice</mk>
   <mr>Estonian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Estonian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Estisk språkpakke for LibreOffice</nb>
   <nl_BE>Estonian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Estoons Taal Meta-Pakket voor LibreOffice</nl>
   <or>Estonian Language Meta-Package for LibreOffice</or>
   <pl>Estoński meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Estónio Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Estónio Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Estonian Language Meta-Package for LibreOffice</ro>
   <ru>Estonian Language Meta-Package for LibreOffice</ru>
   <sk>Estonian Language Meta-Package for LibreOffice</sk>
   <sl>Estonski jezikovni meta-paket za LibreOffice</sl>
   <so>Estonian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në estonisht</sq>
   <sr>Estonian Language Meta-Package for LibreOffice</sr>
   <sv>Estniskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Estonian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Estonya Dili Üst-Paketi</tr>
   <uk>Estonian Language Meta-Package for LibreOffice</uk>
   <vi>Estonian Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 爱沙尼亚语语言包</zh_CN>
   <zh_HK>Estonian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Estonian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-et
libreoffice-help-et
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-et
libreoffice-help-et
libreoffice-gtk3
</uninstall_package_names>

</app>
