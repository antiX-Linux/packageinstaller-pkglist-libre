<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Gujarati_LO_full
</name>

<description>
   <am>Gujarati Language Meta-Package for LibreOffice</am>
   <ar>Gujarati Language Meta-Package for LibreOffice</ar>
   <be>Gujarati Language Meta-Package for LibreOffice</be>
   <bg>Gujarati Language Meta-Package for LibreOffice</bg>
   <bn>Gujarati Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Gujarati per LibreOffice</ca>
   <cs>Gujarati Language Meta-Package for LibreOffice</cs>
   <da>Gujarati Language Meta-Package for LibreOffice</da>
   <de>Gujarati Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Gujarati</el>
   <en>Gujarati Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Guyaratí para LibreOffice</es_ES>
   <es>Metapaquete de idioma Guyaratí para LibreOffice</es>
   <et>Gujarati Language Meta-Package for LibreOffice</et>
   <eu>Gujarati Language Meta-Package for LibreOffice</eu>
   <fa>Gujarati Language Meta-Package for LibreOffice</fa>
   <fil_PH>Gujarati Language Meta-Package for LibreOffice</fil_PH>
   <fi>Gujaratinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue gujarati pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue gujarati pour LibreOffice</fr>
   <gl_ES>Gujarati Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Gujarati Language Meta-Package for LibreOffice</gu>
   <he_IL>Gujarati Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु गुजराती भाषा मेटा-पैकेज</hi>
   <hr>Gujarati Language Meta-Package for LibreOffice</hr>
   <hu>Gujarati Language Meta-Package for LibreOffice</hu>
   <id>Gujarati Language Meta-Package for LibreOffice</id>
   <is>Gujarati Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua gujarati per LibreOffice</it>
   <ja>LibreOffice用のグジャラート語メタパッケージ</ja>
   <kk>Gujarati Language Meta-Package for LibreOffice</kk>
   <ko>Gujarati Language Meta-Package for LibreOffice</ko>
   <ku>Gujarati Language Meta-Package for LibreOffice</ku>
   <lt>Gujarati Language Meta-Package for LibreOffice</lt>
   <mk>Gujarati Language Meta-Package for LibreOffice</mk>
   <mr>Gujarati Language Meta-Package for LibreOffice</mr>
   <nb_NO>Gujarati Language Meta-Package for LibreOffice</nb_NO>
   <nb>Gujarati språkpakke for LibreOffice</nb>
   <nl_BE>Gujarati Language Meta-Package for LibreOffice</nl_BE>
   <nl>Gujarati Taal Meta-Pakket voor LibreOffice</nl>
   <or>Gujarati Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka gudżarati dla LibreOffice</pl>
   <pt_BR>Guzerate Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Gujarati Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Gujarati Language Meta-Package for LibreOffice</ro>
   <ru>Gujarati Language Meta-Package for LibreOffice</ru>
   <sk>Gujarati Language Meta-Package for LibreOffice</sk>
   <sl>Gujaratski jezikovni meta-paket za LibreOffice</sl>
   <so>Gujarati Language Meta-Package for LibreOffice</so>
   <sq>Përkthimi i Firefox-it ESR në gujaratase (Indi)</sq>
   <sr>Gujarati Language Meta-Package for LibreOffice</sr>
   <sv>Gujarati Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Gujarati สำหรับ LibreOffice</th>
   <tr>LibreOffice için Gujaratça Dili Üst-Paketi</tr>
   <uk>Gujarati Language Meta-Package for LibreOffice</uk>
   <vi>Gujarati Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 古吉拉特语语言包</zh_CN>
   <zh_HK>Gujarati Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Gujarati Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-gu
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-gu
libreoffice-gtk3
</uninstall_package_names>

</app>
