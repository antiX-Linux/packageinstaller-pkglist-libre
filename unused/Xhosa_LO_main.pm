<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Xhosa_LO_main
</name>

<description>
   <am>Xhosa Language Meta-Package for LibreOffice</am>
   <ar>Xhosa Language Meta-Package for LibreOffice</ar>
   <be>Xhosa Language Meta-Package for LibreOffice</be>
   <bg>Xhosa Language Meta-Package for LibreOffice</bg>
   <bn>Xhosa Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Xhosa per LibreOffice</ca>
   <cs>Xhosa Language Meta-Package for LibreOffice</cs>
   <da>Xhosa Language Meta-Package for LibreOffice</da>
   <de>Xhosa Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Xhosa</el>
   <en>Xhosa Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete idioma Xhosa para LibreOffice</es_ES>
   <es>Metapaquete idioma Xhosa para LibreOffice</es>
   <et>Xhosa Language Meta-Package for LibreOffice</et>
   <eu>Xhosa Language Meta-Package for LibreOffice</eu>
   <fa>Xhosa Language Meta-Package for LibreOffice</fa>
   <fil_PH>Xhosa Language Meta-Package for LibreOffice</fil_PH>
   <fi>Xhosankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Meta-Paquet de langue Xhosa pour LibreOffice</fr_BE>
   <fr>Meta-Paquet de langue Xhosa pour LibreOffice</fr>
   <gl_ES>Xosa Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Xhosa Language Meta-Package for LibreOffice</gu>
   <he_IL>Xhosa Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु कोसा भाषा मेटा-पैकेज</hi>
   <hr>Xhosa Language Meta-Package for LibreOffice</hr>
   <hu>Xhosa Language Meta-Package for LibreOffice</hu>
   <id>Xhosa Language Meta-Package for LibreOffice</id>
   <is>Xhosa Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua xhosa per LibreOffice</it>
   <ja>LibreOffice 用コサ語メタパッケージ</ja>
   <kk>Xhosa Language Meta-Package for LibreOffice</kk>
   <ko>Xhosa Language Meta-Package for LibreOffice</ko>
   <ku>Xhosa Language Meta-Package for LibreOffice</ku>
   <lt>Xhosa Language Meta-Package for LibreOffice</lt>
   <mk>Xhosa Language Meta-Package for LibreOffice</mk>
   <mr>Xhosa Language Meta-Package for LibreOffice</mr>
   <nb_NO>Xhosa Language Meta-Package for LibreOffice</nb_NO>
   <nb>Xhosa språkpakke for LibreOffice</nb>
   <nl_BE>Xhosa Language Meta-Package for LibreOffice</nl_BE>
   <nl>Xhosa Taal Meta-Pakket voor LibreOffice</nl>
   <or>Xhosa Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Xhosa dla LibreOffice</pl>
   <pt_BR>Xhosa Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Xosa Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Xhosa Language Meta-Package for LibreOffice</ro>
   <ru>Xhosa Language Meta-Package for LibreOffice</ru>
   <sk>Xhosa Language Meta-Package for LibreOffice</sk>
   <sl>Khoški jezikovni meta-paket za LibreOffice</sl>
   <so>Xhosa Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në xosa</sq>
   <sr>Xhosa Language Meta-Package for LibreOffice</sr>
   <sv>Xhosa Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Xhosa สำหรับ LibreOffice</th>
   <tr>LibreOffice için Xhosa Dili Üst-Paketi</tr>
   <uk>Xhosa Language Meta-Package for LibreOffice</uk>
   <vi>Xhosa Language Meta-Package for LibreOffice</vi>
   <zh_CN>Xhosa Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Xhosa Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Xhosa Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-xh
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-xh
libreoffice-gtk3
</uninstall_package_names>

</app>
