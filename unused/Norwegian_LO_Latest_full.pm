<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Norwegian_LO_Latest_full
</name>

<description>
   <am>Norwegian Language Meta-Package for LibreOffice</am>
   <ar>Norwegian Language Meta-Package for LibreOffice</ar>
   <be>Norwegian Language Meta-Package for LibreOffice</be>
   <bg>Norwegian Language Meta-Package for LibreOffice</bg>
   <bn>Norwegian Language Meta-Package for LibreOffice</bn>
   <ca>Meta-paquet de llengua per a LibreOffice</ca>
   <cs>Norwegian Language Meta-Package for LibreOffice</cs>
   <da>Norsk sprog-metapakke til LibreOffice</da>
   <de>Norwegisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Νορβηγικά</el>
   <en>Norwegian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Idioma Noruego para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Noruego para LibreOffice</es>
   <et>Norwegian Language Meta-Package for LibreOffice</et>
   <eu>Norwegian Language Meta-Package for LibreOffice</eu>
   <fa>Norwegian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Norwegian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Norjalainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue norvégienne pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue norvégienne pour LibreOffice</fr>
   <gl_ES>Noruego Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Norwegian Language Meta-Package for LibreOffice</gu>
   <he_IL>Norwegian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु नॉर्वेजियाई भाषा मेटा-पैकेज</hi>
   <hr>Norwegian Language Meta-Package for LibreOffice</hr>
   <hu>Norwegian Language Meta-Package for LibreOffice</hu>
   <id>Norwegian Language Meta-Package for LibreOffice</id>
   <is>Norwegian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua norvegese per LibreOffice</it>
   <ja>LibreOffice用のノルウェー語メタパッケージ</ja>
   <kk>Norwegian Language Meta-Package for LibreOffice</kk>
   <ko>Norwegian Language Meta-Package for LibreOffice</ko>
   <ku>Norwegian Language Meta-Package for LibreOffice</ku>
   <lt>Norwegian Language Meta-Package for LibreOffice</lt>
   <mk>Norwegian Language Meta-Package for LibreOffice</mk>
   <mr>Norwegian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Norwegian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Norsk bokmålsk språkpakke for LibreOffice</nb>
   <nl_BE>Norwegian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Noorse Taal Meta-Pakket voor LibreOffice</nl>
   <or>Norwegian Language Meta-Package for LibreOffice</or>
   <pl>Norweski metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Norueguês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Norueguês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Norwegian Language Meta-Package for LibreOffice</ro>
   <ru>Norwegian Language Meta-Package for LibreOffice</ru>
   <sk>Norwegian Language Meta-Package for LibreOffice</sk>
   <sl>Norveški jezikovni meta-paket za LibreOffice</sl>
   <so>Norwegian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në norvegjisht</sq>
   <sr>Norwegian Language Meta-Package for LibreOffice</sr>
   <sv>Norskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Norwegian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Norveççe Dili Üst-Paketi</tr>
   <uk>Norwegian Language Meta-Package for LibreOffice</uk>
   <vi>Norwegian Language Meta-Package for LibreOffice</vi>
   <zh_CN>Norwegian Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Norwegian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Norwegian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "deb http://deb.debian.org/debian bullseye-backports main contrib non-free">/etc/apt/sources.list.d/pitemp.list
apt-get update
</preinstall>

<install_package_names>
-t bullseye-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-nb
libreoffice-java-common
</install_package_names>

<postinstall>
rm /etc/apt/sources.list.d/pitemp.list
apt-get update
</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-nb
</uninstall_package_names>

</app>
