<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Danish_LO_main
</name>

<description>
   <am>Danish Language Meta-Package for LibreOffice</am>
   <ar>Danish Language Meta-Package for LibreOffice</ar>
   <be>Danish Language Meta-Package for LibreOffice</be>
   <bg>Danish Language Meta-Package for LibreOffice</bg>
   <bn>Danish Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Danès per LibreOffice</ca>
   <cs>Danish Language Meta-Package for LibreOffice</cs>
   <da>Danish Language Meta-Package for LibreOffice</da>
   <de>Dänisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Δανέζικα</el>
   <en>Danish Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma danés para LibreOffice</es_ES>
   <es>Metapaquete de idioma Danés para LibreOffice</es>
   <et>Danish Language Meta-Package for LibreOffice</et>
   <eu>Danish Language Meta-Package for LibreOffice</eu>
   <fa>Danish Language Meta-Package for LibreOffice</fa>
   <fil_PH>Danish Language Meta-Package for LibreOffice</fil_PH>
   <fi>Tanskankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de langue danoise pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de langue danoise pour LibreOffice</fr>
   <gl_ES>Danés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Danish Language Meta-Package for LibreOffice</gu>
   <he_IL>Danish Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु डेनिश भाषा मेटा-पैकेज</hi>
   <hr>Danish Language Meta-Package for LibreOffice</hr>
   <hu>Danish Language Meta-Package for LibreOffice</hu>
   <id>Danish Language Meta-Package for LibreOffice</id>
   <is>Danish Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua danese per LibreOffice</it>
   <ja>LibreOffice用のデンマーク語メタパッケージ</ja>
   <kk>Danish Language Meta-Package for LibreOffice</kk>
   <ko>Danish Language Meta-Package for LibreOffice</ko>
   <ku>Danish Language Meta-Package for LibreOffice</ku>
   <lt>Danish Language Meta-Package for LibreOffice</lt>
   <mk>Danish Language Meta-Package for LibreOffice</mk>
   <mr>Danish Language Meta-Package for LibreOffice</mr>
   <nb_NO>Danish Language Meta-Package for LibreOffice</nb_NO>
   <nb>Dansk språkpakke for LibreOffice</nb>
   <nl_BE>Danish Language Meta-Package for LibreOffice</nl_BE>
   <nl>Deens Taal Meta-Pakket voor LibreOffice</nl>
   <or>Danish Language Meta-Package for LibreOffice</or>
   <pl>Duński meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Dinamarquês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Dinamarquês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Danish Language Meta-Package for LibreOffice</ro>
   <ru>Danish Language Meta-Package for LibreOffice</ru>
   <sk>Danish Language Meta-Package for LibreOffice</sk>
   <sl>Danski jezikovni meta-paket za LibreOffice</sl>
   <so>Danish Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në danisht</sq>
   <sr>Danish Language Meta-Package for LibreOffice</sr>
   <sv>Danskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Danish สำหรับ LibreOffice</th>
   <tr>LibreOffice için Danca Dili Üst-Paketi</tr>
   <uk>Danish Language Meta-Package for LibreOffice</uk>
   <vi>Danish Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 丹麦语语言包</zh_CN>
   <zh_HK>Danish Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Danish Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-da
libreoffice-help-da
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-da
libreoffice-help-da
libreoffice-gtk3
</uninstall_package_names>

</app>
