<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Galician_LO_main
</name>

<description>
   <am>Galician Language Meta-Package for LibreOffice</am>
   <ar>Galician Language Meta-Package for LibreOffice</ar>
   <be>Galician Language Meta-Package for LibreOffice</be>
   <bg>Galician Language Meta-Package for LibreOffice</bg>
   <bn>Galician Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Gallec per LibreOffice</ca>
   <cs>Galician Language Meta-Package for LibreOffice</cs>
   <da>Galician Language Meta-Package for LibreOffice</da>
   <de>Galizisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Galician</el>
   <en>Galician Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Gallego para LibreOffice</es_ES>
   <es>Metapaquete de idioma Gallego para LibreOffice</es>
   <et>Galician Language Meta-Package for LibreOffice</et>
   <eu>Galician Language Meta-Package for LibreOffice</eu>
   <fa>Galician Language Meta-Package for LibreOffice</fa>
   <fil_PH>Galician Language Meta-Package for LibreOffice</fil_PH>
   <fi>Galiciankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet de langue galicienne pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue galicienne pour LibreOffice</fr>
   <gl_ES>Galego Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Galician Language Meta-Package for LibreOffice</gu>
   <he_IL>Galician Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु गैलिशनियाई भाषा मेटा-पैकेज</hi>
   <hr>Galician Language Meta-Package for LibreOffice</hr>
   <hu>Galician Language Meta-Package for LibreOffice</hu>
   <id>Galician Language Meta-Package for LibreOffice</id>
   <is>Galician Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua galiziana per LibreOffice</it>
   <ja>LibreOffice用のガリシア語メタパッケージ</ja>
   <kk>Galician Language Meta-Package for LibreOffice</kk>
   <ko>Galician Language Meta-Package for LibreOffice</ko>
   <ku>Galician Language Meta-Package for LibreOffice</ku>
   <lt>Galician Language Meta-Package for LibreOffice</lt>
   <mk>Galician Language Meta-Package for LibreOffice</mk>
   <mr>Galician Language Meta-Package for LibreOffice</mr>
   <nb_NO>Galician Language Meta-Package for LibreOffice</nb_NO>
   <nb>Galisisk språkpakke for LibreOffice</nb>
   <nl_BE>Galician Language Meta-Package for LibreOffice</nl_BE>
   <nl>Galiciaans Taal Meta-Pakket voor LibreOffice</nl>
   <or>Galician Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka galicyjskiego dla LibreOffice</pl>
   <pt_BR>Galego Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Galego Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Galician Language Meta-Package for LibreOffice</ro>
   <ru>Galician Language Meta-Package for LibreOffice</ru>
   <sk>Galician Language Meta-Package for LibreOffice</sk>
   <sl>Galicijski jezikovni meta-paket za LibreOffice</sl>
   <so>Galician Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në galicisht</sq>
   <sr>Galician Language Meta-Package for LibreOffice</sr>
   <sv>Galiciskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Galician สำหรับ LibreOffice</th>
   <tr>LibreOffice için Galiçya Dili Üst-Paketi</tr>
   <uk>Galician Language Meta-Package for LibreOffice</uk>
   <vi>Galician Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 加利西亚语语言包</zh_CN>
   <zh_HK>Galician Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Galician Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-gl
libreoffice-help-gl
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-gl
libreoffice-help-gl
libreoffice-gtk3
</uninstall_package_names>

</app>
