<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Malayalam_LO_main
</name>

<description>
   <am>Malayalam Language Meta-Package for LibreOffice</am>
   <ar>Malayalam Language Meta-Package for LibreOffice</ar>
   <be>Malayalam Language Meta-Package for LibreOffice</be>
   <bg>Malayalam Language Meta-Package for LibreOffice</bg>
   <bn>Malayalam Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Malaià per LibreOffice</ca>
   <cs>Malayalam Language Meta-Package for LibreOffice</cs>
   <da>Malayalam Language Meta-Package for LibreOffice</da>
   <de>Malayalam Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Malayalam</el>
   <en>Malayalam Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma malayo para LibreOffice</es_ES>
   <es>Metapaquete de idioma Malayo para LibreOffice</es>
   <et>Malayalam Language Meta-Package for LibreOffice</et>
   <eu>Malayalam Language Meta-Package for LibreOffice</eu>
   <fa>Malayalam Language Meta-Package for LibreOffice</fa>
   <fil_PH>Malayalam Language Meta-Package for LibreOffice</fil_PH>
   <fi>Malajalamkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue malayalam pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue malayalam pour LibreOffice</fr>
   <gl_ES>Malaio Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Malayalam Language Meta-Package for LibreOffice</gu>
   <he_IL>Malayalam Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु मलयालम भाषा मेटा-पैकेज</hi>
   <hr>Malayalam Language Meta-Package for LibreOffice</hr>
   <hu>Malayalam Language Meta-Package for LibreOffice</hu>
   <id>Malayalam Language Meta-Package for LibreOffice</id>
   <is>Malayalam Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua malayalam per LibreOffice</it>
   <ja>LibreOffice用のマラヤーラム語メタパッケージ</ja>
   <kk>Malayalam Language Meta-Package for LibreOffice</kk>
   <ko>Malayalam Language Meta-Package for LibreOffice</ko>
   <ku>Malayalam Language Meta-Package for LibreOffice</ku>
   <lt>Malayalam Language Meta-Package for LibreOffice</lt>
   <mk>Malayalam Language Meta-Package for LibreOffice</mk>
   <mr>Malayalam Language Meta-Package for LibreOffice</mr>
   <nb_NO>Malayalam Language Meta-Package for LibreOffice</nb_NO>
   <nb>Malayalam språkpakke for LibreOffice</nb>
   <nl_BE>Malayalam Language Meta-Package for LibreOffice</nl_BE>
   <nl>Malayalam Taal Meta-Pakket voor LibreOffice</nl>
   <or>Malayalam Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka malajalam dla LibreOffice</pl>
   <pt_BR>Malaiala Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Malaio Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Malayalam Language Meta-Package for LibreOffice</ro>
   <ru>Malayalam Language Meta-Package for LibreOffice</ru>
   <sk>Malayalam Language Meta-Package for LibreOffice</sk>
   <sl>Malajlamski jezikovni meta-paket za LibreOffice</sl>
   <so>Malayalam Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në malajalamisht</sq>
   <sr>Malayalam Language Meta-Package for LibreOffice</sr>
   <sv>Malayalam Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Malayalam สำหรับ LibreOffice</th>
   <tr>LibreOffice için Malayalam Dili Üst-Paketi</tr>
   <uk>Malayalam Language Meta-Package for LibreOffice</uk>
   <vi>Malayalam Language Meta-Package for LibreOffice</vi>
   <zh_CN>Malayalam Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Malayalam Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Malayalam Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ml
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ml
libreoffice-gtk3
</uninstall_package_names>

</app>
