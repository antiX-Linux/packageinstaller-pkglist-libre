<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hindi_LO_full
</name>

<description>
   <am>Hindi Language Meta-Package for LibreOffice</am>
   <ar>Hindi Language Meta-Package for LibreOffice</ar>
   <be>Hindi Language Meta-Package for LibreOffice</be>
   <bg>Hindi Language Meta-Package for LibreOffice</bg>
   <bn>Hindi Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Hindi per LibreOffice</ca>
   <cs>Hindi Language Meta-Package for LibreOffice</cs>
   <da>Hindi Language Meta-Package for LibreOffice</da>
   <de>Hindi Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Χίντι</el>
   <en>Hindi Language Meta-Package for LibreOffice</en>
   <es_ES>Metapaquete de idioma Hindi para LibreOffice</es_ES>
   <es>Metapaquete de idioma Hindi para LibreOffice</es>
   <et>Hindi Language Meta-Package for LibreOffice</et>
   <eu>Hindi Language Meta-Package for LibreOffice</eu>
   <fa>Hindi Language Meta-Package for LibreOffice</fa>
   <fil_PH>Hindi Language Meta-Package for LibreOffice</fil_PH>
   <fi>Hindinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue hindi pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue hindi pour LibreOffice</fr>
   <gl_ES>Hindi Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Hindi Language Meta-Package for LibreOffice</gu>
   <he_IL>Hindi Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु हिंदी भाषा मेटा-पैकेज</hi>
   <hr>Hindi Language Meta-Package for LibreOffice</hr>
   <hu>Hindi Language Meta-Package for LibreOffice</hu>
   <id>Hindi Language Meta-Package for LibreOffice</id>
   <is>Hindi Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua hindi per LibreOffice</it>
   <ja>LibreOffice 用のヒンディー語メタパッケージ</ja>
   <kk>Hindi Language Meta-Package for LibreOffice</kk>
   <ko>Hindi Language Meta-Package for LibreOffice</ko>
   <ku>Hindi Language Meta-Package for LibreOffice</ku>
   <lt>Hindi Language Meta-Package for LibreOffice</lt>
   <mk>Hindi Language Meta-Package for LibreOffice</mk>
   <mr>Hindi Language Meta-Package for LibreOffice</mr>
   <nb_NO>Hindi Language Meta-Package for LibreOffice</nb_NO>
   <nb>Hindi språkpakke for LibreOffice</nb>
   <nl_BE>Hindi Language Meta-Package for LibreOffice</nl_BE>
   <nl>Hindi Taal Meta-Pakket voor LibreOffice</nl>
   <or>Hindi Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Hindi dla LibreOffice</pl>
   <pt_BR>Hindi Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Hindi Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Hindi Language Meta-Package for LibreOffice</ro>
   <ru>Hindi Language Meta-Package for LibreOffice</ru>
   <sk>Hindi Language Meta-Package for LibreOffice</sk>
   <sl>Hindujski jezikovni meta-paket za LibreOffice</sl>
   <so>Hindi Language Meta-Package for LibreOffice</so>
   <sq>Përkthimi i Firefox-it ESR në Hinduisht (Indi)</sq>
   <sr>Hindi Language Meta-Package for LibreOffice</sr>
   <sv>Hindi Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Hindi สำหรับ LibreOffice</th>
   <tr>LibreOffice için Hintçe Dili Üst-Paketi</tr>
   <uk>Hindi Language Meta-Package for LibreOffice</uk>
   <vi>Hindi Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 印地语语言包</zh_CN>
   <zh_HK>Hindi Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Hindi Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-hi
libreoffice-help-hi
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-hi
libreoffice-help-hi
libreoffice-gtk3
</uninstall_package_names>

</app>
