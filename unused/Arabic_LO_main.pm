<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Arabic_LO_main
</name>

<description>
   <am>Arabic Language Meta-Package for LibreOffice</am>
   <ar>Arabic Language Meta-Package for LibreOffice</ar>
   <be>Arabic Language Meta-Package for LibreOffice</be>
   <bg>Arabic Language Meta-Package for LibreOffice</bg>
   <bn>Arabic Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Àrab per LibreOffice</ca>
   <cs>Arabic Language Meta-Package for LibreOffice</cs>
   <da>Arabic Language Meta-Package for LibreOffice</da>
   <de>Arabisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Αραβικά</el>
   <en>Arabic Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Arabe para LibreOffice</es_ES>
   <es>Metapaquete de idioma Árabe para LibreOffice</es>
   <et>Arabic Language Meta-Package for LibreOffice</et>
   <eu>Arabic Language Meta-Package for LibreOffice</eu>
   <fa>Arabic Language Meta-Package for LibreOffice</fa>
   <fil_PH>Arabic Language Meta-Package for LibreOffice</fil_PH>
   <fi>Arabiankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet de langue arabe pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue arabe pour LibreOffice</fr>
   <gl_ES>Árabe mta-paquete de idioma para LibreOffice</gl_ES>
   <gu>Arabic Language Meta-Package for LibreOffice</gu>
   <he_IL>חבילת על לערבית עבור LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु अरबी भाषा मेटा-पैकेज</hi>
   <hr>Arabic Language Meta-Package for LibreOffice</hr>
   <hu>Arabic Language Meta-Package for LibreOffice</hu>
   <id>Arabic Language Meta-Package for LibreOffice</id>
   <is>Arabic Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua araba per LibreOffice</it>
   <ja>LibreOffice用のアラビア語メタパッケージ</ja>
   <kk>Arabic Language Meta-Package for LibreOffice</kk>
   <ko>Arabic Language Meta-Package for LibreOffice</ko>
   <ku>Arabic Language Meta-Package for LibreOffice</ku>
   <lt>Arabic Language Meta-Package for LibreOffice</lt>
   <mk>Arabic Language Meta-Package for LibreOffice</mk>
   <mr>Arabic Language Meta-Package for LibreOffice</mr>
   <nb_NO>Arabic Language Meta-Package for LibreOffice</nb_NO>
   <nb>Arabisk språkpakke for LibreOffice</nb>
   <nl_BE>Arabic Language Meta-Package for LibreOffice</nl_BE>
   <nl>Arabische Taal Meta-Pakket voor LibreOffice​</nl>
   <or>Arabic Language Meta-Package for LibreOffice</or>
   <pl>Arabski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Árabe Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Árabe Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Arabic Language Meta-Package for LibreOffice</ro>
   <ru>Arabic Language Meta-Package for LibreOffice</ru>
   <sk>Arabic Language Meta-Package for LibreOffice</sk>
   <sl>Arabski jezikovni meta-paket za LibreOffice</sl>
   <so>Arabic Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në arabisht</sq>
   <sr>Arabic Language Meta-Package for LibreOffice</sr>
   <sv>Arabiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Arabic สำหรับ LibreOffice</th>
   <tr>LibreOffice için Arapça Dili Üst-Paketi</tr>
   <uk>Arabic Language Meta-Package for LibreOffice</uk>
   <vi>Arabic Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 阿拉伯语语言包</zh_CN>
   <zh_HK>Arabic Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Arabic Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ar
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ar
libreoffice-gtk3
</uninstall_package_names>

</app>
