<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bosnian_LO_main
</name>

<description>
   <am>Bosnian Language Meta-Package for LibreOffice</am>
   <ar>Bosnian Language Meta-Package for LibreOffice</ar>
   <be>Bosnian Language Meta-Package for LibreOffice</be>
   <bg>Bosnian Language Meta-Package for LibreOffice</bg>
   <bn>Bosnian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Bosnià per LibreOffice</ca>
   <cs>Bosnian Language Meta-Package for LibreOffice</cs>
   <da>Bosnian Language Meta-Package for LibreOffice</da>
   <de>Bosnisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Βόσνια</el>
   <en>Bosnian Language Meta-Package for LibreOffice</en>
   <es_ES>Metapaquete de idioma Bosnio para LibreOffice</es_ES>
   <es>Metapaquete de idioma Bosnio para LibreOffice</es>
   <et>Bosnian Language Meta-Package for LibreOffice</et>
   <eu>Bosnian Language Meta-Package for LibreOffice</eu>
   <fa>Bosnian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Bosnian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Bosnialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue bosniaque pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue bosniaque pour LibreOffice</fr>
   <gl_ES>Bosnio Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Bosnian Language Meta-Package for LibreOffice</gu>
   <he_IL>Bosnian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु बोस्नियाई भाषा मेटा-पैकेज</hi>
   <hr>Bosnian Language Meta-Package for LibreOffice</hr>
   <hu>Bosnian Language Meta-Package for LibreOffice</hu>
   <id>Bosnian Language Meta-Package for LibreOffice</id>
   <is>Bosnian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua bosniaca per LibreOffice</it>
   <ja>LibreOffice 用ボスニア語メタパッケージ</ja>
   <kk>Bosnian Language Meta-Package for LibreOffice</kk>
   <ko>Bosnian Language Meta-Package for LibreOffice</ko>
   <ku>Bosnian Language Meta-Package for LibreOffice</ku>
   <lt>Bosnian Language Meta-Package for LibreOffice</lt>
   <mk>Bosnian Language Meta-Package for LibreOffice</mk>
   <mr>Bosnian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Bosnian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Bosnisk språkpakke for LibreOffice</nb>
   <nl_BE>Bosnian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Bosnisch Taal Meta-Pakket voor LibreOffice</nl>
   <or>Bosnian Language Meta-Package for LibreOffice</or>
   <pl>Bośniacki meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Bósnio Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Bósnio Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Bosnian Language Meta-Package for LibreOffice</ro>
   <ru>Bosnian Language Meta-Package for LibreOffice</ru>
   <sk>Bosnian Language Meta-Package for LibreOffice</sk>
   <sl>Bosanski jezikovni meta-paket za LibreOffice</sl>
   <so>Bosnian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në bosnjisht</sq>
   <sr>Bosnian Language Meta-Package for LibreOffice</sr>
   <sv>Bosniskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Bosnian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Boşnak Dili Üst-Paketi</tr>
   <uk>Bosnian Language Meta-Package for LibreOffice</uk>
   <vi>Bosnian Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 波斯尼亚语语言包</zh_CN>
   <zh_HK>Bosnian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Bosnian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-bs
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-bs
libreoffice-gtk3
</uninstall_package_names>

</app>
