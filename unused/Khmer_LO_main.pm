<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Khmer_LO_main
</name>

<description>
   <am>Khmer Language Meta-Package for LibreOffice</am>
   <ar>Khmer Language Meta-Package for LibreOffice</ar>
   <be>Khmer Language Meta-Package for LibreOffice</be>
   <bg>Khmer Language Meta-Package for LibreOffice</bg>
   <bn>Khmer Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Khmer per LibreOffice</ca>
   <cs>Khmer Language Meta-Package for LibreOffice</cs>
   <da>Khmer Language Meta-Package for LibreOffice</da>
   <de>Khmer Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Χμερ</el>
   <en>Khmer Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma khmer para LibreOffice</es_ES>
   <es>Metapaquete de idioma Khmer para LibreOffice</es>
   <et>Khmer Language Meta-Package for LibreOffice</et>
   <eu>Khmer Language Meta-Package for LibreOffice</eu>
   <fa>Khmer Language Meta-Package for LibreOffice</fa>
   <fil_PH>Khmer Language Meta-Package for LibreOffice</fil_PH>
   <fi>Khmerinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue khmère pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue khmère pour LibreOffice</fr>
   <gl_ES>Khmer Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Khmer Language Meta-Package for LibreOffice</gu>
   <he_IL>Khmer Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु खमेर भाषा मेटा-पैकेज</hi>
   <hr>Khmer Language Meta-Package for LibreOffice</hr>
   <hu>Khmer Language Meta-Package for LibreOffice</hu>
   <id>Khmer Language Meta-Package for LibreOffice</id>
   <is>Khmer Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua khmer per LibreOffice</it>
   <ja>LibreOffice用のクメール語メタパッケージ</ja>
   <kk>Khmer Language Meta-Package for LibreOffice</kk>
   <ko>Khmer Language Meta-Package for LibreOffice</ko>
   <ku>Khmer Language Meta-Package for LibreOffice</ku>
   <lt>Khmer Language Meta-Package for LibreOffice</lt>
   <mk>Khmer Language Meta-Package for LibreOffice</mk>
   <mr>Khmer Language Meta-Package for LibreOffice</mr>
   <nb_NO>Khmer Language Meta-Package for LibreOffice</nb_NO>
   <nb>Khmer språkpakke for LibreOffice</nb>
   <nl_BE>Khmer Language Meta-Package for LibreOffice</nl_BE>
   <nl>Khmer Taal Meta-Pakket voor LibreOffice</nl>
   <or>Khmer Language Meta-Package for LibreOffice</or>
   <pl>Khmerski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Quemer Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Khmer Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Khmer Language Meta-Package for LibreOffice</ro>
   <ru>Khmer Language Meta-Package for LibreOffice</ru>
   <sk>Khmer Language Meta-Package for LibreOffice</sk>
   <sl>Kmerski jezikovni meta-paket za LibreOffice</sl>
   <so>Khmer Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në kamboxhisht</sq>
   <sr>Khmer Language Meta-Package for LibreOffice</sr>
   <sv>Khmer Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Khmer สำหรับ LibreOffice</th>
   <tr>LibreOffice için Kmerce Dili Üst-Paketi</tr>
   <uk>Khmer Language Meta-Package for LibreOffice</uk>
   <vi>Khmer Language Meta-Package for LibreOffice</vi>
   <zh_CN>Khmer Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Khmer Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Khmer Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-km
libreoffice-help-km
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-km
libreoffice-help-km
libreoffice-gtk3
</uninstall_package_names>

</app>
