<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Latvian_LO_full
</name>

<description>
   <am>Latvian Language Meta-Package for LibreOffice</am>
   <ar>Latvian Language Meta-Package for LibreOffice</ar>
   <be>Latvian Language Meta-Package for LibreOffice</be>
   <bg>Latvian Language Meta-Package for LibreOffice</bg>
   <bn>Latvian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Letó per LibreOffice</ca>
   <cs>Latvian Language Meta-Package for LibreOffice</cs>
   <da>Latvian Language Meta-Package for LibreOffice</da>
   <de>Lettisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Λετονικά</el>
   <en>Latvian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma letón para LibreOffice</es_ES>
   <es>Metapaquete de idioma Letón para LibreOffice</es>
   <et>Latvian Language Meta-Package for LibreOffice</et>
   <eu>Latvian Language Meta-Package for LibreOffice</eu>
   <fa>Latvian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Latvian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Latvialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue lettone pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue lettone pour LibreOffice</fr>
   <gl_ES>Letón Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Latvian Language Meta-Package for LibreOffice</gu>
   <he_IL>Latvian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु लातवियाई भाषा मेटा-पैकेज</hi>
   <hr>Latvian Language Meta-Package for LibreOffice</hr>
   <hu>Latvian Language Meta-Package for LibreOffice</hu>
   <id>Latvian Language Meta-Package for LibreOffice</id>
   <is>Latvian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua lettone per LibreOffice</it>
   <ja>LibreOffice用ラトビア語メタパッケージ</ja>
   <kk>Latvian Language Meta-Package for LibreOffice</kk>
   <ko>Latvian Language Meta-Package for LibreOffice</ko>
   <ku>Latvian Language Meta-Package for LibreOffice</ku>
   <lt>Latvian Language Meta-Package for LibreOffice</lt>
   <mk>Latvian Language Meta-Package for LibreOffice</mk>
   <mr>Latvian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Latvian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Latvisk språkpakke for LibreOffice</nb>
   <nl_BE>Latvian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Lets Taal Meta-Pakket voor LibreOffice</nl>
   <or>Latvian Language Meta-Package for LibreOffice</or>
   <pl>Łotewski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Letão Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Letão Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Latvian Language Meta-Package for LibreOffice</ro>
   <ru>Latvian Language Meta-Package for LibreOffice</ru>
   <sk>Latvian Language Meta-Package for LibreOffice</sk>
   <sl>Latvijski jezikovni meta-paket za LibreOffice</sl>
   <so>Latvian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në letonisht</sq>
   <sr>Latvian Language Meta-Package for LibreOffice</sr>
   <sv>Lettländskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Latvian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Letonca Dili Üst-Paketi</tr>
   <uk>Latvian Language Meta-Package for LibreOffice</uk>
   <vi>Latvian Language Meta-Package for LibreOffice</vi>
   <zh_CN>Latvian Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Latvian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Latvian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-lv
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-lv
libreoffice-gtk3
</uninstall_package_names>

</app>
