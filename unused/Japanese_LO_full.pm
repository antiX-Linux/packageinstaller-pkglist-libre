<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Japanese_LO_full
</name>

<description>
   <am>Japanese localisation of LibreOffice</am>
   <ar>Japanese localisation of LibreOffice</ar>
   <be>Japanese localisation of LibreOffice</be>
   <bg>Japanese localisation of LibreOffice</bg>
   <bn>Japanese localisation of LibreOffice</bn>
   <ca>Localització en Japonès de LibreOffice</ca>
   <cs>Japanese localisation of LibreOffice</cs>
   <da>Japansk oversættelse af LibreOffice</da>
   <de>Japanische Lokalisierung von LibreOffice</de>
   <el>LibreOffice στα Ιαπωνικά</el>
   <en>Japanese localisation of LibreOffice</en>
   <es_ES>Localización Japonesa de LibreOffice</es_ES>
   <es>Localización Japonés de LibreOffice</es>
   <et>Japanese localisation of LibreOffice</et>
   <eu>Japanese localisation of LibreOffice</eu>
   <fa>Japanese localisation of LibreOffice</fa>
   <fil_PH>Japanese localisation of LibreOffice</fil_PH>
   <fi>Japanilainen kotoistus LibreOffice:lle</fi>
   <fr_BE>Localisation en japonais pour LibreOffice</fr_BE>
   <fr>Localisation en japonais pour LibreOffice</fr>
   <gl_ES>Localización de LibreOffice ao xaponés</gl_ES>
   <gu>Japanese localisation of LibreOffice</gu>
   <he_IL>תרגומי LibreOffice ליפנית</he_IL>
   <hi>थंडरबर्ड का जापानी संस्करण</hi>
   <hr>Japanese localisation of LibreOffice</hr>
   <hu>Japanese localisation of LibreOffice</hu>
   <id>Japanese localisation of LibreOffice</id>
   <is>Japanese localisation of LibreOffice</is>
   <it>Localizzazione giapponese di LibreOffice</it>
   <ja>LibreOffice の日本語版</ja>
   <kk>Japanese localisation of LibreOffice</kk>
   <ko>Japanese localisation of LibreOffice</ko>
   <ku>Japanese localisation of LibreOffice</ku>
   <lt>Japanese localisation of LibreOffice</lt>
   <mk>Japanese localisation of LibreOffice</mk>
   <mr>Japanese localisation of LibreOffice</mr>
   <nb_NO>Japanese localisation of LibreOffice</nb_NO>
   <nb>Japansk lokaltilpassing av LibreOffice</nb>
   <nl_BE>Japanese localisation of LibreOffice</nl_BE>
   <nl>Japanse lokalisatie van LibreOffice</nl>
   <or>Japanese localisation of LibreOffice</or>
   <pl>Japońska lokalizacja LibreOffice</pl>
   <pt_BR>Japonês Localização para o LibreOffice</pt_BR>
   <pt>Japonês Localização para LibreOffice</pt>
   <ro>Japanese localisation of LibreOffice</ro>
   <ru>Japanese localisation of LibreOffice</ru>
   <sk>Japanese localisation of LibreOffice</sk>
   <sl>Japonske krajevne nastavitve za LibreOffice</sl>
   <so>Japanese localisation of LibreOffice</so>
   <sq>Përkthimi në japonisht i LibreOffice-it</sq>
   <sr>Japanese localisation of LibreOffice</sr>
   <sv>Japansk lokalisering av LibreOffice</sv>
   <th>Japanese localisation ของ LibreOffice</th>
   <tr>LibreOffice Japonca yerelleştirmesi</tr>
   <uk>Japanese локалізація LibreOffice</uk>
   <vi>Japanese localisation of LibreOffice</vi>
   <zh_CN>Japanese localisation of LibreOffice</zh_CN>
   <zh_HK>Japanese localisation of LibreOffice</zh_HK>
   <zh_TW>Japanese localisation of LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-ja
libreoffice-help-ja
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-ja
libreoffice-help-ja
libreoffice-gtk3
</uninstall_package_names>

</app>
