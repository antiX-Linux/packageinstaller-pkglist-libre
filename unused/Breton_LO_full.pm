<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Breton_LO_full
</name>

<description>
   <am>Breton Language Meta-Package for LibreOffice</am>
   <ar>Breton Language Meta-Package for LibreOffice</ar>
   <be>Breton Language Meta-Package for LibreOffice</be>
   <bg>Breton Language Meta-Package for LibreOffice</bg>
   <bn>Breton Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Bretó per LibreOffice</ca>
   <cs>Breton Language Meta-Package for LibreOffice</cs>
   <da>Breton Language Meta-Package for LibreOffice</da>
   <de>Bretonisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Breton</el>
   <en>Breton Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Bretón para LibreOffice</es_ES>
   <es>Metapaquete de idioma Bretón para LibreOffice</es>
   <et>Breton Language Meta-Package for LibreOffice</et>
   <eu>Breton Language Meta-Package for LibreOffice</eu>
   <fa>Breton Language Meta-Package for LibreOffice</fa>
   <fil_PH>Breton Language Meta-Package for LibreOffice</fil_PH>
   <fi>Bretonilainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de langue bretonne pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de langue bretonne pour LibreOffice</fr>
   <gl_ES>Bretón Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Breton Language Meta-Package for LibreOffice</gu>
   <he_IL>Breton Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु ब्रेटन भाषा मेटा-पैकेज</hi>
   <hr>Breton Language Meta-Package for LibreOffice</hr>
   <hu>Breton Language Meta-Package for LibreOffice</hu>
   <id>Breton Language Meta-Package for LibreOffice</id>
   <is>Breton Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua bretone per LibreOffice</it>
   <ja>LibreOffice 用ブルトン語メタパッケージ</ja>
   <kk>Breton Language Meta-Package for LibreOffice</kk>
   <ko>Breton Language Meta-Package for LibreOffice</ko>
   <ku>Breton Language Meta-Package for LibreOffice</ku>
   <lt>Breton Language Meta-Package for LibreOffice</lt>
   <mk>Breton Language Meta-Package for LibreOffice</mk>
   <mr>Breton Language Meta-Package for LibreOffice</mr>
   <nb_NO>Breton Language Meta-Package for LibreOffice</nb_NO>
   <nb>Bretonsk språkpakke for LibreOffice</nb>
   <nl_BE>Breton Language Meta-Package for LibreOffice</nl_BE>
   <nl>Bretons Taal Meta-Pakket voor LibreOffice</nl>
   <or>Breton Language Meta-Package for LibreOffice</or>
   <pl>Bretoński meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Bretão Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Bretão Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Breton Language Meta-Package for LibreOffice</ro>
   <ru>Breton Language Meta-Package for LibreOffice</ru>
   <sk>Breton Language Meta-Package for LibreOffice</sk>
   <sl>Bretonski jezikovni meta-paket za LibreOffice</sl>
   <so>Breton Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në bretonisht</sq>
   <sr>Breton Language Meta-Package for LibreOffice</sr>
   <sv>Bretonska Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Breton สำหรับ LibreOffice</th>
   <tr>LibreOffice için Bretonca Dili Üst-Paketi</tr>
   <uk>Breton Language Meta-Package for LibreOffice</uk>
   <vi>Breton Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 布列塔尼语语言包</zh_CN>
   <zh_HK>Breton Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Breton Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-br
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-br
</uninstall_package_names>

</app>
