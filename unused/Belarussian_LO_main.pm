<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Belarussian_LO_main
</name>

<description>
   <am>Belarusian Language Meta-Package for LibreOffice</am>
   <ar>Belarusian Language Meta-Package for LibreOffice</ar>
   <be>Belarusian Language Meta-Package for LibreOffice</be>
   <bg>Belarusian Language Meta-Package for LibreOffice</bg>
   <bn>Belarusian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Bielorús per LibreOffice</ca>
   <cs>Belarusian Language Meta-Package for LibreOffice</cs>
   <da>Belarusian Language Meta-Package for LibreOffice</da>
   <de>Weißrussisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Λευκορώσικα</el>
   <en>Belarusian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Bielorruso para LibreOffice</es_ES>
   <es>Metapaquete de idioma Bielorruso para LibreOffice</es>
   <et>Belarusian Language Meta-Package for LibreOffice</et>
   <eu>Belarusian Language Meta-Package for LibreOffice</eu>
   <fa>Belarusian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Belarusian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Valko-venäläinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet de langue biélorusse pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue biélorusse pour LibreOffice</fr>
   <gl_ES>Beloruso Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Belarusian Language Meta-Package for LibreOffice</gu>
   <he_IL>Belarusian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु बेलारूसी भाषा मेटा-पैकेज</hi>
   <hr>Belarusian Language Meta-Package for LibreOffice</hr>
   <hu>Belarusian Language Meta-Package for LibreOffice</hu>
   <id>Belarusian Language Meta-Package for LibreOffice</id>
   <is>Belarusian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua bielorussa per LibreOffice</it>
   <ja>LibreOffice 用のベラルーシ語メタパッケージ</ja>
   <kk>Belarusian Language Meta-Package for LibreOffice</kk>
   <ko>Belarusian Language Meta-Package for LibreOffice</ko>
   <ku>Belarusian Language Meta-Package for LibreOffice</ku>
   <lt>Belarusian Language Meta-Package for LibreOffice</lt>
   <mk>Belarusian Language Meta-Package for LibreOffice</mk>
   <mr>Belarusian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Belarusian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Hviterussisk språkpakke for LibreOffice</nb>
   <nl_BE>Belarusian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Russisch Taal Meta-Pakket voor LibreOffice</nl>
   <or>Belarusian Language Meta-Package for LibreOffice</or>
   <pl>Białoruski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Bielorrusso Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Bielorrusso Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Belarusian Language Meta-Package for LibreOffice</ro>
   <ru>Belarusian Language Meta-Package for LibreOffice</ru>
   <sk>Belarusian Language Meta-Package for LibreOffice</sk>
   <sl>Beloruski jezikovni meta-paket za LibreOffice</sl>
   <so>Belarusian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në bjellorusisht</sq>
   <sr>Belarusian Language Meta-Package for LibreOffice</sr>
   <sv>Belarusiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Belarusian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Beyaz Rusça Dili Üst-Paketi</tr>
   <uk>Belarusian Language Meta-Package for LibreOffice</uk>
   <vi>Belarusian Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 白俄罗斯语语言包</zh_CN>
   <zh_HK>Belarusian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Belarusian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-be
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-be
libreoffice-gtk3
</uninstall_package_names>

</app>
