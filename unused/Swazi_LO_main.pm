<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Swazi_LO_main
</name>

<description>
   <am>Swazi Language Meta-Package for LibreOffice</am>
   <ar>Swazi Language Meta-Package for LibreOffice</ar>
   <be>Swazi Language Meta-Package for LibreOffice</be>
   <bg>Swazi Language Meta-Package for LibreOffice</bg>
   <bn>Swazi Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Swazi per LibreOffice</ca>
   <cs>Swazi Language Meta-Package for LibreOffice</cs>
   <da>Swazi Language Meta-Package for LibreOffice</da>
   <de>Swazi Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Swazi</el>
   <en>Swazi Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Swazi para LibreOffice</es_ES>
   <es>Metapaquete de idioma Swazi para LibreOffice</es>
   <et>Swazi Language Meta-Package for LibreOffice</et>
   <eu>Swazi Language Meta-Package for LibreOffice</eu>
   <fa>Swazi Language Meta-Package for LibreOffice</fa>
   <fil_PH>Swazi Language Meta-Package for LibreOffice</fil_PH>
   <fi>Swazinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue swazie pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue swazie pour LibreOffice</fr>
   <gl_ES>Swazi Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Swazi Language Meta-Package for LibreOffice</gu>
   <he_IL>Swazi Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु स्वाज़ी भाषा मेटा-पैकेज</hi>
   <hr>Swazi Language Meta-Package for LibreOffice</hr>
   <hu>Swazi Language Meta-Package for LibreOffice</hu>
   <id>Swazi Language Meta-Package for LibreOffice</id>
   <is>Swazi Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua swati per LibreOffice</it>
   <ja>LibreOffice用のスワジ語メタパッケージ</ja>
   <kk>Swazi Language Meta-Package for LibreOffice</kk>
   <ko>Swazi Language Meta-Package for LibreOffice</ko>
   <ku>Swazi Language Meta-Package for LibreOffice</ku>
   <lt>Swazi Language Meta-Package for LibreOffice</lt>
   <mk>Swazi Language Meta-Package for LibreOffice</mk>
   <mr>Swazi Language Meta-Package for LibreOffice</mr>
   <nb_NO>Swazi Language Meta-Package for LibreOffice</nb_NO>
   <nb>Swazi språkpakke for LibreOffice</nb>
   <nl_BE>Swazi Language Meta-Package for LibreOffice</nl_BE>
   <nl>Swazische Taal Meta-Pakket voor LibreOffice</nl>
   <or>Swazi Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Swazi dla LibreOffice</pl>
   <pt_BR>Suazi Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Swazi Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Swazi Language Meta-Package for LibreOffice</ro>
   <ru>Swazi Language Meta-Package for LibreOffice</ru>
   <sk>Swazi Language Meta-Package for LibreOffice</sk>
   <sl>Svazijski jezikovni meta-paket za LibreOffice</sl>
   <so>Swazi Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në suazi</sq>
   <sr>Swazi Language Meta-Package for LibreOffice</sr>
   <sv>Swazi Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Swazi สำหรับ LibreOffice</th>
   <tr>LibreOffice için Swazice Dili Üst-Paketi</tr>
   <uk>Swazi Language Meta-Package for LibreOffice</uk>
   <vi>Swazi Language Meta-Package for LibreOffice</vi>
   <zh_CN>Swazi Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Swazi Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Swazi Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ss
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ss
libreoffice-gtk3
</uninstall_package_names>

</app>
