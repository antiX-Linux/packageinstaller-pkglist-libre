<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Slovenian_LO_full
</name>

<description>
   <am>Slovenian Language Meta-Package for LibreOffice</am>
   <ar>Slovenian Language Meta-Package for LibreOffice</ar>
   <be>Slovenian Language Meta-Package for LibreOffice</be>
   <bg>Slovenian Language Meta-Package for LibreOffice</bg>
   <bn>Slovenian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Eslovè per LibreOffice</ca>
   <cs>Slovenian Language Meta-Package for LibreOffice</cs>
   <da>Slovenian Language Meta-Package for LibreOffice</da>
   <de>Slowenisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Σλοβενικά</el>
   <en>Slovenian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Esloveno para LibreOffice</es_ES>
   <es>Metapaquete de idioma Esloveno para LibreOffice</es>
   <et>Slovenian Language Meta-Package for LibreOffice</et>
   <eu>Slovenian Language Meta-Package for LibreOffice</eu>
   <fa>Slovenian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Slovenian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Slovenialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue slovène pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue slovène pour LibreOffice</fr>
   <gl_ES>Esloveno Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Slovenian Language Meta-Package for LibreOffice</gu>
   <he_IL>Slovenian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु स्लोवेनियाई भाषा मेटा-पैकेज</hi>
   <hr>Slovenian Language Meta-Package for LibreOffice</hr>
   <hu>Slovenian Language Meta-Package for LibreOffice</hu>
   <id>Slovenian Language Meta-Package for LibreOffice</id>
   <is>Slovenian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua slovena per LibreOffice</it>
   <ja>LibreOffice用のスロベニア語メタパッケージ</ja>
   <kk>Slovenian Language Meta-Package for LibreOffice</kk>
   <ko>Slovenian Language Meta-Package for LibreOffice</ko>
   <ku>Slovenian Language Meta-Package for LibreOffice</ku>
   <lt>Slovenian Language Meta-Package for LibreOffice</lt>
   <mk>Slovenian Language Meta-Package for LibreOffice</mk>
   <mr>Slovenian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Slovenian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Slovensk språkpakke for LibreOffice</nb>
   <nl_BE>Slovenian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Sloveens Taal Meta-Pakket voor LibreOffice</nl>
   <or>Slovenian Language Meta-Package for LibreOffice</or>
   <pl>Słoweński meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Esloveno Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Esloveno Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Slovenian Language Meta-Package for LibreOffice</ro>
   <ru>Slovenian Language Meta-Package for LibreOffice</ru>
   <sk>Slovenian Language Meta-Package for LibreOffice</sk>
   <sl>Slovenski jezikovni meta-paket za LibreOffice</sl>
   <so>Slovenian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në sllovenisht</sq>
   <sr>Slovenian Language Meta-Package for LibreOffice</sr>
   <sv>Sloveniskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Slovenian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Slovence Dili Üst-Paketi</tr>
   <uk>Slovenian Language Meta-Package for LibreOffice</uk>
   <vi>Slovenian Language Meta-Package for LibreOffice</vi>
   <zh_CN>Slovenian Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Slovenian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Slovenian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-sl
libreoffice-help-sl
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-sl
libreoffice-help-sl
libreoffice-gtk3
</uninstall_package_names>

</app>
