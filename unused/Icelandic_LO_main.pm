<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Icelandic_LO_main
</name>

<description>
   <am>Icelandic Language Meta-Package for LibreOffice</am>
   <ar>Icelandic Language Meta-Package for LibreOffice</ar>
   <be>Icelandic Language Meta-Package for LibreOffice</be>
   <bg>Icelandic Language Meta-Package for LibreOffice</bg>
   <bn>Icelandic Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Islandès per LibreOffice</ca>
   <cs>Icelandic Language Meta-Package for LibreOffice</cs>
   <da>Icelandic Language Meta-Package for LibreOffice</da>
   <de>Isländisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Ισλανδικά</el>
   <en>Icelandic Language Meta-Package for LibreOffice</en>
   <es_ES>Meta Paquete de Idioma Islandés para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Islandés para LibreOffice</es>
   <et>Icelandic Language Meta-Package for LibreOffice</et>
   <eu>Icelandic Language Meta-Package for LibreOffice</eu>
   <fa>Icelandic Language Meta-Package for LibreOffice</fa>
   <fil_PH>Icelandic Language Meta-Package for LibreOffice</fil_PH>
   <fi>Islanninkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue islandaise pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue islandaise pour LibreOffice</fr>
   <gl_ES>Islandés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Icelandic Language Meta-Package for LibreOffice</gu>
   <he_IL>Icelandic Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु आइसलैंडिक भाषा मेटा-पैकेज</hi>
   <hr>Icelandic Language Meta-Package for LibreOffice</hr>
   <hu>Icelandic Language Meta-Package for LibreOffice</hu>
   <id>Icelandic Language Meta-Package for LibreOffice</id>
   <is>Icelandic Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua islandese per LibreOffice</it>
   <ja>LibreOffice用アイスランド言語メタパッケージ</ja>
   <kk>Icelandic Language Meta-Package for LibreOffice</kk>
   <ko>Icelandic Language Meta-Package for LibreOffice</ko>
   <ku>Icelandic Language Meta-Package for LibreOffice</ku>
   <lt>Icelandic Language Meta-Package for LibreOffice</lt>
   <mk>Icelandic Language Meta-Package for LibreOffice</mk>
   <mr>Icelandic Language Meta-Package for LibreOffice</mr>
   <nb_NO>Icelandic Language Meta-Package for LibreOffice</nb_NO>
   <nb>Islandsk språkpakke for LibreOffice</nb>
   <nl_BE>Icelandic Language Meta-Package for LibreOffice</nl_BE>
   <nl>IJslandse Taal Meta-Pakket voor LibreOffice</nl>
   <or>Icelandic Language Meta-Package for LibreOffice</or>
   <pl>Islandzki meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Islandês Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Islandês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Icelandic Language Meta-Package for LibreOffice</ro>
   <ru>Icelandic Language Meta-Package for LibreOffice</ru>
   <sk>Icelandic Language Meta-Package for LibreOffice</sk>
   <sl>Islandski jezikovni meta-paket za LibreOffice</sl>
   <so>Icelandic Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në islandisht</sq>
   <sr>Icelandic Language Meta-Package for LibreOffice</sr>
   <sv>Isländskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Icelandic สำหรับ LibreOffice</th>
   <tr>LibreOffice için İzlandaca Dili Üst-Paketi</tr>
   <uk>Icelandic Language Meta-Package for LibreOffice</uk>
   <vi>Icelandic Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 冰岛语元语言包</zh_CN>
   <zh_HK>Icelandic Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Icelandic Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-is
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-is
libreoffice-gtk3
</uninstall_package_names>

</app>
