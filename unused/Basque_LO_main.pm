<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Basque_LO_main
</name>

<description>
   <am>Basque Language Meta-Package for LibreOffice</am>
   <ar>Basque Language Meta-Package for LibreOffice</ar>
   <be>Basque Language Meta-Package for LibreOffice</be>
   <bg>Basque Language Meta-Package for LibreOffice</bg>
   <bn>Basque Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Euskara per LibreOffice</ca>
   <cs>Basque Language Meta-Package for LibreOffice</cs>
   <da>Basque Language Meta-Package for LibreOffice</da>
   <de>Baskisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Βασκικά</el>
   <en>Basque Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete para LibreOffice en Euskera</es_ES>
   <es>Metapaquete para LibreOffice en Vasco</es>
   <et>Basque Language Meta-Package for LibreOffice</et>
   <eu>Basque Language Meta-Package for LibreOffice</eu>
   <fa>Basque Language Meta-Package for LibreOffice</fa>
   <fil_PH>Basque Language Meta-Package for LibreOffice</fil_PH>
   <fi>Baskilainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de langue Basque pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de langue Basque pour LibreOffice</fr>
   <gl_ES>Basco Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Basque Language Meta-Package for LibreOffice</gu>
   <he_IL>Basque Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु बास्क भाषा मेटा-पैकेज</hi>
   <hr>Basque Language Meta-Package for LibreOffice</hr>
   <hu>Basque Language Meta-Package for LibreOffice</hu>
   <id>Basque Language Meta-Package for LibreOffice</id>
   <is>Basque Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua basca per LibreOffice</it>
   <ja>LibreOffice 用のバスク語メタパッケージ</ja>
   <kk>Basque Language Meta-Package for LibreOffice</kk>
   <ko>Basque Language Meta-Package for LibreOffice</ko>
   <ku>Basque Language Meta-Package for LibreOffice</ku>
   <lt>Basque Language Meta-Package for LibreOffice</lt>
   <mk>Basque Language Meta-Package for LibreOffice</mk>
   <mr>Basque Language Meta-Package for LibreOffice</mr>
   <nb_NO>Basque Language Meta-Package for LibreOffice</nb_NO>
   <nb>Baskisk språkpakke for LibreOffice</nb>
   <nl_BE>Basque Language Meta-Package for LibreOffice</nl_BE>
   <nl>Baskisch Taal Meta-Pakket voor LibreOffice​</nl>
   <or>Basque Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka baskijskiego dla LibreOffice</pl>
   <pt_BR>Basco Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Basco Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Basque Language Meta-Package for LibreOffice</ro>
   <ru>Basque Language Meta-Package for LibreOffice</ru>
   <sk>Basque Language Meta-Package for LibreOffice</sk>
   <sl>Baskovski jezikovni meta-paket za LibreOffice</sl>
   <so>Basque Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në baskisht</sq>
   <sr>Basque Language Meta-Package for LibreOffice</sr>
   <sv>Baskiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Basque สำหรับ LibreOffice</th>
   <tr>LibreOffice için Bask Dili Üst-Paketi</tr>
   <uk>Basque Language Meta-Package for LibreOffice</uk>
   <vi>Basque Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 巴斯克语语言包</zh_CN>
   <zh_HK>Basque Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Basque Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-eu
libreoffice-help-eu
libreoffice-gtk2
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-eu
libreoffice-help-eu
libreoffice-gtk2
</uninstall_package_names>

</app>
