<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Venda_LO_main
</name>

<description>
   <am>Venda Language Meta-Package for LibreOffice</am>
   <ar>Venda Language Meta-Package for LibreOffice</ar>
   <be>Venda Language Meta-Package for LibreOffice</be>
   <bg>Venda Language Meta-Package for LibreOffice</bg>
   <bn>Venda Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Venda per LibreOffice</ca>
   <cs>Venda Language Meta-Package for LibreOffice</cs>
   <da>Venda Language Meta-Package for LibreOffice</da>
   <de>Venda Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Venda</el>
   <en>Venda Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Venda para LibreOffice</es_ES>
   <es>Metapaquete de idioma Venda para LibreOffice</es>
   <et>Venda Language Meta-Package for LibreOffice</et>
   <eu>Venda Language Meta-Package for LibreOffice</eu>
   <fa>Venda Language Meta-Package for LibreOffice</fa>
   <fil_PH>Venda Language Meta-Package for LibreOffice</fil_PH>
   <fi>Vendankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet de langues Venda pour LibreOffice</fr_BE>
   <fr>Méta-Paquet de langues Venda pour LibreOffice</fr>
   <gl_ES>Venda Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Venda Language Meta-Package for LibreOffice</gu>
   <he_IL>Venda Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु वेंदा भाषा मेटा-पैकेज</hi>
   <hr>Venda Language Meta-Package for LibreOffice</hr>
   <hu>Venda Language Meta-Package for LibreOffice</hu>
   <id>Venda Language Meta-Package for LibreOffice</id>
   <is>Venda Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua venda per LibreOffice</it>
   <ja>LibreOffice用 ヴェンダ語メタパッケージ</ja>
   <kk>Venda Language Meta-Package for LibreOffice</kk>
   <ko>Venda Language Meta-Package for LibreOffice</ko>
   <ku>Venda Language Meta-Package for LibreOffice</ku>
   <lt>Venda Language Meta-Package for LibreOffice</lt>
   <mk>Venda Language Meta-Package for LibreOffice</mk>
   <mr>Venda Language Meta-Package for LibreOffice</mr>
   <nb_NO>Venda Language Meta-Package for LibreOffice</nb_NO>
   <nb>Venda språkpakke for LibreOffice</nb>
   <nl_BE>Venda Language Meta-Package for LibreOffice</nl_BE>
   <nl>Venda Taal Meta-Pakket voor LibreOffice</nl>
   <or>Venda Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Venda dla Libre Office</pl>
   <pt_BR>Venda Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Venda Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Venda Language Meta-Package for LibreOffice</ro>
   <ru>Venda Language Meta-Package for LibreOffice</ru>
   <sk>Venda Language Meta-Package for LibreOffice</sk>
   <sl>Vendski jezikovni meta-paket za LibreOffice</sl>
   <so>Venda Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në venda</sq>
   <sr>Venda Language Meta-Package for LibreOffice</sr>
   <sv>Venda Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Venda สำหรับ LibreOffice</th>
   <tr>LibreOffice için Venda Dili Üst-Paketi</tr>
   <uk>Venda Language Meta-Package for LibreOffice</uk>
   <vi>Venda Language Meta-Package for LibreOffice</vi>
   <zh_CN>Venda Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Venda Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Venda Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ve
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ve
libreoffice-gtk3
</uninstall_package_names>

</app>
