<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Spanish_LO_full
</name>

<description>
   <am>Spanish Language Meta-Package for LibreOffice</am>
   <ar>Spanish Language Meta-Package for LibreOffice</ar>
   <be>Spanish Language Meta-Package for LibreOffice</be>
   <bg>Spanish Language Meta-Package for LibreOffice</bg>
   <bn>Spanish Language Meta-Package for LibreOffice</bn>
   <ca>Meta-paquet de llengua castellana per LibreOffice</ca>
   <cs>Spanish Language Meta-Package for LibreOffice</cs>
   <da>Spansk sprog-metapakke til LibreOffice</da>
   <de>Spanisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Ισπανικά</el>
   <en>Spanish Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Español para LibreOffice</es_ES>
   <es>Metapaquete de idioma Español para LibreOffice</es>
   <et>Spanish Language Meta-Package for LibreOffice</et>
   <eu>Spanish Language Meta-Package for LibreOffice</eu>
   <fa>Spanish Language Meta-Package for LibreOffice</fa>
   <fil_PH>Spanish Language Meta-Package for LibreOffice</fil_PH>
   <fi>Espanjalainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue espagnole pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue espagnole pour LibreOffice</fr>
   <gl_ES>Español Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Spanish Language Meta-Package for LibreOffice</gu>
   <he_IL>Spanish Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु स्पेनिश भाषा मेटा-पैकेज</hi>
   <hr>Spanish Language Meta-Package for LibreOffice</hr>
   <hu>Spanish Language Meta-Package for LibreOffice</hu>
   <id>Spanish Language Meta-Package for LibreOffice</id>
   <is>Spanish Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua spagnola per LibreOffice</it>
   <ja>LibreOffice用のスペイン語メタパッケージ</ja>
   <kk>Spanish Language Meta-Package for LibreOffice</kk>
   <ko>Spanish Language Meta-Package for LibreOffice</ko>
   <ku>Spanish Language Meta-Package for LibreOffice</ku>
   <lt>Spanish Language Meta-Package for LibreOffice</lt>
   <mk>Spanish Language Meta-Package for LibreOffice</mk>
   <mr>Spanish Language Meta-Package for LibreOffice</mr>
   <nb_NO>Spanish Language Meta-Package for LibreOffice</nb_NO>
   <nb>Spansk språkpakke for LibreOffice</nb>
   <nl_BE>Spanish Language Meta-Package for LibreOffice</nl_BE>
   <nl>Spaanse Taal Meta-Pakket voor LibreOffice</nl>
   <or>Spanish Language Meta-Package for LibreOffice</or>
   <pl>Hiszpański metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Espanhol Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Castelhano Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Spanish Language Meta-Package for LibreOffice</ro>
   <ru>Spanish Language Meta-Package for LibreOffice</ru>
   <sk>Spanish Language Meta-Package for LibreOffice</sk>
   <sl>Španski jezikovni meta-paket za LibreOffice</sl>
   <so>Spanish Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në spanjisht</sq>
   <sr>Spanish Language Meta-Package for LibreOffice</sr>
   <sv>Spanskt Språk-Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Spanish สำหรับ LibreOffice</th>
   <tr>LibreOffice için İspanyolca Dili Üst-Paketi</tr>
   <uk>Spanish Language Meta-Package for LibreOffice</uk>
   <vi>Spanish Language Meta-Package for LibreOffice</vi>
   <zh_CN>Spanish Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Spanish Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Spanish Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-es
libreoffice-help-es
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-es
libreoffice-help-es
libreoffice-gtk3
</uninstall_package_names>

</app>
