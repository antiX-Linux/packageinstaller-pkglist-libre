<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Chinese_TW_LO_main
</name>

<description>
   <am>Chinese_traditional localisation of LibreOffice</am>
   <ar>Chinese_traditional localisation of LibreOffice</ar>
   <be>Chinese_traditional localisation of LibreOffice</be>
   <bg>Chinese_traditional localisation of LibreOffice</bg>
   <bn>Chinese_traditional localisation of LibreOffice</bn>
   <ca>Localització de LibreOffice en Xinès tradicional</ca>
   <cs>Chinese_traditional localisation of LibreOffice</cs>
   <da>Kinesisk (traditionel) oversættelse af LibreOffice</da>
   <de>Chinesische (traditionell) Lokalisierung von LibreOffice</de>
   <el>LibreOffice για παραδοσιακά Κινεζικά</el>
   <en>Chinese_traditional localisation of LibreOffice</en>
   <es_ES>Localización Chino tradicional de LibreOffice</es_ES>
   <es>Localización Chino tradicional de LibreOffice</es>
   <et>Chinese_traditional localisation of LibreOffice</et>
   <eu>Chinese_traditional localisation of LibreOffice</eu>
   <fa>Chinese_traditional localisation of LibreOffice</fa>
   <fil_PH>Chinese_traditional localisation of LibreOffice</fil_PH>
   <fi>Perinteisen kiinankielen kotoistus LibreOffice:lle</fi>
   <fr_BE>Localisation en chinois-traditionnel pour LibreOffice</fr_BE>
   <fr>Localisation en chinois-traditionnel pour LibreOffice</fr>
   <gl_ES>Localización do LibreOffice en Chinés tradicional</gl_ES>
   <gu>Chinese_traditional localisation of LibreOffice</gu>
   <he_IL>Chinese_traditional localisation of LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस का पारंपरिक चीनी संस्करण</hi>
   <hr>Chinese_traditional localisation of LibreOffice</hr>
   <hu>Chinese_traditional localisation of LibreOffice</hu>
   <id>Chinese_traditional localisation of LibreOffice</id>
   <is>Chinese_traditional localisation of LibreOffice</is>
   <it>Localizzazione in cinese_tradizionale di LibreOffice</it>
   <ja>LibreOffice の繁体字中国語版</ja>
   <kk>Chinese_traditional localisation of LibreOffice</kk>
   <ko>Chinese_traditional localisation of LibreOffice</ko>
   <ku>Chinese_traditional localisation of LibreOffice</ku>
   <lt>Chinese_traditional localisation of LibreOffice</lt>
   <mk>Chinese_traditional localisation of LibreOffice</mk>
   <mr>Chinese_traditional localisation of LibreOffice</mr>
   <nb_NO>Chinese_traditional localisation of LibreOffice</nb_NO>
   <nb>Kinesisk (tradisjonell) lokaltilpassing av LibreOffice</nb>
   <nl_BE>Chinese_traditional localisation of LibreOffice</nl_BE>
   <nl>Traditioneel Chinese lokalisatie van LibreOffice</nl>
   <or>Chinese_traditional localisation of LibreOffice</or>
   <pl>Chiński tradycyjny lokalizacja LibreOffice</pl>
   <pt_BR>Chinês Tradicional Localização para o LibreOffice</pt_BR>
   <pt>Chinês_tradicional Localização para LibreOffice</pt>
   <ro>Chinese_traditional localisation of LibreOffice</ro>
   <ru>Chinese_traditional localisation of LibreOffice</ru>
   <sk>Chinese_traditional localisation of LibreOffice</sk>
   <sl>Kitajske (tradicionalno) krajevne nastavitve za LibreOffice</sl>
   <so>Chinese_traditional localisation of LibreOffice</so>
   <sq>Përkthimi i LibreOffice-it në kinezçe tradicionale</sq>
   <sr>Chinese_traditional localisation of LibreOffice</sr>
   <sv>Kinesisk_traditionell lokalisering av LibreOffice</sv>
   <th>Chinese_traditional localisation ของ LibreOffice</th>
   <tr>LibreOffice Geleneksel_Çince yerelleştirmesi</tr>
   <uk>Chinese_traditional локалізація LibreOffice</uk>
   <vi>Chinese_traditional localisation of LibreOffice</vi>
   <zh_CN>LibreOffice 繁体中文语言包</zh_CN>
   <zh_HK>Chinese_traditional localisation of LibreOffice</zh_HK>
   <zh_TW>Chinese_traditional localisation of LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-zh-tw
libreoffice-help-zh-tw
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-zh-tw
libreoffice-help-zh-tw
libreoffice-gtk3
</uninstall_package_names>

</app>
