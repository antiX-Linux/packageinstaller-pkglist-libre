<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Kannada_LO_full
</name>

<description>
   <am>Kannada Language Meta-Package for LibreOffice</am>
   <ar>Kannada Language Meta-Package for LibreOffice</ar>
   <be>Kannada Language Meta-Package for LibreOffice</be>
   <bg>Kannada Language Meta-Package for LibreOffice</bg>
   <bn>Kannada Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Kannada per LibreOffice</ca>
   <cs>Kannada Language Meta-Package for LibreOffice</cs>
   <da>Kannada Language Meta-Package for LibreOffice</da>
   <de>Kannada Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Kannada</el>
   <en>Kannada Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Kannada Language para LibreOffice</es_ES>
   <es>Metapaquete de Kannada Language para LibreOffice</es>
   <et>Kannada Language Meta-Package for LibreOffice</et>
   <eu>Kannada Language Meta-Package for LibreOffice</eu>
   <fa>Kannada Language Meta-Package for LibreOffice</fa>
   <fil_PH>Kannada Language Meta-Package for LibreOffice</fil_PH>
   <fi>Kannadankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet linguistique Kannada (Inde) pour LibreOffice</fr_BE>
   <fr>Méta-Paquet linguistique Kannada (Inde) pour LibreOffice</fr>
   <gl_ES>Canarín Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Kannada Language Meta-Package for LibreOffice</gu>
   <he_IL>Kannada Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु कन्नड़ भाषा मेटा-पैकेज</hi>
   <hr>Kannada Language Meta-Package for LibreOffice</hr>
   <hu>Kannada Language Meta-Package for LibreOffice</hu>
   <id>Kannada Language Meta-Package for LibreOffice</id>
   <is>Kannada Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua kannada per LibreOffice</it>
   <ja>LibreOffice用のカンナダ語メタパッケージ</ja>
   <kk>Kannada Language Meta-Package for LibreOffice</kk>
   <ko>Kannada Language Meta-Package for LibreOffice</ko>
   <ku>Kannada Language Meta-Package for LibreOffice</ku>
   <lt>Kannada Language Meta-Package for LibreOffice</lt>
   <mk>Kannada Language Meta-Package for LibreOffice</mk>
   <mr>Kannada Language Meta-Package for LibreOffice</mr>
   <nb_NO>Kannada Language Meta-Package for LibreOffice</nb_NO>
   <nb>Kannada språkpakke for LibreOffice</nb>
   <nl_BE>Kannada Language Meta-Package for LibreOffice</nl_BE>
   <nl>Kannada Taal Meta-Pakket voor LibreOffice</nl>
   <or>Kannada Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka kannada dla LibreOffice</pl>
   <pt_BR>Canarim Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Canarim Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Kannada Language Meta-Package for LibreOffice</ro>
   <ru>Kannada Language Meta-Package for LibreOffice</ru>
   <sk>Kannada Language Meta-Package for LibreOffice</sk>
   <sl>Kannadski jezikovni meta-paket za LibreOffice</sl>
   <so>Kannada Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në kanada</sq>
   <sr>Kannada Language Meta-Package for LibreOffice</sr>
   <sv>Kannada Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Kannada สำหรับ LibreOffice</th>
   <tr>LibreOffice için Dravidyanca Dili Üst-Paketi</tr>
   <uk>Kannada Language Meta-Package for LibreOffice</uk>
   <vi>Kannada Language Meta-Package for LibreOffice</vi>
   <zh_CN>Kannada Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Kannada Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Kannada Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-kn
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-kn
libreoffice-gtk3
</uninstall_package_names>

</app>
