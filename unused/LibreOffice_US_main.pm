<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Office
</category>

<name>
Office-LibreOffice(US)_main
</name>

<description>
   <am>Meta-Package for LibreOffice</am>
   <ar>Meta-Package for LibreOffice</ar>
   <be>Meta-Package for LibreOffice</be>
   <bg>Meta-Package for LibreOffice</bg>
   <bn>Meta-Package for LibreOffice</bn>
   <ca>Metapaquet per LibreOffice</ca>
   <cs>Meta-Package for LibreOffice</cs>
   <da>Meta-Package for LibreOffice</da>
   <de>Meta-Paket für LibreOffice</de>
   <el>Meta-Package για το LibreOffice</el>
   <en>Meta-Package for LibreOffice</en>
   <es_ES>Meta-Package para LibreOffice</es_ES>
   <es>Metapaquete para LibreOffice</es>
   <et>Meta-Package for LibreOffice</et>
   <eu>Meta-Package for LibreOffice</eu>
   <fa>Meta-Package for LibreOffice</fa>
   <fil_PH>Meta-Package for LibreOffice</fil_PH>
   <fi>Metatieto-paketti LibreOffice:lle</fi>
   <fr_BE>Meta-Paquet pour LibreOffice</fr_BE>
   <fr>Meta-Paquet pour LibreOffice</fr>
   <gl_ES>Meta-paquete para LibreOffice</gl_ES>
   <gu>Meta-Package for LibreOffice</gu>
   <he_IL>Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु मेटा-पैकेज</hi>
   <hr>Meta-Package for LibreOffice</hr>
   <hu>Meta-Package for LibreOffice</hu>
   <id>Meta-Package for LibreOffice</id>
   <is>Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto per LibreOffice</it>
   <ja>LibreOffice用メタパッケージ</ja>
   <kk>Meta-Package for LibreOffice</kk>
   <ko>Meta-Package for LibreOffice</ko>
   <ku>Meta-Package for LibreOffice</ku>
   <lt>Meta-Package for LibreOffice</lt>
   <mk>Meta-Package for LibreOffice</mk>
   <mr>Meta-Package for LibreOffice</mr>
   <nb_NO>Meta-Package for LibreOffice</nb_NO>
   <nb>LibreOffice metapakke</nb>
   <nl_BE>Meta-Package for LibreOffice</nl_BE>
   <nl>Meta-Pakket voor LibreOffice</nl>
   <or>Meta-Package for LibreOffice</or>
   <pl>Meta-Pakiet dla LibreOffice</pl>
   <pt_BR>Meta-Pacote para o LibreOffice</pt_BR>
   <pt>Meta-Pacote para LibreOffice</pt>
   <ro>Meta-Package for LibreOffice</ro>
   <ru>Мета-пакет для LibreOffice</ru>
   <sk>Meta-Package for LibreOffice</sk>
   <sl>Meta-paket za LibreOffice</sl>
   <so>Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë për LibreOffice</sq>
   <sr>Meta-Package for LibreOffice</sr>
   <sv>Meta-Paket för LibreOffice</sv>
   <th>Meta-Package สำหรับ LibreOffice</th>
   <tr>LibreOffice için Üst-Paket</tr>
   <uk>Meta-Package for LibreOffice</uk>
   <vi>Meta-Package for LibreOffice</vi>
   <zh_CN>Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-help-en-us
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-help-en-us
</uninstall_package_names>

</app>
