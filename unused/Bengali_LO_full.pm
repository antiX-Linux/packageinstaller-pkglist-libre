<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Bengali_LO_full
</name>

<description>
   <am>Bengali Language Meta-Package for LibreOffice</am>
   <ar>Bengali Language Meta-Package for LibreOffice</ar>
   <be>Bengali Language Meta-Package for LibreOffice</be>
   <bg>Bengali Language Meta-Package for LibreOffice</bg>
   <bn>Bengali Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Bengalí per LibreOffice</ca>
   <cs>Bengali Language Meta-Package for LibreOffice</cs>
   <da>Bengali Language Meta-Package for LibreOffice</da>
   <de>Bengalisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Bengali</el>
   <en>Bengali Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Bengalí para LibreOffice</es_ES>
   <es>Metapaquete de idioma Bengalí para LibreOffice</es>
   <et>Bengali Language Meta-Package for LibreOffice</et>
   <eu>Bengali Language Meta-Package for LibreOffice</eu>
   <fa>Bengali Language Meta-Package for LibreOffice</fa>
   <fil_PH>Bengali Language Meta-Package for LibreOffice</fil_PH>
   <fi>Bengalilainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet de langue bengalie pour LibreOffice</fr_BE>
   <fr>Méta-paquet de langue bengalie pour LibreOffice</fr>
   <gl_ES>Bengalí Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Bengali Language Meta-Package for LibreOffice</gu>
   <he_IL>Bengali Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु बंगाली भाषा मेटा-पैकेज</hi>
   <hr>Bengali Language Meta-Package for LibreOffice</hr>
   <hu>Bengali Language Meta-Package for LibreOffice</hu>
   <id>Bengali Language Meta-Package for LibreOffice</id>
   <is>Bengali Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua bengalese per LibreOffice</it>
   <ja>LibreOffice 用のベンガル語メタパッケージ</ja>
   <kk>Bengali Language Meta-Package for LibreOffice</kk>
   <ko>Bengali Language Meta-Package for LibreOffice</ko>
   <ku>Bengali Language Meta-Package for LibreOffice</ku>
   <lt>Bengali Language Meta-Package for LibreOffice</lt>
   <mk>Bengali Language Meta-Package for LibreOffice</mk>
   <mr>Bengali Language Meta-Package for LibreOffice</mr>
   <nb_NO>Bengali Language Meta-Package for LibreOffice</nb_NO>
   <nb>Bengalsk språkpakke for LibreOffice</nb>
   <nl_BE>Bengali Language Meta-Package for LibreOffice</nl_BE>
   <nl>Bengaals Taal Meta-Pakket voor LibreOffice</nl>
   <or>Bengali Language Meta-Package for LibreOffice</or>
   <pl>Bengalski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Bengali Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Bengali Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Bengali Language Meta-Package for LibreOffice</ro>
   <ru>Bengali Language Meta-Package for LibreOffice</ru>
   <sk>Bengali Language Meta-Package for LibreOffice</sk>
   <sl>Bengalski jezikovni meta-paket za LibreOffice</sl>
   <so>Bengali Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në bengalisht</sq>
   <sr>Bengali Language Meta-Package for LibreOffice</sr>
   <sv>Bengali Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Bengali สำหรับ LibreOffice</th>
   <tr>LibreOffice için Bengalce Dili Üst-Paketi</tr>
   <uk>Bengali Language Meta-Package for LibreOffice</uk>
   <vi>Bengali Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 孟加拉语语言包</zh_CN>
   <zh_HK>Bengali Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Bengali Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-bn
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-bn
</uninstall_package_names>

</app>
