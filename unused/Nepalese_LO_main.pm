<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Nepalese_LO_main
</name>

<description>
   <am>Nepalese Language Meta-Package for LibreOffice</am>
   <ar>Nepalese Language Meta-Package for LibreOffice</ar>
   <be>Nepalese Language Meta-Package for LibreOffice</be>
   <bg>Nepalese Language Meta-Package for LibreOffice</bg>
   <bn>Nepalese Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Nepalès per LibreOffice</ca>
   <cs>Nepalese Language Meta-Package for LibreOffice</cs>
   <da>Nepalese Language Meta-Package for LibreOffice</da>
   <de>Nepalesisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Nepalese</el>
   <en>Nepalese Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Nepalí para LibreOffice</es_ES>
   <es>Metapaquete de idioma Nepalí para LibreOffice</es>
   <et>Nepalese Language Meta-Package for LibreOffice</et>
   <eu>Nepalese Language Meta-Package for LibreOffice</eu>
   <fa>Nepalese Language Meta-Package for LibreOffice</fa>
   <fil_PH>Nepalese Language Meta-Package for LibreOffice</fil_PH>
   <fi>Nepalilainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue népalaise pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue népalaise pour LibreOffice</fr>
   <gl_ES>Nepalés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Nepalese Language Meta-Package for LibreOffice</gu>
   <he_IL>Nepalese Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु नेपाली भाषा मेटा-पैकेज</hi>
   <hr>Nepalese Language Meta-Package for LibreOffice</hr>
   <hu>Nepalese Language Meta-Package for LibreOffice</hu>
   <id>Nepalese Language Meta-Package for LibreOffice</id>
   <is>Nepalese Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua nepalese per LibreOffice</it>
   <ja>LibreOffice用ネパール語メタパッケージ</ja>
   <kk>Nepalese Language Meta-Package for LibreOffice</kk>
   <ko>Nepalese Language Meta-Package for LibreOffice</ko>
   <ku>Nepalese Language Meta-Package for LibreOffice</ku>
   <lt>Nepalese Language Meta-Package for LibreOffice</lt>
   <mk>Nepalese Language Meta-Package for LibreOffice</mk>
   <mr>Nepalese Language Meta-Package for LibreOffice</mr>
   <nb_NO>Nepalese Language Meta-Package for LibreOffice</nb_NO>
   <nb>Nepalesisk språkpakke for LibreOffice</nb>
   <nl_BE>Nepalese Language Meta-Package for LibreOffice</nl_BE>
   <nl>Nepalees Taal Meta-Pakket voor LibreOffice</nl>
   <or>Nepalese Language Meta-Package for LibreOffice</or>
   <pl>Nepalski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Nepali Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Nepalês Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Nepalese Language Meta-Package for LibreOffice</ro>
   <ru>Nepalese Language Meta-Package for LibreOffice</ru>
   <sk>Nepalese Language Meta-Package for LibreOffice</sk>
   <sl>Nepalski jezikovni meta-paket za LibreOffice</sl>
   <so>Nepalese Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në nepalisht</sq>
   <sr>Nepalese Language Meta-Package for LibreOffice</sr>
   <sv>Nepalesiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Nepalese สำหรับ LibreOffice</th>
   <tr>LibreOffice için Nepalce Dili Üst-Paketi</tr>
   <uk>Nepalese Language Meta-Package for LibreOffice</uk>
   <vi>Nepalese Language Meta-Package for LibreOffice</vi>
   <zh_CN>Nepalese Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Nepalese Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Nepalese Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ne
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-ne
libreoffice-gtk3
</uninstall_package_names>

</app>
