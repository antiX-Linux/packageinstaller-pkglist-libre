<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Lithuanian_LO_full
</name>

<description>
   <am>Lithuanian Language Meta-Package for LibreOffice</am>
   <ar>Lithuanian Language Meta-Package for LibreOffice</ar>
   <be>Lithuanian Language Meta-Package for LibreOffice</be>
   <bg>Lithuanian Language Meta-Package for LibreOffice</bg>
   <bn>Lithuanian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Lituà per LibreOffice</ca>
   <cs>Lithuanian Language Meta-Package for LibreOffice</cs>
   <da>Lithuanian Language Meta-Package for LibreOffice</da>
   <de>Litauisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Λιθουανικά</el>
   <en>Lithuanian Language Meta-Package for LibreOffice</en>
   <es_ES>Metapaquete de idioma lituano para LibreOffice</es_ES>
   <es>Metapaquete de idioma Lituano para LibreOffice</es>
   <et>Lithuanian Language Meta-Package for LibreOffice</et>
   <eu>Lithuanian Language Meta-Package for LibreOffice</eu>
   <fa>Lithuanian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Lithuanian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Liettualainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue lituanienne pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue lituanienne pour LibreOffice</fr>
   <gl_ES>Lituano Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Lithuanian Language Meta-Package for LibreOffice</gu>
   <he_IL>Lithuanian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु लिथुआनियाई भाषा मेटा-पैकेज</hi>
   <hr>Lithuanian Language Meta-Package for LibreOffice</hr>
   <hu>Lithuanian Language Meta-Package for LibreOffice</hu>
   <id>Lithuanian Language Meta-Package for LibreOffice</id>
   <is>Lithuanian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua lituana per LibreOffice</it>
   <ja>LibreOffice用のリトアニア語メタパッケージ</ja>
   <kk>Lithuanian Language Meta-Package for LibreOffice</kk>
   <ko>Lithuanian Language Meta-Package for LibreOffice</ko>
   <ku>Lithuanian Language Meta-Package for LibreOffice</ku>
   <lt>Lithuanian Language Meta-Package for LibreOffice</lt>
   <mk>Lithuanian Language Meta-Package for LibreOffice</mk>
   <mr>Lithuanian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Lithuanian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Litauisk språkpakke for LibreOffice</nb>
   <nl_BE>Lithuanian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Litouws Taal Meta-Pakket voor LibreOffice</nl>
   <or>Lithuanian Language Meta-Package for LibreOffice</or>
   <pl>Litewski meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Lituano Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Lituano Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Lithuanian Language Meta-Package for LibreOffice</ro>
   <ru>Lithuanian Language Meta-Package for LibreOffice</ru>
   <sk>Lithuanian Language Meta-Package for LibreOffice</sk>
   <sl>Litvanski jezikovni meta-paket za LibreOffice</sl>
   <so>Lithuanian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në lituanisht</sq>
   <sr>Lithuanian Language Meta-Package for LibreOffice</sr>
   <sv>Litauiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Lithuanian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Litvanyaca Dili Üst-Paketi</tr>
   <uk>Lithuanian Language Meta-Package for LibreOffice</uk>
   <vi>Lithuanian Language Meta-Package for LibreOffice</vi>
   <zh_CN>Lithuanian Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Lithuanian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Lithuanian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-lt
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-lt
libreoffice-gtk3
</uninstall_package_names>

</app>
