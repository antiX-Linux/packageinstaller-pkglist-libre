<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Croatian_LO_full
</name>

<description>
   <am>Croatian LibreOffice Language Meta-Package</am>
   <ar>Croatian LibreOffice Language Meta-Package</ar>
   <be>Croatian LibreOffice Language Meta-Package</be>
   <bg>Croatian LibreOffice Language Meta-Package</bg>
   <bn>Croatian LibreOffice Language Meta-Package</bn>
   <ca>Localització de LibreOffice en Croata</ca>
   <cs>Croatian LibreOffice Language Meta-Package</cs>
   <da>Kroatisk LibreOffice sprog-metapakke</da>
   <de>Kroatisches LibreOffice Meta-Paket</de>
   <el>LibreOffice στα Κροατικά</el>
   <en>Croatian LibreOffice Language Meta-Package</en>
   <es_ES>Meta paquete de lenguaje Croata para LibreOffice</es_ES>
   <es>Metapaquete de lenguaje Croata para LibreOffice</es>
   <et>Croatian LibreOffice Language Meta-Package</et>
   <eu>Croatian LibreOffice Language Meta-Package</eu>
   <fa>Croatian LibreOffice Language Meta-Package</fa>
   <fil_PH>Croatian LibreOffice Language Meta-Package</fil_PH>
   <fi>Kroatiankielinen metatieto-paketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet langue croate pour LibreOffice</fr_BE>
   <fr>Méta-Paquet langue croate pour LibreOffice</fr>
   <gl_ES>Croata Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Croatian LibreOffice Language Meta-Package</gu>
   <he_IL>Croatian LibreOffice Language Meta-Package</he_IL>
   <hi>लिब्रे-ऑफिस हेतु क्रोशियाई भाषा मेटा-पैकेज</hi>
   <hr>Croatian LibreOffice Language Meta-Package</hr>
   <hu>Croatian LibreOffice Language Meta-Package</hu>
   <id>Croatian LibreOffice Language Meta-Package</id>
   <is>Croatian LibreOffice Language Meta-Package</is>
   <it>Meta-pacchetto della lingua croata per LibreOffice</it>
   <ja>LibreOffice のクロアチア語用メタパッケージ</ja>
   <kk>Croatian LibreOffice Language Meta-Package</kk>
   <ko>Croatian LibreOffice Language Meta-Package</ko>
   <ku>Croatian LibreOffice Language Meta-Package</ku>
   <lt>Croatian LibreOffice Language Meta-Package</lt>
   <mk>Croatian LibreOffice Language Meta-Package</mk>
   <mr>Croatian LibreOffice Language Meta-Package</mr>
   <nb_NO>Croatian LibreOffice Language Meta-Package</nb_NO>
   <nb>Kroatisk språkpakke for LibreOffice</nb>
   <nl_BE>Croatian LibreOffice Language Meta-Package</nl_BE>
   <nl>Kroatische LibreOffice Taal Meta-Pakket</nl>
   <or>Croatian LibreOffice Language Meta-Package</or>
   <pl>Chorwacki metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Croata Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Croata Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Croatian LibreOffice Language Meta-Package</ro>
   <ru>Croatian LibreOffice Language Meta-Package</ru>
   <sk>Croatian LibreOffice Language Meta-Package</sk>
   <sl>Hrvaški jezikovni metapaket za LibreOffice</sl>
   <so>Croatian LibreOffice Language Meta-Package</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në kroatisht</sq>
   <sr>Croatian LibreOffice Language Meta-Package</sr>
   <sv>Kroatisk LibreOffice Språk Meta-Paket</sv>
   <th>Meta-Package ภาษา Croatian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Hırvatça Dili Üst-Paketi</tr>
   <uk>Croatian LibreOffice Language Meta-Package</uk>
   <vi>Croatian LibreOffice Language Meta-Package</vi>
   <zh_CN>LibreOffice 克罗地亚语语言包</zh_CN>
   <zh_HK>Croatian LibreOffice Language Meta-Package</zh_HK>
   <zh_TW>Croatian LibreOffice Language Meta-Package</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-hr
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-hr
libreoffice-gtk3
</uninstall_package_names>

</app>
