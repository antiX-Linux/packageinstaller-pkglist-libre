<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Portuguese
</name>

<description>
   <am>Portuguese dictionary for hunspell</am>
   <ar>Portuguese dictionary for hunspell</ar>
   <be>Portuguese dictionary for hunspell</be>
   <bg>Portuguese dictionary for hunspell</bg>
   <bn>Portuguese dictionary for hunspell</bn>
   <ca>Diccionari Portuguès per hunspell</ca>
   <cs>Portuguese dictionary for hunspell</cs>
   <da>Portuguese dictionary for hunspell</da>
   <de>Portugiesisches Wörterbuch für “Hunspell”</de>
   <el>Πορτογαλικά λεξικό για hunspell</el>
   <en>Portuguese dictionary for hunspell</en>
   <es_ES>Diccionario Portugués para hunspell</es_ES>
   <es>Diccionario Portugués para hunspell</es>
   <et>Portuguese dictionary for hunspell</et>
   <eu>Portuguese dictionary for hunspell</eu>
   <fa>Portuguese dictionary for hunspell</fa>
   <fil_PH>Portuguese dictionary for hunspell</fil_PH>
   <fi>Portuguese dictionary for hunspell</fi>
   <fr_BE>Portugais dictionnaire pour hunspell</fr_BE>
   <fr>Portugais dictionnaire pour hunspell</fr>
   <gl_ES>Portuguese dictionary for hunspell</gl_ES>
   <gu>Portuguese dictionary for hunspell</gu>
   <he_IL>Portuguese dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु पुर्तगाली शब्दकोष</hi>
   <hr>Portuguese dictionary for hunspell</hr>
   <hu>Portuguese dictionary for hunspell</hu>
   <id>Portuguese dictionary for hunspell</id>
   <is>Portuguese dictionary for hunspell</is>
   <it>Dizionario portoghese per hunspell</it>
   <ja>Hunspell 用ポルトガル語辞書</ja>
   <kk>Portuguese dictionary for hunspell</kk>
   <ko>Portuguese dictionary for hunspell</ko>
   <ku>Portuguese dictionary for hunspell</ku>
   <lt>Portuguese dictionary for hunspell</lt>
   <mk>Portuguese dictionary for hunspell</mk>
   <mr>Portuguese dictionary for hunspell</mr>
   <nb_NO>Portuguese dictionary for hunspell</nb_NO>
   <nb>Portugisisk ordliste for hunspell</nb>
   <nl_BE>Portuguese dictionary for hunspell</nl_BE>
   <nl>Portuguese dictionary for hunspell</nl>
   <or>Portuguese dictionary for hunspell</or>
   <pl>Portuguese dictionary for hunspell</pl>
   <pt_BR>Dicionário Português (PT) para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Português para o hunspell</pt>
   <ro>Portuguese dictionary for hunspell</ro>
   <ru>Portuguese dictionary for hunspell</ru>
   <sk>Portuguese dictionary for hunspell</sk>
   <sl>Portugalščina - slovar za hunspell</sl>
   <so>Portuguese dictionary for hunspell</so>
   <sq>Fjalor portugalisht hunspell</sq>
   <sr>Portuguese dictionary for hunspell</sr>
   <sv>Portugisisk ordbok för hunspell</sv>
   <th>Portuguese dictionary for hunspell</th>
   <tr>Hunspell için Portekizce sözlük</tr>
   <uk>Portuguese dictionary for hunspell</uk>
   <vi>Portuguese dictionary for hunspell</vi>
   <zh_CN>Portuguese dictionary for hunspell</zh_CN>
   <zh_HK>Portuguese dictionary for hunspell</zh_HK>
   <zh_TW>Portuguese dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-pt-pt
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-pt-pt
</uninstall_package_names>

</app>
