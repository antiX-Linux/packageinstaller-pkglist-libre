<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Graphics
</category>

<name>
Blender
</name>

<description>
   <am>a free and open source 3D animation suite</am>
   <ar>a free and open source 3D animation suite</ar>
   <be>a free and open source 3D animation suite</be>
   <bg>a free and open source 3D animation suite</bg>
   <bn>a free and open source 3D animation suite</bn>
   <ca>Suite d'animació 3D lliure i de codi obert</ca>
   <cs>a free and open source 3D animation suite</cs>
   <da>en fri og open source 3D-animationspakke</da>
   <de>Eine kostenlose und quelloffene 3D-Animationssuite</de>
   <el>Ελεύθερη σουίτα 3D animation</el>
   <en>a free and open source 3D animation suite</en>
   <es_ES>Una suite de animación en 3D libre y de código abierto</es_ES>
   <es>Una suite de animación en 3D libre y de código abierto</es>
   <et>a free and open source 3D animation suite</et>
   <eu>a free and open source 3D animation suite</eu>
   <fa>a free and open source 3D animation suite</fa>
   <fil_PH>a free and open source 3D animation suite</fil_PH>
   <fi>Vapaa ja koodiltaan avoin 3D animaatio-ohjelmisto</fi>
   <fr_BE>Une suite logicielle libre et open source d'animation 3D</fr_BE>
   <fr>Une suite logicielle libre et open source d'animation 3D</fr>
   <gl_ES>Conxunto de animación 3D libre e de código aberto</gl_ES>
   <gu>a free and open source 3D animation suite</gu>
   <he_IL>a free and open source 3D animation suite</he_IL>
   <hi>निःशुल्क व मुक्त स्रोत 3डी एनीमेशन प्रोग्राम</hi>
   <hr>a free and open source 3D animation suite</hr>
   <hu>a free and open source 3D animation suite</hu>
   <id>a free and open source 3D animation suite</id>
   <is>a free and open source 3D animation suite</is>
   <it>Programma gratuito ed open source di animazione 3D</it>
   <ja>フリーでオープンソースの3Dアニメーション・スイート</ja>
   <kk>a free and open source 3D animation suite</kk>
   <ko>a free and open source 3D animation suite</ko>
   <ku>a free and open source 3D animation suite</ku>
   <lt>a free and open source 3D animation suite</lt>
   <mk>a free and open source 3D animation suite</mk>
   <mr>a free and open source 3D animation suite</mr>
   <nb_NO>a free and open source 3D animation suite</nb_NO>
   <nb>en fri og åpen programpakke for 3D-animasjon</nb>
   <nl_BE>a free and open source 3D animation suite</nl_BE>
   <nl>een gratis en open source 3D animatie suite</nl>
   <or>a free and open source 3D animation suite</or>
   <pl>wolne i otwarte oprogramowanie do modelowania i renderowania obrazów oraz animacji 3D</pl>
   <pt_BR>Conjunto de animação 3D livre e de código fonte aberto</pt_BR>
   <pt>Conjunto de animação 3D livre e de fonte aberta</pt>
   <ro>a free and open source 3D animation suite</ro>
   <ru>Бесплатная среда 3D анимации с открытым кодом</ru>
   <sk>a free and open source 3D animation suite</sk>
   <sl>brezplačen odprtokodni paket za 3D animacijo</sl>
   <so>a free and open source 3D animation suite</so>
   <sq>Një suitë e lirë dhe me burim të hapët animacionesh 3D</sq>
   <sr>a free and open source 3D animation suite</sr>
   <sv>en fri och öppen källkods 3D animationssvit</sv>
   <th>โปรแกรมทำ 3D Animation ฟรีและ Open-source</th>
   <tr>özgür ve açık kaynaklı 3 boyutlu canlandırma takımı</tr>
   <uk>вільний та відкритий пакет для роботи з 3D графікою</uk>
   <vi>a free and open source 3D animation suite</vi>
   <zh_CN>免费开源的 3D 动画套件</zh_CN>
   <zh_HK>a free and open source 3D animation suite</zh_HK>
   <zh_TW>a free and open source 3D animation suite</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
blender
libgl1-mesa-dri
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
blender
</uninstall_package_names>
</app>
