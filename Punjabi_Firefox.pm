<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Punjabi_Firefox
</name>

<description>
   <am>Punjabi localisation of Firefox</am>
   <ar>Punjabi localisation of Firefox</ar>
   <be>Punjabi localisation of Firefox</be>
   <bg>Punjabi localisation of Firefox</bg>
   <bn>Punjabi localisation of Firefox</bn>
   <ca>Localització de Firefox en Punjabí</ca>
   <cs>Punjabi localisation of Firefox</cs>
   <da>Punjabi localisation of Firefox</da>
   <de>Pakistanische und Indische Lokalisation für die Sprache “Pandschabisch” von “Firefox”</de>
   <el>Εντοπισμός του Firefox στα Παντζάμπι</el>
   <en>Punjabi localisation of Firefox</en>
   <es_ES>Localización Punjabi de Firefox</es_ES>
   <es>Localización Punjabi de Firefox</es>
   <et>Punjabi localisation of Firefox</et>
   <eu>Punjabi localisation of Firefox</eu>
   <fa>Punjabi localisation of Firefox</fa>
   <fil_PH>Punjabi localisation of Firefox</fil_PH>
   <fi>Punjabi localisation of Firefox</fi>
   <fr_BE>Localisation en pendjabi (Inde) pour Firefox</fr_BE>
   <fr>Localisation en pendjabi (Inde) pour Firefox</fr>
   <gl_ES>Punjabi localisation of Firefox</gl_ES>
   <gu>Punjabi localisation of Firefox</gu>
   <he_IL>Punjabi localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का पंजाबी संस्करण</hi>
   <hr>Punjabi localisation of Firefox</hr>
   <hu>Punjabi localisation of Firefox</hu>
   <id>Punjabi localisation of Firefox</id>
   <is>Punjabi localisation of Firefox</is>
   <it>Localizzazione punjabi di Firefox</it>
   <ja>パンジャーブ語版 Firefox</ja>
   <kk>Punjabi localisation of Firefox</kk>
   <ko>Punjabi localisation of Firefox</ko>
   <ku>Punjabi localisation of Firefox</ku>
   <lt>Punjabi localisation of Firefox</lt>
   <mk>Punjabi localisation of Firefox</mk>
   <mr>Punjabi localisation of Firefox</mr>
   <nb_NO>Punjabi localisation of Firefox</nb_NO>
   <nb>Punjabi lokaltilpassing av Firefox</nb>
   <nl_BE>Punjabi localisation of Firefox</nl_BE>
   <nl>Punjabi localisation of Firefox</nl>
   <or>Punjabi localisation of Firefox</or>
   <pl>Punjabi localisation of Firefox</pl>
   <pt_BR>Punjabi Localização para o Firefox</pt_BR>
   <pt>Panjabi Localização para Firefox</pt>
   <ro>Punjabi localisation of Firefox</ro>
   <ru>Punjabi localisation of Firefox</ru>
   <sk>Punjabi localisation of Firefox</sk>
   <sl>Pundžabi krajevne nastavitve za Firefox</sl>
   <so>Punjabi localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në panxhabi</sq>
   <sr>Punjabi localisation of Firefox</sr>
   <sv>Punjabi lokalisering för Firefox</sv>
   <th>Punjabi localisation of Firefox</th>
   <tr>Firefox'un  Pencapça yerelleştirmesi</tr>
   <uk>Punjabi localisation of Firefox</uk>
   <vi>Punjabi localisation of Firefox</vi>
   <zh_CN>Punjabi localisation of Firefox</zh_CN>
   <zh_HK>Punjabi localisation of Firefox</zh_HK>
   <zh_TW>Punjabi localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-l10n-pa-in
</install_package_names>

<postinstall>
</postinstall>

<uninstall_package_names>
firefox-l10n-pa-in
</uninstall_package_names>

</app>
