<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Xhosa_Default_Firefox_esr
</name>

<description>
   <am>Xhosa localisation of Firefox ESR</am>
   <ar>Xhosa localisation of Firefox ESR</ar>
   <be>Xhosa localisation of Firefox ESR</be>
   <bg>Xhosa localisation of Firefox ESR</bg>
   <bn>Xhosa localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Xhosa</ca>
   <cs>Xhosa localisation of Firefox ESR</cs>
   <da>Xhosa localisation of Firefox ESR</da>
   <de>Afrikanische Lokalisation für die Bantusprache “isiXhosa” von “Firefox ESR”</de>
   <el>Xhosa για Firefox ESR</el>
   <en>Xhosa localisation of Firefox ESR</en>
   <es_ES>Localización Xhosa de Firefox ESR</es_ES>
   <es>Localización Xhosa de Firefox ESR</es>
   <et>Xhosa localisation of Firefox ESR</et>
   <eu>Xhosa localisation of Firefox ESR</eu>
   <fa>Xhosa localisation of Firefox ESR</fa>
   <fil_PH>Xhosa localisation of Firefox ESR</fil_PH>
   <fi>Xhosa localisation of Firefox ESR</fi>
   <fr_BE>Localisation en xhosa pour Firefox ESR</fr_BE>
   <fr>Localisation en xhosa pour Firefox ESR</fr>
   <gl_ES>Xhosa localisation of Firefox ESR</gl_ES>
   <gu>Xhosa localisation of Firefox ESR</gu>
   <he_IL>Xhosa localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का कोसा संस्करण</hi>
   <hr>Xhosa localisation of Firefox ESR</hr>
   <hu>Xhosa localisation of Firefox ESR</hu>
   <id>Xhosa localisation of Firefox ESR</id>
   <is>Xhosa localisation of Firefox ESR</is>
   <it>Localizzazione xhosa di Firefox ESR</it>
   <ja>コサ語版 Firefox ESR</ja>
   <kk>Xhosa localisation of Firefox ESR</kk>
   <ko>Xhosa localisation of Firefox ESR</ko>
   <ku>Xhosa localisation of Firefox ESR</ku>
   <lt>Xhosa localisation of Firefox ESR</lt>
   <mk>Xhosa localisation of Firefox ESR</mk>
   <mr>Xhosa localisation of Firefox ESR</mr>
   <nb_NO>Xhosa localisation of Firefox ESR</nb_NO>
   <nb>Xhosa lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Xhosa localisation of Firefox ESR</nl_BE>
   <nl>Xhosa localisation of Firefox ESR</nl>
   <or>Xhosa localisation of Firefox ESR</or>
   <pl>Xhosa localisation of Firefox ESR</pl>
   <pt_BR>Xhosa Localização para o Firefox ESR</pt_BR>
   <pt>Xosa Localização para Firefox ESR</pt>
   <ro>Xhosa localisation of Firefox ESR</ro>
   <ru>Xhosa localisation of Firefox ESR</ru>
   <sk>Xhosa localisation of Firefox ESR</sk>
   <sl>Khoške krajevne nastavitve za Firefox ESR</sl>
   <so>Xhosa localisation of Firefox ESR</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në xosa</sq>
   <sr>Xhosa localisation of Firefox ESR</sr>
   <sv>Xhosa lokalisering av Firefox ESR</sv>
   <th>Xhosa localisation of Firefox ESR</th>
   <tr>Firefox ESR Xhosa Dili yerelleştirmesi</tr>
   <uk>Xhosa localisation of Firefox ESR</uk>
   <vi>Xhosa localisation of Firefox ESR</vi>
   <zh_CN>Xhosa localisation of Firefox ESR</zh_CN>
   <zh_HK>Xhosa localisation of Firefox ESR</zh_HK>
   <zh_TW>Xhosa localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-esr-l10n-xh
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-xh
</uninstall_package_names>

</app>
