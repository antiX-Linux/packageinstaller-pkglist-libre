<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Norwegian
</name>

<description>
   <am>Norwegian dictionary for hunspell</am>
   <ar>Norwegian dictionary for hunspell</ar>
   <be>Norwegian dictionary for hunspell</be>
   <bg>Norwegian dictionary for hunspell</bg>
   <bn>Norwegian dictionary for hunspell</bn>
   <ca>Diccionari Noruec per hunspell</ca>
   <cs>Norwegian dictionary for hunspell</cs>
   <da>Norwegian dictionary for hunspell</da>
   <de>Norwegisches Wörterbuch für “Hunspell”</de>
   <el>Νορβηγικά λεξικό για hunspell</el>
   <en>Norwegian dictionary for hunspell</en>
   <es_ES>Diccionario Noruego para hunspell</es_ES>
   <es>Diccionario Noruego para hunspell</es>
   <et>Norwegian dictionary for hunspell</et>
   <eu>Norwegian dictionary for hunspell</eu>
   <fa>Norwegian dictionary for hunspell</fa>
   <fil_PH>Norwegian dictionary for hunspell</fil_PH>
   <fi>Norwegian dictionary for hunspell</fi>
   <fr_BE>Norvégien dictionnaire pour hunspell</fr_BE>
   <fr>Norvégien dictionnaire pour hunspell</fr>
   <gl_ES>Norwegian dictionary for hunspell</gl_ES>
   <gu>Norwegian dictionary for hunspell</gu>
   <he_IL>Norwegian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु नॉर्वेजियाई शब्दकोष</hi>
   <hr>Norwegian dictionary for hunspell</hr>
   <hu>Norwegian dictionary for hunspell</hu>
   <id>Norwegian dictionary for hunspell</id>
   <is>Norwegian dictionary for hunspell</is>
   <it>Dizionario norvegese per hunspell</it>
   <ja>Hunspell 用ノルウェー語辞書</ja>
   <kk>Norwegian dictionary for hunspell</kk>
   <ko>Norwegian dictionary for hunspell</ko>
   <ku>Norwegian dictionary for hunspell</ku>
   <lt>Norwegian dictionary for hunspell</lt>
   <mk>Norwegian dictionary for hunspell</mk>
   <mr>Norwegian dictionary for hunspell</mr>
   <nb_NO>Norwegian dictionary for hunspell</nb_NO>
   <nb>Norsk (bokmålsk) ordliste for hunspell</nb>
   <nl_BE>Norwegian dictionary for hunspell</nl_BE>
   <nl>Norwegian dictionary for hunspell</nl>
   <or>Norwegian dictionary for hunspell</or>
   <pl>Norwegian dictionary for hunspell</pl>
   <pt_BR>Dicionário Norueguês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Norueguês para hunspell</pt>
   <ro>Norwegian dictionary for hunspell</ro>
   <ru>Norwegian dictionary for hunspell</ru>
   <sk>Norwegian dictionary for hunspell</sk>
   <sl>Norveški slovar za hunspell</sl>
   <so>Norwegian dictionary for hunspell</so>
   <sq>Fjalor norvegjisht për hunspell</sq>
   <sr>Norwegian dictionary for hunspell</sr>
   <sv>Norsk ordbok för hunspell</sv>
   <th>Norwegian dictionary for hunspell</th>
   <tr>Hunspell için Norveççe sözlük</tr>
   <uk>Norwegian dictionary for hunspell</uk>
   <vi>Norwegian dictionary for hunspell</vi>
   <zh_CN>Norwegian dictionary for hunspell</zh_CN>
   <zh_HK>Norwegian dictionary for hunspell</zh_HK>
   <zh_TW>Norwegian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-no
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-no
</uninstall_package_names>

</app>
