<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
Connman
</name>

<description>
   <am>Connection Manager is designed to be a light network manager</am>
   <ar>Connection Manager is designed to be a light network manager</ar>
   <be>Connection Manager is designed to be a light network manager</be>
   <bg>Connection Manager is designed to be a light network manager</bg>
   <bn>Connection Manager is designed to be a light network manager</bn>
   <ca>Connection Manager és un gestor de xarxes lleuger</ca>
   <cs>Connection Manager is designed to be a light network manager</cs>
   <da>Connection Manager is designed to be a light network manager</da>
   <de>“Connection Manager” ist ein einfacher Netzwerkmanager</de>
   <el>Το Connection Manager έχει σχεδιαστεί για να είναι ένας ελαφρύς διαχειριστής δικτύου</el>
   <en>Connection Manager is designed to be a light network manager</en>
   <es_ES>Gestor de conexiones de red ligero</es_ES>
   <es>Gestor de conexiones de red ligero</es>
   <et>Connection Manager is designed to be a light network manager</et>
   <eu>Connection Manager is designed to be a light network manager</eu>
   <fa>Connection Manager is designed to be a light network manager</fa>
   <fil_PH>Connection Manager is designed to be a light network manager</fil_PH>
   <fi>Connection Manager is designed to be a light network manager</fi>
   <fr_BE>Connection Manager est conçu pour être un gestionnaire de réseau léger.</fr_BE>
   <fr>Connection Manager est conçu pour être un gestionnaire de réseau léger.</fr>
   <gl_ES>Connection Manager is designed to be a light network manager</gl_ES>
   <gu>Connection Manager is designed to be a light network manager</gu>
   <he_IL>Connection Manager is designed to be a light network manager</he_IL>
   <hi>एक सरल नेटवर्क प्रबंधक के उद्देश्य से निर्मित कनेक्शन प्रबंधक</hi>
   <hr>Connection Manager is designed to be a light network manager</hr>
   <hu>Connection Manager is designed to be a light network manager</hu>
   <id>Connection Manager is designed to be a light network manager</id>
   <is>Connection Manager is designed to be a light network manager</is>
   <it>Connection Manager, un gestore di rete progettato per essere leggero</it>
   <ja>コネクション・マネージャーは、軽量ネットワーク・マネージャーとして設計されています</ja>
   <kk>Connection Manager is designed to be a light network manager</kk>
   <ko>Connection Manager is designed to be a light network manager</ko>
   <ku>Connection Manager is designed to be a light network manager</ku>
   <lt>Connection Manager is designed to be a light network manager</lt>
   <mk>Connection Manager is designed to be a light network manager</mk>
   <mr>Connection Manager is designed to be a light network manager</mr>
   <nb_NO>Connection Manager is designed to be a light network manager</nb_NO>
   <nb>«Connection Manager» er en lettvekts nettverksbehandler</nb>
   <nl_BE>Connection Manager is designed to be a light network manager</nl_BE>
   <nl>Connection Manager is ontworpen als een lichte netwerkbeheerder</nl>
   <or>Connection Manager is designed to be a light network manager</or>
   <pl>Connection Manager is designed to be a light network manager</pl>
   <pt_BR>O 'Connection Manager' é projetado para ser um gerenciador de rede leve</pt_BR>
   <pt>O Connection Manager (Gestor de Ligações) é um gestor leve de ligações de rede</pt>
   <ro>Connection Manager is designed to be a light network manager</ro>
   <ru>Connection Manager is designed to be a light network manager</ru>
   <sk>Connection Manager is designed to be a light network manager</sk>
   <sl>Upravljalnik povezav je izdelan kot lahek omrežni upravljalnik</sl>
   <so>Connection Manager is designed to be a light network manager</so>
   <sq>Përgjegjësi i Lidhjeve është konceptuar për të qenë një përgjegjës rrjetesh i peshës së lehtë</sq>
   <sr>Connection Manager is designed to be a light network manager</sr>
   <sv>Connection Manager är designad för att vara en lätt nätverkshanterare</sv>
   <th>Connection Manager is designed to be a light network manager</th>
   <tr>Bağlantı Yöneticisi, hafif bir ağ yöneticisi olacak şekilde tasarlanmıştır</tr>
   <uk>Connection Manager is designed to be a light network manager</uk>
   <vi>Connection Manager is designed to be a light network manager</vi>
   <zh_CN>Connection Manager is designed to be a light network manager</zh_CN>
   <zh_HK>Connection Manager is designed to be a light network manager</zh_HK>
   <zh_TW>Connection Manager is designed to be a light network manager</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
connman

</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
connman
</uninstall_package_names>
</app>
