<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Danish_Default_Firefox_esr
</name>

<description>
   <am>Danish localisation of Firefox ESR</am>
   <ar>Danish localisation of Firefox ESR</ar>
   <be>Danish localisation of Firefox ESR</be>
   <bg>Danish localisation of Firefox ESR</bg>
   <bn>Danish localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Danès</ca>
   <cs>Danish localisation of Firefox ESR</cs>
   <da>Danish localisation of Firefox ESR</da>
   <de>Dänische Lokalisation von “Firefox ESR”</de>
   <el>Δανικά για Firefox ESR</el>
   <en>Danish localisation of Firefox ESR</en>
   <es_ES>Localización Danesa de Firefox ESR</es_ES>
   <es>Localización Danés de Firefox ESR</es>
   <et>Danish localisation of Firefox ESR</et>
   <eu>Danish localisation of Firefox ESR</eu>
   <fa>Danish localisation of Firefox ESR</fa>
   <fil_PH>Danish localisation of Firefox ESR</fil_PH>
   <fi>Danish localisation of Firefox ESR</fi>
   <fr_BE>Localisation en danois pour Firefox ESR</fr_BE>
   <fr>Localisation en danois pour Firefox ESR</fr>
   <gl_ES>Danish localisation of Firefox ESR</gl_ES>
   <gu>Danish localisation of Firefox ESR</gu>
   <he_IL>Danish localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का डेनिश संस्करण</hi>
   <hr>Danish localisation of Firefox ESR</hr>
   <hu>Danish localisation of Firefox ESR</hu>
   <id>Danish localisation of Firefox ESR</id>
   <is>Danish localisation of Firefox ESR</is>
   <it>Localizzazione danese di Firefox ESR</it>
   <ja>デンマーク語版 Firefox ESR</ja>
   <kk>Danish localisation of Firefox ESR</kk>
   <ko>Danish localisation of Firefox ESR</ko>
   <ku>Danish localisation of Firefox ESR</ku>
   <lt>Danish localisation of Firefox ESR</lt>
   <mk>Danish localisation of Firefox ESR</mk>
   <mr>Danish localisation of Firefox ESR</mr>
   <nb_NO>Danish localisation of Firefox ESR</nb_NO>
   <nb>Dansk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Danish localisation of Firefox ESR</nl_BE>
   <nl>Deense lokalisatie van Firefox ESR</nl>
   <or>Danish localisation of Firefox ESR</or>
   <pl>Danish localisation of Firefox ESR</pl>
   <pt_BR>Dinamarquês Localização para o Firefox ESR</pt_BR>
   <pt>Dinamarquês Localização para Firefox ESR</pt>
   <ro>Danish localisation of Firefox ESR</ro>
   <ru>Danish localisation of Firefox ESR</ru>
   <sk>Danish localisation of Firefox ESR</sk>
   <sl>Danske krajevne nastavitve za Firefox ESR</sl>
   <so>Danish localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në danisht</sq>
   <sr>Danish localisation of Firefox ESR</sr>
   <sv>Dansk lokalisering av Firefox ESR</sv>
   <th>Danish localisation of Firefox ESR</th>
   <tr>Firefox ESR Danca yerelleştirmesi</tr>
   <uk>Danish localisation of Firefox ESR</uk>
   <vi>Danish localisation of Firefox ESR</vi>
   <zh_CN>Danish localisation of Firefox ESR</zh_CN>
   <zh_HK>Danish localisation of Firefox ESR</zh_HK>
   <zh_TW>Danish localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-da
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-da
</uninstall_package_names>

</app>
