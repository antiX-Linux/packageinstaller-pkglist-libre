<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hebrew
</name>

<description>
   <am>Hebrew dictionary for hunspell</am>
   <ar>Hebrew dictionary for hunspell</ar>
   <be>Hebrew dictionary for hunspell</be>
   <bg>Hebrew dictionary for hunspell</bg>
   <bn>Hebrew dictionary for hunspell</bn>
   <ca>Diccionari Hebreu per hunspell</ca>
   <cs>Hebrew dictionary for hunspell</cs>
   <da>Hebrew dictionary for hunspell</da>
   <de>Hebräisches Wörterbuch für “Hunspell”</de>
   <el>Εβραϊκό λεξικό για hunspell</el>
   <en>Hebrew dictionary for hunspell</en>
   <es_ES>Diccionario Hebreo para hunspell</es_ES>
   <es>Diccionario Hebreo para hunspell</es>
   <et>Hebrew dictionary for hunspell</et>
   <eu>Hebrew dictionary for hunspell</eu>
   <fa>Hebrew dictionary for hunspell</fa>
   <fil_PH>Hebrew dictionary for hunspell</fil_PH>
   <fi>Hebrew dictionary for hunspell</fi>
   <fr_BE>Hébreu dictionnaire pour hunspell</fr_BE>
   <fr>Hébreu dictionnaire pour hunspell</fr>
   <gl_ES>Hebrew dictionary for hunspell</gl_ES>
   <gu>Hebrew dictionary for hunspell</gu>
   <he_IL>Hebrew dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु हिब्रू शब्दकोष</hi>
   <hr>Hebrew dictionary for hunspell</hr>
   <hu>Hebrew dictionary for hunspell</hu>
   <id>Hebrew dictionary for hunspell</id>
   <is>Hebrew dictionary for hunspell</is>
   <it>Dizionario ebraico per hunspell</it>
   <ja>Hunspell 用ヘブライ語辞書</ja>
   <kk>Hebrew dictionary for hunspell</kk>
   <ko>Hebrew dictionary for hunspell</ko>
   <ku>Hebrew dictionary for hunspell</ku>
   <lt>Hebrew dictionary for hunspell</lt>
   <mk>Hebrew dictionary for hunspell</mk>
   <mr>Hebrew dictionary for hunspell</mr>
   <nb_NO>Hebrew dictionary for hunspell</nb_NO>
   <nb>Hebraisk ordliste for hunspell</nb>
   <nl_BE>Hebrew dictionary for hunspell</nl_BE>
   <nl>Hebrew dictionary for hunspell</nl>
   <or>Hebrew dictionary for hunspell</or>
   <pl>Hebrew dictionary for hunspell</pl>
   <pt_BR>Dicionário Hebraico para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Hebreu para hunspell</pt>
   <ro>Hebrew dictionary for hunspell</ro>
   <ru>Hebrew dictionary for hunspell</ru>
   <sk>Hebrew dictionary for hunspell</sk>
   <sl>Hebrejski slovar za hunspell</sl>
   <so>Hebrew dictionary for hunspell</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në hebraisht</sq>
   <sr>Hebrew dictionary for hunspell</sr>
   <sv>Hebreisk ordbok för hunspell</sv>
   <th>Hebrew dictionary for hunspell</th>
   <tr>Hunspell için İbranice sözlük</tr>
   <uk>Hebrew dictionary for hunspell</uk>
   <vi>Hebrew dictionary for hunspell</vi>
   <zh_CN>Hebrew dictionary for hunspell</zh_CN>
   <zh_HK>Hebrew dictionary for hunspell</zh_HK>
   <zh_TW>Hebrew dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-he
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-he
</uninstall_package_names>

</app>
