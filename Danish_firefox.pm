<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Danish_Firefox
</name>

<description>
   <am>Danish localisation of Firefox</am>
   <ar>Danish localisation of Firefox</ar>
   <be>Danish localisation of Firefox</be>
   <bg>Danish localisation of Firefox</bg>
   <bn>Danish localisation of Firefox</bn>
   <ca>Localització de Firefox en Danès</ca>
   <cs>Danish localisation of Firefox</cs>
   <da>Dansk oversættelse af Firefox</da>
   <de>Dänische Lokalisierung von Firefox</de>
   <el>Δανέζικα για το Firefox</el>
   <en>Danish localisation of Firefox</en>
   <es_ES>Localización Danesa de Firefox</es_ES>
   <es>Localización Danés de Firefox</es>
   <et>Danish localisation of Firefox</et>
   <eu>Danish localisation of Firefox</eu>
   <fa>Danish localisation of Firefox</fa>
   <fil_PH>Danish localisation of Firefox</fil_PH>
   <fi>Tanskalainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en danois pour Firefox</fr_BE>
   <fr>Localisation en danois pour Firefox</fr>
   <gl_ES>Localización de Firefox en danés</gl_ES>
   <gu>Danish localisation of Firefox</gu>
   <he_IL>Danish localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का डेनिश संस्करण</hi>
   <hr>Danish localisation of Firefox</hr>
   <hu>Danish localisation of Firefox</hu>
   <id>Danish localisation of Firefox</id>
   <is>Danish localisation of Firefox</is>
   <it>Localizzazione danese di Firefox</it>
   <ja>Firefox のデンマーク語版</ja>
   <kk>Danish localisation of Firefox</kk>
   <ko>Danish localisation of Firefox</ko>
   <ku>Danish localisation of Firefox</ku>
   <lt>Danish localisation of Firefox</lt>
   <mk>Danish localisation of Firefox</mk>
   <mr>Danish localisation of Firefox</mr>
   <nb_NO>Danish localisation of Firefox</nb_NO>
   <nb>Dansk lokaltilpassing av Firefox</nb>
   <nl_BE>Danish localisation of Firefox</nl_BE>
   <nl>Deense lokalisatie van Firefox</nl>
   <or>Danish localisation of Firefox</or>
   <pl>Duńska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Dinamarquês Localização para o Firefox</pt_BR>
   <pt>Dinamarquês Localização para Firefox</pt>
   <ro>Danish localisation of Firefox</ro>
   <ru>Датская локализация Firefox</ru>
   <sk>Danish localisation of Firefox</sk>
   <sl>Danske krajevne nastavitve za Firefox</sl>
   <so>Danish localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në danisht</sq>
   <sr>Danish localisation of Firefox</sr>
   <sv>Dansk lokalisering av Firefox</sv>
   <th>Danish localisation ของ Firefox</th>
   <tr>Firefox'un Danca yerelleştirmesi</tr>
   <uk>Danish локалізація Firefox</uk>
   <vi>Danish localisation of Firefox</vi>
   <zh_CN>Firefox 丹麦语语言包</zh_CN>
   <zh_HK>Danish localisation of Firefox</zh_HK>
   <zh_TW>Danish localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-da
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-da
</uninstall_package_names>
</app>
