<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Ukrainian_Thunderbird
</name>

<description>
   <am>Ukrainian localisation of Thunderbird</am>
   <ar>Ukrainian localisation of Thunderbird</ar>
   <be>Ukrainian localisation of Thunderbird</be>
   <bg>Ukrainian localisation of Thunderbird</bg>
   <bn>Ukrainian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Ucrainès</ca>
   <cs>Ukrainian localisation of Thunderbird</cs>
   <da>Ukrainsk oversættelse af Thunderbird</da>
   <de>Ukrainische Lokalisierung von Thunderbird</de>
   <el>Ουκρανικά για το Thunderbird</el>
   <en>Ukrainian localisation of Thunderbird</en>
   <es_ES>Localización Ucraniana de Thunderbird</es_ES>
   <es>Localización Ucraniano de Thunderbird</es>
   <et>Ukrainian localisation of Thunderbird</et>
   <eu>Ukrainian localisation of Thunderbird</eu>
   <fa>Ukrainian localisation of Thunderbird</fa>
   <fil_PH>Ukrainian localisation of Thunderbird</fil_PH>
   <fi>Ukrainalainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en ukrainien pour Thunderbird</fr_BE>
   <fr>Localisation en ukrainien pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao ucraniano</gl_ES>
   <gu>Ukrainian localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לאוקראינית</he_IL>
   <hi>थंडरबर्ड का यूक्रेनी संस्करण</hi>
   <hr>Ukrainian localisation of Thunderbird</hr>
   <hu>Ukrainian localisation of Thunderbird</hu>
   <id>Ukrainian localisation of Thunderbird</id>
   <is>Ukrainian localisation of Thunderbird</is>
   <it>Localizzazione ucraina di Thunderbird</it>
   <ja>Thunderbird のウクライナ語版</ja>
   <kk>Ukrainian localisation of Thunderbird</kk>
   <ko>Ukrainian localisation of Thunderbird</ko>
   <ku>Ukrainian localisation of Thunderbird</ku>
   <lt>Ukrainian localisation of Thunderbird</lt>
   <mk>Ukrainian localisation of Thunderbird</mk>
   <mr>Ukrainian localisation of Thunderbird</mr>
   <nb_NO>Ukrainian localisation of Thunderbird</nb_NO>
   <nb>Ukrainsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Ukrainian localisation of Thunderbird</nl_BE>
   <nl>Oekraïense lokalisatie van Thunderbird</nl>
   <or>Ukrainian localisation of Thunderbird</or>
   <pl>Ukraińska lokalizacja Thunderbirda</pl>
   <pt_BR>Ucraniano Localização para o Thunderbird</pt_BR>
   <pt>Ucraniano Localização para Thunderbird</pt>
   <ro>Ukrainian localisation of Thunderbird</ro>
   <ru>Ukrainian localisation of Thunderbird</ru>
   <sk>Ukrainian localisation of Thunderbird</sk>
   <sl>Ukrajinske krajevne nastavitve za Thunderbird</sl>
   <so>Ukrainian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në ukrainisht</sq>
   <sr>Ukrainian localisation of Thunderbird</sr>
   <sv>Ukrainsk lokalisering av Thunderbird</sv>
   <th>Ukrainian localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Ukraynaca yerelleştirmesi</tr>
   <uk>Ukrainian localisation of Thunderbird</uk>
   <vi>Ukrainian localisation of Thunderbird</vi>
   <zh_CN>Ukrainian localisation of Thunderbird</zh_CN>
   <zh_HK>Ukrainian localisation of Thunderbird</zh_HK>
   <zh_TW>Ukrainian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-uk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-uk
</uninstall_package_names>

</app>
