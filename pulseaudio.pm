<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Audio
</category>

<name>
Pulseaudio
</name>

<description>
   <am>Advanced sound server system</am>
   <ar>Advanced sound server system</ar>
   <be>Advanced sound server system</be>
   <bg>Advanced sound server system</bg>
   <bn>Advanced sound server system</bn>
   <ca>Servidor avançat del sistema de so</ca>
   <cs>Advanced sound server system</cs>
   <da>Advanced sound server system</da>
   <de>Erweitertes System zur Audiogeräteverwaltung (engl.: Advanced sound server system)</de>
   <el>Προηγμένο σύστημα διακομιστή ήχου</el>
   <en>Advanced sound server system</en>
   <es_ES>Sistema de servidor de sonido avanzado</es_ES>
   <es>Sistema de servidor de sonido avanzado</es>
   <et>Advanced sound server system</et>
   <eu>Advanced sound server system</eu>
   <fa>Advanced sound server system</fa>
   <fil_PH>Advanced sound server system</fil_PH>
   <fi>Tehokas äänipalvelinjärjestelmä</fi>
   <fr_BE>Système de serveur sonore avancé</fr_BE>
   <fr>Système de serveur sonore avancé</fr>
   <gl_ES>Advanced sound server system</gl_ES>
   <gu>Advanced sound server system</gu>
   <he_IL>Advanced sound server system</he_IL>
   <hi>विस्तृत ध्वनि सर्वर सिस्टम</hi>
   <hr>Advanced sound server system</hr>
   <hu>Advanced sound server system</hu>
   <id>Advanced sound server system</id>
   <is>Advanced sound server system</is>
   <it>Sistema avanzato di server audio</it>
   <ja>高機能なサウンドサーバーシステム</ja>
   <kk>Advanced sound server system</kk>
   <ko>Advanced sound server system</ko>
   <ku>Advanced sound server system</ku>
   <lt>Advanced sound server system</lt>
   <mk>Advanced sound server system</mk>
   <mr>Advanced sound server system</mr>
   <nb_NO>Advanced sound server system</nb_NO>
   <nb>Avansert lydtjener-system</nb>
   <nl_BE>Advanced sound server system</nl_BE>
   <nl>Advanced sound server system</nl>
   <or>Advanced sound server system</or>
   <pl>Advanced sound server system</pl>
   <pt_BR>Sistema de servidor de som avançado</pt_BR>
   <pt>Sistema de servidor de som avançado</pt>
   <ro>Advanced sound server system</ro>
   <ru>Advanced sound server system</ru>
   <sk>Advanced sound server system</sk>
   <sl>Napredni sistem zvočnega strežnika</sl>
   <so>Advanced sound server system</so>
   <sq>Sistem i thelluar shërbyesi tingujsh</sq>
   <sr>Advanced sound server system</sr>
   <sv>Avancerat ljudserver-system</sv>
   <th>Advanced sound server system</th>
   <tr>Gelişmiş ses sunucu sistemi</tr>
   <uk>Advanced sound server system</uk>
   <vi>Advanced sound server system</vi>
   <zh_CN>Advanced sound server system</zh_CN>
   <zh_HK>Advanced sound server system</zh_HK>
   <zh_TW>Advanced sound server system</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
pulseaudio
rtkit
pavucontrol
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
pulseaudio
rtkit
pavucontrol
</uninstall_package_names>
</app>
