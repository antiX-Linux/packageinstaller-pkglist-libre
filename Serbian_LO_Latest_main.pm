<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Serbian_LO_Latest_main
</name>

<description>
   <am>Serbian Language Meta-Package for LibreOffice</am>
   <ar>Serbian Language Meta-Package for LibreOffice</ar>
   <be>Serbian Language Meta-Package for LibreOffice</be>
   <bg>Serbian Language Meta-Package for LibreOffice</bg>
   <bn>Serbian Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Serbi per LibreOffice</ca>
   <cs>Serbian Language Meta-Package for LibreOffice</cs>
   <da>Serbian Language Meta-Package for LibreOffice</da>
   <de>Serbisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Σερβικά</el>
   <en>Serbian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Serbio para LibreOffice</es_ES>
   <es>Metapaquete de idioma Serbio para LibreOffice</es>
   <et>Serbian Language Meta-Package for LibreOffice</et>
   <eu>Serbian Language Meta-Package for LibreOffice</eu>
   <fa>Serbian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Serbian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Serbialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue serbe pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue serbe pour LibreOffice</fr>
   <gl_ES>Serbio Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Serbian Language Meta-Package for LibreOffice</gu>
   <he_IL>Serbian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु सर्बियाई भाषा मेटा-पैकेज</hi>
   <hr>Serbian Language Meta-Package for LibreOffice</hr>
   <hu>Serbian Language Meta-Package for LibreOffice</hu>
   <id>Serbian Language Meta-Package for LibreOffice</id>
   <is>Serbian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua serba per LibreOffice</it>
   <ja>LibreOffice用のセルビア語メタパッケージ</ja>
   <kk>Serbian Language Meta-Package for LibreOffice</kk>
   <ko>Serbian Language Meta-Package for LibreOffice</ko>
   <ku>Serbian Language Meta-Package for LibreOffice</ku>
   <lt>Serbian Language Meta-Package for LibreOffice</lt>
   <mk>Serbian Language Meta-Package for LibreOffice</mk>
   <mr>Serbian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Serbian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Serbisk språkpakke for LibreOffice</nb>
   <nl_BE>Serbian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Servisch Taal Meta-Pakket voor LibreOffice</nl>
   <or>Serbian Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka serbskiego dla LibreOffice</pl>
   <pt_BR>Sérvio Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Sérvio Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Serbian Language Meta-Package for LibreOffice</ro>
   <ru>Serbian Language Meta-Package for LibreOffice</ru>
   <sk>Serbian Language Meta-Package for LibreOffice</sk>
   <sl>Srbski jezikovni meta-paket za LibreOffice</sl>
   <so>Serbian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në serbisht</sq>
   <sr>Serbian Language Meta-Package for LibreOffice</sr>
   <sv>Serbiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Serbian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Sırpça Dili Üst-Paketi</tr>
   <uk>Serbian Language Meta-Package for LibreOffice</uk>
   <vi>Serbian Language Meta-Package for LibreOffice</vi>
   <zh_CN>Serbian Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Serbian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Serbian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-sr
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-sr
libreoffice-gtk3
</uninstall_package_names>

</app>
