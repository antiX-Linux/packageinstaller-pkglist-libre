<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
ft10-transformation
</name>

<description>
   <en>Modern looking toolbar and menu with tint2 and jgmenu for antiX</en>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
ft10-transformation
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
ft10-transformation
</uninstall_package_names>
</app>
