<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Czech_Thunderbird
</name>

<description>
   <am>Czech localisation of Thunderbird</am>
   <ar>Czech localisation of Thunderbird</ar>
   <be>Czech localisation of Thunderbird</be>
   <bg>Czech localisation of Thunderbird</bg>
   <bn>Czech localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Txec</ca>
   <cs>Czech localisation of Thunderbird</cs>
   <da>Tjekkisk oversættelse af Thunderbird</da>
   <de>Tschechische Lokalisierung von Thunderbird</de>
   <el>Τσέχικα για το Thunderbird</el>
   <en>Czech localisation of Thunderbird</en>
   <es_ES>Localización Checa de Thunderbird</es_ES>
   <es>Localización Checo de Thunderbird</es>
   <et>Czech localisation of Thunderbird</et>
   <eu>Czech localisation of Thunderbird</eu>
   <fa>Czech localisation of Thunderbird</fa>
   <fil_PH>Czech localisation of Thunderbird</fil_PH>
   <fi>Tsekkiläinen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation tchèque pour Thunderbird</fr_BE>
   <fr>Localisation tchèque pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird para checo</gl_ES>
   <gu>Czech localisation of Thunderbird</gu>
   <he_IL>Czech localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का चेक संस्करण</hi>
   <hr>Czech localisation of Thunderbird</hr>
   <hu>Czech localisation of Thunderbird</hu>
   <id>Czech localisation of Thunderbird</id>
   <is>Czech localisation of Thunderbird</is>
   <it>Localizzazione in ceco di Thunderbird</it>
   <ja>Thunderbird のチェコ語版</ja>
   <kk>Czech localisation of Thunderbird</kk>
   <ko>Czech localisation of Thunderbird</ko>
   <ku>Czech localisation of Thunderbird</ku>
   <lt>Czech localisation of Thunderbird</lt>
   <mk>Czech localisation of Thunderbird</mk>
   <mr>Czech localisation of Thunderbird</mr>
   <nb_NO>Czech localisation of Thunderbird</nb_NO>
   <nb>Tsjekkisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Czech localisation of Thunderbird</nl_BE>
   <nl>Tjechische lokalisatie van Thunderbird</nl>
   <or>Czech localisation of Thunderbird</or>
   <pl>Czeska lokalizacja Thunderbirda</pl>
   <pt_BR>Checo Localização para o Thunderbird</pt_BR>
   <pt>Checo Localização para Thunderbird</pt>
   <ro>Czech localisation of Thunderbird</ro>
   <ru>Czech localisation of Thunderbird</ru>
   <sk>Czech localisation of Thunderbird</sk>
   <sl>Czech localisation of Thunderbird</sl>
   <so>Czech localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në çekisht</sq>
   <sr>Czech localisation of Thunderbird</sr>
   <sv>Tjeckisk lokalisering av Thunderbird</sv>
   <th>Czech localisation ของ Thunderbird</th>
   <tr>Thunderbird'in Çekce yerelleştirmesi</tr>
   <uk>Czech локалізація Thunderbird</uk>
   <vi>Czech localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 捷克语语言包</zh_CN>
   <zh_HK>Czech localisation of Thunderbird</zh_HK>
   <zh_TW>Czech localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-cs
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-cs
</uninstall_package_names>

</app>
