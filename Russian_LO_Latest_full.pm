<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Russian_LO_Latest_full
</name>

<description>
   <am>Russian localisation of LibreOffice</am>
   <ar>Russian localisation of LibreOffice</ar>
   <be>Russian localisation of LibreOffice</be>
   <bg>Russian localisation of LibreOffice</bg>
   <bn>Russian localisation of LibreOffice</bn>
   <ca>Localització de LibreOffice en Rus</ca>
   <cs>Russian localisation of LibreOffice</cs>
   <da>Russisk oversættelse af LibreOffice</da>
   <de>Russische Lokalisierung von LibreOffice</de>
   <el>LibreOffice στα Ρωσικά</el>
   <en>Russian localisation of LibreOffice</en>
   <es_ES>Localización Rusa de LibreOffice</es_ES>
   <es>Localización Ruso de LibreOffice</es>
   <et>Russian localisation of LibreOffice</et>
   <eu>Russian localisation of LibreOffice</eu>
   <fa>Russian localisation of LibreOffice</fa>
   <fil_PH>Russian localisation of LibreOffice</fil_PH>
   <fi>Venäläinen kotoistus LibreOffice:lle</fi>
   <fr_BE>Localisation en russe pour LibreOffice</fr_BE>
   <fr>Localisation en russe pour LibreOffice</fr>
   <gl_ES>Localización do LibreOffice ao ruso</gl_ES>
   <gu>Russian localisation of LibreOffice</gu>
   <he_IL>תרגומי LibreOffice לרוסית</he_IL>
   <hi>लिब्रे-ऑफिस का रूसी संस्करण</hi>
   <hr>Russian localisation of LibreOffice</hr>
   <hu>Russian localisation of LibreOffice</hu>
   <id>Russian localisation of LibreOffice</id>
   <is>Russian localisation of LibreOffice</is>
   <it>Localizzazione russa di LibreOffice</it>
   <ja>LibreOffice のロシア語版</ja>
   <kk>Russian localisation of LibreOffice</kk>
   <ko>Russian localisation of LibreOffice</ko>
   <ku>Russian localisation of LibreOffice</ku>
   <lt>Russian localisation of LibreOffice</lt>
   <mk>Russian localisation of LibreOffice</mk>
   <mr>Russian localisation of LibreOffice</mr>
   <nb_NO>Russian localisation of LibreOffice</nb_NO>
   <nb>Russisk lokaltilpassing av LibreOffice</nb>
   <nl_BE>Russian localisation of LibreOffice</nl_BE>
   <nl>Russische lokalisatie van LibreOffice</nl>
   <or>Russian localisation of LibreOffice</or>
   <pl>Rosyjska lokalizacja LibreOffice</pl>
   <pt_BR>Russo Localização para o LibreOffice</pt_BR>
   <pt>Russo Localização para LibreOffice</pt>
   <ro>Russian localisation of LibreOffice</ro>
   <ru>Русский перевод для LibreOffice</ru>
   <sk>Russian localisation of LibreOffice</sk>
   <sl>Ruske krajevne nastavitve za LibreOffice</sl>
   <so>Russian localisation of LibreOffice</so>
   <sq>Përkthimi në rusisht i LibreOffice-it</sq>
   <sr>Russian localisation of LibreOffice</sr>
   <sv>Rysk  lokalisering av LibreOffice</sv>
   <th>Russian localisation ของ LibreOffice</th>
   <tr>LibreOffice Rusça yerelleştirmesi</tr>
   <uk>Russian локалізація LibreOffice</uk>
   <vi>Russian localisation of LibreOffice</vi>
   <zh_CN>Russian localisation of LibreOffice</zh_CN>
   <zh_HK>Russian localisation of LibreOffice</zh_HK>
   <zh_TW>Russian localisation of LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-ru
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-ru
</uninstall_package_names>

</app>
