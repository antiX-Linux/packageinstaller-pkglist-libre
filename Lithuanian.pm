<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Lithuanian
</name>

<description>
   <am>Lithuanian dictionary for hunspell</am>
   <ar>Lithuanian dictionary for hunspell</ar>
   <be>Lithuanian dictionary for hunspell</be>
   <bg>Lithuanian dictionary for hunspell</bg>
   <bn>Lithuanian dictionary for hunspell</bn>
   <ca>Diccionari Lituà per hunspell</ca>
   <cs>Lithuanian dictionary for hunspell</cs>
   <da>Lithuanian dictionary for hunspell</da>
   <de>Litauisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό στα λιθουανικά για hunspell</el>
   <en>Lithuanian dictionary for hunspell</en>
   <es_ES>Diccionario Lituano para hunspell</es_ES>
   <es>Diccionario Lituano para hunspell</es>
   <et>Lithuanian dictionary for hunspell</et>
   <eu>Lithuanian dictionary for hunspell</eu>
   <fa>Lithuanian dictionary for hunspell</fa>
   <fil_PH>Lithuanian dictionary for hunspell</fil_PH>
   <fi>Lithuanian dictionary for hunspell</fi>
   <fr_BE>Lituanien dictionnaire pour hunspell</fr_BE>
   <fr>Lituanien dictionnaire pour hunspell</fr>
   <gl_ES>Lithuanian dictionary for hunspell</gl_ES>
   <gu>Lithuanian dictionary for hunspell</gu>
   <he_IL>Lithuanian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु लिथुआनियाई शब्दकोष</hi>
   <hr>Lithuanian dictionary for hunspell</hr>
   <hu>Lithuanian dictionary for hunspell</hu>
   <id>Lithuanian dictionary for hunspell</id>
   <is>Lithuanian dictionary for hunspell</is>
   <it>Dizionario lithuanian per hunspell</it>
   <ja>Hunspell 用リトアニア語辞書</ja>
   <kk>Lithuanian dictionary for hunspell</kk>
   <ko>Lithuanian dictionary for hunspell</ko>
   <ku>Lithuanian dictionary for hunspell</ku>
   <lt>Lithuanian dictionary for hunspell</lt>
   <mk>Lithuanian dictionary for hunspell</mk>
   <mr>Lithuanian dictionary for hunspell</mr>
   <nb_NO>Lithuanian dictionary for hunspell</nb_NO>
   <nb>Litauisk ordliste for hunspell</nb>
   <nl_BE>Lithuanian dictionary for hunspell</nl_BE>
   <nl>Lithuanian dictionary for hunspell</nl>
   <or>Lithuanian dictionary for hunspell</or>
   <pl>Lithuanian dictionary for hunspell</pl>
   <pt_BR>Dicionário Lituano para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Lituano para hunspell</pt>
   <ro>Lithuanian dictionary for hunspell</ro>
   <ru>Lithuanian dictionary for hunspell</ru>
   <sk>Lithuanian dictionary for hunspell</sk>
   <sl>Litvanski slovar za hunspell</sl>
   <so>Lithuanian dictionary for hunspell</so>
   <sq>Fjalor lituanisht për hunspell</sq>
   <sr>Lithuanian dictionary for hunspell</sr>
   <sv>Litauisk ordbok för hunspell</sv>
   <th>Lithuanian dictionary for hunspell</th>
   <tr>Hunspell için Litvanyaca sözlük</tr>
   <uk>Lithuanian dictionary for hunspell</uk>
   <vi>Lithuanian dictionary for hunspell</vi>
   <zh_CN>Lithuanian dictionary for hunspell</zh_CN>
   <zh_HK>Lithuanian dictionary for hunspell</zh_HK>
   <zh_TW>Lithuanian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-lt
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-lt
</uninstall_package_names>

</app>
