<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hebrew_LO_Latest_main
</name>

<description>
   <am>Hebrew Language Meta-Package for LibreOffice</am>
   <ar>Hebrew Language Meta-Package for LibreOffice</ar>
   <be>Hebrew Language Meta-Package for LibreOffice</be>
   <bg>Hebrew Language Meta-Package for LibreOffice</bg>
   <bn>Hebrew Language Meta-Package for LibreOffice</bn>
   <ca>Metpaquet d'idioma Hebreu per LibreOffice</ca>
   <cs>Hebrew Language Meta-Package for LibreOffice</cs>
   <da>Hebrew Language Meta-Package for LibreOffice</da>
   <de>Hebräisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Εβραϊκά</el>
   <en>Hebrew Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Hebreo para LibreOffice</es_ES>
   <es>Metapaquete de idioma Hebreo para LibreOffice</es>
   <et>Hebrew Language Meta-Package for LibreOffice</et>
   <eu>Hebrew Language Meta-Package for LibreOffice</eu>
   <fa>Hebrew Language Meta-Package for LibreOffice</fa>
   <fil_PH>Hebrew Language Meta-Package for LibreOffice</fil_PH>
   <fi>Hepreankielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue hébraïque pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue hébraïque pour LibreOffice</fr>
   <gl_ES>Hebreo Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Hebrew Language Meta-Package for LibreOffice</gu>
   <he_IL>Hebrew Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु हिब्रू भाषा मेटा-पैकेज</hi>
   <hr>Hebrew Language Meta-Package for LibreOffice</hr>
   <hu>Hebrew Language Meta-Package for LibreOffice</hu>
   <id>Hebrew Language Meta-Package for LibreOffice</id>
   <is>Hebrew Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua ebraica per LibreOffice</it>
   <ja>LibreOffice用のヘブライ語メタパッケージ</ja>
   <kk>Hebrew Language Meta-Package for LibreOffice</kk>
   <ko>Hebrew Language Meta-Package for LibreOffice</ko>
   <ku>Hebrew Language Meta-Package for LibreOffice</ku>
   <lt>Hebrew Language Meta-Package for LibreOffice</lt>
   <mk>Hebrew Language Meta-Package for LibreOffice</mk>
   <mr>Hebrew Language Meta-Package for LibreOffice</mr>
   <nb_NO>Hebrew Language Meta-Package for LibreOffice</nb_NO>
   <nb>Hebraisk språkpakke for LibreOffice</nb>
   <nl_BE>Hebrew Language Meta-Package for LibreOffice</nl_BE>
   <nl>Hebreews Taal Meta-Pakket voor LibreOffice</nl>
   <or>Hebrew Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka hebrajskiego dla LibreOffice</pl>
   <pt_BR>Hebraico Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Hebreu Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Hebrew Language Meta-Package for LibreOffice</ro>
   <ru>Hebrew Language Meta-Package for LibreOffice</ru>
   <sk>Hebrew Language Meta-Package for LibreOffice</sk>
   <sl>Hebrejski jezikovni meta-paket za LibreOffice</sl>
   <so>Hebrew Language Meta-Package for LibreOffice</so>
   <sq>Përkthimi i Firefox-it ESR në hebraisht</sq>
   <sr>Hebrew Language Meta-Package for LibreOffice</sr>
   <sv>Hebreiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Hebrew สำหรับ LibreOffice</th>
   <tr>LibreOffice için İbranice Dili Üst-Paketi</tr>
   <uk>Hebrew Language Meta-Package for LibreOffice</uk>
   <vi>Hebrew Language Meta-Package for LibreOffice</vi>
   <zh_CN>LibreOffice 希伯来语语言包</zh_CN>
   <zh_HK>Hebrew Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Hebrew Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-he
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-he
libreoffice-gtk3
</uninstall_package_names>

</app>
