<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hungarian_LO_Latest_main
</name>

<description>
   <am>Hungarian LibreOffice Language Meta-Package</am>
   <ar>Hungarian LibreOffice Language Meta-Package</ar>
   <be>Hungarian LibreOffice Language Meta-Package</be>
   <bg>Hungarian LibreOffice Language Meta-Package</bg>
   <bn>Hungarian LibreOffice Language Meta-Package</bn>
   <ca>Meta-paquet per LibreOffice en Hongarès</ca>
   <cs>Hungarian LibreOffice Language Meta-Package</cs>
   <da>Ungarsk LibreOffice sprog-metapakke</da>
   <de>Ungarisches LibreOffice Meta-Paket</de>
   <el>LibreOffice στα Ουγγρικά</el>
   <en>Hungarian LibreOffice Language Meta-Package</en>
   <es_ES>Meta-Paquete de Idioma Húngaro LibreOffice</es_ES>
   <es>Metapaquete de Idioma Húngaro LibreOffice</es>
   <et>Hungarian LibreOffice Language Meta-Package</et>
   <eu>Hungarian LibreOffice Language Meta-Package</eu>
   <fa>Hungarian LibreOffice Language Meta-Package</fa>
   <fil_PH>Hungarian LibreOffice Language Meta-Package</fil_PH>
   <fi>Unkarilainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue hongroise pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue hongroise pour LibreOffice</fr>
   <gl_ES>Húngaro Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Hungarian LibreOffice Language Meta-Package</gu>
   <he_IL>Hungarian LibreOffice Language Meta-Package</he_IL>
   <hi>लिब्रे-ऑफिस हेतु हंगेरियाई भाषा मेटा-पैकेज</hi>
   <hr>Hungarian LibreOffice Language Meta-Package</hr>
   <hu>Hungarian LibreOffice Language Meta-Package</hu>
   <id>Hungarian LibreOffice Language Meta-Package</id>
   <is>Hungarian LibreOffice Language Meta-Package</is>
   <it>Meta-pacchetto della lingua ungherese per LibreOffice</it>
   <ja>ハンガリー語 LibreOffice 言語メタパッケージ</ja>
   <kk>Hungarian LibreOffice Language Meta-Package</kk>
   <ko>Hungarian LibreOffice Language Meta-Package</ko>
   <ku>Hungarian LibreOffice Language Meta-Package</ku>
   <lt>Hungarian LibreOffice Language Meta-Package</lt>
   <mk>Hungarian LibreOffice Language Meta-Package</mk>
   <mr>Hungarian LibreOffice Language Meta-Package</mr>
   <nb_NO>Hungarian LibreOffice Language Meta-Package</nb_NO>
   <nb>Ungarsk språkpakke for LibreOffice</nb>
   <nl_BE>Hungarian LibreOffice Language Meta-Package</nl_BE>
   <nl>Hongaarse LibreOffice Taal Meta-Pakket</nl>
   <or>Hungarian LibreOffice Language Meta-Package</or>
   <pl>Węgierski metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Húngaro Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Húngaro Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Hungarian LibreOffice Language Meta-Package</ro>
   <ru>Hungarian LibreOffice Language Meta-Package</ru>
   <sk>Hungarian LibreOffice Language Meta-Package</sk>
   <sl>Madžarski jezikovni metapaket za LibreOffice</sl>
   <so>Hungarian LibreOffice Language Meta-Package</so>
   <sq>Përkthimi i Firefox-it në hungarisht</sq>
   <sr>Hungarian LibreOffice Language Meta-Package</sr>
   <sv>Ungerskt LibreOffice Språk Meta-Paket </sv>
   <th>Meta-Package ภาษา Hungarian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Macarca Dili Üst-Paketi</tr>
   <uk>Hungarian LibreOffice Language Meta-Package</uk>
   <vi>Hungarian LibreOffice Language Meta-Package</vi>
   <zh_CN>LibreOffice 匈牙利语元语言包</zh_CN>
   <zh_HK>Hungarian LibreOffice Language Meta-Package</zh_HK>
   <zh_TW>Hungarian LibreOffice Language Meta-Package</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-hu
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-hu
libreoffice-gtk3
</uninstall_package_names>

</app>
