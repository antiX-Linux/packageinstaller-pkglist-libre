<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Children
</category>

<name>
Secondary
</name>

<description>
   <am>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</am>
   <ar>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</ar>
   <be>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</be>
   <bg>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</bg>
   <bn>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</bn>
   <ca>Secundària: inclou calibre, celestia, dia, laby, lightspeed, melting, ri-li i stellarium</ca>
   <cs>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</cs>
   <da>Videregående skole. Inkluderer: calibre, celestia, dia, laby, lightspeed,  melting, ri-li og stellarium</da>
   <de>Oberschule: Beinhaltet: Kaliber, Celestia, Dia, Laby, Lightspeed, Schmelzen, Ri-li und Stellarium.</de>
   <el>Δευτεροβάθμια. Περιλαμβάνει: calibre, celestia, dia, laby, lightpeed, melting, ri-li και stellarium</el>
   <en>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</en>
   <es_ES>Secundario. incluye: alibre, celestia, dia, laby, lightspeed, melting, ri-li y stellarium</es_ES>
   <es>Secondary incluye: alibre, celestia, dia, laby, lightspeed, melting, ri-li y stellarium</es>
   <et>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</et>
   <eu>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</eu>
   <fa>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</fa>
   <fil_PH>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</fil_PH>
   <fi>Tokaluokka. Sisältää ohjelmat: calibre, celestia, dia, laby, lightspeed, melting, ri-li ja stellarium</fi>
   <fr_BE>Secondaire. Inclus: calibre, celestia, dia, laby, lightspeed, melting, ri-li et stellarium</fr_BE>
   <fr>Secondaire. Inclus: calibre, celestia, dia, laby, lightspeed, melting, ri-li et stellarium</fr>
   <gl_ES>Ensino básico 3º ciclo. Inclúe: calibre, celestia, dia, laby, lightspeed, melting, ri-li e stellarium</gl_ES>
   <gu>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</gu>
   <he_IL>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</he_IL>
   <hi>माध्यमिक विद्यालय। सम्मिलित : calibre, celestia, dia, laby, lightspeed, melting, ri-li व stellarium</hi>
   <hr>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</hr>
   <hu>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</hu>
   <id>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</id>
   <is>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</is>
   <it>Superiori. Include: calibre, celestia, dia, laby, lightspeed, melting, ri-li e stellarium</it>
   <ja>セカンダリ。以下を含みます： calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</ja>
   <kk>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</kk>
   <ko>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</ko>
   <ku>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</ku>
   <lt>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</lt>
   <mk>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</mk>
   <mr>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</mr>
   <nb_NO>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</nb_NO>
   <nb>Ungdomsskole. Inkluderer calibre, celestia, dia, laby, lightspeed, melting, ri-li og stellarium</nb>
   <nl_BE>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</nl_BE>
   <nl>Gevorderd. Inclusief: calibre, celestia, dia, laby, lightspeed, melting, ri-li  en stellarium</nl>
   <or>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</or>
   <pl>Szkoła średnia: Zawiera: calibre, celestia, dia, laby, lightspeed, melting, ri-li i stellarium</pl>
   <pt_BR>Ensino secundário. Inclui: calibre (gerenciador de e-books), celestia (simulação do espaço), dia (editor de diagramas), laby (aprender a programar), lightspeed (objeto a velocidade da luz), melting (calcular a temperatura de fusão), ri-li (jogo de quebra-cabeça do trem de brinquedo) e stellarium (planetário)</pt_BR>
   <pt>Ensino básico 3º ciclo. Inclui: calibre, celestia, dia, laby, lightspeed, melting, ri-li e stellarium</pt>
   <ro>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</ro>
   <ru>Средняя школа, включает: calibre, celestia, dia, laby, lightspeed, melting, ri-li и stellarium</ru>
   <sk>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</sk>
   <sl>Srednja šola. Vsebuje: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</sl>
   <so>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</so>
   <sq>Cikli i dytë. Përfshin: calibre, celestia, dia, laby, lightspeed, melting, ri-li dhe stellarium</sq>
   <sr>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</sr>
   <sv>Secondary. Inkluderar: calibre, celestia, dia, laby, lightspeed, melting, ri-li och stellarium</sv>
   <th>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</th>
   <tr>Ortaokul. İçeriği: calibre, celestia, dia, laby, lightspeed, melting, ri-li ve stellarium</tr>
   <uk>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</uk>
   <vi>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</vi>
   <zh_CN>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</zh_CN>
   <zh_HK>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</zh_HK>
   <zh_TW>Secondary. Includes: calibre, celestia, dia, laby, lightspeed, melting, ri-li and stellarium</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
calibre
laby
lightspeed
melting
ri-li
stellarium
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
calibre
laby
lightspeed
melting
ri-li
stellarium
</uninstall_package_names>
</app>
