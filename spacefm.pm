<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
File Managers
</category>

<name>
SpaceFM
</name>

<description>
   <am>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</am>
   <ar>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</ar>
   <be>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</be>
   <bg>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</bg>
   <bn>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</bn>
   <ca>Gestor d'escriptori i fitxers multi-panell amb pestanyes estable, ràpid, flexible i pràctic</ca>
   <cs>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</cs>
   <da>en fil- og skrivebordshåndtering med flere paneler og faneblade som tilbyder stabilitet, bekvemmelighed og fleksibilitet</da>
   <de>Eine Multi-Panel-Tabbed-Datei und Desktop-Manager bietet Stabilität, Geschwindigkeit, Komfort und Flexibilität</de>
   <el>Διαχειριστής καρτελών, πολλαπλών πινάκων και επιφάνειας εργασίας που προσφέρει σταθερότητα, ταχύτητα, ευκολία και ευελιξία</el>
   <en>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</en>
   <es_ES>Administrador de archivos y del escritorio multi panel con soporte de pestañas que ofrece estabilidad, velocidad, conveniencia y flexibilidad.</es_ES>
   <es>gestor de archivos y del escritorio multi panel con soporte de pestañas que ofrece estabilidad, velocidad, conveniencia y flexibilidad.</es>
   <et>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</et>
   <eu>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</eu>
   <fa>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</fa>
   <fil_PH>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</fil_PH>
   <fi>monipaneelinen välilehdellinen tiedostojen ja työpöydän hallintasovellus joka tarjoaa vakautta, on nopea, mukava käyttää sekä joustava</fi>
   <fr_BE>Un gestionnaire de bureau et de fichiers à plusieurs panneaux, procurant stabilité, rapidité, facilité et souplesse</fr_BE>
   <fr>Un gestionnaire de bureau et de fichiers à plusieurs panneaux, procurant stabilité, rapidité, facilité et souplesse</fr>
   <gl_ES>Xestor de ficheiros e do escritorio multi-panel con lapelas, que proporciona estabilidade, rapidez, comodidade e flexibilidade</gl_ES>
   <gu>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</gu>
   <he_IL>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</he_IL>
   <hi>एकाधिक-पैनल व टैब युक्त फाइल व डेस्कटॉप प्रबंधक जिसका उद्देश्य है स्थिरता, तीव्रता, सुविधा एवं सुगम्यता</hi>
   <hr>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</hr>
   <hu>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</hu>
   <id>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</id>
   <is>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</is>
   <it>gestore di file e desktop a più riquadri con schede che offre stabilità, velocità, convenienza e flessibilità.</it>
   <ja>安定性やスピード、利便性、柔軟性を提供するマルチパネル・タブ付きのファイル兼デスクトップマネージャー</ja>
   <kk>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</kk>
   <ko>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</ko>
   <ku>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</ku>
   <lt>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</lt>
   <mk>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</mk>
   <mr>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</mr>
   <nb_NO>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</nb_NO>
   <nb>fil- og skrivebordsbehandler med flere faner, som er stabil, rask, praktisk og fleksibel</nb>
   <nl_BE>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</nl_BE>
   <nl>een bestand en desktop manager met meerdere tabbladen voor stabiliteit, snelheid, gemak en flexibiliteit</nl>
   <or>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</or>
   <pl>wielopanelowy menedżer plików i pulpitu oferujący stabilność, szybkość, wygodę i elastyczność</pl>
   <pt_BR>Gerenciador de arquivos da área de trabalho multipainel com abas/guias que proporcionam estabilidade, rapidez, comodidade e flexibilidade</pt_BR>
   <pt>Gestor de ficheiros e da área de trabalho multi-painel com separadores, que proporciona estabilidade, rapidez, comodidade e flexibilidade</pt>
   <ro>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</ro>
   <ru>Мультипанельный менеджер файлов с поддержкой вкладок и окружением рабочего стола</ru>
   <sk>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</sk>
   <sl>Upravljalnik datotek in namizja z več panoji, ki ponuja stabilnost, hitrost, fleksibilnost in udobje</sl>
   <so>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</so>
   <sq>Një përgjegjës kartelash dhe desktopi me shumë panele, në skeda, që ofron qëndrueshmëri, shpejtësi, voli dhe zhdërvjelltësi</sq>
   <sr>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</sr>
   <sv>en multi-panels fil och skrivbordshanterare med flikar som erbjuder stabilitet, snabbhet, bekvämlighet och flexibilitet</sv>
   <th>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</th>
   <tr>kararlılık, hız, kolaylık ve esneklik sunan çoklu panel sekmeli bir dosya ve masaüstü yöneticisi</tr>
   <uk>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</uk>
   <vi>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</vi>
   <zh_CN>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</zh_CN>
   <zh_HK>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</zh_HK>
   <zh_TW>a multi-panel tabbed file and desktop manager offering stability, speed, convenience and flexibility</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
spacefm
udevil
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
spacefm
</uninstall_package_names>
</app>
