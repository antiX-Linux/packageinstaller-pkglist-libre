<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Korean_Thunderbird
</name>

<description>
   <am>Korean localisation of Thunderbird</am>
   <ar>Korean localisation of Thunderbird</ar>
   <be>Korean localisation of Thunderbird</be>
   <bg>Korean localisation of Thunderbird</bg>
   <bn>Korean localisation of Thunderbird</bn>
   <ca>Localització en Coreà per Thunderbird</ca>
   <cs>Korean localisation of Thunderbird</cs>
   <da>Koreansk oversættelse af Thunderbird</da>
   <de>Koreanische Lokalisierung von Thunderbird</de>
   <el>Κορεατικά για το Thunderbird</el>
   <en>Korean localisation of Thunderbird</en>
   <es_ES>Localización Coreana de Thunderbird</es_ES>
   <es>Localización Coreano de Thunderbird</es>
   <et>Korean localisation of Thunderbird</et>
   <eu>Korean localisation of Thunderbird</eu>
   <fa>Korean localisation of Thunderbird</fa>
   <fil_PH>Korean localisation of Thunderbird</fil_PH>
   <fi>Korealainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en coréen pour Thunderbird</fr_BE>
   <fr>Localisation en coréen pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao corean</gl_ES>
   <gu>Korean localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לקוריאנית</he_IL>
   <hi>थंडरबर्ड का कोरियाई संस्करण</hi>
   <hr>Korean localisation of Thunderbird</hr>
   <hu>Korean localisation of Thunderbird</hu>
   <id>Korean localisation of Thunderbird</id>
   <is>Korean localisation of Thunderbird</is>
   <it>Localizzazione coreana di Thunderbird</it>
   <ja>Thunderbird の韓国（朝鮮）語版</ja>
   <kk>Korean localisation of Thunderbird</kk>
   <ko>Korean localisation of Thunderbird</ko>
   <ku>Korean localisation of Thunderbird</ku>
   <lt>Korean localisation of Thunderbird</lt>
   <mk>Korean localisation of Thunderbird</mk>
   <mr>Korean localisation of Thunderbird</mr>
   <nb_NO>Korean localisation of Thunderbird</nb_NO>
   <nb>Koreansk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Korean localisation of Thunderbird</nl_BE>
   <nl>Koreaanse lokalisatie van Thunderbird</nl>
   <or>Korean localisation of Thunderbird</or>
   <pl>Koreańska lokalizacja Thunderbirda</pl>
   <pt_BR>Coreano Localização para o Thunderbird</pt_BR>
   <pt>Coreano Localização para Thunderbird</pt>
   <ro>Korean localisation of Thunderbird</ro>
   <ru>Korean localisation of Thunderbird</ru>
   <sk>Korean localisation of Thunderbird</sk>
   <sl>Korejske krajevne nastavitve za Thunderbird</sl>
   <so>Korean localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në koreançe</sq>
   <sr>Korean localisation of Thunderbird</sr>
   <sv>Koreansk lokalisering av Thunderbird</sv>
   <th>Korean localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Korece yerelleştirmesi</tr>
   <uk>Korean локалізація Thunderbird</uk>
   <vi>Korean localisation of Thunderbird</vi>
   <zh_CN>Korean localisation of Thunderbird</zh_CN>
   <zh_HK>Korean localisation of Thunderbird</zh_HK>
   <zh_TW>Korean localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-ko
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-ko
</uninstall_package_names>

</app>
