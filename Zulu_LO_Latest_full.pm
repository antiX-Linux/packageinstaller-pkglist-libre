<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Zulu_LO_Latest_full
</name>

<description>
   <am>Zulu Language Meta-Package for LibreOffice</am>
   <ar>Zulu Language Meta-Package for LibreOffice</ar>
   <be>Zulu Language Meta-Package for LibreOffice</be>
   <bg>Zulu Language Meta-Package for LibreOffice</bg>
   <bn>Zulu Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Zulu per LibreOffice</ca>
   <cs>Zulu Language Meta-Package for LibreOffice</cs>
   <da>Zulu Language Meta-Package for LibreOffice</da>
   <de>Zulu Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Zulu</el>
   <en>Zulu Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Zulú para LibreOffice</es_ES>
   <es>Metapaquete de idioma Zulú para LibreOffice</es>
   <et>Zulu Language Meta-Package for LibreOffice</et>
   <eu>Zulu Language Meta-Package for LibreOffice</eu>
   <fa>Zulu Language Meta-Package for LibreOffice</fa>
   <fil_PH>Zulu Language Meta-Package for LibreOffice</fil_PH>
   <fi>Zulunkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Meta-Paquet de langue Zoulou pour LibreOffice</fr_BE>
   <fr>Meta-Paquet de langue Zoulou pour LibreOffice</fr>
   <gl_ES>Zulú Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Zulu Language Meta-Package for LibreOffice</gu>
   <he_IL>Zulu Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु ज़ुलु भाषा मेटा-पैकेज</hi>
   <hr>Zulu Language Meta-Package for LibreOffice</hr>
   <hu>Zulu Language Meta-Package for LibreOffice</hu>
   <id>Zulu Language Meta-Package for LibreOffice</id>
   <is>Zulu Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua zulu per LibreOffice</it>
   <ja>LibreOffice用 ズールー語メタパッケージ</ja>
   <kk>Zulu Language Meta-Package for LibreOffice</kk>
   <ko>Zulu Language Meta-Package for LibreOffice</ko>
   <ku>Zulu Language Meta-Package for LibreOffice</ku>
   <lt>Zulu Language Meta-Package for LibreOffice</lt>
   <mk>Zulu Language Meta-Package for LibreOffice</mk>
   <mr>Zulu Language Meta-Package for LibreOffice</mr>
   <nb_NO>Zulu Language Meta-Package for LibreOffice</nb_NO>
   <nb>Zulu språkpakke for LibreOffice</nb>
   <nl_BE>Zulu Language Meta-Package for LibreOffice</nl_BE>
   <nl>Zulu Taal Meta-Pakket voor LibreOffice</nl>
   <or>Zulu Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Zulu dla LibreOffice</pl>
   <pt_BR>Zulu Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Zulu Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Zulu Language Meta-Package for LibreOffice</ro>
   <ru>Zulu Language Meta-Package for LibreOffice</ru>
   <sk>Zulu Language Meta-Package for LibreOffice</sk>
   <sl>Zulujski jezikovni meta-paket za LibreOffice</sl>
   <so>Zulu Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në zulu</sq>
   <sr>Zulu Language Meta-Package for LibreOffice</sr>
   <sv>Zulu Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Zulu สำหรับ LibreOffice</th>
   <tr>LibreOffice için Zulu Dili Üst-Paketi</tr>
   <uk>Zulu Language Meta-Package for LibreOffice</uk>
   <vi>Zulu Language Meta-Package for LibreOffice</vi>
   <zh_CN>Zulu Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Zulu Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Zulu Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-zu
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-zu
</uninstall_package_names>

</app>
