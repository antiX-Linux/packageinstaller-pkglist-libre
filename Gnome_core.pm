<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
Gnome-core
</name>

<description>
   <am>Very minimal install of Gnome</am>
   <ar>Very minimal install of Gnome</ar>
   <be>Very minimal install of Gnome</be>
   <bg>Very minimal install of Gnome</bg>
   <bn>Very minimal install of Gnome</bn>
   <ca>Instal·lació molt mínima de Gnome</ca>
   <cs>Very minimal install of Gnome</cs>
   <da>Very minimal install of Gnome</da>
   <de>Besonders minimalistische Installation der Desktop-Umgebung “Gnome”</de>
   <el>Πολύ ελάχιστη εγκατάσταση του Gnome</el>
   <en>Very minimal install of Gnome</en>
   <es_ES>Instalación mínima de Gnome</es_ES>
   <es>Instalación mínima de Gnome</es>
   <et>Very minimal install of Gnome</et>
   <eu>Very minimal install of Gnome</eu>
   <fa>Very minimal install of Gnome</fa>
   <fil_PH>Very minimal install of Gnome</fil_PH>
   <fi>Very minimal install of Gnome</fi>
   <fr_BE>Installation très minimale de Gnome</fr_BE>
   <fr>Installation très minimale de Gnome</fr>
   <gl_ES>Very minimal install of Gnome</gl_ES>
   <gu>Very minimal install of Gnome</gu>
   <he_IL>Very minimal install of Gnome</he_IL>
   <hi>गनोम का अति संक्षिप्त इंस्टॉल</hi>
   <hr>Very minimal install of Gnome</hr>
   <hu>Very minimal install of Gnome</hu>
   <id>Very minimal install of Gnome</id>
   <is>Very minimal install of Gnome</is>
   <it>Installazione molto minimale di Gnome</it>
   <ja>最小限のGnomeインストール</ja>
   <kk>Very minimal install of Gnome</kk>
   <ko>Very minimal install of Gnome</ko>
   <ku>Very minimal install of Gnome</ku>
   <lt>Very minimal install of Gnome</lt>
   <mk>Very minimal install of Gnome</mk>
   <mr>Very minimal install of Gnome</mr>
   <nb_NO>Very minimal install of Gnome</nb_NO>
   <nb>Minste mulige installasjon av Gnome</nb>
   <nl_BE>Very minimal install of Gnome</nl_BE>
   <nl>Uiterst minimale installatie van Gnome</nl>
   <or>Very minimal install of Gnome</or>
   <pl>Very minimal install of Gnome</pl>
   <pt_BR>Instalação mínima do Gnome</pt_BR>
   <pt>Instalação muito mínima do Gnome</pt>
   <ro>Very minimal install of Gnome</ro>
   <ru>Very minimal install of Gnome</ru>
   <sk>Very minimal install of Gnome</sk>
   <sl>Zelo minimalna namestitev Gnome namizja</sl>
   <so>Very minimal install of Gnome</so>
   <sq>Mjete deposh GIT</sq>
   <sr>Very minimal install of Gnome</sr>
   <sv>Mycket minimal installation av Gnome</sv>
   <th>Very minimal install of Gnome</th>
   <tr>Gnome'un çok az kurulumu</tr>
   <uk>Very minimal install of Gnome</uk>
   <vi>Very minimal install of Gnome</vi>
   <zh_CN>Very minimal install of Gnome</zh_CN>
   <zh_HK>Very minimal install of Gnome</zh_HK>
   <zh_TW>Very minimal install of Gnome</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gnome-core
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gnome-core
</uninstall_package_names>
</app>
