<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Catalan_Firefox
</name>

<description>
   <am>Catalan localisation of Firefox</am>
   <ar>Catalan localisation of Firefox</ar>
   <be>Catalan localisation of Firefox</be>
   <bg>Catalan localisation of Firefox</bg>
   <bn>Catalan localisation of Firefox</bn>
   <ca>Localització de Firefox en Català</ca>
   <cs>Catalan localisation of Firefox</cs>
   <da>Catalansk oversættelse af Firefox</da>
   <de>Katalanische Lokalisierung von Firefox</de>
   <el>Καταλανικά για το Firefox</el>
   <en>Catalan localisation of Firefox</en>
   <es_ES>Localización Catalana para Firefox</es_ES>
   <es>Localización Catalán para Firefox</es>
   <et>Catalan localisation of Firefox</et>
   <eu>Catalan localisation of Firefox</eu>
   <fa>Catalan localisation of Firefox</fa>
   <fil_PH>Catalan localisation of Firefox</fil_PH>
   <fi>Katalonialainen Firefox-kotoistus </fi>
   <fr_BE>Localisation en catalan pour Firefox</fr_BE>
   <fr>Localisation en catalan pour Firefox</fr>
   <gl_ES>Localización de Firefox en catalán</gl_ES>
   <gu>Catalan localisation of Firefox</gu>
   <he_IL>Catalan localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का कैटलन संस्करण</hi>
   <hr>Catalan localisation of Firefox</hr>
   <hu>Catalan localisation of Firefox</hu>
   <id>Catalan localisation of Firefox</id>
   <is>Catalan localisation of Firefox</is>
   <it>Localizzazione catalana di Firefox</it>
   <ja>Firefox のカタロニア語版</ja>
   <kk>Catalan localisation of Firefox</kk>
   <ko>Catalan localisation of Firefox</ko>
   <ku>Catalan localisation of Firefox</ku>
   <lt>Catalan localisation of Firefox</lt>
   <mk>Catalan localisation of Firefox</mk>
   <mr>Catalan localisation of Firefox</mr>
   <nb_NO>Catalan localisation of Firefox</nb_NO>
   <nb>Katalansk lokaltilpassing av Firefox</nb>
   <nl_BE>Catalan localisation of Firefox</nl_BE>
   <nl>Catalaanse lokalisatie van Firefox</nl>
   <or>Catalan localisation of Firefox</or>
   <pl>Katalońska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Catalão Localização para o Firefox</pt_BR>
   <pt>Catalão Localização para Firefox</pt>
   <ro>Catalan localisation of Firefox</ro>
   <ru>Каталонская локализация Firefox</ru>
   <sk>Catalan localisation of Firefox</sk>
   <sl>Katalonske krajevne nastavitve za Firefox</sl>
   <so>Catalan localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në katalanisht</sq>
   <sr>Catalan localisation of Firefox</sr>
   <sv>Katalansk lokalisering av Firefox</sv>
   <th>Catalan localisation ของFirefox</th>
   <tr>Firefox'un Katalanca yerelleştirmesi</tr>
   <uk>Catalan локалізація Firefox</uk>
   <vi>Catalan localisation of Firefox</vi>
   <zh_CN>Firefox 加泰罗尼亚语语言包</zh_CN>
   <zh_HK>Catalan localisation of Firefox</zh_HK>
   <zh_TW>Catalan localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-ca
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-ca
</uninstall_package_names>
</app>
