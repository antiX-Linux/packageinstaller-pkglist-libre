<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Office
</category>

<name>
Calibre
</name>

<description>
   <am>an e-book library management application</am>
   <ar>an e-book library management application</ar>
   <be>an e-book library management application</be>
   <bg>an e-book library management application</bg>
   <bn>an e-book library management application</bn>
   <ca>una aplicació de gestió de biblioteca de llibres electrònics</ca>
   <cs>an e-book library management application</cs>
   <da>et program til at håndtere e-bogsbibliotek</da>
   <de>Eine E-Book-Bibliotheksverwaltung</de>
   <el>Εφαρμογή διαχείρισης βιβλιοθήκης ηλεκτρονικών βιβλίων</el>
   <en>an e-book library management application</en>
   <es_ES>Organizador de biblioteca de e-book</es_ES>
   <es>Organizador de biblioteca de e-book</es>
   <et>an e-book library management application</et>
   <eu>an e-book library management application</eu>
   <fa>an e-book library management application</fa>
   <fil_PH>an e-book library management application</fil_PH>
   <fi>e-kirjoja sisältävän kirjaston hallintasovellus</fi>
   <fr_BE>Une application pour organiser sa bibliothèque e-book</fr_BE>
   <fr>Une application pour organiser sa bibliothèque e-book</fr>
   <gl_ES>Aplicativo de xestión de bibliotecas para libros electrónicos</gl_ES>
   <gu>an e-book library management application</gu>
   <he_IL>an e-book library management application</he_IL>
   <hi>ई-पुस्तक संग्रह प्रबंधन अनुप्रयोग</hi>
   <hr>an e-book library management application</hr>
   <hu>an e-book library management application</hu>
   <id>an e-book library management application</id>
   <is>an e-book library management application</is>
   <it>Applicazione per la gestione di librerie e-book</it>
   <ja>電子図書館管理アプリ</ja>
   <kk>an e-book library management application</kk>
   <ko>an e-book library management application</ko>
   <ku>an e-book library management application</ku>
   <lt>an e-book library management application</lt>
   <mk>an e-book library management application</mk>
   <mr>an e-book library management application</mr>
   <nb_NO>an e-book library management application</nb_NO>
   <nb>program for å håndtere e-bøker</nb>
   <nl_BE>an e-book library management application</nl_BE>
   <nl>een e-boek bibliotheek beheer applicatie</nl>
   <or>an e-book library management application</or>
   <pl>aplikacja do zarządzania bibliotekami e-booków w systemie,  ich wyświetlania, edycji, tworzenia i konwersji</pl>
   <pt_BR>E-book - Aplicativo de gerenciamento de bibliotecas para livros eletrônicos</pt_BR>
   <pt>Aplicação de gestão de bibliotecas para livros electrónicos e-book</pt>
   <ro>an e-book library management application</ro>
   <ru>Менеджер библиотеки электронных книг</ru>
   <sk>an e-book library management application</sk>
   <sl>aplikacija za upravljanje knjižnic z elektronskimi knjigami</sl>
   <so>an e-book library management application</so>
   <sq>Një aplikacion administrimi bibliotekash e-librash</sq>
   <sr>an e-book library management application</sr>
   <sv>en hanteringsapp för e-boksbibliotek</sv>
   <th>แอปพลิเคชันจัดการห้องสมุด e-book</th>
   <tr>bir e-kitap kütüphane yönetim uygulaması</tr>
   <uk>програма керування бібліотекою електронних книжок</uk>
   <vi>một ứng dụng quản lí thư viện sách điện tử</vi>
   <zh_CN>电子书管理程序</zh_CN>
   <zh_HK>an e-book library management application</zh_HK>
   <zh_TW>an e-book library management application</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
calibre
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
calibre
</uninstall_package_names>
</app>
