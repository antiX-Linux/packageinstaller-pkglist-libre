<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Themes
</category>

<name>
Arc-EvoPro2 Gtk Theme
</name>

<description>
   <am>antiX 19 GTK theme (Dark)</am>
   <ar>antiX 19 GTK theme (Dark)</ar>
   <be>antiX 19 GTK theme (Dark)</be>
   <bg>antiX 19 GTK theme (Dark)</bg>
   <bn>antiX 19 GTK theme (Dark)</bn>
   <ca>Tema antiX 19 GTK (Fosc)</ca>
   <cs>antiX 19 GTK theme (Dark)</cs>
   <da>antiX 19 GTK theme (Dark)</da>
   <de>antiX 19 GTK Stil (Dunkel)</de>
   <el>antiX 19 GTK theme (Σκούρο)</el>
   <en>antiX 19 GTK theme (Dark)</en>
   <es_ES>Tema GTK (oscuro) para antiX 19</es_ES>
   <es>Tema GTK oscuro para antiX 19</es>
   <et>antiX 19 GTK theme (Dark)</et>
   <eu>antiX 19 GTK theme (Dark)</eu>
   <fa>antiX 19 GTK theme (Dark)</fa>
   <fil_PH>antiX 19 GTK theme (Dark)</fil_PH>
   <fi>antiX 19 GTK teema (tumma)</fi>
   <fr_BE>antiX 19 GTK thème (Dark)</fr_BE>
   <fr>antiX 19 GTK thème (Dark)</fr>
   <gl_ES>antiX 19 GTK theme (Dark)</gl_ES>
   <gu>antiX 19 GTK theme (Dark)</gu>
   <he_IL>antiX 19 GTK theme (Dark)</he_IL>
   <hi>एंटी-एक्स 19 जीटीके थीम (गहरी)</hi>
   <hr>antiX 19 GTK theme (Dark)</hr>
   <hu>antiX 19 GTK theme (Dark)</hu>
   <id>antiX 19 GTK theme (Dark)</id>
   <is>antiX 19 GTK theme (Dark)</is>
   <it>Tema GTK (Scuro) di antiX 19</it>
   <ja>antiX 19 GTKテーマ(ダーク)</ja>
   <kk>antiX 19 GTK theme (Dark)</kk>
   <ko>antiX 19 GTK theme (Dark)</ko>
   <ku>antiX 19 GTK theme (Dark)</ku>
   <lt>antiX 19 GTK theme (Dark)</lt>
   <mk>antiX 19 GTK theme (Dark)</mk>
   <mr>antiX 19 GTK theme (Dark)</mr>
   <nb_NO>antiX 19 GTK theme (Dark)</nb_NO>
   <nb>antiX 19 GTK-tema (mørkt)</nb>
   <nl_BE>antiX 19 GTK theme (Dark)</nl_BE>
   <nl>antiX 19 GTK thema (donker)</nl>
   <or>antiX 19 GTK theme (Dark)</or>
   <pl>antiX 19 GTK theme (Dark)</pl>
   <pt_BR>Tema Escuro para o antiX 19 GTK</pt_BR>
   <pt>Tema GTK (Escuro) para o antiX 19</pt>
   <ro>antiX 19 GTK theme (Dark)</ro>
   <ru>antiX 19 GTK theme (Dark)</ru>
   <sk>antiX 19 GTK theme (Dark)</sk>
   <sl>antiX 19 GTK tema (temna)</sl>
   <so>antiX 19 GTK theme (Dark)</so>
   <sq>Temë GTK antiX 19 (E errët)</sq>
   <sr>antiX 19 GTK theme (Dark)</sr>
   <sv>antiX 19 GTK tema (Mörk)</sv>
   <th>antiX 19 GTK theme (Dark)</th>
   <tr>antiX 19 GTK teması (Koyu)</tr>
   <uk>antiX 19 GTK theme (Dark)</uk>
   <vi>antiX 19 GTK theme (Dark)</vi>
   <zh_CN>antiX 19 GTK theme (Dark)</zh_CN>
   <zh_HK>antiX 19 GTK theme (Dark)</zh_HK>
   <zh_TW>antiX 19 GTK theme (Dark)</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
arc-evopro2-theme-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
arc-evopro2-theme-antix
</uninstall_package_names>
</app>
