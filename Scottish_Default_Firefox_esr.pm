<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Scottish_Default_Firefox_esr
</name>

<description>
   <am>Scottish Gaelic localisation of Firefox ESR</am>
   <ar>Scottish Gaelic localisation of Firefox ESR</ar>
   <be>Scottish Gaelic localisation of Firefox ESR</be>
   <bg>Scottish Gaelic localisation of Firefox ESR</bg>
   <bn>Scottish Gaelic localisation of Firefox ESR</bn>
   <ca>Localització en gaèlic escocès de Firefox ESR</ca>
   <cs>Scottish Gaelic localisation of Firefox ESR</cs>
   <da>Scottish Gaelic localisation of Firefox ESR</da>
   <de>Scottish Gaelic localisation of Firefox ESR</de>
   <el>Scottish Gaelic για Firefox ESR</el>
   <en>Scottish Gaelic localisation of Firefox ESR</en>
   <es_ES>Localización en gaélico escocés de Firefox ESR</es_ES>
   <es>Localización en gaélico escocés de Firefox ESR</es>
   <et>Scottish Gaelic localisation of Firefox ESR</et>
   <eu>Scottish Gaelic localisation of Firefox ESR</eu>
   <fa>Scottish Gaelic localisation of Firefox ESR</fa>
   <fil_PH>Scottish Gaelic localisation of Firefox ESR</fil_PH>
   <fi>Scottish Gaelic localisation of Firefox ESR</fi>
   <fr_BE>Localisation en gaélique écossais pour Firefox ESR</fr_BE>
   <fr>Localisation en gaélique écossais pour Firefox ESR</fr>
   <gl_ES>Scottish Gaelic localisation of Firefox ESR</gl_ES>
   <gu>Scottish Gaelic localisation of Firefox ESR</gu>
   <he_IL>Scottish Gaelic localisation of Firefox ESR</he_IL>
   <hi>Scottish Gaelic localisation of Firefox ESR</hi>
   <hr>Scottish Gaelic localisation of Firefox ESR</hr>
   <hu>Scottish Gaelic localisation of Firefox ESR</hu>
   <id>Scottish Gaelic localisation of Firefox ESR</id>
   <is>Scottish Gaelic localisation of Firefox ESR</is>
   <it>Localizzazione Scottish Gaelic di Firefox ESR</it>
   <ja>Scottish Gaelic localisation of Firefox ESR</ja>
   <kk>Scottish Gaelic localisation of Firefox ESR</kk>
   <ko>Scottish Gaelic localisation of Firefox ESR</ko>
   <ku>Scottish Gaelic localisation of Firefox ESR</ku>
   <lt>Scottish Gaelic localisation of Firefox ESR</lt>
   <mk>Scottish Gaelic localisation of Firefox ESR</mk>
   <mr>Scottish Gaelic localisation of Firefox ESR</mr>
   <nb_NO>Scottish Gaelic localisation of Firefox ESR</nb_NO>
   <nb>Skotsk-gælisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Scottish Gaelic localisation of Firefox ESR</nl_BE>
   <nl>Scottish Gaelic localisation of Firefox ESR</nl>
   <or>Scottish Gaelic localisation of Firefox ESR</or>
   <pl>Scottish Gaelic localisation of Firefox ESR</pl>
   <pt_BR>Gaélico Escocês Localização para o Firefox ESR</pt_BR>
   <pt>Gaélico Escocês Localização para Firefox ESR</pt>
   <ro>Scottish Gaelic localisation of Firefox ESR</ro>
   <ru>Scottish Gaelic localisation of Firefox ESR</ru>
   <sk>Scottish Gaelic localisation of Firefox ESR</sk>
   <sl>Škotsko gelške krajevne nastavitve za Firefox ESR</sl>
   <so>Scottish Gaelic localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në gaelik skoceze</sq>
   <sr>Scottish Gaelic localisation of Firefox ESR</sr>
   <sv>Skottsk Gaelisk lokalisering av Firefox ESR</sv>
   <th>Scottish Gaelic localisation of Firefox ESR</th>
   <tr>Scottish Gaelic localisation of Firefox ESR</tr>
   <uk>Scottish Gaelic localisation of Firefox ESR</uk>
   <vi>Scottish Gaelic localisation of Firefox ESR</vi>
   <zh_CN>Scottish Gaelic localisation of Firefox ESR</zh_CN>
   <zh_HK>Scottish Gaelic localisation of Firefox ESR</zh_HK>
   <zh_TW>Scottish Gaelic localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-gd
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-gd
</uninstall_package_names>

</app>
