<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Video
</category>

<name>
SMplayer
</name>

<description>
   <am>themable gui frontend to MPlayer with other interesting features</am>
   <ar>themable gui frontend to MPlayer with other interesting features</ar>
   <be>themable gui frontend to MPlayer with other interesting features</be>
   <bg>themable gui frontend to MPlayer with other interesting features</bg>
   <bn>themable gui frontend to MPlayer with other interesting features</bn>
   <ca>Frontal IGU configurable per MPlayer amb característiques interessants</ca>
   <cs>themable gui frontend to MPlayer with other interesting features</cs>
   <da>grafisk brugerflade-frontend med temaer til MPlayer med andre interessante funktionaliteter</da>
   <de>Themen-anpassbares GUI-Frontend zu MPlayer mit weiteren interessanten Funktionen</de>
   <el>Γραφικό περιβάλλον frontend του MPlayer και με άλλα ενδιαφέροντα χαρακτηριστικά</el>
   <en>themable gui frontend to MPlayer with other interesting features</en>
   <es_ES>Interfaz gráfica de usuario temática para MPlayer con otras características interesantes</es_ES>
   <es>Interfaz gráfica de usuario temática para MPlayer con otras características interesantes</es>
   <et>themable gui frontend to MPlayer with other interesting features</et>
   <eu>themable gui frontend to MPlayer with other interesting features</eu>
   <fa>themable gui frontend to MPlayer with other interesting features</fa>
   <fil_PH>themable gui frontend to MPlayer with other interesting features</fil_PH>
   <fi>teemattava graafinen MPlayer-etuaste ja muita kiinnostavia ominaisuuksia</fi>
   <fr_BE>Interface graphique thématique pour MPlayer avec d'autres fonctionnalités intéressantes</fr_BE>
   <fr>Interface graphique thématique pour MPlayer avec d'autres fonctionnalités intéressantes</fr>
   <gl_ES>Interface gráfica para o MPlayer con varios temas e con outras funcionalidades interesantes</gl_ES>
   <gu>themable gui frontend to MPlayer with other interesting features</gu>
   <he_IL>themable gui frontend to MPlayer with other interesting features</he_IL>
   <hi>एम-प्लेयर का विभिन्न थीम युक्त ग्राफ़िकल फ्रंट एन्ड जिसमें अनेक रोचक विशेषताएँ हैं</hi>
   <hr>themable gui frontend to MPlayer with other interesting features</hr>
   <hu>themable gui frontend to MPlayer with other interesting features</hu>
   <id>themable gui frontend to MPlayer with other interesting features</id>
   <is>themable gui frontend to MPlayer with other interesting features</is>
   <it>interfaccia grafica per MPlayer con ulteriori interessanti funzionalità</it>
   <ja>MPlayer に他の興味深い機能を組み合わせた GUI フロントエンド</ja>
   <kk>themable gui frontend to MPlayer with other interesting features</kk>
   <ko>themable gui frontend to MPlayer with other interesting features</ko>
   <ku>themable gui frontend to MPlayer with other interesting features</ku>
   <lt>themable gui frontend to MPlayer with other interesting features</lt>
   <mk>themable gui frontend to MPlayer with other interesting features</mk>
   <mr>themable gui frontend to MPlayer with other interesting features</mr>
   <nb_NO>themable gui frontend to MPlayer with other interesting features</nb_NO>
   <nb>Grenseflate til Mplayer med ulike temaer og annen interessant funksjonalitet</nb>
   <nl_BE>themable gui frontend to MPlayer with other interesting features</nl_BE>
   <nl>gui die van thema's kan worden voorzien, frontend voor MPlayer met andere interessante functies</nl>
   <or>themable gui frontend to MPlayer with other interesting features</or>
   <pl>tematyczna nakładka na GUI do MPlayera z innymi ciekawymi funkcjami</pl>
   <pt_BR>Interface gráfica de usuário (GUI) para o MPlayer com vários temas e com outras funcionalidades interessantes</pt_BR>
   <pt>Interface gráfica para o MPlayer com vários temas e com outras funcionalidades interessantes</pt>
   <ro>themable gui frontend to MPlayer with other interesting features</ro>
   <ru>Графическая оболочка для MPlayer с множеством фишек</ru>
   <sk>themable gui frontend to MPlayer with other interesting features</sk>
   <sl>Prilagodljiva grafična podoba za Mplayer z zanimivimi dodatnimi funkcijami</sl>
   <so>themable gui frontend to MPlayer with other interesting features</so>
   <sq>Pjesë e përparme GUI, e ndryshueshme me tema, për MPlayer-in, me veçori të tjera interesante</sq>
   <sr>themable gui frontend to MPlayer with other interesting features</sr>
   <sv>temabar grafisk front-end till MPlayer med andra intressanta möjligheter</sv>
   <th>themable gui frontend to MPlayer with other interesting features</th>
   <tr>diğer ilginç özellikler ile MPlayer için temalı grafik arayüz ön ucu</tr>
   <uk>themable gui frontend to MPlayer with other interesting features</uk>
   <vi>themable gui frontend to MPlayer with other interesting features</vi>
   <zh_CN>themable gui frontend to MPlayer with other interesting features</zh_CN>
   <zh_HK>themable gui frontend to MPlayer with other interesting features</zh_HK>
   <zh_TW>themable gui frontend to MPlayer with other interesting features</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
smplayer
smplayer-themes
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
smplayer
smplayer-themes
</uninstall_package_names>
</app>
