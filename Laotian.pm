<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Laotian
</name>

<description>
   <am>Laotian dictionary for hunspell</am>
   <ar>Laotian dictionary for hunspell</ar>
   <be>Laotian dictionary for hunspell</be>
   <bg>Laotian dictionary for hunspell</bg>
   <bn>Laotian dictionary for hunspell</bn>
   <ca>Diccionari Laosià per hunspell</ca>
   <cs>Laotian dictionary for hunspell</cs>
   <da>Laotian dictionary for hunspell</da>
   <de>Laotisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Λάος για hunspell</el>
   <en>Laotian dictionary for hunspell</en>
   <es_ES>Diccionario Laosiano para hunspell</es_ES>
   <es>Diccionario Laosiano para hunspell</es>
   <et>Laotian dictionary for hunspell</et>
   <eu>Laotian dictionary for hunspell</eu>
   <fa>Laotian dictionary for hunspell</fa>
   <fil_PH>Laotian dictionary for hunspell</fil_PH>
   <fi>Laotian dictionary for hunspell</fi>
   <fr_BE>Laotien dictionnaire pour hunspell</fr_BE>
   <fr>Laotien dictionnaire pour hunspell</fr>
   <gl_ES>Laotian dictionary for hunspell</gl_ES>
   <gu>Laotian dictionary for hunspell</gu>
   <he_IL>Laotian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु लोटियन शब्दकोष</hi>
   <hr>Laotian dictionary for hunspell</hr>
   <hu>Laotian dictionary for hunspell</hu>
   <id>Laotian dictionary for hunspell</id>
   <is>Laotian dictionary for hunspell</is>
   <it>Dizionario laotian per hunspell</it>
   <ja>Hunspell 用ラオス語辞書</ja>
   <kk>Laotian dictionary for hunspell</kk>
   <ko>Laotian dictionary for hunspell</ko>
   <ku>Laotian dictionary for hunspell</ku>
   <lt>Laotian dictionary for hunspell</lt>
   <mk>Laotian dictionary for hunspell</mk>
   <mr>Laotian dictionary for hunspell</mr>
   <nb_NO>Laotian dictionary for hunspell</nb_NO>
   <nb>Laotisk ordliste for hunspell</nb>
   <nl_BE>Laotian dictionary for hunspell</nl_BE>
   <nl>Laotian dictionary for hunspell</nl>
   <or>Laotian dictionary for hunspell</or>
   <pl>Laotian dictionary for hunspell</pl>
   <pt_BR>Dicionário Laosiano para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Laosiano para hunspell</pt>
   <ro>Laotian dictionary for hunspell</ro>
   <ru>Laotian dictionary for hunspell</ru>
   <sk>Laotian dictionary for hunspell</sk>
   <sl>laotanski slovar za hunspell</sl>
   <so>Laotian dictionary for hunspell</so>
   <sq>Fjalor laosisht për hunspell</sq>
   <sr>Laotian dictionary for hunspell</sr>
   <sv>Laotisk ordbok för hunspell</sv>
   <th>Laotian dictionary for hunspell</th>
   <tr>Hunspell için Laosça sözlük</tr>
   <uk>Laotian dictionary for hunspell</uk>
   <vi>Laotian dictionary for hunspell</vi>
   <zh_CN>Laotian dictionary for hunspell</zh_CN>
   <zh_HK>Laotian dictionary for hunspell</zh_HK>
   <zh_TW>Laotian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-lo
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-lo
</uninstall_package_names>

</app>
