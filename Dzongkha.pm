<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Dzongkha
</name>

<description>
   <am>Dzongkha dictionary for hunspell</am>
   <ar>Dzongkha dictionary for hunspell</ar>
   <be>Dzongkha dictionary for hunspell</be>
   <bg>Dzongkha dictionary for hunspell</bg>
   <bn>Dzongkha dictionary for hunspell</bn>
   <ca>Diccionari Dzongkha per hunspell</ca>
   <cs>Dzongkha dictionary for hunspell</cs>
   <da>Dzongkha dictionary for hunspell</da>
   <de>Bhutanisches Wörterbuch für die Sprache Dzongkha für “Hunspell”</de>
   <el>Dzongkha λεξικό για hunspell</el>
   <en>Dzongkha dictionary for hunspell</en>
   <es_ES>Diccionario Dzongkha para hunspell</es_ES>
   <es>Diccionario Dzongkha para hunspell</es>
   <et>Dzongkha dictionary for hunspell</et>
   <eu>Dzongkha dictionary for hunspell</eu>
   <fa>Dzongkha dictionary for hunspell</fa>
   <fil_PH>Dzongkha dictionary for hunspell</fil_PH>
   <fi>Dzongkha dictionary for hunspell</fi>
   <fr_BE>Dzongkha dictionnaire pour hunspell</fr_BE>
   <fr>Dzongkha dictionnaire pour hunspell</fr>
   <gl_ES>Dzongkha dictionary for hunspell</gl_ES>
   <gu>Dzongkha dictionary for hunspell</gu>
   <he_IL>Dzongkha dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु जोंगखा शब्दकोष</hi>
   <hr>Dzongkha dictionary for hunspell</hr>
   <hu>Dzongkha dictionary for hunspell</hu>
   <id>Dzongkha dictionary for hunspell</id>
   <is>Dzongkha dictionary for hunspell</is>
   <it>Dizionario di lingua dzongkha per hunspell</it>
   <ja>Hunspell 用ゾンカ語辞書</ja>
   <kk>Dzongkha dictionary for hunspell</kk>
   <ko>Dzongkha dictionary for hunspell</ko>
   <ku>Dzongkha dictionary for hunspell</ku>
   <lt>Dzongkha dictionary for hunspell</lt>
   <mk>Dzongkha dictionary for hunspell</mk>
   <mr>Dzongkha dictionary for hunspell</mr>
   <nb_NO>Dzongkha dictionary for hunspell</nb_NO>
   <nb>Dzongkha ordliste for hunspell</nb>
   <nl_BE>Dzongkha dictionary for hunspell</nl_BE>
   <nl>Dzongkha woordenboek voor hunspell</nl>
   <or>Dzongkha dictionary for hunspell</or>
   <pl>Dzongkha dictionary for hunspell</pl>
   <pt_BR>Dicionário Butanês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Dzongkha para hunspell</pt>
   <ro>Dzongkha dictionary for hunspell</ro>
   <ru>Dzongkha dictionary for hunspell</ru>
   <sk>Dzongkha dictionary for hunspell</sk>
   <sl>Dzongha slovar za hunspell</sl>
   <so>Dzongkha dictionary for hunspell</so>
   <sq>Fjalor xonga për hunspell</sq>
   <sr>Dzongkha dictionary for hunspell</sr>
   <sv>Dzongkha ordbok för hunspell</sv>
   <th>Dzongkha dictionary for hunspell</th>
   <tr>Hunspell için Bhutan Dili sözlüğü</tr>
   <uk>Dzongkha dictionary for hunspell</uk>
   <vi>Dzongkha dictionary for hunspell</vi>
   <zh_CN>Dzongkha dictionary for hunspell</zh_CN>
   <zh_HK>Dzongkha dictionary for hunspell</zh_HK>
   <zh_TW>Dzongkha dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-dz
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-dz
</uninstall_package_names>

</app>
