<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
GB_English_Firefox
</name>

<description>
   <am>GB English localisation of Firefox</am>
   <ar>GB English localisation of Firefox</ar>
   <be>GB English localisation of Firefox</be>
   <bg>GB English localisation of Firefox</bg>
   <bn>GB English localisation of Firefox</bn>
   <ca>Localització de Firefox en anglès (UK)</ca>
   <cs>GB English localisation of Firefox</cs>
   <da>Engelsk (storbritannien) oversættelse af Firefox</da>
   <de>Englische (GB) Lokalisierung von Firefox</de>
   <el>Αγγλικά(GΒ) για το Firefox</el>
   <en>GB English localisation of Firefox</en>
   <es_ES>Localización Inglesa (GB) de Firefox</es_ES>
   <es>Localización Inglés (GB) de Firefox</es>
   <et>GB English localisation of Firefox</et>
   <eu>GB English localisation of Firefox</eu>
   <fa>GB English localisation of Firefox</fa>
   <fil_PH>GB English localisation of Firefox</fil_PH>
   <fi>GB-englantilainen Firefox-kotoistus</fi>
   <fr_BE>Localisation anglaise_GB pour Firefox</fr_BE>
   <fr>Localisation anglaise_GB pour Firefox</fr>
   <gl_ES>Localización de Firefox en British English</gl_ES>
   <gu>GB English localisation of Firefox</gu>
   <he_IL>GB English localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का ब्रिटिश अंग्रेज़ी संस्करण</hi>
   <hr>GB English localisation of Firefox</hr>
   <hu>GB English localisation of Firefox</hu>
   <id>GB English localisation of Firefox</id>
   <is>GB English localisation of Firefox</is>
   <it>Localizzazione in inglese GB di Firefox</it>
   <ja>Firefox のイギリス英語版</ja>
   <kk>GB English localisation of Firefox</kk>
   <ko>GB English localisation of Firefox</ko>
   <ku>GB English localisation of Firefox</ku>
   <lt>GB English localisation of Firefox</lt>
   <mk>GB English localisation of Firefox</mk>
   <mr>GB English localisation of Firefox</mr>
   <nb_NO>GB English localisation of Firefox</nb_NO>
   <nb>Britisk engelsk lokaltilpassing av Firefox</nb>
   <nl_BE>GB English localisation of Firefox</nl_BE>
   <nl>GB Engelse lokalisatie van Firefox</nl>
   <or>GB English localisation of Firefox</or>
   <pl>Angielski Brytyjski lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Inglês da Grã-Bretanha Localização para o Firefox</pt_BR>
   <pt>Inglês GB Localização para Firefox</pt>
   <ro>GB English localisation of Firefox</ro>
   <ru>GB English localisation of Firefox</ru>
   <sk>GB English localisation of Firefox</sk>
   <sl>VB angleške krajevne nastavitve za Firefox</sl>
   <so>GB English localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në anglishte britanike</sq>
   <sr>GB English localisation of Firefox</sr>
   <sv>GB Engelsk lokalisering av Firefox</sv>
   <th>GB English localisation ของ Firefox</th>
   <tr>Firefox'un Britanya İngilizcesi yerelleştirmesi</tr>
   <uk>GB English локалізація Firefox</uk>
   <vi>GB English localisation of Firefox</vi>
   <zh_CN>Firefox 英式英语语言包</zh_CN>
   <zh_HK>GB English localisation of Firefox</zh_HK>
   <zh_TW>GB English localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-en-gb
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-en-gb
</uninstall_package_names>
</app>
