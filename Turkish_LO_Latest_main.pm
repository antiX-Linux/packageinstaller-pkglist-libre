<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Turkish_LO_Latest_main
</name>

<description>
   <am>Turkish Language Meta-Package for LibreOffice</am>
   <ar>Turkish Language Meta-Package for LibreOffice</ar>
   <be>Turkish Language Meta-Package for LibreOffice</be>
   <bg>Turkish Language Meta-Package for LibreOffice</bg>
   <bn>Turkish Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Turc per LibreOffice</ca>
   <cs>Turkish Language Meta-Package for LibreOffice</cs>
   <da>Turkish Language Meta-Package for LibreOffice</da>
   <de>Türkisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Τουρκικά</el>
   <en>Turkish Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idioma Turco para LibreOffice</es_ES>
   <es>Metapaquete de idioma Turco para LibreOffice</es>
   <et>Turkish Language Meta-Package for LibreOffice</et>
   <eu>Turkish Language Meta-Package for LibreOffice</eu>
   <fa>Turkish Language Meta-Package for LibreOffice</fa>
   <fil_PH>Turkish Language Meta-Package for LibreOffice</fil_PH>
   <fi>Turkinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet en langue turque pour LibreOffice</fr_BE>
   <fr>Méta-Paquet en langue turque pour LibreOffice</fr>
   <gl_ES>Turco Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Turkish Language Meta-Package for LibreOffice</gu>
   <he_IL>Turkish Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु तुर्क भाषा मेटा-पैकेज</hi>
   <hr>Turkish Language Meta-Package for LibreOffice</hr>
   <hu>Turkish Language Meta-Package for LibreOffice</hu>
   <id>Turkish Language Meta-Package for LibreOffice</id>
   <is>Turkish Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua turca per LibreOffice</it>
   <ja>LibreOffice用のトルコ語メタパッケージ</ja>
   <kk>Turkish Language Meta-Package for LibreOffice</kk>
   <ko>Turkish Language Meta-Package for LibreOffice</ko>
   <ku>Turkish Language Meta-Package for LibreOffice</ku>
   <lt>Turkish Language Meta-Package for LibreOffice</lt>
   <mk>Turkish Language Meta-Package for LibreOffice</mk>
   <mr>Turkish Language Meta-Package for LibreOffice</mr>
   <nb_NO>Turkish Language Meta-Package for LibreOffice</nb_NO>
   <nb>Tyrkisk språkpakke for LibreOffice</nb>
   <nl_BE>Turkish Language Meta-Package for LibreOffice</nl_BE>
   <nl>Taal Meta-Pakket voor LibreOffice</nl>
   <or>Turkish Language Meta-Package for LibreOffice</or>
   <pl>Turecki meta-pakiet językowy dla LibreOffice</pl>
   <pt_BR>Turco Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Turco Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Turkish Language Meta-Package for LibreOffice</ro>
   <ru>Turkish Language Meta-Package for LibreOffice</ru>
   <sk>Turkish Language Meta-Package for LibreOffice</sk>
   <sl>Turški jezikovni meta-paket za LibreOffice</sl>
   <so>Turkish Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në turqisht</sq>
   <sr>Turkish Language Meta-Package for LibreOffice</sr>
   <sv>Turkiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Turkish สำหรับ LibreOffice</th>
   <tr>LibreOffice için Türkçe Dili Üst-Paketi</tr>
   <uk>Turkish Language Meta-Package for LibreOffice</uk>
   <vi>Turkish Language Meta-Package for LibreOffice</vi>
   <zh_CN>Turkish Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Turkish Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Turkish Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-tr
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-tr
libreoffice-gtk3
</uninstall_package_names>

</app>
