<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
Cinnamon-Full
</name>

<description>
   <am>Cinnamon desktop - full install</am>
   <ar>Cinnamon desktop - full install</ar>
   <be>Cinnamon desktop - full install</be>
   <bg>Cinnamon desktop - full install</bg>
   <bn>Cinnamon desktop - full install</bn>
   <ca>Escriptori Cinnamon - instal·lació completa</ca>
   <cs>Cinnamon desktop - full install</cs>
   <da>Cinnamon desktop - full install</da>
   <de>“Cinnamon” Desktop-Umgebuing, vollständige Installation</de>
   <el>Επιτραπέζιος υπολογιστής Cinnamon - πλήρης εγκατάσταση</el>
   <en>Cinnamon desktop - full install</en>
   <es_ES>Escritorio Cinnamon - instalación completa</es_ES>
   <es>Escritorio Cinnamon - instalación completa</es>
   <et>Cinnamon desktop - full install</et>
   <eu>Cinnamon desktop - full install</eu>
   <fa>Cinnamon desktop - full install</fa>
   <fil_PH>Cinnamon desktop - full install</fil_PH>
   <fi>Cinnamon desktop - full install</fi>
   <fr_BE>Bureau Cinnamon - installation complète</fr_BE>
   <fr>Bureau Cinnamon - installation complète</fr>
   <gl_ES>Cinnamon desktop - full install</gl_ES>
   <gu>Cinnamon desktop - full install</gu>
   <he_IL>Cinnamon desktop - full install</he_IL>
   <hi>सिनेमन डेस्कटॉप - पूर्ण इंस्टॉल</hi>
   <hr>Cinnamon desktop - full install</hr>
   <hu>Cinnamon desktop - full install</hu>
   <id>Cinnamon desktop - full install</id>
   <is>Cinnamon desktop - full install</is>
   <it>Cinnamon desktop - installazione completa</it>
   <ja>Cinnamonデスクトップのフルインストール</ja>
   <kk>Cinnamon desktop - full install</kk>
   <ko>Cinnamon desktop - full install</ko>
   <ku>Cinnamon desktop - full install</ku>
   <lt>Cinnamon desktop - full install</lt>
   <mk>Cinnamon desktop - full install</mk>
   <mr>Cinnamon desktop - full install</mr>
   <nb_NO>Cinnamon desktop - full install</nb_NO>
   <nb>Skrivebordet Cinnamon – full installasjon</nb>
   <nl_BE>Cinnamon desktop - full install</nl_BE>
   <nl>Cinnamon-bureaublad - volledige installatie</nl>
   <or>Cinnamon desktop - full install</or>
   <pl>Cinnamon desktop - full install</pl>
   <pt_BR>Área de Trabalho Cinnamon/Canela - instalação completa</pt_BR>
   <pt>Ambiente de trabalho Cinnamon - instalação completa</pt>
   <ro>Cinnamon desktop - full install</ro>
   <ru>Cinnamon desktop - full install</ru>
   <sk>Cinnamon desktop - full install</sk>
   <sl>Cinnamon namizje - celotna namestitev</sl>
   <so>Cinnamon desktop - full install</so>
   <sq>Desktopi Cinnamon - instalim i plotë</sq>
   <sr>Cinnamon desktop - full install</sr>
   <sv>Cinnamon skrivbordsmiljö - full installation</sv>
   <th>Cinnamon desktop - full install</th>
   <tr>Cinnamon masaüstü - tam kurulum</tr>
   <uk>Cinnamon desktop - full install</uk>
   <vi>Cinnamon desktop - full install</vi>
   <zh_CN>Cinnamon desktop - full install</zh_CN>
   <zh_HK>Cinnamon desktop - full install</zh_HK>
   <zh_TW>Cinnamon desktop - full install</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
cinnamon-desktop-environment
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
cinnamon-desktop-environment
</uninstall_package_names>
</app>
