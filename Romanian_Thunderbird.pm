<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Romanian_Thunderbird
</name>

<description>
   <am>Romanian localisation of Thunderbird</am>
   <ar>Romanian localisation of Thunderbird</ar>
   <be>Romanian localisation of Thunderbird</be>
   <bg>Romanian localisation of Thunderbird</bg>
   <bn>Romanian localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Romanès</ca>
   <cs>Romanian localisation of Thunderbird</cs>
   <da>Rumænsk oversættelse af Thunderbird</da>
   <de>Rumänische Lokalisierung von Thunderbird</de>
   <el>Ρουμανικά για το Thunderbird</el>
   <en>Romanian localisation of Thunderbird</en>
   <es_ES>Localización Rumana de Thunderbird</es_ES>
   <es>Localización Rumano de Thunderbird</es>
   <et>Romanian localisation of Thunderbird</et>
   <eu>Romanian localisation of Thunderbird</eu>
   <fa>Romanian localisation of Thunderbird</fa>
   <fil_PH>Romanian localisation of Thunderbird</fil_PH>
   <fi>Romanialainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en roumain pour Thunderbird</fr_BE>
   <fr>Localisation en roumain pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao romanés</gl_ES>
   <gu>Romanian localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לרומנית</he_IL>
   <hi>थंडरबर्ड का रोमानियाई संस्करण</hi>
   <hr>Romanian localisation of Thunderbird</hr>
   <hu>Romanian localisation of Thunderbird</hu>
   <id>Romanian localisation of Thunderbird</id>
   <is>Romanian localisation of Thunderbird</is>
   <it>Localizzazione rumena di Thunderbird</it>
   <ja>Thunderbird のルーマニア語版</ja>
   <kk>Romanian localisation of Thunderbird</kk>
   <ko>Romanian localisation of Thunderbird</ko>
   <ku>Romanian localisation of Thunderbird</ku>
   <lt>Romanian localisation of Thunderbird</lt>
   <mk>Romanian localisation of Thunderbird</mk>
   <mr>Romanian localisation of Thunderbird</mr>
   <nb_NO>Romanian localisation of Thunderbird</nb_NO>
   <nb>Romansk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Romanian localisation of Thunderbird</nl_BE>
   <nl>Roemeense lokalisatie van Thunderbird</nl>
   <or>Romanian localisation of Thunderbird</or>
   <pl>Rumuńska lokalizacja Thunderbirda</pl>
   <pt_BR>Romeno Localização para o Thunderbird</pt_BR>
   <pt>Romeno Localização para Thunderbird</pt>
   <ro>Romanian localisation of Thunderbird</ro>
   <ru>Romanian localisation of Thunderbird</ru>
   <sk>Romanian localisation of Thunderbird</sk>
   <sl>Romunske krajevne nastavitve za Thunderbird</sl>
   <so>Romanian localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në rumanisht</sq>
   <sr>Romanian localisation of Thunderbird</sr>
   <sv>Rumänsk lokalisering av Thunderbird </sv>
   <th>Romanian localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Rumence yerelleştirmesi</tr>
   <uk>Romanian локалізація Thunderbird</uk>
   <vi>Romanian localisation of Thunderbird</vi>
   <zh_CN>Romanian localisation of Thunderbird</zh_CN>
   <zh_HK>Romanian localisation of Thunderbird</zh_HK>
   <zh_TW>Romanian localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-ro
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-ro
</uninstall_package_names>

</app>
