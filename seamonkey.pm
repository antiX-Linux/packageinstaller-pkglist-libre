<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Seamonkey
</name>

<description>
   <am>Latest Seamonkey packaged by MX Community</am>
   <ar>Latest Seamonkey packaged by MX Community</ar>
   <be>Latest Seamonkey packaged by MX Community</be>
   <bg>Latest Seamonkey packaged by MX Community</bg>
   <bn>Latest Seamonkey packaged by MX Community</bn>
   <ca>Darrer SeaMonkey empaquetat per la Comunitat MX</ca>
   <cs>Latest Seamonkey packaged by MX Community</cs>
   <da>Latest Seamonkey packaged by MX Community</da>
   <de>Aktuellstes “SeaMonkey” Internet-Anwendungspaket (Webbrowser, E-Mail-Programm, Chat-Client, HTML-Editor, Adressbuch und weitere Hilfsprogramme), paketiert von der MX-Linux Gemeinschaft.</de>
   <el>Τελευταίο Seamonkey συσκευασμένο από την Κοινότητα MX</el>
   <en>Latest Seamonkey packaged by MX Community</en>
   <es_ES>Último Seamonkey empaquetado por la comunidad MX</es_ES>
   <es>Último Seamonkey empaquetado por la comunidad MX</es>
   <et>Latest Seamonkey packaged by MX Community</et>
   <eu>Latest Seamonkey packaged by MX Community</eu>
   <fa>Latest Seamonkey packaged by MX Community</fa>
   <fil_PH>Latest Seamonkey packaged by MX Community</fil_PH>
   <fi>Viimeisin MX-yhteisön pakkaama Seamonkey</fi>
   <fr_BE>Le dernier Seamonkey empaqueté par la communauté MX</fr_BE>
   <fr>Le dernier Seamonkey empaqueté par la communauté MX</fr>
   <gl_ES>Latest Seamonkey packaged by MX Community</gl_ES>
   <gu>Latest Seamonkey packaged by MX Community</gu>
   <he_IL>Latest Seamonkey packaged by MX Community</he_IL>
   <hi>एमएक्स समुदाय द्वारा पैकेज किया गया नवीनतम सीमंकी</hi>
   <hr>Latest Seamonkey packaged by MX Community</hr>
   <hu>Latest Seamonkey packaged by MX Community</hu>
   <id>Latest Seamonkey packaged by MX Community</id>
   <is>Latest Seamonkey packaged by MX Community</is>
   <it>Seamonkey più recente fornito dalla  MX Community</it>
   <ja>MXコミュニティによる最新の Seamonkeyパッケージ</ja>
   <kk>Latest Seamonkey packaged by MX Community</kk>
   <ko>Latest Seamonkey packaged by MX Community</ko>
   <ku>Latest Seamonkey packaged by MX Community</ku>
   <lt>Latest Seamonkey packaged by MX Community</lt>
   <mk>Latest Seamonkey packaged by MX Community</mk>
   <mr>Latest Seamonkey packaged by MX Community</mr>
   <nb_NO>Latest Seamonkey packaged by MX Community</nb_NO>
   <nb>Seneste Seamonkey pakket av MX-miljøet</nb>
   <nl_BE>Latest Seamonkey packaged by MX Community</nl_BE>
   <nl>Latest Seamonkey packaged by MX Community</nl>
   <or>Latest Seamonkey packaged by MX Community</or>
   <pl>Latest Seamonkey packaged by MX Community</pl>
   <pt_BR>Seamonkey - Versão mais recente do navegador de internet empacotado pela Comunidade MX</pt_BR>
   <pt>Seamonkey mais recente empacotado pela Comunidade MX</pt>
   <ro>Latest Seamonkey packaged by MX Community</ro>
   <ru>Latest Seamonkey packaged by MX Community</ru>
   <sk>Latest Seamonkey packaged by MX Community</sk>
   <sl>Zadnja Seamnokey pakiran s strani MX skupnosti</sl>
   <so>Latest Seamonkey packaged by MX Community</so>
   <sq>Seamonkey më i ri, paketuar nga Bashkësia MX</sq>
   <sr>Latest Seamonkey packaged by MX Community</sr>
   <sv>Senaste Seamonkey paketerat av MX Community</sv>
   <th>Latest Seamonkey packaged by MX Community</th>
   <tr>MX Topluluğu tarafından paketlenmiş en son Seamonkey</tr>
   <uk>Latest Seamonkey packaged by MX Community</uk>
   <vi>Latest Seamonkey packaged by MX Community</vi>
   <zh_CN>Latest Seamonkey packaged by MX Community</zh_CN>
   <zh_HK>Latest Seamonkey packaged by MX Community</zh_HK>
   <zh_TW>Latest Seamonkey packaged by MX Community</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
seamonkey
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
seamonkey
</uninstall_package_names>
</app>
