<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Basque_Default_Firefox_esr
</name>

<description>
   <am>Basque localisation of Firefox ESR</am>
   <ar>Basque localisation of Firefox ESR</ar>
   <be>Basque localisation of Firefox ESR</be>
   <bg>Basque localisation of Firefox ESR</bg>
   <bn>Basque localisation of Firefox ESR</bn>
   <ca>Localització en Euskera de Firefox ESR</ca>
   <cs>Basque localisation of Firefox ESR</cs>
   <da>Basque localisation of Firefox ESR</da>
   <de>Basque localisation of Firefox ESR</de>
   <el>Βασκικά για το Firefox ESR</el>
   <en>Basque localisation of Firefox ESR</en>
   <es_ES>Localización en euskera de Firefox ESR</es_ES>
   <es>Localización en vasco de Firefox ESR</es>
   <et>Basque localisation of Firefox ESR</et>
   <eu>Basque localisation of Firefox ESR</eu>
   <fa>Basque localisation of Firefox ESR</fa>
   <fil_PH>Basque localisation of Firefox ESR</fil_PH>
   <fi>Basque localisation of Firefox ESR</fi>
   <fr_BE>Localisation en basque pour Firefox ESR</fr_BE>
   <fr>Localisation en basque pour Firefox ESR</fr>
   <gl_ES>Basque localisation of Firefox ESR</gl_ES>
   <gu>Basque localisation of Firefox ESR</gu>
   <he_IL>Basque localisation of Firefox ESR</he_IL>
   <hi>Basque localisation of Firefox ESR</hi>
   <hr>Basque localisation of Firefox ESR</hr>
   <hu>Basque localisation of Firefox ESR</hu>
   <id>Basque localisation of Firefox ESR</id>
   <is>Basque localisation of Firefox ESR</is>
   <it>Localizzazione Basque di Firefox ESR</it>
   <ja>Basque localisation of Firefox ESR</ja>
   <kk>Basque localisation of Firefox ESR</kk>
   <ko>Basque localisation of Firefox ESR</ko>
   <ku>Basque localisation of Firefox ESR</ku>
   <lt>Basque localisation of Firefox ESR</lt>
   <mk>Basque localisation of Firefox ESR</mk>
   <mr>Basque localisation of Firefox ESR</mr>
   <nb_NO>Basque localisation of Firefox ESR</nb_NO>
   <nb>Baskisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Basque localisation of Firefox ESR</nl_BE>
   <nl>Baskische lokalisatie van Firefox ESR</nl>
   <or>Basque localisation of Firefox ESR</or>
   <pl>Basque localisation of Firefox ESR</pl>
   <pt_BR>Basco Localização para o Firefox ESR</pt_BR>
   <pt>Basco Localização para Firefox ESR</pt>
   <ro>Basque localisation of Firefox ESR</ro>
   <ru>Basque localisation of Firefox ESR</ru>
   <sk>Basque localisation of Firefox ESR</sk>
   <sl>Baskovske krajevne nastavitve za Firefox ESR</sl>
   <so>Basque localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në baskisht</sq>
   <sr>Basque localisation of Firefox ESR</sr>
   <sv>Baskisk lokalisering av Firefox ESR</sv>
   <th>Basque localisation of Firefox ESR</th>
   <tr>Basque localisation of Firefox ESR</tr>
   <uk>Basque localisation of Firefox ESR</uk>
   <vi>Basque localisation of Firefox ESR</vi>
   <zh_CN>Basque localisation of Firefox ESR</zh_CN>
   <zh_HK>Basque localisation of Firefox ESR</zh_HK>
   <zh_TW>Basque localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-eu
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-eu
</uninstall_package_names>

</app>
