<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Office
</category>

<name>
Gnumeric
</name>

<description>
   <am>lightweight spreadsheet</am>
   <ar>lightweight spreadsheet</ar>
   <be>lightweight spreadsheet</be>
   <bg>lightweight spreadsheet</bg>
   <bn>lightweight spreadsheet</bn>
   <ca>Full de càlcul lleuger</ca>
   <cs>lightweight spreadsheet</cs>
   <da>letvægts regneark</da>
   <de>Leichtgewichtige Tabellenkalkulation</de>
   <el>Ελαφρύ υπολογιστικό φύλλο</el>
   <en>lightweight spreadsheet</en>
   <es_ES>Hoja de cálculo ligera</es_ES>
   <es>Hoja de cálculo ligera</es>
   <et>lightweight spreadsheet</et>
   <eu>lightweight spreadsheet</eu>
   <fa>lightweight spreadsheet</fa>
   <fil_PH>lightweight spreadsheet</fil_PH>
   <fi>keveä taulukkolaskentaohjelma</fi>
   <fr_BE>Tableur léger</fr_BE>
   <fr>Tableur léger</fr>
   <gl_ES>Folla de cálcula simple e lixeira</gl_ES>
   <gu>lightweight spreadsheet</gu>
   <he_IL>lightweight spreadsheet</he_IL>
   <hi>सरल स्प्रेडशीट</hi>
   <hr>lightweight spreadsheet</hr>
   <hu>lightweight spreadsheet</hu>
   <id>lightweight spreadsheet</id>
   <is>lightweight spreadsheet</is>
   <it>foglio di calcolo leggero</it>
   <ja>軽量な表計算ソフト</ja>
   <kk>lightweight spreadsheet</kk>
   <ko>lightweight spreadsheet</ko>
   <ku>lightweight spreadsheet</ku>
   <lt>lightweight spreadsheet</lt>
   <mk>lightweight spreadsheet</mk>
   <mr>lightweight spreadsheet</mr>
   <nb_NO>lightweight spreadsheet</nb_NO>
   <nb>lettvektig regneark</nb>
   <nl_BE>lightweight spreadsheet</nl_BE>
   <nl>lichtgewicht speadsheet</nl>
   <or>lightweight spreadsheet</or>
   <pl>lekki arkusz kalkulacyjny</pl>
   <pt_BR>Planilha eletrônica simples e leve</pt_BR>
   <pt>Folha de cálculo simples e ligeira</pt>
   <ro>lightweight spreadsheet</ro>
   <ru>Легковесный редактор электронных таблиц (включает основные функции Excel)</ru>
   <sk>lightweight spreadsheet</sk>
   <sl>lahka preglednica</sl>
   <so>lightweight spreadsheet</so>
   <sq>Fletëllogaritje e peshës së lehtë</sq>
   <sr>lightweight spreadsheet</sr>
   <sv>lättvikts spreadsheet</sv>
   <th>Spreadsheet น้ำหนักเบา</th>
   <tr>hafif bir hesap tablosu</tr>
   <uk>легкий табличний процесор</uk>
   <vi>lightweight spreadsheet</vi>
   <zh_CN>轻量级电子表格程序</zh_CN>
   <zh_HK>lightweight spreadsheet</zh_HK>
   <zh_TW>lightweight spreadsheet</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gnumeric
gnumeric-common
gnumeric-doc
gnumeric-plugins-extra
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gnumeric
gnumeric-common
gnumeric-doc
gnumeric-plugins-extra
</uninstall_package_names>
</app>
