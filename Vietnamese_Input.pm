<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Vietnamese_Input
</name>

<description>
   <am>Vietnamese Fonts and ibus</am>
   <ar>Vietnamese Fonts and ibus</ar>
   <be>Vietnamese Fonts and ibus</be>
   <bg>Vietnamese Fonts and ibus</bg>
   <bn>Vietnamese Fonts and ibus</bn>
   <ca>Lletra tipogràfica i IBUS Vietnamites</ca>
   <cs>Vietnamese Fonts and ibus</cs>
   <da>Vietnamesisk skrifttyper og ibus</da>
   <de>Vietnamesische Schriftzeichen und IBus</de>
   <el>Βιετναμέζικες γραμματοσειρές και ibus</el>
   <en>Vietnamese Fonts and ibus</en>
   <es_ES>Fuentes tipográficas e IBus vietnamitas</es_ES>
   <es>Fuentes tipográficas e IBus vietnamitas</es>
   <et>Vietnamese Fonts and ibus</et>
   <eu>Vietnamese Fonts and ibus</eu>
   <fa>Vietnamese Fonts and ibus</fa>
   <fil_PH>Vietnamese Fonts and ibus</fil_PH>
   <fi>Vietnamilaiset kirjasimet ja ibus</fi>
   <fr_BE>Polices vietnamiennes et ibus</fr_BE>
   <fr>Polices vietnamiennes et ibus</fr>
   <gl_ES>Fontes e ibus vietmanita</gl_ES>
   <gu>Vietnamese Fonts and ibus</gu>
   <he_IL>Vietnamese Fonts and ibus</he_IL>
   <hi>वियतनामी मुद्रलिपि व Ibus</hi>
   <hr>Vietnamese Fonts and ibus</hr>
   <hu>Vietnamese Fonts and ibus</hu>
   <id>Vietnamese Fonts and ibus</id>
   <is>Vietnamese Fonts and ibus</is>
   <it>Fonts e ibus per la lingua vietnamita</it>
   <ja>ベトナム語のフォントと ibus</ja>
   <kk>Vietnamese Fonts and ibus</kk>
   <ko>Vietnamese Fonts and ibus</ko>
   <ku>Vietnamese Fonts and ibus</ku>
   <lt>Vietnamese Fonts and ibus</lt>
   <mk>Vietnamese Fonts and ibus</mk>
   <mr>Vietnamese Fonts and ibus</mr>
   <nb_NO>Vietnamese Fonts and ibus</nb_NO>
   <nb>Vietnamesiske skrifter og ibus</nb>
   <nl_BE>Vietnamese Fonts and ibus</nl_BE>
   <nl>Vietnamese Fonts en ibus</nl>
   <or>Vietnamese Fonts and ibus</or>
   <pl>Wietnamskie fonty i ibus</pl>
   <pt_BR>Fontes e ibus Vietnamita</pt_BR>
   <pt>Vietnamita Fontes e ibus</pt>
   <ro>Vietnamese Fonts and ibus</ro>
   <ru>Vietnamese Fonts and ibus</ru>
   <sk>Vietnamese Fonts and ibus</sk>
   <sl>Vietnamske pisave in ibus</sl>
   <so>Vietnamese Fonts and ibus</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në vietnamisht</sq>
   <sr>Vietnamese Fonts and ibus</sr>
   <sv>Vietnamesiska Typsnitt och ibus</sv>
   <th>ฟอนต์ภาษาเวียดนามและ ibus</th>
   <tr>Vietnamca Yazı Tipi ve ibus</tr>
   <uk>Vietnamese Fonts and ibus</uk>
   <vi>Vietnamese Fonts and ibus</vi>
   <zh_CN>Vietnamese Fonts and ibus</zh_CN>
   <zh_HK>Vietnamese Fonts and ibus</zh_HK>
   <zh_TW>Vietnamese Fonts and ibus</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
im-config
ibus-unikey
ibus-table-viqr
ibus-gtk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
im-config
ibus-unikey
ibus-table-viqr
ibus-gtk
</uninstall_package_names>

</app>
