<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Falkon
</name>

<description>
   <am>Latest Falkon lightweight browser</am>
   <ar>Latest Falkon lightweight browser</ar>
   <be>Latest Falkon lightweight browser</be>
   <bg>Latest Falkon lightweight browser</bg>
   <bn>Latest Falkon lightweight browser</bn>
   <ca>El darrer navegador lleuger Falkon</ca>
   <cs>Latest Falkon lightweight browser</cs>
   <da>Seneste Falkon letvægtsbrowser</da>
   <de>Neuester Falkon leichtgewichtiger Browser</de>
   <el>Το τελευταίο ελαφρύ πρόγραμμα περιήγησης Falkon</el>
   <en>Latest Falkon lightweight browser</en>
   <es_ES>Último navegador ligero Falkon</es_ES>
   <es>Último navegador ligero Falkon</es>
   <et>Latest Falkon lightweight browser</et>
   <eu>Latest Falkon lightweight browser</eu>
   <fa>Latest Falkon lightweight browser</fa>
   <fil_PH>Latest Falkon lightweight browser</fil_PH>
   <fi>Viimeisin Falkon-selain, keveä</fi>
   <fr_BE>Dernière version du navigateur léger Falkon</fr_BE>
   <fr>Dernière version du navigateur léger Falkon</fr>
   <gl_ES>Versión máis recente do navegador lixeiro Falkon</gl_ES>
   <gu>Latest Falkon lightweight browser</gu>
   <he_IL>Latest Falkon lightweight browser</he_IL>
   <hi>नवीनतम Falkon सरल ब्राउज़र</hi>
   <hr>Latest Falkon lightweight browser</hr>
   <hu>Latest Falkon lightweight browser</hu>
   <id>Latest Falkon lightweight browser</id>
   <is>Latest Falkon lightweight browser</is>
   <it>Ultimo browser leggero Falkon</it>
   <ja>最新の Falkon 軽量ブラウザー</ja>
   <kk>Latest Falkon lightweight browser</kk>
   <ko>Latest Falkon lightweight browser</ko>
   <ku>Latest Falkon lightweight browser</ku>
   <lt>Latest Falkon lightweight browser</lt>
   <mk>Latest Falkon lightweight browser</mk>
   <mr>Latest Falkon lightweight browser</mr>
   <nb_NO>Latest Falkon lightweight browser</nb_NO>
   <nb>Seneste Falkon-nettleser (lettvekt)</nb>
   <nl_BE>Latest Falkon lightweight browser</nl_BE>
   <nl>Nieuwste Falkon lichtgewicht browser</nl>
   <or>Latest Falkon lightweight browser</or>
   <pl>najnowsza lekka przeglądarka Falkon</pl>
   <pt_BR>Falkon - Versão mais recente do navegador de internet leve e rápido</pt_BR>
   <pt>Versão mais recente do navegador ligeiro Falkon</pt>
   <ro>Latest Falkon lightweight browser</ro>
   <ru>Свежий релиз облегченного браузера Falkon</ru>
   <sk>Latest Falkon lightweight browser</sk>
   <sl>Zadnja različica lahkega brskalnika Falkon</sl>
   <so>Latest Falkon lightweight browser</so>
   <sq>Shfletuesi më i ri i peshës së lehtë, Falkon</sq>
   <sr>Latest Falkon lightweight browser</sr>
   <sv>Senaste Falkon lättviktswebbläsare</sv>
   <th>เบราว์เซอร์น้ำหนักเบา Falkon ล่าสุด</th>
   <tr>En son Falcon hafif tarayıcı</tr>
   <uk>Latest Falkon lightweight browser</uk>
   <vi>Latest Falkon lightweight browser</vi>
   <zh_CN>最新版 Falkon 轻量级浏览器</zh_CN>
   <zh_HK>Latest Falkon lightweight browser</zh_HK>
   <zh_TW>Latest Falkon lightweight browser</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>https://screenshots.debian.net/screenshots/000/017/192/large.png</screenshot>

<preinstall>

</preinstall>

<install_package_names>
falkon
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
falkon
</uninstall_package_names>
</app>
