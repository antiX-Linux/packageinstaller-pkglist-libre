<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Romanian_LO_Latest_full
</name>

<description>
   <am>Romanian Language Meta-Package for LibreOffice</am>
   <ar>Romanian Language Meta-Package for LibreOffice</ar>
   <be>Romanian Language Meta-Package for LibreOffice</be>
   <bg>Romanian Language Meta-Package for LibreOffice</bg>
   <bn>Romanian Language Meta-Package for LibreOffice</bn>
   <ca>Meta-paquet de llengua per LibreOffice en Romanès</ca>
   <cs>Romanian Language Meta-Package for LibreOffice</cs>
   <da>Rumænsk sprog-metapakke til LibreOffice</da>
   <de>Rumänisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Ρουμανικά</el>
   <en>Romanian Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de Idioma Rumano para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Rumano para LibreOffice</es>
   <et>Romanian Language Meta-Package for LibreOffice</et>
   <eu>Romanian Language Meta-Package for LibreOffice</eu>
   <fa>Romanian Language Meta-Package for LibreOffice</fa>
   <fil_PH>Romanian Language Meta-Package for LibreOffice</fil_PH>
   <fi>Romanialainen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-paquet langue roumaine pour LibreOffice</fr_BE>
   <fr>Méta-paquet langue roumaine pour LibreOffice</fr>
   <gl_ES>Romanés Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Romanian Language Meta-Package for LibreOffice</gu>
   <he_IL>Romanian Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु रोमानियाई भाषा मेटा-पैकेज</hi>
   <hr>Romanian Language Meta-Package for LibreOffice</hr>
   <hu>Romanian Language Meta-Package for LibreOffice</hu>
   <id>Romanian Language Meta-Package for LibreOffice</id>
   <is>Romanian Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua rumena per LibreOffice</it>
   <ja>LibreOffice用ルーマニア語メタパッケージ</ja>
   <kk>Romanian Language Meta-Package for LibreOffice</kk>
   <ko>Romanian Language Meta-Package for LibreOffice</ko>
   <ku>Romanian Language Meta-Package for LibreOffice</ku>
   <lt>Romanian Language Meta-Package for LibreOffice</lt>
   <mk>Romanian Language Meta-Package for LibreOffice</mk>
   <mr>Romanian Language Meta-Package for LibreOffice</mr>
   <nb_NO>Romanian Language Meta-Package for LibreOffice</nb_NO>
   <nb>Romansk språkpakke for LibreOffice</nb>
   <nl_BE>Romanian Language Meta-Package for LibreOffice</nl_BE>
   <nl>Roemeense Taal Meta-Pakket voor LibreOffice</nl>
   <or>Romanian Language Meta-Package for LibreOffice</or>
   <pl>Rumuński metapakiet językowy dla LibreOffice</pl>
   <pt_BR>Romeno Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Romeno Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Romanian Language Meta-Package for LibreOffice</ro>
   <ru>Romanian Language Meta-Package for LibreOffice</ru>
   <sk>Romanian Language Meta-Package for LibreOffice</sk>
   <sl>Romunski jezikovni meta-paket za LibreOffice</sl>
   <so>Romanian Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në rumanisht</sq>
   <sr>Romanian Language Meta-Package for LibreOffice</sr>
   <sv>Rumänskt Språk Meta-Paket för LibreOffice </sv>
   <th>Meta-Package ภาษา Romanian สำหรับ LibreOffice</th>
   <tr>LibreOffice için Rumence Dili Üst-Paketi</tr>
   <uk>Romanian Language Meta-Package for LibreOffice</uk>
   <vi>Romanian Language Meta-Package for LibreOffice</vi>
   <zh_CN>Romanian Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Romanian Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Romanian Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-ro
libreoffice-java-common
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-java-common
libreoffice-gtk3
libreoffice-l10n-ro
</uninstall_package_names>

</app>
