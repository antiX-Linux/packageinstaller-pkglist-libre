<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Albanian_Default_Firefox_esr
</name>

<description>
   <am>Albanian localisation of Firefox ESR</am>
   <ar>Albanian localisation of Firefox ESR</ar>
   <be>Albanian localisation of Firefox ESR</be>
   <bg>Albanian localisation of Firefox ESR</bg>
   <bn>Albanian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Albanès</ca>
   <cs>Albanian localisation of Firefox ESR</cs>
   <da>Albanian localisation of Firefox ESR</da>
   <de>Albanische Lokalisation von “Firefox ESR”</de>
   <el>Αλβανικά για το Firefox ESR</el>
   <en>Albanian localisation of Firefox ESR</en>
   <es_ES>Localización albanés de Firefox ESR</es_ES>
   <es>Localización Albanés de Firefox ESR</es>
   <et>Albanian localisation of Firefox ESR</et>
   <eu>Albanian localisation of Firefox ESR</eu>
   <fa>Albanian localisation of Firefox ESR</fa>
   <fil_PH>Albanian localisation of Firefox ESR</fil_PH>
   <fi>Albanian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en albanais pour Firefox ESR</fr_BE>
   <fr>Localisation en albanais pour Firefox ESR</fr>
   <gl_ES>Albanian localisation of Firefox ESR</gl_ES>
   <gu>Albanian localisation of Firefox ESR</gu>
   <he_IL>Albanian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का अल्बानियाई संस्करण</hi>
   <hr>Albanian localisation of Firefox ESR</hr>
   <hu>Albanian localisation of Firefox ESR</hu>
   <id>Albanian localisation of Firefox ESR</id>
   <is>Albanian localisation of Firefox ESR</is>
   <it>Localizzazione albanian di Firefox ESR</it>
   <ja>Firefox ESR アルバニア語版</ja>
   <kk>Albanian localisation of Firefox ESR</kk>
   <ko>Albanian localisation of Firefox ESR</ko>
   <ku>Albanian localisation of Firefox ESR</ku>
   <lt>Albanian localisation of Firefox ESR</lt>
   <mk>Albanian localisation of Firefox ESR</mk>
   <mr>Albanian localisation of Firefox ESR</mr>
   <nb_NO>Albanian localisation of Firefox ESR</nb_NO>
   <nb>Albansk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Albanian localisation of Firefox ESR</nl_BE>
   <nl>Albanese lokalisatie van Firefox ESR</nl>
   <or>Albanian localisation of Firefox ESR</or>
   <pl>Albanian localisation of Firefox ESR</pl>
   <pt_BR>Albanês Localização para o Firefox ESR</pt_BR>
   <pt>Albanês Localização para Firefox ESR</pt>
   <ro>Albanian localisation of Firefox ESR</ro>
   <ru>Albanian localisation of Firefox ESR</ru>
   <sk>Albanian localisation of Firefox ESR</sk>
   <sl>Albanske krajevne nastavitve za Firefox ESR</sl>
   <so>Albanian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në shqip</sq>
   <sr>Albanian localisation of Firefox ESR</sr>
   <sv>Albansk lokalisering av Firefox-ESR</sv>
   <th>Albanian localisation of Firefox ESR</th>
   <tr>Firefox ESR Arnavutça yerelleştirmesi</tr>
   <uk>Albanian localisation of Firefox ESR</uk>
   <vi>Albanian localisation of Firefox ESR</vi>
   <zh_CN>Albanian localisation of Firefox ESR</zh_CN>
   <zh_HK>Albanian localisation of Firefox ESR</zh_HK>
   <zh_TW>Albanian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-sq
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-sq
</uninstall_package_names>

</app>
