<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Themes
</category>

<name>
Adapta Gtk Theme
</name>

<description>
   <am>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</am>
   <ar>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</ar>
   <be>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</be>
   <bg>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</bg>
   <bn>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</bn>
   <ca>Un tema modern Gtk en variants clara i fosca, amb ginys de realçat blau neó</ca>
   <cs>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</cs>
   <da>Et moderne fladt Gtk-tema i lyse og mørke varianter, med widgets med neonblå højlys</da>
   <de>Ein modernes flaches Gtk-Thema in hellen und dunklen Varianten, mit Widgets mit neonblauen Akzenten</de>
   <el>Μοντέρνο, επίπεδο Gtk θέμα σε φωτεινή και σκοτεινή παραλλαγή, με widgets που έχουν μπλε φωτεινά χρώματα</el>
   <en>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</en>
   <es_ES>Un tema moderno, plano de GTK en variantes claro y oscuro, con widgets resaltados con neón azul</es_ES>
   <es>Un tema GTK moderno y plano, en variantes claro y oscuro, con widgets resaltados con neón azul</es>
   <et>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</et>
   <eu>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</eu>
   <fa>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</fa>
   <fil_PH>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</fil_PH>
   <fi>nykyaikainen litteä Gtk-teema kirkkaan ja tumman muunnelman kanssa, mukana myös vimpaimia jotka tuovat neonsiniset korostukset</fi>
   <fr_BE>Thème contemporain Gtk Flat en versions claires et sombres, avec widgets aux éclairages bleus neon</fr_BE>
   <fr>Thème contemporain Gtk Flat en versions claires et sombres, avec widgets aux éclairages bleus neon</fr>
   <gl_ES>Tema moderno e simple para Gtk, nas variantes claro e escuro, con elementos ornamentados a azul neón</gl_ES>
   <gu>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</gu>
   <he_IL>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</he_IL>
   <hi>नीयन नीले सीमान्त युक्त गहरे व हल्के रंग में उपलब्ध एक आधुनिक, सपाट जीटीके थीम</hi>
   <hr>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</hr>
   <hu>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</hu>
   <id>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</id>
   <is>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</is>
   <it>Un moderno tema Gtk in varianti chiare e scure, con widget a luci neon blu</it>
   <ja>モダンでフラットな Gtk テーマにはライトとダークのバリエーションがあり、ウィジェットはネオンブルーのハイライトがあります</ja>
   <kk>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</kk>
   <ko>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</ko>
   <ku>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</ku>
   <lt>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</lt>
   <mk>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</mk>
   <mr>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</mr>
   <nb_NO>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</nb_NO>
   <nb>et moderne, flatt Gtk-tema i lys og mørk utgave der skjermelementene har neonblå uthevinger</nb>
   <nl_BE>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</nl_BE>
   <nl>een modern plat Gtk Thema in lichte en donkere varianten, met widgets die neon blauwe highlights hebben</nl>
   <or>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</or>
   <pl>nowoczesny płaski motyw Gtk w jasnych i ciemnych wariantach, z widżetami o neonowo niebieskich wyróżnieniach</pl>
   <pt_BR>Tema moderno e simples para Gtk, nas variantes 'claro' e 'escuro', com elementos ornamentais em azul neon</pt_BR>
   <pt>Tema moderno e simples para Gtk, nas variantes 'claro' e 'escuro', com elementos ornamentados a azul neon</pt>
   <ro>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</ro>
   <ru>Современная плоска тема GTK в светлом и темном оформлениях, с виджетами с неоновыми акцентами</ru>
   <sk>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</sk>
   <sl>sodobna in čista Gtk tema v temni in svetli izvedbi z gradniki, ki imajo neonsko modre poudarke</sl>
   <so>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</so>
   <sq>Një Temë Gtk moderne, e sheshtë, në variante të çelët dhe të errët, me widget-e që kanë theksim me të kaltër neon</sq>
   <sr>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</sr>
   <sv>ett modernt platt Gtk-tema i ljus eller mörk variant och widgets med neonblå reflexer</sv>
   <th>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</th>
   <tr>Açık ve koyu biçimlerde, neon mavisi vurgulara sahip uygulama parçacıklarıyla modern bir düz Gtk Teması</tr>
   <uk>сучасна пласка Gtk-тема у світлому і темному варіантах з блакитно-неоновою підсвіткою віджетів</uk>
   <vi>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</vi>
   <zh_CN>一个现代化的偏平 Gtk 主题，构建有霓虹蓝色高亮</zh_CN>
   <zh_HK>a modern flat Gtk Theme in light and dark variants, with widgets having neon blue highlights</zh_HK>
   <zh_TW>具有淺色和深色變體的現代扁平Gtk主題，其小部件具有霓虹燈藍色亮點</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
adapta-gtk-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
adapta-gtk-theme
</uninstall_package_names>
</app>
