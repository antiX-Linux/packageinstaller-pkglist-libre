<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Graphics
</category>

<name>
font-manager
</name>

<description>
   <am>font management application from the GNOME desktop</am>
   <ar>font management application from the GNOME desktop</ar>
   <be>font management application from the GNOME desktop</be>
   <bg>font management application from the GNOME desktop</bg>
   <bn>font management application from the GNOME desktop</bn>
   <ca>aplicació de gestió de fonts de l'escriptori GNOME</ca>
   <cs>font management application from the GNOME desktop</cs>
   <da>font management application from the GNOME desktop</da>
   <de>Schriftenverwaltung der GNOME Oberfläche</de>
   <el>Εφαρμογή διαχείρισης γραμματοσειρών από την επιφάνεια εργασίας του GNOME</el>
   <en>font management application from the GNOME desktop</en>
   <es_ES>Aplicación de gestión de fuentes desde el escritorio de GNOME</es_ES>
   <es>Aplicación de gestión de fuentes desde el escritorio de GNOME</es>
   <et>font management application from the GNOME desktop</et>
   <eu>font management application from the GNOME desktop</eu>
   <fa>font management application from the GNOME desktop</fa>
   <fil_PH>font management application from the GNOME desktop</fil_PH>
   <fi>kirjasimien hallintaan tarkoitettu sovellus GNOME-työpöytää kehittävältä ryhmältä</fi>
   <fr_BE>Application de gestion de polices du bureau GNOME</fr_BE>
   <fr>Application de gestion de polices du bureau GNOME</fr>
   <gl_ES>Aplicativo de xestión de fontes, do escritorio GNOME</gl_ES>
   <gu>font management application from the GNOME desktop</gu>
   <he_IL>font management application from the GNOME desktop</he_IL>
   <hi>गनोम डेस्कटॉप द्वारा मुद्रलिपि प्रबंधन अनुप्रयोग</hi>
   <hr>font management application from the GNOME desktop</hr>
   <hu>font management application from the GNOME desktop</hu>
   <id>font management application from the GNOME desktop</id>
   <is>font management application from the GNOME desktop</is>
   <it>applicazione per la gestione dei font da GNOME desktop</it>
   <ja>GNOME デスクトップ用のフォント管理アプリ</ja>
   <kk>font management application from the GNOME desktop</kk>
   <ko>font management application from the GNOME desktop</ko>
   <ku>font management application from the GNOME desktop</ku>
   <lt>font management application from the GNOME desktop</lt>
   <mk>font management application from the GNOME desktop</mk>
   <mr>font management application from the GNOME desktop</mr>
   <nb_NO>font management application from the GNOME desktop</nb_NO>
   <nb>behandling av skrifttyper i skrivebordsmiljøet GNOME</nb>
   <nl_BE>font management application from the GNOME desktop</nl_BE>
   <nl>font beheer applicatie vanuit de GNOME desktop</nl>
   <or>font management application from the GNOME desktop</or>
   <pl>aplikacja do zarządzania czcionkami ze środowiska GNOME</pl>
   <pt_BR>Aplicativo de gerenciamento de fontes do ambiente de trabalho GNOME</pt_BR>
   <pt>Aplicação de gestão de fontes, do ambiente de trabalho GNOME</pt>
   <ro>font management application from the GNOME desktop</ro>
   <ru>Менеджер шрифтов из состава GNOME</ru>
   <sk>font management application from the GNOME desktop</sk>
   <sl>aplikacija za upravljanje slogov pisav za GNOME namizje</sl>
   <so>font management application from the GNOME desktop</so>
   <sq>Aplikacion administrimi shkronjash për desktopin GNOME</sq>
   <sr>font management application from the GNOME desktop</sr>
   <sv>stilhanteringsprogram från GNOME skrivbordsmiljö</sv>
   <th>แอปพลิเคชันจัดการฟอนต์สำหรับเดสก์ท็อป GNOME</th>
   <tr>GNOME masaüstünden yazı tipi yönetim uygulaması</tr>
   <uk>font management application from the GNOME desktop</uk>
   <vi>font management application from the GNOME desktop</vi>
   <zh_CN>来自 GNOME 桌面的字体管理应用</zh_CN>
   <zh_HK>font management application from the GNOME desktop</zh_HK>
   <zh_TW>font management application from the GNOME desktop</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
font-manager
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
font-manager
</uninstall_package_names>
</app>
