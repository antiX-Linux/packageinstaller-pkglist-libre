<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Icons
</category>

<name>
Faba Icon Colors
</name>

<description>
   <am>a Faba icon theme with many colour choices</am>
   <ar>a Faba icon theme with many colour choices</ar>
   <be>a Faba icon theme with many colour choices</be>
   <bg>a Faba icon theme with many colour choices</bg>
   <bn>a Faba icon theme with many colour choices</bn>
   <ca>un tema d'icones Faba amb moltes opcions de color</ca>
   <cs>a Faba icon theme with many colour choices</cs>
   <da>et Faba-ikontema med mange farvevalg</da>
   <de>Ein Faba-Icon-Thema mit vielen Farbvarianten</de>
   <el>Το θέμα εικονίδια Faba με πολλές επιλογές χρωμάτων</el>
   <en>a Faba icon theme with many colour choices</en>
   <es_ES>Tema de íconos Faba con colores a elección</es_ES>
   <es>Tema de íconos Faba con colores a elección</es>
   <et>a Faba icon theme with many colour choices</et>
   <eu>a Faba icon theme with many colour choices</eu>
   <fa>a Faba icon theme with many colour choices</fa>
   <fil_PH>a Faba icon theme with many colour choices</fil_PH>
   <fi>Faba-kuvaketeema monine värivaihtoehtoineen</fi>
   <fr_BE>Jeu d'icônes Faba aux multiples choix de couleurs</fr_BE>
   <fr>Jeu d'icônes Faba aux multiples choix de couleurs</fr>
   <gl_ES>Tema de iconas Faba con moitas opcións de cores</gl_ES>
   <gu>a Faba icon theme with many colour choices</gu>
   <he_IL>a Faba icon theme with many colour choices</he_IL>
   <hi>अनेक रंगों के विकल्पों से युक्त Faba आइकन थीम</hi>
   <hr>a Faba icon theme with many colour choices</hr>
   <hu>a Faba icon theme with many colour choices</hu>
   <id>a Faba icon theme with many colour choices</id>
   <is>a Faba icon theme with many colour choices</is>
   <it>Tema di icone Faba con molte scelte di colore</it>
   <ja>多くの色の選択肢がある Faba アイコンテーマ</ja>
   <kk>a Faba icon theme with many colour choices</kk>
   <ko>a Faba icon theme with many colour choices</ko>
   <ku>a Faba icon theme with many colour choices</ku>
   <lt>a Faba icon theme with many colour choices</lt>
   <mk>a Faba icon theme with many colour choices</mk>
   <mr>a Faba icon theme with many colour choices</mr>
   <nb_NO>a Faba icon theme with many colour choices</nb_NO>
   <nb>Faba ikontema med mange fargevalg</nb>
   <nl_BE>a Faba icon theme with many colour choices</nl_BE>
   <nl>een Faba icoon thema met veel kleurkeuzes</nl>
   <or>a Faba icon theme with many colour choices</or>
   <pl>motyw ikon Faba z dużym wyborem kolorów</pl>
   <pt_BR>Faba - Tema de ícones com muitas opções de cores</pt_BR>
   <pt>Tema de ícones Faba com muitas opções de cores</pt>
   <ro>a Faba icon theme with many colour choices</ro>
   <ru>Тема иконок Faba с множеством цветовых вариантов</ru>
   <sk>a Faba icon theme with many colour choices</sk>
   <sl>Faba tema za ikone, z mnogimi izbori barv</sl>
   <so>a Faba icon theme with many colour choices</so>
   <sq>Një temë ikonash Faba me shumë zgjedhje për ngjyrat</sq>
   <sr>a Faba icon theme with many colour choices</sr>
   <sv>ett Faba ikontema med många färgval</sv>
   <th>a Faba icon theme with many colour choices</th>
   <tr>bazı renk seçenekleri ile bir Faba simge teması</tr>
   <uk>тема піктограм Faba з багатим набором кольорів</uk>
   <vi>a Faba icon theme with many colour choices</vi>
   <zh_CN>Faba 图标主题，有多种颜色可选</zh_CN>
   <zh_HK>a Faba icon theme with many colour choices</zh_HK>
   <zh_TW>a Faba icon theme with many colour choices</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
faba-colors-icon-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
faba-colors-icon-theme
</uninstall_package_names>
</app>
