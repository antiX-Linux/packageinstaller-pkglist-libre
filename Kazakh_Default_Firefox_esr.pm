<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Kazakh_Default_Firefox_esr
</name>

<description>
   <am>Kazakh localisation of Firefox ESR</am>
   <ar>Kazakh localisation of Firefox ESR</ar>
   <be>Kazakh localisation of Firefox ESR</be>
   <bg>Kazakh localisation of Firefox ESR</bg>
   <bn>Kazakh localisation of Firefox ESR</bn>
   <ca>Localització en Kazakh de Firefox ESR</ca>
   <cs>Kazakh localisation of Firefox ESR</cs>
   <da>Kazakh localisation of Firefox ESR</da>
   <de>Kazakh localisation of Firefox ESR</de>
   <el>Καζακστάν για Firefox ESR</el>
   <en>Kazakh localisation of Firefox ESR</en>
   <es_ES>Localización en kazajo de Firefox ESR</es_ES>
   <es>Localización en kazajo de Firefox ESR</es>
   <et>Kazakh localisation of Firefox ESR</et>
   <eu>Kazakh localisation of Firefox ESR</eu>
   <fa>Kazakh localisation of Firefox ESR</fa>
   <fil_PH>Kazakh localisation of Firefox ESR</fil_PH>
   <fi>Kazakh localisation of Firefox ESR</fi>
   <fr_BE>Localisation kazakhe pour Firefox ESR</fr_BE>
   <fr>Localisation kazakhe pour Firefox ESR</fr>
   <gl_ES>Kazakh localisation of Firefox ESR</gl_ES>
   <gu>Kazakh localisation of Firefox ESR</gu>
   <he_IL>Kazakh localisation of Firefox ESR</he_IL>
   <hi>Kazakh localisation of Firefox ESR</hi>
   <hr>Kazakh localisation of Firefox ESR</hr>
   <hu>Kazakh localisation of Firefox ESR</hu>
   <id>Kazakh localisation of Firefox ESR</id>
   <is>Kazakh localisation of Firefox ESR</is>
   <it>Localizzazione Kazakh di Firefox ESR</it>
   <ja>Kazakh localisation of Firefox ESR</ja>
   <kk>Kazakh localisation of Firefox ESR</kk>
   <ko>Kazakh localisation of Firefox ESR</ko>
   <ku>Kazakh localisation of Firefox ESR</ku>
   <lt>Kazakh localisation of Firefox ESR</lt>
   <mk>Kazakh localisation of Firefox ESR</mk>
   <mr>Kazakh localisation of Firefox ESR</mr>
   <nb_NO>Kazakh localisation of Firefox ESR</nb_NO>
   <nb>Kazakh lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Kazakh localisation of Firefox ESR</nl_BE>
   <nl>Kazakh localisation of Firefox ESR</nl>
   <or>Kazakh localisation of Firefox ESR</or>
   <pl>Kazakh localisation of Firefox ESR</pl>
   <pt_BR>Cazaque Localização para o Firefox ESR</pt_BR>
   <pt>Cazaque Localização para Firefox ESR</pt>
   <ro>Kazakh localisation of Firefox ESR</ro>
   <ru>Kazakh localisation of Firefox ESR</ru>
   <sk>Kazakh localisation of Firefox ESR</sk>
   <sl>Kazahske krajevne nastavitve za Firefox ESR</sl>
   <so>Kazakh localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në kazakistanisht</sq>
   <sr>Kazakh localisation of Firefox ESR</sr>
   <sv>Kazakisk lokalisering av Firefox ESR</sv>
   <th>Kazakh localisation of Firefox ESR</th>
   <tr>Kazakh localisation of Firefox ESR</tr>
   <uk>Kazakh localisation of Firefox ESR</uk>
   <vi>Kazakh localisation of Firefox ESR</vi>
   <zh_CN>Kazakh localisation of Firefox ESR</zh_CN>
   <zh_HK>Kazakh localisation of Firefox ESR</zh_HK>
   <zh_TW>Kazakh localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-kk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-kk
</uninstall_package_names>

</app>
