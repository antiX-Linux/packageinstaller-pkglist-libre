<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hebrew_Default_Firefox_esr
</name>

<description>
   <am>Hebrew localisation of Firefox ESR</am>
   <ar>Hebrew localisation of Firefox ESR</ar>
   <be>Hebrew localisation of Firefox ESR</be>
   <bg>Hebrew localisation of Firefox ESR</bg>
   <bn>Hebrew localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Hebreu</ca>
   <cs>Hebrew localisation of Firefox ESR</cs>
   <da>Hebrew localisation of Firefox ESR</da>
   <de>Hebräische Lokalisation von “Firefox ESR”</de>
   <el>Εβραϊκά για Firefox ESR</el>
   <en>Hebrew localisation of Firefox ESR</en>
   <es_ES>Localización Hebreo de Firefox ESR</es_ES>
   <es>Localización Hebreo de Firefox ESR</es>
   <et>Hebrew localisation of Firefox ESR</et>
   <eu>Hebrew localisation of Firefox ESR</eu>
   <fa>Hebrew localisation of Firefox ESR</fa>
   <fil_PH>Hebrew localisation of Firefox ESR</fil_PH>
   <fi>Hebrew localisation of Firefox ESR</fi>
   <fr_BE>Localisation en hébreu pour Firefox ESR</fr_BE>
   <fr>Localisation en hébreu pour Firefox ESR</fr>
   <gl_ES>Hebrew localisation of Firefox ESR</gl_ES>
   <gu>Hebrew localisation of Firefox ESR</gu>
   <he_IL>Hebrew localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का हिब्रू संस्करण</hi>
   <hr>Hebrew localisation of Firefox ESR</hr>
   <hu>Hebrew localisation of Firefox ESR</hu>
   <id>Hebrew localisation of Firefox ESR</id>
   <is>Hebrew localisation of Firefox ESR</is>
   <it>Localizzazione ebraica di Firefox ESR</it>
   <ja>ヘブライ語版 Firefox ESR</ja>
   <kk>Hebrew localisation of Firefox ESR</kk>
   <ko>Hebrew localisation of Firefox ESR</ko>
   <ku>Hebrew localisation of Firefox ESR</ku>
   <lt>Hebrew localisation of Firefox ESR</lt>
   <mk>Hebrew localisation of Firefox ESR</mk>
   <mr>Hebrew localisation of Firefox ESR</mr>
   <nb_NO>Hebrew localisation of Firefox ESR</nb_NO>
   <nb>Hebraisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Hebrew localisation of Firefox ESR</nl_BE>
   <nl>Hebrew localisation of Firefox ESR</nl>
   <or>Hebrew localisation of Firefox ESR</or>
   <pl>Hebrew localisation of Firefox ESR</pl>
   <pt_BR>Hebraico Localização para o Firefox ESR</pt_BR>
   <pt>Hebreu Localização para Firefox ESR</pt>
   <ro>Hebrew localisation of Firefox ESR</ro>
   <ru>Hebrew localisation of Firefox ESR</ru>
   <sk>Hebrew localisation of Firefox ESR</sk>
   <sl>Hebrejske krajevne nastavitve za Firefox ESR</sl>
   <so>Hebrew localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në hebraisht</sq>
   <sr>Hebrew localisation of Firefox ESR</sr>
   <sv>Hebreisk lokalisering av Firefox ESR</sv>
   <th>Hebrew localisation of Firefox ESR</th>
   <tr>Firefox ESR İbranice yerelleştirmesi</tr>
   <uk>Hebrew localisation of Firefox ESR</uk>
   <vi>Hebrew localisation of Firefox ESR</vi>
   <zh_CN>Hebrew localisation of Firefox ESR</zh_CN>
   <zh_HK>Hebrew localisation of Firefox ESR</zh_HK>
   <zh_TW>Hebrew localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-he
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-he
</uninstall_package_names>

</app>
