<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Portugese(PT) Thunderbird
</name>

<description>
   <am>Portuguese(PT) localisation of Thunderbird</am>
   <ar>Portuguese(PT) localisation of Thunderbird</ar>
   <be>Portuguese(PT) localisation of Thunderbird</be>
   <bg>Portuguese(PT) localisation of Thunderbird</bg>
   <bn>Portuguese(PT) localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Portuguès (PT)</ca>
   <cs>Portuguese(PT) localisation of Thunderbird</cs>
   <da>Portuguese(PT) localisation of Thunderbird</da>
   <de>Portugiesische (PT) Lokalisation von “Thunderbird”</de>
   <el>Πορτογαλικά (PT) εντοπισμός του Thunderbird</el>
   <en>Portuguese(PT) localisation of Thunderbird</en>
   <es_ES>Localización Portugués (PT) de Thunderbird</es_ES>
   <es>Localización Portugués (PT) de Thunderbird</es>
   <et>Portuguese(PT) localisation of Thunderbird</et>
   <eu>Portuguese(PT) localisation of Thunderbird</eu>
   <fa>Portuguese(PT) localisation of Thunderbird</fa>
   <fil_PH>Portuguese(PT) localisation of Thunderbird</fil_PH>
   <fi>Portuguese(PT) localisation of Thunderbird</fi>
   <fr_BE>Localisation en portugais pour Thunderbird</fr_BE>
   <fr>Localisation en portugais pour Thunderbird</fr>
   <gl_ES>Portuguese(PT) localisation of Thunderbird</gl_ES>
   <gu>Portuguese(PT) localisation of Thunderbird</gu>
   <he_IL>Portuguese(PT) localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का पुर्तगाली(पुर्तगाल) संस्करण</hi>
   <hr>Portuguese(PT) localisation of Thunderbird</hr>
   <hu>Portuguese(PT) localisation of Thunderbird</hu>
   <id>Portuguese(PT) localisation of Thunderbird</id>
   <is>Portuguese(PT) localisation of Thunderbird</is>
   <it>Localizzazione portoghese(PT) di Thunderbird</it>
   <ja>Thunderbird のポルトガル語（ポルトガル）版</ja>
   <kk>Portuguese(PT) localisation of Thunderbird</kk>
   <ko>Portuguese(PT) localisation of Thunderbird</ko>
   <ku>Portuguese(PT) localisation of Thunderbird</ku>
   <lt>Portuguese(PT) localisation of Thunderbird</lt>
   <mk>Portuguese(PT) localisation of Thunderbird</mk>
   <mr>Portuguese(PT) localisation of Thunderbird</mr>
   <nb_NO>Portuguese(PT) localisation of Thunderbird</nb_NO>
   <nb>Portugisisk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Portuguese(PT) localisation of Thunderbird</nl_BE>
   <nl>Portuguese(PT) localisation of Thunderbird</nl>
   <or>Portuguese(PT) localisation of Thunderbird</or>
   <pl>Portuguese(PT) localisation of Thunderbird</pl>
   <pt_BR>Português (PT) Localização para o Thunderbird</pt_BR>
   <pt>Português (PT) Localização para Thunderbird</pt>
   <ro>Portuguese(PT) localisation of Thunderbird</ro>
   <ru>Portuguese(PT) localisation of Thunderbird</ru>
   <sk>Portuguese(PT) localisation of Thunderbird</sk>
   <sl>Portugalske (PT) krajevne nastavitve za Thunderbird</sl>
   <so>Portuguese(PT) localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në portugalisht(PT)</sq>
   <sr>Portuguese(PT) localisation of Thunderbird</sr>
   <sv>Portugisisk (PT) lokalisering för Thunderbird</sv>
   <th>Portuguese(PT) localisation of Thunderbird</th>
   <tr>Thunderbird'ün Portekiz Portekizcesi yerelleştirmesi</tr>
   <uk>Portuguese(PT) localisation of Thunderbird</uk>
   <vi>Portuguese(PT) localisation of Thunderbird</vi>
   <zh_CN>Portuguese(PT) localisation of Thunderbird</zh_CN>
   <zh_HK>Portuguese(PT) localisation of Thunderbird</zh_HK>
   <zh_TW>Portuguese(PT) localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-pt-pt
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-pt-pt
</uninstall_package_names>

</app>
