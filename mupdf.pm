<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Office
</category>

<name>
MuPDF
</name>

<description>
   <am>Lightweight and fast PDF, XPS, and E-book viewer</am>
   <ar>Lightweight and fast PDF, XPS, and E-book viewer</ar>
   <be>Lightweight and fast PDF, XPS, and E-book viewer</be>
   <bg>Lightweight and fast PDF, XPS, and E-book viewer</bg>
   <bn>Lightweight and fast PDF, XPS, and E-book viewer</bn>
   <ca>Visor lleuger i ràpid de PDF, XPS i E-Books</ca>
   <cs>Lightweight and fast PDF, XPS, and E-book viewer</cs>
   <da>Lightweight and fast PDF, XPS, and E-book viewer</da>
   <de>Einfacher und schneller Betrachter für E-Books, PDF- und XPS-Dateien.</de>
   <el>Ελαφρύ και γρήγορο πρόγραμμα προβολής PDF, XPS και E-book</el>
   <en>Lightweight and fast PDF, XPS, and E-book viewer</en>
   <es_ES>Visor ligero y rápido de libros electrónicos, XPS y PDF</es_ES>
   <es>Visor ligero y rápido de libros electrónicos, XPS y PDF</es>
   <et>Lightweight and fast PDF, XPS, and E-book viewer</et>
   <eu>Lightweight and fast PDF, XPS, and E-book viewer</eu>
   <fa>Lightweight and fast PDF, XPS, and E-book viewer</fa>
   <fil_PH>Lightweight and fast PDF, XPS, and E-book viewer</fil_PH>
   <fi>Kevyt sekä nopea PDF, XPS, ja E-kirja -lukuohjelma</fi>
   <fr_BE>Visionneuse PDF, XPS et E-book légère et rapide</fr_BE>
   <fr>Visionneuse PDF, XPS et E-book légère et rapide</fr>
   <gl_ES>Lightweight and fast PDF, XPS, and E-book viewer</gl_ES>
   <gu>Lightweight and fast PDF, XPS, and E-book viewer</gu>
   <he_IL>Lightweight and fast PDF, XPS, and E-book viewer</he_IL>
   <hi>सरल व तीव्र पीडीएफ, एक्सपीएस व ई-पुस्तक प्रदर्शक</hi>
   <hr>Lightweight and fast PDF, XPS, and E-book viewer</hr>
   <hu>Lightweight and fast PDF, XPS, and E-book viewer</hu>
   <id>Lightweight and fast PDF, XPS, and E-book viewer</id>
   <is>Lightweight and fast PDF, XPS, and E-book viewer</is>
   <it>Visualizzatore leggero e veloce di PDF, XPS ed E-book</it>
   <ja>軽量で高速なPDF、XPS、電子書籍ビューア</ja>
   <kk>Lightweight and fast PDF, XPS, and E-book viewer</kk>
   <ko>Lightweight and fast PDF, XPS, and E-book viewer</ko>
   <ku>Lightweight and fast PDF, XPS, and E-book viewer</ku>
   <lt>Lightweight and fast PDF, XPS, and E-book viewer</lt>
   <mk>Lightweight and fast PDF, XPS, and E-book viewer</mk>
   <mr>Lightweight and fast PDF, XPS, and E-book viewer</mr>
   <nb_NO>Lightweight and fast PDF, XPS, and E-book viewer</nb_NO>
   <nb>Lettvektig og raskt visningsprogram for PDF, XPS og e-bøker</nb>
   <nl_BE>Lightweight and fast PDF, XPS, and E-book viewer</nl_BE>
   <nl>Lightweight and fast PDF, XPS, and E-book viewer</nl>
   <or>Lightweight and fast PDF, XPS, and E-book viewer</or>
   <pl>Lightweight and fast PDF, XPS, and E-book viewer</pl>
   <pt_BR>Visualizador de PDF, XPS e E-book leve e rápido</pt_BR>
   <pt>Visualizador leve e rápido para ficheiros PDF, XPS e livros electrónicos</pt>
   <ro>Lightweight and fast PDF, XPS, and E-book viewer</ro>
   <ru>Lightweight and fast PDF, XPS, and E-book viewer</ru>
   <sk>Lightweight and fast PDF, XPS, and E-book viewer</sk>
   <sl>Lahek in hiter pregledovalnik za PDF, XPS in E-knjige</sl>
   <so>Lightweight and fast PDF, XPS, and E-book viewer</so>
   <sq>Parës i peshës së lehtë dhe i shpejtë, për PDF, XPS, dhe E-libra</sq>
   <sr>Lightweight and fast PDF, XPS, and E-book viewer</sr>
   <sv>Lättvikts och snabb PDF, XPS, och E-bok läsare</sv>
   <th>Lightweight and fast PDF, XPS, and E-book viewer</th>
   <tr>Hafif ve hızlı PDF, XPS ve E-kitap görüntüleyici</tr>
   <uk>Lightweight and fast PDF, XPS, and E-book viewer</uk>
   <vi>Lightweight and fast PDF, XPS, and E-book viewer</vi>
   <zh_CN>Lightweight and fast PDF, XPS, and E-book viewer</zh_CN>
   <zh_HK>Lightweight and fast PDF, XPS, and E-book viewer</zh_HK>
   <zh_TW>Lightweight and fast PDF, XPS, and E-book viewer</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
mupdf
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
mupdf
</uninstall_package_names>
</app>
