<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Romansh_Thunderbird
</name>

<description>
   <am>Romansh localisation of Thunderbird</am>
   <ar>Romansh localisation of Thunderbird</ar>
   <be>Romansh localisation of Thunderbird</be>
   <bg>Romansh localisation of Thunderbird</bg>
   <bn>Romansh localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Romanx</ca>
   <cs>Romansh localisation of Thunderbird</cs>
   <da>Romansh localisation of Thunderbird</da>
   <de>Schweizerische Lokalisation für die Sprache “Rätoromanisch” von “Thunderbird”</de>
   <el>Romansh για Thunderbird</el>
   <en>Romansh localisation of Thunderbird</en>
   <es_ES>Localización Romanche de Thunderbird</es_ES>
   <es>Localización Romanche de Thunderbird</es>
   <et>Romansh localisation of Thunderbird</et>
   <eu>Romansh localisation of Thunderbird</eu>
   <fa>Romansh localisation of Thunderbird</fa>
   <fil_PH>Romansh localisation of Thunderbird</fil_PH>
   <fi>Romansh localisation of Thunderbird</fi>
   <fr_BE>Localisation en romanche (Suisse - Grisons) pour Thunderbird</fr_BE>
   <fr>Localisation en romanche (Suisse - Grisons) pour Thunderbird</fr>
   <gl_ES>Romansh localisation of Thunderbird</gl_ES>
   <gu>Romansh localisation of Thunderbird</gu>
   <he_IL>Romansh localisation of Thunderbird</he_IL>
   <hi>थंडरबर्ड का रोमांश संस्करण</hi>
   <hr>Romansh localisation of Thunderbird</hr>
   <hu>Romansh localisation of Thunderbird</hu>
   <id>Romansh localisation of Thunderbird</id>
   <is>Romansh localisation of Thunderbird</is>
   <it>Localizzazione in romancio di Thunderbird</it>
   <ja>ロマンシュ語版 Thunderbird</ja>
   <kk>Romansh localisation of Thunderbird</kk>
   <ko>Romansh localisation of Thunderbird</ko>
   <ku>Romansh localisation of Thunderbird</ku>
   <lt>Romansh localisation of Thunderbird</lt>
   <mk>Romansh localisation of Thunderbird</mk>
   <mr>Romansh localisation of Thunderbird</mr>
   <nb_NO>Romansh localisation of Thunderbird</nb_NO>
   <nb>Romansh lokaltilpassing av Thunderbird</nb>
   <nl_BE>Romansh localisation of Thunderbird</nl_BE>
   <nl>Romansh localisation of Thunderbird</nl>
   <or>Romansh localisation of Thunderbird</or>
   <pl>Romansh localisation of Thunderbird</pl>
   <pt_BR>Romanche Localização para o Thunderbird</pt_BR>
   <pt>Romanche Localização para Thunderbird</pt>
   <ro>Romansh localisation of Thunderbird</ro>
   <ru>Romansh localisation of Thunderbird</ru>
   <sk>Romansh localisation of Thunderbird</sk>
   <sl>Retoromanske krajevne nastavitve za Thunderbird</sl>
   <so>Romansh localisation of Thunderbird</so>
   <sq>Romansh localisation of Thunderbird</sq>
   <sr>Romansh localisation of Thunderbird</sr>
   <sv>Romansk lokalisering av Thunderbird</sv>
   <th>Romansh localisation of Thunderbird</th>
   <tr>Thunderbird'ün Romanşça yerelleştirmesi</tr>
   <uk>Romansh localisation of Thunderbird</uk>
   <vi>Romansh localisation of Thunderbird</vi>
   <zh_CN>Romansh localisation of Thunderbird</zh_CN>
   <zh_HK>Romansh localisation of Thunderbird</zh_HK>
   <zh_TW>Romansh localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-rm
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-rm
</uninstall_package_names>

</app>
