<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Spanish_Thunderbird
</name>

<description>
   <am>Spanish localisation of Thunderbird</am>
   <ar>Spanish localisation of Thunderbird</ar>
   <be>Spanish localisation of Thunderbird</be>
   <bg>Spanish localisation of Thunderbird</bg>
   <bn>Spanish localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Castellà</ca>
   <cs>Spanish localisation of Thunderbird</cs>
   <da>Spansk oversættelse af Thunderbird</da>
   <de>Spanische Lokalisierung von Thunderbird</de>
   <el>Ισπανικά για το Thunderbird</el>
   <en>Spanish localisation of Thunderbird</en>
   <es_ES>Localización Española para Thunderbird</es_ES>
   <es>Localización Español para Thunderbird</es>
   <et>Spanish localisation of Thunderbird</et>
   <eu>Spanish localisation of Thunderbird</eu>
   <fa>Spanish localisation of Thunderbird</fa>
   <fil_PH>Spanish localisation of Thunderbird</fil_PH>
   <fi>Espanjalainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en espagnol pour Thunderbird</fr_BE>
   <fr>Localisation en espagnol pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao españo</gl_ES>
   <gu>Spanish localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לספרדית</he_IL>
   <hi>थंडरबर्ड का स्पेनिश संस्करण</hi>
   <hr>Spanish localisation of Thunderbird</hr>
   <hu>Spanish localisation of Thunderbird</hu>
   <id>Spanish localisation of Thunderbird</id>
   <is>Spanish localisation of Thunderbird</is>
   <it>Localizzazione spagnola di Thunderbird</it>
   <ja>Thunderbird のスペイン語版</ja>
   <kk>Spanish localisation of Thunderbird</kk>
   <ko>Spanish localisation of Thunderbird</ko>
   <ku>Spanish localisation of Thunderbird</ku>
   <lt>Spanish localisation of Thunderbird</lt>
   <mk>Spanish localisation of Thunderbird</mk>
   <mr>Spanish localisation of Thunderbird</mr>
   <nb_NO>Spanish localisation of Thunderbird</nb_NO>
   <nb>Spansk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Spanish localisation of Thunderbird</nl_BE>
   <nl>Spaanse lokalisatie van Thunderbird</nl>
   <or>Spanish localisation of Thunderbird</or>
   <pl>Hiszpańska lokalizacja Thunderbirda</pl>
   <pt_BR>Espanhol Localização para o Thunderbird</pt_BR>
   <pt>Castelhano Localização para Thunderbird</pt>
   <ro>Spanish localisation of Thunderbird</ro>
   <ru>Spanish localisation of Thunderbird</ru>
   <sk>Spanish localisation of Thunderbird</sk>
   <sl>Španske krajevne nastavitve za Thunderbird</sl>
   <so>Spanish localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në spanjisht</sq>
   <sr>Spanish localisation of Thunderbird</sr>
   <sv>Spansk lokalisering av Thunderbird </sv>
   <th>Spanish localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün İspanyolca yerelleştirmesi</tr>
   <uk>Spanish localisation of Thunderbird</uk>
   <vi>Spanish localisation of Thunderbird</vi>
   <zh_CN>Spanish localisation of Thunderbird</zh_CN>
   <zh_HK>Spanish localisation of Thunderbird</zh_HK>
   <zh_TW>Spanish localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-es-es
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-es-es
</uninstall_package_names>

</app>
