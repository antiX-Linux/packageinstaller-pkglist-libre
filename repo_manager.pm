<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Utility
</category>

<name>
Repo Manager
</name>

<description>
   <am>GUI for setting antiX and Debian repos</am>
   <ar>GUI for setting antiX and Debian repos</ar>
   <be>GUI for setting antiX and Debian repos</be>
   <bg>GUI for setting antiX and Debian repos</bg>
   <bn>GUI for setting antiX and Debian repos</bn>
   <ca>IGU per definir els dipòsits d'antiX i Debian</ca>
   <cs>GUI for setting antiX and Debian repos</cs>
   <da>GUI for setting antiX and Debian repos</da>
   <de>Graphische Oberfläche zur Festlegung der antiX- und Debian-Repositorien.</de>
   <el>GUI για ρύθμιση των repos antiX και Debian</el>
   <en>GUI for setting antiX and Debian repos</en>
   <es_ES>GUI para configurar repositorios antiX y Debian</es_ES>
   <es>GUI para configurar repositorios antiX y Debian</es>
   <et>GUI for setting antiX and Debian repos</et>
   <eu>GUI for setting antiX and Debian repos</eu>
   <fa>GUI for setting antiX and Debian repos</fa>
   <fil_PH>GUI for setting antiX and Debian repos</fil_PH>
   <fi>Käyttöliittymä antiX- ja Debian-pakettihakemistojen asettamiseen</fi>
   <fr_BE>Interface graphique pour configurer les dépôts antiX et Debian</fr_BE>
   <fr>Interface graphique pour configurer les dépôts antiX et Debian</fr>
   <gl_ES>GUI for setting antiX and Debian repos</gl_ES>
   <gu>GUI for setting antiX and Debian repos</gu>
   <he_IL>GUI for setting antiX and Debian repos</he_IL>
   <hi>एंटी-एक्स व डेबियन पैकेज-संग्रह सेट करने हेतु ग्राफ़िकल अंतरफलक</hi>
   <hr>GUI for setting antiX and Debian repos</hr>
   <hu>GUI for setting antiX and Debian repos</hu>
   <id>GUI for setting antiX and Debian repos</id>
   <is>GUI for setting antiX and Debian repos</is>
   <it>Interfaccia grafica per l'impostazione di repository antiX e Debian</it>
   <ja>antiX と Debian リポジトリ設定用 GUI</ja>
   <kk>GUI for setting antiX and Debian repos</kk>
   <ko>GUI for setting antiX and Debian repos</ko>
   <ku>GUI for setting antiX and Debian repos</ku>
   <lt>GUI for setting antiX and Debian repos</lt>
   <mk>GUI for setting antiX and Debian repos</mk>
   <mr>GUI for setting antiX and Debian repos</mr>
   <nb_NO>GUI for setting antiX and Debian repos</nb_NO>
   <nb>GUI for oppsett av kodelagre for antiX og Debian</nb>
   <nl_BE>GUI for setting antiX and Debian repos</nl_BE>
   <nl>GUI for setting antiX and Debian repos</nl>
   <or>GUI for setting antiX and Debian repos</or>
   <pl>GUI for setting antiX and Debian repos</pl>
   <pt_BR>Interface gráfica de usuário (GUI) para configurar repositórios antiX e Debian</pt_BR>
   <pt>Interface gráfico para configurar repositórios antiX e Debian</pt>
   <ro>GUI for setting antiX and Debian repos</ro>
   <ru>GUI for setting antiX and Debian repos</ru>
   <sk>GUI for setting antiX and Debian repos</sk>
   <sl>Grafični vmesnik za nastavljanje antiX in Debian repozitorijev</sl>
   <so>GUI for setting antiX and Debian repos</so>
   <sq>GUI për ujdisje deposh antiX dhe Debian</sq>
   <sr>GUI for setting antiX and Debian repos</sr>
   <sv>Grafiskt användargränssnitt för att ställa in antiX och Debian förråd</sv>
   <th>GUI for setting antiX and Debian repos</th>
   <tr>antiX ve Debian depolarını ayarlamak için grafik arayüz</tr>
   <uk>GUI for setting antiX and Debian repos</uk>
   <vi>GUI for setting antiX and Debian repos</vi>
   <zh_CN>GUI for setting antiX and Debian repos</zh_CN>
   <zh_HK>GUI for setting antiX and Debian repos</zh_HK>
   <zh_TW>GUI for setting antiX and Debian repos</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
repo-manager
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
repo-manager
</uninstall_package_names>
</app>
