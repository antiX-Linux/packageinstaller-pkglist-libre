<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Utility
</category>

<name>
Skippy-XD
</name>

<description>
   <am>Full-screen task-switcher for X11</am>
   <ar>Full-screen task-switcher for X11</ar>
   <be>Full-screen task-switcher for X11</be>
   <bg>Full-screen task-switcher for X11</bg>
   <bn>Full-screen task-switcher for X11</bn>
   <ca>Canviador de tasques en pantalla sencera per X11</ca>
   <cs>Full-screen task-switcher for X11</cs>
   <da>Full-screen task-switcher for X11</da>
   <de>Vollbild-Programmumschalter für das X Window System (X11)</de>
   <el>Εναλλαγή εργασιών πλήρους οθόνης για X11</el>
   <en>Full-screen task-switcher for X11</en>
   <es_ES>Conmutador de tareas de pantalla completa para X11</es_ES>
   <es>Conmutador de tareas de pantalla completa para X11</es>
   <et>Full-screen task-switcher for X11</et>
   <eu>Full-screen task-switcher for X11</eu>
   <fa>Full-screen task-switcher for X11</fa>
   <fil_PH>Full-screen task-switcher for X11</fil_PH>
   <fi>Full-screen task-switcher for X11</fi>
   <fr_BE>Sélecteur de tâches en plein écran pour X11</fr_BE>
   <fr>Sélecteur de tâches en plein écran pour X11</fr>
   <gl_ES>Full-screen task-switcher for X11</gl_ES>
   <gu>Full-screen task-switcher for X11</gu>
   <he_IL>Full-screen task-switcher for X11</he_IL>
   <hi>X11 हेतु पूर्ण-स्क्रीन कार्य-परिवर्तन साधन</hi>
   <hr>Full-screen task-switcher for X11</hr>
   <hu>Full-screen task-switcher for X11</hu>
   <id>Full-screen task-switcher for X11</id>
   <is>Full-screen task-switcher for X11</is>
   <it>Commutatore di attività a schermo intero per X11</it>
   <ja>X11用のフルスクリーン・タスクスイッチャー</ja>
   <kk>Full-screen task-switcher for X11</kk>
   <ko>Full-screen task-switcher for X11</ko>
   <ku>Full-screen task-switcher for X11</ku>
   <lt>Full-screen task-switcher for X11</lt>
   <mk>Full-screen task-switcher for X11</mk>
   <mr>Full-screen task-switcher for X11</mr>
   <nb_NO>Full-screen task-switcher for X11</nb_NO>
   <nb>Oppgaveveksler (fullskjerm) for X11</nb>
   <nl_BE>Full-screen task-switcher for X11</nl_BE>
   <nl>Full-screen task-switcher for X11</nl>
   <or>Full-screen task-switcher for X11</or>
   <pl>Full-screen task-switcher for X11</pl>
   <pt_BR>Alternador de tarefas em tela cheia para X11</pt_BR>
   <pt>Gestor de tarefas em ecrã completo para X11</pt>
   <ro>Full-screen task-switcher for X11</ro>
   <ru>Full-screen task-switcher for X11</ru>
   <sk>Full-screen task-switcher for X11</sk>
   <sl>Celozaslonski preklopnik optravil za X11</sl>
   <so>Full-screen task-switcher for X11</so>
   <sq>Këmbyes punësh për X11, sa krejt ekrani</sq>
   <sr>Full-screen task-switcher for X11</sr>
   <sv>Fullskärms aktivitetsväxlare för X11</sv>
   <th>Full-screen task-switcher for X11</th>
   <tr>X11 için tam ekran görev değiştirici</tr>
   <uk>Full-screen task-switcher for X11</uk>
   <vi>Full-screen task-switcher for X11</vi>
   <zh_CN>Full-screen task-switcher for X11</zh_CN>
   <zh_HK>Full-screen task-switcher for X11</zh_HK>
   <zh_TW>Full-screen task-switcher for X11</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
skippy-xd
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
skippy-xd
</uninstall_package_names>
</app>
