<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
French_Thunderbird
</name>

<description>
   <am>French localisation of Thunderbird</am>
   <ar>French localisation of Thunderbird</ar>
   <be>French localisation of Thunderbird</be>
   <bg>French localisation of Thunderbird</bg>
   <bn>French localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Francès</ca>
   <cs>French localisation of Thunderbird</cs>
   <da>Fransk oversættelse af Thunderbird</da>
   <de>Französische Lokalisierung von Thunderbird</de>
   <el>Γαλλικά για το Thunderbird</el>
   <en>French localisation of Thunderbird</en>
   <es_ES>Localización Francesa de Thunderbird</es_ES>
   <es>Localización Francés de Thunderbird</es>
   <et>French localisation of Thunderbird</et>
   <eu>French localisation of Thunderbird</eu>
   <fa>French localisation of Thunderbird</fa>
   <fil_PH>French localisation of Thunderbird</fil_PH>
   <fi>Ranskalainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en français pour Thunderbird</fr_BE>
   <fr>Localisation en français pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao francés</gl_ES>
   <gu>French localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לצרפתית</he_IL>
   <hi>थंडरबर्ड का फ्रेंच संस्करण</hi>
   <hr>French localisation of Thunderbird</hr>
   <hu>French localisation of Thunderbird</hu>
   <id>French localisation of Thunderbird</id>
   <is>French localisation of Thunderbird</is>
   <it>Localizzazione francese di Thunderbird</it>
   <ja>Thunderbird のフランス語版</ja>
   <kk>French localisation of Thunderbird</kk>
   <ko>French localisation of Thunderbird</ko>
   <ku>French localisation of Thunderbird</ku>
   <lt>French localisation of Thunderbird</lt>
   <mk>French localisation of Thunderbird</mk>
   <mr>French localisation of Thunderbird</mr>
   <nb_NO>French localisation of Thunderbird</nb_NO>
   <nb>Fransk lokaltilpassing av Thunderbird</nb>
   <nl_BE>French localisation of Thunderbird</nl_BE>
   <nl>Franse lokalisatie van Thunderbird</nl>
   <or>French localisation of Thunderbird</or>
   <pl>Francuska lokalizacja Thunderbirda</pl>
   <pt_BR>Francês Localização para o Thunderbird</pt_BR>
   <pt>Francês Localização para Thunderbird</pt>
   <ro>French localisation of Thunderbird</ro>
   <ru>French localisation of Thunderbird</ru>
   <sk>French localisation of Thunderbird</sk>
   <sl>Francoska lokalizacija za Thunderbird</sl>
   <so>French localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në frëngjisht</sq>
   <sr>French localisation of Thunderbird</sr>
   <sv>Fransk lokalisering av Thunderbird</sv>
   <th>French localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Fransızca yerelleştirmesi</tr>
   <uk>French локалізація Thunderbird</uk>
   <vi>French localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 法语语言包</zh_CN>
   <zh_HK>French localisation of Thunderbird</zh_HK>
   <zh_TW>French localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-fr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-fr
</uninstall_package_names>

</app>
