<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Kannada_Default_Firefox_esr
</name>

<description>
   <am>Kannada localisation of Firefox ESR</am>
   <ar>Kannada localisation of Firefox ESR</ar>
   <be>Kannada localisation of Firefox ESR</be>
   <bg>Kannada localisation of Firefox ESR</bg>
   <bn>Kannada localisation of Firefox ESR</bn>
   <ca>Localització en Kannada de Firefox ESR</ca>
   <cs>Kannada localisation of Firefox ESR</cs>
   <da>Kannada localisation of Firefox ESR</da>
   <de>Kannada localisation of Firefox ESR</de>
   <el>Kannada για Firefox ESR</el>
   <en>Kannada localisation of Firefox ESR</en>
   <es_ES>Localización en canarés de Firefox ESR</es_ES>
   <es>Localización en canarés de Firefox ESR</es>
   <et>Kannada localisation of Firefox ESR</et>
   <eu>Kannada localisation of Firefox ESR</eu>
   <fa>Kannada localisation of Firefox ESR</fa>
   <fil_PH>Kannada localisation of Firefox ESR</fil_PH>
   <fi>Kannada localisation of Firefox ESR</fi>
   <fr_BE>Localisation en kannada (Inde) pour Firefox ESR</fr_BE>
   <fr>Localisation en kannada (Inde) pour Firefox ESR</fr>
   <gl_ES>Kannada localisation of Firefox ESR</gl_ES>
   <gu>Kannada localisation of Firefox ESR</gu>
   <he_IL>Kannada localisation of Firefox ESR</he_IL>
   <hi>Kannada localisation of Firefox ESR</hi>
   <hr>Kannada localisation of Firefox ESR</hr>
   <hu>Kannada localisation of Firefox ESR</hu>
   <id>Kannada localisation of Firefox ESR</id>
   <is>Kannada localisation of Firefox ESR</is>
   <it>Localizzazione Kannada di Firefox ESR</it>
   <ja>Kannada localisation of Firefox ESR</ja>
   <kk>Kannada localisation of Firefox ESR</kk>
   <ko>Kannada localisation of Firefox ESR</ko>
   <ku>Kannada localisation of Firefox ESR</ku>
   <lt>Kannada localisation of Firefox ESR</lt>
   <mk>Kannada localisation of Firefox ESR</mk>
   <mr>Kannada localisation of Firefox ESR</mr>
   <nb_NO>Kannada localisation of Firefox ESR</nb_NO>
   <nb>Kannada lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Kannada localisation of Firefox ESR</nl_BE>
   <nl>Kannada localisation of Firefox ESR</nl>
   <or>Kannada localisation of Firefox ESR</or>
   <pl>Kannada localisation of Firefox ESR</pl>
   <pt_BR>Canarim Localização para o Firefox ESR</pt_BR>
   <pt>Canarim Localização para Firefox ESR</pt>
   <ro>Kannada localisation of Firefox ESR</ro>
   <ru>Kannada localisation of Firefox ESR</ru>
   <sk>Kannada localisation of Firefox ESR</sk>
   <sl>Kannadske krajevne nastavitve za Firefox ESR</sl>
   <so>Kannada localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në kanada</sq>
   <sr>Kannada localisation of Firefox ESR</sr>
   <sv>Kannada lokalisering av Firefox ESR</sv>
   <th>Kannada localisation of Firefox ESR</th>
   <tr>Kannada localisation of Firefox ESR</tr>
   <uk>Kannada localisation of Firefox ESR</uk>
   <vi>Kannada localisation of Firefox ESR</vi>
   <zh_CN>Kannada localisation of Firefox ESR</zh_CN>
   <zh_HK>Kannada localisation of Firefox ESR</zh_HK>
   <zh_TW>Kannada localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-kn
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-kn
</uninstall_package_names>

</app>
