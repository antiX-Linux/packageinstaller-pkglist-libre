<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Icons
</category>

<name>
Papirus antiX
</name>

<description>
   <am>Full version of Papirus icons for antiX Linux</am>
   <ar>Full version of Papirus icons for antiX Linux</ar>
   <be>Full version of Papirus icons for antiX Linux</be>
   <bg>Full version of Papirus icons for antiX Linux</bg>
   <bn>Full version of Papirus icons for antiX Linux</bn>
   <ca>Versió completa d'icones Papirus per antiX Linux</ca>
   <cs>Full version of Papirus icons for antiX Linux</cs>
   <da>Full version of Papirus icons for antiX Linux</da>
   <de>Piktogramme (Ugs.: “Icons”) für antiX im Stil “Numix Bevil” (Vollständiger Satz)</de>
   <el>Πλήρης έκδοση εικονιδίων Papirus για antiX Linux</el>
   <en>Full version of Papirus icons for antiX Linux</en>
   <es_ES>Versión completa de los iconos Papirus para antiX Linux</es_ES>
   <es>Versión completa de los iconos Papirus para antiX Linux</es>
   <et>Full version of Papirus icons for antiX Linux</et>
   <eu>Full version of Papirus icons for antiX Linux</eu>
   <fa>Full version of Papirus icons for antiX Linux</fa>
   <fil_PH>Full version of Papirus icons for antiX Linux</fil_PH>
   <fi>Full version of Papirus icons for antiX Linux</fi>
   <fr_BE>Icônes pour antiX dans le style “Papirus” (version complète)</fr_BE>
   <fr>Icônes pour antiX dans le style “Papirus” (version complète)</fr>
   <gl_ES>Full version of Papirus icons for antiX Linux</gl_ES>
   <gu>Full version of Papirus icons for antiX Linux</gu>
   <he_IL>Full version of Papirus icons for antiX Linux</he_IL>
   <hi>एंटी-एक्स लिनक्स हेतु Papirus आइकन का पूर्ण संस्करण</hi>
   <hr>Full version of Papirus icons for antiX Linux</hr>
   <hu>Full version of Papirus icons for antiX Linux</hu>
   <id>Full version of Papirus icons for antiX Linux</id>
   <is>Full version of Papirus icons for antiX Linux</is>
   <it>Versione completa di icone Papirus per antiX Linux</it>
   <ja>AntiX Linux用 Papirusアイコンのフルバージョン</ja>
   <kk>Full version of Papirus icons for antiX Linux</kk>
   <ko>Full version of Papirus icons for antiX Linux</ko>
   <ku>Full version of Papirus icons for antiX Linux</ku>
   <lt>Full version of Papirus icons for antiX Linux</lt>
   <mk>Full version of Papirus icons for antiX Linux</mk>
   <mr>Full version of Papirus icons for antiX Linux</mr>
   <nb_NO>Full version of Papirus icons for antiX Linux</nb_NO>
   <nb>Full versjon av Papirus-ikoner for antiX Linux</nb>
   <nl_BE>Full version of Papirus icons for antiX Linux</nl_BE>
   <nl>Full version of Papirus icons for antiX Linux</nl>
   <or>Full version of Papirus icons for antiX Linux</or>
   <pl>Full version of Papirus icons for antiX Linux</pl>
   <pt_BR>Conjunto completo de ícones Papirus para o antiX Linux</pt_BR>
   <pt>Versão completa dos ícones Papirus para o antiX Linux</pt>
   <ro>Full version of Papirus icons for antiX Linux</ro>
   <ru>Full version of Papirus icons for antiX Linux</ru>
   <sk>Full version of Papirus icons for antiX Linux</sk>
   <sl>Polna različica Papirus ikon za antiX Linux</sl>
   <so>Full version of Papirus icons for antiX Linux</so>
   <sq>Version i plotë i ikonave Papirus për antiX Linux</sq>
   <sr>Full version of Papirus icons for antiX Linux</sr>
   <sv>Full version av Papirus ikoner för antiX Linux</sv>
   <th>Full version of Papirus icons for antiX Linux</th>
   <tr>antiX Linux için Papirus simgelerinin tam sürümü</tr>
   <uk>Full version of Papirus icons for antiX Linux</uk>
   <vi>Full version of Papirus icons for antiX Linux</vi>
   <zh_CN>Full version of Papirus icons for antiX Linux</zh_CN>
   <zh_HK>Full version of Papirus icons for antiX Linux</zh_HK>
   <zh_TW>Full version of Papirus icons for antiX Linux</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
papiris-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
papiris-antix
</uninstall_package_names>
</app>
