<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Lower_Sorbian_Default_Firefox_esr
</name>

<description>
   <am>Lower Sorbian localisation of Firefox ESR</am>
   <ar>Lower Sorbian localisation of Firefox ESR</ar>
   <be>Lower Sorbian localisation of Firefox ESR</be>
   <bg>Lower Sorbian localisation of Firefox ESR</bg>
   <bn>Lower Sorbian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Baix-Serbi</ca>
   <cs>Lower Sorbian localisation of Firefox ESR</cs>
   <da>Lower Sorbian localisation of Firefox ESR</da>
   <de>Lokalisation für die wendische Sprache von “Firefox ESR”</de>
   <el>Κάτω Sorbian για Firefox ESR</el>
   <en>Lower Sorbian localisation of Firefox ESR</en>
   <es_ES>Localización Sorbio inferior de Firefox ESR</es_ES>
   <es>Localización Sorabo inferior de Firefox ESR</es>
   <et>Lower Sorbian localisation of Firefox ESR</et>
   <eu>Lower Sorbian localisation of Firefox ESR</eu>
   <fa>Lower Sorbian localisation of Firefox ESR</fa>
   <fil_PH>Lower Sorbian localisation of Firefox ESR</fil_PH>
   <fi>Lower Sorbian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en bas sorabe pour Firefox ESR</fr_BE>
   <fr>Localisation en bas sorabe pour Firefox ESR</fr>
   <gl_ES>Lower Sorbian localisation of Firefox ESR</gl_ES>
   <gu>Lower Sorbian localisation of Firefox ESR</gu>
   <he_IL>Lower Sorbian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का निचली सोर्बियाई संस्करण</hi>
   <hr>Lower Sorbian localisation of Firefox ESR</hr>
   <hu>Lower Sorbian localisation of Firefox ESR</hu>
   <id>Lower Sorbian localisation of Firefox ESR</id>
   <is>Lower Sorbian localisation of Firefox ESR</is>
   <it>Localizzazione lower sorbian di Firefox ESR</it>
   <ja>低地ソルブ語版 Firefox ESR</ja>
   <kk>Lower Sorbian localisation of Firefox ESR</kk>
   <ko>Lower Sorbian localisation of Firefox ESR</ko>
   <ku>Lower Sorbian localisation of Firefox ESR</ku>
   <lt>Lower Sorbian localisation of Firefox ESR</lt>
   <mk>Lower Sorbian localisation of Firefox ESR</mk>
   <mr>Lower Sorbian localisation of Firefox ESR</mr>
   <nb_NO>Lower Sorbian localisation of Firefox ESR</nb_NO>
   <nb>Nedre sorbisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Lower Sorbian localisation of Firefox ESR</nl_BE>
   <nl>Lower Sorbian localisation of Firefox ESR</nl>
   <or>Lower Sorbian localisation of Firefox ESR</or>
   <pl>Lower Sorbian localisation of Firefox ESR</pl>
   <pt_BR>Sorábia Inferior Localização para o Firefox ESR</pt_BR>
   <pt>Baixo Sórbio Localização para Firefox ESR</pt>
   <ro>Lower Sorbian localisation of Firefox ESR</ro>
   <ru>Lower Sorbian localisation of Firefox ESR</ru>
   <sk>Lower Sorbian localisation of Firefox ESR</sk>
   <sl>Spodnje lužiško srbske krajevne nastavitve za Firefox ESR</sl>
   <so>Lower Sorbian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në sorbishte të poshtme</sq>
   <sr>Lower Sorbian localisation of Firefox ESR</sr>
   <sv>Nedersorbisk lokalisering av Firefox ESR</sv>
   <th>Lower Sorbian localisation of Firefox ESR</th>
   <tr>Firefox ESR Aşağı Sırpça yerelleştirmesi</tr>
   <uk>Lower Sorbian localisation of Firefox ESR</uk>
   <vi>Lower Sorbian localisation of Firefox ESR</vi>
   <zh_CN>Lower Sorbian localisation of Firefox ESR</zh_CN>
   <zh_HK>Lower Sorbian localisation of Firefox ESR</zh_HK>
   <zh_TW>Lower Sorbian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-esr-l10n-dsb
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-dsb
</uninstall_package_names>

</app>
