<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Assamese_Default_Firefox_esr
</name>

<description>
   <am>Assamese localisation of Firefox-ESR</am>
   <ar>Assamese localisation of Firefox-ESR</ar>
   <be>Assamese localisation of Firefox-ESR</be>
   <bg>Assamese localisation of Firefox-ESR</bg>
   <bn>Assamese localisation of Firefox-ESR</bn>
   <ca>Localització de Firefox-ESR en Assamès</ca>
   <cs>Assamese localisation of Firefox-ESR</cs>
   <da>Assamese localisation of Firefox-ESR</da>
   <de>Assamesische Lokalisation von “Firefox ESR”</de>
   <el>Assamese για τον Firefox-ESR</el>
   <en>Assamese localisation of Firefox-ESR</en>
   <es_ES>Localización Asames de Firefox-ESR</es_ES>
   <es>Localización Asamés de Firefox ESR</es>
   <et>Assamese localisation of Firefox-ESR</et>
   <eu>Assamese localisation of Firefox-ESR</eu>
   <fa>Assamese localisation of Firefox-ESR</fa>
   <fil_PH>Assamese localisation of Firefox-ESR</fil_PH>
   <fi>Assamese localisation of Firefox-ESR</fi>
   <fr_BE>Localisation en assamais pour Firefox ESR</fr_BE>
   <fr>Localisation en assamais pour Firefox ESR</fr>
   <gl_ES>Assamese localisation of Firefox-ESR</gl_ES>
   <gu>Assamese localisation of Firefox-ESR</gu>
   <he_IL>Assamese localisation of Firefox-ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का असमी संस्करण</hi>
   <hr>Assamese localisation of Firefox-ESR</hr>
   <hu>Assamese localisation of Firefox-ESR</hu>
   <id>Assamese localisation of Firefox-ESR</id>
   <is>Assamese localisation of Firefox-ESR</is>
   <it>Localizzazione assamese di Firefox ESR</it>
   <ja>アッサム語版 Firefox-ESR</ja>
   <kk>Assamese localisation of Firefox-ESR</kk>
   <ko>Assamese localisation of Firefox-ESR</ko>
   <ku>Assamese localisation of Firefox-ESR</ku>
   <lt>Assamese localisation of Firefox-ESR</lt>
   <mk>Assamese localisation of Firefox-ESR</mk>
   <mr>Assamese localisation of Firefox-ESR</mr>
   <nb_NO>Assamese localisation of Firefox-ESR</nb_NO>
   <nb>Assamesisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Assamese localisation of Firefox-ESR</nl_BE>
   <nl>Assamees lokalisatie van Firefox-ESR</nl>
   <or>Assamese localisation of Firefox-ESR</or>
   <pl>Assamese localisation of Firefox-ESR</pl>
   <pt_BR>Assamês Localização para o Firefox ESR</pt_BR>
   <pt>Assamês Localização para Firefox ESR</pt>
   <ro>Assamese localisation of Firefox-ESR</ro>
   <ru>Assamese localisation of Firefox-ESR</ru>
   <sk>Assamese localisation of Firefox-ESR</sk>
   <sl>Asamejske krajevne nastavitve za Firefox-ESR</sl>
   <so>Assamese localisation of Firefox-ESR</so>
   <sq>Përkthimi i Firefox-it ESR në asamisht</sq>
   <sr>Assamese localisation of Firefox-ESR</sr>
   <sv>Assamesisk lokalisering av Firefox-ESR</sv>
   <th>Assamese localisation of Firefox-ESR</th>
   <tr>Firefox-ESR Assamese dili yerelleştirmesi</tr>
   <uk>Assamese localisation of Firefox-ESR</uk>
   <vi>Assamese localisation of Firefox-ESR</vi>
   <zh_CN>Assamese localisation of Firefox-ESR</zh_CN>
   <zh_HK>Assamese localisation of Firefox-ESR</zh_HK>
   <zh_TW>Assamese localisation of Firefox-ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-as
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-as
</uninstall_package_names>

</app>
