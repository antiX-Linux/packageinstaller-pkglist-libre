<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Ukrainian_Default_Firefox_esr
</name>

<description>
   <am>Ukrainian localisation of Firefox ESR</am>
   <ar>Ukrainian localisation of Firefox ESR</ar>
   <be>Ukrainian localisation of Firefox ESR</be>
   <bg>Ukrainian localisation of Firefox ESR</bg>
   <bn>Ukrainian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Ucrainès</ca>
   <cs>Ukrainian localisation of Firefox ESR</cs>
   <da>Ukrainian localisation of Firefox ESR</da>
   <de>Ukrainische Lokalisation von “Firefox ESR”</de>
   <el>Ουκρανικά για Firefox ESR</el>
   <en>Ukrainian localisation of Firefox ESR</en>
   <es_ES>Localización Ucraniana de Firefox ESR</es_ES>
   <es>Localización Ucraniano de Firefox ESR</es>
   <et>Ukrainian localisation of Firefox ESR</et>
   <eu>Ukrainian localisation of Firefox ESR</eu>
   <fa>Ukrainian localisation of Firefox ESR</fa>
   <fil_PH>Ukrainian localisation of Firefox ESR</fil_PH>
   <fi>Ukrainian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en ukrainien pour Firefox ESR</fr_BE>
   <fr>Localisation en ukrainien pour Firefox ESR</fr>
   <gl_ES>Ukrainian localisation of Firefox ESR</gl_ES>
   <gu>Ukrainian localisation of Firefox ESR</gu>
   <he_IL>Ukrainian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का यूक्रेनी संस्करण</hi>
   <hr>Ukrainian localisation of Firefox ESR</hr>
   <hu>Ukrainian localisation of Firefox ESR</hu>
   <id>Ukrainian localisation of Firefox ESR</id>
   <is>Ukrainian localisation of Firefox ESR</is>
   <it>Localizzazione ucraina di Firefox ESR</it>
   <ja>ウクライナ語版 Firefox ESR</ja>
   <kk>Ukrainian localisation of Firefox ESR</kk>
   <ko>Ukrainian localisation of Firefox ESR</ko>
   <ku>Ukrainian localisation of Firefox ESR</ku>
   <lt>Ukrainian localisation of Firefox ESR</lt>
   <mk>Ukrainian localisation of Firefox ESR</mk>
   <mr>Ukrainian localisation of Firefox ESR</mr>
   <nb_NO>Ukrainian localisation of Firefox ESR</nb_NO>
   <nb>Ukrainsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Ukrainian localisation of Firefox ESR</nl_BE>
   <nl>Ukrainian localisation of Firefox ESR</nl>
   <or>Ukrainian localisation of Firefox ESR</or>
   <pl>Ukrainian localisation of Firefox ESR</pl>
   <pt_BR>Ucraniano Localização para o Firefox ESR</pt_BR>
   <pt>Ucraniano Localização para Firefox ESR</pt>
   <ro>Ukrainian localisation of Firefox ESR</ro>
   <ru>Ukrainian localisation of Firefox ESR</ru>
   <sk>Ukrainian localisation of Firefox ESR</sk>
   <sl>Ukrajinske krajevne nastavitve za Firefox ESR</sl>
   <so>Ukrainian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në ukrainisht</sq>
   <sr>Ukrainian localisation of Firefox ESR</sr>
   <sv>Ukrainsk lokalisering av Firefox ESR</sv>
   <th>Ukrainian localisation of Firefox ESR</th>
   <tr>Firefox ESR Ukraynaca yerelleştirmesi</tr>
   <uk>Ukrainian localisation of Firefox ESR</uk>
   <vi>Ukrainian localisation of Firefox ESR</vi>
   <zh_CN>Ukrainian localisation of Firefox ESR</zh_CN>
   <zh_HK>Ukrainian localisation of Firefox ESR</zh_HK>
   <zh_TW>Ukrainian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-uk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-uk
</uninstall_package_names>

</app>
