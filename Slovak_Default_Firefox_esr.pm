<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Slovak_Default_Firefox_esr
</name>

<description>
   <am>Slovak localisation of Firefox ESR</am>
   <ar>Slovak localisation of Firefox ESR</ar>
   <be>Slovak localisation of Firefox ESR</be>
   <bg>Slovak localisation of Firefox ESR</bg>
   <bn>Slovak localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Eslovac</ca>
   <cs>Slovak localisation of Firefox ESR</cs>
   <da>Slovak localisation of Firefox ESR</da>
   <de>Slowakische Lokalisation von “Firefox ESR”</de>
   <el>Σλοβακικά για το Firefox ESR</el>
   <en>Slovak localisation of Firefox ESR</en>
   <es_ES>Localización Eslovaco de Firefox ESR</es_ES>
   <es>Localización Eslovaco de Firefox ESR</es>
   <et>Slovak localisation of Firefox ESR</et>
   <eu>Slovak localisation of Firefox ESR</eu>
   <fa>Slovak localisation of Firefox ESR</fa>
   <fil_PH>Slovak localisation of Firefox ESR</fil_PH>
   <fi>Slovak localisation of Firefox ESR</fi>
   <fr_BE>Localisation en slovaque pour Firefox ESR</fr_BE>
   <fr>Localisation en slovaque pour Firefox ESR</fr>
   <gl_ES>Slovak localisation of Firefox ESR</gl_ES>
   <gu>Slovak localisation of Firefox ESR</gu>
   <he_IL>Slovak localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का स्लोवाकियाई संस्करण</hi>
   <hr>Slovak localisation of Firefox ESR</hr>
   <hu>Slovak localisation of Firefox ESR</hu>
   <id>Slovak localisation of Firefox ESR</id>
   <is>Slovak localisation of Firefox ESR</is>
   <it>Localizzazione slovak di Firefox ESR</it>
   <ja>スロバキア語版 Firefox ESR</ja>
   <kk>Slovak localisation of Firefox ESR</kk>
   <ko>Slovak localisation of Firefox ESR</ko>
   <ku>Slovak localisation of Firefox ESR</ku>
   <lt>Slovak localisation of Firefox ESR</lt>
   <mk>Slovak localisation of Firefox ESR</mk>
   <mr>Slovak localisation of Firefox ESR</mr>
   <nb_NO>Slovak localisation of Firefox ESR</nb_NO>
   <nb>Slovakisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Slovak localisation of Firefox ESR</nl_BE>
   <nl>Slovak localisation of Firefox ESR</nl>
   <or>Slovak localisation of Firefox ESR</or>
   <pl>Slovak localisation of Firefox ESR</pl>
   <pt_BR>Eslovaco Localização para o Firefox ESR</pt_BR>
   <pt>Eslovaco Localização para Firefox ESR</pt>
   <ro>Slovak localisation of Firefox ESR</ro>
   <ru>Slovak localisation of Firefox ESR</ru>
   <sk>Slovak localisation of Firefox ESR</sk>
   <sl>Slovaške krajevne nastavitve za Firefox ESR</sl>
   <so>Slovak localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në sllovakisht</sq>
   <sr>Slovak localisation of Firefox ESR</sr>
   <sv>Slovakisk lokalisering av Firefox ESR</sv>
   <th>Slovak localisation of Firefox ESR</th>
   <tr>Firefox ESR Slovakça yerelleştirmesi</tr>
   <uk>Slovak localisation of Firefox ESR</uk>
   <vi>Slovak localisation of Firefox ESR</vi>
   <zh_CN>Slovak localisation of Firefox ESR</zh_CN>
   <zh_HK>Slovak localisation of Firefox ESR</zh_HK>
   <zh_TW>Slovak localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-sk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-sk
</uninstall_package_names>

</app>
