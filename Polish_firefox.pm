<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Polish_Firefox
</name>

<description>
   <am>Polish localisation of Firefox</am>
   <ar>Polish localisation of Firefox</ar>
   <be>Polish localisation of Firefox</be>
   <bg>Polish localisation of Firefox</bg>
   <bn>Polish localisation of Firefox</bn>
   <ca>Localització de Firefox en Polonès</ca>
   <cs>Polish localisation of Firefox</cs>
   <da>Polsk oversættelse af Firefox</da>
   <de>Polnische Lokalisierung von Firefox</de>
   <el>Πολωνικά για το Firefox</el>
   <en>Polish localisation of Firefox</en>
   <es_ES>Localización Polaca de Firefox</es_ES>
   <es>Localización Polaco de Firefox</es>
   <et>Polish localisation of Firefox</et>
   <eu>Polish localisation of Firefox</eu>
   <fa>Polish localisation of Firefox</fa>
   <fil_PH>Polish localisation of Firefox</fil_PH>
   <fi>Puolalainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en polonais pour Firefox</fr_BE>
   <fr>Localisation en polonais pour Firefox</fr>
   <gl_ES>Localización de Firefox ao polaco</gl_ES>
   <gu>Polish localisation of Firefox</gu>
   <he_IL>תרגומי Firefox לפולנית</he_IL>
   <hi>फायरफॉक्स का पोलिश संस्करण</hi>
   <hr>Polish localisation of Firefox</hr>
   <hu>Polish localisation of Firefox</hu>
   <id>Polish localisation of Firefox</id>
   <is>Polish localisation of Firefox</is>
   <it>Localizzazione polacca di Firefox</it>
   <ja>ポーランド語版 Firefox</ja>
   <kk>Polish localisation of Firefox</kk>
   <ko>Polish localisation of Firefox</ko>
   <ku>Polish localisation of Firefox</ku>
   <lt>Polish localisation of Firefox</lt>
   <mk>Polish localisation of Firefox</mk>
   <mr>Polish localisation of Firefox</mr>
   <nb_NO>Polish localisation of Firefox</nb_NO>
   <nb>Polsk lokaltilpassing av Firefox</nb>
   <nl_BE>Polish localisation of Firefox</nl_BE>
   <nl>Poolse lokalisatie van Firefox</nl>
   <or>Polish localisation of Firefox</or>
   <pl>Polska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Polonês Localização para o Firefox</pt_BR>
   <pt>Polaco Localização para Firefox</pt>
   <ro>Polish localisation of Firefox</ro>
   <ru>Польская локализация Firefox</ru>
   <sk>Polish localisation of Firefox</sk>
   <sl>Poljske krajevne nastavitve za Firefox</sl>
   <so>Polish localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në polonisht</sq>
   <sr>Polish localisation of Firefox</sr>
   <sv>Polsk lokalisering av Firefox</sv>
   <th>Polish localisation ของ Firefox</th>
   <tr>Firefox'un Lehçe yerelleştirmesi</tr>
   <uk>Polish локалізація Firefox</uk>
   <vi>Polish localisation of Firefox</vi>
   <zh_CN>Polish localisation of Firefox</zh_CN>
   <zh_HK>Polish localisation of Firefox</zh_HK>
   <zh_TW>Polish localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-pl
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-pl
</uninstall_package_names>
</app>
