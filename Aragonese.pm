<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Aragonese
</name>

<description>
   <am>Aragonese dictionary for hunspell</am>
   <ar>Aragonese dictionary for hunspell</ar>
   <be>Aragonese dictionary for hunspell</be>
   <bg>Aragonese dictionary for hunspell</bg>
   <bn>Aragonese dictionary for hunspell</bn>
   <ca>Diccionari Aragonès per hunspell</ca>
   <cs>Aragonese dictionary for hunspell</cs>
   <da>Aragonese dictionary for hunspell</da>
   <de>Aragonesisches Wörterbuch für “Hunspell”</de>
   <el>λεξικό αραγονικά για hunspell</el>
   <en>Aragonese dictionary for hunspell</en>
   <es_ES>Diccionario aragonés para hunspell</es_ES>
   <es>Diccionario Aragonés para hunspell</es>
   <et>Aragonese dictionary for hunspell</et>
   <eu>Aragonese dictionary for hunspell</eu>
   <fa>Aragonese dictionary for hunspell</fa>
   <fil_PH>Aragonese dictionary for hunspell</fil_PH>
   <fi>Aragonese dictionary for hunspell</fi>
   <fr_BE>Aragonais dictionnaire pour hunspell</fr_BE>
   <fr>Aragonais dictionnaire pour hunspell</fr>
   <gl_ES>Aragonese dictionary for hunspell</gl_ES>
   <gu>Aragonese dictionary for hunspell</gu>
   <he_IL>Aragonese dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु आरागॉन शब्दकोष</hi>
   <hr>Aragonese dictionary for hunspell</hr>
   <hu>Aragonese dictionary for hunspell</hu>
   <id>Aragonese dictionary for hunspell</id>
   <is>Aragonese dictionary for hunspell</is>
   <it>Dizionario aragonese per hunspell</it>
   <ja>Hunspell 用アラゴン語辞書</ja>
   <kk>Aragonese dictionary for hunspell</kk>
   <ko>Aragonese dictionary for hunspell</ko>
   <ku>Aragonese dictionary for hunspell</ku>
   <lt>Aragonese dictionary for hunspell</lt>
   <mk>Aragonese dictionary for hunspell</mk>
   <mr>Aragonese dictionary for hunspell</mr>
   <nb_NO>Aragonese dictionary for hunspell</nb_NO>
   <nb>Aragonesisk ordliste for hunspell</nb>
   <nl_BE>Aragonese dictionary for hunspell</nl_BE>
   <nl>Aragonees woordenboek voor hunspell</nl>
   <or>Aragonese dictionary for hunspell</or>
   <pl>Aragonese dictionary for hunspell</pl>
   <pt_BR>Dicionário Aragonês para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Aragonês para hunspell</pt>
   <ro>Aragonese dictionary for hunspell</ro>
   <ru>Aragonese dictionary for hunspell</ru>
   <sk>Aragonese dictionary for hunspell</sk>
   <sl>Aragonski slovar za hunspell</sl>
   <so>Aragonese dictionary for hunspell</so>
   <sq>Fjalor aragonisht për hunspell</sq>
   <sr>Aragonese dictionary for hunspell</sr>
   <sv>Aragonsk ordbok för hunspell</sv>
   <th>Aragonese dictionary for hunspell</th>
   <tr>Hunspell için Aragonca sözlük</tr>
   <uk>Aragonese dictionary for hunspell</uk>
   <vi>Aragonese dictionary for hunspell</vi>
   <zh_CN>Aragonese dictionary for hunspell</zh_CN>
   <zh_HK>Aragonese dictionary for hunspell</zh_HK>
   <zh_TW>Aragonese dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-an
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-an
</uninstall_package_names>

</app>
