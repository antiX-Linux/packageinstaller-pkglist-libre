<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
North Sámi
</name>

<description>
   <am>North Sámi dictionary for hunspell</am>
   <ar>North Sámi dictionary for hunspell</ar>
   <be>North Sámi dictionary for hunspell</be>
   <bg>North Sámi dictionary for hunspell</bg>
   <bn>North Sámi dictionary for hunspell</bn>
   <ca>Diccionari Sàmi del nord de per hunspell</ca>
   <cs>North Sámi dictionary for hunspell</cs>
   <da>North Sámi dictionary for hunspell</da>
   <de>Nordsamisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Βόρεια Σάμη για hunspell</el>
   <en>North Sámi dictionary for hunspell</en>
   <es_ES>Diccionario Sámi del Norte para hunspell</es_ES>
   <es>Diccionario Sámi del norte para hunspell</es>
   <et>North Sámi dictionary for hunspell</et>
   <eu>North Sámi dictionary for hunspell</eu>
   <fa>North Sámi dictionary for hunspell</fa>
   <fil_PH>North Sámi dictionary for hunspell</fil_PH>
   <fi>North Sámi dictionary for hunspell</fi>
   <fr_BE>Sámi du nord dictionnaire pour hunspell</fr_BE>
   <fr>Sámi du nord dictionnaire pour hunspell</fr>
   <gl_ES>North Sámi dictionary for hunspell</gl_ES>
   <gu>North Sámi dictionary for hunspell</gu>
   <he_IL>North Sámi dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु उत्तरी सामी शब्दकोष</hi>
   <hr>North Sámi dictionary for hunspell</hr>
   <hu>North Sámi dictionary for hunspell</hu>
   <id>North Sámi dictionary for hunspell</id>
   <is>North Sámi dictionary for hunspell</is>
   <it>Dizionario north sámi per hunspell</it>
   <ja>Hunspell 用北サーミ語辞書</ja>
   <kk>North Sámi dictionary for hunspell</kk>
   <ko>North Sámi dictionary for hunspell</ko>
   <ku>North Sámi dictionary for hunspell</ku>
   <lt>North Sámi dictionary for hunspell</lt>
   <mk>North Sámi dictionary for hunspell</mk>
   <mr>North Sámi dictionary for hunspell</mr>
   <nb_NO>North Sámi dictionary for hunspell</nb_NO>
   <nb>Nordsamisk ordliste for hunspell</nb>
   <nl_BE>North Sámi dictionary for hunspell</nl_BE>
   <nl>North Sámi dictionary for hunspell</nl>
   <or>North Sámi dictionary for hunspell</or>
   <pl>North Sámi dictionary for hunspell</pl>
   <pt_BR>Dicionário Sámi do Norte para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Sámi do Norte para hunspell</pt>
   <ro>North Sámi dictionary for hunspell</ro>
   <ru>North Sámi dictionary for hunspell</ru>
   <sk>North Sámi dictionary for hunspell</sk>
   <sl>Severni Sami slovar za hunspell</sl>
   <so>North Sámi dictionary for hunspell</so>
   <sq>Fjalor samishte veriore për hunspell</sq>
   <sr>North Sámi dictionary for hunspell</sr>
   <sv>Nordsamisk ordbok för hunspell</sv>
   <th>North Sámi dictionary for hunspell</th>
   <tr>Hunspell için Laponca sözlük</tr>
   <uk>North Sámi dictionary for hunspell</uk>
   <vi>North Sámi dictionary for hunspell</vi>
   <zh_CN>North Sámi dictionary for hunspell</zh_CN>
   <zh_HK>North Sámi dictionary for hunspell</zh_HK>
   <zh_TW>North Sámi dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-se
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-se
</uninstall_package_names>

</app>
