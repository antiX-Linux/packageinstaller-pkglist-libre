<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hungarian_Default_Firefox_esr
</name>

<description>
   <am>Hungarian localisation of Firefox ESR</am>
   <ar>Hungarian localisation of Firefox ESR</ar>
   <be>Hungarian localisation of Firefox ESR</be>
   <bg>Hungarian localisation of Firefox ESR</bg>
   <bn>Hungarian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Hongarès</ca>
   <cs>Hungarian localisation of Firefox ESR</cs>
   <da>Hungarian localisation of Firefox ESR</da>
   <de>Ungarische Lokalisation von “Firefox ESR”</de>
   <el>Ουγγρικά για τον Firefox ESR</el>
   <en>Hungarian localisation of Firefox ESR</en>
   <es_ES>Localización Hungaro de Firefox ESR</es_ES>
   <es>Localización Húngaro de Firefox ESR</es>
   <et>Hungarian localisation of Firefox ESR</et>
   <eu>Hungarian localisation of Firefox ESR</eu>
   <fa>Hungarian localisation of Firefox ESR</fa>
   <fil_PH>Hungarian localisation of Firefox ESR</fil_PH>
   <fi>Hungarian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en hongrois pour Firefox ESR</fr_BE>
   <fr>Localisation en hongrois pour Firefox ESR</fr>
   <gl_ES>Hungarian localisation of Firefox ESR</gl_ES>
   <gu>Hungarian localisation of Firefox ESR</gu>
   <he_IL>Hungarian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का हंगेरियाई संस्करण</hi>
   <hr>Hungarian localisation of Firefox ESR</hr>
   <hu>Hungarian localisation of Firefox ESR</hu>
   <id>Hungarian localisation of Firefox ESR</id>
   <is>Hungarian localisation of Firefox ESR</is>
   <it>Localizzazione ungherese di Firefox ESR</it>
   <ja>ブルガリア語版 Firefox ESR</ja>
   <kk>Hungarian localisation of Firefox ESR</kk>
   <ko>Hungarian localisation of Firefox ESR</ko>
   <ku>Hungarian localisation of Firefox ESR</ku>
   <lt>Hungarian localisation of Firefox ESR</lt>
   <mk>Hungarian localisation of Firefox ESR</mk>
   <mr>Hungarian localisation of Firefox ESR</mr>
   <nb_NO>Hungarian localisation of Firefox ESR</nb_NO>
   <nb>Ungarsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Hungarian localisation of Firefox ESR</nl_BE>
   <nl>Hungarian localisation of Firefox ESR</nl>
   <or>Hungarian localisation of Firefox ESR</or>
   <pl>Hungarian localisation of Firefox ESR</pl>
   <pt_BR>Húngaro Localização para o Firefox ESR</pt_BR>
   <pt>Húngaro Localização para Firefox ESR</pt>
   <ro>Hungarian localisation of Firefox ESR</ro>
   <ru>Hungarian localisation of Firefox ESR</ru>
   <sk>Hungarian localisation of Firefox ESR</sk>
   <sl>Madžarske krajevne nastavitve za Firefox ESR</sl>
   <so>Hungarian localisation of Firefox ESR</so>
   <sq>HPLIP dhe ekstra shtypjeje</sq>
   <sr>Hungarian localisation of Firefox ESR</sr>
   <sv>Ungersk lokalisering av Firefox ESR</sv>
   <th>Hungarian localisation of Firefox ESR</th>
   <tr>Firefox ESR Macarca yerelleştirmesi</tr>
   <uk>Hungarian localisation of Firefox ESR</uk>
   <vi>Hungarian localisation of Firefox ESR</vi>
   <zh_CN>Hungarian localisation of Firefox ESR</zh_CN>
   <zh_HK>Hungarian localisation of Firefox ESR</zh_HK>
   <zh_TW>Hungarian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-hu
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-hu
</uninstall_package_names>

</app>
