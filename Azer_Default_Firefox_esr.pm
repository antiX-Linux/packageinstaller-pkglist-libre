<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Azer_Default_Firefox_esr
</name>

<description>
   <am>Azerbaijani localisation of Firefox ESR</am>
   <ar>Azerbaijani localisation of Firefox ESR</ar>
   <be>Azerbaijani localisation of Firefox ESR</be>
   <bg>Azerbaijani localisation of Firefox ESR</bg>
   <bn>Azerbaijani localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Azerbaidjanès</ca>
   <cs>Azerbaijani localisation of Firefox ESR</cs>
   <da>Azerbaijani localisation of Firefox ESR</da>
   <de>Aserbaidschanische (ehem: Türkische) Lokalisation von “Firefox ESR”</de>
   <el>Αζερμπαϊτζάν για Firefox ESR</el>
   <en>Azerbaijani localisation of Firefox ESR</en>
   <es_ES>Localización Azerbaiyán de Firefox ESR</es_ES>
   <es>Localización Azerbaiyano de Firefox ESR</es>
   <et>Azerbaijani localisation of Firefox ESR</et>
   <eu>Azerbaijani localisation of Firefox ESR</eu>
   <fa>Azerbaijani localisation of Firefox ESR</fa>
   <fil_PH>Azerbaijani localisation of Firefox ESR</fil_PH>
   <fi>Azerbaijani localisation of Firefox ESR</fi>
   <fr_BE>Localisation en azerbaïdjanais pour Firefox ESR</fr_BE>
   <fr>Localisation en azerbaïdjanais pour Firefox ESR</fr>
   <gl_ES>Azerbaijani localisation of Firefox ESR</gl_ES>
   <gu>Azerbaijani localisation of Firefox ESR</gu>
   <he_IL>Azerbaijani localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का अज़रबैजानी संस्करण</hi>
   <hr>Azerbaijani localisation of Firefox ESR</hr>
   <hu>Azerbaijani localisation of Firefox ESR</hu>
   <id>Azerbaijani localisation of Firefox ESR</id>
   <is>Azerbaijani localisation of Firefox ESR</is>
   <it>Localizzazione azera di Firefox ESR</it>
   <ja>アゼルバイジャン語版 Firefox ESR</ja>
   <kk>Azerbaijani localisation of Firefox ESR</kk>
   <ko>Azerbaijani localisation of Firefox ESR</ko>
   <ku>Azerbaijani localisation of Firefox ESR</ku>
   <lt>Azerbaijani localisation of Firefox ESR</lt>
   <mk>Azerbaijani localisation of Firefox ESR</mk>
   <mr>Azerbaijani localisation of Firefox ESR</mr>
   <nb_NO>Azerbaijani localisation of Firefox ESR</nb_NO>
   <nb>Aserbadjansk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Azerbaijani localisation of Firefox ESR</nl_BE>
   <nl>Azerbeidzjaanse lokalisatie van Firefox ESR</nl>
   <or>Azerbaijani localisation of Firefox ESR</or>
   <pl>Azerbaijani localisation of Firefox ESR</pl>
   <pt_BR>Azerbaijão Localização para o Firefox ESR</pt_BR>
   <pt>Azeri Localização para Firefox ESR</pt>
   <ro>Azerbaijani localisation of Firefox ESR</ro>
   <ru>Azerbaijani localisation of Firefox ESR</ru>
   <sk>Azerbaijani localisation of Firefox ESR</sk>
   <sl>Azerbejdžanske krajevne nastavitve za Firefox ESR</sl>
   <so>Azerbaijani localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në azerbajxhanisht</sq>
   <sr>Azerbaijani localisation of Firefox ESR</sr>
   <sv>Azerbajdzjansk lokalisering av Firefox-ESR</sv>
   <th>Azerbaijani localisation of Firefox ESR</th>
   <tr>Firefox ESR Azerice yerelleştirmesi</tr>
   <uk>Azerbaijani localisation of Firefox ESR</uk>
   <vi>Azerbaijani localisation of Firefox ESR</vi>
   <zh_CN>Azerbaijani localisation of Firefox ESR</zh_CN>
   <zh_HK>Azerbaijani localisation of Firefox ESR</zh_HK>
   <zh_TW>Azerbaijani localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-az
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-az
</uninstall_package_names>

</app>
