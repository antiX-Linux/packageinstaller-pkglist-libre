<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Greek_Thunderbird
</name>

<description>
   <am>Greek localisation of Thunderbird</am>
   <ar>Greek localisation of Thunderbird</ar>
   <be>Greek localisation of Thunderbird</be>
   <bg>Greek localisation of Thunderbird</bg>
   <bn>Greek localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird per grec</ca>
   <cs>Greek localisation of Thunderbird</cs>
   <da>Græsk oversættelse af Thunderbird</da>
   <de>Griechische Lokalisierung von Thunderbird</de>
   <el>Ελληνικά για το Thunderbird</el>
   <en>Greek localisation of Thunderbird</en>
   <es_ES>Localización Griega de Thunderbird</es_ES>
   <es>Localización Griego de Thunderbird</es>
   <et>Greek localisation of Thunderbird</et>
   <eu>Greek localisation of Thunderbird</eu>
   <fa>Greek localisation of Thunderbird</fa>
   <fil_PH>Greek localisation of Thunderbird</fil_PH>
   <fi>Kreikkalainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en grec pour Thunderbird</fr_BE>
   <fr>Localisation en grec pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao grego</gl_ES>
   <gu>Greek localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird ליוונית</he_IL>
   <hi>थंडरबर्ड का यूनानी संस्करण</hi>
   <hr>Greek localisation of Thunderbird</hr>
   <hu>Greek localisation of Thunderbird</hu>
   <id>Greek localisation of Thunderbird</id>
   <is>Greek localisation of Thunderbird</is>
   <it>Localizzazione greca di Thunderbird</it>
   <ja>Thunderbird のギリシャ語版</ja>
   <kk>Greek localisation of Thunderbird</kk>
   <ko>Greek localisation of Thunderbird</ko>
   <ku>Greek localisation of Thunderbird</ku>
   <lt>Greek localisation of Thunderbird</lt>
   <mk>Greek localisation of Thunderbird</mk>
   <mr>Greek localisation of Thunderbird</mr>
   <nb_NO>Greek localisation of Thunderbird</nb_NO>
   <nb>Gresk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Greek localisation of Thunderbird</nl_BE>
   <nl>Griekse lokalisatie van Thunderbird</nl>
   <or>Greek localisation of Thunderbird</or>
   <pl>Grecka lokalizacja Thunderbirda</pl>
   <pt_BR>Grego Localização para o Thunderbird</pt_BR>
   <pt>Grego Localização para Thunderbird</pt>
   <ro>Greek localisation of Thunderbird</ro>
   <ru>Greek localisation of Thunderbird</ru>
   <sk>Greek localisation of Thunderbird</sk>
   <sl>Grška lokalizacija za Thunderbird</sl>
   <so>Greek localisation of Thunderbird</so>
   <sq>Fjalor greqisht për hunspell</sq>
   <sr>Greek localisation of Thunderbird</sr>
   <sv>Grekisk lokalisering för Thunderbird</sv>
   <th>Greek localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün Yunanca yerelleştirmesi</tr>
   <uk>Greek локалізація Thunderbird</uk>
   <vi>Greek localisation of Thunderbird</vi>
   <zh_CN>Thunderbird 希腊语语言包</zh_CN>
   <zh_HK>Greek localisation of Thunderbird</zh_HK>
   <zh_TW>Greek localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-el
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-el
</uninstall_package_names>

</app>
