<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Firefox-ESR
</name>

<description>
   <am>Latest Firefox-ESR</am>
   <ar>Latest Firefox-ESR</ar>
   <be>Latest Firefox-ESR</be>
   <bg>Latest Firefox-ESR</bg>
   <bn>Latest Firefox-ESR</bn>
   <ca>El darrer Firefox-ESR</ca>
   <cs>Latest Firefox-ESR</cs>
   <da>Seneste Firefox-ESR</da>
   <de>Aktueller Firefox-ESR</de>
   <el>Το τελευταίο Firefox-ESR</el>
   <en>Latest Firefox-ESR</en>
   <es_ES>Ultimo Firefox-ESR</es_ES>
   <es>Último Firefox-ESR</es>
   <et>Latest Firefox-ESR</et>
   <eu>Latest Firefox-ESR</eu>
   <fa>Latest Firefox-ESR</fa>
   <fil_PH>Latest Firefox-ESR</fil_PH>
   <fi>Viimeisin Firefox-ESR (pidennetyn tuen julkaisu)</fi>
   <fr_BE>Dernière version de Firefox-ESR</fr_BE>
   <fr>Dernière version de Firefox-ESR</fr>
   <gl_ES>Versión máis recenete do Firefox-ESR</gl_ES>
   <gu>Latest Firefox-ESR</gu>
   <he_IL>Latest Firefox-ESR</he_IL>
   <hi>फायरफॉक्स का विस्तारित समर्थन संस्करण</hi>
   <hr>Latest Firefox-ESR</hr>
   <hu>Latest Firefox-ESR</hu>
   <id>Latest Firefox-ESR</id>
   <is>Latest Firefox-ESR</is>
   <it>Ultimo Firefox-ESR</it>
   <ja>最新の Firefox 延長サポート版 (ESR)</ja>
   <kk>Latest Firefox-ESR</kk>
   <ko>Latest Firefox-ESR</ko>
   <ku>Latest Firefox-ESR</ku>
   <lt>Latest Firefox-ESR</lt>
   <mk>Latest Firefox-ESR</mk>
   <mr>Latest Firefox-ESR</mr>
   <nb_NO>Latest Firefox-ESR</nb_NO>
   <nb>Seneste Firefox-ESR</nb>
   <nl_BE>Latest Firefox-ESR</nl_BE>
   <nl>Nieuwste Firefox-ESR</nl>
   <or>Latest Firefox-ESR</or>
   <pl>Najnowszy Firefox-ESR</pl>
   <pt_BR>Firefox-ESR - Versão mais recente do navegador de internet</pt_BR>
   <pt>Versão mais recente do Firefox-ESR</pt>
   <ro>Latest Firefox-ESR</ro>
   <ru>Актуальный браузер Firefox ESR</ru>
   <sk>Latest Firefox-ESR</sk>
   <sl>Zadnji Firefox-ESR</sl>
   <so>Latest Firefox-ESR</so>
   <sq>Firefox më i ri-ESR</sq>
   <sr>Latest Firefox-ESR</sr>
   <sv>Senaste Firefox-ESR</sv>
   <th>Firefox-ESR รุ่นล่าสุด</th>
   <tr>En son Firefox-ESR</tr>
   <uk>Latest Firefox-ESR</uk>
   <vi>Latest Firefox-ESR</vi>
   <zh_CN>最新版 Firefox-ESR</zh_CN>
   <zh_HK>Latest Firefox-ESR</zh_HK>
   <zh_TW>Latest Firefox-ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-esr
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-esr
</uninstall_package_names>
</app>
