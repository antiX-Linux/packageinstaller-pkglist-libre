<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Desktop Environments
</category>

<name>
Xfce4-lite without elogind
</name>

<description>
   <am>Very minimal install of Xfce4</am>
   <ar>Very minimal install of Xfce4</ar>
   <be>Very minimal install of Xfce4</be>
   <bg>Very minimal install of Xfce4</bg>
   <bn>Very minimal install of Xfce4</bn>
   <ca>Instal·lació molt mínima de Xfce4</ca>
   <cs>Very minimal install of Xfce4</cs>
   <da>Very minimal install of Xfce4</da>
   <de>Sehr minimalistische Installation der Desktop-Umgebung “Xfce 4”</de>
   <el>Πολύ ελάχιστη εγκατάσταση του Xfce4</el>
   <en>Very minimal install of Xfce4</en>
   <es_ES>Instalación mínima de Xfce4</es_ES>
   <es>Instalación mínima de Xfce4</es>
   <et>Very minimal install of Xfce4</et>
   <eu>Very minimal install of Xfce4</eu>
   <fa>Very minimal install of Xfce4</fa>
   <fil_PH>Very minimal install of Xfce4</fil_PH>
   <fi>Hyvin minimaalinen Xfce4:n asennus.</fi>
   <fr_BE>Installation très minimale de Xfce4</fr_BE>
   <fr>Installation très minimale de Xfce4</fr>
   <gl_ES>Very minimal install of Xfce4</gl_ES>
   <gu>Very minimal install of Xfce4</gu>
   <he_IL>Very minimal install of Xfce4</he_IL>
   <hi>एक्सएफसीई4 का अति संक्षिप्त इंस्टॉल</hi>
   <hr>Very minimal install of Xfce4</hr>
   <hu>Very minimal install of Xfce4</hu>
   <id>Very minimal install of Xfce4</id>
   <is>Very minimal install of Xfce4</is>
   <it>Installazione molto minimale di Xfce4</it>
   <ja>Xfce 4 の最小インストール</ja>
   <kk>Very minimal install of Xfce4</kk>
   <ko>Very minimal install of Xfce4</ko>
   <ku>Very minimal install of Xfce4</ku>
   <lt>Very minimal install of Xfce4</lt>
   <mk>Very minimal install of Xfce4</mk>
   <mr>Very minimal install of Xfce4</mr>
   <nb_NO>Very minimal install of Xfce4</nb_NO>
   <nb>Minste mulige installasjon av Xfce4</nb>
   <nl_BE>Very minimal install of Xfce4</nl_BE>
   <nl>Very minimal install of Xfce4</nl>
   <or>Very minimal install of Xfce4</or>
   <pl>Very minimal install of Xfce4</pl>
   <pt_BR>Instalação mínima do Xfce4</pt_BR>
   <pt>Instalação muito mínima do Xfce4</pt>
   <ro>Very minimal install of Xfce4</ro>
   <ru>Very minimal install of Xfce4</ru>
   <sk>Very minimal install of Xfce4</sk>
   <sl>Zelo minimalna namestitev Xfce4</sl>
   <so>Very minimal install of Xfce4</so>
   <sq>Instalim shumë minimal i Xfce4-s</sq>
   <sr>Very minimal install of Xfce4</sr>
   <sv>Mycket minimal installation av Xfce4</sv>
   <th>Very minimal install of Xfce4</th>
   <tr>Xfce4 çok küçük kurulumu</tr>
   <uk>Very minimal install of Xfce4</uk>
   <vi>Very minimal install of Xfce4</vi>
   <zh_CN>Very minimal install of Xfce4</zh_CN>
   <zh_HK>Very minimal install of Xfce4</zh_HK>
   <zh_TW>Very minimal install of Xfce4</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
xfce4
thunar-volman
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
xfce4
thunar-volman
</uninstall_package_names>
</app>
