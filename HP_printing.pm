<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Printing
</category>

<name>
HP Printing extras
</name>

<description>
   <am>HPLIP and printing extras</am>
   <ar>HPLIP and printing extras</ar>
   <be>HPLIP and printing extras</be>
   <bg>HPLIP and printing extras</bg>
   <bn>HPLIP and printing extras</bn>
   <ca>HPLIP i extres d'impressió</ca>
   <cs>HPLIP and printing extras</cs>
   <da>Ekstra til HPLIP og udskrivning</da>
   <de>HPLIP (HP's Linux Imaging and Printing) und Druck-Extras</de>
   <el>HPLIP και εκτύπωση</el>
   <en>HPLIP and printing extras</en>
   <es_ES>HPLIP y complementos de impresión</es_ES>
   <es>HPLIP y complementos de impresión</es>
   <et>HPLIP and printing extras</et>
   <eu>HPLIP and printing extras</eu>
   <fa>HPLIP and printing extras</fa>
   <fil_PH>HPLIP and printing extras</fil_PH>
   <fi>HPLIP ja tulostukseen liittyvät lisäohjelmat</fi>
   <fr_BE>HPLIP et utilitaires d'impression</fr_BE>
   <fr>HPLIP et utilitaires d'impression</fr>
   <gl_ES>Creación de imaxes e impresión para computadores HP con Linux e extras de impresión</gl_ES>
   <gu>HPLIP and printing extras</gu>
   <he_IL>HPLIP and printing extras</he_IL>
   <hi>HPLIP व अतिरिक्त प्रिंटिंग सॉफ्टवेयर</hi>
   <hr>HPLIP and printing extras</hr>
   <hu>HPLIP and printing extras</hu>
   <id>HPLIP and printing extras</id>
   <is>HPLIP and printing extras</is>
   <it>HPLIP ed ulteriori pacchetti per la stampa</it>
   <ja>HPLIP と印刷用エキストラ</ja>
   <kk>HPLIP and printing extras</kk>
   <ko>HPLIP and printing extras</ko>
   <ku>HPLIP and printing extras</ku>
   <lt>HPLIP ir spausdinimo papildiniai</lt>
   <mk>HPLIP and printing extras</mk>
   <mr>HPLIP and printing extras</mr>
   <nb_NO>HPLIP and printing extras</nb_NO>
   <nb>HPLIP og ekstra utskriftsprogrammer</nb>
   <nl_BE>HPLIP and printing extras</nl_BE>
   <nl>HPLIP en printing extras</nl>
   <or>HPLIP and printing extras</or>
   <pl>HPLIP i dodatki do drukowania</pl>
   <pt_BR>Criação de imagens e impressão para computadores HP com Linux (HP Linux Imaging and Printing) e extras de impressão</pt_BR>
   <pt>Criação de imagens e impressão para computadores HP com Linux [HP Linux Imaging and Printing] e extras de impressão</pt>
   <ro>HPLIP and printing extras</ro>
   <ru>HPLIP и вспомогательные компоненты печати</ru>
   <sk>HPLIP and printing extras</sk>
   <sl>Dodatki za HPLIP in tiskanje</sl>
   <so>HPLIP and printing extras</so>
   <sq>Fjalor hindisht për hunspell</sq>
   <sr>HPLIP and printing extras</sr>
   <sv>HPLIP och utskrifts-extras</sv>
   <th>HPLIP และเครื่องมือการพิมพ์เพิ่มเติม</th>
   <tr>HPLIP ve yazdırma ile ilgili ekler</tr>
   <uk>HPLIP and printing extras</uk>
   <vi>HPLIP and printing extras</vi>
   <zh_CN>HPLIP 与额外打印工具</zh_CN>
   <zh_HK>HPLIP and printing extras</zh_HK>
   <zh_TW>HPLIP and printing extras</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
printer-driver-hpijs
hpijs-ppds
hplip-data
hplip-gui
cups
cups-pdf
cups-client
magicfilter
gv
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
printer-driver-hpijs
hpijs-ppds
hplip-data
hplip-gui
magicfilter
gv
</uninstall_package_names>
</app>
