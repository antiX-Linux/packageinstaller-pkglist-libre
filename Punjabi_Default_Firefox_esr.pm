<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Punjabi_Default_Firefox_esr
</name>

<description>
   <am>Punjabi localisation of Firefox ESR</am>
   <ar>Punjabi localisation of Firefox ESR</ar>
   <be>Punjabi localisation of Firefox ESR</be>
   <bg>Punjabi localisation of Firefox ESR</bg>
   <bn>Punjabi localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Punjabí</ca>
   <cs>Punjabi localisation of Firefox ESR</cs>
   <da>Punjabi localisation of Firefox ESR</da>
   <de>Pakistanische und Indische Lokalisation für die Sprache “Pandschabisch” von “Firefox ESR”</de>
   <el>Εντοπισμός Punjabi του Firefox ESR</el>
   <en>Punjabi localisation of Firefox ESR</en>
   <es_ES>Localización Punjabi de Firefox ESR</es_ES>
   <es>Localización Punjabi de Firefox ESR</es>
   <et>Punjabi localisation of Firefox ESR</et>
   <eu>Punjabi localisation of Firefox ESR</eu>
   <fa>Punjabi localisation of Firefox ESR</fa>
   <fil_PH>Punjabi localisation of Firefox ESR</fil_PH>
   <fi>Punjabi localisation of Firefox ESR</fi>
   <fr_BE>Localisation en punjabi pour Firefox ESR</fr_BE>
   <fr>Localisation en punjabi pour Firefox ESR</fr>
   <gl_ES>Punjabi localisation of Firefox ESR</gl_ES>
   <gu>Punjabi localisation of Firefox ESR</gu>
   <he_IL>Punjabi localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का पंजाबी संस्करण</hi>
   <hr>Punjabi localisation of Firefox ESR</hr>
   <hu>Punjabi localisation of Firefox ESR</hu>
   <id>Punjabi localisation of Firefox ESR</id>
   <is>Punjabi localisation of Firefox ESR</is>
   <it>Localizzazione punjabi di Firefox ESR</it>
   <ja>パンジャーブ語版 Firefox ESR</ja>
   <kk>Punjabi localisation of Firefox ESR</kk>
   <ko>Punjabi localisation of Firefox ESR</ko>
   <ku>Punjabi localisation of Firefox ESR</ku>
   <lt>Punjabi localisation of Firefox ESR</lt>
   <mk>Punjabi localisation of Firefox ESR</mk>
   <mr>Punjabi localisation of Firefox ESR</mr>
   <nb_NO>Punjabi localisation of Firefox ESR</nb_NO>
   <nb>Punjabi lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Punjabi localisation of Firefox ESR</nl_BE>
   <nl>Punjabi localisation of Firefox ESR</nl>
   <or>Punjabi localisation of Firefox ESR</or>
   <pl>Punjabi localisation of Firefox ESR</pl>
   <pt_BR>Punjabi Localização para o Firefox ESR</pt_BR>
   <pt>Panjabi Localização para Firefox ESR</pt>
   <ro>Punjabi localisation of Firefox ESR</ro>
   <ru>Punjabi localisation of Firefox ESR</ru>
   <sk>Punjabi localisation of Firefox ESR</sk>
   <sl>Pundžabi krajevne nastavitve za Firefox ESR</sl>
   <so>Punjabi localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në panxhabi</sq>
   <sr>Punjabi localisation of Firefox ESR</sr>
   <sv>Punjabi lokalisering för Firefox ESR</sv>
   <th>Punjabi localisation of Firefox ESR</th>
   <tr>Firefox ESR Pencapça yerelleştirmesi</tr>
   <uk>Punjabi localisation of Firefox ESR</uk>
   <vi>Punjabi localisation of Firefox ESR</vi>
   <zh_CN>Punjabi localisation of Firefox ESR</zh_CN>
   <zh_HK>Punjabi localisation of Firefox ESR</zh_HK>
   <zh_TW>Punjabi localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-pa-in
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-pa-in
</uninstall_package_names>

</app>
