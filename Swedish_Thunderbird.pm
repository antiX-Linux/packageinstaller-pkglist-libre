<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Swedish_Thunderbird
</name>

<description>
   <am>Swedish localisation of Thunderbird</am>
   <ar>Swedish localisation of Thunderbird</ar>
   <be>Swedish localisation of Thunderbird</be>
   <bg>Swedish localisation of Thunderbird</bg>
   <bn>Swedish localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Suec</ca>
   <cs>Swedish localisation of Thunderbird</cs>
   <da>Svensk oversættelse af Thunderbird</da>
   <de>Schwedische Lokalisierung von Thunderbird</de>
   <el>Σουηδικά για το Thunderbird</el>
   <en>Swedish localisation of Thunderbird</en>
   <es_ES>Localización Sueca de Thunderbird</es_ES>
   <es>Localización Sueco de Thunderbird</es>
   <et>Swedish localisation of Thunderbird</et>
   <eu>Swedish localisation of Thunderbird</eu>
   <fa>Swedish localisation of Thunderbird</fa>
   <fil_PH>Swedish localisation of Thunderbird</fil_PH>
   <fi>Ruotsalainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation en suédois pour Thunderbird</fr_BE>
   <fr>Localisation en suédois pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao sueco</gl_ES>
   <gu>Swedish localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לשוודית</he_IL>
   <hi>थंडरबर्ड का स्वीडिश संस्करण</hi>
   <hr>Swedish localisation of Thunderbird</hr>
   <hu>Swedish localisation of Thunderbird</hu>
   <id>Swedish localisation of Thunderbird</id>
   <is>Swedish localisation of Thunderbird</is>
   <it>Localizzazione svedese di Thunderbird</it>
   <ja>Thunderbird のスウェーデン語版</ja>
   <kk>Swedish localisation of Thunderbird</kk>
   <ko>Swedish localisation of Thunderbird</ko>
   <ku>Swedish localisation of Thunderbird</ku>
   <lt>Swedish localisation of Thunderbird</lt>
   <mk>Swedish localisation of Thunderbird</mk>
   <mr>Swedish localisation of Thunderbird</mr>
   <nb_NO>Swedish localisation of Thunderbird</nb_NO>
   <nb>Svensk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Swedish localisation of Thunderbird</nl_BE>
   <nl>Zweedse lokalisatie van Thunderbird</nl>
   <or>Swedish localisation of Thunderbird</or>
   <pl>Szwedzka lokalizacja Thunderbirda</pl>
   <pt_BR>Sueco Localização para o Thunderbird</pt_BR>
   <pt>Sueco Localização para Thunderbird</pt>
   <ro>Swedish localisation of Thunderbird</ro>
   <ru>Swedish localisation of Thunderbird</ru>
   <sk>Swedish localisation of Thunderbird</sk>
   <sl>Švedske krajevne nastavitve za Thunderbird</sl>
   <so>Swedish localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në suedisht</sq>
   <sr>Swedish localisation of Thunderbird</sr>
   <sv>Svensk lokalisering av Thunderbird</sv>
   <th>Swedish localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün İsveççe yerelleştirmesi</tr>
   <uk>Swedish localisation of Thunderbird</uk>
   <vi>Swedish localisation of Thunderbird</vi>
   <zh_CN>Swedish localisation of Thunderbird</zh_CN>
   <zh_HK>Swedish localisation of Thunderbird</zh_HK>
   <zh_TW>Swedish localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-sv-se
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-sv-se
</uninstall_package_names>

</app>
