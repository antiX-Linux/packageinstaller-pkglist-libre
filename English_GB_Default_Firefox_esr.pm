<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
English_GB_Default_Firefox_esr
</name>

<description>
   <am>GB English localisation of Firefox ESR</am>
   <ar>GB English localisation of Firefox ESR</ar>
   <be>GB English localisation of Firefox ESR</be>
   <bg>GB English localisation of Firefox ESR</bg>
   <bn>GB English localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Anglès (GB)</ca>
   <cs>GB English localisation of Firefox ESR</cs>
   <da>GB English localisation of Firefox ESR</da>
   <de>Englische Lokalisation für Großbritannien von “Firefox ESR”</de>
   <el>Αγγλικά (ΜΒ)  για Firefox ESR</el>
   <en>GB English localisation of Firefox ESR</en>
   <es_ES>Localización Inglés (GB) de Firefox ESR</es_ES>
   <es>Localización Inglés (GB) de Firefox ESR</es>
   <et>GB English localisation of Firefox ESR</et>
   <eu>GB English localisation of Firefox ESR</eu>
   <fa>GB English localisation of Firefox ESR</fa>
   <fil_PH>GB English localisation of Firefox ESR</fil_PH>
   <fi>GB English localisation of Firefox ESR</fi>
   <fr_BE>Localisation en anglais (GB) pour Firefox ESR</fr_BE>
   <fr>Localisation en anglais (GB) pour Firefox ESR</fr>
   <gl_ES>GB English localisation of Firefox ESR</gl_ES>
   <gu>GB English localisation of Firefox ESR</gu>
   <he_IL>GB English localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का ब्रिटिश अंग्रेज़ी संस्करण</hi>
   <hr>GB English localisation of Firefox ESR</hr>
   <hu>GB English localisation of Firefox ESR</hu>
   <id>GB English localisation of Firefox ESR</id>
   <is>GB English localisation of Firefox ESR</is>
   <it>Localizzazione in inglese GB di Firefox ESR</it>
   <ja>イギリス英語版 Firefox ESR</ja>
   <kk>GB English localisation of Firefox ESR</kk>
   <ko>GB English localisation of Firefox ESR</ko>
   <ku>GB English localisation of Firefox ESR</ku>
   <lt>GB English localisation of Firefox ESR</lt>
   <mk>GB English localisation of Firefox ESR</mk>
   <mr>GB English localisation of Firefox ESR</mr>
   <nb_NO>GB English localisation of Firefox ESR</nb_NO>
   <nb>Britisk engelsk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>GB English localisation of Firefox ESR</nl_BE>
   <nl>GB Engelse lokalisatie van Firefox ESR</nl>
   <or>GB English localisation of Firefox ESR</or>
   <pl>GB English localisation of Firefox ESR</pl>
   <pt_BR>Inglês da Grã-Bretanha Localização para o Firefox ESR</pt_BR>
   <pt>Inglês GB Localização para Firefox ESR</pt>
   <ro>GB English localisation of Firefox ESR</ro>
   <ru>GB English localisation of Firefox ESR</ru>
   <sk>GB English localisation of Firefox ESR</sk>
   <sl>VB angleške krajevne nastavitve za Firefox ESR</sl>
   <so>GB English localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në anglishte britanike</sq>
   <sr>GB English localisation of Firefox ESR</sr>
   <sv>GB Engelsk lokalisering av Firefox ESR</sv>
   <th>GB English localisation of Firefox ESR</th>
   <tr>Firefox ESR Britanya İngilizcesi yerelleştirmesi</tr>
   <uk>GB English localisation of Firefox ESR</uk>
   <vi>GB English localisation of Firefox ESR</vi>
   <zh_CN>GB English localisation of Firefox ESR</zh_CN>
   <zh_HK>GB English localisation of Firefox ESR</zh_HK>
   <zh_TW>GB English localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-en-gb
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-en-gb
</uninstall_package_names>

</app>
