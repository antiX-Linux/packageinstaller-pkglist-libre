<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Kurmanji
</name>

<description>
   <am>Kurmanji dictionary for hunspell</am>
   <ar>Kurmanji dictionary for hunspell</ar>
   <be>Kurmanji dictionary for hunspell</be>
   <bg>Kurmanji dictionary for hunspell</bg>
   <bn>Kurmanji dictionary for hunspell</bn>
   <ca>Diccionari Kumanji per hunspell</ca>
   <cs>Kurmanji dictionary for hunspell</cs>
   <da>Kurmanji dictionary for hunspell</da>
   <de>Kurdisches (kurmandschi) Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Kurmanji για hunspell</el>
   <en>Kurmanji dictionary for hunspell</en>
   <es_ES>Diccionario Kurmanji para hunspell</es_ES>
   <es>Diccionario Kurmanji para hunspell</es>
   <et>Kurmanji dictionary for hunspell</et>
   <eu>Kurmanji dictionary for hunspell</eu>
   <fa>Kurmanji dictionary for hunspell</fa>
   <fil_PH>Kurmanji dictionary for hunspell</fil_PH>
   <fi>Kurmanji dictionary for hunspell</fi>
   <fr_BE>Kurmanji dictionnaire pour hunspell</fr_BE>
   <fr>Kurmanji dictionnaire pour hunspell</fr>
   <gl_ES>Kurmanji dictionary for hunspell</gl_ES>
   <gu>Kurmanji dictionary for hunspell</gu>
   <he_IL>Kurmanji dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु कुरमानी शब्दकोष</hi>
   <hr>Kurmanji dictionary for hunspell</hr>
   <hu>Kurmanji dictionary for hunspell</hu>
   <id>Kurmanji dictionary for hunspell</id>
   <is>Kurmanji dictionary for hunspell</is>
   <it>Dizionario kurmanji per hunspell</it>
   <ja>Hhunspell 用クルマンジ語辞書</ja>
   <kk>Kurmanji dictionary for hunspell</kk>
   <ko>Kurmanji dictionary for hunspell</ko>
   <ku>Kurmanji dictionary for hunspell</ku>
   <lt>Kurmanji dictionary for hunspell</lt>
   <mk>Kurmanji dictionary for hunspell</mk>
   <mr>Kurmanji dictionary for hunspell</mr>
   <nb_NO>Kurmanji dictionary for hunspell</nb_NO>
   <nb>Kurmanji ordliste for hunspell</nb>
   <nl_BE>Kurmanji dictionary for hunspell</nl_BE>
   <nl>Kurmanji dictionary for hunspell</nl>
   <or>Kurmanji dictionary for hunspell</or>
   <pl>Kurmanji dictionary for hunspell</pl>
   <pt_BR>Dicionário Curmânji para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Curmânji para hunspell</pt>
   <ro>Kurmanji dictionary for hunspell</ro>
   <ru>Kurmanji dictionary for hunspell</ru>
   <sk>Kurmanji dictionary for hunspell</sk>
   <sl>Kurmanjski slovar za hunspell</sl>
   <so>Kurmanji dictionary for hunspell</so>
   <sq>Fjalor kurmanxhi për hunspell</sq>
   <sr>Kurmanji dictionary for hunspell</sr>
   <sv>Kurmanji ordbok för hunspell</sv>
   <th>Kurmanji dictionary for hunspell</th>
   <tr>Hunspell için Kürtçe sözlük</tr>
   <uk>Kurmanji dictionary for hunspell</uk>
   <vi>Kurmanji dictionary for hunspell</vi>
   <zh_CN>Kurmanji dictionary for hunspell</zh_CN>
   <zh_HK>Kurmanji dictionary for hunspell</zh_HK>
   <zh_TW>Kurmanji dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-kmr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-kmr
</uninstall_package_names>

</app>
