<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Occitan
</name>

<description>
   <am>Occitan dictionary for hunspell</am>
   <ar>Occitan dictionary for hunspell</ar>
   <be>Occitan dictionary for hunspell</be>
   <bg>Occitan dictionary for hunspell</bg>
   <bn>Occitan dictionary for hunspell</bn>
   <ca>Diccionari Occità per hunspell</ca>
   <cs>Occitan dictionary for hunspell</cs>
   <da>Occitan dictionary for hunspell</da>
   <de>Okzitanisches Wörterbuch für “Hunspell”</de>
   <el>Οξιτανικό λεξικό για hunspell</el>
   <en>Occitan dictionary for hunspell</en>
   <es_ES>Diccionario Occitano para hunspell</es_ES>
   <es>Diccionario Occitano para hunspell</es>
   <et>Occitan dictionary for hunspell</et>
   <eu>Occitan dictionary for hunspell</eu>
   <fa>Occitan dictionary for hunspell</fa>
   <fil_PH>Occitan dictionary for hunspell</fil_PH>
   <fi>Occitan dictionary for hunspell</fi>
   <fr_BE>Occitan dictionnaire pour hunspell</fr_BE>
   <fr>Occitan dictionnaire pour hunspell</fr>
   <gl_ES>Occitan dictionary for hunspell</gl_ES>
   <gu>Occitan dictionary for hunspell</gu>
   <he_IL>Occitan dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु ओसीटान शब्दकोष</hi>
   <hr>Occitan dictionary for hunspell</hr>
   <hu>Occitan dictionary for hunspell</hu>
   <id>Occitan dictionary for hunspell</id>
   <is>Occitan dictionary for hunspell</is>
   <it>Dizionario della lingua occitana per hunspell</it>
   <ja>Hunspell 用オック語辞書</ja>
   <kk>Occitan dictionary for hunspell</kk>
   <ko>Occitan dictionary for hunspell</ko>
   <ku>Occitan dictionary for hunspell</ku>
   <lt>Occitan dictionary for hunspell</lt>
   <mk>Occitan dictionary for hunspell</mk>
   <mr>Occitan dictionary for hunspell</mr>
   <nb_NO>Occitan dictionary for hunspell</nb_NO>
   <nb>Occitansk ordliste for hunspell</nb>
   <nl_BE>Occitan dictionary for hunspell</nl_BE>
   <nl>Occitan dictionary for hunspell</nl>
   <or>Occitan dictionary for hunspell</or>
   <pl>Occitan dictionary for hunspell</pl>
   <pt_BR>Dicionário Occitana para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Occitano para hunspell</pt>
   <ro>Occitan dictionary for hunspell</ro>
   <ru>Occitan dictionary for hunspell</ru>
   <sk>Occitan dictionary for hunspell</sk>
   <sl>Okitanski slovar za hunspell</sl>
   <so>Occitan dictionary for hunspell</so>
   <sq>Fjalor oçitanisht për hunspell</sq>
   <sr>Occitan dictionary for hunspell</sr>
   <sv>Occitansk ordbok för hunspell</sv>
   <th>Occitan dictionary for hunspell</th>
   <tr>Hunspell için Oksitanca sözlük</tr>
   <uk>Occitan dictionary for hunspell</uk>
   <vi>Occitan dictionary for hunspell</vi>
   <zh_CN>Occitan dictionary for hunspell</zh_CN>
   <zh_HK>Occitan dictionary for hunspell</zh_HK>
   <zh_TW>Occitan dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
hunspell-oc
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-oc
</uninstall_package_names>

</app>
