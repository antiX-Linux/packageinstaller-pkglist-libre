<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Marathi_Default_Firefox_esr
</name>

<description>
   <am>Marathi localisation of Firefox ESR</am>
   <ar>Marathi localisation of Firefox ESR</ar>
   <be>Marathi localisation of Firefox ESR</be>
   <bg>Marathi localisation of Firefox ESR</bg>
   <bn>Marathi localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Marathi</ca>
   <cs>Marathi localisation of Firefox ESR</cs>
   <da>Marathi localisation of Firefox ESR</da>
   <de>Indische Lokalisation für die Sprache “Marathi” von “Firefox ESR”</de>
   <el>Μαράθι για Firefox ESR</el>
   <en>Marathi localisation of Firefox ESR</en>
   <es_ES>Localización Maratí de Firefox ESR</es_ES>
   <es>Localización Maratí de Firefox ESR</es>
   <et>Marathi localisation of Firefox ESR</et>
   <eu>Marathi localisation of Firefox ESR</eu>
   <fa>Marathi localisation of Firefox ESR</fa>
   <fil_PH>Marathi localisation of Firefox ESR</fil_PH>
   <fi>Marathi localisation of Firefox ESR</fi>
   <fr_BE>Localisation en marathi pour Firefox ESR</fr_BE>
   <fr>Localisation en marathi pour Firefox ESR</fr>
   <gl_ES>Marathi localisation of Firefox ESR</gl_ES>
   <gu>Marathi localisation of Firefox ESR</gu>
   <he_IL>Marathi localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का मराठी संस्करण</hi>
   <hr>Marathi localisation of Firefox ESR</hr>
   <hu>Marathi localisation of Firefox ESR</hu>
   <id>Marathi localisation of Firefox ESR</id>
   <is>Marathi localisation of Firefox ESR</is>
   <it>Localizzazione marathi di Firefox ESR</it>
   <ja>マラーティー語版 Firefox ESR</ja>
   <kk>Marathi localisation of Firefox ESR</kk>
   <ko>Marathi localisation of Firefox ESR</ko>
   <ku>Marathi localisation of Firefox ESR</ku>
   <lt>Marathi localisation of Firefox ESR</lt>
   <mk>Marathi localisation of Firefox ESR</mk>
   <mr>Marathi localisation of Firefox ESR</mr>
   <nb_NO>Marathi localisation of Firefox ESR</nb_NO>
   <nb>Marati lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Marathi localisation of Firefox ESR</nl_BE>
   <nl>Marathi localisation of Firefox ESR</nl>
   <or>Marathi localisation of Firefox ESR</or>
   <pl>Marathi localisation of Firefox ESR</pl>
   <pt_BR>Marata Localização para o Firefox ESR</pt_BR>
   <pt>Marati Localização para Firefox ESR</pt>
   <ro>Marathi localisation of Firefox ESR</ro>
   <ru>Marathi localisation of Firefox ESR</ru>
   <sk>Marathi localisation of Firefox ESR</sk>
   <sl>Marathi krajevne nastavitve za Firefox ESR</sl>
   <so>Marathi localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në marati</sq>
   <sr>Marathi localisation of Firefox ESR</sr>
   <sv>Marathi lokalisering av Firefox ESR</sv>
   <th>Marathi localisation of Firefox ESR</th>
   <tr>Firefox ESR Marathice yerelleştirmesi</tr>
   <uk>Marathi localisation of Firefox ESR</uk>
   <vi>Marathi localisation of Firefox ESR</vi>
   <zh_CN>Marathi localisation of Firefox ESR</zh_CN>
   <zh_HK>Marathi localisation of Firefox ESR</zh_HK>
   <zh_TW>Marathi localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-mr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-mr
</uninstall_package_names>

</app>
