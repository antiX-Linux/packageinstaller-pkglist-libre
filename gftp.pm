<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
FTP
</category>

<name>
gftp
</name>

<description>
   <am>a multithreaded FTP client</am>
   <ar>a multithreaded FTP client</ar>
   <be>a multithreaded FTP client</be>
   <bg>a multithreaded FTP client</bg>
   <bn>a multithreaded FTP client</bn>
   <ca>un client de FTP multifilar</ca>
   <cs>a multithreaded FTP client</cs>
   <da>en multitrådet FTP-klient</da>
   <de>Ein Multithreading-FTP-Client</de>
   <el>Ένα πολυνηματικό πρόγραμμα FTP</el>
   <en>a multithreaded FTP client</en>
   <es_ES>Cliente FTP multiproceso</es_ES>
   <es>Cliente FTP multiproceso</es>
   <et>a multithreaded FTP client</et>
   <eu>a multithreaded FTP client</eu>
   <fa>a multithreaded FTP client</fa>
   <fil_PH>a multithreaded FTP client</fil_PH>
   <fi>monisäikeinen FTP-asiakasohjelma</fi>
   <fr_BE>Client FTP multiprocessus</fr_BE>
   <fr>Client FTP multiprocessus</fr>
   <gl_ES>Cliente de FTP multiliña de execución</gl_ES>
   <gu>a multithreaded FTP client</gu>
   <he_IL>a multithreaded FTP client</he_IL>
   <hi>एकाधिक थ्रेड कार्यक्षमता युक्त एफटीपी साधन</hi>
   <hr>a multithreaded FTP client</hr>
   <hu>a multithreaded FTP client</hu>
   <id>a multithreaded FTP client</id>
   <is>a multithreaded FTP client</is>
   <it>Client FTP multithread</it>
   <ja>マルチスレッドの FTP クライアント</ja>
   <kk>a multithreaded FTP client</kk>
   <ko>a multithreaded FTP client</ko>
   <ku>a multithreaded FTP client</ku>
   <lt>a multithreaded FTP client</lt>
   <mk>a multithreaded FTP client</mk>
   <mr>a multithreaded FTP client</mr>
   <nb_NO>a multithreaded FTP client</nb_NO>
   <nb>flertrådet FTP-klient</nb>
   <nl_BE>a multithreaded FTP client</nl_BE>
   <nl>een multithreaded FTP programma</nl>
   <or>a multithreaded FTP client</or>
   <pl>wielowątkowy klient FTP</pl>
   <pt_BR>Cliente de FTP multilinha de execução (multithreaded)</pt_BR>
   <pt>Cliente de FTP multilinha de execução (multithreaded)</pt>
   <ro>a multithreaded FTP client</ro>
   <ru>Многопоточный FTP клиент</ru>
   <sk>a multithreaded FTP client</sk>
   <sl>večnitni FTP odjemalec</sl>
   <so>a multithreaded FTP client</so>
   <sq>Përkthimi i Thunderbird-it në gjermanisht</sq>
   <sr>a multithreaded FTP client</sr>
   <sv>en multitråds FTP-klient</sv>
   <th>FTP Client แบบ multi-threaded</th>
   <tr>çoklu işleyebilen bir FTP istemcisi</tr>
   <uk>багатопотоковий FTP-клієнт</uk>
   <vi>a multithreaded FTP client</vi>
   <zh_CN>一个多线程 FTP 客户端</zh_CN>
   <zh_HK>a multithreaded FTP client</zh_HK>
   <zh_TW>a multithreaded FTP client</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gftp-gtk
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gftp-gtk
</uninstall_package_names>
</app>
