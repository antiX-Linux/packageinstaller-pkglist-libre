<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Email
</category>

<name>
Claws-Mail
</name>

<description>
   <am>Claws Mail lightweight email client</am>
   <ar>Claws Mail lightweight email client</ar>
   <be>Claws Mail lightweight email client</be>
   <bg>Claws Mail lightweight email client</bg>
   <bn>Claws Mail lightweight email client</bn>
   <ca>Client de correu lleuger Claws Mail</ca>
   <cs>Claws Mail lightweight email client</cs>
   <da>Claws Mail letsvægts e-mailklient</da>
   <de>Claws Mail leichtgewichtiger E-Mail-Client</de>
   <el>Ελαφρύ πρόγραμμα ηλεκτρονικού ταχυδρομείου</el>
   <en>Claws Mail lightweight email client</en>
   <es_ES>Cliente de correo electrónico liviano Claws Mail</es_ES>
   <es>Cliente de correo electrónico liviano Claws Mail</es>
   <et>Claws Mail lightweight email client</et>
   <eu>Claws Mail lightweight email client</eu>
   <fa>Claws Mail lightweight email client</fa>
   <fil_PH>Claws Mail lightweight email client</fil_PH>
   <fi>Claws Mail on keveä sähköposti-asiakasohjelma</fi>
   <fr_BE>Client mail léger Claws Mail</fr_BE>
   <fr>Client mail léger Claws Mail</fr>
   <gl_ES>Cliente de correo-electrónico simple e lixiero</gl_ES>
   <gu>Claws Mail lightweight email client</gu>
   <he_IL>Claws Mail lightweight email client</he_IL>
   <hi>Claws Mail सरल ईमेल साधन</hi>
   <hr>Claws Mail lightweight email client</hr>
   <hu>Claws Mail lightweight email client</hu>
   <id>Claws Mail lightweight email client</id>
   <is>Claws Mail lightweight email client</is>
   <it>Claws Mail, client email leggero</it>
   <ja>Claws Mail - 軽量な電子メーラー</ja>
   <kk>Claws Mail lightweight email client</kk>
   <ko>Claws Mail lightweight email client</ko>
   <ku>Claws Mail lightweight email client</ku>
   <lt>Claws Mail supaprastintas el. pašto klientas</lt>
   <mk>Claws Mail lightweight email client</mk>
   <mr>Claws Mail lightweight email client</mr>
   <nb_NO>Claws Mail lightweight email client</nb_NO>
   <nb>Claws Mail, lettvektig e-postklient</nb>
   <nl_BE>Claws Mail lightweight email client</nl_BE>
   <nl>Claws Mail lichtgewicht email programma</nl>
   <or>Claws Mail lightweight email client</or>
   <pl>Claws Mail lekki klient poczty e-mail</pl>
   <pt_BR>Claws Mail - Cliente de e-mail simples e leve</pt_BR>
   <pt>Cliente de correio-electrónico simples e ligeiro</pt>
   <ro>Claws Mail lightweight email client</ro>
   <ru>Легковесный e-mail клиент</ru>
   <sk>Claws Mail lightweight email client</sk>
   <sl>Lahek poštni odjemalec Clwas Mail</sl>
   <so>Claws Mail lightweight email client</so>
   <sq>Klienti email i peshës së lehtë Claws Mail</sq>
   <sr>Claws Mail lightweight email client</sr>
   <sv>Claws Mail lättvikts E-mejl klient</sv>
   <th>Claws โปรแกรมอีเมลน้ำหนักเบา</th>
   <tr>Claws Mail hafif e-posta istemcisi</tr>
   <uk>Claws Mail легка програма для роботи з електронною поштою</uk>
   <vi>Claws Mail lightweight email client</vi>
   <zh_CN>Claws Mail 轻量级邮件客户端</zh_CN>
   <zh_HK>Claws Mail lightweight email client</zh_HK>
   <zh_TW>Claws Mail lightweight email client</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
claws-mail
claws-mail-i18n
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
claws-mail
claws-mail-i18n
</uninstall_package_names>
</app>
