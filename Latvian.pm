<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Latvian
</name>

<description>
   <am>Latvian dictionary for hunspell</am>
   <ar>Latvian dictionary for hunspell</ar>
   <be>Latvian dictionary for hunspell</be>
   <bg>Latvian dictionary for hunspell</bg>
   <bn>Latvian dictionary for hunspell</bn>
   <ca>Diccionari Letó per hunspell</ca>
   <cs>Latvian dictionary for hunspell</cs>
   <da>Latvian dictionary for hunspell</da>
   <de>Lettisches Wörterbuch für “Hunspell”</de>
   <el>Λετονικό λεξικό για hunspell</el>
   <en>Latvian dictionary for hunspell</en>
   <es_ES>Diccionario Leton para hunspell</es_ES>
   <es>Diccionario Letón para hunspell</es>
   <et>Latvian dictionary for hunspell</et>
   <eu>Latvian dictionary for hunspell</eu>
   <fa>Latvian dictionary for hunspell</fa>
   <fil_PH>Latvian dictionary for hunspell</fil_PH>
   <fi>Latvian dictionary for hunspell</fi>
   <fr_BE>Letton dictionnaire pour hunspell</fr_BE>
   <fr>Letton dictionnaire pour hunspell</fr>
   <gl_ES>Latvian dictionary for hunspell</gl_ES>
   <gu>Latvian dictionary for hunspell</gu>
   <he_IL>Latvian dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु लात्वियाई शब्दकोष</hi>
   <hr>Latvian dictionary for hunspell</hr>
   <hu>Latvian dictionary for hunspell</hu>
   <id>Latvian dictionary for hunspell</id>
   <is>Latvian dictionary for hunspell</is>
   <it>Dizionario lettone per hunspell</it>
   <ja>Hunspell 用ラトビア語辞書</ja>
   <kk>Latvian dictionary for hunspell</kk>
   <ko>Latvian dictionary for hunspell</ko>
   <ku>Latvian dictionary for hunspell</ku>
   <lt>Latvian dictionary for hunspell</lt>
   <mk>Latvian dictionary for hunspell</mk>
   <mr>Latvian dictionary for hunspell</mr>
   <nb_NO>Latvian dictionary for hunspell</nb_NO>
   <nb>Latvisk ordliste for hunspell</nb>
   <nl_BE>Latvian dictionary for hunspell</nl_BE>
   <nl>Latvian dictionary for hunspell</nl>
   <or>Latvian dictionary for hunspell</or>
   <pl>Latvian dictionary for hunspell</pl>
   <pt_BR>Dicionário Letão para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Letão para hunspell</pt>
   <ro>Latvian dictionary for hunspell</ro>
   <ru>Latvian dictionary for hunspell</ru>
   <sk>Latvian dictionary for hunspell</sk>
   <sl>Latvijski slovar za hunspell</sl>
   <so>Latvian dictionary for hunspell</so>
   <sq>Fjalor letonisht për hunspell</sq>
   <sr>Latvian dictionary for hunspell</sr>
   <sv>Lettisk ordbok för hunspell</sv>
   <th>Latvian dictionary for hunspell</th>
   <tr>Hunspell için Letonca sözlük</tr>
   <uk>Latvian dictionary for hunspell</uk>
   <vi>Latvian dictionary for hunspell</vi>
   <zh_CN>Latvian dictionary for hunspell</zh_CN>
   <zh_HK>Latvian dictionary for hunspell</zh_HK>
   <zh_TW>Latvian dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-lv
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-lv
</uninstall_package_names>

</app>
