<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Graphics
</category>

<name>
Inkscape
</name>

<description>
   <am>a vector-based drawing program</am>
   <ar>a vector-based drawing program</ar>
   <be>a vector-based drawing program</be>
   <bg>a vector-based drawing program</bg>
   <bn>a vector-based drawing program</bn>
   <ca>Programa de dibuix basat en vectors</ca>
   <cs>a vector-based drawing program</cs>
   <da>et vektorbaseret tegneprogram</da>
   <de>Ein vektorbasiertes Zeichenprogramm</de>
   <el>Πρόγραμμα σχεδίασης vector-based</el>
   <en>a vector-based drawing program</en>
   <es_ES>programa de dibujo vectorial</es_ES>
   <es>programa de dibujo vectorial</es>
   <et>a vector-based drawing program</et>
   <eu>a vector-based drawing program</eu>
   <fa>a vector-based drawing program</fa>
   <fil_PH>a vector-based drawing program</fil_PH>
   <fi>Vektoripohjainen piirto-ohjelma</fi>
   <fr_BE>Programme de dessin vectoriel</fr_BE>
   <fr>Programme de dessin vectoriel</fr>
   <gl_ES>Programa do deseño de base vectorial</gl_ES>
   <gu>a vector-based drawing program</gu>
   <he_IL>a vector-based drawing program</he_IL>
   <hi>वेक्टर-आधारित चित्रकला प्रोग्राम</hi>
   <hr>a vector-based drawing program</hr>
   <hu>a vector-based drawing program</hu>
   <id>a vector-based drawing program</id>
   <is>a vector-based drawing program</is>
   <it>programma di disegno vettoriale</it>
   <ja>ベクトル描画プログラム</ja>
   <kk>a vector-based drawing program</kk>
   <ko>a vector-based drawing program</ko>
   <ku>a vector-based drawing program</ku>
   <lt>a vector-based drawing program</lt>
   <mk>a vector-based drawing program</mk>
   <mr>a vector-based drawing program</mr>
   <nb_NO>a vector-based drawing program</nb_NO>
   <nb>vektorbasert tegneprogram</nb>
   <nl_BE>a vector-based drawing program</nl_BE>
   <nl>een vector-gebaseerd tekenprogramma</nl>
   <or>a vector-based drawing program</or>
   <pl>program do tworzenia grafiki wektorowej</pl>
   <pt_BR>Programa de desenho de base vetorial</pt_BR>
   <pt>Programa de desenho de base vectorial</pt>
   <ro>a vector-based drawing program</ro>
   <ru>Векторный графический редактор с богатым функционалом</ru>
   <sk>a vector-based drawing program</sk>
   <sl>Program za vektorsko risanje</sl>
   <so>a vector-based drawing program</so>
   <sq>Një program për vizatime me bazë vektoriale</sq>
   <sr>a vector-based drawing program</sr>
   <sv>ett vektorbaserat ritprogram</sv>
   <th>a vector-based drawing program</th>
   <tr>bir vektörel çizim programı</tr>
   <uk>програма для роботи з векторною графікою</uk>
   <vi>a vector-based drawing program</vi>
   <zh_CN>a vector-based drawing program</zh_CN>
   <zh_HK>a vector-based drawing program</zh_HK>
   <zh_TW>a vector-based drawing program</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
inkscape
aspell
imagemagick
libwmf-bin
perlmagick
pstoedit
transfig
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
inkscape
</uninstall_package_names>
</app>
