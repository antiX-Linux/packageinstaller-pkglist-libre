<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Hindi_Default_Firefox_esr
</name>

<description>
   <am>Hindi (India) localisation of Firefox ESR</am>
   <ar>Hindi (India) localisation of Firefox ESR</ar>
   <be>Hindi (India) localisation of Firefox ESR</be>
   <bg>Hindi (India) localisation of Firefox ESR</bg>
   <bn>Hindi (India) localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Hindi (Índia)</ca>
   <cs>Hindi (India) localisation of Firefox ESR</cs>
   <da>Hindi (India) localisation of Firefox ESR</da>
   <de>Indische Lokalisation für die Sprache “Hindi” von “Firefox ESR”</de>
   <el>Χίντι (Ινδία) για τον Firefox ESR</el>
   <en>Hindi (India) localisation of Firefox ESR</en>
   <es_ES>Localización Hindi (India) de Firefox ESR</es_ES>
   <es>Localización Hindi (India) de Firefox ESR</es>
   <et>Hindi (India) localisation of Firefox ESR</et>
   <eu>Hindi (India) localisation of Firefox ESR</eu>
   <fa>Hindi (India) localisation of Firefox ESR</fa>
   <fil_PH>Hindi (India) localisation of Firefox ESR</fil_PH>
   <fi>Hindi (India) localisation of Firefox ESR</fi>
   <fr_BE>Localisation en hindi (Inde) pour Firefox ESR</fr_BE>
   <fr>Localisation en hindi (Inde) pour Firefox ESR</fr>
   <gl_ES>Hindi (India) localisation of Firefox ESR</gl_ES>
   <gu>Hindi (India) localisation of Firefox ESR</gu>
   <he_IL>Hindi (India) localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का हिंदी (भारत) संस्करण</hi>
   <hr>Hindi (India) localisation of Firefox ESR</hr>
   <hu>Hindi (India) localisation of Firefox ESR</hu>
   <id>Hindi (India) localisation of Firefox ESR</id>
   <is>Hindi (India) localisation of Firefox ESR</is>
   <it>Localizzazione in lingua hindi (India) di Firefox ESR</it>
   <ja>ヒンディー語（インド）版 Firefox ESR</ja>
   <kk>Hindi (India) localisation of Firefox ESR</kk>
   <ko>Hindi (India) localisation of Firefox ESR</ko>
   <ku>Hindi (India) localisation of Firefox ESR</ku>
   <lt>Hindi (India) localisation of Firefox ESR</lt>
   <mk>Hindi (India) localisation of Firefox ESR</mk>
   <mr>Hindi (India) localisation of Firefox ESR</mr>
   <nb_NO>Hindi (India) localisation of Firefox ESR</nb_NO>
   <nb>Hindi (India) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Hindi (India) localisation of Firefox ESR</nl_BE>
   <nl>Hindi (India) localisation of Firefox ESR</nl>
   <or>Hindi (India) localisation of Firefox ESR</or>
   <pl>Hindi (India) localisation of Firefox ESR</pl>
   <pt_BR>Hindi (Índia) Localização para o Firefox ESR</pt_BR>
   <pt>Hindi (Índia) Localização para Firefox ESR</pt>
   <ro>Hindi (India) localisation of Firefox ESR</ro>
   <ru>Hindi (India) localisation of Firefox ESR</ru>
   <sk>Hindi (India) localisation of Firefox ESR</sk>
   <sl>Hindujske (Indija) krajevne nastavitve za Firefox-ESR</sl>
   <so>Hindi (India) localisation of Firefox ESR</so>
   <sq>Përkthimi i Thunderbird-it në hebraisht</sq>
   <sr>Hindi (India) localisation of Firefox ESR</sr>
   <sv>Hindi (Indien) lokalisering av Firefox ESR</sv>
   <th>Hindi (India) localisation of Firefox ESR</th>
   <tr>Firefox ESR Hintçe (Hindistan) yerelleştirmesi</tr>
   <uk>Hindi (India) localisation of Firefox ESR</uk>
   <vi>Hindi (India) localisation of Firefox ESR</vi>
   <zh_CN>Hindi (India) localisation of Firefox ESR</zh_CN>
   <zh_HK>Hindi (India) localisation of Firefox ESR</zh_HK>
   <zh_TW>Hindi (India) localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-hi-in
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-hi-in
</uninstall_package_names>

</app>
