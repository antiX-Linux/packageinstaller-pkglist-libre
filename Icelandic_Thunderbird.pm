<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Icelandic_Thunderbird
</name>

<description>
   <am>Icelandic localisation of Thunderbird</am>
   <ar>Icelandic localisation of Thunderbird</ar>
   <be>Icelandic localisation of Thunderbird</be>
   <bg>Icelandic localisation of Thunderbird</bg>
   <bn>Icelandic localisation of Thunderbird</bn>
   <ca>Localització de Thunderbird en Islandès</ca>
   <cs>Icelandic localisation of Thunderbird</cs>
   <da>Islandsk oversættelse af Thunderbird</da>
   <de>Isländische Lokalisierung von Thunderbird</de>
   <el>Ισλανδικά για το Thunderbird</el>
   <en>Icelandic localisation of Thunderbird</en>
   <es_ES>Localización Islandesa de Thunderbird</es_ES>
   <es>Localización Islandés de Thunderbird</es>
   <et>Icelandic localisation of Thunderbird</et>
   <eu>Icelandic localisation of Thunderbird</eu>
   <fa>Icelandic localisation of Thunderbird</fa>
   <fil_PH>Icelandic localisation of Thunderbird</fil_PH>
   <fi>Islantilainen Thunderbird-kotoistus</fi>
   <fr_BE>Localisation islandaise pour Thunderbird</fr_BE>
   <fr>Localisation islandaise pour Thunderbird</fr>
   <gl_ES>Localización de Thunderbird ao islandés</gl_ES>
   <gu>Icelandic localisation of Thunderbird</gu>
   <he_IL>תרגומי Thunderbird לאיסלנדית</he_IL>
   <hi>थंडरबर्ड का आइसलैंडिक संस्करण</hi>
   <hr>Icelandic localisation of Thunderbird</hr>
   <hu>Icelandic localisation of Thunderbird</hu>
   <id>Icelandic localisation of Thunderbird</id>
   <is>Icelandic localisation of Thunderbird</is>
   <it>Localizzazione islandese di Thunderbird</it>
   <ja>Thunderbird のアイスランド語版</ja>
   <kk>Icelandic localisation of Thunderbird</kk>
   <ko>Icelandic localisation of Thunderbird</ko>
   <ku>Icelandic localisation of Thunderbird</ku>
   <lt>Icelandic localisation of Thunderbird</lt>
   <mk>Icelandic localisation of Thunderbird</mk>
   <mr>Icelandic localisation of Thunderbird</mr>
   <nb_NO>Icelandic localisation of Thunderbird</nb_NO>
   <nb>Islandsk lokaltilpassing av Thunderbird</nb>
   <nl_BE>Icelandic localisation of Thunderbird</nl_BE>
   <nl>IJslandse lokalisatie van Thunderbird</nl>
   <or>Icelandic localisation of Thunderbird</or>
   <pl>Islandzka lokalizacja Thunderbirda</pl>
   <pt_BR>Islandês Localização para o Thunderbird</pt_BR>
   <pt>Islandês Localização para Thunderbird</pt>
   <ro>Icelandic localisation of Thunderbird</ro>
   <ru>Icelandic localisation of Thunderbird</ru>
   <sk>Icelandic localisation of Thunderbird</sk>
   <sl>Icelandic localisation of Thunderbird</sl>
   <so>Icelandic localisation of Thunderbird</so>
   <sq>Përkthimi i Thunderbird-it në islandisht</sq>
   <sr>Icelandic localisation of Thunderbird</sr>
   <sv>Isländsk  lokalisering av Thunderbird </sv>
   <th>Icelandic localisation ของ Thunderbird</th>
   <tr>Thunderbird'ün İzlandaca yerelleştirmesi</tr>
   <uk>Icelandic локалізація Thunderbird</uk>
   <vi>Icelandic localisation of Thunderbird</vi>
   <zh_CN>Icelandic localisation of Thunderbird</zh_CN>
   <zh_HK>Icelandic localisation of Thunderbird</zh_HK>
   <zh_TW>Icelandic localisation of Thunderbird</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
thunderbird-l10n-is
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
thunderbird-l10n-is
</uninstall_package_names>

</app>
