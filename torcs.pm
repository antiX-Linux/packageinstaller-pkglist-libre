<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
Torcs
</name>

<description>
   <en>3D racing cars simulator game</en>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
torcs
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
torcs
</uninstall_package_names>
</app>
