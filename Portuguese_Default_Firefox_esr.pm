<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Portugese_Default_Firefox_esr
</name>

<description>
   <am>Portuguese(PT) localisation of Firefox ESR</am>
   <ar>Portuguese(PT) localisation of Firefox ESR</ar>
   <be>Portuguese(PT) localisation of Firefox ESR</be>
   <bg>Portuguese(PT) localisation of Firefox ESR</bg>
   <bn>Portuguese(PT) localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Portuguès (Portugal)</ca>
   <cs>Portuguese(PT) localisation of Firefox ESR</cs>
   <da>Portuguese(PT) localisation of Firefox ESR</da>
   <de>Portugiesische (PT) Lokalisation von “Firefox ESR”</de>
   <el>Πορτογαλικά (PT) εντοπισμός του Firefox ESR</el>
   <en>Portuguese(PT) localisation of Firefox ESR</en>
   <es_ES>Localización Portugués (PT) de Firefox ESR</es_ES>
   <es>Localización Portugués (PT) de Firefox ESR</es>
   <et>Portuguese(PT) localisation of Firefox ESR</et>
   <eu>Portuguese(PT) localisation of Firefox ESR</eu>
   <fa>Portuguese(PT) localisation of Firefox ESR</fa>
   <fil_PH>Portuguese(PT) localisation of Firefox ESR</fil_PH>
   <fi>Portuguese(PT) localisation of Firefox ESR</fi>
   <fr_BE>Localisation en portugais pour Firefox ESR</fr_BE>
   <fr>Localisation en portugais pour Firefox ESR</fr>
   <gl_ES>Portuguese(PT) localisation of Firefox ESR</gl_ES>
   <gu>Portuguese(PT) localisation of Firefox ESR</gu>
   <he_IL>Portuguese(PT) localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का पुर्तगाली (पुर्तगाल) संस्करण</hi>
   <hr>Portuguese(PT) localisation of Firefox ESR</hr>
   <hu>Portuguese(PT) localisation of Firefox ESR</hu>
   <id>Portuguese(PT) localisation of Firefox ESR</id>
   <is>Portuguese(PT) localisation of Firefox ESR</is>
   <it>Localizzazione portoghese(PT) di Firefox ESR</it>
   <ja>ポルトガル語（ポルトガル）版 Firefox ESR</ja>
   <kk>Portuguese(PT) localisation of Firefox ESR</kk>
   <ko>Portuguese(PT) localisation of Firefox ESR</ko>
   <ku>Portuguese(PT) localisation of Firefox ESR</ku>
   <lt>Portuguese(PT) localisation of Firefox ESR</lt>
   <mk>Portuguese(PT) localisation of Firefox ESR</mk>
   <mr>Portuguese(PT) localisation of Firefox ESR</mr>
   <nb_NO>Portuguese(PT) localisation of Firefox ESR</nb_NO>
   <nb>Portugisisk (PT) lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Portuguese(PT) localisation of Firefox ESR</nl_BE>
   <nl>Portuguese(PT) localisation of Firefox ESR</nl>
   <or>Portuguese(PT) localisation of Firefox ESR</or>
   <pl>Portuguese(PT) localisation of Firefox ESR</pl>
   <pt_BR>Português (PT) Localização para o Firefox ESR</pt_BR>
   <pt>Português (PT) Localização para Firefox ESR</pt>
   <ro>Portuguese(PT) localisation of Firefox ESR</ro>
   <ru>Portuguese(PT) localisation of Firefox ESR</ru>
   <sk>Portuguese(PT) localisation of Firefox ESR</sk>
   <sl>Portugalske (PT) krajevne nastavitve za Firefox-ESR</sl>
   <so>Portuguese(PT) localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në portugalisht (PT)</sq>
   <sr>Portuguese(PT) localisation of Firefox ESR</sr>
   <sv>Portugisisk(PT) lokalisering för Firefox ESR</sv>
   <th>Portuguese(PT) localisation of Firefox ESR</th>
   <tr>Firefox ESR Portekizce(PT) yerelleştirmesi</tr>
   <uk>Portuguese(PT) localisation of Firefox ESR</uk>
   <vi>Portuguese(PT) localisation of Firefox ESR</vi>
   <zh_CN>Portuguese(PT) localisation of Firefox ESR</zh_CN>
   <zh_HK>Portuguese(PT) localisation of Firefox ESR</zh_HK>
   <zh_TW>Portuguese(PT) localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-pt-pt
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-pt-pt
</uninstall_package_names>

</app>
