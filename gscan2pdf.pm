<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Graphics
</category>

<name>
gscan2pdf
</name>

<description>
   <am>A GUI to produce PDFs or DjVus from scanned documents</am>
   <ar>A GUI to produce PDFs or DjVus from scanned documents</ar>
   <be>A GUI to produce PDFs or DjVus from scanned documents</be>
   <bg>A GUI to produce PDFs or DjVus from scanned documents</bg>
   <bn>A GUI to produce PDFs or DjVus from scanned documents</bn>
   <ca>Una IGU per crear PDFs o DJVus des de documents escanejats</ca>
   <cs>A GUI to produce PDFs or DjVus from scanned documents</cs>
   <da>En grafisk brugerflade til at skabe PDF'er eller DjVus fra skannede dokumenter</da>
   <de>Eine GUI zum Erzeugen von PDFs oder DjVus aus gescannten Dokumenten</de>
   <el>Ένα GUI για την δημιουργία αρχείων PDF ή DjVus από σαρωμένα έγγραφα</el>
   <en>A GUI to produce PDFs or DjVus from scanned documents</en>
   <es_ES>Una GUI para producir archivos PDF o DjVus a partir de documentos escaneados</es_ES>
   <es>Una GUI para producir archivos PDF o DjVus a partir de documentos escaneados</es>
   <et>A GUI to produce PDFs or DjVus from scanned documents</et>
   <eu>A GUI to produce PDFs or DjVus from scanned documents</eu>
   <fa>A GUI to produce PDFs or DjVus from scanned documents</fa>
   <fil_PH>A GUI to produce PDFs or DjVus from scanned documents</fil_PH>
   <fi>Graafinen käyttöliittymä PDF tai DjVu-tiedostojen tuottamiseen skannatuista asiakirjoista</fi>
   <fr_BE>Une interface graphique permettant de créer des PDFs et DjVus à partir de documents scannés</fr_BE>
   <fr>Une interface graphique permettant de créer des PDFs et DjVus à partir de documents scannés</fr>
   <gl_ES>Interface gráfica para producir ficheiros PDF e DjVu por dixitalización en escáner</gl_ES>
   <gu>A GUI to produce PDFs or DjVus from scanned documents</gu>
   <he_IL>A GUI to produce PDFs or DjVus from scanned documents</he_IL>
   <hi>स्कैन किए प्रलेखों से पीडीएफ या DjVu सृजन हेतु ग्राफ़िकल अंतरफलक</hi>
   <hr>A GUI to produce PDFs or DjVus from scanned documents</hr>
   <hu>A GUI to produce PDFs or DjVus from scanned documents</hu>
   <id>A GUI to produce PDFs or DjVus from scanned documents</id>
   <is>A GUI to produce PDFs or DjVus from scanned documents</is>
   <it>Un'interfaccia grafica per ottenere PDF o DjVus da documenti scansionati</it>
   <ja>スキャンした文書から PDF や DjVus を作成する GUI</ja>
   <kk>A GUI to produce PDFs or DjVus from scanned documents</kk>
   <ko>A GUI to produce PDFs or DjVus from scanned documents</ko>
   <ku>A GUI to produce PDFs or DjVus from scanned documents</ku>
   <lt>A GUI to produce PDFs or DjVus from scanned documents</lt>
   <mk>A GUI to produce PDFs or DjVus from scanned documents</mk>
   <mr>A GUI to produce PDFs or DjVus from scanned documents</mr>
   <nb_NO>A GUI to produce PDFs or DjVus from scanned documents</nb_NO>
   <nb>produser PDF eller DjVU fra skannede dokumenter (grafisk brukergrensesnitt)</nb>
   <nl_BE>A GUI to produce PDFs or DjVus from scanned documents</nl_BE>
   <nl>Een GUI voor het produceren van PDF's of DjVus uit gescande documenten</nl>
   <or>A GUI to produce PDFs or DjVus from scanned documents</or>
   <pl>narzędzie do tworzenia plików PDF lub DjVu z zeskanowanych dokumentów</pl>
   <pt_BR>Interface gráfica para produzir arquivos PDF e DjVus a partir de documentos digitalizados (scanner)</pt_BR>
   <pt>Interface gŕafica para produzir ficheiros PDF e DjVu por digitalização em scanner</pt>
   <ro>A GUI to produce PDFs or DjVus from scanned documents</ro>
   <ru>GUI для создания PDF или DjVu из сканированных документов</ru>
   <sk>A GUI to produce PDFs or DjVus from scanned documents</sk>
   <sl>Grafični vmesnik za ustvarjanje PFDjev ali Djev iz skeniranih dokumentov</sl>
   <so>A GUI to produce PDFs or DjVus from scanned documents</so>
   <sq>Përkthimi i Thunderbird-it në greqisht</sq>
   <sr>A GUI to produce PDFs or DjVus from scanned documents</sr>
   <sv>Ett grafiskt gränssnitt för att  producera PDFs eller DjVus av skannade dokument</sv>
   <th>GUI สำหรับสร้าง PDF หรือ DjVus จากการสแกนเอกสาร</th>
   <tr>Taranan belgelerden PDF veya DjVus üretmek için bir grafiksel arayüz</tr>
   <uk>Графічний інтерфейс для створення PDF та DjVU файлів з відсканованих документів</uk>
   <vi>A GUI to produce PDFs or DjVus from scanned documents</vi>
   <zh_CN>通过扫描创建 PDF 或 DjVu 文件的 GUI 程序</zh_CN>
   <zh_HK>A GUI to produce PDFs or DjVus from scanned documents</zh_HK>
   <zh_TW>A GUI to produce PDFs or DjVus from scanned documents</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gscan2pdf
djvulibre-bin
gocr
unpaper
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gscan2pdf
djvulibre-bin
gocr
unpaper
</uninstall_package_names>
</app>
