<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Malay_Default_Firefox_esr
</name>

<description>
   <am>Malay localisation of Firefox ESR</am>
   <ar>Malay localisation of Firefox ESR</ar>
   <be>Malay localisation of Firefox ESR</be>
   <bg>Malay localisation of Firefox ESR</bg>
   <bn>Malay localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Malai</ca>
   <cs>Malay localisation of Firefox ESR</cs>
   <da>Malay localisation of Firefox ESR</da>
   <de>Malaiische Lokalisation von “Firefox ESR”</de>
   <el>Malay για Firefox ESR</el>
   <en>Malay localisation of Firefox ESR</en>
   <es_ES>Localización Malayo de Firefox ESR</es_ES>
   <es>Localización Malayo de Firefox ESR</es>
   <et>Malay localisation of Firefox ESR</et>
   <eu>Malay localisation of Firefox ESR</eu>
   <fa>Malay localisation of Firefox ESR</fa>
   <fil_PH>Malay localisation of Firefox ESR</fil_PH>
   <fi>Malay localisation of Firefox ESR</fi>
   <fr_BE>Localisation en malais pour Firefox ESR</fr_BE>
   <fr>Localisation en malais pour Firefox ESR</fr>
   <gl_ES>Malay localisation of Firefox ESR</gl_ES>
   <gu>Malay localisation of Firefox ESR</gu>
   <he_IL>Malay localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का मलय संस्करण</hi>
   <hr>Malay localisation of Firefox ESR</hr>
   <hu>Malay localisation of Firefox ESR</hu>
   <id>Malay localisation of Firefox ESR</id>
   <is>Malay localisation of Firefox ESR</is>
   <it>Localizzazione malay di Firefox ESR</it>
   <ja>マレー語版 Firefox ESR</ja>
   <kk>Malay localisation of Firefox ESR</kk>
   <ko>Malay localisation of Firefox ESR</ko>
   <ku>Malay localisation of Firefox ESR</ku>
   <lt>Malay localisation of Firefox ESR</lt>
   <mk>Malay localisation of Firefox ESR</mk>
   <mr>Malay localisation of Firefox ESR</mr>
   <nb_NO>Malay localisation of Firefox ESR</nb_NO>
   <nb>Malaysisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Malay localisation of Firefox ESR</nl_BE>
   <nl>Malay localisation of Firefox ESR</nl>
   <or>Malay localisation of Firefox ESR</or>
   <pl>Malay localisation of Firefox ESR</pl>
   <pt_BR>Malaio Localização para o Firefox ESR</pt_BR>
   <pt>Malaio Localização para Firefox ESR</pt>
   <ro>Malay localisation of Firefox ESR</ro>
   <ru>Malay localisation of Firefox ESR</ru>
   <sk>Malay localisation of Firefox ESR</sk>
   <sl>Malajske krajevne nastavitve za Firefox ESR</sl>
   <so>Malay localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në malagashe</sq>
   <sr>Malay localisation of Firefox ESR</sr>
   <sv>Malay lokalisering av Firefox ESR</sv>
   <th>Malay localisation of Firefox ESR</th>
   <tr>Firefox ESR Malayca yerelleştirmesi</tr>
   <uk>Malay localisation of Firefox ESR</uk>
   <vi>Malay localisation of Firefox ESR</vi>
   <zh_CN>Malay localisation of Firefox ESR</zh_CN>
   <zh_HK>Malay localisation of Firefox ESR</zh_HK>
   <zh_TW>Malay localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-ms
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-ms
</uninstall_package_names>

</app>
