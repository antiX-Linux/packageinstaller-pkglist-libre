<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Croatian_Default_Firefox_esr
</name>

<description>
   <am>Croatian localisation of Firefox ESR</am>
   <ar>Croatian localisation of Firefox ESR</ar>
   <be>Croatian localisation of Firefox ESR</be>
   <bg>Croatian localisation of Firefox ESR</bg>
   <bn>Croatian localisation of Firefox ESR</bn>
   <ca>Localització de Firefox-ESR en Croata</ca>
   <cs>Croatian localisation of Firefox ESR</cs>
   <da>Croatian localisation of Firefox ESR</da>
   <de>Kroatische Lokalisation von “Firefox ESR”</de>
   <el>Κροατικά για Firefox ESR</el>
   <en>Croatian localisation of Firefox ESR</en>
   <es_ES>Localización Croata de Firefox ESR</es_ES>
   <es>Localización Croata de Firefox ESR</es>
   <et>Croatian localisation of Firefox ESR</et>
   <eu>Croatian localisation of Firefox ESR</eu>
   <fa>Croatian localisation of Firefox ESR</fa>
   <fil_PH>Croatian localisation of Firefox ESR</fil_PH>
   <fi>Croatian localisation of Firefox ESR</fi>
   <fr_BE>Localisation en croate pour Firefox ESR</fr_BE>
   <fr>Localisation en croate pour Firefox ESR</fr>
   <gl_ES>Croatian localisation of Firefox ESR</gl_ES>
   <gu>Croatian localisation of Firefox ESR</gu>
   <he_IL>Croatian localisation of Firefox ESR</he_IL>
   <hi>फायरफॉक्स ईएसआर का क्रोएशियाई संस्करण</hi>
   <hr>Croatian localisation of Firefox ESR</hr>
   <hu>Croatian localisation of Firefox ESR</hu>
   <id>Croatian localisation of Firefox ESR</id>
   <is>Croatian localisation of Firefox ESR</is>
   <it>Localizzazione croata di Firefox ESR</it>
   <ja>クロアチア語版 Firefox ESR</ja>
   <kk>Croatian localisation of Firefox ESR</kk>
   <ko>Croatian localisation of Firefox ESR</ko>
   <ku>Croatian localisation of Firefox ESR</ku>
   <lt>Croatian localisation of Firefox ESR</lt>
   <mk>Croatian localisation of Firefox ESR</mk>
   <mr>Croatian localisation of Firefox ESR</mr>
   <nb_NO>Croatian localisation of Firefox ESR</nb_NO>
   <nb>Kroatisk lokaltilpassing av Firefox ESR</nb>
   <nl_BE>Croatian localisation of Firefox ESR</nl_BE>
   <nl>Kroatische lokalisatie van Firefox ESR</nl>
   <or>Croatian localisation of Firefox ESR</or>
   <pl>Croatian localisation of Firefox ESR</pl>
   <pt_BR>Croata Localização para o Firefox ESR</pt_BR>
   <pt>Croata Localização para Firefox ESR</pt>
   <ro>Croatian localisation of Firefox ESR</ro>
   <ru>Croatian localisation of Firefox ESR</ru>
   <sk>Croatian localisation of Firefox ESR</sk>
   <sl>Hrvaške krajevne nastavitve za Firefox ESR</sl>
   <so>Croatian localisation of Firefox ESR</so>
   <sq>Përkthimi i Firefox-it ESR në kroatisht</sq>
   <sr>Croatian localisation of Firefox ESR</sr>
   <sv>Kroatisk lokalisering av Firefox ESR</sv>
   <th>Croatian localisation of Firefox ESR</th>
   <tr>Firefox ESR Hırvatça yerelleştirmesi</tr>
   <uk>Croatian localisation of Firefox ESR</uk>
   <vi>Croatian localisation of Firefox ESR</vi>
   <zh_CN>Croatian localisation of Firefox ESR</zh_CN>
   <zh_HK>Croatian localisation of Firefox ESR</zh_HK>
   <zh_TW>Croatian localisation of Firefox ESR</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-hr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-hr
</uninstall_package_names>

</app>
