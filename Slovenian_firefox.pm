<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Slovenian_Firefox
</name>

<description>
   <am>Slovenian localisation of Firefox</am>
   <ar>Slovenian localisation of Firefox</ar>
   <be>Slovenian localisation of Firefox</be>
   <bg>Slovenian localisation of Firefox</bg>
   <bn>Slovenian localisation of Firefox</bn>
   <ca>Localització de Firefox en Eslovè</ca>
   <cs>Slovenian localisation of Firefox</cs>
   <da>Slovensk oversættelse af Firefox</da>
   <de>Slowenische Lokalisierung von Firefox</de>
   <el>Σλοβενικά για το  Firefox</el>
   <en>Slovenian localisation of Firefox</en>
   <es_ES>Localización Eslovena de Firefox</es_ES>
   <es>Localización Esloveno de Firefox</es>
   <et>Slovenian localisation of Firefox</et>
   <eu>Slovenian localisation of Firefox</eu>
   <fa>Slovenian localisation of Firefox</fa>
   <fil_PH>Slovenian localisation of Firefox</fil_PH>
   <fi>Slovenialainen Firefox-kotoistus</fi>
   <fr_BE>Localisation en slovène pour Firefox</fr_BE>
   <fr>Localisation en slovène pour Firefox</fr>
   <gl_ES>Localización de Firefox ao esloveno</gl_ES>
   <gu>Slovenian localisation of Firefox</gu>
   <he_IL>Slovenian localisation of Firefox</he_IL>
   <hi>फायरफॉक्स का स्लोवेनियाई संस्करण</hi>
   <hr>Slovenian localisation of Firefox</hr>
   <hu>Slovenian localisation of Firefox</hu>
   <id>Slovenian localisation of Firefox</id>
   <is>Slovenian localisation of Firefox</is>
   <it>Localizzazione slovena di Firefox</it>
   <ja>Firefox のスロベニア語版</ja>
   <kk>Slovenian localisation of Firefox</kk>
   <ko>Slovenian localisation of Firefox</ko>
   <ku>Slovenian localisation of Firefox</ku>
   <lt>Slovenian localisation of Firefox</lt>
   <mk>Slovenian localisation of Firefox</mk>
   <mr>Slovenian localisation of Firefox</mr>
   <nb_NO>Slovenian localisation of Firefox</nb_NO>
   <nb>Slovensk lokaltilpassing av Firefox</nb>
   <nl_BE>Slovenian localisation of Firefox</nl_BE>
   <nl>Sloveense lokalisatie van Firefox</nl>
   <or>Slovenian localisation of Firefox</or>
   <pl>Słoweńska lokalizacja przeglądarki Firefox</pl>
   <pt_BR>Esloveno Localização para o Firefox</pt_BR>
   <pt>Esloveno Localização para Firefox</pt>
   <ro>Slovenian localisation of Firefox</ro>
   <ru>Словенская локализация Firefox</ru>
   <sk>Slovenian localisation of Firefox</sk>
   <sl>Slovenske krajevne nastavitve za Firefox</sl>
   <so>Slovenian localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në sllovenisht</sq>
   <sr>Slovenian localisation of Firefox</sr>
   <sv>Slovensk lokalisering av Firefox</sv>
   <th>Slovenian localisation ของ Firefox</th>
   <tr>Firefox'un Slovence yerelleştirmesi</tr>
   <uk>Slovenian localisation of Firefox</uk>
   <vi>Slovenian localisation of Firefox</vi>
   <zh_CN>Slovenian localisation of Firefox</zh_CN>
   <zh_HK>Slovenian localisation of Firefox</zh_HK>
   <zh_TW>Slovenian localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-l10n-xpi-sl
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-l10n-xpi-sl
</uninstall_package_names>
</app>
