<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Themes
</category>

<name>
Numix Gtk Theme
</name>

<description>
   <am>A modern flat theme with a combination of light and dark elements</am>
   <ar>A modern flat theme with a combination of light and dark elements</ar>
   <be>A modern flat theme with a combination of light and dark elements</be>
   <bg>A modern flat theme with a combination of light and dark elements</bg>
   <bn>A modern flat theme with a combination of light and dark elements</bn>
   <ca>Un tema pla modern que combina elements clars i foscos</ca>
   <cs>A modern flat theme with a combination of light and dark elements</cs>
   <da>A modern flat theme with a combination of light and dark elements</da>
   <de>Ein moderner flacher Stil, mit einer Kombination aus hellen und dunklen Elementen.</de>
   <el>Ένα μοντέρνο επίπεδο θέμα με συνδυασμό φωτεινών και σκοτεινών στοιχείων</el>
   <en>A modern flat theme with a combination of light and dark elements</en>
   <es_ES>Un tema plano moderno con una combinación de elementos claros y oscuros</es_ES>
   <es>Un tema plano moderno con una combinación de elementos claros y oscuros</es>
   <et>A modern flat theme with a combination of light and dark elements</et>
   <eu>A modern flat theme with a combination of light and dark elements</eu>
   <fa>A modern flat theme with a combination of light and dark elements</fa>
   <fil_PH>A modern flat theme with a combination of light and dark elements</fil_PH>
   <fi>Nykyaikainen litteä teema jossa on yhdistelty keveitä ja tummia elementtejä</fi>
   <fr_BE>Un thème plat et moderne combinant des éléments clairs et foncés.</fr_BE>
   <fr>Un thème plat et moderne combinant des éléments clairs et foncés.</fr>
   <gl_ES>A modern flat theme with a combination of light and dark elements</gl_ES>
   <gu>A modern flat theme with a combination of light and dark elements</gu>
   <he_IL>A modern flat theme with a combination of light and dark elements</he_IL>
   <hi>हल्के व गहरे रंग के विलय से युक्त आधुनिक सपाट थीम</hi>
   <hr>A modern flat theme with a combination of light and dark elements</hr>
   <hu>A modern flat theme with a combination of light and dark elements</hu>
   <id>A modern flat theme with a combination of light and dark elements</id>
   <is>A modern flat theme with a combination of light and dark elements</is>
   <it>Un tema flat moderno con una combinazione di elementi chiari e scuri</it>
   <ja>ライトとダークの要素を組み合わせたモダンでフラットテーマ</ja>
   <kk>A modern flat theme with a combination of light and dark elements</kk>
   <ko>A modern flat theme with a combination of light and dark elements</ko>
   <ku>A modern flat theme with a combination of light and dark elements</ku>
   <lt>A modern flat theme with a combination of light and dark elements</lt>
   <mk>A modern flat theme with a combination of light and dark elements</mk>
   <mr>A modern flat theme with a combination of light and dark elements</mr>
   <nb_NO>A modern flat theme with a combination of light and dark elements</nb_NO>
   <nb>Et moderne, flatt tema som kombinerer lyse og mørke elementer</nb>
   <nl_BE>A modern flat theme with a combination of light and dark elements</nl_BE>
   <nl>A modern flat theme with a combination of light and dark elements</nl>
   <or>A modern flat theme with a combination of light and dark elements</or>
   <pl>A modern flat theme with a combination of light and dark elements</pl>
   <pt_BR>Tema plano moderno com uma combinação de elementos claros e escuros</pt_BR>
   <pt>Tema moderno "plano" que combina elementos claros e escuros</pt>
   <ro>A modern flat theme with a combination of light and dark elements</ro>
   <ru>A modern flat theme with a combination of light and dark elements</ru>
   <sk>A modern flat theme with a combination of light and dark elements</sk>
   <sl>Moderna ploskva tema v kombinaciji temnih in svetlih elementov</sl>
   <so>A modern flat theme with a combination of light and dark elements</so>
   <sq>Një temë e sheshtë moderne, me një ndërthurje elementësh të çelët dhe të errët</sq>
   <sr>A modern flat theme with a combination of light and dark elements</sr>
   <sv>Ett modernt platt tema med en kombination av ljusa och mörka element</sv>
   <th>A modern flat theme with a combination of light and dark elements</th>
   <tr>Açık ve koyu unsurların birleşiminden oluşan çağdaş, düz tema</tr>
   <uk>A modern flat theme with a combination of light and dark elements</uk>
   <vi>A modern flat theme with a combination of light and dark elements</vi>
   <zh_CN>A modern flat theme with a combination of light and dark elements</zh_CN>
   <zh_HK>A modern flat theme with a combination of light and dark elements</zh_HK>
   <zh_TW>A modern flat theme with a combination of light and dark elements</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
numix-gtk-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
numix-gtk-theme
</uninstall_package_names>
</app>
