<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Swahili
</name>

<description>
   <am>Swahili dictionary for hunspell</am>
   <ar>Swahili dictionary for hunspell</ar>
   <be>Swahili dictionary for hunspell</be>
   <bg>Swahili dictionary for hunspell</bg>
   <bn>Swahili dictionary for hunspell</bn>
   <ca>Diccionari Swahili per hunspell</ca>
   <cs>Swahili dictionary for hunspell</cs>
   <da>Swahili dictionary for hunspell</da>
   <de>Ostafrikanisches (Kisuaheli) Wörterbuch für “Hunspell”</de>
   <el>Λεξικό Σουαχίλι για hunspell</el>
   <en>Swahili dictionary for hunspell</en>
   <es_ES>Diccionario Suajili para hunspell</es_ES>
   <es>Diccionario Suajili para hunspell</es>
   <et>Swahili dictionary for hunspell</et>
   <eu>Swahili dictionary for hunspell</eu>
   <fa>Swahili dictionary for hunspell</fa>
   <fil_PH>Swahili dictionary for hunspell</fil_PH>
   <fi>Swahili dictionary for hunspell</fi>
   <fr_BE>Swahili dictionnaire pour hunspell</fr_BE>
   <fr>Swahili dictionnaire pour hunspell</fr>
   <gl_ES>Swahili dictionary for hunspell</gl_ES>
   <gu>Swahili dictionary for hunspell</gu>
   <he_IL>Swahili dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु स्वाहिली शब्दकोष</hi>
   <hr>Swahili dictionary for hunspell</hr>
   <hu>Swahili dictionary for hunspell</hu>
   <id>Swahili dictionary for hunspell</id>
   <is>Swahili dictionary for hunspell</is>
   <it>Dizionario swahili per hunspell</it>
   <ja>Hunspell 用スワヒリ語辞書</ja>
   <kk>Swahili dictionary for hunspell</kk>
   <ko>Swahili dictionary for hunspell</ko>
   <ku>Swahili dictionary for hunspell</ku>
   <lt>Swahili dictionary for hunspell</lt>
   <mk>Swahili dictionary for hunspell</mk>
   <mr>Swahili dictionary for hunspell</mr>
   <nb_NO>Swahili dictionary for hunspell</nb_NO>
   <nb>Swahili ordliste for hunspell</nb>
   <nl_BE>Swahili dictionary for hunspell</nl_BE>
   <nl>Swahili dictionary for hunspell</nl>
   <or>Swahili dictionary for hunspell</or>
   <pl>Swahili dictionary for hunspell</pl>
   <pt_BR>Dicionário Suaíli para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Swahili para hunspell</pt>
   <ro>Swahili dictionary for hunspell</ro>
   <ru>Swahili dictionary for hunspell</ru>
   <sk>Swahili dictionary for hunspell</sk>
   <sl>Svahili slovar za hunspell</sl>
   <so>Swahili dictionary for hunspell</so>
   <sq>Fjalor në suahili për hunspell</sq>
   <sr>Swahili dictionary for hunspell</sr>
   <sv>Swahili ordbok för hunspell</sv>
   <th>Swahili dictionary for hunspell</th>
   <tr>Hunspell için Sivahili Dilinde sözlük</tr>
   <uk>Swahili dictionary for hunspell</uk>
   <vi>Swahili dictionary for hunspell</vi>
   <zh_CN>Swahili dictionary for hunspell</zh_CN>
   <zh_HK>Swahili dictionary for hunspell</zh_HK>
   <zh_TW>Swahili dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-sw
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-sw
</uninstall_package_names>

</app>
