<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Indic_LO_Latest_main
</name>

<description>
   <am>Indic Language Meta-Package for LibreOffice</am>
   <ar>Indic Language Meta-Package for LibreOffice</ar>
   <be>Indic Language Meta-Package for LibreOffice</be>
   <bg>Indic Language Meta-Package for LibreOffice</bg>
   <bn>Indic Language Meta-Package for LibreOffice</bn>
   <ca>Metapaquet d'idioma Índic per LibreOffice</ca>
   <cs>Indic Language Meta-Package for LibreOffice</cs>
   <da>Indic Language Meta-Package for LibreOffice</da>
   <de>Indisches Meta-Paket für LibreOffice</de>
   <el>LibreOffice στα Indic</el>
   <en>Indic Language Meta-Package for LibreOffice</en>
   <es_ES>Meta-Paquete de idiomas Indicos para LibreOffice</es_ES>
   <es>Metapaquete de Idioma Índico para LibreOffice</es>
   <et>Indic Language Meta-Package for LibreOffice</et>
   <eu>Indic Language Meta-Package for LibreOffice</eu>
   <fa>Indic Language Meta-Package for LibreOffice</fa>
   <fil_PH>Indic Language Meta-Package for LibreOffice</fil_PH>
   <fi>Indinkielinen kielipaketti LibreOffice:lle</fi>
   <fr_BE>Méta-Paquet linguistique Indic pour LibreOffice</fr_BE>
   <fr>Méta-Paquet linguistique Indic pour LibreOffice</fr>
   <gl_ES>Linguas índicas Meta-paquete de Idioma para LibreOffice</gl_ES>
   <gu>Indic Language Meta-Package for LibreOffice</gu>
   <he_IL>Indic Language Meta-Package for LibreOffice</he_IL>
   <hi>लिब्रे-ऑफिस हेतु इंडिक भाषा मेटा-पैकेज</hi>
   <hr>Indic Language Meta-Package for LibreOffice</hr>
   <hu>Indic Language Meta-Package for LibreOffice</hu>
   <id>Indic Language Meta-Package for LibreOffice</id>
   <is>Indic Language Meta-Package for LibreOffice</is>
   <it>Meta-pacchetto della lingua indica per LibreOffice</it>
   <ja>LibreOffice用のインド語族言語メタパッケージ</ja>
   <kk>Indic Language Meta-Package for LibreOffice</kk>
   <ko>Indic Language Meta-Package for LibreOffice</ko>
   <ku>Indic Language Meta-Package for LibreOffice</ku>
   <lt>Indic Language Meta-Package for LibreOffice</lt>
   <mk>Indic Language Meta-Package for LibreOffice</mk>
   <mr>Indic Language Meta-Package for LibreOffice</mr>
   <nb_NO>Indic Language Meta-Package for LibreOffice</nb_NO>
   <nb>Indisk språkpakke for LibreOffice</nb>
   <nl_BE>Indic Language Meta-Package for LibreOffice</nl_BE>
   <nl>Indic Taal Meta-Pakket voor LibreOffice</nl>
   <or>Indic Language Meta-Package for LibreOffice</or>
   <pl>Meta-pakiet języka Indic dla LibreOffice</pl>
   <pt_BR>Índico Pacote de Idioma para o LibreOffice</pt_BR>
   <pt>Línguas indianas Meta-Pacote de Idioma para LibreOffice</pt>
   <ro>Indic Language Meta-Package for LibreOffice</ro>
   <ru>Indic Language Meta-Package for LibreOffice</ru>
   <sk>Indic Language Meta-Package for LibreOffice</sk>
   <sl>Indikski jezikovni meta-paket za LibreOffice</sl>
   <so>Indic Language Meta-Package for LibreOffice</so>
   <sq>Meta-Paketë gjuhësore për LibreOffice-in në indisht</sq>
   <sr>Indic Language Meta-Package for LibreOffice</sr>
   <sv>Indiskt Språk Meta-Paket för LibreOffice</sv>
   <th>Meta-Package ภาษา Indic สำหรับ LibreOffice</th>
   <tr>LiibreOffice için Hint Dili  Üst-Paketi</tr>
   <uk>Indic Language Meta-Package for LibreOffice</uk>
   <vi>Indic Language Meta-Package for LibreOffice</vi>
   <zh_CN>Indic Language Meta-Package for LibreOffice</zh_CN>
   <zh_HK>Indic Language Meta-Package for LibreOffice</zh_HK>
   <zh_TW>Indic Language Meta-Package for LibreOffice</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
-t bookworm-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-in
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-in
libreoffice-gtk3
</uninstall_package_names>

</app>
