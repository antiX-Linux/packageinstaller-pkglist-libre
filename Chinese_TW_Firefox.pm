<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Chinese_TW_Firefox
</name>

<description>
   <am>Chinese_traditional localisation of Firefox</am>
   <ar>Chinese_traditional localisation of Firefox</ar>
   <be>Chinese_traditional localisation of Firefox</be>
   <bg>Chinese_traditional localisation of Firefox</bg>
   <bn>Chinese_traditional localisation of Firefox</bn>
   <ca>Localització de Firefox en Xinès tradicional</ca>
   <cs>Chinese_traditional localisation of Firefox</cs>
   <da>Chinese_traditional localisation of Firefox</da>
   <de>Chinesische Lokalisation (traditionelle Langzeichen der chinesischen Schrift) von “Firefox”</de>
   <el>Παραδοσιακά κινέζικα για το Firefox</el>
   <en>Chinese_traditional localisation of Firefox</en>
   <es_ES>Localización en Chino_tradicional de Firefox ESR</es_ES>
   <es>Localización Chino tradicional de Firefox</es>
   <et>Chinese_traditional localisation of Firefox</et>
   <eu>Chinese_traditional localisation of Firefox</eu>
   <fa>Chinese_traditional localisation of Firefox</fa>
   <fil_PH>Chinese_traditional localisation of Firefox</fil_PH>
   <fi>Chinese_traditional localisation of Firefox</fi>
   <fr_BE>Localisation en chinois traditionnel pour Firefox</fr_BE>
   <fr>Localisation en chinois traditionnel pour Firefox</fr>
   <gl_ES>Chinese_traditional localisation of Firefox</gl_ES>
   <gu>Chinese_traditional localisation of Firefox</gu>
   <he_IL>Chinese_traditional localisation of Firefox</he_IL>
   <hi>फायरफॉक्स ईएसआर का पारंपरिक_चीनी संस्करण</hi>
   <hr>Chinese_traditional localisation of Firefox</hr>
   <hu>Chinese_traditional localisation of Firefox</hu>
   <id>Chinese_traditional localisation of Firefox</id>
   <is>Chinese_traditional localisation of Firefox</is>
   <it>Localizzazione in cinese_tadizionale di Firefox</it>
   <ja>繁体中国語版 Firefox</ja>
   <kk>Chinese_traditional localisation of Firefox</kk>
   <ko>Chinese_traditional localisation of Firefox</ko>
   <ku>Chinese_traditional localisation of Firefox</ku>
   <lt>Chinese_traditional localisation of Firefox</lt>
   <mk>Chinese_traditional localisation of Firefox</mk>
   <mr>Chinese_traditional localisation of Firefox</mr>
   <nb_NO>Chinese_traditional localisation of Firefox</nb_NO>
   <nb>Kinesisk (tradisjonell) lokaltilpassing av Firefox</nb>
   <nl_BE>Chinese_traditional localisation of Firefox</nl_BE>
   <nl>Chinese (traditionele) lokalisatie van Firefox</nl>
   <or>Chinese_traditional localisation of Firefox</or>
   <pl>Chinese_traditional localisation of Firefox</pl>
   <pt_BR>Chinês Tradicional Localização para o Firefox</pt_BR>
   <pt>Chinês_tradicional Localização para Firefox ESR</pt>
   <ro>Chinese_traditional localisation of Firefox</ro>
   <ru>Chinese_traditional localisation of Firefox</ru>
   <sk>Chinese_traditional localisation of Firefox</sk>
   <sl>Kitajske (tradicionalno) krajevne nastavitve za Firefox</sl>
   <so>Chinese_traditional localisation of Firefox</so>
   <sq>Përkthimi i Firefox-it në kinezçe tradicionale</sq>
   <sr>Chinese_traditional localisation of Firefox</sr>
   <sv>Kinesisk_traditionell lokalisering av Firefox</sv>
   <th>Chinese_traditional localisation of Firefox</th>
   <tr>Firefox Geleneksel_Çince yerelleştirmesi</tr>
   <uk>Chinese_traditional localisation of Firefox</uk>
   <vi>Chinese_traditional localisation of Firefox</vi>
   <zh_CN>Chinese_traditional localisation of Firefox</zh_CN>
   <zh_HK>Chinese_traditional localisation of Firefox</zh_HK>
   <zh_TW>Chinese_traditional localisation of Firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-l10n-zh-tw
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
f-l10n-zh-tw
</uninstall_package_names>
irefox
</app>
