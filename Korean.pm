<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Language
</category>

<name>
Korean
</name>

<description>
   <am>Korean dictionary for hunspell</am>
   <ar>Korean dictionary for hunspell</ar>
   <be>Korean dictionary for hunspell</be>
   <bg>Korean dictionary for hunspell</bg>
   <bn>Korean dictionary for hunspell</bn>
   <ca>Diccionari Coreà per hunspell</ca>
   <cs>Korean dictionary for hunspell</cs>
   <da>Korean dictionary for hunspell</da>
   <de>Koreanisches Wörterbuch für “Hunspell”</de>
   <el>Λεξικό στα Κορεατικά για hunspell</el>
   <en>Korean dictionary for hunspell</en>
   <es_ES>Diccionario Coreano para hunspell</es_ES>
   <es>Diccionario Coreano para hunspell</es>
   <et>Korean dictionary for hunspell</et>
   <eu>Korean dictionary for hunspell</eu>
   <fa>Korean dictionary for hunspell</fa>
   <fil_PH>Korean dictionary for hunspell</fil_PH>
   <fi>Korean dictionary for hunspell</fi>
   <fr_BE>Coréen dictionnaire pour hunspell</fr_BE>
   <fr>Coréen dictionnaire pour hunspell</fr>
   <gl_ES>Korean dictionary for hunspell</gl_ES>
   <gu>Korean dictionary for hunspell</gu>
   <he_IL>Korean dictionary for hunspell</he_IL>
   <hi>Hunspell हेतु कोरियाई शब्दकोष</hi>
   <hr>Korean dictionary for hunspell</hr>
   <hu>Korean dictionary for hunspell</hu>
   <id>Korean dictionary for hunspell</id>
   <is>Korean dictionary for hunspell</is>
   <it>Dizionario coreano per hunspell</it>
   <ja>hunspell 用韓国（朝鮮）語辞書</ja>
   <kk>Korean dictionary for hunspell</kk>
   <ko>Korean dictionary for hunspell</ko>
   <ku>Korean dictionary for hunspell</ku>
   <lt>Korean dictionary for hunspell</lt>
   <mk>Korean dictionary for hunspell</mk>
   <mr>Korean dictionary for hunspell</mr>
   <nb_NO>Korean dictionary for hunspell</nb_NO>
   <nb>Koreansk ordliste for hunspell</nb>
   <nl_BE>Korean dictionary for hunspell</nl_BE>
   <nl>Korean dictionary for hunspell</nl>
   <or>Korean dictionary for hunspell</or>
   <pl>Korean dictionary for hunspell</pl>
   <pt_BR>Dicionário Coreano para o hunspell (corretor ortográfico e analisador morfológico)</pt_BR>
   <pt>Dicionário Coreano para hunspell</pt>
   <ro>Korean dictionary for hunspell</ro>
   <ru>Korean dictionary for hunspell</ru>
   <sk>Korean dictionary for hunspell</sk>
   <sl>Korejski slovar za hunspell</sl>
   <so>Korean dictionary for hunspell</so>
   <sq>Fjalor koreançe për hunspell</sq>
   <sr>Korean dictionary for hunspell</sr>
   <sv>Koreansk ordbok för hunspell</sv>
   <th>Korean dictionary for hunspell</th>
   <tr>Hunspell için Korece sözlük</tr>
   <uk>Korean dictionary for hunspell</uk>
   <vi>Korean dictionary for hunspell</vi>
   <zh_CN>Korean dictionary for hunspell</zh_CN>
   <zh_HK>Korean dictionary for hunspell</zh_HK>
   <zh_TW>Korean dictionary for hunspell</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-ko
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ko
</uninstall_package_names>

</app>
